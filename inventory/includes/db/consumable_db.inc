<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
function add_consumable_master($master_id, $company_id, $unit_id, 
	$principal_id, $consumable_code, $consumable_name, $size, 
	$weight, $cbm, $gsm, $description, $document, $consumable_image)
{
	$sql = "INSERT INTO ".TB_PREF."consumable_master (master_id, company_id,
			unit_id, principal_id, consumable_code, consumable_name, 
			size, weight, cbm, gsm, description, document,consumable_image)
		VALUES ("
		.db_escape($master_id).","
		.db_escape($company_id).","
		.db_escape($unit_id).","
		.db_escape($principal_id).","
		.db_escape($consumable_code).","
		.db_escape($consumable_name).","
		.db_escape($size).","
		.db_escape($weight).","
		.db_escape($cbm).","
		.db_escape($gsm).","
		.db_escape($description).","
		.db_escape($document).","
		.db_escape($consumable_image).")";

	db_query($sql,"an Consumable could not be added");
}

function update_consumable_master($id, $master_id, $company_id, $unit_id, 
	$principal_id, $consumable_code, $consumable_name, $size, 
	$weight, $cbm, $gsm, $description, $document, $consumable_image)

{
	$sql = "UPDATE ".TB_PREF."consumable_master SET "
		."master_id = ".db_escape($master_id).","
		."company_id = ".db_escape($company_id).","
		."unit_id = ".db_escape($unit_id).","
		."principal_id = ".db_escape($principal_id).","
		."consumable_code = ".db_escape($consumable_code).","
		."consumable_name = ".db_escape($consumable_name).","
		."size = ".db_escape($size).","
		."weight = ".db_escape($weight).","
		."cbm = ".db_escape($cbm).","
		."gsm = ".db_escape($gsm).","
		."description = ".db_escape($description).","
		."document = ".db_escape($document).","
		."consumable_image = ".db_escape($consumable_image)
        ."WHERE consumable_id = ".db_escape($id);

	db_query($sql,"an consumable could not be updated");
}

function delete_consumable_master($id)
{
	$sql="DELETE FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($id);

	db_query($sql,"an consumable could not be deleted");
}

function get_consumable_master($show_inactive)
{
	$sql = "SELECT cm.*, m.master_name as master_name, c.company_name as company_name, pm.name as principal_name  FROM ".TB_PREF."consumable_master cm, ".TB_PREF."master_creation m, "
	.TB_PREF."item_company c, ".TB_PREF."principal_master pm WHERE cm.master_id=m.master_id AND cm.company_id = c.company_id AND cm.principal_id = pm.principal_id ";
	if (!$show_inactive)
		$sql .= " AND !cm.inactive";
	$sql .= " ORDER by consumable_code";
	return db_query($sql, "could not get consumable");
}


function get_consumable_master_row($id)
{
	$sql="SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($id);

	$result = db_query($sql,"an consumable could not be retrieved");

	return db_fetch($result);
}
function get_generatecode(){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master ORDER BY consumable_id DESC LIMIT 1";
	return db_query($sql, "could not get consumable");
}
/*function get_consumable_master($show_inactive)
{
	$sql = "SELECT c.*, t.name as tax_name FROM ".TB_PREF."stock_category c, "
		.TB_PREF."item_tax_types t WHERE c.dflt_tax_type=t.id";
	if (!$show_inactive)
		$sql .= " AND !c.inactive";
	$sql .= " ORDER by description";

	return db_query($sql, "could not get stock categories");
}

function get_consumable_master($id)
{
	$sql="SELECT * FROM ".TB_PREF."stock_category WHERE category_id=".db_escape($id);

	$result = db_query($sql,"an item category could not be retrieved");

	return db_fetch($result);
}

function get_category_name($id)
{
	$sql = "SELECT description FROM ".TB_PREF."stock_category WHERE category_id=".db_escape($id);

	$result = db_query($sql, "could not get sales type");

	$row = db_fetch_row($result);
	return $row[0];
}*/

?>