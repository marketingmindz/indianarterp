<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// - -  ----------------    Code by bajrang   - get opening stock  ------- - - - - -

function get_cons_opening_stock($selected_id)
{
	$sql = "SELECT stock_qty FROM ".TB_PREF."opening_stock_master where open_stock_id = ".db_escape($selected_id);
    $result =   db_query($sql, "could not get opening stock quantity");
    $stock = db_fetch($result);
	return $stock['stock_qty'];
}

function get_all_opening_stock_master() {
  $sql = "SELECT * FROM ".TB_PREF."opening_stock_master";
    $result =   db_query($sql, "could not get opening stock Master");
	return $result;
}

function check_consumable_stock($stock_type_id, $consumble_id, $consumble_category, $location_id, $work_center_id)
{
	$check_consumable = "select stock_type_id,consumable_id,consumable_category from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && consumable_id = '$consumble_id' && consumable_category = '$consumble_category' && location_id = '$location_id' && work_center_id = '$work_center_id' ";
	$result = db_query($check_consumable,'check_consumable_stock stock check error');
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_design_stock($stock_type_id, $pro_design_id, $location_id, $work_center_id)
{
	$check_design = "select stock_type_id,pro_design_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_design_id = '$pro_design_id'  && location_id = '$location_id' && work_center_id = '$work_center_id'";
	$result = db_query($check_design);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_finish_stock($stock_type_id,$pro_design_id, $pro_finish_id, $location_id, $work_center_id)
{
	$check_finish = "select stock_type_id,pro_finish_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_finish_id = '$pro_finish_id' && pro_design_id = '$pro_design_id'  && location_id = '$location_id' && work_center_id = '$work_center_id'";
	$result = db_query($check_finish);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function update_stock_transaction($location_id, $work_center_id, $consumable_category, $consumable_id, $unit, $stock_quantity, $date, $type, $closing_stock, $opening_stock=0)
{
	$dateTime = dateTimeNow();

	$sql = "insert into ".TB_PREF."stock_transaction(date, consumable_category, consumable_id, location_id, work_center_id, pro_team_id, opening_stock, closing_stock, trans_quantity, unit, type) values("
		.db_escape($dateTime).","
		.db_escape($consumable_category).","
		.db_escape($consumable_id).","
		.db_escape($location_id).","
		.db_escape($work_center_id).","
		.db_escape($pro_team_id).","
		.db_escape($opening_stock).","
		.db_escape($closing_stock).","
		.db_escape($stock_quantity).","
		.db_escape($unit).","
		.db_escape($type)
		.");";
	db_query($sql, "Stock Transaction is not updated.");
}

function add_open_stock_category($stock_type_id, $location_id, $work_center_id, $consumble_id, $consumble_category, $pro_cat_id, $pro_range_id,$pro_design_id,$finish_code_id, $unit, $level, $stock_quantity,$date)
{
	$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id, location_id, work_center_id,consumable_id,consumable_category,pro_cat_id,pro_range_id,pro_design_id,pro_finish_id,unit,level,stock_qty,date)
		VALUES (" .db_escape($stock_type_id).","
		.db_escape($location_id).","
		.db_escape($work_center_id).","
		.db_escape($consumble_id).","
		.db_escape($consumble_category).","
		.db_escape($pro_cat_id).","
		.db_escape($pro_range_id).","
		.db_escape($pro_design_id).","
		.db_escape($finish_code_id).","
		.db_escape($unit).","
		.db_escape($level).","
		.db_escape($stock_quantity).","
		.db_escape($date).")";
        
   	   db_query($sql,"could not add new stock");

   	   $closing_stock = $stock_quantity;
   	   update_stock_transaction($location_id, $work_center_id, $consumble_id, $consumble_category, $unit, $stock_quantity, $date, "new", $closing_stock);
	
}


function update_open_stock_category($selected_id,$stock_type_id, $location_id, $work_center_id, $consumble_id, $consumble_category, $pro_cat_id, $pro_range_id,$pro_design_id,$finish_code_id,$unit,$level,$stock_quantity,$date){

	$opening_stock = get_cons_opening_stock($selected_id);

	$sql = "UPDATE ".TB_PREF."opening_stock_master SET stock_type_id=".db_escape($stock_type_id)
	  .",location_id=".db_escape($location_id)
	  .",work_center_id=".db_escape($work_center_id)
	  .",consumable_id=".db_escape($consumble_id)
	  .",consumable_category=".db_escape($consumble_category)
	  .",pro_cat_id=".db_escape($pro_cat_id)
	  .",pro_range_id=".db_escape($pro_range_id)
	  .",pro_design_id=".db_escape($pro_design_id)
	  .",pro_finish_id=".db_escape($finish_code_id)
	  .",unit=".db_escape($unit)
	  .",level=".db_escape($level)
	  .",stock_qty=".db_escape($stock_quantity)
	  .",date=".db_escape($date)
    	."WHERE open_stock_id=".db_escape($selected_id);
    	db_query($sql, "could not update opening stock master");
	
	$closing_stock = $stock_quantity;
	$txn_quantity = $closing_stock-$opening_stock;
	update_stock_transaction($location_id, $work_center_id, $consumble_id, $consumble_category, $unit, $txn_quantity, $date, "Update", $closing_stock, $opening_stock );

}

function delete_opening_stock($selected_id)
{
	$sql = "DELETE FROM ".TB_PREF."opening_stock_master WHERE open_stock_id=".db_escape($selected_id);
	db_query($sql, "could not delete opening stock master");
}

function get_available_quntity($sub_id){
	$sql="SELECT sum(quantity) FROM ".TB_PREF."opening_master_stock WHERE sub_master_id=".db_escape($sub_id)." AND type = 'cr'";
	$total_cr = db_query($sql,"could not add new stock");
	echo $total_cr;
	return $total_cr;
}




function get_stock_type($stock_type_id) {
  $sql = "SELECT stock_name FROM ".TB_PREF."stock_type where stock_type_id = ".db_escape($stock_type_id);
    $result =  db_query($sql, "could not get stock type");
	return db_fetch($result);
}


function get_consumable_name($consumable_id)
{
	$sql = "SELECT master_id, master_name, inactive, master_reference FROM ".TB_PREF."master_creation where master_id= ".db_escape($consumable_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);	
}

function get_finish_product_name($pro_finish_id)
{
	$sql = "SELECT finish_comp_code FROM ".TB_PREF."finish_product where finish_pro_id= ".db_escape($pro_finish_id);
	$result = db_query($sql,"Could not get finish name");
	return db_fetch($result);	
}

function get_design_product_name($pro_design_id)
{
	$sql = "SELECT design_code FROM ".TB_PREF."design_code where design_id= ".db_escape($pro_design_id);
	$result = db_query($sql,"Could not get design code");
	return db_fetch($result);	
}

function get_opening_stock_master($stock_id){
	$sql = "SELECT * FROM ".TB_PREF."opening_stock_master where open_stock_id= ".db_escape($stock_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);
}



function get_opening_stock_search($all_items)
{
	global $order_number;
	 $sql ='Select op.open_stock_id,l.location_name,w.work_center_name, s.stock_name, 
	 CASE WHEN op.consumable_id = -1 THEN "N/A" ELSE m.master_name END as ConsNAME,
	 CASE WHEN op.consumable_category = -1 THEN "N/A" ELSE c.consumable_name END as ConsCAT,
	 CASE WHEN i.company_name IS NULL THEN "No Brand" ELSE i.company_name END as company_name,
	 c.consumable_code, 
	 CASE WHEN op.pro_design_id = -1 THEN "N/A" ELSE d.design_code END as DesignNAME, 
	 CASE WHEN op.pro_finish_id = -1 THEN "N/A" ELSE f.finish_comp_code END as FinshNAME,
	op.unit, op.level, op.stock_qty, op.date from '.TB_PREF.'opening_stock_master op 
	 Left Join '.TB_PREF.'finish_product f on f.finish_pro_id =op.pro_finish_id 
	 LEFT Join '.TB_PREF.'design_code d on op.pro_design_id = d.design_id 
	 Left Join '.TB_PREF.'master_creation m on op.consumable_id = m.master_id
	 Left Join '.TB_PREF.'consumable_master c on op.consumable_category = c.consumable_id 
	 Left Join '.TB_PREF.'item_company i on i.company_id = c.company_id
	 Left Join '.TB_PREF.'stock_type s on op.stock_type_id = s.stock_type_id 
	 Left Join '.TB_PREF.'location_master l on op.location_id = l.location_id 
	 Left Join '.TB_PREF.'work_center w on op.work_center_id = w.work_center_id where  "1" = "1" ';	 
	 if(isset($_POST['location_id']) && $_POST['location_id'] != '-1')
	 {
		 $sql .= " AND op.location_id = ".db_escape($_POST['location_id']) ;
	 }
	 if(isset($_POST['work_center_id']) && $_POST['work_center_id'] != '-1')
	 {
		 $sql .= " AND op.work_center_id = ".db_escape($_POST['work_center_id']) ;
	 }
	 if(isset($_POST['cons_type']) && $_POST['cons_type'] != '-1')
	 {
		 $sql .= " AND op.consumable_id = ".db_escape($_POST['cons_type'])  ;
	 }
	 if(isset($_POST['cons_select']) && $_POST['cons_select'] != '-1' && $_POST['cons_select'] != '0')
	 {
		 $sql .= " AND op.consumable_category = ".db_escape($_POST['cons_select']);
	 }
	  $sql .=' order by op.open_stock_id DESC';
	return $sql;
}

/*get stock transaction sql*/
function get_stock_transaction_sql($all_items)
{
	global $order_number;
	 $sql ='Select txn.trans_id, l.location_name,w.work_center_name, m.master_name, c.consumable_name, c.consumable_code, CASE WHEN i.company_name IS NULL THEN "No Brand" ELSE i.company_name END as company_name, txn.opening_stock, txn.closing_stock, txn.trans_quantity, txn.unit, txn.type, txn.date from 
	'.TB_PREF.'stock_transaction txn 
	 Left Join '.TB_PREF.'master_creation m on txn.consumable_category = m.master_id
	 Left Join '.TB_PREF.'consumable_master c on txn.consumable_id = c.consumable_id 
	 Left Join '.TB_PREF.'item_company i on i.company_id = c.company_id
	 Left Join '.TB_PREF.'location_master l on txn.location_id = l.location_id 
	 Left Join '.TB_PREF.'work_center w on txn.work_center_id = w.work_center_id where  "1" = "1" ';	 
	 if(isset($_POST['location_id']) && $_POST['location_id'] != '-1')
	 {
		 $sql .= " AND txn.location_id = ".db_escape($_POST['location_id']) ;
	 }
	 if(isset($_POST['work_center_id']) && $_POST['work_center_id'] != '-1')
	 {
		 $sql .= " AND txn.work_center_id = ".db_escape($_POST['work_center_id']) ;
	 }
	 if(isset($_POST['cons_type']) && $_POST['cons_type'] != '-1')
	 {
		 $sql .= " AND txn.consumable_id = ".db_escape($_POST['cons_type'])  ;
	 }
	 if(isset($_POST['cons_select']) && $_POST['cons_select'] != '-1' && $_POST['cons_select'] != '0')
	 {
		 $sql .= " AND txn.consumable_category = ".db_escape($_POST['cons_select']);
	 }
	  $sql .=' order by txn.date DESC, txn.trans_id DESC';
	return $sql;
}



//--------------------------------------------------------------------------------------------------

/*function update_brand($id, $name)
{
    	$sql = "UPDATE ".TB_PREF."make_brand SET name=".db_escape($name)
    	." WHERE brand_id=".db_escape($id);
    	db_query($sql, "could not update brand");
}*/
//--------------------------------------------------------------------------------------------------

/*function delete_brand($id)
{
	$sql = "DELETE FROM ".TB_PREF."make_brand WHERE brand_id=".db_escape($id);

	db_query($sql, "could not delete brand");
}*/
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

/*function get_all_brand($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."make_brand";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get brand");
}*/

/*function get_brand($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."make_brand WHERE brand_id=".db_escape($selected_id);
	$result = db_query($sql,"brand could not be retrieved");
	return db_fetch($result);
}*/

/*function check_brand($name)
{
	 $sql = "SELECT * FROM  ".TB_PREF."make_brand WHERE name= ".db_escape($name)."  "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}*/
/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/



?>