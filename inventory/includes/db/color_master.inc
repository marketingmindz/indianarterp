<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_color_master($color_name, $color_description, $color_reference, $color_image)
{
	
		$sql = "INSERT INTO ".TB_PREF."color_master(color_name, color_description, color_reference, color_image)
		VALUES (" . db_escape($color_name) . ", " .
		db_escape($color_description). ", " . db_escape($color_reference) . ", " . db_escape($color_image) . ")";
		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_color_master($id, $color_name, $color_description, $color_reference, $color_image)
{
    	$sql = "UPDATE ".TB_PREF."color_master SET color_name=".db_escape($color_name)
    	.",color_description=".db_escape($color_description)
    	.",color_reference=".db_escape($color_reference)
    	.",color_image=".db_escape($color_image)
    	."WHERE color_id=".db_escape($id);
    	db_query($sql, "could not update color master");
}
//--------------------------------------------------------------------------------------------------

function delete_color_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."color_master WHERE color_id=".db_escape($id);

	db_query($sql, "could not delete color master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_color_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."color_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get color master");
}

function get_color_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."color_master WHERE color_id=".db_escape($selected_id);
	$result = db_query($sql,"master could not be retrieved");
	return db_fetch($result);
}

function check_color($color_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m , '.TB_PREF.'color_master c, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($color_reference).'
	 OR cat.cat_reference = '.db_escape($color_reference).' OR m.master_reference = '.db_escape($color_reference).' OR c.color_reference = '.db_escape($color_reference).' OR w.wood_reference = '.db_escape($color_reference).' OR s.skin_reference = '.db_escape($color_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_color($color_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat, '.TB_PREF.'master_creation m, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($color_reference).' 
	 	OR cat.cat_reference = '.db_escape($color_reference).' OR m.master_reference = '.db_escape($color_reference).' OR w.wood_reference = '.db_escape($color_reference).' OR s.skin_reference = '.db_escape($color_reference).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}




?>