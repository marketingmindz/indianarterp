<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//----------------------------------------------------------------------------------------
function get_desing_code_id($code_id){
 	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_code=".db_escape($code_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_reference_no()
{
	$sql = "SELECT reference_no FROM ".TB_PREF."item_adjustment_master ORDER BY reference_no DESC LIMIT 1";
    $query1 = db_query($sql,"could not get the ref code.");

	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $reference_no= $row['reference_no'];
	   $reference_no++;
	}
	if($reference_no == '' && $reference_no == NULL)
	{
		$reference_no = 1;
	}	
	return $reference_no;
}

//---------- Code by Bajrang - Check Product Type stock  && handle this  --------------
function update_product_type_stock($design_id,$finish_code_id,$quntity,$id,$available_quantity)
{
	$new_quantity = $quntity+$available_quantity;
	$sql = "update ".TB_PREF."item_adjustment_rel set quantity = ".db_escape($new_quantity)." where design_id=".db_escape($design_id)."&& pro_finish_code_id =".db_escape($finish_code_id)." && adj_id = ".db_escape($id);
	db_query($sql,"Product stock cannot be updated");
}

function adjust_product_stock($location_id,$work_center_id,$pro_team_id, $reference_no, $date, $stock_type_id, $type_id, $memo, $category_id,$range_id, $design_id, $finish_code_id,$pro_qty,$pro_unit)
{
	$adj_date = date2sql($date);
	
	$sql = "INSERT INTO ".TB_PREF."item_adjustment_master(location_id, work_center_id, pro_team_id, reference_no,	date, stock_type_id, adj_type,memo) VALUES(";
		 $sql .= db_escape($location_id) . "," .
			 db_escape($work_center_id) . "," .
			 db_escape($pro_team_id) . ", " .
			 db_escape($reference_no) . ", " .
			 db_escape($adj_date) . ", " .
			 db_escape($stock_type_id) . ", " .
			 db_escape($type_id) . ", " .
			 db_escape($memo) . ")";
	
			db_query($sql, "Adjustment could not be made");
			
			/*Get the auto increment value of the order number created from the sql above */
     $adj_id = db_insert_id();
			
			$sql = "INSERT INTO ".TB_PREF."item_adjustment_rel (adj_id, consumable_id, consumable_category, category_id, range_id, design_id, pro_finish_code_id, unit, quantity) VALUES (";
			$sql .= $adj_id . ", " . 
			"'-1','-1',".
			db_escape($category_id). "," .
			db_escape($range_id). "," .
			db_escape($design_id) . "," .
			db_escape($finish_code_id) . ", " .
			db_escape($pro_unit) . ", " .
			db_escape($pro_qty). ")";
			
			db_query($sql, "adjustment relation can not be made");
			$consumable_type_obj->lines_on_con[$line_no]->cons_detail_rec = db_insert_id();

}


function check_product_opening_stock($location_id, $work_center_id, $design_id, $finish_code_id, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			display_error(_("Insufficient stock!!."));
			return false;
		}		

		if($stock_qty == NULL && $stock_qty == '')
		{
			display_error(_("Stock is not available for this item"));
			die;
		}
		else
		{
			if($stock_qty < $quantity)
			{
				display_error(_("Insufficient stock in opening stock. Opening stock quantity is ".$stock_qty));
			die;
			}
			$stock_qty = $stock_qty-$quantity;
			$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
			db_query($sql, "cannot update opening stock");
			return true;
		}
	
}


function check_product_stock(&$header_adjustment,&$product_type_obj)
{
	foreach ($product_type_obj->line_items as $line_no => $line_items)
	 {
		 $stock_availbale = check_product_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'],$line_items->design_id, $line_items->finish_code_id, $line_items->pro_qty);
		 if($stock_availbale)
		 {
			$sql = "SELECT adj_rel.adj_id, adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id AND adj_rel.design_id = " . db_escape($line_items->design_id)." AND adj_rel.pro_finish_code_id = ".db_escape($line_items->finish_code_id) . " where adj.location_id = ". db_escape($header_adjustment['location_id']) ." AND adj.work_center_id = " .db_escape($header_adjustment['work_center_id']) . " AND adj.pro_team_id = " .db_escape($header_adjustment['pro_team_id']);
			
			$result = db_query($sql,"adjustment cannot be processed - #product Type");
			$adj_id = db_fetch($result);
			$id = $adj_id['adj_id'];
			$available_quantity = $adj_id['quantity'];
			if($id != NULL && $id != '')
			{
				update_product_type_stock($line_items->design_id,$line_items->finish_code_id,$line_items->pro_qty,$id,$available_quantity);
			}
			else
			{
				adjust_product_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'], $header_adjustment['pro_team_id'], $header_adjustment['reference_name'], $header_adjustment['date'], $header_adjustment['stock_type_id'], $header_adjustment['type_id'], $header_adjustment['memo_description'], $line_items->category_id,$line_items->range_id, $line_items->design_id, $line_items->finish_code_id,  $line_items->pro_qty, $line_items->pro_unit);
			}
		 }
	 }

}

//-----------------   Code by Bajrang - -check Consumable stock && handle this -  -  -- - ----
function update_consumable_type_stock($consumable_id,$consumable_category,$quntity,$id,$available_quantity)
{
	$new_quantity = $quntity+$available_quantity;
	$sql = "update ".TB_PREF."item_adjustment_rel set quantity = ".db_escape($new_quantity)." where consumable_id=".db_escape($consumable_id)."&& consumable_category =".db_escape($consumable_category)." && adj_id = ".db_escape($id);
	db_query($sql,"consumbale_stock_updated");
}

function adjust_consumable_stock($location_id,$work_center_id,$pro_team_id, $reference_no, $date, $stock_type_id, $type_id, $memo, $consumable_id,$consumable_category,$quantity,$unit)
{
	$adj_date = date2sql($date);
	
	$sql = "INSERT INTO ".TB_PREF."item_adjustment_master(location_id, work_center_id, pro_team_id, reference_no,	date, stock_type_id, adj_type,memo) VALUES(";
		 $sql .= db_escape($location_id) . "," .
			 db_escape($work_center_id) . "," .
			 db_escape($pro_team_id) . ", " .
			 db_escape($reference_no) . ", " .
			 db_escape($adj_date) . ", " .
			 db_escape($stock_type_id) . ", " .
			 db_escape($type_id) . ", " .
			 db_escape($memo) . ")";
	
			db_query($sql, "Adjustment could not be made");
			
			/*Get the auto increment value of the order number created from the sql above */
     $design_id = db_insert_id();
			
	$sql = "INSERT INTO ".TB_PREF."item_adjustment_rel (adj_id, consumable_id, consumable_category, category_id, range_id, design_id, pro_finish_code_id, unit, quantity) VALUES (";
			$sql .= $design_id . ", " . db_escape($consumable_id).",".
			db_escape($consumable_category) . "," . 
			"'-1','-1','-1','-1',".
			
			db_escape($unit) . ", " .
			db_escape($quantity). ")";
			
			db_query($sql, "adjustment relation can not be made");
			$consumable_type_obj->lines_on_con[$line_no]->cons_detail_rec = db_insert_id();
}

function check_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			display_error(_("Insufficient stock!!."));
			return false;
		}		

		if($stock_qty == NULL && $stock_qty == '')
		{
			display_error(_("Stock is not available for this item"));
			die;
		}
		else
		{
			if($stock_qty < $quantity)
			{
				display_error(_("Insufficient stock in opening stock. Opening stock quantity is ".$stock_qty));
			die;
			}
			return true;
		}	
}

function generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity, $unit, $date)
{
	$pr_no = '';
	$sql = "SELECT pr_no FROM ".TB_PREF."purchase_request ORDER BY pr_no DESC LIMIT 1";
	$query1 = db_query($sql,"cannot get purchase request no.");
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $pr_no= $row['pr_no'];
	   $pr_no++;
	}
	if($pr_no == '' && $pr_no == NULL)
	{
		$pr_no = 1;
	}
	$date = date2sql($date);
	$sql = "INSERT INTO ".TB_PREF."purchase_request(pr_no,location_id,work_center_id,type,date) VALUES(";
	$sql .= db_escape($pr_no) . "," .
	db_escape($location_id) . "," .
	db_escape($work_center_id) . "," ."'1',".
	db_escape($date).")";
	db_query($sql, "Purchase request is not created");
		
	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
		$sql .= $pr_no . ", " . 
		db_escape($consumable_id).",".
		db_escape($consumable_category) . "," . 		
		db_escape($unit) . ", " .
		db_escape($request_quantity). ")";
		db_query($sql, " purchase request items are not inserted in database can not be made");
}

function consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category)
{
	$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$available_stock_qty = $stock['stock_qty'];
		return $available_stock_qty;
}


function update_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity,$unit, $date)
{
		$available_stock_qty = consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category);
		$stock_qty = $available_stock_qty-$quantity;
		$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		db_query($sql, "cannot update opening stock");

		$closing_stock = $stock_qty;
		$opening_stock = $available_stock_qty;
		$txn_quantity = $quantity;
		update_stock_transaction($location_id, $work_center_id, $consumable_id, $consumable_category, $unit, $txn_quantity, $date, "Item Assign", $closing_stock, $opening_stock);

		/*Auto generate purchase request*/
		/*if($available_stock_qty < $level && $stock_qty < $available_stock_qty)
		{
			$request_quantity = $quantity;
			generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity,$unit, $date);
		}
		elseif($stock_qty < $level)
		{
			$request_quantity = $level - $stock_qty;
			generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity,$unit, $date);
		}*/
		return true;
}
function adjust_consumable_type_stock(&$header_adjustment,&$consumable_type_obj)
{
	foreach ($consumable_type_obj->line_con as $line_no => $lines_on_con)
	{
	 $stock_availbale = check_consumable_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'], $lines_on_con->cons_type, $lines_on_con->cons_select, $lines_on_con->cons_quntity);
	}
	if($stock_availbale)
	{
		foreach ($consumable_type_obj->line_con as $line_no => $lines_on_con)
		{
            update_consumable_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'], $lines_on_con->cons_type, $lines_on_con->cons_select, $lines_on_con->cons_quntity,$lines_on_con->cons_unit,$header_adjustment['date']);

			$sql = "SELECT adj_rel.adj_id, adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id AND adj_rel.consumable_id = " . db_escape($lines_on_con->cons_type)." AND adj_rel.consumable_category = ".db_escape($lines_on_con->cons_select) . " where adj.location_id = ". db_escape($header_adjustment['location_id']) ." AND adj.work_center_id = " .db_escape($header_adjustment['work_center_id']) . " AND adj.pro_team_id = " .db_escape($header_adjustment['pro_team_id']);
			
			$result = db_query($sql,"adjustment cannot be processed - #XXX");
			$adj_id = db_fetch($result);
			$id = $adj_id['adj_id'];
			$available_quantity = $adj_id['quantity'];
			if($id != NULL && $id != '')
			{
				update_consumable_type_stock($lines_on_con->cons_type,$lines_on_con->cons_select,$lines_on_con->cons_quntity,$id,$available_quantity);
			}
			else
			{
				adjust_consumable_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'], $header_adjustment['pro_team_id'], $header_adjustment['reference_name'], $header_adjustment['date'], $header_adjustment['stock_type_id'], $header_adjustment['type_id'], $header_adjustment['memo_description'], $lines_on_con->cons_type,$lines_on_con->cons_select, $lines_on_con->cons_quntity, $lines_on_con->cons_unit);
			}
		}
	}

}


function do_adjustment(&$header_adjustment,&$consumable_type_obj,&$product_type_obj)
{
	if($header_adjustment['stock_type_id'] == '101')
	{
		$chech_stock = adjust_consumable_type_stock($header_adjustment, $consumable_type_obj );
	}
	if($header_adjustment['stock_type_id'] == '102')
	{
		$chech_stock = check_product_stock($header_adjustment, $product_type_obj);
	}
	
	return $design_id;
}

// code for negative adjustment - ---- 

//---------- Code by Bajrang - Check Product Type stock  && handle this  --------------
function update_neg_product_type_stock($design_id,$finish_code_id,$quntity,$id,$available_quantity)
{
	$new_quantity = $available_quantity - $quntity;
	$sql = "update ".TB_PREF."item_adjustment_rel set quantity = ".db_escape($new_quantity)." where design_id=".db_escape($design_id)."&& pro_finish_code_id =".db_escape($finish_code_id)." && adj_id = ".db_escape($id);
	db_query($sql,"Product stock cannot be updated");
}


function update_neg_product_opening_stock($location_id, $work_center_id, $design_id, $finish_code_id, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			display_error(_("Enter correct location and work center - ##product_neg_stock##!!."));
			die;
		}
		else
		{
			$stock_qty = $stock_qty+$quantity;
			$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
			db_query($sql, "cannot update opening stock");
			return true;
		}
	
}


function check_neg_product_stock(&$header_adjustment,&$product_type_obj)
{
	foreach ($product_type_obj->line_items as $line_no => $line_items)
	 {
		 check_neg_product_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'],$line_items->design_id, $line_items->finish_code_id, $line_items->pro_qty);
		 if($stock_availbale)
		 {
			$sql = "SELECT adj_rel.adj_id, adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id where adj_rel.design_id = " . db_escape($line_items->design_id)." AND adj_rel.pro_finish_code_id = ".db_escape($line_items->finish_code_id) . " AND adj.location_id = ". db_escape($header_adjustment['location_id']) ." AND adj.work_center_id = " .db_escape($header_adjustment['work_center_id']) . " AND adj.pro_team_id = " .db_escape($header_adjustment['pro_team_id']);
			
			$result = db_query($sql,"adjustment cannot be processed - #product Type123321");
			$adj_id = db_fetch($result);
			$id = $adj_id['adj_id'];
			$available_quantity = $adj_id['quantity'];
			if($id != NULL && $id != '')
			{
				if($line_items->pro_qty < $available_quantity)
				{
					update_neg_product_type_stock($line_items->design_id,$line_items->finish_code_id,$line_items->pro_qty,$id,$available_quantity);
				}
				else
				{
					display_error(_("Enter quantity less than ".$available_quantity));
					die;
				}
			}
		 check_neg_product_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'],$line_items->design_id, $line_items->finish_code_id, $line_items->pro_qty);

		 }
	 }

}

//-----------------   Code by Bajrang - -check Consumable stock && handle this -  -  -- - ----
function update_neg_consumable_type_stock($consumable_id,$consumable_category,$quntity,$id,$available_quantity)
{
	$new_quantity = $available_quantity - $quntity;
	$sql = "update ".TB_PREF."item_adjustment_rel set quantity = ".db_escape($new_quantity)." where consumable_id=".db_escape($consumable_id)."&& consumable_category =".db_escape($consumable_category)." && adj_id = ".db_escape($id);
	db_query($sql,"consumbale_stock_updated");
}

function update_neg_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity, $unit)
{

	$available_stock_qty = consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category);

	$stock_qty = $available_stock_qty+$quantity;
	$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
	db_query($sql, "cannot update opening stock");

	$closing_stock = $stock_qty;
	$opening_stock = $available_stock_qty;
	$txn_quantity = $quantity;
	update_stock_transaction($location_id, $work_center_id, $consumable_id, $consumable_category, $unit, $txn_quantity, $date, "Item Return", $closing_stock, $opening_stock);

	return true;
}

function check_neg_consumable_stock(&$header_adjustment,&$consumable_type_obj)
{
	foreach ($consumable_type_obj->line_con as $line_no => $lines_on_con)
	 {
			$sql = "SELECT adj_rel.adj_id, adj_rel.quantity from  0_item_adjustment_master adj 
			         left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id 
					 where adj_rel.consumable_id = " . db_escape($lines_on_con->cons_type)." 
					 AND adj_rel.consumable_category = ".db_escape($lines_on_con->cons_select) . "
					 AND adj.location_id = ". db_escape($header_adjustment['location_id']) ." 
					 AND adj.work_center_id = " .db_escape($header_adjustment['work_center_id']) . " AND adj.pro_team_id = " .db_escape($header_adjustment['pro_team_id']);
			
			$result = db_query($sql,"adjustment cannot be processed - #XXX123");
			$adj_id = db_fetch($result);
			$id = $adj_id['adj_id'];
			$available_quantity = $adj_id['quantity'];
			if($available_quantity != NULL && $available_quantity != '')
			{
				if($lines_on_con->cons_quntity <= $available_quantity)
				{
					update_neg_consumable_type_stock($lines_on_con->cons_type,$lines_on_con->cons_select,$lines_on_con->cons_quntity,$id,$available_quantity);
				}
				else
				{
					display_error(_("Enter quantity less than ".$available_quantity));
					die;
				}
			}
			update_neg_consumable_opening_stock($header_adjustment['location_id'], $header_adjustment['work_center_id'], $lines_on_con->cons_type, $lines_on_con->cons_select, $lines_on_con->cons_quntity, $lines_on_con->cons_unit);

	 }

}


function do_negative_adjustment(&$header_adjustment,&$consumable_type_obj,&$product_type_obj)
{
	if($header_adjustment['stock_type_id'] == '101')
	{
		$chech_stock = check_neg_consumable_stock($header_adjustment, $consumable_type_obj );
	}
	if($header_adjustment['stock_type_id'] == '102')
	{
		$chech_stock = check_neg_product_stock($header_adjustment, $product_type_obj);
	}
	
	return $design_id;
}

//------------------------ Get Design code --------------------------------------------

function get_design_search($all_items)
{

	
	global $order_number;
	
	$sql ='Select d.design_code,d.product_name, CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeNAME, c.cat_name, p.pro_name, d.weight, d.height, d.density, d.description from '.TB_PREF.'design_code d Left Join '.TB_PREF.'item_range t on d.range_id = t.id Left Join '.TB_PREF.'item_category c on d.category_id = c.category_id 
	Left Join '.TB_PREF.'item_products p on d.product_type = p.pro_id';
		
	if (isset($order_number) && $order_number != "")
	{
		$sql .= " Where d.design_code LIKE ".db_escape('%'. $order_number . '%')." OR d.product_name LIKE".db_escape('%'. $order_number . '%');
	}
	
	
	
	return $sql;
}

function get_cons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_select_name($cons_select){
	$sql = "SELECT c.consumable_name, c.consumable_code, i.company_name FROM ".TB_PREF."consumable_master c
			Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
			 WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_design_code_row($design_code_id){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_code=".db_escape($design_code_id);
	$result = db_query($sql,"an design code could not be retrieved");
	return db_fetch($result);
}


function get_pro_category_name($pro_category){
	$sql = "SELECT * FROM ".TB_PREF."item_category WHERE category_id=".db_escape($pro_category);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_pro_range_name($range_id){
	$sql = "SELECT * FROM ".TB_PREF."item_range WHERE id=".db_escape($range_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_pro_design_code($design_id){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($design_id);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_pro_finish_code($finish_code_id){
	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_code_id);
	$result = db_query($sql);
	return db_fetch($result);
}
//-------------------- read assemble part --------------------------

function get_design_header($design_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($design_id);

   	$result = db_query($sql, "The design code cannot be retrieved");
	if (db_num_rows($result) == 1)
	{

      	$myrow = db_fetch($result);
				$order->design_id = $design_id;
      			$_POST['design_id'] = $myrow["design_id"];
				$_POST['category_id']  = $myrow["category_id"];
				//$_POST['collection_id']  = $myrow["collection_id"];
				$_POST['range_id']  = $myrow["range_id"];
				if($myrow["category_id"] > 0 && $myrow["range_id"]>0)
				   {
					    $_POST['collection_id']  = "Collection";
				   }else if($myrow["category_id"] > 0 && $myrow["range_id"]<=0){
				      $_POST['collection_id']  = "NonCollection";
				   }else{
					   $_POST['collection_id']  = "SelectCollection";
				}
				$_POST['product_name']  = $myrow["product_name"];
				$_POST['pro_id']  = $myrow["product_type"];
				$_POST['pkg_unit']  = $myrow["pkg_unit"];
				$_POST['pkg_w']  = $myrow["pkg_w"];
				$_POST['pkg_h']  = $myrow["pkg_h"];
				$_POST['pkg_d']  = $myrow["pkg_d"];
				$_POST['width_design']  = $myrow["weight"];
				$_POST['height_design']  = $myrow["height"];
				$_POST['density_design']  = $myrow["density"];
				$_POST['design_code']  = $myrow["design_code"];
				$_POST['description']  = $myrow["description"];
				$_POST['pro_cbm']  = $myrow["pro_cbm"];
				$_POST['pak_cbm']  = $myrow["pak_cbm"];

      	return true;
	}

	
	return false;
}

//*************************************************************
function read_design($design_id){
	
	$result = get_design_header($design_id, $order);
	
}

function read_asb_items($design_id, &$asb)
{
	
	$sql = "SELECT * FROM ".TB_PREF."assemble_part WHERE design_id=".db_escape($design_id); 
	$result = db_query($sql, "assemble part cannot be retrive");
	
	if (db_num_rows($result) > 0)
		{
			while ($myrow = db_fetch($result))
			{
	
				if ($asb->add_to_asb_part($asb->lines_on_asb, $myrow["part_name"],
					$myrow["assemble_width"],$myrow["assemble_height"],$myrow["assemble_density"],$myrow["iscbm"], $myrow["assemble_code"], $myrow["quantity"], $myrow["netweight"])) {
						$newline = &$asb->line_items[$asb->lines_on_asb-1];
						$newline->asb_detail_rec = $myrow["asb_line_details"];			
				}
			} 
			
		} 
}
function read_cons_items($design_id, &$cons)
{
	
	$sql = "SELECT * FROM ".TB_PREF."consumable_part WHERE design_id=".db_escape($design_id); 
	$result = db_query($sql, "consumable part cannot be retrive");
	
		if (db_num_rows($result) > 0)
			{
				while ($myrow1 = db_fetch($result))
				{
		
					if ($cons->add_to_con_part($cons->lines_on_con, $myrow1["cons_type"],
						$myrow1["cons_select"],$myrow1["cons_unit"],$myrow1["cons_qty"],$myrow1["cons_iscbm"])) {
							$newline1 = &$cons->line_con[($cons->lines_on_con-1)+100];
							$newline1->con_detail_rec = $myrow1["con_line_details"];			
					}
				} 
			}  
}
function get_design_image($des_id){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($des_id); 
	$result = db_query($sql);
	return db_fetch($result);
}

//------------ last reference id ----------------
function get_last_reference_id(){
	$sql = "SELECT reference_no FROM ".TB_PREF."item_adjustment_master ORDER BY reference_no DESC LIMIT 1"; 
	$result = db_query($sql);
	return db_fetch($result);
}

function check_stock_quantity($location_id,$c_id,$id,$consumable_id,$consumble_category)
{
	
	$sql = "SELECT  adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id AND adj_rel.consumable_id = '$consumable_id' AND adj_rel.consumable_category = '$consumble_category' where adj.location_id = '$location_id' AND adj.work_center_id = '$c_id' AND adj.pro_team_id = '$id'";
	
	$result = db_query($sql);
	return db_fetch($result);
}

?>