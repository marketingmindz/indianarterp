<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_product_type($pro_name)
{
	
		$sql = "INSERT INTO ".TB_PREF."item_products(pro_name)
		VALUES (" . db_escape($pro_name) . ")";
        
   	   db_query($sql,"could not add new product type");
}

//--------------------------------------------------------------------------------------------------

function update_product_type($id, $pro_name)
{
    	$sql = "UPDATE ".TB_PREF."item_products SET pro_name=".db_escape($pro_name)
    	." WHERE pro_id=".db_escape($id);
    	db_query($sql, "could not update product type");
}
//--------------------------------------------------------------------------------------------------

function delete_product_type($id)
{
	$sql = "DELETE FROM ".TB_PREF."item_products WHERE pro_id=".db_escape($id);

	db_query($sql, "could not delete product type");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_product_type($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."item_products";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get product type");
}

function get_product_type($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."item_products WHERE pro_id=".db_escape($selected_id);
	$result = db_query($sql,"product type could not be retrieved");
	return db_fetch($result);
}

function check_product_type($pro_name)
{
	 $sql = "SELECT * FROM  ".TB_PREF."item_products WHERE pro_name= ".db_escape($pro_name)."  "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}
/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/



?>