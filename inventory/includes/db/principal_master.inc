<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_principal($name)
{
	
		$sql = "INSERT INTO ".TB_PREF."principal_master(name)
		VALUES (" . db_escape($name) . ")";
        
   	   db_query($sql,"could not add new pricipal master");
}

//--------------------------------------------------------------------------------------------------

function update_principal($id, $name)
{
    	$sql = "UPDATE ".TB_PREF."principal_master SET name=".db_escape($name)
    	." WHERE principal_id=".db_escape($id);
    	db_query($sql, "could not update principal master");
}
//--------------------------------------------------------------------------------------------------

function delete_principal($id)
{
	$sql = "DELETE FROM ".TB_PREF."principal_master WHERE principal_id=".db_escape($id);

	db_query($sql, "could not delete principal master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_principal($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."principal_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get principal master");
}

function get_principal($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."principal_master WHERE principal_id=".db_escape($selected_id);
	$result = db_query($sql,"principal master could not be retrieved");
	return db_fetch($result);
}

function check_principal($name)
{
	 $sql = "SELECT * FROM  ".TB_PREF."principal_master WHERE name= ".db_escape($name)."  "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}
/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/



?>