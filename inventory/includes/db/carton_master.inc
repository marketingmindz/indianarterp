<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_carton_master($carton_name, $carton_description, $carton_reference)
{
	
		$sql = "INSERT INTO ".TB_PREF."carton_master(carton_name, carton_description, carton_reference)
		VALUES (" . db_escape($carton_name) . ", " .
		db_escape($carton_description). ", " . db_escape($carton_reference) . ")";
		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_carton_master($id, $carton_name, $carton_description, $carton_reference)
{
    	$sql = "UPDATE ".TB_PREF."carton_master SET carton_name=".db_escape($carton_name)
    	.",carton_description=".db_escape($carton_description)
    	.",carton_reference=".db_escape($carton_reference)
    	."WHERE carton_id=".db_escape($id);
    	db_query($sql, "could not update carton master");
}
//--------------------------------------------------------------------------------------------------

function delete_carton_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."carton_master WHERE carton_id=".db_escape($id);

	db_query($sql, "could not delete carton master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_carton_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."carton_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get carton master");
}

function get_carton_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."carton_master WHERE carton_id=".db_escape($selected_id);
	$result = db_query($sql,"master could not be retrieved");
	return db_fetch($result);
}

function check_carton($carton_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m , '.TB_PREF.'carton_master c, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($carton_reference).'
	 OR cat.cat_reference = '.db_escape($carton_reference).' OR m.master_reference = '.db_escape($carton_reference).' OR c.carton_reference = '.db_escape($carton_reference).' OR w.wood_reference = '.db_escape($carton_reference).' OR s.skin_reference = '.db_escape($carton_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_carton($carton_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat, '.TB_PREF.'master_creation m, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($carton_reference).' 
	 	OR cat.cat_reference = '.db_escape($carton_reference).' OR m.master_reference = '.db_escape($carton_reference).' OR w.wood_reference = '.db_escape($carton_reference).' OR s.skin_reference = '.db_escape($carton_reference).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}




?>