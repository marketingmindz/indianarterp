<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_wood_master($wood_name, $wood_reference, $wood_image, $wood_description)
{
	
		$sql = "INSERT INTO ".TB_PREF."wood_master(wood_name, wood_reference, wood_image, wood_description )
		VALUES (" . db_escape($wood_name) . ", " .
		db_escape($wood_reference). "," .db_escape($wood_image). ", " . db_escape($wood_description) . ")";
		db_query($sql,"could not add wood master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_wood_master($id, $wood_name, $wood_reference, $wood_image, $wood_description)
{
    	$sql = "UPDATE ".TB_PREF."wood_master SET wood_name=".db_escape($wood_name)
    	.",wood_reference=".db_escape($wood_reference)
    	
		.",wood_description=".db_escape($wood_description)
    	."WHERE wood_id=".db_escape($id);
    	db_query($sql, "could not update wood master");
}
//--------------------------------------------------------------------------------------------------

function delete_wood_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."wood_master WHERE wood_id=".db_escape($id);

	db_query($sql, "could not delete color master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_wood_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."wood_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get wood master");
}

function get_wood_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."wood_master WHERE wood_id=".db_escape($selected_id);
	$result = db_query($sql,"wood master could not be retrieved");
	return db_fetch($result);
}

function check_wood($wood_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m , '.TB_PREF.'color_master c, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($wood_reference).'
	 OR cat.cat_reference = '.db_escape($wood_reference).' OR m.master_reference = '.db_escape($wood_reference).' OR c.color_reference = '.db_escape($wood_reference).' OR w.wood_reference = '.db_escape($wood_reference).' OR s.skin_reference = '.db_escape($wood_reference).'  ';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_wood($wood_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m , '.TB_PREF.'color_master c,'.TB_PREF.'skin_master s WHERE t.reference = '.db_escape($wood_reference).'
	 OR cat.cat_reference = '.db_escape($wood_reference).' OR m.master_reference = '.db_escape($wood_reference).' OR c.color_reference = '.db_escape($wood_reference).' OR s.skin_reference = '.db_escape($wood_reference).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}
//--------- get wood image -------------
function update_wood_image($select_id,$wood_reference){
	
	$sql = "UPDATE ".TB_PREF."wood_master SET wood_image=".db_escape($wood_reference)
    	."WHERE wood_id=".db_escape($select_id);
    	db_query($sql, "could not update wood image");
}



?>