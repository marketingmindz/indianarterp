<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_team($pro_team_name, $pro_team_description)
{
	
		$sql = "INSERT INTO ".TB_PREF."production_team(pro_team_name, pro_team_description)
		VALUES (" . db_escape($pro_team_name) . ", " .
		db_escape($pro_team_description). ")";		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_team($id, $pro_team_name, $pro_team_description)
{
    	$sql = "UPDATE ".TB_PREF."production_team SET pro_team_name=".db_escape($pro_team_name)
    	.",pro_team_description=".db_escape($pro_team_description)
    	."WHERE pro_team_id=".db_escape($id);
    	db_query($sql, "could not update master");
}
//--------------------------------------------------------------------------------------------------

function delete_team($id)
{
	 $sql = "DELETE FROM ".TB_PREF."production_team WHERE pro_team_id=".db_escape($id);

	db_query($sql, "could not delete prodcution team");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_team($all=false) {
  $sql = "SELECT * FROM ".TB_PREF."production_team";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get prodcution");
}

function get_team($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."production_team WHERE pro_team_id=".db_escape($selected_id);
	$result = db_query($sql,"prodcution could not be retrieved");
	return db_fetch($result);
}


?>