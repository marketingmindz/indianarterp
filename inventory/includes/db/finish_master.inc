<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_finish_master($finish_name, $finish_reference, $finish_description, $finish_image)
{
	
		$sql = "INSERT INTO ".TB_PREF."finish_master(finish_name, finish_reference, finish_description, finish_image )
		VALUES (" . db_escape($finish_name) . ", " .db_escape($finish_reference). "," .db_escape($finish_description). ", " . db_escape($finish_image) . ")";
		db_query($sql,"could not add finish master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_finish_master($id, $finish_name, $finish_reference, $finish_description, $finish_image)
{
    	$sql = "UPDATE ".TB_PREF."finish_master SET finish_name=".db_escape($finish_name)
    	.",finish_reference=".db_escape($finish_reference)
    	.",finish_description=".db_escape($finish_description)
    	."WHERE finish_id=".db_escape($id);
    	db_query($sql, "could not update finish master");
}
//--------------------------------------------------------------------------------------------------

function delete_finish_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."finish_master WHERE finish_id=".db_escape($id);

	db_query($sql, "could not delete finish master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_finish_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."finish_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get finish master");
}

function get_finish_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."finish_master WHERE finish_id=".db_escape($selected_id);
	$result = db_query($sql,"finish master could not be retrieved");
	return db_fetch($result);
}

function check_finish($finish_reference)
{
	 $sql = 'SELECT t.reference, cat.cat_reference, m.master_reference, c.color_reference, w.wood_reference FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m , '.TB_PREF.'color_master c, '.TB_PREF.'wood_master w, '.TB_PREF.'skin_master s, '.TB_PREF.'finish_master f WHERE t.reference = '.db_escape($finish_reference).'
	 OR cat.cat_reference = '.db_escape($finish_reference).' OR m.master_reference = '.db_escape($finish_reference).' OR c.color_reference = '.db_escape($finish_reference).' OR w.wood_reference = '.db_escape($finish_reference).' OR s.skin_reference = '.db_escape($finish_reference).' OR f.finish_reference = '.db_escape($finish_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_finish($finish_reference)
{
	 $sql = 'SELECT t.reference, cat.cat_reference, m.master_reference, c.color_reference FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat, '.TB_PREF.'master_creation m, '.TB_PREF.'color_master c, '.TB_PREF.'skin_master s, '.TB_PREF.'wood_master w WHERE t.reference = '.db_escape($finish_reference).' 
	 	OR cat.cat_reference = '.db_escape($finish_reference).' OR m.master_reference = '.db_escape($finish_reference).' OR c.color_reference = '.db_escape($finish_reference).' OR s.skin_reference = '.db_escape($finish_reference).' OR w.wood_reference = '.db_escape($finish_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}

function update_finish_image($select_id, $finish_image){
		$sql = "UPDATE ".TB_PREF."finish_master SET finish_image=".db_escape($finish_image)
    	."WHERE finish_id=".db_escape($select_id);
    	db_query($sql, "could not update finish image");
}



?>