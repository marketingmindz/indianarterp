<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// - -  ----------------    Code by bajrang   - get opening stock  ------- - - - - -

function get_all_opening_stock_master() {
  $sql = "SELECT * FROM ".TB_PREF."opening_stock_master";
    $result =   db_query($sql, "could not get opening stock Master");
	return $result;
}

function check_consumable_stock($stock_type_id, $consumble_id, $consumble_category, $location_id, $work_center_id)
{
	$check_consumable = "select stock_type_id,consumable_id,consumable_category from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && consumable_id = '$consumble_id' && consumable_category = '$consumble_category' && location_id = '$location_id' && work_center_id = '$work_center_id' ";
	$result = db_query($check_consumable,'check_consumable_stock stock check error');
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_design_stock($stock_type_id, $pro_design_id, $location_id, $work_center_id)
{
	$check_design = "select stock_type_id,pro_design_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_design_id = '$pro_design_id'  && location_id = '$location_id' && work_center_id = '$work_center_id'";
	$result = db_query($check_design);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_finish_stock($stock_type_id,$pro_design_id, $pro_finish_id, $location_id, $work_center_id)
{
	$check_finish = "select stock_type_id,pro_finish_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_finish_id = '$pro_finish_id' && pro_design_id = '$pro_design_id'  && location_id = '$location_id' && work_center_id = '$work_center_id'";
	$result = db_query($check_finish);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}



function add_open_stock_category($stock_type_id, $location_id, $work_center_id, $consumble_id, $consumble_category, $pro_cat_id, $pro_range_id,$pro_design_id,$finish_code_id, $unit, $level, $stock_quantity,$date)
{
	$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id, location_id, work_center_id,consumable_id,consumable_category,pro_cat_id,pro_range_id,pro_design_id,pro_finish_id,unit,level,stock_qty,date)
		VALUES (" .db_escape($stock_type_id).","
		.db_escape($location_id).","
		.db_escape($work_center_id).","
		.db_escape($consumble_id).","
		.db_escape($consumble_category).","
		.db_escape($pro_cat_id).","
		.db_escape($pro_range_id).","
		.db_escape($pro_design_id).","
		.db_escape($finish_code_id).","
		.db_escape($unit).","
		.db_escape($level).","
		.db_escape($stock_quantity).","
		.db_escape($date).")";
        
   	   db_query($sql,"could not add new stock");
	
}


function update_open_stock_category($selected_id,$stock_type_id, $location_id, $work_center_id, $consumble_id, $consumble_category, $pro_cat_id, $pro_range_id,$pro_design_id,$finish_code_id,$unit,$level,$stock_quantity,$date){
	$sql = "UPDATE ".TB_PREF."opening_stock_master SET stock_type_id=".db_escape($stock_type_id)
	  .",location_id=".db_escape($location_id)
	  .",work_center_id=".db_escape($work_center_id)
	  .",consumable_id=".db_escape($consumble_id)
	  .",consumable_category=".db_escape($consumble_category)
	  .",pro_cat_id=".db_escape($pro_cat_id)
	  .",pro_range_id=".db_escape($pro_range_id)
	  .",pro_design_id=".db_escape($pro_design_id)
	  .",pro_finish_id=".db_escape($finish_code_id)
	  .",unit=".db_escape($unit)
	  .",level=".db_escape($level)
	  .",stock_qty=".db_escape($stock_quantity)
	  .",date=".db_escape($date)
    	."WHERE open_stock_id=".db_escape($selected_id);
    	db_query($sql, "could not update opening stock master");
	
}

function delete_opening_stock($selected_id)
{
	$sql = "DELETE FROM ".TB_PREF."opening_stock_master WHERE open_stock_id=".db_escape($selected_id);
	db_query($sql, "could not delete opening stock master");
}

function get_available_quntity($sub_id){
	$sql="SELECT sum(quantity) FROM ".TB_PREF."opening_master_stock WHERE sub_master_id=".db_escape($sub_id)." AND type = 'cr'";
	$total_cr = db_query($sql,"could not add new stock");
	echo $total_cr;
	return $total_cr;
}




function get_stock_type($stock_type_id) {
  $sql = "SELECT stock_name FROM ".TB_PREF."stock_type where stock_type_id = ".db_escape($stock_type_id);
    $result =  db_query($sql, "could not get stock type");
	return db_fetch($result);
}


function get_consumable_name($consumable_id)
{
	$sql = "SELECT master_id, master_name, inactive, master_reference FROM ".TB_PREF."master_creation where master_id= ".db_escape($consumable_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);	
}

function get_finish_product_name($pro_finish_id)
{
	$sql = "SELECT finish_comp_code FROM ".TB_PREF."finish_product where finish_pro_id= ".db_escape($pro_finish_id);
	$result = db_query($sql,"Could not get finish name");
	return db_fetch($result);	
}

function get_design_product_name($pro_design_id)
{
	$sql = "SELECT design_code FROM ".TB_PREF."design_code where design_id= ".db_escape($pro_design_id);
	$result = db_query($sql,"Could not get design code");
	return db_fetch($result);	
}

function get_opening_stock_master($stock_id){
	$sql = "SELECT * FROM ".TB_PREF."opening_stock_master where open_stock_id= ".db_escape($stock_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);
}



function get_opening_stock_search($all_items)
{
	global $order_number;
	 $sql ='Select op.open_stock_id,l.location_name,w.work_center_name, 
	 m.master_name,
	 c.consumable_name,
	op.unit, op.level, op.stock_qty from '.TB_PREF.'opening_stock_master op 
	 
	 Left Join '.TB_PREF.'master_creation m on op.consumable_id = m.master_id
	 Left Join '.TB_PREF.'consumable_master c on op.consumable_category = c.consumable_id 

	 Left Join '.TB_PREF.'location_master l on op.location_id = l.location_id 
	 Left Join '.TB_PREF.'work_center w on op.work_center_id = w.work_center_id where  "1" = "1" ';	 
	 if(isset($_POST['location_id']) && $_POST['location_id'] != '-1')
	 {
		 $sql .= " AND op.location_id = ".db_escape($_POST['location_id']) ;
	 }
	 if(isset($_POST['work_center_id']) && $_POST['work_center_id'] != '-1')
	 {
		 $sql .= " AND op.work_center_id = ".db_escape($_POST['work_center_id']) ;
	 }
	 if(isset($_POST['cons_type']) && $_POST['cons_type'] != '-1')
	 {
		 $sql .= " AND op.consumable_id = ".db_escape($_POST['cons_type'])  ;
	 }
	 if(isset($_POST['cons_select']) && $_POST['cons_select'] != '-1' && $_POST['cons_select'] != '0')
	 {
		 $sql .= " AND op.consumable_category = ".db_escape($_POST['cons_select']);
	 }
	  $sql .=' order by op.open_stock_id DESC';
	return $sql;
}



?>