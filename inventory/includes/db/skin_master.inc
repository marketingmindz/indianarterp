<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_skin_master($skin_name, $skin_reference, $skin_description)
{
	
		$sql = "INSERT INTO ".TB_PREF."skin_master(skin_name, skin_reference, skin_description)
		VALUES (" . db_escape($skin_name) . ", " .
		db_escape($skin_reference). ", " . db_escape($skin_description) . ")";
		
        
   	   db_query($sql,"could not add skin master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_skin_master($id, $skin_name, $skin_reference, $skin_description)
{
    	$sql = "UPDATE ".TB_PREF."skin_master SET skin_name=".db_escape($skin_name)
    	.",skin_reference=".db_escape($skin_reference)
    	.",skin_description=".db_escape($skin_description)
    	."WHERE skin_id=".db_escape($id);
    	db_query($sql, "could not update skin master");
}
//--------------------------------------------------------------------------------------------------

function delete_skin_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."skin_master WHERE skin_id=".db_escape($id);

	db_query($sql, "could not delete skin master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_skin_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."skin_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get skin master");
}

function get_skin_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."skin_master WHERE skin_id=".db_escape($selected_id);
	$result = db_query($sql,"master could not be retrieved");
	return db_fetch($result);
}


function check_skin($skin_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m, '.TB_PREF.'color_master col, '.TB_PREF.'skin_master sk, '.TB_PREF.'wood_master wd WHERE cat.cat_reference = '.db_escape($skin_reference).'
	 OR m.master_reference = '.db_escape($skin_reference).' OR t.reference = '.db_escape($skin_reference).' OR col.color_reference = '.db_escape($skin_reference).' OR sk.skin_reference = '.db_escape($skin_reference).' OR wd.wood_reference = '.db_escape($skin_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_skin($skin_reference)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m, '.TB_PREF.'color_master col, '.TB_PREF.'wood_master wd WHERE cat.cat_reference = '.db_escape($skin_reference).'
	 OR m.master_reference = '.db_escape($skin_reference).' OR t.reference = '.db_escape($skin_reference).' OR col.color_reference = '.db_escape($skin_reference).' OR wd.wood_reference = '.db_escape($skin_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
?>