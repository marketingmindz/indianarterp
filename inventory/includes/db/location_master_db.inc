<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_location_master($location_code, $location_name, $work_center_id, $location_address, $telephone_no, $sec_telephone_no, $facsimeile_no, $email_id)
{
	
	$sql = "INSERT INTO ".TB_PREF."location_master(location_code, location_name, work_center_id,location_address, telephone_no, sec_telephone_no, facsimeile_no, email_id)
		VALUES (" . db_escape($location_code) . ", " . db_escape($location_name) . ", ". db_escape($work_center_id) . ", " .  db_escape($location_address) . ", " .db_escape($telephone_no) . ", " .db_escape($sec_telephone_no) . ", " . db_escape($facsimeile_no) . ", " . 
		db_escape($email_id). ")";		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_location_master($id, $location_code, $location_name, $work_center_id, $location_address, $telephone_no, $sec_telephone_no, $facsimeile_no, $email_id)
{
      $sql = "UPDATE ".TB_PREF."location_master SET location_code=".db_escape($location_code)
	  .",location_name=".db_escape($location_name)
	  .",work_center_id=".db_escape($work_center_id)
	  .",location_address=".db_escape($location_address)
	  .",telephone_no=".db_escape($telephone_no)
	  .",sec_telephone_no=".db_escape($sec_telephone_no)
	  .",facsimeile_no=".db_escape($facsimeile_no)
      .",email_id=".db_escape($email_id)
      ."WHERE location_id=".db_escape($id);
    	db_query($sql, "could not update master");
}
//--------------------------------------------------------------------------------------------------

function delete_location_master($id)
{
	
	 $sql = "DELETE FROM ".TB_PREF."location_master WHERE location_id=".db_escape($id);

	db_query($sql, "could not delete prodcution team");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_location_master($all=false) {
  $sql = "SELECT * FROM ".TB_PREF."location_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get center");
}

function get_location_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."location_master WHERE location_id=".db_escape($selected_id);
	$result = db_query($sql,"prodcution could not be retrieved");
	return db_fetch($result);
}




function get_team_name($work_id)
{
	$sql="SELECT work_center_name FROM ".TB_PREF."work_center WHERE work_center_id = ".db_escape($work_id);
	$result = db_query($sql,"work center name could not be retrieved");
	$work_center_res =  db_fetch($result);
	return $work_center_res['work_center_name'];
}
function get_work_id($work_id){
	$work_id = explode(",",$work_id);
	$count = sizeof($work_id);
	$work_name = "";
	for($i = 0; $i<$count; $i++)
	{		
		$work_name .= get_team_name($work_id[$i]).",";
	}
	$work_center_name = rtrim($work_name,',');
	return $work_center_name;
}
/*function check_master($work_center_name, $master_reference)
{
	 $sql = "SELECT * FROM  ".TB_PREF."work_center WHERE work_center_name= ".db_escape($work_center_name)." OR master_reference=".db_escape($master_reference)." "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}
*/

/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/

?>