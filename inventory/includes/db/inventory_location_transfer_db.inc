<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//----------------------------------------------------------------------------------------

function get_reference_no()
{
	$sql = "SELECT reference_no FROM ".TB_PREF."item_location_transfer_master ORDER BY reference_no DESC LIMIT 1 ";
    $query1 = db_query($sql,"Could not get the reference no.");
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $reference_no= $row['reference_no'];
	   $reference_no++;
	}
	if($reference_no == '' && $reference_no == NULL)
	{
		$reference_no = 1;
	}	
	return $reference_no;
}


function consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category)
{
	$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$available_stock_qty = $stock['stock_qty'];
		return $available_stock_qty;
}



function get_cons_select_name($cons_select){
	$sql = "SELECT c.consumable_name, c.consumable_code, i.company_name FROM ".TB_PREF."consumable_master c
			Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
			 WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_category_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."item_category WHERE category_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_range_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."item_range WHERE id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_design_code($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_fihish_code($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function check_product_opening_stock($location_id, $work_center_id, $design_id, $finish_code_id, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			display_error(_("Insufficient stock!!."));
			return false;
		}		

		if($stock_qty == NULL && $stock_qty == '')
		{
			display_error(_("Stock is not available for this item"));
			die;
		}
		else
		{
			if($stock_qty < $quantity)
			{
				display_error(_("Insufficient stock in opening stock. Opening stock quantity is ".$stock_qty));
			die;
			}
			return true;
		}
	
}

function update_from_product_opening_stock($location_id, $work_center_id, $design_id, $finish_code_id, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		$stock_qty = $stock_qty-$quantity;
		$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		db_query($sql, "cannot update opening stock");
		return true;
}


function update_product_opening_stock($location_id, $work_center_id,$pro_cat_id, $pro_range_id, $design_id, $finish_code_id, $quantity, $date)
{
	$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id, location_id, work_center_id, consumable_id, consumable_category, pro_cat_id, pro_range_id,pro_design_id,pro_finish_id,stock_qty,date)
		VALUES ("."101".","
		.db_escape($location_id).","
		.db_escape($work_center_id).",'-1','-1',"
		.db_escape($pro_cat_id).","
		.db_escape($pro_range_id).","
		.db_escape($design_id).","
		.db_escape($finish_code_id).","
		.db_escape($quantity).","
		.db_escape($date).")";
		
	   db_query($sql,"could not add new stock");
		}		
		else
		{
			$stock_qty = $stock_qty+$quantity;
			$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where pro_design_id = " . db_escape($design_id)." AND pro_finish_id = ".db_escape($finish_code_id)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id)." && date = ".db_escape($date);
			db_query($sql, "cannot update opening stock");
			return true;
		}
}



function check_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity)
{
		$sql = "SELECT stock_qty from  ".TB_PREF."opening_stock_master where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		
		$result = db_query($sql,"stock cannot be retrived - #ioioio");
		$stock = db_fetch($result);
		$stock_qty = $stock['stock_qty'];
		if (db_num_rows($result) < 0)	
		{
			display_error(_("Insufficient stock!!."));
			return false;
		}		
		if($stock_qty == NULL && $stock_qty == '')
		{
			display_error(_($sql."Stock is not available for this consumable item - ERROR ##!!!"));
			die;
		}
		else
		{
			if($stock_qty < $quantity)
			{
				display_error(_("Insufficient stock in opening stock,. Opening stock quantity is ".$stock_qty));
			die;
			}
			return true;
		}
}


function generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity, $unit, $date)
{
	$pr_no = '';
	$sql = "SELECT pr_no FROM ".TB_PREF."purchase_request ORDER BY pr_no DESC LIMIT 1";
	$query1 = db_query($sql,"cannot get purchase request no.");
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $pr_no= $row['pr_no'];
	   $pr_no++;
	}
	if($pr_no == '' && $pr_no == NULL)
	{
		$pr_no = 1;
	}
	$sql = "INSERT INTO ".TB_PREF."purchase_request(pr_no,location_id,work_center_id,type,date) VALUES(";
	$sql .= db_escape($pr_no) . "," .
	db_escape($location_id) . "," .
	db_escape($work_center_id) . "," ."'1',".
	db_escape($date).")";
	db_query($sql, "Purchase request is not created");

	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
		$sql .= $pr_no . ", " . 
		db_escape($consumable_id).",".
		db_escape($consumable_category) . "," . 		
		db_escape($unit) . ", " .
		db_escape($request_quantity). ")";
		db_query($sql, " purchase request items are not inserted in database can not be made");

}

// Bajrang L. Bidasara --  Assign stock directly to prodcution team --- 
function update_consumable_type_stock($consumable_id,$consumable_category,$quntity,$id,$available_quantity)
{
	$new_quantity = $quntity+$available_quantity;
	$sql = "update ".TB_PREF."item_adjustment_rel set quantity = ".db_escape($new_quantity)." where consumable_id=".db_escape($consumable_id)."&& consumable_category =".db_escape($consumable_category)." && adj_id = ".db_escape($id);
	db_query($sql,"consumbale_stock_updated");
	
}

function adjust_consumable_stock($location_id,$work_center_id,$pro_team_id, $reference_no, $date, $stock_type_id, $type_id, $memo, $consumable_id,$consumable_category,$quantity,$unit)
{
	$adj_date = date2sql($date);
	
	$sql = "INSERT INTO ".TB_PREF."item_adjustment_master(location_id, work_center_id, pro_team_id, reference_no,	date, stock_type_id, adj_type,memo) VALUES(";
		 $sql .= db_escape($location_id) . "," .
			 db_escape($work_center_id) . "," .
			 db_escape($pro_team_id) . ", " .
			 db_escape($reference_no) . ", " .
			 db_escape($adj_date) . ", " .
			 db_escape($stock_type_id) . ", " .
			 db_escape($type_id) . ", " .
			 db_escape($memo) . ")";
	
			db_query($sql, "Adjustment could not be made");
			
			/*Get the auto increment value of the order number created from the sql above */
     $design_id = db_insert_id();
		
	$sql = "INSERT INTO ".TB_PREF."item_adjustment_rel (adj_id, consumable_id, consumable_category, category_id, range_id, design_id, pro_finish_code_id, unit, quantity) VALUES (";
			$sql .= $design_id . ", " . db_escape($consumable_id).",".
			db_escape($consumable_category) . "," . 
			"'-1','-1','-1','-1',".
			
			db_escape($unit) . ", " .
			db_escape($quantity). ")";
			
			db_query($sql, "adjustment relation can not be made");
			$consumable_type_obj->lines_on_con[$line_no]->cons_detail_rec = db_insert_id();	
}

/*Stock Transaction*/
function update_stock_transaction($location_id, $work_center_id, $consumable_category, $consumable_id, $unit, $stock_quantity, $date, $type, $closing_stock, $opening_stock=0)
{
	$dateTime = dateTimeNow();

	$sql = "insert into ".TB_PREF."stock_transaction(date, consumable_category, consumable_id, location_id, work_center_id, pro_team_id, opening_stock, closing_stock, trans_quantity, unit, type) values("
		.db_escape($dateTime).","
		.db_escape($consumable_category).","
		.db_escape($consumable_id).","
		.db_escape($location_id).","
		.db_escape($work_center_id).","
		.db_escape($pro_team_id).","
		.db_escape($opening_stock).","
		.db_escape($closing_stock).","
		.db_escape($stock_quantity).","
		.db_escape($unit).","
		.db_escape($type)
		.");";
	db_query($sql, "Stock Transaction is not updated.");
}


function update_from_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity, $unit, $date)
{
		$available_stock_qty = consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category);
		$stock_qty = $available_stock_qty-$quantity;

		$sql = "update ".TB_PREF."opening_stock_master set stock_qty = ".db_escape($stock_qty)." where consumable_id = " . db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category)." AND location_id = ".db_escape($location_id)." AND work_center_id = ".db_escape($work_center_id);
		db_query($sql, "cannot update opening stock");


		$closing_stock = $stock_qty;
		$opening_stock = $available_stock_qty;
		$txn_quantity = $quantity;
		update_stock_transaction($location_id, $work_center_id, $consumable_id, $consumable_category, $unit, $txn_quantity, $date, "Item Transfer Out", $closing_stock, $opening_stock);



		/*if($available_stock_qty < $level && $stock_qty < $available_stock_qty)
		{
			$request_quantity = $quantity;
			generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity,$unit, $date);
		}
		elseif($stock_qty < $level)
		{
			$request_quantity = $level - $stock_qty;
			generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity,$unit, $date);
		}*/
		return true;
}

function update_consumable_opening_stock($location_id, $work_center_id, $consumable_id, $consumable_category, $quantity, $unit, $date)
{
	$check_consumable = "select stock_qty from ".TB_PREF."opening_stock_master where  
				consumable_id = ".db_escape($consumable_id).
				" && consumable_category = ".db_escape($consumable_category).
				" && location_id = ".db_escape($location_id).
				" && work_center_id = ".db_escape($work_center_id);
	$result = db_query($check_consumable,'check_consumable_stock stock check error');
	if(db_num_rows($result) < 1 )
	{
		$available_stock_qty = 0;
		$stock_quantity = $quantity;

		$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id, location_id, work_center_id, consumable_id, consumable_category, pro_cat_id, pro_range_id,pro_design_id,pro_finish_id,stock_qty,unit,date)
		VALUES ("."101".","
		.db_escape($location_id).","
		.db_escape($work_center_id).","
		.db_escape($consumable_id).","
		.db_escape($consumable_category).",'-1','-1','-1','-1',"
		.db_escape($quantity).","
		.db_escape($unit).","
		.db_escape($date).")";
		
	   db_query($sql,"could not add new stock");
	}
	else
	{
		while ($myrow = db_fetch($result))
		{
			$stock_quantity = $myrow['stock_qty'];
		}
		$available_stock_qty = $stock_quantity;
		$stock_quantity = $stock_quantity+$quantity;
		$sql = "UPDATE ".TB_PREF."opening_stock_master SET stock_qty=".db_escape($stock_quantity). ", date = ".db_escape($date)
			  ." where location_id=".db_escape($location_id)
			  ." && work_center_id=".db_escape($work_center_id)
			  ." && consumable_id = ".db_escape($consumable_id)
			  ." && consumable_category = ".db_escape($consumable_category);
		db_query($sql, "could not update opening stock master");
	 }

	$closing_stock = $stock_quantity;
	$opening_stock = $available_stock_qty;
	$txn_quantity = $quantity;
	update_stock_transaction($location_id, $work_center_id, $consumable_id, $consumable_category, $unit, $txn_quantity, $date, "Item Transfer In", $closing_stock, $opening_stock);

}




function add_inventory(&$header_design,&$pack_sec,&$product_sec)
{
	global $Refs;

	begin_transaction();
	//hook_db_prewrite($dc_obj, ST_DESIGN);
		
	 if($header_design['stock_type_id'] == 101)
	 {
		 foreach ($pack_sec->line_con as $line_con => $con_line)
         {
			check_consumable_opening_stock($header_design['from_Location_id'], $header_design['from_work_center_id'], $con_line->cons_type, $con_line->cons_select, $con_line->cons_quntity ); 
		 }
	 }
	 if($header_design['stock_type_id'] == 102)
	 {
		 foreach ($product_sec->line_items as $line_no => $asb_line)
		 {
			check_product_opening_stock($header_design['from_Location_id'], $header_design['from_work_center_id'], $asb_line->design_id, $asb_line->finish_code_id, $asb_line->pro_qty); 
		 }
	 }	
		
	$sql = "INSERT INTO ".TB_PREF."item_location_transfer_master(from_loc_id, from_work_center_id,to_loc_id,to_work_center_id, 
	reference_no,date,stock_type_id,type, memo) VALUES(";
    $sql.= db_escape($header_design['from_Location_id']) . "," .
	 db_escape($header_design['from_work_center_id']) . "," .
	 db_escape($header_design['to_Location_id']) . ", " .
	 db_escape($header_design['to_work_center_id']) . ", " .
	 db_escape($header_design['reference_no']) . ", " .
	 db_escape($header_design['date']) . ", " .
	 db_escape($header_design['stock_type_id']) . ", " .
	 db_escape($header_design['type_id']). ", " .
	 db_escape($header_design['memo_description']) . ")";

	db_query($sql, "The design header record could not be inserted");
  /*Get the auto increment value of the order number created from the sql above */
     $loc_transfer_id = db_insert_id();
     
	 if($header_design['stock_type_id'] == 101)
	 {
	  // consumable  
		 foreach ($pack_sec->line_con as $line_con => $con_line)
		 {
			$sql = "INSERT INTO ".TB_PREF."item_location_transfer_relation(loc_transfer_id,consumable_name_id,consumable_category_id,product_category_id, product_range_id, product_design_id, product_finish_id, con_unit,con_quantity) VALUES (";
			$sql.= $loc_transfer_id . ", " . db_escape($con_line->cons_type). "," .
			db_escape($con_line->cons_select). "," .
			"'-1','-1','-1','-1',".
			db_escape($con_line->cons_unit). "," .
			db_escape($con_line->cons_quntity). ")";
			db_query($sql, "One of the consumable detail records could not be inserted");
			$pack_sec->line_con[$line_no]->fcon_detail_rec = db_insert_id();

			update_from_consumable_opening_stock($header_design['from_Location_id'], $header_design['from_work_center_id'], $con_line->cons_type, $con_line->cons_select, $con_line->cons_quntity, $con_line->cons_unit,$header_design['date']  );

			update_consumable_opening_stock($header_design['to_Location_id'], $header_design['to_work_center_id'], $con_line->cons_type, $con_line->cons_select, $con_line->cons_quntity,$con_line->cons_unit, $header_design['date'] );

			if(isset($_POST['directToPT']) && $_POST['directToPT'] == "1")
			{
				update_from_consumable_opening_stock($header_design['to_Location_id'], $header_design['to_work_center_id'], $con_line->cons_type, $con_line->cons_select, $con_line->cons_quntity,$con_line->cons_unit,$header_design['date']);

				$sql = "SELECT adj_rel.adj_id, adj_rel.quantity from  ".TB_PREF."item_adjustment_master adj left join ".TB_PREF."item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id where adj_rel.consumable_id = " . db_escape($con_line->cons_type)." AND adj_rel.consumable_category = ".db_escape($con_line->cons_select) . " AND adj.location_id = ". db_escape($header_design['to_Location_id']) ." AND adj.work_center_id = " .db_escape($header_design['to_work_center_id']) . " AND adj.pro_team_id = " .db_escape($_POST['production_team']);
			
				$result = db_query($sql,"adjustment cannot be processed - #XXX");
				$adj_id = db_fetch($result);
				$id = $adj_id['adj_id']; 
				$available_quantity = $adj_id['quantity'];
				if($id != NULL && $id != '')
				{
					update_consumable_type_stock($con_line->cons_type,$con_line->cons_select,$con_line->cons_quntity,$id,$available_quantity);
				}
				else
				{
					adjust_consumable_stock($header_design['to_Location_id'], $header_design['to_work_center_id'], $_POST['production_team'], $header_design['reference_no'], $header_design['date'], $header_design['stock_type_id'], $header_design['type_id'], $header_design['memo_description'], $con_line->cons_type,$con_line->cons_select, $con_line->cons_quntity, $con_line->cons_unit);
				}
			}
	 
		 }
	 }
	 
	 if($header_design['stock_type_id'] == 102)
	 {
	  // product ;  
		 foreach ($product_sec->line_items as $line_no => $asb_line)
		 {
			$sql = "INSERT INTO ".TB_PREF."item_location_transfer_relation(loc_transfer_id, consumable_name_id,consumable_category_id, product_category_id, product_range_id,product_design_id,product_finish_id,con_unit,con_quantity) VALUES (";
			$sql .= $loc_transfer_id . ", " .
			"'-1','-1',".
			db_escape($asb_line->category_id). "," .
			db_escape($asb_line->range_id) . "," .
			db_escape($asb_line->design_id) . "," .
			db_escape($asb_line->finish_code_id) . "," .
			db_escape($asb_line->pro_unit) . "," .
			db_escape($asb_line->pro_qty). ")";
			db_query($sql, "One of the product detail records could not be inserted");
			$product_sec->line_items[$line_no]->pro_detail_rec = db_insert_id();
			
			update_from_product_opening_stock($header_design['from_Location_id'], $header_design['from_work_center_id'], $asb_line->design_id, $asb_line->finish_code_id, $asb_line->pro_qty); 

			update_product_opening_stock($header_design['to_Location_id'], $header_design['to_work_center_id'], $asb_line->category_id, $asb_line->range_id,  $asb_line->design_id, $asb_line->finish_code_id, $asb_line->pro_qty, $header_design['date'] );
		 }
	 }
	 
	 
	 
	commit_transaction();

	return $loc_transfer_id;
}





?>