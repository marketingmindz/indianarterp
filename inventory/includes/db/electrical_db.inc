<?php session_start();
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
function get_electrical_search($all_items)
{

	
	global $order_number, $master_name;
	$m_name = $_SESSION['master_name_search']; 
	
	$sql = 'SELECT cm.consumable_code ,'.
			'cm.consumable_name,c.company_name as company_name, '.
			'm.master_name as master_name, '.
			'pm.name as principal_name, '.
			'cm.unit_id, cm.weight, '.
			'cm.size, cm.cbm, cm.gsm, '.
			'cm.document, cm.consumable_image '. 
			'FROM '.TB_PREF.'consumable_master cm
			LEFT JOIN '.TB_PREF.'master_creation m ON  cm.master_id=m.master_id 
			LEFT JOIN '.TB_PREF.'item_company c ON  cm.company_id = c.company_id
			LEFT JOIN '.TB_PREF.'principal_master pm ON cm.principal_id = pm.principal_id'.
			' WHERE cm.master_id = '.db_escape($m_name);
		
	//.db_escape($master_name);
		
		
	
	if (isset($order_number) && $order_number != "")
	{
		$sql .= " AND cm.consumable_code LIKE ".db_escape('%'. $order_number . '%');
	}
	if(isset($_POST['consumable_name']) && $_POST['consumable_name'] != "")
	{
		$sql .= " AND cm.consumable_name LIKE ".db_escape('%'. $_POST['consumable_name'] . '%');
	}
	$sql .= " ORDER by consumable_code";
	
	//return db_query($sql, "could not get consumable");
	/*$sql = "SELECT 
		porder.order_no, 
		porder.reference, 
		supplier.supp_name, 
		location.location_name,
		porder.requisition_no, 
		porder.ord_date, 
		supplier.curr_code, 
		Sum(line.unit_price*line.quantity_ordered) AS OrderValue,
		porder.into_stock_location
		FROM ".TB_PREF."purch_orders as porder, "
			.TB_PREF."purch_order_details as line, "
			.TB_PREF."suppliers as supplier, "
			.TB_PREF."locations as location
		WHERE porder.order_no = line.order_no
		AND porder.supplier_id = supplier.supplier_id
		AND location.loc_code = porder.into_stock_location ";*/

	/*if (isset($_GET['supplier_id']))
		$sql .= "AND supplier.supplier_id=".@$_GET['supplier_id']." ";*/
	/*if (isset($order_number) && $order_number != "")
	{
		$sql .= "AND porder.reference LIKE ".db_escape('%'. $order_number . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " AND porder.ord_date >= '$data_after'";
		$sql .= " AND porder.ord_date <= '$date_before'";

		if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
		{
			$sql .= " AND porder.into_stock_location = ".db_escape($_POST['StockLocation']);
		}
		if (isset($selected_stock_item))
		{
			$sql .= " AND line.item_code=".db_escape($selected_stock_item);
		}
		if ($supplier_id != ALL_TEXT)
			$sql .= " AND supplier.supplier_id=".db_escape($supplier_id);
		
	} //end not order number selected*/

	
	return $sql;
}
function get_electric_master_row($id)
{
	$sql="SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_code=".db_escape($id);

	$result = db_query($sql,"an consumable could not be retrieved");

	return db_fetch($result);
}
function get_category_master_row($consum_name)
{
	$sql="SELECT * FROM ".TB_PREF."master_creation WHERE master_name=".db_escape($consum_name);

	$result = db_query($sql);

	return db_fetch($result);
}

function delete_cosumable_category_master($id)
{
	$sql="DELETE FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($id);

	db_query($sql,"an consumable could not be deleted");
}

?>