<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_leg_type($leg_name)
{
	
		$sql = "INSERT INTO ".TB_PREF."leg_master(leg_name)
		VALUES (" . db_escape($leg_name) . ")";
        
   	   db_query($sql,"could not add new leg type");
}

//--------------------------------------------------------------------------------------------------

function update_leg_type($id, $leg_name)
{
    	$sql = "UPDATE ".TB_PREF."leg_master SET leg_name=".db_escape($leg_name)
    	." WHERE leg_id=".db_escape($id);
    	db_query($sql, "could not update leg type");
}
//--------------------------------------------------------------------------------------------------

function delete_leg_type($id)
{
	$sql = "DELETE FROM ".TB_PREF."leg_master WHERE leg_id=".db_escape($id);

	db_query($sql, "could not delete leg type");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_leg_type($all=false) {	
    $sql = "SELECT * FROM ".TB_PREF."leg_master";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get leg type");
}

function get_leg_type($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."leg_master WHERE leg_id=".db_escape($selected_id);
	$result = db_query($sql,"leg type could not be retrieved");
	return db_fetch($result);
}

function check_leg_type($leg_name)
{
	 $sql = "SELECT * FROM  ".TB_PREF."leg_master WHERE leg_name= ".db_escape($leg_name)."  "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}


?>