<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//----------------------------------------------------------------------------------------


function get_custom_product_crm_id()
{
	$sql = "select crm_id from ".TB_PREF."finish_product where category_id = '84'";
	$result = db_query($sql, "could not get Product CRM Id");
	$crm_id = Array();

	while($row = db_fetch($result))
	{
		$crm_id[] = $row['crm_id'];
	}
	return $crm_id;
}


function read_finish_product($finish_code)
{
	$sql = "select * from ".TB_PREF."finish_product where finish_comp_code = ".db_escape('ITNIR001-MPPM');
	$result = db_query($sql, "Could not read finish product");
	return db_fetch($result);
}

function insert_custom_product($custom_pro)
{
	$finish = read_finish_product($custom_pro['customproducts_tks_referencepr']);

	$sql = "INSERT INTO ".TB_PREF."finish_product (category_id, crm_id, range_id, design_id, color_id, 
	leg_id, wood_id, skin_id, fabric_id, finish_id, finish_product_name, finish_comp_code, gross_weight, asb_weight, asb_height, asb_density, description, product_image, collection_image, structure_wood_id, no_sitting ) VALUES(";
	$sql .= db_escape("84") . "," .
		db_escape($custom_pro['id']) . "," .
		db_escape("-1") . "," .
		db_escape($finish['design_id']) . ", " .
		db_escape($finish['color_id']) . ", " .
		db_escape($finish['leg_id']) . ", " .
		db_escape($finish['wood_id']) . ", " .
		db_escape($finish['skin_id']) . ", " .
		db_escape($finish['fabric_id']). ", " .
		db_escape($finish['finish_id']). ", " .
		db_escape($finish['finish_product_name']). ", " .
		db_escape($custom_pro['customproducts_tks_temporaryco']). ", " .
		db_escape($finish['gross_weight']). ", " .
		db_escape($finish['asb_weight']). ", " .
		db_escape($finish['asb_height']). ", " .
		db_escape($finish['asb_density']). ", " .
		db_escape($finish['description']). ", " .
		db_escape($finish['product_image']). ", " .
		db_escape($finish['collection_image']). ", " .
		db_escape($custom_pro['cf_942']). ", " .
		db_escape($finish['no_setting']) . ")";
	db_query($sql, "Custom Product could not be inserted");

}

function update_custom_product($custom_pro)
{
	$finish = read_finish_product($custom_pro['customproducts_tks_referencepr']);

	$sql = "UPDATE ".TB_PREF."finish_product SET 
		finish_product_name=" . db_escape($finish['finish_product_name']). ",
		category_id=" . db_escape("84") . ",
		range_id= ".  db_escape("-1"). ",
		design_id=" . db_escape($finish['design_id']). ",
		color_id=" . db_escape($finish['color_id']) . ",
		leg_id=" . db_escape($finish['leg_id']).",
		wood_id=". db_escape($finish['wood_id']).",
		skin_id=". db_escape($finish['skin_id']).",
		fabric_id=". db_escape($finish['fabric_id']).",
		finish_id= ".  db_escape($finish['finish_id']). ",
		finish_code=" . db_escape($finish['finish_code']) . ",
		finish_comp_code=" . db_escape($custom_pro['customproducts_tks_temporaryco']).",
		gross_weight=". db_escape($finish['gross_weight']).",
		asb_weight=". db_escape($finish['asb_weight']).",
		asb_height=". db_escape($finish['asb_height']).",
		asb_density= ".  db_escape($finish['asb_density']). ",
		description= ".  db_escape($finish['description']). ",
		structure_wood_id= ".  db_escape($custom_pro['cf_942']). ",
		no_sitting=" . db_escape($finish['no_setting']);
				
    $sql .= " WHERE crm_id = ".$custom_pro['id'];
	db_query($sql, "The custom product could not be updated");
}


function get_product_crm_id()
{
	$sql = "select crm_id from ".TB_PREF."finish_product";
	$result = db_query($sql, "could not get Product CRM Id");
	$crm_id = Array();

	while($row = db_fetch($result))
	{
		$crm_id[] = $row['crm_id'];
	}
	return $crm_id;
}

function get_finish_codes()
{
	$sql = "select finish_comp_code from ".TB_PREF."finish_product";
	$result = db_query($sql, "could not get Fisih Product Code.");
	$finish_codes = Array();

	while($row = db_fetch($result))
	{
		$finish_codes[] = $row['finish_comp_code'];
	}
	return $finish_codes;
}


function update_finish_product_crm_id($finish_comp_code, $product_crm_id)
{
	 $sql = "UPDATE ".TB_PREF."finish_product SET 
		crm_id =" . db_escape($product_crm_id);
				
    $sql .= " WHERE finish_comp_code = ".db_escape($finish_comp_code);
	db_query($sql, "The finish product could not be updated");
}




function add_product_from_crm_to_erp(&$header_design)
{
	global $Refs;
		
	$sql = "INSERT INTO ".TB_PREF."finish_product (category_id, range_id, design_id, color_id, 
		leg_id, wood_id, skin_id, fabric_id, finish_id, finish_product_name, finish_code, finish_comp_code, gross_weight, asb_weight, asb_height, asb_density, description, product_image, collection_image, structure_wood_id, no_sitting ) VALUES(";
	$sql .= db_escape($header_design['category_id']) . "," .
		 db_escape($header_design['range_id']) . "," .
		 db_escape($header_design['design_id']) . ", " .
		 db_escape($header_design['color_id']) . ", " .
		 db_escape($header_design['leg_id']) . ", " .
		 db_escape($header_design['wood_id']) . ", " .
		 db_escape($header_design['skin_id']) . ", " .
		 db_escape($header_design['fabric_id']). ", " .
		 db_escape($header_design['finish_id']). ", " .
		 db_escape($header_design['finish_product_name']). ", " .
		 db_escape($header_design['finish_code']). ", " .
		 db_escape($header_design['finish_comp_code']). ", " .
		 db_escape($header_design['gross_weight']). ", " .
		 db_escape($header_design['asb_weight']). ", " .
		 db_escape($header_design['asb_height']). ", " .
		 db_escape($header_design['asb_density']). ", " .
		 db_escape($header_design['description']). ", " .
		 db_escape($header_design['product_image']). ", " .
		 db_escape($header_design['collection_image']). ", " .
		 db_escape($header_design['structure_wood_id']). ", " .
		 db_escape($header_design['no_setting']) . ")";

	db_query($sql, "The design header record could not be inserted");
}



function get_part_type_name($part_name){
	$sql = "SELECT * FROM ".TB_PREF."part_master WHERE part_id=".db_escape($part_name);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_fabric_type_name($fabric_name){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($fabric_name);
	$result = db_query($sql);
	return db_fetch($result);
}


function get_fcons_select_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_fcons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}


//------------------------- Add Finish Product Code --------------------------------

function array_equal($a, $b) {
	return (is_array($a) && is_array($b) && array_diff($a, $b) === array_diff($b, $a));
	return 1;
}

function add_finishcode(&$header_design,&$fabric_obj,&$consumable_obj)
{
	global $Refs;

	$sql = "select * from ".TB_PREF."finish_product where finish_comp_code = ".db_escape($header_design['finish_comp_code']);
	$result = db_query($sql, "Could not check Design Code");
	if(db_num_rows($result) > 0)
	{
		return true;
	}

	begin_transaction();
	//hook_db_prewrite($dc_obj, ST_DESIGN);
		
	$sql = "INSERT INTO ".TB_PREF."finish_product (category_id, range_id, design_id, color_id, 
	leg_id, wood_id, skin_id, fabric_id, finish_id, finish_product_name, finish_code, finish_comp_code, gross_weight, asb_weight, asb_height, asb_density, description, product_image, collection_image, structure_wood_id, no_sitting ) VALUES(";
	$sql .= db_escape($header_design['category_id']) . "," .
		db_escape($header_design['range_id']) . "," .
		db_escape($header_design['design_id']) . ", " .
		db_escape($header_design['color_id']) . ", " .
		db_escape($header_design['leg_id']) . ", " .
		db_escape($header_design['wood_id']) . ", " .
		db_escape($header_design['skin_id']) . ", " .
		db_escape($header_design['fabric_id']). ", " .
		db_escape($header_design['finish_id']). ", " .
		db_escape($header_design['finish_product_name']). ", " .
		db_escape($header_design['finish_code']). ", " .
		db_escape($header_design['finish_comp_code']). ", " .
		db_escape($header_design['gross_weight']). ", " .
		db_escape($header_design['asb_weight']). ", " .
		db_escape($header_design['asb_height']). ", " .
		db_escape($header_design['asb_density']). ", " .
		db_escape($header_design['description']). ", " .
		db_escape($header_design['product_image']). ", " .
		db_escape($header_design['collection_image']). ", " .
		db_escape($header_design['structure_wood_id']). ", " .
		db_escape($header_design['no_setting']) . ")";
	db_query($sql, "The design header record could not be inserted");


     /*Get the auto increment value of the order number created from the sql above */
     $finish_pro_id = db_insert_id();
     
	#*******************fabric custom work here******************
	if($header_design['skin_id'] == 30)
	{
		#Get current finish product fabric array
		foreach ($fabric_obj->line_items as $line_no => $asb_line)
		{
			$fabric_ids[] = $asb_line->fabric_name;
		}
			
		#Get existing fabric combination name
		$sql_fabcode = "SELECT fab_code_id,fabric_comb FROM ".TB_PREF."fabric_code group by fab_code_id ";
		$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
		$id_fabcode = '';
			
		$fab_comb_for_finish_pro = '';
		while ($row_fabcode = db_fetch($result_fabcode)) 
		{
			$id_fabcode =  $row_fabcode["fab_code_id"];
		
			#Get each fabric combination array
			$id_fabcomb = array();
			$sql_fabcomb = "SELECT fabric_code_name FROM ".TB_PREF."fabric_combination where fab_code_id =".$id_fabcode;
		 	$result_fabcomb = db_query($sql_fabcomb, "One of the fabric combination records could not be get");
			while ($row_fabcomb = db_fetch($result_fabcomb)) {
				$id_fabcomb[] =  $row_fabcomb["fabric_code_name"];
			} 
			 
			#Compare new combination with existing combination
			$result_diff = array_equal($fabric_ids,$id_fabcomb);
			if($result_diff == 1){
				$fab_comb_for_finish_pro = $row_fabcode["fab_code_id"];
				 break;
			}
			
		}
	
		if($fab_comb_for_finish_pro == '')
		{
			#Insert new fabric combination in 0_fabric_code
			$new_fabcode = 'f'.($id_fabcode+1);
			$sql = "INSERT INTO ".TB_PREF."fabric_code (fabric_comb) VALUES ('".$new_fabcode."')";
			db_query($sql, "One of the fabric finish_pro relation records could not be inserted into fabric_code");
			$new_fabcod_id =db_insert_id();
			
			#Insert  finish product relation with fabric combination
			 $sql = "INSERT INTO ".TB_PREF."fabric_pro_relation (fab_code_id, finish_code_id) VALUES (";
					$sql .= $new_fabcod_id . ", " . $finish_pro_id .")";
			 db_query($sql, "One of the fabric finish_pro relation records could not be inserted into fabric_pro_relation");
			 
			 #Insert  New fabric combination 
			 foreach ($fabric_ids as $fabric_id)
			 {
				$sql = "INSERT INTO ".TB_PREF."fabric_combination (fab_code_id, fabric_code_name) VALUES (";
					$sql .= $new_fabcod_id. ", " . $fabric_id .")";
				db_query($sql, "One of the fabric combination records could not be inserted into fabric_combination");
			 }
			 
			 
		}else{
			#Insert into 0_fabric_pro_relation for finish product relation with fabric combination
			 $sql = "INSERT INTO ".TB_PREF."fabric_pro_relation (fab_code_id, finish_code_id) VALUES (";
			 $sql .= $fab_comb_for_finish_pro . ", " . $finish_pro_id.")";
			 db_query($sql, "One of the fabric finish_pro relation records could not be inserted into fabric_pro_relation");
		}
	}
	
		
	foreach ($fabric_obj->line_items as $line_no => $asb_line)
	{
		$sql = "INSERT INTO ".TB_PREF."fabric_part (finish_pro_id, part_id, fabric_id, perecentage) VALUES (";
		$sql .= $finish_pro_id . ", " . db_escape($asb_line->part_name). "," .
		db_escape($asb_line->fabric_name). "," .
		db_escape($asb_line->percentage). ")";
		db_query($sql, "One of the fabric detail records could not be inserted");
		$fabric_obj->line_items[$line_no]->fab_detail_rec = db_insert_id();
	}
	
	// $consumable_obj->design_id = db_insert_id();  
	foreach ($consumable_obj->line_con as $line_no => $con_line)
    {
		$sql = "INSERT INTO ".TB_PREF."fconsumable_part(finish_pro_id, cons_type, cons_select, cons_unit, cons_qty, cons_iscbm) VALUES (";
		$sql .= $finish_pro_id . ", " . db_escape($con_line->cons_type). "," .
		db_escape($con_line->cons_select). "," .
		db_escape($con_line->cons_unit) . "," .
		db_escape($con_line->cons_quntity) . ", " .
		db_escape($con_line->cons_iscbm). ")";
		db_query($sql, "One of the consumable detail records could not be inserted");
		$consumable_obj->line_con[$line_no]->fcon_detail_rec = db_insert_id();
    }
	commit_transaction();

	return $finish_pro_id;
}



//*************** get finish details ************
function get_finish_search($all_items)
{

	
	global $order_number;
	
	$sql ='SELECT d.finish_comp_code,d.finish_product_name, CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeNAME, c.cat_name, d.asb_weight, d.asb_density, d.asb_height, d.description from '.TB_PREF.'finish_product d Left Join '.TB_PREF.'item_range t on d.range_id = t.id Left Join '.TB_PREF.'item_category c on d.category_id = c.category_id ';//shubham code
		
	if (isset($order_number) && $order_number != "")
	{
		$sql .= " Where d.finish_comp_code LIKE ".db_escape('%'. $order_number . '%')." OR d.finish_product_name LIKE".db_escape('%'. $order_number . '%');
	}

	$sql .= " order by d.finish_comp_code";
	
	return $sql;
}


function get_finish_code_id($code_id){
 	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_comp_code=".db_escape($code_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_finish_code_row($finish_code){
	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_comp_code=".db_escape($finish_code);
	$result = db_query($sql,"an finish code could not be retrieved");
	return db_fetch($result);
}

function finish_product_deleted($id)
{
	$sql = "DELETE FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($id);
	db_query($sql, "The finish code could not be deleted");

	$sql = "DELETE FROM ".TB_PREF."fabric_part WHERE finish_pro_id =".db_escape($id);
	db_query($sql, "The fabric part could not be deleted.");
	
	$sql = "DELETE FROM ".TB_PREF."fconsumable_part WHERE finish_pro_id =".db_escape($id);
	db_query($sql, "The consumable part could not be deleted.");
}
//**************** end finish details **********


//----------------------------------------------------------------------------------------


function update_finishcode($finish_pro_id, &$udpate_design, &$fabric_obj, &$consumable_obj)
{
	
	begin_transaction();
    //Update the purchase order header with any changes 
    $sql = "UPDATE ".TB_PREF."finish_product SET 
		finish_product_name=" . db_escape($udpate_design['finish_product_name']). ",
		category_id=" . db_escape($udpate_design['category_id']) . ",
		range_id= ".  db_escape($udpate_design['range_id']). ",
		design_id=" . db_escape($udpate_design['design_id']). ",
		color_id=" . db_escape($udpate_design['color_id']) . ",
		leg_id=" . db_escape($udpate_design['leg_id']).",
		wood_id=". db_escape($udpate_design['wood_id']).",
		skin_id=". db_escape($udpate_design['skin_id']).",
		fabric_id=". db_escape($udpate_design['fabric_id']).",
		finish_id= ".  db_escape($udpate_design['finish_id']). ",
		finish_code=" . db_escape($udpate_design['finish_code']) . ",
		finish_comp_code=" . db_escape($udpate_design['finish_comp_code']).",
		gross_weight=". db_escape($udpate_design['gross_weight']).",
		asb_weight=". db_escape($udpate_design['asb_weight']).",
		asb_height=". db_escape($udpate_design['asb_height']).",
		asb_density= ".  db_escape($udpate_design['asb_density']). ",
		description= ".  db_escape($udpate_design['description']). ",
		structure_wood_id= ".  db_escape($udpate_design['structure_wood_id']). ",
		no_sitting=" . db_escape($udpate_design['no_setting']);
				
    $sql .= " WHERE finish_pro_id = ".$finish_pro_id;
	db_query($sql, "The finish product could not be updated");

	$sql = "DELETE FROM ".TB_PREF."fabric_part WHERE finish_pro_id="
		.db_escape($finish_pro_id);
	db_query($sql, "could not delete old assemble part details");
  
	foreach ($fabric_obj->line_items as $line_no => $asb_line)
     {
		$sql = "INSERT INTO ".TB_PREF."fabric_part (finish_pro_id, part_id, fabric_id, perecentage) VALUES (";
		$sql .= $finish_pro_id . ", " . db_escape($asb_line->part_name). "," .
		db_escape($asb_line->fabric_name). "," .
		db_escape($asb_line->percentage). ")";
		db_query($sql, "One of the fabric detail records could not be inserted");
		$fabric_obj->line_items[$line_no]->fab_detail_rec = db_insert_id();
     }
	 
	$sql = "DELETE FROM ".TB_PREF."fconsumable_part WHERE finish_pro_id="
		.db_escape($finish_pro_id);
		db_query($sql, "could not delete old consumable part details");
	
	  foreach ($consumable_obj->line_con as $line_no => $con_line)
     {
		$sql = "INSERT INTO ".TB_PREF."fconsumable_part(finish_pro_id, cons_type, cons_select, cons_unit, cons_qty, cons_iscbm) VALUES (";
		$sql .= $finish_pro_id . ", " . db_escape($con_line->cons_type). "," .
		db_escape($con_line->cons_select). "," .
		db_escape($con_line->cons_unit) . "," .
		db_escape($con_line->cons_quntity) . ", " .
		db_escape($con_line->cons_iscbm). ")";
		db_query($sql, "One of the consumable detail records could not be inserted");
		$consumable_obj->line_con[$line_no]->fcon_detail_rec = db_insert_id();
     }

	
	commit_transaction();

	return $finish_pro_id;
}



//-------------------- read assemble part --------------------------

function get_finish_header($finish_pro_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_pro_id);

   	$result = db_query($sql, "The finish code cannot be retrieved");
	if (db_num_rows($result) == 1)
	{

      	$myrow = db_fetch($result);
				$order->finish_pro_id = $finish_pro_id;
      			$_POST['finish_pro_id'] = $myrow["finish_pro_id"];
				$_POST['category_id']  = $myrow["category_id"];
				//$_POST['collection_id']  = $myrow["collection_id"];
          		$_POST['range_id']  = $myrow["range_id"];
					if($myrow["category_id"] > 0 && $myrow["range_id"]>0)
					   {
						  $_POST['collection_id']  = "Collection";
					   }else if($myrow["category_id"] > 0 && $myrow["range_id"]<=0){
						  $_POST['collection_id']  = "NonCollection";
					   }else{
						 $_POST['collection_id']  = "SelectCollection";
					}
				$_POST['range_id']  = $myrow["range_id"];	
				$_POST['design_id'] = $myrow["design_id"];
				$_POST['color_id']  = $myrow["color_id"];
				$_POST['leg_id']  = $myrow["leg_id"];
				$_POST['wood_id']  = $myrow["wood_id"];
				$_POST['skin_id']  = $myrow["skin_id"];
				$_POST['pro_id']  = $myrow["product_type"];
				$_POST['fabric_id']  = $myrow["fabric_id"];
				
				$_POST['finish_id']  = $myrow["finish_id"];
				$_POST['finish_product_name']  = $myrow["finish_product_name"];
				$_POST['finish_code']  = $myrow["finish_code"];
				$_POST['finish_comp_code']  = $myrow["finish_comp_code"];
				$_POST['gross_weight']  = $myrow["gross_weight"];
				$_POST['asb_weight']  = $myrow["asb_weight"];
				$_POST['asb_height']  = $myrow["asb_height"];
				$_POST['asb_density']  = $myrow["asb_density"];
				$_POST['height_design']  = $myrow["height"];
				$_POST['description']  = $myrow["description"];
				$_POST['structure_wood_id']  = $myrow["structure_wood_id"];
				$_POST['no_setting']  = $myrow["no_sitting"];
				$_POST['coll_pic '] = $myrow["collection_image"];			

      	return true;
	}

	
	return false;
}

//*************************************************************
function read_fabric($finish_pro_id){
	
	$result = get_finish_header($finish_pro_id, $order);
	
}

function read_fab_items($finish_pro_id, &$fab)
{
	
	$sql = "SELECT * FROM ".TB_PREF."fabric_part WHERE finish_pro_id=".db_escape($finish_pro_id); 
	$result = db_query($sql, "fabric part cannot be retrive");
	
	if (db_num_rows($result) > 0)
		{
			while ($myrow = db_fetch($result))
			{
	
				if ($fab->add_to_fab_part($fab->lines_on_fab, $myrow["part_id"],$myrow["fabric_id"],$myrow["perecentage"])) {
						$newline = &$fab->line_items[$fab->lines_on_fab-1];
						$newline->fab_detail_rec = $myrow["fab_line_details"];			
				}
			} 
			
		} 
}
function read_cons_items($finish_pro_id, &$cons)
{
	
	$sql = "SELECT * FROM ".TB_PREF."fconsumable_part WHERE finish_pro_id=".db_escape($finish_pro_id); 
	$result = db_query($sql, "consumable part cannot be retrive");
	
		if (db_num_rows($result) > 0)
			{
				while ($myrow1 = db_fetch($result))
				{
		
					if ($cons->add_to_fcon_part($cons->lines_on_fcon, $myrow1["cons_type"],
						$myrow1["cons_select"],$myrow1["cons_unit"],$myrow1["cons_qty"],$myrow1["cons_iscbm"])) {
							$newline1 = &$cons->line_con[($cons->lines_on_fcon-1)+100];
							$newline1->fcon_detail_rec = $myrow1["fcon_line_details"];			
					}
				} 
			}  
}

// -------------- get image list -----------

function get_product_image($finish_pro_id){
	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_pro_id); 
	$result = db_query($sql);
	return db_fetch($result);
}

function update_collection_image($selected_id, $coll_image){
	$sql = "UPDATE ".TB_PREF."finish_product SET collection_image=" . db_escape($coll_image)." WHERE finish_pro_id=".db_escape($selected_id);
	db_query($sql, "could not update collection image");
}
function update_finish_image($selected_id, $session_photo){
	$sql = "UPDATE ".TB_PREF."finish_product SET product_image=" . db_escape($session_photo)." WHERE finish_pro_id=".db_escape($selected_id);
	db_query($sql, "could not update product image");
}



// -------------- Code by bajrang -----------

function get_design_header($design_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($design_id);

   	$result = db_query($sql, "The design code cannot be retrieved");
	if (db_num_rows($result) == 1)
	{

      	$myrow = db_fetch($result);
      			$_POST['design_id'] = $myrow["design_id"];
				$_POST['category_id']  = $myrow["category_id"];
				//$_POST['collection_id']  = $myrow["collection_id"];
				$_POST['range_id']  = $myrow["range_id"];
				if($myrow["category_id"] > 0 && $myrow["range_id"]>0)
				   {
					    $_POST['collection_id']  = "Collection";
				   }else if($myrow["category_id"] > 0 && $myrow["range_id"]<=0){
				      $_POST['collection_id']  = "NonCollection";
				   }else{
					   $_POST['collection_id']  = "SelectCollection";
				}
				$_POST['finish_product_name']  = $myrow["product_name"];
				

      	return true;
	}

	
	return false;
}


function read_design($design_id){
	
	$result = get_design_header($design_id, $order);
	
}

function check_finish_code($check_finish){
	echo $check_finish;
	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_comp_code=".db_escape($check_finish); 
	$result = db_query($sql);
	return db_fetch($result);
}
// --------------END Code by bajrang -----------
?>