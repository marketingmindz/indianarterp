<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//----------------------------------------------------------------------------------------

function get_desing_code_id($code_id){
 	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_code=".db_escape($code_id);
	$result = db_query($sql);
	return db_fetch($result);
}
function delete_design($id)
{
	$sql = "DELETE FROM ".TB_PREF."design_code WHERE design_id=".db_escape($id);
	db_query($sql, "The design code could not be deleted");

	$sql = "DELETE FROM ".TB_PREF."assemble_part WHERE design_id =".db_escape($id);
	db_query($sql, "The assemble part could not be deleted.");
	
	$sql = "DELETE FROM ".TB_PREF."consumable_part WHERE design_id =".db_escape($id);
	db_query($sql, "The consumable part could not be deleted.");
}

//----------------------------------------------------------------------------------------
//------- update desing image ---------------
function update_design_image($select_id,$desing_image){
	
	$sql = "UPDATE ".TB_PREF."design_code SET design_image=".db_escape($desing_image)
    	."WHERE design_id=".db_escape($select_id);
    	db_query($sql, "could not update desing image");
}

//---------- end desing image --------------
function add_designcode(&$header_design,&$assemble_obj,&$consumable_obj)
{
	global $Refs;

	$sql = "SELECT * FROM ".TB_PREF."design_code where design_code = ".db_escape($header_design['design_code']);
	$result = db_query($sql, "Could not check Design Code");
	if(db_num_rows($result) > 0)
	{
		return true;
	}

	begin_transaction();

	$sql = "INSERT INTO ".TB_PREF."design_code (range_id, category_id, design_code, product_type, 
	product_name, pkg_unit, pkg_w, pkg_h, pkg_d, weight, height, density, description, design_image, pro_cbm, pak_cbm) VALUES(";
	$sql .= db_escape($header_design['range_id']) . "," .
	 db_escape($header_design['category_id']) . "," .
	 db_escape($header_design['design_code']) . ", " .
	 db_escape($header_design['pro_id']) . ", " .
	 db_escape($header_design['product_name']) . ", " .
	 db_escape($header_design['pkg_unit']) . ", " .
	 db_escape($header_design['pkg_w']) . ", " .
	 db_escape($header_design['pkg_h']) . ", " .
	 db_escape($header_design['pkg_d']) . ", " .
	 db_escape($header_design['width']) . ", " .
	 db_escape($header_design['height']) . ", " .
	 db_escape($header_design['density']). ", " .
	 db_escape($header_design['description']). ", " .
	 db_escape($header_design['design_image']). ", " .
	  db_escape($header_design['pro_cbm']). ", " .
	 db_escape($header_design['pak_cbm']) . ")";

	db_query($sql, "The design header record could not be inserted");
		
	/*Get the auto increment value of the order number created from the sql above */
	$design_id = db_insert_id();

	/*Suresh code start here for design code image and tag upload 2Feb 2017 
      foreach ($header_design['imgPlusTag'] as $imgPlusTagNo => $imgPlusTag) {		 
		if(!empty($imgPlusTag['img'])) {
		   $imgPlusTagSql = "INSERT INTO ".TB_PREF."design_code_images (designCodeID, Image, img_tag) VALUES('".$design_id."','".$imgPlusTag['img']."','".$imgPlusTag['tag']."')";
			db_query($imgPlusTagSql, "image plus tag save");
		}
	}
	Suresh code end here for design code image and tag upload 2Feb 2017 */

	foreach ($assemble_obj->line_items as $line_no => $asb_line)
	{
		$sql = "INSERT INTO ".TB_PREF."assemble_part (design_id, part_name, assemble_width, assemble_height, assemble_density, iscbm, assemble_code, quantity, netweight, img_name, part_category, part_collection, part_range, old_design) VALUES (";
		$sql .= $design_id . ", " . db_escape($asb_line->part_name). "," .
		db_escape($asb_line->width) . "," .
		db_escape($asb_line->height) . ", " .
		db_escape($asb_line->density) . ", " .
		db_escape($asb_line->iscbm) . ", " .
		db_escape($asb_line->asb_code) . ", " .
		db_escape($asb_line->qty) . ", " .
		db_escape($asb_line->n_wt) . ", " .
		db_escape($asb_line->img_name) . ", " .
		db_escape($asb_line->part_category) . ", " .
		db_escape($asb_line->part_collection) . ", " .
		db_escape($asb_line->part_range) . ", " .
		db_escape($asb_line->old_design). ")";
		db_query($sql, "One of the assemble detail records could not be inserted");
		$assemble_obj->line_items[$line_no]->asb_detail_rec = db_insert_id();
		
		$sql_check = "SELECT * FROM ".TB_PREF."design_code where design_code = ".db_escape($asb_line->asb_code);
		$check_result = db_query($sql_check, "Could not check Design Code");
		if(db_num_rows($check_result) == 0)
		{
			
			$sql_des = "INSERT INTO ".TB_PREF."design_code (range_id, category_id, design_code,
			product_name, weight, height, density, design_image) VALUES(";
			$sql_des .= db_escape($asb_line->part_range) . "," .
			 db_escape($asb_line->part_category) . "," .
			 db_escape($asb_line->asb_code) . ", " .
			 db_escape($asb_line->part_name) . ", " .
			 db_escape($asb_line->width) . ", " .
			 db_escape($asb_line->height) . ", " .
			 db_escape($asb_line->density). ", " .
			 db_escape($asb_line->img_name) . ")";
			 db_query($sql_des, "The design header record could not be inserted");
		}

	
	}

	 
	// $consumable_obj->design_id = db_insert_id();  
	foreach ($consumable_obj->line_con as $line_no => $con_line)
	{
		$sql = "INSERT INTO ".TB_PREF."consumable_part(design_id, cons_type, cons_select, cons_unit, cons_qty, cons_iscbm) VALUES (";
		$sql .= $design_id . ", " . db_escape($con_line->cons_type). "," .
		db_escape($con_line->cons_select). "," .
		db_escape($con_line->cons_unit) . "," .
		db_escape($con_line->cons_quntity) . ", " .
		db_escape($con_line->cons_iscbm). ")";
		db_query($sql, "One of the assemble detail records could not be inserted");
		$consumable_obj->line_con[$line_no]->con_detail_rec = db_insert_id();
	}
	commit_transaction();

	
	//-------------- Code by Chandan - Start ---------------

	// When design codes created, insert into consumables table also.
	/*$size = $header_design['pkg_w'].",".$header_design['pkg_h'].",".$header_design['pkg_d'];
	$document = '';
	$design_image = $header_design['design_code'].".jpg";

	$sql = "INSERT INTO ".TB_PREF."consumable_master (master_id, company_id,
			unit_id, principal_id, consumable_code, consumable_name, 
			size, weight, cbm, gsm, description, document,consumable_image)
		VALUES ("
		.db_escape(45).","
		.db_escape('-1').","
		.db_escape($header_design['pkg_unit']).","
		.db_escape(0).","
		.db_escape($header_design['design_code']).","
		.db_escape($header_design['product_name']).","
		.db_escape($size).","
		.db_escape($header_design['weight']).","
		.db_escape($header_design['pak_cbm']).","
		.db_escape('').","
		.db_escape($header_design['description']).","
		.db_escape($document).","
		.db_escape($design_image).")";

	db_query($sql,"an Consumable could not be added");*/

	//-------------- Code by Chandan - End ---------------

	return $design_id;
}

/*range_id=" . db_escape($udpate_design['range_id']) . ",
		category_id= ".  db_escape($udpate_design['category_id']). ",*/
function update_designcode($design_id, &$udpate_design, &$assemble_obj, &$consumable_obj)
{
	
	begin_transaction();
    //Update the purchase order header with any changes 
    $sql = "UPDATE ".TB_PREF."design_code SET 
		design_code=" . db_escape($udpate_design['design_code']). ",
		product_type=" . db_escape($udpate_design['pro_id']) . ",
		product_name=" . db_escape($udpate_design['product_name']).",
		pkg_unit=". db_escape($udpate_design['pkg_unit']).",
		pkg_w=". db_escape($udpate_design['pkg_w']).",
		pkg_h=". db_escape($udpate_design['pkg_h']).",
		pkg_d=". db_escape($udpate_design['pkg_d']).",
		weight=". db_escape($udpate_design['width']).",
		height=". db_escape($udpate_design['height']).",
		density=". db_escape($udpate_design['density']).",
		description=". db_escape($udpate_design['description']).",
		pro_cbm=". db_escape($udpate_design['pro_cbm']).",
		pak_cbm=". db_escape($udpate_design['pak_cbm']);
		
		
    $sql .= " WHERE design_id = ".$design_id;
	db_query($sql, "The design could not be updated");
	
	$sql = "DELETE FROM ".TB_PREF."assemble_part WHERE design_id="
		.db_escape($design_id);
	db_query($sql, "could not delete old assemble part details");
  
	
	 foreach ($assemble_obj->line_items as $line_no => $asb_line)
	{
		$sql = "INSERT INTO ".TB_PREF."assemble_part (design_id, part_name, assemble_width, assemble_height, assemble_density, iscbm, assemble_code, quantity, netweight, img_name, part_category, part_collection, part_range, old_design) VALUES (";
		$sql .= $design_id . ", " . db_escape($asb_line->part_name). "," .
		db_escape($asb_line->width) . "," .
		db_escape($asb_line->height) . ", " .
		db_escape($asb_line->density) . ", " .
		db_escape($asb_line->iscbm) . ", " .
		db_escape($asb_line->asb_code) . ", " .
		db_escape($asb_line->qty) . ", " .
		db_escape($asb_line->n_wt) . ", " .
		db_escape($asb_line->img_name) . ", " .
		db_escape($asb_line->part_category) . ", " .
		db_escape($asb_line->part_collection) . ", " .
		db_escape($asb_line->part_range) . ", " .
		db_escape($asb_line->old_design). ")";
		db_query($sql, "One of the assemble detail records could not be inserted");
		$assemble_obj->line_items[$line_no]->asb_detail_rec = db_insert_id();

		$sql_check = "SELECT * FROM ".TB_PREF."design_code where design_code = ".db_escape($asb_line->asb_code);
		$check_result = db_query($sql_check, "Could not check Design Code");
		if(db_num_rows($check_result) == 0)
		{
			
			$sql_des = "INSERT INTO ".TB_PREF."design_code (range_id, category_id, design_code,
			product_name, weight, height, density, design_image) VALUES(";
			$sql_des .= db_escape($asb_line->part_range) . "," .
			 db_escape($asb_line->part_category) . "," .
			 db_escape($asb_line->asb_code) . ", " .
			 db_escape($asb_line->part_name) . ", " .
			 db_escape($asb_line->width) . ", " .
			 db_escape($asb_line->height) . ", " .
			 db_escape($asb_line->density). ", " .
			 db_escape($asb_line->img_name) . ")";
			 db_query($sql_des, "The design header record could not be inserted");
		}
     }
	
	$sql = "DELETE FROM ".TB_PREF."consumable_part WHERE design_id="
		.db_escape($design_id);
		db_query($sql, "could not delete old consumable part details");
	
	 foreach ($consumable_obj->line_con as $line_no => $con_line)
     {
		$sql = "INSERT INTO ".TB_PREF."consumable_part(design_id, cons_type, cons_select, cons_unit, cons_qty, cons_iscbm) VALUES (";
		$sql .= $design_id . ", " . db_escape($con_line->cons_type). "," .
		db_escape($con_line->cons_select). "," .
		db_escape($con_line->cons_unit) . "," .
		db_escape($con_line->cons_quntity) . ", " .
		db_escape($con_line->cons_iscbm). ")";
		db_query($sql, "One of the assemble detail records could not be inserted");
		$consumable_obj->line_con[$line_no]->con_detail_rec = db_insert_id();
     }

	
	commit_transaction();

	return $design_id;
}

//------------------------ Get Design code --------------------------------------------
//------------------ update first section part-----------------------------------------

function update_first_designcode($design_id, &$udpate_design)
{
	
	begin_transaction();
    //Update the purchase order header with any changes 
    $sql = "UPDATE ".TB_PREF."design_code SET 
		design_code=" . db_escape($udpate_design['design_code']). ",
		product_type=" . db_escape($udpate_design['pro_id']) . ",
		product_name=" . db_escape($udpate_design['product_name']).",
		pkg_unit=". db_escape($udpate_design['pkg_unit']).",
		pkg_w=". db_escape($udpate_design['pkg_w']).",
		pkg_h=". db_escape($udpate_design['pkg_h']).",
		pkg_d=". db_escape($udpate_design['pkg_d']).",
		weight=". db_escape($udpate_design['width']).",
		height=". db_escape($udpate_design['height']).",
		density=". db_escape($udpate_design['density']).",
		description=". db_escape($udpate_design['description']).",
		pro_cbm=". db_escape($udpate_design['pro_cbm']).",
		pak_cbm=". db_escape($udpate_design['pak_cbm']);
		
		
    $sql .= " WHERE design_id = ".$design_id;
	db_query($sql, "The design could not be updated");

	
	commit_transaction();

	return $design_id;
}


//------------------ end first section part -------------------------------------
function get_design_search($all_items)
{
	global $order_number;
	
	$sql ='SELECT d.design_code,d.product_name, CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeNAME, c.cat_name, p.pro_name, d.weight, d.density, d.height, d.description FROM '.TB_PREF.'design_code d Left Join '.TB_PREF.'item_range t on d.range_id = t.id Left Join '.TB_PREF.'item_category c on d.category_id = c.category_id 
	Left Join '.TB_PREF.'item_products p on d.product_type = p.pro_id';//shubham changes
		
	if (isset($order_number) && $order_number != "")
	{
		$sql .= " WHERE d.design_code LIKE ".db_escape('%'. $order_number . '%')." OR d.product_name LIKE".db_escape('%'. $order_number . '%')." order by design_code";
	}
	else
	{
		$sql .= " order by range_id, design_code";
	}
	return $sql;
}

function get_cons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_select_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_design_code_row($design_code_id){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_code=".db_escape($design_code_id);
	$result = db_query($sql,"an design code could n=> 1113
    [2] => SAND ISLAND BED SIDEot be retrieved");
	return db_fetch($result);
}

function get_category_type_name($part_category){
	$sql = "SELECT * FROM ".TB_PREF."item_category WHERE category_id=".db_escape($part_category);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_range_type_name($part_range){
	$sql = "SELECT * FROM ".TB_PREF."item_range WHERE id=".db_escape($part_range);
	$result = db_query($sql);
	return db_fetch($result);
}

//-------------------- read assemble part --------------------------

function get_design_header($design_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($design_id);

   	$result = db_query($sql, "The design code cannot be retrieved");
	if (db_num_rows($result) == 1)
	{

      	$myrow = db_fetch($result);
				$order->design_id = $design_id;
      			$_POST['design_id'] = $myrow["design_id"];
				$_POST['category_id']  = $myrow["category_id"];
				//$_POST['collection_id']  = $myrow["collection_id"];
				$_POST['range_id']  = $myrow["range_id"];
				if($myrow["category_id"] > 0 && $myrow["range_id"]>0)
				   {
					    $_POST['collection_id']  = "Collection";
				   }else if($myrow["category_id"] > 0 && $myrow["range_id"]<=0){
				      $_POST['collection_id']  = "NonCollection";
				   }else{
					   $_POST['collection_id']  = "SelectCollection";
				}
				$_POST['product_name']  = $myrow["product_name"];
				$_POST['pro_id']  = $myrow["product_type"];
				$_POST['pkg_unit']  = $myrow["pkg_unit"];
				$_POST['pkg_w']  = $myrow["pkg_w"];
				$_POST['pkg_h']  = $myrow["pkg_h"];
				$_POST['pkg_d']  = $myrow["pkg_d"];
				$_POST['width_design']  = $myrow["weight"];
				$_POST['height_design']  = $myrow["height"];
				$_POST['density_design']  = $myrow["density"];
				$_POST['design_code']  = $myrow["design_code"];
				$_POST['description']  = $myrow["description"];
				$_POST['pro_cbm']  = $myrow["pro_cbm"];
				$_POST['pak_cbm']  = $myrow["pak_cbm"];

      	return true;
	}

	
	return false;
}

//*************************************************************
function read_design($design_id){
	
	$result = get_design_header($design_id, $order);
	
}

function read_asb_items($design_id, &$asb)
{
	
	$sql = "SELECT * FROM ".TB_PREF."assemble_part WHERE design_id=".db_escape($design_id); 
	$result = db_query($sql, "assemble part cannot be retrive");
	
	if (db_num_rows($result) > 0)
		{
			while ($myrow = db_fetch($result))
			{

				if ($asb->add_to_asb_part($asb->lines_on_asb, $myrow["part_category"], $myrow["part_collection"], $myrow["part_range"], $myrow["old_design"] , $myrow["assemble_code"], $myrow["part_name"],
					$myrow["assemble_width"],$myrow["assemble_height"],$myrow["assemble_density"],$myrow["iscbm"], $myrow["quantity"], $myrow["netweight"], $myrow["img_name"])) {
						$newline = &$asb->line_items[$asb->lines_on_asb-1];
						$newline->asb_detail_rec = $myrow["asb_line_details"];			
				}
			} 
			
		} 
}
function read_cons_items($design_id, &$cons)
{
	
	$sql = "SELECT * FROM ".TB_PREF."consumable_part WHERE design_id=".db_escape($design_id); 
	$result = db_query($sql, "consumable part cannot be retrive");
	
		if (db_num_rows($result) > 0)
			{
				while ($myrow1 = db_fetch($result))
				{
		
					if ($cons->add_to_con_part($cons->lines_on_con, $myrow1["cons_type"],
						$myrow1["cons_select"],$myrow1["cons_unit"],$myrow1["cons_qty"],$myrow1["cons_iscbm"])) {
							$newline1 = &$cons->line_con[($cons->lines_on_con-1)+100];
							$newline1->con_detail_rec = $myrow1["con_line_details"];			
					}
				} 
			}  
}
function get_design_image($des_id){
	$sql = "SELECT * FROM ".TB_PREF."design_code WHERE design_id=".db_escape($des_id); 
	$result = db_query($sql);
	return db_fetch($result);
}

?>