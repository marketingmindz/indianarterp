<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_item_range($range_name, $reference)
{
	
		$sql = "INSERT INTO ".TB_PREF."item_range(range_name, reference)
		VALUES (" . db_escape($range_name) . ", " .
		db_escape($reference). ")";
        
   	   db_query($sql,"could not add new item range");
}

//--------------------------------------------------------------------------------------------------

function update_item_range($id, $range_name, $reference)
{
    	$sql = "UPDATE ".TB_PREF."item_range SET range_name=".db_escape($range_name)
    	.",reference=".db_escape($reference)
    	." WHERE id=".db_escape($id);
    	db_query($sql, "could not update range");
}
//--------------------------------------------------------------------------------------------------

function delete_item_range($id)
{
	$sql = "DELETE FROM ".TB_PREF."item_range WHERE id=".db_escape($id);

	db_query($sql, "could not delete range");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_item_range($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."item_range";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get range");
}

function get_item_range($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."item_range WHERE id=".db_escape($selected_id);
	$result = db_query($sql,"range could not be retrieved");
	return db_fetch($result);
}

/*function check_range($range_name, $reference)
{
	 $sql = "SELECT * FROM  ".TB_PREF."item_range WHERE range_name= ".db_escape($range_name)." OR reference=".db_escape($reference)." "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}*/

function check_range($reference)
{
	 $sql = 'SELECT t.reference, cat.cat_reference, m.master_reference FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m WHERE cat.cat_reference = '.db_escape($reference).'
	 OR m.master_reference = '.db_escape($reference).' OR t.reference = '.db_escape($reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}

function check_update_range($reference)
{
	 $sql = 'SELECT cat.cat_reference, m.master_reference FROM '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m WHERE cat.cat_reference = '.db_escape($reference).' OR m.master_reference = '.db_escape($reference).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}
/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/



?>