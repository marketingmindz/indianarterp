<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_work_center($work_center_name, $pro_team_id, $work_center_desc)
{
	
	$sql = "INSERT INTO ".TB_PREF."work_center(work_center_name, pro_team_id,work_center_desc)
		VALUES (" . db_escape($work_center_name) . ", " . db_escape($pro_team_id) . ", " .
		db_escape($work_center_desc). ")";		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_work_center($id, $work_center_name, $pro_team_id, $work_center_desc)
{
      $sql = "UPDATE ".TB_PREF."work_center SET work_center_name=".db_escape($work_center_name)
	  .",pro_team_id=".db_escape($pro_team_id)
    	.",work_center_desc=".db_escape($work_center_desc)
    	."WHERE work_center_id=".db_escape($id);
    	db_query($sql, "could not update master");
}
//--------------------------------------------------------------------------------------------------

function delete_work_center($id)
{
	
	 $sql = "DELETE FROM ".TB_PREF."work_center WHERE work_center_id=".db_escape($id);

	db_query($sql, "could not delete prodcution team");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_work_center($all=false) {
  $sql = "SELECT * FROM ".TB_PREF."work_center";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get center");
}

function get_work_center($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."work_center WHERE work_center_id=".db_escape($selected_id);
	$result = db_query($sql,"prodcution could not be retrieved");
	return db_fetch($result);
}



//----------- get production tema ------------

/*function get_production_team_name($pro_id){
	$pro_name = explode(",",$pro_id);
	foreach($pro_name as $val){
		$pro_id = $val;
		$sql = "Select * from production_team";
	}
}*/



function get_team_name($prod_team_id)
{
	$sql="SELECT pro_team_name FROM ".TB_PREF."production_team WHERE pro_team_id = ".db_escape($prod_team_id);
	$result = db_query($sql,"production team name could not be retrieved");
	$prod_team_res =  db_fetch($result);
	return $prod_team_res['pro_team_name'];
}
function get_product_id($pro_id){
	$prod_team_id = explode(",",$pro_id);
	$count = sizeof($prod_team_id);
	$team_name = "";
	for($i = 0; $i<$count; $i++)
	{		
		$team_name .= get_team_name($prod_team_id[$i]).",";
	}
	$team_name_str = rtrim($team_name,',');
	return $team_name_str;
}
/*function check_master($work_center_name, $master_reference)
{
	 $sql = "SELECT * FROM  ".TB_PREF."work_center WHERE work_center_name= ".db_escape($work_center_name)." OR master_reference=".db_escape($master_reference)." "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}
*/

/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/

?>