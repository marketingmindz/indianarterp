<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// - -  ----------------    Code by bajrang   - get opening stock  ------- - - - - -

function get_all_opening_stock_master() {
  $sql = "SELECT * FROM ".TB_PREF."opening_stock_master";
    $result =   db_query($sql, "could not get opening stock Master");
	return $result;
}

function check_consumable_stock($stock_type_id, $consumble_id, $consumble_category)
{
	$check_consumable = "select stock_type_id,consumable_id,consumable_category from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && consumable_id = '$consumble_id' && consumable_category = '$consumble_category'";
	$result = db_query($check_consumable,'aa');
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_design_stock($stock_type_id, $pro_design_id)
{
	$check_design = "select stock_type_id,pro_design_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_design_id = '$pro_design_id'";
	$result = db_query($check_design);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

function check_finish_stock($stock_type_id,$pro_design_id, $pro_finish_id)
{
	$check_finish = "select stock_type_id,pro_finish_id from ".TB_PREF."opening_stock_master where stock_type_id = '$stock_type_id' && pro_finish_id = '$pro_finish_id' && pro_design_id = '$pro_design_id,'";
	$result = db_query($check_finish);
	if(mysql_num_rows($result) < 1 )
	{
		return true;
	}
	else
	{
		return false;
	}
}



function add_open_stock_category($stock_type_id, $consumble_id, $consumble_category, $pro_cat_id, $pro_range_id,$pro_design_id,$finish_code_id,$stock_quantity,$date)
{
	$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id,consumable_id,consumable_category,pro_cat_id,pro_range_id,pro_design_id,pro_finish_id,stock_qty,date)
		VALUES (" .db_escape($stock_type_id).","
		.db_escape($consumble_id).","
		.db_escape($consumble_category).","
		.db_escape($pro_cat_id).","
		.db_escape($pro_range_id).","
		.db_escape($pro_design_id).","
		.db_escape($finish_code_id).","
		.db_escape($stock_quantity).","
		.db_escape($date).")";
        
   	   db_query($sql,"could not add new stock");
	
}


function update_open_stock_category($selected_id,$stock_type_id, $consumble_id, $consumble_category, $pro_cat_id,				          $pro_range_id,$pro_design_id,$finish_code_id,$stock_quantity,$date){
	$sql = "UPDATE ".TB_PREF."opening_stock_master SET stock_type_id=".db_escape($stock_type_id)
	  .",consumable_id=".db_escape($consumble_id)
	  .",consumable_category=".db_escape($consumble_category)
	  .",pro_cat_id=".db_escape($pro_cat_id)
	  .",pro_range_id=".db_escape($pro_range_id)
	  .",pro_design_id=".db_escape($pro_design_id)
	  .",pro_finish_id=".db_escape($finish_code_id)
	  .",stock_qty=".db_escape($stock_quantity)
	  .",date=".db_escape($date)
    	."WHERE open_stock_id=".db_escape($selected_id);
    	db_query($sql, "could not update opening stock master");
	
}

function delete_opening_stock($selected_id)
{
	$sql = "DELETE FROM ".TB_PREF."opening_stock_master WHERE open_stock_id=".db_escape($selected_id);
	db_query($sql, "could not delete opening stock master");
}

function get_available_quntity($sub_id){
	$sql="SELECT sum(quantity) FROM ".TB_PREF."opening_master_stock WHERE sub_master_id=".db_escape($sub_id)." AND type = 'cr'";
	$total_cr = db_query($sql,"could not add new stock");
	echo $total_cr;
	return $total_cr;
}




function get_stock_type($stock_type_id) {
  $sql = "SELECT stock_name FROM ".TB_PREF."stock_type where stock_type_id = ".db_escape($stock_type_id);
    $result =  db_query($sql, "could not get stock type");
	return db_fetch($result);
}


function get_consumable_name($consumable_id)
{
	$sql = "SELECT master_id, master_name, inactive, master_reference FROM ".TB_PREF."master_creation where master_id= ".db_escape($consumable_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);	
}

function get_finish_product_name($pro_finish_id)
{
	$sql = "SELECT finish_comp_code FROM ".TB_PREF."finish_product where finish_pro_id= ".db_escape($pro_finish_id);
	$result = db_query($sql,"Could not get finish name");
	return db_fetch($result);	
}

function get_design_product_name($pro_design_id)
{
	$sql = "SELECT design_code FROM ".TB_PREF."design_code where design_id= ".db_escape($pro_design_id);
	$result = db_query($sql,"Could not get design code");
	return db_fetch($result);	
}

function get_opening_stock_master($stock_id){
	$sql = "SELECT * FROM ".TB_PREF."opening_stock_master where open_stock_id= ".db_escape($stock_id);
	$result = db_query($sql,"Could not get consumable name");
	return db_fetch($result);
}



function get_opening_stock_search($all_items)
{
	global $order_number;
	 $sql ='Select op.open_stock_id, s.stock_name, CASE WHEN op.consumable_id = -1 THEN "N/A" ELSE m.master_name END as ConsNAME,
	 CASE WHEN op.pro_design_id = -1 THEN "N/A" ELSE d.design_code END as DesignNAME, CASE WHEN op.pro_finish_id = -1 THEN "N/A" ELSE f.finish_comp_code END as FinshNAME, op.stock_qty, op.date from '.TB_PREF.'opening_stock_master op Left Join '.TB_PREF.'finish_product f on f.finish_pro_id =op.pro_finish_id LEFT Join '.TB_PREF.'design_code d on op.pro_design_id = d.design_id Left Join '.TB_PREF.'master_creation m on op.consumable_id = m.master_id Left Join '.TB_PREF.'stock_type s on op.stock_type_id = s.stock_type_id order by op.open_stock_id';

	return $sql;
}



function get_location_details()
{
	$sql = "SELECT  location_id, location_name,work_center_id from ".TB_PREF."location_master";
	/*$sql = "SELECT stock.loc_code, stock.location_name, "
	.db_escape($stock_id)." as stock_id, reorders.reorder_level
		FROM ".TB_PREF."location_master stock LEFT JOIN ".TB_PREF."loc_stock reorders ON
		reorders.loc_code=stock.loc_code
		AND reorders.stock_id = ".db_escape($stock_id)
		." ORDER BY reorders.loc_code";*/
	return db_query($sql,"an item reorder could not be retreived");
}

function  get_work_center_details($id)
{
	$sql = "SELECT  work_center_name,pro_team_id from ".TB_PREF."work_center where work_center_id = '$id'";
	$result = db_query($sql);
	return db_fetch($result);
}

function get_pro_team($id)
{
	$sql = "SELECT pro_team_name from ".TB_PREF."production_team where pro_team_id = '$id'";
	$result = db_query($sql);
	return db_fetch($result);
}

function get_stock_quantity($location_id,$c_id,$id,$consumable_id,$consumble_category)
{
	
	$sql = "SELECT  adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id where adj_rel.consumable_id = '$consumable_id' AND adj_rel.consumable_category = '$consumble_category' AND  adj.location_id = '$location_id' AND adj.work_center_id = '$c_id' AND adj.pro_team_id = '$id'";
	
	$result = db_query($sql);
	return db_fetch($result);
}

function get_product_stock_quantity($location_id,$c_id,$id,$design_id,$finish_code_id)
{
	
	$sql = "SELECT  adj_rel.quantity from  0_item_adjustment_master adj left join 0_item_adjustment_rel adj_rel on adj_rel.adj_id = adj.adj_id where AND adj_rel.design_id = '$design_id' AND adj_rel.pro_finish_code_id = '$finish_code_id' AND adj.location_id = '$location_id' AND adj.work_center_id = '$c_id' AND adj.pro_team_id = '$id'";
	
	
	$result = db_query($sql);
	return db_fetch($result);
}


function get_product_opening_stock_qty($location_id,$c_id,$design_id,$finish_code_id)
{
	
	$sql = "SELECT  * from ".TB_PREF."opening_stock_master where location_id = '$location_id' AND work_center_id = '$c_id' AND pro_design_id = '$design_id' AND pro_finish_id = '$finish_code_id'";
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_opening_stock_qty($location_id, $c_id,$consumable_id, $consumable_category)
{
	
	$sql = "SELECT  * from ".TB_PREF."opening_stock_master where location_id = '$location_id' AND work_center_id = '$c_id'  AND consumable_id = '$consumable_id' AND consumable_category = '$consumable_category'";
	$result = db_query($sql);
	return db_fetch($result);
}


?>