<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_master($master_name, $master_description, $master_reference)
{
	
		$sql = "INSERT INTO ".TB_PREF."master_creation(master_name, master_description, master_reference)
		VALUES (" . db_escape($master_name) . ", " .
		db_escape($master_description). ", " . db_escape($master_reference) . ")";
		
        
   	   db_query($sql,"could not add new master");
	  
}

//--------------------------------------------------------------------------------------------------

function update_master($id, $master_name, $master_description, $master_reference)
{
    	$sql = "UPDATE ".TB_PREF."master_creation SET master_name=".db_escape($master_name)
    	.",master_description=".db_escape($master_description)
    	.",master_reference=".db_escape($master_reference)
    	."WHERE master_id=".db_escape($id);
    	db_query($sql, "could not update master");
}
//--------------------------------------------------------------------------------------------------

function delete_master($id)
{
	$sql = "DELETE FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($id);

	db_query($sql, "could not delete master");
}
//--------------------------------------------------------------------------------------------------

/*function check_range_used($id) {
	$sql = "SELECT count(*) FROM ".TB_PREF."item_range WHERE id=".(int)$id;
	$ret = db_query($sql, 'cannot check reference usage');
	$row = db_fetch($ret);
	return $row[0];
}*/

function get_all_master($all=false) {
    $sql = "SELECT * FROM ".TB_PREF."master_creation";
	if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get master");
}

function get_master($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($selected_id);
	$result = db_query($sql,"master could not be retrieved");
	return db_fetch($result);
}

function check_master($master_reference)
{
	 $sql = 'SELECT t.reference, cat.cat_reference, m.master_reference FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat , '.TB_PREF.'master_creation m WHERE t.reference = '.db_escape($master_reference).'
	 OR cat.cat_reference = '.db_escape($master_reference).' OR m.master_reference = '.db_escape($master_reference).'';
	 $result = db_query($sql);
	 return db_fetch($result);
}
function check_update_master($master_reference)
{
	 $sql = 'SELECT t.reference, cat.cat_reference FROM '.TB_PREF.'item_range t, '.TB_PREF.'item_category cat WHERE t.reference = '.db_escape($master_reference).' OR cat.cat_reference = '.db_escape($master_reference).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}
/*function check_master($master_name, $master_reference)
{
	 $sql = "SELECT * FROM  ".TB_PREF."master_creation WHERE master_name= ".db_escape($master_name)." OR master_reference=".db_escape($master_reference)." "; 
	 $result = db_query($sql);
	 return db_fetch($result);
}
*/

/*function item_range_used($range) {
	$sql= "SELECT COUNT(*) FROM ".TB_PREF."stock_master WHERE range=".db_escape($range);
	$result = db_query($sql, "could not query stock master");
	$myrow = db_fetch_row($result);
	return ($myrow[0] > 0);
}
*/



?>