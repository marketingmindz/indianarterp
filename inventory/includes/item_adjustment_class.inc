<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



class item_adjustment 
{

	var $line_items = array();
	var $line_con = array();
	var $line_no;
	var $category_id;
	var $collection_id;
	var $range_id;
	var $design_id;
	var $finish_code_id;
	var $pro_unit;
	var $pro_qty;
	var $adj_id;
     
	 var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	
    var $cons_detail_rec;
	var $pro_details_rec;
	   
	var $lines_on_pro = 0;
	var $lines_on_con = 0;
	//*****************************************************
	function item_adjustment()
	{
		$this->line_items = array();
		$this->line_con = array();
		$this->lines_on_pro =0;
		$this->lines_on_con = 0;
	}
	
	//********************************************************


   
   
   function add_to_cons_part($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity)
	{
		if (isset($cons_quntity) && $cons_quntity != 0)
		{
			$this->line_con[$line_no] = new con_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity);
			$this->lines_on_con++;
			return 1;
		}
		return 0;
	}
   
    function update_cons_item($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity)
	{
		
		$this->line_con[$line_no]->cons_type = $cons_type;
		$this->line_con[$line_no]->cons_select = $cons_select;
		$this->line_con[$line_no]->cons_unit = $cons_unit;
		$this->line_con[$line_no]->cons_quntity = $cons_quntity;
		
	}
	//**************** ***************
	
	//-------------------- product ------------
		function add_to_product_part($line_no, $category_id, $collection_id, $range_id, $design_id, $finish_code_id, $pro_unit, $pro_qty)
	{
		if (isset($pro_qty) && $pro_qty != 0)
		{
			$line_no += 100; 
			$this->line_items[$line_no] = new pro_line_details($line_no, $category_id, $collection_id, $range_id, $design_id, $finish_code_id, $pro_unit, $pro_qty);
			$this->lines_on_pro++;
			return 1;
		}
		return 0;
	}
	

   
    function update_product_item($line_no, $category_id, $collection_id, $range_id, $design_id, $finish_code_id, $pro_unit, $pro_qty)
	{
		
		if($line_no < 100){ 
		   $line_no += 100; 
		}
		$this->line_items[$line_no]->category_id = $category_id;
		$this->line_items[$line_no]->collection_id = $collection_id;
		$this->line_items[$line_no]->range_id = $range_id;
		$this->line_items[$line_no]->design_id = $design_id;
		$this->line_items[$line_no]->finish_code_id = $finish_code_id;
		$this->line_items[$line_no]->pro_unit = $pro_unit;
		$this->line_items[$line_no]->pro_qty = $pro_qty;
		
	}
	
	
	//-------------------- end product ----------------
	function order_abs_has_items() 
	{
		return count($this->line_items) != 0;
	}
	function order_con_has_items() 
	{
		return count($this->line_con) != 0;
	}
	
	function clear_abs_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		
		$this->lines_on_pro = 0;  
		$this->adj_id = 0;
	}
	function clear_con_items() 
	{
    	unset($this->line_con);
		$this->line_con = array();
		
		$this->lines_on_con = 0;  
		$this->adj_id = 0;
	}
	//***********************************
   
} /* end of class defintion */

class pro_line_details 
{

	var $line_no;
	var $pro_detail_rec;
	var $category_id;
	var $collection_id;
	var $rang_id;
	var $design_id;
	var $finish_code_id;
	var $pro_unit;
	var $pro_qty;
	

	function pro_line_details($line_no, $category_id, $collection_id ,$range_id, $design_id, $finish_code_id, $pro_unit, $pro_qty)
	{

		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->category_id = $category_id;

		$this->collection_id = $collection_id;
		//$this->unit_id = $unit_id;
		$this->range_id = $range_id;
		$this->design_id = $design_id;
		$this->finish_code_id = $finish_code_id;
		$this->pro_unit = $pro_unit;
		$this->pro_qty = $pro_qty;
		
	}

}

class con_line_details 
{

	var $line_no;
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	

	function con_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity)
	{
		$this->line_no = $line_no;
		$this->cons_type = $cons_type;
		$this->cons_select = $cons_select;
		$this->cons_unit = $cons_unit;
		$this->cons_quntity = $cons_quntity;
		
	}

}
?>

