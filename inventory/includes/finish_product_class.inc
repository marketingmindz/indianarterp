<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/inventory/includes/ui/finish_product_db.inc");


class finish_order 
{

	var $line_items = array();
	var $line_con = array();
	var $line_no;
	var $part_name;
	var $finish_pro_id;
	var $fabric_name;
	var $percentage;  
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
     
   
	var $lines_on_fab = 0;
	var $lines_on_fcon = 0;
	//*****************************************************
	function finish_order()
	{
		$this->line_items = array();
		$this->line_con = array();
		$this->lines_on_fab = $this->finish_pro_id = 0;
		$this->lines_on_fcon = $this->finish_pro_id = 0;
	}
	
	//********************************************************
	
	function add_to_fab_part($line_no, $part_name, $fabric_name, $percentage)
	{
		if (isset($percentage) && $percentage != 0)
		{
			$this->line_items[$line_no] = new fab_line_details($line_no, $part_name, $fabric_name, $percentage);
			$this->lines_on_fab++;
			return 1;
		}
		return 0;
	}
   
    function update_fab_item($line_no, $part_name, $fabric_name, $percentage)
	{
		$this->line_items[$line_no]->part_name = $part_name;
		$this->line_items[$line_no]->fabric_name = $fabric_name;
		$this->line_items[$line_no]->percentage = $percentage;
	}
   
   
   function add_to_fcon_part($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{
		if (isset($cons_quntity) && $cons_quntity != 0)
		{
			$line_no += 100; 
			$this->line_con[$line_no] = new fcon_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm);
			$this->lines_on_fcon++;
			return 1;
		}
		return 0;
	}
   
    function update_fcon_item($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{
		if($line_no < 100){ 
		   $line_no += 100; 
		}
		$this->line_con[$line_no]->cons_type = $cons_type;
		$this->line_con[$line_no]->cons_select = $cons_select;
		$this->line_con[$line_no]->cons_unit = $cons_unit;
		$this->line_con[$line_no]->cons_quntity = $cons_quntity;
		$this->line_con[$line_no]->cons_iscbm = $cons_iscbm;
	}
	//**************** ***************
	function order_fab_has_items() 
	{
		return count($this->line_items) != 0;
	}
	function order_fcon_has_items() 
	{
		return count($this->line_con) != 0;
	}
	
	function clear_fab_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		
		$this->lines_on_fab = 0;  
		$this->finish_pro_id = 0;
	}
	function clear_fcon_items() 
	{
    	unset($this->line_con);
		$this->line_con = array();
		
		$this->lines_on_fcon = 0;  
		$this->finish_pro_id = 0;
	}
	//***********************************
   
} /* end of class defintion */

class fab_line_details 
{

	var $line_no;
	var $part_name;
	var $fabric_name;
	var $fab_detail_rec;
	var $percentage;
	

	function fab_line_details($line_no, $part_name, $fabric_name, $percentage)
	{

		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->part_name = $part_name;
		$this->fabric_name = $fabric_name;
		$this->percentage = $percentage;
	
	}

}

class fcon_line_details 
{

	var $line_no;
	var $fcon_detail_rec;
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	var $con_iscbm;

	function fcon_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{

		
		$this->line_no = $line_no;
		$this->cons_type = $cons_type;
		$this->cons_select = $cons_select;
		$this->cons_unit = $cons_unit;
		$this->cons_quntity = $cons_quntity;
		$this->cons_iscbm = $cons_iscbm;
		
	}

}
?>

