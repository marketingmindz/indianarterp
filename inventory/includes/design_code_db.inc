<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/inventory/includes/db/design_code.inc");
include_once($path_to_root . "/inventory/includes/db/assemble_db.inc");

class design_order 
{

	var $line_items = array();
	var $line_con = array();
	var $line_no;
	var $part_name;
	var $part_catogery;
	var $part_collection;
	var $part_range;
	var $old_design;
	var $design_id;
	var $width;
	var $unit_id;
	var $density;
	var $height;
	var $iscbm;
	var $asb_code;
	var $qty;
	var $n_wt;
	var $img_name;
	var $assembleImg;//shubham code
     
	 var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
     
   
	var $lines_on_asb = 0;
	var $lines_on_con = 0;
	//*****************************************************
	function design_order()
	{
		$this->line_items = array();
		$this->line_con = array();
		$this->lines_on_asb = $this->design_id = 0;
		$this->lines_on_con = $this->design_id = 0;
	}
	
	//********************************************************
	function add_to_asb_part($line_no, $part_category, $part_collection, $part_range, $old_design, $asb_code, $part_name, $width, $density, $height, $iscbm, $qty, $n_wt, $img_name,$assembleImg)//shubham code
	{
		if (isset($qty) && $qty != 0)
		{
			$this->line_items[$line_no] = new asb_line_details($line_no, $part_category, $part_collection, $part_range, $old_design,  $asb_code, $part_name, $width, $density, $height, $iscbm, $qty, $n_wt, $img_name,$assembleImg);//shubham code
			$this->lines_on_asb++;
			return 1;
		}
		return 0;
	}
	
	
   
    function update_abs_item($line_no, $part_category, $part_collection, $part_range, $old_design, $asb_code, $part_name, $width, $density, $height, $iscbm, $qty, $n_wt, $img_name,$assembleImg)//shubham code
	{
		$this->line_items[$line_no]->part_category = $part_category;
		$this->line_items[$line_no]->part_collection = $part_collection;
		$this->line_items[$line_no]->part_range = $part_range;
		$this->line_items[$line_no]->old_design = $old_design;
		$this->line_items[$line_no]->asb_code = $asb_code;
		$this->line_items[$line_no]->part_name = $part_name;
		$this->line_items[$line_no]->width = $width;
		$this->line_items[$line_no]->density = $density;
		$this->line_items[$line_no]->height = $height;
		$this->line_items[$line_no]->iscbm = $iscbm;
		$this->line_items[$line_no]->qty = $qty;
		$this->line_items[$line_no]->n_wt = $n_wt;
		$this->line_items[$line_no]->img_name = $img_name;
		$this->line_items[$line_no]->assembleImg = $assembleImg;//shubham code
	}
   
   
   function add_to_con_part($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{
		if (isset($cons_quntity) && $cons_quntity != 0)
		{
			$line_no += 100; 
			$this->line_con[$line_no] = new con_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm);
			$this->lines_on_con++;
			return 1;
		}
		return 0;
	}
   
    function update_con_item($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{
		if($line_no < 100){ 
		   $line_no += 100; 
		}
		$this->line_con[$line_no]->cons_type = $cons_type;
		$this->line_con[$line_no]->cons_select = $cons_select;
		$this->line_con[$line_no]->cons_unit = $cons_unit;
		$this->line_con[$line_no]->cons_quntity = $cons_quntity;
		$this->line_con[$line_no]->cons_iscbm = $cons_iscbm;
	}
	//**************** ***************
	function order_abs_has_items() 
	{
		return count($this->line_items) != 0;
	}
	function order_con_has_items() 
	{
		return count($this->line_con) != 0;
	}
	
	function clear_abs_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		
		$this->lines_on_asb = 0;  
		$this->design_id = 0;
	}
	function clear_con_items() 
	{
    	unset($this->line_con);
		$this->line_con = array();
		
		$this->lines_on_con = 0;  
		$this->design_id = 0;
	}
	//***********************************
   
} /* end of class defintion */

class asb_line_details 
{

	var $line_no;
	var $part_category;
	var $part_collection;
	var $part_range;
	var $old_design;
	var $part_name;
	var $width;
	var $asb_detail_rec;
	var $unit_id;
	var $density;
	var $height;
	var $iscbm;
	var $asb_code;
	var $qty;
	var $n_wt;
	var $img_name;
	var $assembleImg;//shubham code

	function asb_line_details($line_no, $part_category, $part_collection, $part_range, $old_design, $asb_code, $part_name, $width, $density, $height, $iscbm, $qty, $n_wt, $img_name,$assembleImg)//shubham code
	{

		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->part_category = $part_category;
		$this->part_collection = $part_collection;
		$this->part_range = $part_range;
		$this->old_design = $old_design;
		$this->asb_code = $asb_code;
		$this->part_name = $part_name;
		$this->width = $width;
		//$this->unit_id = $unit_id;
		$this->density = $density;
		$this->height = $height;
		$this->iscbm = $iscbm;
		
		$this->qty = $qty;
		$this->n_wt = $n_wt;
		$this->img_name = $img_name;
		$this->assembleImg = $assembleImg;//shubham code
	}

}

class con_line_details 
{

	var $line_no;
	var $con_detail_rec;
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	var $con_iscbm;

	function con_line_details($line_no, $cons_type, $cons_select, $cons_unit, $cons_quntity, $cons_iscbm)
	{

		
		$this->line_no = $line_no;
		$this->cons_type = $cons_type;

		$this->cons_select = $cons_select;
		$this->cons_unit = $cons_unit;
		$this->cons_quntity = $cons_quntity;
		$this->cons_iscbm = $cons_iscbm;
		
	}

}
?>

