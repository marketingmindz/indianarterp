<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//---------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
function display_design_hearder(&$order){
	
		div_start('details');
		/** detail **/
		
		start_outer_table(TABLESTYLE2, "id='des_tab'");
		
		table_section(1);
		table_section_title(_("Design Code"));
		global $selected_id;
		if ($selected_id != -1) 
		{
			/*if ($Mode == 'Edit') {
				
			} else{
				$myrow = get_design_header($selected_id);
				
				$_POST['design_id'] = $myrow["design_id"];
				$_POST['category_id']  = $myrow["category_id"];
				$_POST['collection_id']  = $myrow["collection_id"];
				$_POST['range_id']  = $myrow["range_id"];
				$_POST['product_name']  = $myrow["product_name"];
				$_POST['pro_id']  = $myrow["product_type"];
				$_POST['width_design']  = $myrow["weight"];
				$_POST['height_design']  = $myrow["height"];
				$_POST['density_design']  = $myrow["density"];
				$_POST['design_code']  = $myrow["design_code"];
				$_POST['description']  = $myrow["description"];
				
			}
			hidden('selected_id', $selected_id);
			hidden('design_id');*/
			
		} 

		design_category_list_row(_("Category:"), 'category_id', null, _('----Select---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){
		
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		text_row(_("Product Name:"), 'product_name', null, 30, 30, '','','','id="product_name"');
		design_product_type_list_row(_("Product Type:"), 'pro_id', null);
		unit_design_list_row(_("Unit"), 'pkg_unit', null, true, "- Select Unit -");
		text_row_three(_("Product Size"), 'width_design','density_design','height_design', null, null, null,'width_design','density_design','height_design');
		text_row_three(_("Packing Size"), 'pkg_w','pkg_d','pkg_h', null, null, null,'pkg_w','pkg_d','pkg_h');
		text_row(_("Product CBM:"), 'pro_cbm', null, 30, 30, '','','','id="pro_cbm" onclick="generateProductCBM()"'); 
		text_row(_("Packing CBM:"), 'pak_cbm', null, 30, 30, '','','','id="pak_cbm" onclick="generatePackagingCBM()"'); 
		text_row(_("Code:"), 'design_code', null, 30, 30, '','','','id="design_code"');
		button_code_generate( _(""),'generate_code', 'Generate Code','','','','','','id="generate_code"');
		textarea_row(_('Description:'), 'description', null, 42, 3);
		table_section(2, '', 'id=finish_pro_img_tbl');
		table_section_title(_("Design Images"));
		
		
		
		/************* image code ********/
		
		if(isset($_POST['design_id'])){
			$row_img = get_design_image($_POST['design_id']);
			$_SESSION['design_photo'] = $imge = $row_img['design_image'];
			$deg_img_arry = explode(',', $imge);
			$img_count=0;
			foreach($deg_img_arry as $pro_img){
				$img_count = $img_count+1;
				if($img_count == 1){
					file_row(_("Design Image  ".$img_count) . ":", 'pic_'.$img_count, 'pic');
				}else{
					file_row(_("Design Image  ".$img_count) . ":", 'pic_'.$img_count, 'pic'.$img_count, 'pic' , $img_count);	
				}
				$deg1_img_link = "";
				$check_remove_image1 = false;
				if (isset($_POST['design_id'])) 
				{
					if($pro_img !=''){
						$deg1_img_link= "<input type='hidden' id='get_img".$img_count."' name='get_img' value='".$pro_img."' ><a href='".company_path().'/desingImage/'.$pro_img."' target='_blank'><img id='blah".$img_count."' src='".company_path().'/desingImage/'.$pro_img."' alt='your image' width='80' height='40'/>
						</a><img id='".$img_count."' src='".company_path().'/images/del.png'."' class='del_imge' >";
						$check_remove_image1 = true;
					}else{
						$deg1_img_link .= "<input type='hidden' id='get_img".$img_count."' name='get_img'  ><img id='blah".$img_count."' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='90' height='60' />";
					}
				} 
				else 
				{
					$deg1_img_link .= "<input type='hidden' id='get_img".$img_count."' name='get_img' ><img id='blah".$img_count."' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='90' height='60' />";
				}
		
				label_row("&nbsp;", $deg1_img_link);
			}
			
			if (isset($_POST['design_id'])) 
				{
					if($pro_img !=''){
						$img_count = $img_count+1;
			file_row(_("Design Image  ".$img_count) . ":", 'pic_'.$img_count, 'pic'.$img_count, 'pic' , $img_count);
			$not_image = "<img id='blah".$img_count."' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='90' height='60' />";
			label_row("&nbsp;", $not_image);
					}
					
				}
				echo "<tr style='' ><td><input type='hidden' name='no_finish_pro_img' id='no_finish_pro_img' value='".$img_count."' ></td> </tr>";
		}else{
			file_row(_("Design Image 1 ") . ":", 'pic_1', 'pic');
		$deg1_img_link = "";
		$check_remove_image1 = false;
		if (isset($_POST['design_id'])) 
		{
			$deg1_img_link= "<input type='hidden' id='get_img1' name='get_img' value='".$pro_img."' > <img id='blah1' src='".company_path().'/desingImage/'.$pro_img."' alt='your image' width='80' height='40' />";
			$check_remove_image1 = true;
		} 
		else 
		{
			$deg1_img_link .= "<input type='hidden' id='get_img1' name='get_img' ><img id='blah1' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='90' height='60' />";
		}

		label_row("&nbsp;", $deg1_img_link);
		
		echo "<tr style='' ><td><input type='hidden' name='no_finish_pro_img' id='no_finish_pro_img' value='1' ></td> </tr>";
		}
		

		end_outer_table(1);
		div_end();
}
function display_assemble_summary(&$order, $editable=true)
{

	display_heading("Assemble Part");

    div_start('items_table');
	start_table(TABLESTYLE, "colspan=7 id='des_tab'");
	$th = array(_("Category"),_("Collection"),_("Range"),_("isCode"),_("Code"),_("Product Name"), _("W"), _("D"), _("H"), _("IsCBM"), _("Qty"), _("Net Weight"),_("Image"), "");
	
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
	$id = find_submit('Edit');
    $k = 0;
	foreach ($order->line_items as $line_no => $asb_line)
   	{
    	if (!$editable || ($id != $line_no))
		{
    		alt_table_row_color($k);
    		$part_category = $asb_line->part_category;
    		$get_part_category = get_category_type_name($part_category);
    		$part_cat = $get_part_category['cat_name'];

    		label_cell($part_cat);
    		label_cell($asb_line->part_collection);
    		$part_range = $asb_line->part_range;
    		$get_part_range = get_range_type_name($part_range);
    		$part_ran = $get_part_range['range_name'];
    		$ab_code = $asb_line->asb_code.",";
    		label_cell($part_ran);
    		//label_cell($a_code);
    		label_cell($asb_line->old_design);
    		label_cell($asb_line->asb_code);
        	label_cell($asb_line->part_name);
    		label_cell($asb_line->width);
			label_cell($asb_line->density);
    		label_cell($asb_line->height);
    		label_cell($asb_line->iscbm);
    		label_cell($asb_line->qty);
    		label_cell($asb_line->n_wt);//shubham code
			if(isset($_GET['ModifyDesignNumber'])) {
    			label_cell("<img src='../../company/0/assembleImg/".$asb_line->img_name."' width='50' height='30' />");
			

    		}else{
    			label_cell("<img src='".$asb_line->assembleImg."' width='50' height='30' />");
			
    		}
			
			if ($editable)
			{
					edit_button_cell("Edit$line_no", _("Edit"),
					  _('Edit document line'));
					delete_button_cell("Delete$line_no", _("Delete"),
						_('Remove line from document'));
			}
		end_row();
		}
		else
		{
			design_assemble_item_controls($order, $k, $line_no);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_assemble_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
}

function design_assemble_item_controls(&$order, &$rowcounter, $line_no=-1)
{
	global $Ajax;
	start_row();
	//alt_table_row_color($rowcounter);
	$id = find_submit('Edit');

	if (($id != -1) && $line_no == $id)
	{
		hidden('line_no', $id);
		$_POST['part_category'] = $order->line_items[$id]->part_category;
		$_POST['part_collection'] = $order->line_items[$id]->part_collection;
		$_POST['part_range'] = $order->line_items[$id]->part_range;
		$_POST['old_design'] = $order->line_items[$id]->old_design;
		if($_POST['old_design']==1){
	
			$_POST['assemble_code_select'] =  $order->line_items[$id]->asb_code;
		}
		else{
			$_POST['assemble_code'] = $order->line_items[$id]->asb_code;
		}
		
		$_POST['part_name'] = $order->line_items[$id]->part_name;
		$_POST['width'] = $order->line_items[$id]->width;
		$_POST['density'] = $order->line_items[$id]->density;
		$_POST['height'] = $order->line_items[$id]->height;
		$_POST['iscbm'] = $order->line_items[$id]->iscbm;
		$_POST['quntity'] = $order->line_items[$id]->qty;
		$_POST['net_weight'] = $order->line_items[$id]->n_wt;
		$_POST['img_name'] = $order->line_items[$id]->img_name;

	    $Ajax->activate('items_table');
	}
	$no = count($order->line_items) - 1;
	$asb_cd = $order->line_items[$no]->asb_code;
	$asb_code_arr = explode("#",$order->line_items[$no]->asb_code);
	$asb_code = $asb_code_arr['1'];

	part_desgin_category_list_cells(null, 'part_category', null, _('----Select---'));
	sub_part_collection_code_list_cells(null,'part_collection', null);
	part_desgin_range_list_cells(null, 'part_range', null, _('----Select---'));

	ass_check_cells_ex_design(null, 'old_design','','','','','','onclick ="get_design_code_list()"' );

	echo "<input type='hidden' id='asb_code' name='asb_code' value='$asb_cd'>";
	
	if($_POST['old_design'] == '1'){
		
		echo "<td>";
			part_text_cells_ex(null, 'assemble_code', 17, 10, '','','','', false,'id="assemble_code" style="display:none;"');
			if($_POST['part_category'] == '-1' && $_POST['part_range'] == '-1'){
				part_sub_desing_code_list_cells(null, 'assemble_code_select', null, _('----Select Code---'));
			}
			else if($_POST['part_category'] != '-1' && $_POST['part_range'] == '-1'){
				part_sub_desing_code_category_list_cells(null,  $_POST['part_category'],$_POST['assemble_code_select'], null, false, true, true, true);
			}else if($_POST['part_category'] != '-1' && $_POST['part_range'] != '-1'){
				part_sub_desing_code_range_list_cells(null,  $_POST['part_range'], $_POST['part_category'],$_POST['assemble_code_select'], null, false, true, true, true);
			}	
		echo "</td>";
	}else{
		
		echo "<td>";
		
			part_text_cells_ex(null, 'assemble_code', 17, 10, '','','','', false,'id="assemble_code"');
			echo "<p id='show_cate' style='display:none;'>";
			if($_POST['part_category'] == '-1' && $_POST['part_range'] == '-1'){
				part_sub_desing_code_list_cells(null, 'assemble_code_select', null, _('----Select Code---'));
			}
			else if($_POST['part_category'] != '-1' && $_POST['part_range'] == '-1'){
				part_sub_desing_code_category_list_cells(null,  $_POST['part_category'],$_POST['assemble_code_select'], null, false, true, true, true);
			}else if($_POST['part_category'] != '-1' && $_POST['part_range'] != '-1'){
				part_sub_desing_code_range_list_cells(null,  $_POST['part_range'], $_POST['part_category'],$_POST['assemble_code_select'], null, false, true, true, true);
			}	
			echo "</p>";
		echo "</td>";
		
	}	
		
	text_cells_ex(null, 'part_name', 20, 20, '','','','', false,'id="part_name"');
	
	
	//text_cells_ex(null, 'part_name', 20, 20);
	//unit_design_list_cells(null, 'unit_id', null, true, " ");
	text_cells_ex(null, 'width', 1, 5, '','','','', false,'id="part_width"');
	text_cells_ex(null, 'density', 1, 5, '','','','',false, 'id="part_density"');
	text_cells_ex(null, 'height', 1, 5, '','','','', false,'id="part_height"');
	check_cells_ex_design(null, 'iscbm');
	
	text_cells_ex(null, 'quntity', 2, 5, '','','','');
	text_cells_ex(null, 'net_weight', 3, 5, '','','','');
	//text_cells_ex(null, 'img_name', 10, 10, '','','','', false,'id="img_name"');
	/*if($_POST['img_name'] == ''){*/
		//echo "<td> <input type='hidden' name='img_name' id='img_name' ><a class='pointer' id='img_click' style = 'cursor: pointer;'>Image </a></td>";
		echo "<td class='choose-file'> <input type='file' name='img_name' id='item_image' /></td>";//shubham code
	/*}else{
		echo "<td> <input type='hidden' name='img_name' id='img_name' value='".$_POST['img_name']."'><img src='".company_path().'/desingImage/'.$_POST['img_name']."' width='50' height='30' id='img_click' /></td>";
	}*/
	if ($id != -1)
	{
		button_cell('AssembleUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('AssembleCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
 		//set_focus('amount');
	} 
	else 
		submit_cells('AssembleAddItem', _("Add Item"), "colspan=2",
		    _('Add new line to assemble'), true);

	end_row();
}

function display_consumable_summary(&$order, $editable=true)
{

	display_heading("Consumable Part");

    div_start('con_table');
	start_table(TABLESTYLE, "colspan=7 id='des_tab'");
	$th = array(_("Type"),_("Select"), _("Quantity"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);
	
	

	$id = find_submits_consume('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{
		if($line_con < 100){ 
		   $line_con += 100; 
		}
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_cons_type_name($cons_type);
			$const_type = $get_cons_type['master_name'];
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_cons_select_name($cons_select);
			$const_select = $get_cons_select['consumable_name'];
        	label_cell($const_type);
    		label_cell($const_select);
    		//label_cell($con_line->cons_unit);
			label_cell($con_line->cons_quntity);
    		//label_cell($con_line->cons_iscbm);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_consumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_consumable_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;
	end_table(1);

    div_end();
}

function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submits_consume('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		//$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_quntity'] = $order->line_con[$id]->cons_quntity;
		//$_POST['cons_iscbm'] = $order->line_con[$id]->cons_iscbm;

	    $Ajax->activate('con_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	//sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	//unit_design_list_cells(null, 'cons_unit', null, true, " ");
	text_cells_ex(null, 'cons_quntity', 5, 5);
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
		if ($id != -1)
		{
			button_cell('ConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('ConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
			//set_focus('amount');
		} 
		else 
			submit_cells('ConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}

?>
<div id="output"></div>
    
    <div id="overlay" class="web_dialog_overlay"></div>
    
    <div id="dialog" class="web_dialog">
        
    </div>

<style type="text/css">
        .web_dialog_overlay
        {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }
        .web_dialog
        {
            display: none;
            position: fixed;
            width: 380px;
            height: 200px;
            top: 50%;
            left: 50%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }
        .web_dialog_title
        {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight:bold;
        }
        .web_dialog_title a
        {
            color: White;
            text-decoration: none;
        }
        .align_right
        {
            text-align: right;
        }
    </style>
     
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
$(function() {
	$(document).on('click', '#img_click', function() {
		$('#dialog').empty();
		$("#popupdiv").dialog({
			title: "jQuery Popup from Server Side",
			width: 600,
			height: 250,
			modal: true,
			buttons: {
				Close: function() {
					$(this).dialog('close');
				}
			}
		});
		
		
		var st = "";
		var i = 1;
		$('#finish_pro_img_tbl img[id^=blah]').each(function(index, element) {
			//console.log("sdfsdfsdf");
            st = '<img src="'+$(this).attr('src')+'" width="60" height="60" style="margin:5px"/><input type="radio" class="imageradio" id="imageradio_'+i+'" name="imageradio" data-value="'+$('#get_img'+i).attr("value")+'" data-url="'+$(this).attr('src')+'" />';			
			$('#dialog').append(st);
			i++;
        });
		
		
		return false;
	});
	
	
	$(document).on('click', '.imageradio', function(e) {
		
		var $parent = $('#img_click').parent();
		var img_name = $(this).attr('data-value');
		
		$parent.append('<img src="'+$(this).data('url')+'" width="50" height="30" id="img_click" />');
		$('#img_name').val(img_name);
		$('#img_click').remove();
		
    });
	
	
})	

	
	</script>
    
<script type="text/javascript">
	$('body').on('click','#img_click', function(){
		 ShowDialog(false);
         e.preventDefault();
	});
	$('body').on('click','#btnClose', function(){
				 HideDialog();
                e.preventDefault();
	});
	$(document).ready(function ()
	{
		$("#img_click").click(function (e)
		{
		   ShowDialog(false);
           e.preventDefault();
		});

	   
		$("#btnClose").click(function (e)
		{
			HideDialog();
			e.preventDefault();
		});

	});

	function ShowDialog(modal)
	{
		$("#overlay").show();
		$("#dialog").fadeIn(300);

		if (modal)
		{
			$("#overlay").unbind("click");
		}
		else
		{
			$("#overlay").click(function (e)
			{
				HideDialog();
			});
		}
	}

	function HideDialog()
	{
		$("#overlay").hide();
		$("#dialog").fadeOut(300);
	} 
        
    </script>