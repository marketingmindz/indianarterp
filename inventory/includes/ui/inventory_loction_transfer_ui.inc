<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//-------------------------------------------------------------------------------------
function display_inventory_transfer_header(&$order){
	div_start('details');
		/** detail **/
		global $Ajax;
		$Ajax->activate('production_team');

		start_outer_table(TABLESTYLE2, "style=min-width:320px;width:80%;");
		
		table_section(1);
		design_location_list_row(_("From Location:"), 'from_Location_id', null, _('------Select Location-----'));
		
		design_work_center_list_row(_("From Work Center:"), 'from_work_center_id', null, _('-----Select Work Center----'));
		
		design_to_location_list_row(_("To Location:"), 'to_Location_id', null, _('-----Select Location----'));
		
		design_to_work_center_list_row(_("To Work Center:"), 'to_work_center_id', null, _('-----Select Work Center----'));
		
		
		$_POST['reference_no'] = get_reference_no();
		
		table_section(2);
			text_row(_("Reference No:"), 'reference_no', null, 22, 20, '','','','id="reference_id"');
			
			date_row(_("Date:"), 'date');
		
		table_section(3);
		
		stock_type_list_row(_("Stock Type:"), 'stock_type_id', null);
		//design_type_list_row(_("Type:"), 'type_id', null, _('-----Select Type----'));
		check_row("Assign Direct to Production Team","directToPT",null,false,"Transfer Items Direct to Production Team","directToPT");
		echo "<tr><td>Production Team:</td><td>";	 
			echo "<select name='production_team' id='production_team'><option value='-1'>- Select Production Team -</option></select>";
		echo "</td></tr>";
		end_outer_table(1);
		
		
}



//  consumable part //

function display_consumable_summary(&$order, $editable=true)
{

	if(isset($_POST['stock_type_id']))
	{
		if($_POST['stock_type_id'] == '102')
		{
			$css = 'style="display:none;"';
		}
		else
		{
			$css = 'style="display:block;"';
		}
	}

    div_start('cons_table',$css);
	display_heading("Consumable Part");
	start_table(TABLESTYLE, "colspan=7 style=min-width:320px;width:80%;");
	$th = array(_("Type"),_("Select"), _("Quantity of <br> From Work Center"), _("To Work Center "), _("Unit"), _("Quantity"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);
	
	

	$id = find_submits_consume('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_cons_type_name($cons_type);
			$const_type = $get_cons_type['master_name'];
			
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_cons_select_name($cons_select);
			$brand = $get_cons_select['company_name'] == "" ? "No Brand" : $get_cons_select['company_name'];
			
			$const_select = $get_cons_select['consumable_name']." - ".$brand." - ".$get_cons_select['consumable_code'];

        	label_cell($const_type);
    		label_cell($const_select);
    		label_cell($con_line->from_quantity);
			label_cell($con_line->to_quantity);
			label_cell($con_line->cons_unit);
			label_cell($con_line->cons_quntity);
			
    		//label_cell($con_line->cons_iscbm);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_consumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_consumable_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;
	end_table(1);

    div_end();
}




function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submits_consume('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_quntity'] = $order->line_con[$id]->cons_quntity;
		$_POST['from_quantity'] = $order->line_con[$id]->from_quantity;
		$_POST['to_quantity'] = $order->line_con[$id]->to_quantity;

	    $Ajax->activate('cons_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	
	text_cells_ex(null, 'from_quantity', 20, 20,null,null,null,null,null,"id=from_quantity");
	text_cells_ex(null, 'to_quantity', 20, 20,null,null,null,null,null,"id=to_quantity");
	
	if($_POST['cons_select'] == '-1')
	{
		unit_list_cells(null, 'cons_unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['cons_select'], 'cons_unit', null, false, true, true, true);
	}
	text_cells_ex(null, 'cons_quntity', 5, 5);
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
		if ($id != -1)
		{
			button_cell('ConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('ConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
			//set_focus('amount');
		} 
		else 
			submit_cells('ConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}


//--------------- product 

function display_product_summary(&$order, $editable=true)
{

	//  code by bajrang   17/07/2015
	if(isset($_POST['stock_type_id']))
	{
		if($_POST['stock_type_id'] == '101')
		{
			$css = 'style="display:none;"';
		}
		else
		{
			$css = 'style="display:block;"';
		}
	}

    div_start('pro_table',$css);
	display_heading("Product Part");
	start_table(TABLESTYLE, "colspan=7 style=min-width:320px;width:80%;");
	$th = array(_("Product Catogory"),_("Product Collection"), _("Product Range"), _("Product Design Code"),  _("Product Finish Code"),_("Product Unit"),_("Product Qty"), "");
	
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
	$id = find_submit('Edit');
    $k = 0;
	foreach ($order->line_items as $line_no => $asb_line)
   	{
		
		if($line_no < 100){ 
		   $line_no += 100; 
		}
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_no))
		{
    		alt_table_row_color($k);
			
			$category_id = $asb_line->category_id;
			$get_category_type = get_cons_category_name($category_id);
			$cat_name = $get_category_type['cat_name'];
			
			$range_id=$asb_line->range_id;
			if($range_id=='-1')
			{
				$range_name='N/A';
			}else
			{
				$get_range_type = get_cons_range_name($range_id);
				$range_name = $get_range_type['range_name'];
				
			}
			
			$design_id = $asb_line->design_id;
			if($design_id=='-1')
			{
				$design_code='N/A';
			}else
			{
				$get_design_type = get_cons_design_code($design_id);
				$design_code = $get_design_type['design_code'];
			}
			
			$finish_code_id = $asb_line->finish_code_id;
			if($finish_code_id=='-1')
			{
				$finish_code='N/A';
			}else
			{
				$get_finish_type = get_cons_fihish_code($finish_code_id);
				$finish_code = $get_finish_type['finish_comp_code'];
			}
			
        	label_cell($cat_name);
    		label_cell($asb_line->collection_id);
    		label_cell($range_name);
			label_cell($design_code);
    		label_cell($finish_code);
    		label_cell($asb_line->pro_unit);
			label_cell($asb_line->pro_qty);
    		
			if ($editable)
			{
					edit_button_cell("Edit$line_no", _("Edit"),
					  _('Edit document line'));
					delete_button_cell("Delete$line_no", _("Delete"),
						_('Remove line from document'));
			}
		end_row();
		}
		else
		{
			product_item_controls($order, $k, $line_no);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		product_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
}

function product_item_controls(&$order, &$rowcounter, $line_no=-1)
{
	global $Ajax;
	start_row();
	//alt_table_row_color($rowcounter);
	$id = find_submit('Edit');
	if (($id != -1) && $line_no == $id)
	{
		hidden('line_no', $id);
		
		//$_POST['unit_id'] = $order->line_items[$id]->unit_id;
		$_POST['category_id'] = $order->line_items[$id]->category_id;
		
		$_POST['collection_id'] = $order->line_items[$id]->collection_id;
		$_POST['range_id'] = $order->line_items[$id]->range_id;
		$_POST['design_id'] = $order->line_items[$id]->design_id;
		
		$_POST['finish_code_id'] = $order->line_items[$id]->finish_code_id;
		$_POST['pro_unit'] = $order->line_items[$id]->pro_unit;
		$_POST['pro_qty'] = $order->line_items[$id]->pro_qty;
		

	    $Ajax->activate('pro_table');
	}
		desgin_category_list_cells(null, 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_cells(null, 'collection_id', null, false, true, 'id=collection_id_row');
		desgin_range_list_cells(null, 'range_id', null, _('----Select---'));
		
		if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
			sub_desing_code_list_cells(null, 'design_id', null, _('----Select Code---'), false, 'id=pro_design_id_row');
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
			desgin_finish_code_list_row(null, $_POST['category_id'],$_POST['design_id'], null, false, true, true, true, true, 'id=pro_design_id_row');
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
			desgin_finish_range_code_list_row(null, $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true,
			true, 'id=pro_design_id_row' );
		}	
		if($_POST['design_id'] == '-1'){
			sub_finish_packaging_code_list_cells(null,'finish_code_id', null, _('----Select Code---'), false, 'id=finish_code_id_row');	
		}else{
			desgin_finish_prodcut_desing_code_list_row(null, $_POST['design_id'], 'finish_code_id', null, false, true, true, true, 'id=finish_code_id_row');
		}
	unit_design_list_cells(null, 'pro_unit', null, true, " ");
	text_cells_ex(null, 'pro_qty', 5, 5);
	if ($id != -1)
	{
		button_cell('ProductUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('ProductCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
 		//set_focus('amount');
	} 
	else 
		submit_cells('ProductAddItem', _("Add Item"), "colspan=2",
		    _('Add new line to Product'), true);

	end_row();
}

function display_memo()
{
	
	div_start('memo');
		/** detail **/
		
		start_table(TABLESTYLE2, "style=min-width:320px;width:80%;");
			textarea_row(_('Memo:'), 'memo_description', null, 50, 3);
		hidden('session_var', '1');  //  code by bajrang   17/07/2015
		end_table(1);
	div_end();
		
}


?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
	</script>