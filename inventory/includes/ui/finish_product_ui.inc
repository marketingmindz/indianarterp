<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//---------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
function display_finish_hearder(&$order){
	
		div_start('details');
		/** detail **/
		
		start_outer_table(TABLESTYLE2);
		
		table_section(1);
		table_section_title(_("Finish Product"));
		design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){

		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		//design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		
		if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
			sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'));
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
			desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
			desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}	
		text_row(_("Finish Product Name:"), 'finish_product_name', null, 30, 30, '','','','id="product_name"');
		text_row_three(_("Assemble Packing Size:"), 'asb_weight','asb_density','asb_height', null, null, null,5,7,6);
		
		table_section(2);
		
		table_section_title(_("Finish Product"));
		design_wood_master_list_row(_("Wood:"), 'wood_id', null, _('No Wood'));
		design_color_master_list_row(_("Color:"), 'color_id', null, _('No Color'));
		design_finish_master_list_row(_("Finish:"), 'finish_id', null, _('No Finish'));
		design_struture_wood_master_list_row(_("Structure of Wood:"), 'structure_wood_id', null, _('No Structure Wood'));
		text_row_sitting(_("No. of Sitting:"), 'no_setting', null, 20, 20, '','','','id="no_setting"');
		design_leg_master_list_row(_("Leg:"), 'leg_id', null, _('No Leg'));
		
		design_skin_master_list_row(_("Use of Fabric:"), 'skin_id', null, _('No Skin'));
		/*design_fabric_master_list_row(_("Fabric:"), 'fabric_id', null, _('----Select Fabric---'));*/
		text_row(_("Gross Weight:"), 'gross_weight', null, 30, 30, '','','','id="gross_weight"');
		
		
		
		end_outer_table(1);
		div_end();
}

//******************** fabric details ****************************


function display_fabric_summary(&$order, $editable=true)
{
	$s_id = $_POST['skin_id'];
	if($s_id == 30){
		echo "<div id='fabric_conect'>";
	}else{
	  echo "<div id='fabric_conect' style='display:none;'>";
	}
	display_heading("Fabric Details", "");
     
    div_start('items_table', "");
	start_table(TABLESTYLE, "colspan=7 width=60%");
	$th = array(_("Part Name"),_("Fabric"),_("Image"), _("Quantity"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
	

	$id = find_submit('Edit');
	$k = 0; 
	foreach ($order->line_items as $line_no => $fab_line)
   	{
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_no))
		{
    		alt_table_row_color($k);
			$part_name = $fab_line->part_name;
			$get_part_name = get_part_type_name($part_name);
			$part_type = $get_part_name['part_name'];
			
			$fabric_name = $fab_line->fabric_name;
			
			$get_fabric_name = get_fabric_type_name($fabric_name);
			$fabric_code = $get_fabric_name['consumable_code'];
			$fabric_name = $get_fabric_name['consumable_name'];
			$fab_img = $get_fabric_name['consumable_image'];
			$fab_name =$fabric_code." ".$fabric_name;
        	label_cell($part_type);
    		label_cell($fab_name);
			label_cell("<img id='bash' src='".company_path().'/consumableImage/'.$fab_img."' alt='your image' width='40' height='30' />");
    		label_cell($fab_line->percentage);
			
			
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_no", _("Edit"),
					  _('Edit document line'));
					delete_button_cell("Delete$line_no", _("Delete"),
						_('Remove line from document'));
			}
		end_row();
		}
		else
		{
			design_fabric_item_controls($order, $k, $line_no);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_fabric_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
	echo "</div>";
}

function design_fabric_item_controls(&$order, &$rowcounter, $line_no=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_no == $id)
	{
		hidden('line_no', $id);
		
		/*$cons_select = $con_line->cons_select;
		$get_cons_select = get_cons_select_name($cons_select);
		$const_select = $get_cons_select['consumable_name'];*/
		$_POST['part_name'] = $order->line_items[$id]->part_name;
		$_POST['fabric_name'] = $order->line_items[$id]->fabric_name;
		$_POST['percentage'] = $order->line_items[$id]->percentage;

	    $Ajax->activate('items_table');
	}
	 	
	desgin_part_master_cells(null, 'part_name',null, _('----Select---'));
	desgin_fabric_master_cells(null, 'fabric_name', null, _('----Select---'));
	echo "<td><img id='fab_img' src='".company_path().'/images/img_not.png'."' alt='your image' width='40' height='30' /></td>";
	text_cells_ex(null, 'percentage', 10, 10);
		if ($id != -1)
		{
			button_cell('FabricUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
			button_cell('FabricCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		} 
		else {
			submit_cells('FabricAddItem', _("Add Item"), "colspan=2",
		    	_('Add new line to fabric'), true);
		}
	end_row();
	
}

//  consumable part //

function display_fconsumable_summary(&$order, $editable=true)
{

	display_heading("Consumable Part");

    div_start('con_table');
	start_table(TABLESTYLE, "colspan=7 width=60%");
	$th = array(_("Type"),_("Select"), _("Quantity"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);
	
	

	$id = find_submits_consume('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{
		if($line_con < 100){ 
		   $line_con += 100; 
		}
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_fcons_type_name($cons_type);
			$const_type = $get_cons_type['master_name'];
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_fcons_select_name($cons_select);
			$const_select = $get_cons_select['consumable_name'];
        	label_cell($const_type);
    		label_cell($const_select);
    		//label_cell($con_line->cons_unit);
			label_cell($con_line->cons_quntity);
    		//label_cell($con_line->cons_iscbm);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_fconsumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_fconsumable_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;
	end_table(1);

    div_end();
}

function design_fconsumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submits_consume('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		//$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_quntity'] = $order->line_con[$id]->cons_quntity;
		//$_POST['cons_iscbm'] = $order->line_con[$id]->cons_iscbm;

	    $Ajax->activate('con_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	//unit_design_list_cells(null, 'cons_unit', null, true, " ");
	text_cells_ex(null, 'cons_quntity', 5, 5);
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
		if ($id != -1)
		{
			button_cell('FConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('FConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
		} 
		else 
			submit_cells('FConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}

//*************** image code header section

function display_image_hearder(&$order){
	
		div_start('details');
		/** detail **/
		
		start_outer_table(TABLESTYLE2);
		
		table_section(1);
		table_section_title(_("Code Generate"));
		//label_row("&nbsp;", 'Code', '', '', '','fn_code');
		
		if(isset($_POST['finish_code'])){
			echo "<td style='display:none;'></td><td style='display:none;'><input type='text' name='finish_code' id='finish_code' value='".$_POST['finish_code']."'></td>";
			
		}else{
			echo "<td style='display:none;'></td><td style='display:none;'><input type='text' name='finish_code' id='finish_code' ></td>";
			
		}
		/*button_code_generate( _(""),'generate_code', 'Generate Code','','','','','','id="generate_code"');*/
		text_row(_("Code:"), 'finish_comp_code', null, 30, 30, '','','','id="finish_comp_code" ');
		button_code_generate( _(""),'generate_code', 'Finish Generate Code','','','','','','id="finish_generate_code"');
		textarea_row(_('Description:'), 'description', null, 42, 5);
		file_row(_("Collection Image") . ":", 'coll_pic', 'coll_pic');
		$col_img_link = "";
		
		$check_remove_image = false;
		if (isset($_POST['coll_pic '])) 
		{
			$col_img_link= "<img id='blah' src='".company_path().'/collectionImage/'.$_POST['coll_pic ']."' alt='your image' width='80' height='80' />";
			$check_remove_image = true;
		} 
		else 
		{
			$col_img_link .= "<img id='blah' src='".company_path().'/images/img_not.png'."' alt='your image' width='80' height='80' />";
		}

		label_row("&nbsp;", $col_img_link);
		table_section(2, '', 'id=finish_pro_img_tbl');
		table_section_title(_("Product Image"));
		
		if(isset($_POST['finish_pro_id'])){
			$f_pro_image = get_product_image($_POST['finish_pro_id']);
			$_SESSION['finish_photo'] = $pro_img = $f_pro_image['product_image'];
			$pro_img_arry = explode(',', $pro_img);
			
			$img_count=0;
			foreach($pro_img_arry as $pro_img){
				$img_count = $img_count+1;
				if($img_count == 1){
					file_row(_("Product Image ".$img_count) . ":", 'pic_'.$img_count, 'pic');
				}else{
					file_row(_("Product Image ".$img_count) . ":", 'pic_'.$img_count, 'pic'.$img_count, 'pic' , $img_count);	
				}
				$pro1_img_link = "";
				$check_remove_image1 = false;
				if (isset($_POST['finish_pro_id'])) 
				{
					if($pro_img != ''){
						$pro1_img_link= "<a href='".company_path().'/finishProductImage/'.$pro_img."' target='_blank'><img id='blah".$img_count."' src='".company_path().'/finishProductImage/'.$pro_img."' alt='your image' 
						width='80' height='80' /></a><img id='".$img_count."' src='".company_path().'/images/del.png'."' class='del_imge' >";
						$check_remove_image2 = true;
					}else{
						$pro1_img_link .= "<img id='blah".$img_count."' src='".company_path().'/images/img_not.png'."' alt='your image' width='90' height='60' />";
					}
				} 
				else 
				{
					$pro1_img_link .= "<img id='blah".$img_count."' src='".company_path().'/images/img_not.png'."' alt='your image' width='90' height='60' />";
				}
		
				label_row("&nbsp;", $pro1_img_link);
				
			}
			if (isset($_POST['finish_pro_id'])) 
				{
					if($pro_img !=''){
						$img_count = $img_count+1;
			file_row(_("Product Image ".$img_count) . ":", 'pic_'.$img_count, 'pic'.$img_count, 'pic' , $img_count);	
			$not_image = "<img id='blah".$img_count."' src='".company_path().'/images/img_not.png'."' alt='your image' width='90' height='60' />";
			label_row("&nbsp;", $not_image);
					}
					
				}
			echo "<tr style='' class='asdf'><td><input type='hidden' name='no_finish_pro_img' id='no_finish_pro_img' value='".$img_count."' ></td> </tr>";
		}else{
	
			file_row(_("Product Image 1 ") . ":", 'pic_1', 'pic');
				$pro1_img_link = "";
				$check_remove_image1 = false;
				if (isset($_POST['finish_pro_id'])) 
				{
					$pro1_img_link= "<img id='blah' src='".company_path().'/finishProductImage/'.$pro_img."' alt='your image' width='80' height='80' />";
					$check_remove_image2 = true;
				} 
				else 
				{
					$pro1_img_link .= "<img id='blah1' src='".company_path().'/images/img_not.png'."' alt='your image' width='90' height='60' />";
				}
		
				label_row("&nbsp;", $pro1_img_link);
				echo "<tr style='' ><td><input type='hidden' name='no_finish_pro_img' id='no_finish_pro_img' value='1' ></td> </tr>";
		}
		
		
		/*button_code_generate( _(""),'finishpro_addnewimg', 'Add another Image','','','','','','id="finishpro_addnewimg"');*/
		end_outer_table(1);
		div_end();
}



?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('click','#color_id',function(){
		var col_id = $(this).val();
		$('#col_img').empty();
		$.ajax({
			url: "get_color_images.php",
			method: "POST",
            data: { col_id : col_id},
			success: function(data){
				if(data !=''){
					var col_img = data;
					var c_img = col_img.split(',');	
					var img_count = c_img.length;
					for(i=0; i < c_img.length; i++){
						$('#col_img').append("<img src='" + c_img[i] + "' width='32' height='22' style='margin-left:5px;'/>");
					}
				}else{
					$('#col_img').append("<img src='../../company/0/images/img_not.png' width='32' height='22'/>");
				}
				
			}
		});
		return false;
	});
	
	$( document ).ready(function() {
		var col_id = $('#color_id').val();
		$('#col_img').empty();
		$.ajax({
			url: "get_color_images.php",
			method: "POST",
            data: { col_id : col_id},
			success: function(data){
				if(data !=''){
					var col_img = data;
					var c_img = col_img.split(',');	
					var img_count = c_img.length;
					for(i=0; i < c_img.length; i++){
						$('#col_img').append("<img src='" + c_img[i] + "' width='32' height='22' style='margin-left:5px;'/>");
					}
				}else{
					$('#col_img').append("<img src='../../company/0/images/img_not.png' width='32' height='22'/>");
				}
				
			}
		});
		return false;
	});
	
</script>