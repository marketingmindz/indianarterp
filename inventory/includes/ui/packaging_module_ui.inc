<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//---------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------
function display_packaging_hearder(&$order){

		div_start('details');
		/** detail **/
		
		start_outer_table(TABLESTYLE2, "style=width:80%;");
		
		table_section(1);
		table_section_title(_("Finish Product Code Selection"));
		design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		
		if($_POST['collection_id'] == "NonCollection"){
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
			sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'));
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
			desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
			desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}
		
		if($_POST['design_id'] == '-1'){
			sub_finish_packaging_code_list_row(_("Finish Product Codes:"),'finish_code_id', null, _('----Select Code---'));	
		}else{
			desgin_finish_prodcut_desing_code_list_row(_("Finish Product Codes:"), $_POST['design_id'], $_POST['finish_code_id'], null, false, true, true, true);
		}
			
		end_outer_table(1);
		div_end();
}

//  consumable part //

function display_packaging_list_summary(&$order, $editable=true)
{

	display_heading("Packaging");

    div_start('items_table');
	start_table(TABLESTYLE, "colspan=7 style=width:80%;");
	$th = array(_("Principal Type"),_("Code"), _("Unit"), _("Quantity"),"");
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
	

	$id = find_submit('Edit');
	$k = 0; 
	foreach ($order->line_items as $line_no => $fab_line)
   	{
		
    	if (!$editable || ($id != $line_no))
		{
    		alt_table_row_color($k);

        	$cons_type = $fab_line->principle_type;
			$get_cons_type = get_pconss_type_name($cons_type);
			$principle_type = $get_cons_type['master_name'];
			$cons_select = $fab_line->code_id;
			$get_cons_select = get_pconss_select_name($cons_select);
		    $const_select = $get_cons_select['consumable_name'];
			
        	label_cell($principle_type);
    		label_cell($const_select);
    		label_cell($fab_line->unit);
			label_cell($fab_line->quantity);
    		/*label_cell($fab_line->width);
			label_cell($fab_line->height);
			label_cell($fab_line->density);*/
			
    		
			if ($editable)
			{
					edit_button_cell("Edit$line_no", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_no", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_packaging_item_controls($order, $k, $line_no);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_packaging_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
}

function design_packaging_item_controls(&$order, &$rowcounter, $line_no=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_no == $id)
	{
		hidden('line_no', $id);
		$_POST['principle_type'] = $order->line_items[$id]->principle_type;
		$_POST['code_id'] = $order->line_items[$id]->code_id;
		$_POST['unit'] = $order->line_items[$id]->unit;
		$_POST['quantity'] = $order->line_items[$id]->quantity;
		//$_POST['width'] = $order->line_items[$id]->width;

	    $Ajax->activate('items_table');
	}
	 
	stock_master_list_cells(null, 'principle_type', null, _('----Select---'));
	if($_POST['principle_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'code_id',null, _('----Select---'));
	}else{
		 customer_consumable_list_cells(null, $_POST['principle_type'],'code_id', null, false, true, true, true);
	}
	//unit_design_list_cells(null, 'unit', null, true, " ");
	if($_POST['code_id'] == '-1')
	{
		unit_list_cells(null, 'unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['code_id'], 'unit', null, false, true, true, true);
	}
	text_cells_ex(null, 'quantity', 5, 5);
	/*text_cells_ex(null, 'width', 10, 10);
	text_cells_ex(null, 'height', 10, 10);
	text_cells_ex(null, 'density', 10, 10);*/
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
	
		if ($id != -1)
		{
			button_cell('FPackagingUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('FPackagingCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
		} 
		else 
			submit_cells('FPackagingAddItem', _("Add Packaging"), "colspan=2",
				_('Add new line to packaging'), true);
	end_row();
	
}



function display_packaging_carton(&$order)
{
	display_heading("Carton:");

	div_start('carton_table');
	start_outer_table(TABLESTYLE2, "style=width:80%;");
		
	table_section(1);
	carton_type_list_row(_("Type:"), 'carton_id', null);
	echo "<input type='hidden' name='screenimages' id='screenimages' value='65' />";
	echo "<input type='hidden' name='screenlables' id='screenlables' value='65' />";
	if($_POST['carton_id'] != 101)
	{
		echo "<tr id='mastercartontr' ><td class='label'>Carton:</td><td>
		<select name='master_carton_id' id='master_carton_id'>
				<option value='".$_POST['master_carton_id']."'> ".$_POST['master_carton_id']."</option>
		</select>";
	}
	else
	{
		echo "<tr id='mastercartontr' style='display:none'><td class='label'>Carton:</td><td>
		<select name='master_carton_id' id='master_carton_id'>
				<option value='-1'> Select Master Corton</option>
		</select>";	
	}	
	text_row(_("Carton qty:"), 'cat_qty', null, 30, 30, '','','','id="cat_qty"');
	text_row_three(_("Margin:"), 'mar_weight','mar_height','mar_density', null, null, null,'mar_w','mar_h','mar_d');
	design_carton_type_list_row(_("Carton type:"), 'carton_type_id', null, _('----Select type---'));	
	text_row(_("Gross weight :"), 'gross_weight', null, 30, 30, '','','','id="gross_weight"');
	
	table_section(2);
	text_row(_("Code:"), 'code', null, 30, 30, '','','','id="code"');	
	text_row_three(_("Pkg. size for catoon:"), 'pkg_s_weight','pkg_s_height','pkg_s_density', null, null, null,'pkg_s_w','pkg_s_h','pkg_s_d');
	text_row_three(_("Carton Size:"), 'car_s_weight','car_s_height','car_s_density', null, null, null,'car_s_w','car_s_h','car_s_d');
	text_row(_("Net weight :"), 'net_weight', null, 30, 30, '','','','id="net_weight"');
	
	end_outer_table(1);
	div_end();
	

}


//----------------- code by chetan ( carton part section ) ---------------

function display_carton_part_section_summary(&$order, $editable=true)
{
	
	display_heading("Carton Part Section");

    div_start('con_table');
	start_table(TABLESTYLE, "colspan=7 style=width:50%;");
	$th = array(_("Part Type"),_("Qty"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);
	
	

	$id = find_submits_consume('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{
		if($line_con < 100){ 
		   $line_con += 100; 
		}
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$part_id = get_assemble_carton_part($con_line->part_name_id);
			$part_name = $part_id['part_name'];
			
			
			label_cell($part_name);
    		label_cell($con_line->part_qty);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			carton_part_section_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		carton_part_section_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;
	end_table(1);

    div_end();
}

function carton_part_section_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submits_consume('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['part_name_id'] = $order->line_con[$id]->part_name_id;
		$_POST['part_qty'] = $order->line_con[$id]->part_qty;
	    $Ajax->activate('con_table');
	}
	$desing_id = $_POST['design_id'];
	if($_POST['design_id'] == '-1'){
		desgin_part_empty_list_cells(null, 'part_name_id', null, _('----Select type---'));
	}else{
		desgin_part_qty_new_list_cells(null, 'part_name_id', $desing_id, null, _('----Select type---'));
	}
	
	text_cells_ex(null, 'part_qty', 10, 10, null, null, null, null, false, 'id="part_qty"');
	//text_cells_ex(null, 'part_qty', 5, 5);



/*	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	//sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	unit_design_list_cells(null, 'cons_unit', null, true, " ");
	text_cells_ex(null, 'cons_quntity', 5, 5);
	check_cells_ex_design(null, 'cons_iscbm',null, true);*/
		if ($id != -1)
		{
			button_cell('CPartUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('CPartCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
			//set_focus('amount');
		} 
		else 
			submit_cells('CPartAddItem', _("Add Part"), "colspan=2",
				_('Add new line to part'), true);
	end_row();
	
}
//-------------- end code ------------------------

function display_carton_session_summary($post)
{
	
	display_heading("Current Cartons:");

    div_start('show_carton_table');
	start_table(TABLESTYLE, " id='showcartontbl' colspan=7 style=width:80%;");
	$th = array(_("Carton "),_("Carton Type"), _("Maste Carton"),_("code"),_("Quantity"),"");
	if (count($order->line_con)) $th[] = '';
	table_header($th);
	
		carton_sesstion_controls($post);
	
	end_table(1);

    div_end();
}
//------------- pop -----------------

function view_part($mscarton)
{
		echo "<td><a style='cursor:pointer'  path='".company_path()."/CartonImage/' data='".json_encode($mscarton)."' 
		' onclick=\"display_carton_part_summary($(this).attr('data'), $(this).attr('path'))\" >View</a></td>\n";		
}
//----------- end pop ----------------
/*function carton_sesstion_controls($post)
{
	global $Ajax;
	$packagin_id = $_POST['packaging_pro_id'];
	if($packagin_id==''){
	
	foreach($_SESSION['master_cart_section']['master']['carton'] as $mscarton){
		start_row();
				
				label_cell('Master');
				$my_carton = get_catron_name($mscarton['carton_type_id']);
				while($myrow = db_fetch($my_carton))
				{
				 $carton_name = $myrow['carton_name'];
				}	
				label_cell($carton_name);
				label_cell('---');
				label_cell($mscarton['code']);
				label_cell($mscarton['cat_qty']);
				view_part($mscarton);
			end_row();
				}
		foreach($_SESSION['master_cart_section']['inner']['carton'] as $incarton){
			start_row();
			
				label_cell('Inner');
				$mscarton['carton_type_id']."";
				$my_carton = get_catron_name($incarton['carton_type_id']);
				while($myrow = db_fetch($my_carton))
				{
				 $carton_name = $myrow['carton_name'];
				}
				label_cell($carton_name);
				label_cell($incarton['master_carton_id']);
				label_cell($incarton['code']);
				label_cell($incarton['cat_qty']);
				view_part($incarton);
			end_row();
		}
	}else{
				$my_carton = get_all_catron_list($packagin_id);
				while ($myrow = db_fetch($my_carton)) 
				{
					$carton = $myrow["carton_type_id"];
					if($carton == '101'){
						$carton = "Master Carton";
					}else{
						$carton = "Inner Carton";
					}
					$carton_type = $myrow["carton_type"];
					$carton_type_name = get_carton_type_name($carton_type);
					$carton_name = $carton_type_name['carton_name'];
					start_row();
						label_cell($carton);
						label_cell($carton_name);
						label_cell($myrow["master_carton_id"]);
						label_cell($myrow["code"]);
						label_cell($myrow["quantity"]);	
					end_row();
				}
	}
}*/
function carton_sesstion_controls($post)
{
	global $Ajax;
	$packagin_id = $_POST['packaging_pro_id'];
	if($packagin_id==''){
	foreach($_SESSION['master_cart_section']['master']['carton'] as $mscarton){
		start_row();
				label_cell('Master');	
				$mscarton['carton_type_id']."";
				//   code by bajrang  ---- get carton name - 22-07-2015
				$my_carton = get_catron_name($mscarton['carton_type_id']);
				while($myrow = db_fetch($my_carton))
				{
				 $carton_name = $myrow['carton_name'];
				}
				label_cell($carton_name);
				label_cell('---');
				label_cell($mscarton['code']);
				label_cell($mscarton['cat_qty']);
				view_part($mscarton);
			end_row();
				}
		foreach($_SESSION['master_cart_section']['inner']['carton'] as $incarton){
			start_row();
				label_cell('Inner');
				$mscarton['carton_type_id']."";
				$my_carton = get_catron_name($incarton['carton_type_id']);
				while($myrow = db_fetch($my_carton))
				{
				 $carton_name = $myrow['carton_name'];
				}
				label_cell($carton_name);
				label_cell($incarton['master_carton_id']);
				
				label_cell($incarton['code']);
				label_cell($incarton['cat_qty']);
				view_part($incarton);
			end_row();
		}
	}else{
				$my_carton = get_all_catron_list($packagin_id);
				
				
				while ($myrow = db_fetch($my_carton)) 
				{
					$cat_part_Val = array();
					$get_carton_part = get_all_carton_part_list($myrow['carton_detail_id']);
					while($part_row = db_fetch($get_carton_part)){
						
						array_push( $cat_part_Val, $part_row);
						
					}
					//--------------------   end all part --------------------------------------------
					$cat_Val = json_encode($myrow);
					$cat_part = json_encode($cat_part_Val);
					$carton = $myrow["carton_type_id"];
					if($carton == '101'){
						$carton = "Master Carton";
					}else{
						$carton = "Inner Carton";
					}
					$cart_detail_id =  $myrow["carton_detail_id"];
					$carton_type = $myrow["carton_type"];
					$carton_type_name = get_carton_type_name($carton_type);
					$carton_name = $carton_type_name['carton_name'];
					if($myrow["master_carton_id"] == '-1'){
						$master_cart_id = "";
					}else{
						$master_cart_id = $myrow["master_carton_id"];
					}
					
					start_row();
						label_cell($carton);
						label_cell($carton_name);
						label_cell($master_cart_id);
						label_cell($myrow["code"]);
						label_cell($myrow["quantity"]);
						
						echo "<td><a style='cursor:pointer' path='".company_path()."/CartonImage/'  data='$cat_Val' data-part='$cat_part' 
		' onclick=\"display_carton_part_summary_search($(this).attr('data'),$(this).attr('data-part'), $(this).attr('path'))\" >View</a></td>\n";
					end_row();
				}
	}
}



function display_packaging_screen(&$order)
{
	display_heading("Screen:");
	div_start('details');
		/** detail **/
		
		start_outer_table(TABLESTYLE2, "style=width:80%;");
			table_section(1);
		file_row(_("Side Image 1 ") . ":", 'pic_0', 'pic');
		
		file_row(_("Side Image 2") . ":", 'pic_1', 'pic1');
		
		file_row(_("Side Image 3") . ":", 'pic_2', 'pic2');
				
		file_row(_("Side Image 4") . ":", 'pic_3', 'pic3');
	
		file_row(_("Side Image 5") . ":", 'pic_4', 'pic4');
		
		file_row(_("Side Image 6") . ":", 'pic_5', 'pic5');
		
			table_section(2);
		pacakaging_label_list_row(_("label 1:"), 'label_id_0', null, _('----Select label 1---'));
		pacakaging_label_list_row(_("label 2:"), 'label_id_1', null, _('----Select label 2---'));
		pacakaging_label_list_row(_("label 3:"), 'label_id_2', null, _('----Select label 3---'));
		pacakaging_label_list_row(_("label 4:"), 'label_id_3', null, _('----Select label 4---'));
		pacakaging_label_list_row(_("label 5:"), 'label_id_4', null, _('----Select label 5---'));
		pacakaging_label_list_row(_("label 6:"), 'label_id_5', null, _('----Select label 6---'));
		end_outer_table(1);
		
		
		echo "<center>";
		submit_center_last('AddthisCarton', _("Add This Carton"), '', 'default');
		echo "</center><br><br><br><br>";
		
		div_end();

}




?>


<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
</script>