<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FINISH';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Finish Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/finish_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------
//***** image code **/

/*$upload_file = "";
if (isset($_FILES['pic']) && $_FILES['pic']['name'] != '') 
{
	$finish_reference = $_POST['finish_reference'];
	$result = $_FILES['pic']['error'];
 	$upload_file = 'Yes'; //Assume all is well to start off with
	$filename = company_path().'/finishImage';
	if (!file_exists($filename))
	{
		mkdir($filename);
	}	
	$filename .= "/".item_img_name($finish_reference).".jpg";
	
	//But check for the worst 
	if ((list($width, $height, $type, $attr) = getimagesize($_FILES['pic']['tmp_name'])) !== false)
		$imagetype = $type;
	else
		$imagetype = false;
	//$imagetype = exif_imagetype($_FILES['pic']['tmp_name']);
	if ($imagetype != IMAGETYPE_GIF && $imagetype != IMAGETYPE_JPEG && $imagetype != IMAGETYPE_PNG)
	{	//File type Check
		display_warning( _('Only graphics files can be uploaded'));
		$upload_file ='No';
	}
	elseif (!in_array(strtoupper(substr(trim($_FILES['pic']['name']), strlen($_FILES['pic']['name']) - 3)), array('JPG','PNG','GIF')))
	{
		display_warning(_('Only graphics files are supported - a file extension of .jpg, .png or .gif is expected'));
		$upload_file ='No';
	} 
	elseif ( $_FILES['pic']['size'] > ($max_image_size * 1024)) 
	{ //File Size Check
		display_warning(_('The file size is over the maximum allowed. The maximum size allowed in KB is') . ' ' . $max_image_size);
		$upload_file ='No';
	} 
	elseif (file_exists($filename))
	{
		$result = unlink($filename);
		if (!$result) 
		{
			display_error(_('The existing image could not be removed'));
			$upload_file ='No';
		}
	}
	
	if ($upload_file == 'Yes')
	{
		$result  =  move_uploaded_file($_FILES['pic']['tmp_name'], $filename);
	}
	$Ajax->activate('details');
 
}
if (isset($_POST['finish_reference']) && file_exists(company_path().'/finishImage/'.item_img_name($_POST['finish_reference']).".jpg")) 
{
	$finish_reference = $_POST['finish_reference'].".JPG";
}else{
	$finish_reference = 'No Image';
}*/
/**** end image code *********************/
if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['finish_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The finish name cannot be empty."));
		set_focus('finish_name');
	}
	if (strlen($_POST['finish_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The finish reference cannot be empty."));
		set_focus('finish_reference');
	}

	if ($input_error != 1) 
	{
		
    	if ($selected_id != -1) 
    	{
      		$check_update_finish = check_update_finish($_POST['finish_reference']);
		    if($check_update_finish)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			 	 if(!empty ($_FILES['pic']['tmp_name'])){
					 	$finish_reference= $_FILES['pic']['name'];
						$image_tmp= $_FILES['pic']['tmp_name'];
						$filename = company_path().'/finishImage/'.$finish_reference;
						move_uploaded_file($image_tmp,$filename);
			 			update_finish_image($selected_id, $finish_reference);
			     }
			     update_finish_master($selected_id, $_POST['finish_name'], $_POST['finish_reference'], $_POST['finish_description'], $finish_reference);
			     display_notification(_('Selected finish master has been updated'));
			  }
    	} 
    	else 
    	{
			$check_finish = check_finish($_POST['finish_reference']);
		    if($check_finish)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
			 	$finish_reference= $_FILES['pic']['name'];
				$image_tmp= $_FILES['pic']['tmp_name'];
				$filename = company_path().'/finishImage/'.$finish_reference;
				move_uploaded_file($image_tmp,$filename);
    		     add_finish_master($_POST['finish_name'], $_POST['finish_reference'], $_POST['finish_description'], $finish_reference);
			     display_notification(_('New finish master has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_finish_master($selected_id);
		display_notification(_('Selected finish master has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_finish_master(check_value('show_inactive'));

start_form(true);

start_table(TABLESTYLE2, "style=width:50%;min-width:320px;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code
 		$edit_mod = "readonly";
		$myrow = get_finish_master($selected_id);

		$_POST['finish_name']  = $myrow["finish_name"];
		$_POST['finish_reference']  = $myrow["finish_reference"];
		$_POST['finish_description']  = $myrow["finish_description"];
		$imge = $myrow["finish_image"];
		$_POST['del_image'] = 0;
	}
	hidden('selected_id', $selected_id);
}
text_row(_("Finish Name:"), 'finish_name', null, 40, 40);
text_row(_("Reference:"), 'finish_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec" '.$edit_mod.'');
file_row(_("Image File (.jpg)") . ":", 'pic', 'pic');
	$finish_img_link = "";
	
	if(!empty($imge))
	{
		$finish_img_link= "<img id='blah' src='".company_path().'/finishImage/'.$imge."' alt='your image' width='80' height='80' />";
	} 
	else 
	{
		$finish_img_link= "<img id='blah' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='80' height='80' />";
	}

	label_row("&nbsp;", $finish_img_link);
	
textarea_row(_('Description:'), 'finish_description', null, 50, 5);

//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

echo "<br>";
start_table(TABLESTYLE, "style=width:50%;min-width:320px;");

$th = array(_('Finish Name'),_('Finish Reference'),_('Image'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["finish_name"]);
	label_cell($myrow["finish_reference"]);
	//label_cell($myrow["wood_description"]);
	label_cell($myrow["finish_image"]);
	
	inactive_control_cell($myrow["finish_id"], $myrow["inactive"], 'finish_master', 'finish_id');
 	edit_button_cell("Edit".$myrow['finish_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['finish_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

//-----------------------------------------------------------------------------------


end_form();

//------------------------------------------------------------------------------------

end_page();


?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
		
		$(function(){
    		jQuery('body').on('change', '#pic', function () {
			  var file = $('#pic').val();
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 
						reader.onloadend = function(){ // set image data as background of div
							$('#blah').attr('src', this.result);
						}
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  $('#pic').val('');
				  $('#blah').attr('src', 'img_not.png');
				}
			  }
			});
  	});
   
</script>