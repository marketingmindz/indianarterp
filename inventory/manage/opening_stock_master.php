<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_STOCKMASTER';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/db/open_stock_db.inc");
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Opening Stock Master"), false, false, "", $js);
simple_page_mode(true);
//----------------------------------------------------------------------------------



if(isset($_GET['stock_id'])){
	$selected_id = $_GET['stock_id'];
}else{
	$selected_id = -1;
}

if(isset($_GET['redirect']))
{
	$redirect = 'reorder_level_inquiry.php';
}
if(isset($_POST['AddStock']))
{
	
	if (strlen($_POST['stock_quantity']) == 0) 
	{
		$input_error = 1;
		display_error(_("The quantity cannot be empty."));
		set_focus('stock_quantity');	
	}
	
	if($_POST['consumble_id'] != -1)
	{
		$check_stock = check_consumable_stock($_POST['stock_type_id'], $_POST['consumble_id'], $_POST['consumble_category'],$_POST['location_id'],$_POST['work_center_id']);
	}
	
	if($_POST['finish_code_id'] != -1)
	{
		$check_stock = check_finish_stock($_POST['stock_type_id'],$_POST['design_id'], $_POST['finish_code_id'], $_POST['location_id'],$_POST['work_center_id']);
	}
	
	if($_POST['finish_code_id'] == -1)
	{
		if($_POST['design_id'] != -1)
		{
			$check_stock = check_design_stock($stock_type_id, $_POST['design_id'], $_POST['location_id'],$_POST['work_center_id']);
		}
	}
			

	if($check_stock == true){
	    add_open_stock_category($_POST['stock_type_id'], $_POST['location_id'],$_POST['work_center_id'], $_POST['consumble_id'], $_POST['consumble_category'], $_POST['category_id'], $_POST['range_id'], $_POST['design_id'],$_POST['finish_code_id'],$_POST['cons_unit'],$_POST['level'], $_POST['stock_quantity'],$_POST['date']);
		display_notification(_('New Open stock has been added'));
		meta_forward('opening_stock_list.php?');		
	}else{
		display_notification(_('Stock Already exists'));
	}

}

if(isset($_POST['UpdateStock']))
{
	if (strlen($_POST['stock_quantity']) == 0) 
	{
		$input_error = 1;
		display_error(_("The quantity cannot be empty."));
		set_focus('stock_quantity');	
	}

	update_open_stock_category($_POST['selected_id'],$_POST['stock_type_id'],$_POST['location_id'],$_POST['work_center_id'], $_POST['consumble_id'], $_POST['consumble_category'], $_POST['category_id'],$_POST['range_id'], $_POST['design_id'],$_POST['finish_code_id'],$_POST['cons_unit'],$_POST['level'], $_POST['stock_quantity'], $_POST['date']);
	display_notification(_('New Open stock has been updated'));
	if(isset($_POST['redirect']))
	{
		meta_forward('reorder_level_inquiry.php');
	}
	else
	{
		//meta_forward('opening_stock_list.php?');
	}
}



if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_opening_stock($selected_id);
		display_notification(_('Selected opening stock has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}

//-----------------------------------------------------------------------------------
start_form(true);
$result = get_all_opening_stock_master();



start_table(TABLESTYLE2, "style=min-width:320px;width:40%;");
	
	
	if ($selected_id != '') {
			//editing an existing status code
			
		$myrow = get_opening_stock_master($selected_id);		
		$_POST['location_id']  = $myrow["location_id"];
		$_POST['work_center_id']  = $myrow["work_center_id"];	
		$_POST['stock_type_id']  = $myrow["stock_type_id"];
		$_POST['consumble_id']  = $myrow["consumable_id"];
		$_POST['consumble_category']  = $myrow["consumable_category"];
		$_POST['category_id']  = $myrow["pro_cat_id"];
		$_POST['range_id']  = $myrow["pro_range_id"];
		$_POST['design_id']  = $myrow["pro_design_id"];
		$_POST['finish_code_id']  = $myrow["pro_finish_id"];
		$_POST['level'] = $myrow['level'];
		$_POST['stock_quantity']  = $myrow["stock_qty"];
		$_POST['date']  = $myrow["date"];
		hidden("selected_id",$selected_id);
		if(isset($_GET['redirect']))
		{
			hidden("redirect",'1');
		}			
	}
	design_location_list_row(_("Location:"), 'location_id', null, _('------Select Location-----'));
	if(isset($_GET['stock_id'])){
		purchase_work_center_list_row(_("Work Center:"),'work_center_id', $_POST['location_id'], $_POST['work_center_id']);
	}else{
		design_work_center_list_row(_("Work Center:"),'work_center_id', null, _('-----Select Work Center----'));
	}
	stock_type_list_row(_("Stock Type:"), 'stock_type_id', null);
	

//------------------ consumble type ----------------------
	stock_master_list_row(_("Consumable Name:"), 'consumble_id', null, _('----Select---'),false,'id="consumable_name"');
		
	if($_POST['consumble_id'] == '-1'){
		sub_consumable__master_list_row(_("Consumable Category:"), 'consumble_category' ,null, _('----Select---'),false,'id="consumable_cat"');
	}else{
		customer_consumable_list_row(_("Consumable Category:"), $_POST['consumble_id'],'consumble_category', null, false, true, true, true, 'id="consumable_cat"');
	}

	design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'),false ,'id=pro_cat_id_row');
	
	sub_collection_code_list_row(_("Collection:"), 'collection_id', null, false, true, 'id=collection_id_row');
	
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));

	if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
		sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'), false,'id=pro_design_id_row');
	}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
		desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true,'id=pro_design_id_row');
	}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
		desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
	}	
	if($_POST['design_id'] == '-1'){
		sub_finish_packaging_code_list_row(_("Finish Product Codes:"),'finish_code_id', null, _('----Select Code---'), false, 'id=finish_code_id_row');	
	}else{
		desgin_finish_prodcut_desing_code_list_row(_("Finish Product Codes:"), $_POST['design_id'], $_POST['finish_code_id'], null, false, true, true, true, 'id=finish_code_id_row');
	}
//'unit', null, 30, 30,null, null,null,"id=null"
    echo "<tr><td>Unit: </td>";
	if($_POST['consumble_category'] == '-1')
	{
		unit_list_cells(null, 'cons_unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['consumble_category'], 'cons_unit', null, false, true, true, true);
	}
	echo "</tr>";
    text_row(_("Reorder Level:"), 'level', null, 30, 30, null,null,null,"id= reorder_level");
	text_row(_("Stock Quantity:"), 'stock_quantity', null, 30, 30);
	date_row(_("Date:") . ":", 'date');
	end_table(1);

	if ($selected_id != -1) 
		{
			echo "<br><center>";
			submit_center_first('UpdateStock', _("Update"), _('Update Stock'), 'default');
			submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel'), 'default');
			echo "</center>";
		}
		else
		{
			echo "<br><center>";
			submit_center_last('AddStock', _("Add Stock"), '', 'default');
			echo "</center>";
		}
	
	end_form();
	end_page();
?>

<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">

$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});

 $(document).ready(function(e) {
    var slc_stock_master = $('#slc_stock_master').val();
		stock_type(slc_stock_master);
});
	
	function consumable_type()
	{
			$("#pro_cat_id_row").hide();
			$("#collection_id_row").hide();
			$("#range_span").hide();
			$("#pro_design_id_row").hide();
			$("#finish_code_id_row").hide();	
			$("#consumable_name").show();
			$("#consumable_cat").show();
			$("#reorder_level").show();
			$("#unit").show();
	}
	
	function product_type()
	{
			$("#pro_cat_id_row").show();
			$("#collection_id_row").show();
			$("#range_span").show();
			$("#pro_design_id_row").show();
			$("#finish_code_id_row").show();	
			$("#consumable_name").hide();
			$("#consumable_cat").hide();
			$("#reorder_level").hide();
			$("#unit").hide();
	}
	
	
	function stock_type(slc_stock_master)
		{
			if(slc_stock_master == '102'){
				product_type();
			}
			else
			{
				consumable_type();
			}		
		}
	$('body').on('change','#slc_stock_master',function(){
		var slc_stock_master = $(this).val();
		stock_type(slc_stock_master);
		
		// reset drop down lists
		$('#slc_master option:eq(0)').attr('selected','selected');
		$('#sub_master option:eq(0)').attr('selected','selected');
		$('#slc_category option:eq(0)').attr('selected','selected');
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
	
	
	
	
	
	$('body').on('click','.editbutton',function(){
		$('body').on('change','.fa-content',function(){
			var slc_stock_master = $('#slc_stock_master').val();
		stock_type(slc_stock_master);
		});
		
	});



	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	// resetting of dropdowns
	
	$(document).on('change','#slc_category',function(){
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
	
	$(document).on('change','#collection_id',function(){
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
	$(document).on('change','#range_id_code',function(){
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
	$(document).on('change','#sub_design',function(){
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
		
	
		$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					 // $('#sub_design').append("<option value='-1'>-- Select --</option>");
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});	
</script>
