<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FAB_COMBINATION';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Fabric Combination"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/fabric_combination.inc");

simple_page_mode(true);



start_form(true);
?>
<center>
	<table class="tablestyle_noborder" cellpadding="2" cellspacing="0">
		<tbody>
        	<tr>
                <td>combination Code:</td>
                <?php 
					#Get existing fabric combination name
					$sql_fabcode = "SELECT fab_code_id,fabric_comb FROM ".TB_PREF."fabric_code group by fab_code_id ";
					$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
					
				?>
                <td><select autocomplete="off" name="FabCode" class="combo" title="" id="FabCode" _last="0">
                		<option selected="selected" value="-1">----Select---</option>
                       <?php while ($row_fabcode = db_fetch($result_fabcode)) { ?>
                       	<option value="<?php echo $row_fabcode["fab_code_id"]; ?>"><?php echo $row_fabcode["fabric_comb"]; ?></option>
                       <?php } ?>
					</select>
                </td>
                <td><button class="ajaxsubmit" type="submit" aspect="default" name="SearchByFabCode" id="SearchByFabCode" value="Search" title="Select documents">
                <img src="../../themes/exclusive/images/ok.gif" height="12"><span>Search</span></button></td>
			</tr>
		</tbody>
	</table>
</center>

<center>
	<table class="tablestyle_noborder" cellpadding="2" cellspacing="0">
		<tbody>
        	<tr>
				<td>Product Code:</td>
				<td><input class="searchbox" type="text" name="ProCode" id="ProCode" size="16" maxlength="18" value="" _last_val="" autocomplete="off"></td>
				<td><button class="ajaxsubmit" type="submit" aspect="default" name="SearchByProCode" id="SearchByProCode" value="Search" title="Select documents">
                <img src="../../themes/exclusive/images/ok.gif" height="12"><span>Search</span></button></td>
			</tr>
		</tbody>
	</table>
</center>


<div id="">
	<center>
    	<div id="fabric_conect" style="">
        	<center>
            	
            </center>
			<div id="fab_item_table">
            	<center>
                <br />
                <br />
                	<table class="tablestyle" colspan="7" width="60%" cellpadding="2" cellspacing="0" width="500">
						<tbody>
                        	<tr>
								<td class="tableheader" width="10%">S.N</td>
								<td class="tableheader" width="20%">Combination Name</td>
                                <td class="tableheader" width="20%">Product Code</td>
                                <td class="tableheader" width="20%">Product Name</td>
								<td class="tableheader" width="30%">Fabric Code</td>
							</tr>
							<tr>
								<td>No Record</td>
                                <td>No Record </td>
                                <td>No Record</td>	
                                <td>No Record</td>
                                <td>No Record </td>
							</tr>
						</tbody>
					</table>
				</center>
			<br></div>
		</div>
	</center>
</div>
<?php


//------------------------------------------------------------------------------------

end_page();
?>

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">


	
	$(document).on('click','#SearchByFabCode',function(){
		var search_code = $('#FabCode').val();
		var search_type = 'fabcode';
		$.ajax({
				url: "fabric_comb_search.php",
				method: "POST",
				data: { code : search_code, type : search_type},
				success: function(data){
					//alert(data);
					//$('#fab_item_table').val(data);
					$('#fab_item_table').html(data);
				}
			});
			return false;
	});
	
	$(document).on('click','#SearchByProCode',function(){
	
		var search_code = $('#ProCode').val();
		var search_type = 'procode';
		$.ajax({
				url: "fabric_comb_search.php",
				method: "POST",
				data: { code : search_code, type : search_type},
				success: function(data){
					//alert(data);
					//$('#fab_item_table').val(data);
					$('#fab_item_table').html(data);
				}
			});
			return false;
	});
	
	
	
</script>