<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMADJUSTMENT';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/item_adjustment_class.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/inventory_stock_status_db.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Inventory Item Status"), false, false, "", $js);

simple_page_mode(true);

if (isset($_GET['stock_id']))
	$selected_id = $_POST['stock_id'] = $_GET['stock_id'];
	
if (!isset($_POST['stock_id']))
	$selected_id = $_POST['stock_id'] = -1;

start_form(true);


start_table(TABLESTYLE2, "style=min-width:320px;width:50%;");
	
	
	stock_type_list_row(_("Stock Type:"), 'stock_type_id', null);

//------------------ consumble type ----------------------
	stock_master_list_row(_("Consumable Name:"), 'consumble_id', null, _('----Select---'),false,'id="consumable_name"');
	
	
	
	if($_POST['consumble_id'] == '-1'){
		sub_consumable__master_list_row(_("Consumable Category:"), 'consumble_category' ,null, _('----Select---'),false,'id="consumable_cat"');
	}else{
		customer_consumable_list_row(_("Consumable Category:"), $_POST['consumble_id'],'consumble_category', null, false, true, true, true, 'id="consumable_cat"');
	}

	design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'),false ,'id=pro_cat_id_row');
	
	sub_collection_code_list_row(_("Collection:"), 'collection_id', null, false, true, 'id=collection_id_row');
	
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));

	if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
		sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'), false,'id=pro_design_id_row');
	}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
		desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true,'id=pro_design_id_row');
	}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
		desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
	}	
	if($_POST['design_id'] == '-1'){
		sub_finish_packaging_code_list_row(_("Finish Product Codes:"),'finish_code_id', null, _('----Select Code---'), false, 'id=finish_code_id_row');	
	}else{
		desgin_finish_prodcut_desing_code_list_row(_("Finish Product Codes:"), $_POST['design_id'], 'finish_code_id', null, false, true, true, true, 'id=finish_code_id_row');
	}


	end_table(1);

			echo "<br><center>";
			submit_center_last('StockStatus', _("Stock Status"), '', 'default');
			echo "</center>";
		
	end_form();




//code by bajrang - - - -17/07/2015


$work_center_details = get_work_center_details();

global 	$Ajax;
$Ajax->activate('status_table');
 

if($_POST['stock_type_id'] == 102)
{

$design_id = $_POST['design_id'];
$finish_code_id = $_POST['finish_code_id'];

div_start('status_table');
echo "<h1>Product Type Stock</h1>";
start_table(TABLESTYLE,"width=500");
	$th = array(_("Location"), _("Work Center"), _("opening stock"), _("Production Team"),
		_("Quantity"));
	table_header($th);
	$j = 1;
	$k = $l = $m = $n = $p =0; //row colour counter
 $_POST['stock_type_id'];	
	$quantity = 0;
 $_POST['consumble_category'];
$location_name = array();
$work_center_id = array();
$loc_details = get_location_details();
$i =0;
while($myrow = db_fetch($loc_details))
{
	$location_name[] = $myrow['$i'];
	$i++;
}
print_r($location_name);
	foreach($location_name as $location)
	{
		     alt_table_row_color($k);
			 echo "<td>".$location['location_name']."</td>";
			 $work_center = $location['work_center_id'];
			 $center_id = explode(",",$work_center);
			 $prod_team = array();
			 echo "<td colspan='4'><table style='width:100%'><tr><td>";
			 
			 echo "<table style='width:100%; border:0px !important; border-spacing:0px;'>";
			// alt_table_row_color($k);
			// echo "<td>";
			 foreach($center_id as $c_id)
			 {
				  alt_table_row_color($l);
				 $work_center_details = get_work_center_details($c_id);
				 $pro_team_id = $work_center_details['pro_team_id'];
				 $location_id = $location['location_id'];
				 label_cell($work_center_details['work_center_name']);
				
				 $pro_team_id= explode(",",$pro_team_id);
				 echo "<td><table style='width:100%; border:0px !important; border-spacing:0px;'>";
			  	 alt_table_row_color($m);
				 
				 echo "<td>";
				    $opening_stock_qty = get_product_opening_stock_qty($location_id, $c_id,$design_id, $finish_code_id );
					$opening_stock = $opening_stock_qty['stock_qty'];
					if($opening_stock == NULL && $opening_stock =='')
					{
						$opening_stock = 0;
					}
					label_cell($opening_stock,"width=50");
				 echo "</td>";
				 
			     echo "<td><table style='width:100%'>";
				 foreach($pro_team_id as $id)
						 {
							alt_table_row_color($n);
							$pro_team = get_pro_team($id);
							label_cell($pro_team['pro_team_name'],"width=150");
							if(isset($_POST['StockStatus']) && $_POST['design_id'] != -1)
							{
							$stock_qunatity = get_product_stock_quantity($location_id, $c_id, $id, $design_id, $finish_code_id );
							$stock = $stock_qunatity['quantity'];
							}
							else
							{
								$stock = 0;
							}
							label_cell($stock,"width=50");
							
							end_row();
						 }
					echo "</table></td>";
					end_row();
				 echo "</table></td>";
			 
			   end_row();
			 }
			// echo "</td>";
			// end_row();
			 echo "</table>";
			
			echo"</td></tr></table></td>";

			
			end_row();
	
		
		$j++;
		If ($j == 12)
		{
			$j = 1;
			table_header($th);
		}
	
	}

end_table();
div_end();
}
else{

$consumable_id = $_POST['consumble_id'];	
$consumable_category = $_POST['consumble_category'];
$loc_details = get_location_details();
$location_name = array();
while($myrow = db_fetch($loc_details))
{
	$location_name[] = $myrow;
}
echo '<pre>';

div_start('status_table');
start_table(TABLESTYLE,"width=500");
	$th = array(_("Location"), _("Work Center"),_("opening stock"), _("Production Team"),
		_("Quantity"));
	table_header($th);
	$j = 1;
	$k = $l = $m = $n = $p =0; //row colour counter	
	$quantity = 0;

	foreach($location_name as $location)
	{
		     alt_table_row_color($k);
			 echo "<td>".$location['location_name'] ."</td>";
			 $location_id = $location['location_id'];
			 $work_center = $location['work_center_id'];
			 $center_id = explode(",",$work_center);
			 $prod_team = array();
			 echo "<td colspan='4'><table style='width:100%'><tr><td>";
			 
			 echo "<table style='width:100%; border:0px !important; border-spacing:0px;'>";
			// alt_table_row_color($k);
			// echo "<td>";
			 foreach($center_id as $c_id)
			 {
				  alt_table_row_color($l);
				 $work_center_details = get_work_center_details($c_id);
				 $pro_team_id = $work_center_details['pro_team_id'];
				 
				 label_cell($work_center_details['work_center_name'],"width=100");
				
				 $pro_team_id= explode(",",$pro_team_id);
				 echo "<td><table style='width:100%; border:0px !important; border-spacing:0px;'>";
			  	 alt_table_row_color($m);
				 echo "<td>";
				$opening_stock_qty = get_cons_opening_stock_qty($location_id, $c_id,$consumable_id, $consumable_category );
				$opening_stock = $opening_stock_qty['stock_qty'];
				if($opening_stock == NULL && $opening_stock =='')
					{
						$opening_stock = 0;
					}
				label_cell($opening_stock,"width=50");
			echo "</td>";
			     echo "<td><table style='width:100%'>";
				 foreach($pro_team_id as $id)
						 {
							alt_table_row_color($n);
							$pro_team = get_pro_team($id);
							label_cell($pro_team['pro_team_name'],"width=150");
							
							if(isset($_POST['StockStatus']) && $_POST['consumble_id'] != '-1')
							{
							$stock_qunatity = get_stock_quantity($location_id, $c_id, $id, $consumable_id, $consumable_category );
							$stock = $stock_qunatity['quantity'];
							if($stock == NULL && $stock == '')
							$stock = 0;
							}
							else
							{
								$stock = 0;
							}
							label_cell($stock,"width=50");
							end_row();
						 }
							 echo "</table></td>";
						end_row();
						 echo "</table></td>";
			 
			   end_row();
			 }
			// echo "</td>";
			// end_row();
			 echo "</table>";
			
			echo"</td></tr></table></td>";

			
			end_row();
	
		
		$j++;
		If ($j == 12)
		{
			$j = 1;
			table_header($th);
		}
	
	}

end_table();
div_end();
}


end_page();

?>


<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}


</style>


<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(e) {
    $("#pro_table").hide();
	$("#cons_table").show();
});
$(document).on('change','#slc_stock_master',function(){
		var stock_type = $("#slc_stock_master").val();
		if(stock_type=='101'){
			//alert(stock_type);
		$("#pro_table").hide();
		$("#cons_table").show();
		
		}else{
			//alert(stock_type);
		$("#pro_table").show();
		$("#cons_table").hide();
		}
		
		$('#slc_master option:eq(0)').attr('selected','selected');
		$('#sub_master option:eq(0)').attr('selected','selected');
		$('#slc_category option:eq(0)').attr('selected','selected');
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
		
	});



	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_id_code").hide();
			}else{
				$("#range_id_code").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
	
	
	// - - ------     location change  ---change work center
	
	
	
	
	$(document).on('change','#slc_work_center',function(){
		//alert("data is synchronization successfully .... ")
		var slc_work_center = $(this).val();
		$.ajax({
			url: "slc_center_calling.php",
			method: "POST",
            data: { id : slc_work_center},
			success: function(data){
					var select_val = $('#slc_production');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
/*	$(document).on('change','#slc_location',function(){
		var slc_location = $('#slc_location').val();
		if(slc_location != '-1'){
			$.ajax({
				url: "slc_location_calling.php",
				method: "POST",
				data: { id : slc_location },
				success: function(data){
						$( "option" ).remove( ".work_extra" );
						$('#slc_work_center').append(data);
					}
			});
		}else{
			$( "option" ).remove( ".work_extra" );
		}
		
	});*/
	
	$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	$(document).ready(function(e) {
		
		
		var r_id = '';
		$.ajax({
			url: "reference_no.php",
			method: "POST",
            data: { r_id : r_id},
			success: function(data){
				//alert(data);
					 $('#product_name').val(data);
					 //reference_id
				}
		});
		return false;
	});

</script>

<script src="../../../erp_khatiart/js/jquery/jquery.js"></script>

<script>

 $(document).ready(function(e) {
    var slc_stock_master = $('#slc_stock_master').val();
		stock_type(slc_stock_master);
});
	
	function consumable_type()
	{
			$("#pro_cat_id_row").hide();
			$("#collection_id_row").hide();
			$("#range_span").hide();
			$("#pro_design_id_row").hide();
			$("#finish_code_id_row").hide();	
			$("#consumable_name").show();
			$("#consumable_cat").show();
	}
	
	function product_type()
	{
			$("#pro_cat_id_row").show();
			$("#collection_id_row").show();
			$("#range_span").show();
			$("#pro_design_id_row").show();
			$("#finish_code_id_row").show();	
			$("#consumable_name").hide();
			$("#consumable_cat").hide();
	}
	
	
	function stock_type(slc_stock_master)
		{
			if(slc_stock_master == '102'){
				product_type();
			}
			else
			{
				consumable_type();
			}		
		}
	$('body').on('change','#slc_stock_master',function(){
		var slc_stock_master = $(this).val();
		stock_type(slc_stock_master);
		
		// reset drop down lists
		$('#slc_master option:eq(0)').attr('selected','selected');
		$('#sub_master option:eq(0)').attr('selected','selected');
		$('#slc_category option:eq(0)').attr('selected','selected');
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
	});
	


</script>