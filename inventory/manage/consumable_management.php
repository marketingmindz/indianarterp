<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CONSUMABLE';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Consumable Management"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/consumable_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

// ----------------- image code ----------------------------

if (isset($_GET['consumable_code']))
{
	$selected_id = $_GET['consumable_code'];
	echo "<script> $(document).ready(function() { $('#slc_master option').not(':selected').attr('disabled', 'disabled');
	 }); </script>";
}
elseif(isset($_POST['selected_id']))
{
	$selected_id = $_POST['selected_id'];	
}


$upload_file = "";
if (isset($_FILES['pic']) && $_FILES['pic']['name'] != '') 
{
	$consumable_code = $_POST['consumable_code'];
	$result = $_FILES['pic']['error'];
 	$upload_file = 'Yes'; //Assume all is well to start off with
	$filename = company_path().'/consumableImage';
	if (!file_exists($filename))
	{
		mkdir($filename);
	}	
	$filename .= "/".item_img_name($consumable_code).".jpg";
	
	//But check for the worst 
	if ((list($width, $height, $type, $attr) = getimagesize($_FILES['pic']['tmp_name'])) !== false)
		$imagetype = $type;
	else
		$imagetype = false;
	//$imagetype = exif_imagetype($_FILES['pic']['tmp_name']);
	if ($imagetype != IMAGETYPE_GIF && $imagetype != IMAGETYPE_JPEG && $imagetype != IMAGETYPE_PNG)
	{	//File type Check
		display_warning( _('Only graphics files can be uploaded'));
		$upload_file ='No';
	}
	elseif (!in_array(strtoupper(substr(trim($_FILES['pic']['name']), strlen($_FILES['pic']['name']) - 3)), array('JPG','PNG','GIF')))
	{
		display_warning(_('Only graphics files are supported - a file extension of .jpg, .png or .gif is expected'));
		$upload_file ='No';
	} 
	elseif ( $_FILES['pic']['size'] > ($max_image_size * 1024)) 
	{ //File Size Check
		display_warning(_('The file size is over the maximum allowed. The maximum size allowed in KB is') . ' ' . $max_image_size);
		$upload_file ='No';
	} 
	elseif (file_exists($filename))
	{
		$result = unlink($filename);
		if (!$result) 
		{
			display_error(_('The existing image could not be removed'));
			$upload_file ='No';
		}
	}
	
	if ($upload_file == 'Yes')
	{
		$result  =  move_uploaded_file($_FILES['pic']['tmp_name'], $filename);
	}
	$Ajax->activate('details');
 /* EOF Add Image upload for New Item  - by Ori */
}
if (isset($_POST['consumable_code']) && file_exists(company_path().'/consumableImage/'.item_img_name($_POST['consumable_code']).".jpg")) 
{
	$consumable_image = $_POST['consumable_code'].".JPG";
}else{
	$consumable_image = 'No Image';
}

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['consumable_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The consumable name cannot be empty."));
		set_focus('consumable_name');
	}
	if (strlen($_POST['consumable_code']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Code cannot be empty."));
		set_focus('consumable_code');
	}
	
	if ($input_error !=1)
	{
    	if ($selected_id != -1) 
    	{
			$consu_size = $_POST['size_w'].",".$_POST['size_d'].",".$_POST['size_h'];
		    update_consumable_master($selected_id, $_POST['master_id'],
				$_POST['company_id'],	$_POST['unit_id'], 
				$_POST['principal_id'], $_POST['consumable_code'], 
				$_POST['consumable_name'], $consu_size,
				$_POST['weight'], $_POST['cbm'],	$_POST['gsm'],	$_POST['description'],
				$_POST['document'], $consumable_image);
			display_notification(_('Selected consumable management has been updated'));
    	} 
    	else 
    	{
			$consu_size = $_POST['size_w'].",".$_POST['size_d'].",".$_POST['size_h'];
		    add_consumable_master($_POST['master_id'],
				$_POST['company_id'],	$_POST['unit_id'], 
				$_POST['principal_id'], $_POST['consumable_code'], 
				$_POST['consumable_name'], $consu_size, 
				$_POST['weight'], $_POST['cbm'],	$_POST['gsm'],	
				$_POST['description'],	$_POST['document'], $consumable_image);
			display_notification(_('New consumable management has been added'));
    	}
		$Mode = 'RESET';
	}
}


if ($Mode == 'Delete')
{
		delete_consumable_master($selected_id);
		display_notification(_('Selected consumable has been deleted'));	
	$Mode = 'RESET';
}

/*
if (list_updated('mb_flag')) {
	$Ajax->activate('details');
}*/

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//----------------------------------------------------------------------------------
/*$result1 = get_generatecode();
while ($myrow1 = db_fetch($result1)) 
{
	
	$con_id = $myrow1['consumable_id'];
}*/
echo "<input type='hidden' id='cons_id' value=''>";
//$result = get_item_categories(check_value('show_inactive'));
$result = get_consumable_master(check_value('show_inactive'));
start_form(true);

//---------------------------- start table ------------------------------------------------------

div_start('details');
/** detail **/

start_outer_table(TABLESTYLE2);

table_section(1);
table_section_title(_("Consumable Master"));
if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing item category
		$myrow = get_consumable_master_row($selected_id);

		$_POST['consumable_id'] = $myrow["consumable_id"];
		$_POST['master_id']  = $myrow["master_id"];
		$_POST['company_id']  = $myrow["company_id"];
		$_POST['unit_id']  = $myrow["unit_id"];
		$_POST['principal_id']  = $myrow["principal_id"];
		$_POST['consumable_code']  = $myrow["consumable_code"];
		$_POST['consumable_name']  = $myrow["consumable_name"];
		$cons_size  = $myrow["size"];
		$con_z = explode(',', $cons_size);
		$_POST['size_w'] = $con_z[0];
		$_POST['size_h'] = $con_z[1];
		$_POST['size_d'] = $con_z[2];
		$_POST['weight']  = $myrow["weight"];
		$_POST['cbm']  = $myrow["cbm"];
		$_POST['gsm']  = $myrow["gsm"];
		$_POST['description']  = $myrow["description"];
		$_POST['document']  = $myrow["document"];
		$_POST['pic']  = $myrow["consumable_image"];
	} else{
		$myrow = get_consumable_master_row($selected_id);

		$_POST['consumable_id'] = $myrow["consumable_id"];
		$_POST['master_id']  = $myrow["master_id"];
		$_POST['company_id']  = $myrow["company_id"];
		$_POST['unit_id']  = $myrow["unit_id"];
		$_POST['principal_id']  = $myrow["principal_id"];
		$_POST['consumable_code']  = $myrow["consumable_code"];
		$_POST['consumable_name']  = $myrow["consumable_name"];
		$cons_size  = $myrow["size"];
		$con_z = explode(',', $cons_size);
		$_POST['size_w'] = $con_z[0];
		$_POST['size_h'] = $con_z[1];
		$_POST['size_d'] = $con_z[2];
		$_POST['weight']  = $myrow["weight"];
		$_POST['cbm']  = $myrow["cbm"];
		$_POST['gsm']  = $myrow["gsm"];
		$_POST['description']  = $myrow["description"];
		$_POST['document']  = $myrow["document"];
		$_POST['pic']  = $myrow["consumable_image"];		
	}
	hidden('selected_id', $selected_id);
	hidden('consumable_id');
} 
stock_master_list_row(_("Master:"), 'master_id', null, _('----Select---'));
text_row(_("Consumable Name:"), 'consumable_name', null, 30, null, '', '', '', 'id="consumable_name"');  
text_row(_("Weight:"), 'weight', null, 30, 30); 
text_row(_("Document:"), 'document', null, 30, 30);
stock_units_list_row(_("Units:"), 'unit_id', null);
stock_principal_list_row(_("Principal Type:"), 'principal_id', null);
file_row(_("Image File (.jpg)") . ":", 'pic', 'pic');
/** image code **/
	$consu_img_link = "";
	if (isset($_POST['consumable_code']) && file_exists(company_path().'/consumableImage/'.item_img_name($_POST['consumable_code']).".jpg")) 
		{
			$consu_img_link .= "<img height='50' width='80' id='blah' alt = '[".$_POST['consumable_code'].".jpg".
				"]' src='".company_path().'/consumableImage/'.item_img_name($_POST['consumable_code']).
				".jpg?nocache=".rand()."'"." width='80' height='80' border='0'>";
			
		} 
		else 
		{
			$consu_img_link .= "<img id='blah' src='".company_path().'/images/img_not.png'."' alt='your image' width='80' height='80' />";
		}
	label_row("",$consu_img_link);
/** image code **/

table_section(2);

table_section_title(_("Consumable Master"));

stock_company_list_row(_("Make/Brand:"), 'company_id', null, _("- Select Brand -"));
text_row_three(_("Size:"), 'size_w','size_d','size_h', null, null, null,'size_w','size_d','size_h');
text_row(_("CBM:"), 'cbm', null, 30, 30, '','','','id="cbm" onclick="generateCBM()"'); 
text_row(_("GSM:"), 'gsm', null, 30, 30); 
text_row(_("Code:"), 'consumable_code', null, 30, 30, '','','','id="consumable_code" readonly="true"');
if ($selected_id == -1)
{
	button_code_generate( _(""),'generate_code', 'Generate Code','','','','','','id="generate_code" onclick="generateCode()"');
}
textarea_row(_('Description:'), 'description', null, 42, 3);

end_outer_table(1);
div_end();
submit_add_or_update_center($selected_id == -1, '', 'both');



/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';



end_form();

end_page();

?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "code_gen.php",
			method: "POST",
            data: { id : slc_master },
			success: function(data){
				    //alert(data);
					$("#cons_id").val(data);
				}
		});
		return false;
	});
	
	$(function() {
			$("#pic").on("change", function()
			{
				var files = !!this.files ? this.files : [];
				if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		 
				if (/^image/.test( files[0].type)){ // only image file
					var reader = new FileReader(); // instance of the FileReader
					reader.readAsDataURL(files[0]); // read the local file
		 			if(fileSize > 300){
						alert("File size must be less than 300 kb");
						return false;
					}
					reader.onloadend = function(){ // set image data as background of div
						$('#blah').attr('src', this.result);
					}
				}
			});
		});
    </script>
<script type="text/javascript">
function generateCode(){
   var consumble_name = document.getElementById('consumable_name').value;
    var consumble_id = document.getElementById('cons_id').value;
   document.getElementById('consumable_code').value = consumble_id;
} 
function generateCBM(){
   var size_w = parseInt(document.getElementById('size_w').value);
    var size_h = parseInt(document.getElementById('size_h').value);
	 var size_d = parseInt(document.getElementById('size_d').value);
	 var cbm_size = (size_w*size_h*size_d)/1000000;
   document.getElementById('cbm').value = cbm_size;
} 
</script>