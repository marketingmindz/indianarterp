<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CONSUMABLE';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/inventory/includes/db/electrical_db.inc");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Consumable Master Management"), false, false, "", $js);
}


global $consum_name;
if (isset($_GET['id']))
{
	$c_sankhya = 0;
	$selected_id = trim($_GET['id']);
	if(isset($_REQUEST['cons_id']))
	{
		$c_sankhya = trim($_REQUEST['cons_id']);
		delete_cosumable_category_master($c_sankhya);
		header('Location: '.$path_to_root.'/inventory/manage/master_category.php?id='.$selected_id); 
		exit(1);  
	}
	$consum_name = $selected_id;
	$myrow = get_category_master_row($consum_name);
	$master_name = $myrow['master_id'];
	/*if($selected_id == 'hardware'){
		$consum_name = Hardware;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
	}
	elseif($selected_id == 'electrical'){
		$consum_name = Electrical;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
		
	}
	elseif($selected_id == 'stationary'){
		$consum_name = Stationary;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
	}
	elseif($selected_id == 'chemical'){
		$consum_name = Chemical;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
	}
	elseif($selected_id == 'fabric'){
		$consum_name = Fabric;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
	}else{
		$consum_name = Packaging;
		$myrow = get_category_master_row($consum_name);
		$master_name = $myrow['master_id'];
	}*/
	$_SESSION['master_name_search'] = $master_name;
}

if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} 
	$Ajax->activate('orders_tbl');
}
//---------------------------------------------------------------------------------------------
?>
<div><a href="cons_export_excel.php?cons_id=<?php echo $master_name ?>" >Export To EXCEL</a></div>
<?php 
if (!@$_GET['popup'])
	start_form();

start_table(TABLESTYLE_NOBORDER, "style=min-width:320px;width:60%;");
start_row();
ref_cells(_("Consumable Code:"), 'order_number', '',null, '', true);
text_cells("Consumable Name","consumable_name",$_POST['consumable_name']);
submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
end_row();
end_table();
echo "<br>";
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];	
}

/*if (isset($_POST['SelectStockFromList']) &&	($_POST['SelectStockFromList'] != "") &&
	($_POST['SelectStockFromList'] != ALL_TEXT))
{
 	$selected_stock_item = $_POST['SelectStockFromList'];
}
else
{
	unset($selected_stock_item);
}*/

//---------------------------------------------------------------------------------------------
	

function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
	$consumable_id = $row["consumable_code"];	
	$myrow = get_electric_master_row($consumable_id);
	$consum_id = $myrow["consumable_id"];
  	return pager_link( _("Edit"),
		"/inventory/manage/consumable_management.php?consumable_code=" . $consum_id, ICON_EDIT);
}
function delete_link($row)
{
	global $consum_name;
	if (@$_GET['popup'])
		return '';
	$consumable_id = $row["consumable_code"];	
	
	$myrow = get_electric_master_row($consumable_id);
	$consum_id = $myrow["consumable_id"];
	$c_id = $consum_name;
	return pager_link(_("Delete"),
			"/inventory/manage/master_category.php?id=".$c_id."&cons_id=".$consum_id, ICON_DELETE);
}



//---------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
$sql = get_electrical_search($all_items);
$cols = array(
		_("Code"),  
		_("Consumable Name"), 
		_("Brand Name"),
		_("Master Name"),
		_("Principal Type"),
		_("Unit"), 
		_("Weight"),
		_("Size"),
		_("CBM"),
		_("GSM"),
		_("Document"),
		_("Image"),
		array('insert'=>true, 'fun'=>'edit_link'),
		/*array('insert'=>true, 'fun'=>'delete_link'),*/
);

/*if (get_post('StockLocation') != $all_items) {
	$cols[_("Location")] = 'skip';
}*/
//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}	
?>
