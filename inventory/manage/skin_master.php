<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SKIN';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Skin Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/skin_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['skin_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Skin name cannot be empty."));
		set_focus('skin_name');
	}
	if (strlen($_POST['skin_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Skin Reference cannot be empty."));
		set_focus('skin_reference');
	}

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_update_skin = check_update_skin($_POST['skin_reference']);
		    if($check_update_skin)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			    update_skin_master($selected_id, $_POST['skin_name'], $_POST['skin_reference'],$_POST['skin_description']);
			    display_notification(_('Selected skin has been updated'));
			  }
    	} 
    	else 
    	{
			$check_skin = check_skin($_POST['skin_reference']);
		    if($check_skin)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
    		     add_skin_master($_POST['skin_name'], $_POST['skin_reference'],$_POST['skin_description']);
			     display_notification(_('New skin has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_skin_master($selected_id);
		display_notification(_('Selected skin has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_skin_master(check_value('show_inactive'));

start_form();
start_table(TABLESTYLE, "width=50%");

$th = array(_('Skin Name'),_('Skin Reference'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["skin_name"]);
	label_cell($myrow["skin_reference"]);
	inactive_control_cell($myrow["skin_id"], $myrow["inactive"], 'skin_master', 'skin_id');
 	edit_button_cell("Edit".$myrow['skin_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['skin_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2);

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code

		$myrow = get_skin_master($selected_id);

		$_POST['skin_name']  = $myrow["skin_name"];
		$_POST['skin_reference']  = $myrow["skin_reference"];
		$_POST['skin_description']  = $myrow["skin_description"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Skin Name:"), 'skin_name', null, 40, 40);
text_row(_("Skin Ref.:"), 'skin_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec"');
textarea_row(_('Description:'), 'skin_description', null, 50, 10);

//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
<script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
   
</script>