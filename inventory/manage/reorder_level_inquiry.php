<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FINISH_DETAILS';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/reorder_level_db.inc");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Opening Stock List"), false, false, "", $js);
}


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} 
	$Ajax->activate('orders_tbl');
	
}


if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}



if(isset($_REQUEST['stock_id']))
	{
		$stock_id = trim($_REQUEST['stock_id']);
		delete_opening_stock($stock_id);
		header('Location: '.$path_to_root.'/inventory/manage/opening_stock_list.php?'); 
		exit(1);  
	}
	


	

function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
	$stock_id = $row['open_stock_id'];
  	return pager_link( _("Edit"),
		"/inventory/manage/opening_stock_master.php?stock_id=".$stock_id."&redirect=1", ICON_EDIT);
		
}

function delete_link($row)
{
	if (@$_GET['popup'])
		return '';
	$stock_id = $row['open_stock_id'];
	return pager_link(_("Delete"),
			"/inventory/manage/opening_stock_list.php?stock_id=".$stock_id, ICON_DELETE);
}

$sql = get_opening_stock_search(!@$_GET['popup'] ? $_POST['supplier_id'] : ALL_TEXT);


if (!@$_GET['popup'])
start_form();
start_outer_table(TABLESTYLE2, "style=min-width:320px;width:60%;");
	table_section(1);
	design_location_list_row(_("Location:"), 'location_id', null, _('------Select Location-----'));
	if($_POST['location_id'] == '-1'){
		design_work_center_list_row(_("Work Center:"), 'work_center_id', null, _('-----Select Work Center----'));
	}
	else
	{
		stock_work_center_list_row(_("Work Center:"), 'work_center_id', null, _('-----Select Work Center----'));
	}
	
	/*if($_POST['work_center_id'] == '-1'){
		design_production_list_row(_("Production Team:"), 'pro_team_id', null, _('--Select Production Team--'));
	}
	else
	{
		stock_production_list_row(_("Production Team:"), 'pro_team_id', null, _('--Select Production Team--'));	
	}*/
	table_section(2);
	start_row();
		stock_master_list_cells("Consumable Type", 'cons_type', null, _('----Select---'));
	end_row();
	start_row();
		if($_POST['cons_type'] == '-1'){
			sub_consumable_master_list_cells("Consumable Category", 'cons_select',null, _('----Select---'));
		}else{
			customer_consumable_list_cells("Consumable Category", $_POST['cons_type'],'cons_select', null, false, true, true, true);
		}
	end_row();
end_outer_table(1);

start_table(TABLESTYLE_NOBORDER, "style=min-width:320px;width:60%; 'align = centre'");
	start_row();
		submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
	end_row();
end_table(1);

$cols = array(
		_("Stock ID")=> array('ord'=>''),
		_("Location"),
		_("Work Center"),
		_("Consumable Name"),
		_("Consumable Category"),
		_("unit"),
		_("Reorder Level"),
		_("Quantity"),
		array('insert'=>true, 'fun'=>'edit_link'),
);
$cols[_("Stock ID")] = 'skip';
$table =& new_db_pager('orders_tbl', $sql, $cols);
$table->width = "80%";
display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}
?>

<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});

/*$(document).on('change','#slc_work_center',function(){
	//alert("data is synchronization successfully .... ")
	var slc_work_center = $(this).val();
	$.ajax({
		url: "slc_center_calling.php",
		method: "POST",
		data: { id : slc_work_center},
		success: function(data){
				var select_val = $('#slc_production');
				select_val.empty().append(data);
			}
	});
	return false;
});*/

$(document).on('change','#slc_master',function(){
//alert("data is synchronization successfully .... ");
	var slc_master = $(this).val();
	$.ajax({
		url: "sub_master_calling.php",
		method: "POST",
		data: { id : slc_master},
		success: function(data){
				var select_val = $('#sub_master');
				select_val.empty().append(data);
			}
	});
	return false;
});

</script>