<?php
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
include_once($path_to_root . "/inventory/includes/finish_product_class.inc");

//print_r($_SESSION['fab_part']);die;


if(isset($_SESSION['fab_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['fab_part']);

}else{
  $_SESSION['fab_part'] = new finish_order;
}

$fab_ref1 = "";



foreach($_SESSION['fab_part']->line_items as $key => $value){
	$fabric_id = $value->fabric_name;
	$sql = "SELECT `fabric_reference` FROM `".TB_PREF."fabric_master` WHERE `fabric_id` = ".db_escape($fabric_id)." ";
	$query = db_query($sql,"get the f_ref");
	 
	while($row = db_fetch($query,MYSQL_NUM))
	{
			 
			  $fab_ref = $row['0'];
	} 
	$fab_ref1 .= $fab_ref.",";
}

$val_t = rtrim(($fab_ref1),",");

$master_id = ""; 
	if(isset($_REQUEST['id']))
   {
	    $design_id = trim($_REQUEST['id']);
	
   }
   if(isset($_REQUEST['wid']))
   {
	    $wood_id = trim($_REQUEST['wid']);
	
   }
   if(isset($_REQUEST['cid']))
   {
	    $color_id = trim($_REQUEST['cid']);
	
   }
   if(isset($_REQUEST['fid']))
   {
	    $finish_id = trim($_REQUEST['fid']);
	
   }   
   if(isset($_REQUEST['struc_wood_id']))
   {
	    $struc_wood_id = trim($_REQUEST['struc_wood_id']);
	
   }
    if(isset($_REQUEST['no_setting']))
   {
	    $no_setting = trim($_REQUEST['no_setting']);
	
   }
   
 		$sql2 = "SELECT `design_code` FROM `".TB_PREF."design_code` WHERE `design_id` = ".db_escape($design_id)." ";
		$sql3 = "SELECT `wood_reference` FROM `".TB_PREF."wood_master` WHERE `wood_id` = ".db_escape($wood_id)." ";
		$sql4 = "SELECT `color_reference` FROM `".TB_PREF."color_master` WHERE `color_id` = ".db_escape($color_id)." ";
		$sql5 = "SELECT `finish_reference` FROM `".TB_PREF."finish_master` WHERE `finish_id` = ".db_escape($finish_id)." ";
		$sql6 = "SELECT `wood_reference` FROM `".TB_PREF."wood_master` WHERE `wood_id` = ".db_escape($struc_wood_id)." ";
		$query2 = db_query($sql2,"get the d_code");
		$query3 = db_query($sql3,"get the w_code");
		$query4 = db_query($sql4,"get the c_code");
		$query5 = db_query($sql5,"get the f_code");
		$query6 = db_query($sql6,"get the sow_code");
		while($row = db_fetch($query2,MYSQL_NUM))
		   {
			  $design_code = $row['0'];
			 
		   }   
		   while($row1 = db_fetch($query3,MYSQL_NUM))
		   {
			  $wood_name = $row1['0'];
			 
		   }   
		   while($row2 = db_fetch($query4,MYSQL_NUM))
		   {
			  $color_name = $row2['0'];
			 
		   }   
		   while($row3 = db_fetch($query5,MYSQL_NUM))
		   {
			  $finish_name = $row3['0'];
			 
		   }
		      
		    while($row4 = db_fetch($query6,MYSQL_NUM))
		   {
			  $struc_wood_name = $row4['0'];
			 
		   }   
		   $finish_code = $design_code."-".$wood_name.$color_name.$finish_name;
		   
		   if($struc_wood_name!= ''){
			   $finish_code = $finish_code.$struc_wood_name;	
		   }
		   if($no_setting !=''){
				$finish_code = $finish_code.$no_setting;	
		   }
		   if($val_t ==''){
			   $finish_code = $finish_code;
		   }else{
			 $finish_code = $finish_code."-".$val_t;
		   }
		   
   			//echo $finish_code;
			
			
			
			
#************fabric combination custom work start here************ 

#array comparision custom function here
function array_equal($a, $b) {
	return (is_array($a) && is_array($b) && array_diff($a, $b) === array_diff($b, $a));
	return 1;
}

if(isset($_POST['skin_id']) && $_POST['skin_id'] == 30){
	#geting current fabric combinaton from sesssion
	foreach ($_SESSION['fab_part'] as $key=>$value) {
		//print_r($value);
		foreach ($value as $key2=>$value2) {
			$fabric_array = (array) $value2;
			$fabric_ids[] = $fabric_array['fabric_name']; 	
		}
	}
}

if($fabric_ids){
#Get existing fabric combination name
	$sql_fabcode = "SELECT fab_code_id,fabric_comb FROM ".TB_PREF."fabric_code group by fab_code_id ";
	$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
	
	$fab_comb_for_finish_pro = '';
	$id_fabcode = '';
	while ($row_fabcode = db_fetch($result_fabcode)) {
		$id_fabcode =  $row_fabcode["fab_code_id"];
		//display_error( _('***'.$id_fabcode));
		
		
			#Get each fabric combination array
			$id_fabcomb = array();
			$sql_fabcomb = "SELECT fabric_code_name FROM ".TB_PREF."fabric_combination where fab_code_id =".$id_fabcode;
		 	$result_fabcomb = db_query($sql_fabcomb, "One of the fabric combination records could not be get");
			while ($row_fabcomb = db_fetch($result_fabcomb)) {
				$id_fabcomb[] =  $row_fabcomb["fabric_code_name"];
			} 
			
			/*foreach ($id_fabcomb as $id_fabcombs)
			 {
				display_error( _("--".$id_fabcombs));
			 }*/
			 
			#Compare new combination with existing combination
			$result_diff = array_equal($fabric_ids,$id_fabcomb);
			
			//display_error( _('====='.$result_diff));
			if($result_diff == 1){
				//display_error( _('Array are  same'));	
				$fab_comb_for_finish_pro = $row_fabcode["fab_code_id"];
				 break;
			}
			
	}
	
	
	if($fab_comb_for_finish_pro == ''){
		#Insert new fabric combination in 0_fabric_code
		$new_fabcode = 'F'.($id_fabcode+1);
	}else{
		$sql_fabcode = "SELECT fab_code_id,fabric_comb FROM ".TB_PREF."fabric_code where fab_code_id = '".$fab_comb_for_finish_pro."' ";
		$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
		$row_fabcode = db_fetch($result_fabcode);
		$new_fabcode = $row_fabcode["fabric_comb"];;	
	}
	
	echo $finish_code .= "-".$new_fabcode ;
}
else{
	echo $finish_code;
}


?>