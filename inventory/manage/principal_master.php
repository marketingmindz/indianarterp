<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PRINCIPAL';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Principal Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/principal_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Principal Master Name cannot be empty."));
		set_focus('name');
	}


	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_range = check_principal($_POST['name']);
		    if($check_range)
             {
	             display_notification(_('Principal Master already exist.'));	 
	          }else{ 
			    update_principal($selected_id, $_POST['name']);
			    display_notification(_('Selected Principal Master has been updated'));
			  }
    	} 
    	else 
    	{
			$check_range = check_principal($_POST['name']);
		    if($check_range)
             { 
	             display_notification(_('Principal Master already exist.'));	 
	         }else{ 
    		     add_principal($_POST['name']);
			     display_notification(_('New Principal Master been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_principal($selected_id);
		display_notification(_('Selected Principal Master has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_principal(check_value('show_inactive'));

start_form();

start_table(TABLESTYLE2, "style=min-width:320px;width:40%;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		
		$myrow = get_principal($selected_id);

		$_POST['name']  = $myrow["name"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Principal Name:"), 'name', null, 40, 40);

end_table(1);
submit_add_or_update_center($selected_id == -1, '', 'both');



echo "<br>";
start_table(TABLESTYLE, "style=min-width:320px;width:40%;");

$th = array(_('Principal Master'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["name"]);
	inactive_control_cell($myrow["principal_id"], $myrow["inactive"], 'principal_master', 'principal_id');
 	edit_button_cell("Edit".$myrow['principal_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['principal_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

end_form();


end_page();


?>
