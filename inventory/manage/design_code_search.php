<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SEARCH_DESIGN';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/design_code.inc");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Design Code List"), false, false, "", $js);
}


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} 
	$Ajax->activate('orders_tbl');
}




	

if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

if(isset($_REQUEST['id']))
	{
		$code_id = trim($_REQUEST['id']);
		$get_id = get_desing_code_id($code_id);
		$get_design_id = $get_id["design_id"];
		delete_design($get_design_id);
		header('Location: '.$path_to_root.'/inventory/manage/design_code_search.php?'); 
		exit(1);  
	}
//-----------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------

?>
<div><a href="export_design_code_list.php">Export To EXCEl</a></div>
<?php
if (!@$_GET['popup'])
	start_form();

	start_table(TABLESTYLE_NOBORDER);
	start_row();
	ref_cells(_("Design Code / Product Name:"), 'order_number', '',null, '', true);
	submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
	end_row();
	end_table();
	
	start_table(TABLESTYLE_NOBORDER);
	start_row();
	
	end_row();
	end_table(1);
	//---------------------------------------------------------------------------------------------
	/*if (isset($_POST['order_number']))
	{
		$order_number = $_POST['order_number'];	
	}*/
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];
}




function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
	$design_code_id = $row['design_code'];
	$myrow = get_design_code_row($design_code_id);
	$design_id = $myrow['design_id'];
	unset($_SESSION['asb_part']);
	unset($_SESSION['con_part']);
  	return pager_link( _("Edit"),
		"/inventory/manage/design_code.php?ModifyDesignNumber=".$design_id  , ICON_EDIT);
		
}

function delete_link($row)
{
	if (@$_GET['popup'])
		return '';
	$design_id = $row['design_code'];
	return pager_link(_("Delete"),
			"/inventory/manage/design_code_search.php?id=". $design_id, ICON_DELETE);
}
/*function prt_link($row)
{
	return print_document_link($row['order_no'], _("Print"), true, 18, ICON_PRINT);
}
*/
//---------------------------------------------------------------------------------------------

$sql = get_design_search($all_items);

$cols = array(
		_("Design Code"),
		_("Product Name"),
		_("Range Name"),
		_("Category Name"), 
		_("Product Type"),
		_("Width"), 
		_("Depth"),
		_("Height"),
		_("Description"),//shubham changes
		array('insert'=>true, 'fun'=>'edit_link'),
		array('insert'=>true, 'fun'=>'delete_link'),
);


//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}	
?>
