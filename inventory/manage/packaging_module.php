<?php
error_reporting(0);
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PACKAGING';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
page(_($help_context = "Packaging Module"));
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/db/packaging_module_db.inc");
include_once($path_to_root . "/inventory/includes/packaging_module_class.inc");
include_once($path_to_root . "/inventory/includes/ui/packaging_module_ui.inc");


// unset($_SESSION['master_cart_section']);
//display_error(_(count($_SESSION['master_cart_section'])));
/*if(!isset($_SESSION['master_cart_section'])){
	$_SESSION['master_cart_section']= '';
}*/
if (isset($_GET['PackingingId'])){
    unset($_SESSION['packaging_part']);
	unset($_SESSION['part_section']);

}
if(isset($_SESSION['packaging_part']) && isset($_SESSION['part_section'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['packaging_part']);
	 fixObject($_SESSION['part_section']);
}else{
	$_SESSION['part_section'] = $_SESSION['packaging_part'] = new packaging_order;
}


if (isset($_GET['PackingingId']))
{
	
	$selected_id = $_GET['PackingingId'];
	$_SESSION['s_id'] = $selected_id;
	$result = read_packaging_header($selected_id);
	read_packaging_part($selected_id, $_SESSION['packaging_part']);
}else{
	$selected_id = -1;	

}

if (isset($_GET['finish_pro_id']))
{
  $selected_finish_pro_id = $_GET['finish_pro_id'];
  $result = read_finish($selected_finish_pro_id);

}
 
//------------ handle new packaging code ----------

function handle_new_packaging_consume_design()
{
	
	$check_cons_data = packaging_check_data();
	
	 if($check_cons_data){
		
	   	$_SESSION['packaging_part']->add_to_pacakaging_part(count($_SESSION['packaging_part']->line_items), $_POST['principle_type'],$_POST['code_id'],
		$_POST['unit'],$_POST['quantity'],$_POST['width'],$_POST['height'],$_POST['density']);
		
		unset_packaging_variables();
		line_packaging_start_focus();
	 } 
	
}

//-------------- end new packaging code ---------------
function handle_packing_delete_item($line_no)
{
	array_splice($_SESSION['packaging_part']->line_items, $line_no, 1);
	unset_packaging_variables();	
    line_packaging_start_focus();
}
//------------ update packaging code ------------
function handle_packaging_update_item()
{
   $_SESSION['packaging_part']->update_pacakaging_part($_POST['line_no'], $_POST['principle_type'],$_POST['code_id'],$_POST['unit'],$_POST['quantity'],
	$_POST['width'],$_POST['height'],$_POST['density']);
	unset_packaging_variables();
    line_packaging_start_focus();
}
//-------------- end update packaging code ---------------
//------------------- add carton part section -----------

function handle_new_carton_part_section()
{
	
	$allow_update = true;
	$qty = array();
	$get_qty = array();
	$de_id = $_POST['design_id'];
	
		if (count($_SESSION['part_section']->line_con) > 0)
		{

			foreach ($_SESSION['part_section']->line_con as $order_item) 
			{
					$qty[$order_item->part_name_id] += $order_item->part_qty;
			} 
		}
		$qty[$_POST['part_name_id']] +=$_POST['part_qty'];
		$get_qty = get_desgin_part_qty($_POST['part_name_id'],$de_id );
		$qty_part[$_POST['part_name_id']] = $get_qty['quantity'];	
		$q_key = key($qty_part);
		$p_key = key($qty);
		if (array_key_exists($q_key, $qty)) {
   				$total_qty = $qty_part[$q_key]-$qty[$q_key];
		}
		if($total_qty < 0){
			display_error(_("stock is over"));
			$allow_update = false;
		}
		//print_r($_SESSION['t_sess']);
		if($_SESSION['t_sess'] != ''){
		
				if (array_key_exists($p_key, $_SESSION['t_sess'])) {
						$to_qty[$q_key] = $qty[$p_key]+$_SESSION['t_sess'][$q_key];
				
				}	
		}
		if (array_key_exists($q_key, $to_qty)) {
   				$to_qty = $qty_part[$q_key]-$to_qty[$q_key];
		};
		
		
		if($to_qty < 0){
			display_error(_("stock is over"));
			$allow_update = false;
		}
		if($allow_update){
			$_SESSION['part_section']->add_to_carton_part(count($_SESSION['part_section']->line_con), $_POST['part_name_id'],$_POST['part_qty']);
			unset_form_part_variables();
			line_part_start_focus();
		}else{
			display_warning(_("The selected Part is already exist"));
		}
	 
	
}
//----------------------- end part section -------------

//------------------ update carton part section ----------
function handle_carton_part_section_update()
{
   $_SESSION['part_section']->update_carton_part_item($_POST['line_no'], $_POST['part_name_id'],$_POST['part_qty']);
	unset_form_part_variables();
    line_part_start_focus();
}
//-------------------- end part section --------------

//------------------- delete part section -------------
function handle_carton_part_section_delete($line_no)
{
	$l = $line_no;
	unset($_SESSION['part_section']->line_con[$line_no]);
	unset_form_part_variables();	
    line_part_start_focus();
}
//-------------------- end part section ---------------

//-------------------- start carton manage section ---------------
function handle_carton_manage_section(){
	

	$session_photo = '';
	$session_lable = $_POST['label_id_0'].','.$_POST['label_id_1'].','.$_POST['label_id_2'].','.
	$_POST['label_id_3'].','.$_POST['label_id_4'].','.$_POST['label_id_5'];

	for($photo_no=0;$photo_no<=5;$photo_no++){
		$photo_name = "pic_".$photo_no;
		$finish_image = $_FILES[$photo_name]['name'];
		$fimage_tmp= $_FILES[$photo_name]['tmp_name'];
		if(!$session_photo){
			$session_photo = $finish_image;
		}else{
			$session_photo .= ",".$finish_image;
		}
		$f_Imge = company_path().'/CartonImage/'.$finish_image;
		move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
	}


		//array_push($_POST['screenimages'], $session_photo);
		//array_push($_POST['screenlables'], $session_photo);
		$_POST['screenimages'] = $session_photo;
		$_POST['screenlables'] = $session_lable;
		$this_carton_part_section = $_SESSION['part_section']->line_con;
		$par_qty = array();
		$to_qty =array();
		foreach($this_carton_part_section as $par_val){
			$par_qty[$par_val->part_name_id] += $par_val->part_qty;
			$_SESSION['p_sess'][$par_val->part_name_id] =$par_qty[$par_val->part_name_id];
		}
		foreach($_SESSION['p_sess'] as $key => $s_v){
			if (array_key_exists($key, $_SESSION['p_sess'])) {			
					$to_qty[$key] += $_SESSION['p_sess'][$key];
			}
			$_SESSION['t_sess'][$key] += $to_qty[$key];
		}
	
	if($_POST['carton_id'] == '101'){
		if(!isset($_SESSION['master_cart_section']['master']['carton'])){
			$_SESSION['master_cart_section']['master']['carton'] = '';
			$_SESSION['master_cart_section']['master']['carton'][1] = $_POST;
			array_push($_SESSION['master_cart_section']['master']['carton'][1], $this_carton_part_section);
			$_SESSION['master_cart_section']['master']['part'][1] = $this_carton_part_section;
			$_SESSION['master_cart_section']['master']['photo'][1] = $session_photo;
			
		}else{
			$newcarton = count($_SESSION['master_cart_section']['master']['carton'])+1;
				
			$_SESSION['master_cart_section']['master']['carton'][$newcarton] = $_POST;
			array_push($_SESSION['master_cart_section']['master']['carton'][$newcarton], $this_carton_part_section);
			$_SESSION['master_cart_section']['master']['part'][$newcarton] = $this_carton_part_section;
			$_SESSION['master_cart_section']['master']['photo'][$newcarton] = $session_photo;
			
		}
	}else{
		if(!isset($_SESSION['master_cart_section']['inner'])){
			$_SESSION['master_cart_section']['inner']['carton'] = '';
			$_SESSION['master_cart_section']['inner']['carton'][1] = $_POST;
			array_push($_SESSION['master_cart_section']['inner']['carton'][1], $this_carton_part_section);
			$_SESSION['master_cart_section']['inner']['part'][1] = $this_carton_part_section;
			$_SESSION['master_cart_section']['inner']['photo'][1] = $session_photo;
			
		}else{
			$newcarton = count($_SESSION['master_cart_section']['inner']['carton'])+1;
			$_SESSION['master_cart_section']['inner']['carton'][$newcarton] = $_POST;
			array_push($_SESSION['master_cart_section']['inner']['carton'][$newcarton], $this_carton_part_section);
			$_SESSION['master_cart_section']['inner']['part'][$newcarton] = $this_carton_part_section;
			$_SESSION['master_cart_section']['inner']['photo'][$newcarton] = $session_photo;
			
		}	
	}
	
	
	unset($_SESSION['p_sess']);
	unset($_SESSION['part_section']->line_con);
	#unset carton and part section
	unset_carton_part_variables();
}


//------------ end carton manage section ----------


function packaging_module_code_check_data()
{
	if(!get_post('finish_code_id')) {
		display_error( _("Finish code cannot be empty."));
		set_focus('finish_code_id');
		return false;
	}
	
    return true;	
}


function packaging_check_data()
{ 
	if(get_post('principal_type') == -1) {
		display_error( _("Principal Type cannot be empty."));
		set_focus('principal_type');
		return false;
	}
	if(get_post('code_id') == 0) {
		display_error( _("Code id cannot be empty."));
		set_focus('code_id');
		return false;
	}
	if(!get_post('unit')) {
		display_error( _("Unit cannot be empty."));
		set_focus('unit');
		return false;
	}
	if (!check_num('quantity', 0))
    {
	   	display_error(_("The quantity entered must be numeric and not less than zero."));
		set_focus('quantity');
	   	return false;	   
    }
	/*if (!check_num('width', 0))
    {
	   	display_error(_("The width entered must be w,h,d"));
		set_focus('width');
	   	return false;	   
    }*/
   return true;	
}

//------------------- line start table ------------

function line_packaging_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  //set_focus('cons_type');
}
//------------------------ add all value --------------------------------
function line_part_start_focus() {
  global 	$Ajax;

  $Ajax->activate('con_table');
  //set_focus('cons_type');
}
function unset_form_part_variables() {
	
	unset($_POST['part_name_id']);
    unset($_POST['part_qty']);

}

function unset_packaging_variables() {
	
	unset($_POST['principle_type']);
    unset($_POST['code_id']);
    unset($_POST['unit']);
    unset($_POST['quantity']);
	unset($_POST['width']);
	unset($_POST['height']);
	unset($_POST['density']);

}

function unset_carton_part_variables() {
	
	unset($_POST['carton_id']);
    unset($_POST['cat_qty']);
    unset($_POST['mar_weight']);
    unset($_POST['mar_height']);
	unset($_POST['mar_density']);
	unset($_POST['carton_type_id']);
    unset($_POST['gross_weight']);
    unset($_POST['code']);
	unset($_POST['pkg_s_weight']);
	unset($_POST['carton_type_id']);
    unset($_POST['gross_weight']);
    unset($_POST['pkg_s_height']);
	unset($_POST['pkg_s_density']);
	unset($_POST['car_s_weight']);
    unset($_POST['car_s_height']);
	unset($_POST['car_s_density']);
	unset($_POST['net_weight']);
}


function handle_commit_packaging_product()
{
	
	
	$header_design = array();
	
	$header_design['packaging_id'] = trim($_POST['packaging_id']);
	$header_design['category_id'] = trim($_POST['category_id']);
	$header_design['range_id'] = trim($_POST['range_id']);
	$header_design['design_id'] = trim($_POST['design_id']);
	$header_design['finish_code_id'] = trim($_POST['finish_code_id']);
	
	$packing_sessiong = &$_SESSION['packaging_part'];
	$mstercarton = &$_SESSION['master_cart_section'];
	
	$order_no = add_packaging_module($header_design,$packing_sessiong,$mstercarton);
	unset($_SESSION['packaging_part']);
	meta_forward($_SERVER['PHP_SELF']);
}

/* update packaging*/

function handle_update_commit_packaging()
{
	global $selected_id;
	$selected_id = $_SESSION['s_id'];
	$udpate_design = array();
	$udpate_design['category_id'] = trim($_POST['category_id']);
	$udpate_design['range_id'] = trim($_POST['range_id']);
	$udpate_design['design_id'] = trim($_POST['design_id']);
	$udpate_design['finish_code_id'] = trim($_POST['finish_code_id']);
	
	$pack_sec = &$_SESSION['packaging_part'];
	$order_no = update_packaging($selected_id, $udpate_design, $pack_sec);
	unset($_SESSION['packaging_part']);
}

//------------------- end line start table --------------



$id = find_submit('Delete');
if ($id != -1)
{
	handle_packing_delete_item($id);
}
if (isset($_POST['FPackagingAddItem']))
{
	handle_new_packaging_consume_design();
}
if (isset($_POST['FPackagingUpdateItem'])){
	
	handle_packaging_update_item();
}

if (isset($_POST['FPackagingCancelItemChanges'])) {
	line_packaging_start_focus();
}



$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_carton_part_section_delete($id);
}
if (isset($_POST['CPartAddItem']))
{
	handle_new_carton_part_section();
}
if (isset($_POST['CPartUpdateItem'])){
 	handle_carton_part_section_update();
}
if (isset($_POST['CPartCancelItemChanges'])) {
	line_part_start_focus();
}

/*if (isset($_POST['Addtolist']))
{
	handle_commit_packaging_product();
}*/






if (isset($_POST['AddthisCarton']))
{
	handle_carton_manage_section();
}


if (isset($_POST['Addtolist']))
{
	handle_commit_packaging_product();
}
if (isset($_POST['UpdatePackaging']))
{
	handle_update_commit_packaging();
}

$myrow = $_POST['finish_code_id'];
if($myrow['finish_code_id']==''){
	echo "<input type='hidden' id='des_id' value=''>";
	unset($_SESSION['packaging_part']);
	unset($_SESSION['part_section']);
	unset($_SESSION['master_cart_section']);
	unset($_SESSION['p_sess']);
	unset($_SESSION['t_sess']);
}else{
 echo "<input type='hidden' id='des_id' value='".$myrow."'>";
}





//---------------------------- start table ------------------------------------------------------
start_form(true); 

display_packaging_hearder($_SESSION['PACKK']);
display_packaging_list_summary($_SESSION['packaging_part']);
display_packaging_carton($_SESSION['carton_list']);
display_carton_part_section_summary($_SESSION['part_section']);
display_packaging_screen($_SESSION['screen_list']);
display_carton_session_summary($_POST);

if ($selected_id != -1) 
{
	echo "<br><center>";
	/*submit_center_first('UpdatePackaging', _("Update"), _('Update to packaging code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to packaging code'), 'default');*/
	echo "</center>";
}
else
{
	submit_center_last('Addtolist', _("Add To List"), '', 'default');
	
}
/** detail **/


/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';


end_form();
?>

<!---------------- show pop window --------------------------->
<div id="display_carton">
<center><span class="headingtext">Carton:</span></center>
<div id="carton_table"><center><table class="tablestyle2" cellpadding="2" cellspacing="0">
<tbody><tr valign="top"><td>
<table class="tablestyle">
<tbody><tr><td class="label">Type:</td><td><span id="carton_type_d">
</span>
</td>
</tr>
    <tr id="mastercarton_d"><td class="label">Master Carton:</td><td>
            <span id="master_carton_id_d" ></span></td>
     </tr>
    <tr>
          <td class="label">Carton qty:</td><td><span id="cat_qty_d"></span></td>
    </tr>
   <tr>    <td class="label">Margin:</td><td>
			<label style="margin-left:10px;">W = <span id="mar_w_d"></span></label>
            <label style="margin-left:10px;">D = <span id="mar_h_d"></span></label>
            <label style="margin-left:10px;">H = <span id="mar_d_d"></span></label> 
            </td>
   </tr>
   <tr><td class="label">Carton type:</td>
		<td><span id="_carton_type_id_sel"><span id="carton_type_id_d"></span>
		</span> </td>
  </tr>
  <tr><td class="label">Gross weight :</td><td><span id="gross_weight_d"></td>
</tr>
</tbody></table>
</td><td style="border-left:1px solid #cccccc;">
<table class="tablestyle">
<tbody><tr><td class="label">Code:</td>
			<td><span id="code_d"></span></td>
  </tr>
  <tr><td class="label">Pkg. size for catoon:</td>
      <td><label style="margin-left:10px;">W = <span id="pkg_s_w_d"></span></label>
          <label style="margin-left:10px;">D = <span id="pkg_s_h_d"></span></label>
          <label style="margin-left:10px;">H = <span id="pkg_s_d_d"></span></label>
       </td>
   </tr>
   <tr><td class="label">Carton Size:</td>
       <td><label style="margin-left:10px;">W = <span id="car_s_w_d"></span></label>
       <label style="margin-left:10px;">D = <span id="car_s_h_d"></span></label>
       <label style="margin-left:10px;">H = <span id="car_s_d_d"></span></label>
      </td>
   </tr>
   <tr><td class="label">Net weight :</td>
       <td><span id="net_weight_d"></span></td>
    </tr>
</tbody></table>
</td></tr>
</tbody></table></center>

</div>

<center><span class="headingtext">Carton Part Sectoin</span></center>
<div id="con_table"><center>
<table class="tablestyle" colspan="7" width="40%" cellpadding="2" cellspacing="0">
        <tr>
            <td class="tableheader">Part Type</td>
            <td class="tableheader">Qty</td>
        </tr>
   </table>
<table class="tablestyle" id="part_section_table" colspan="7" width="40%" cellpadding="2" cellspacing="0">
        
   </table></center>
<br></div>

<div>
<center><span class="headingtext">Screen</span></center>
<center>
	<table class="tablestyle" id="screen_img_table"  cellspacing="5">
    	<tr>
        <td class="screen_img"><label id="label_0"></label><img src="" id="img_0" /> </td>
        <td class="screen_img"><label id="label_1"></label><img src="" id="img_1" /> </td>
        <td class="screen_img"><label id="label_2"></label><img src="" id="img_2"  /> </td>
        </tr>
        <tr>
        <td class="screen_img"><label id="label_3"></label><img src="" id="img_3"  /> </td>
        <td class="screen_img"><label id="label_4"></label><img src="" id="img_4"  /> </td>
        <td class="screen_img"><label id="label_5"></label><img src="" id="img_5"  /> </td>
        </tr>    
    </table>
</div>
</center>

<center> 
    <table class="tablestyle" >
        <tr><td class="tableheader"  onclick="$('#display_carton').hide();" style="cursor:pointer" ><strong>Close</strong></td>
        </tr>
   </table>
</center>
</div>


<!------------------ end pop window ---------------------------->
<style>
button#Addtolist {
	align-content: center;
	margin-left: 500px;
}
body{ position:relative; }
#display_carton {
display:none;
margin:auto !important;
top:10% !important;
height:500px;
background:#FDFCFF;
border:#000 2px solid;
position:fixed;
width:80%;
}

#screen_img_table
{
	height:200px !important;
}


.screen_img
{
	width:90px !important;
	height:90px !important;
}
.screen_img img
{
	max-width:100%;
	max-height: 100%;
}

</style>

<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
		$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_size.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
				var size = data.split(',');
					$('#pkg_s_w').val(size[0]);
					$('#pkg_s_d').val(size[1]);
					$('#pkg_s_h').val(size[2]);
				}
		});
		
	});

	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		if(design_id != '-1'){
			$.ajax({
				url: "get_carton_part_section.php",
				method: "POST",
				data: { f_id : design_id },
				success: function(data){
						$( "option" ).remove( ".part_extra" );
						$('#part_name_id').append(data);
					}
			});
		}else{
			$( "option" ).remove( ".part_extra" );
		}
		
	});

	$(document).on('change','#collection_id', function(){
		var coll_id = $('#collection_id').val();
		if(coll_id == 'NonCollection'){
			var rang_id = $('#range_id_code').val(-1);
		}
		
	});
/*
$( "#code" ).click(function() {
	
		var carton_type = $('#carton_type :selected').text();
		
		//var packaging_finish_code = $('#packaging_finish_code').val();
		var packaging_finish_code = $('#packaging_finish_code :selected').text();
		
		//alert(carton_type);
		//alert(packaging_finish_code);
		var result_code = packaging_finish_code.split('-');
		
    	//alert(result[0]);
		//alert(packaging_finish_code);
		//alert(carton_type);
		if(carton_type == 'Master Carton')
	    {
			var new_code = "MC" + result_code[0] + "-A";
			//alert(new_code);
			$('#code').val(new_code);
		}else
		{
			var new_code = "IC" + result_code[0] + "-A";
			//alert(new_code);
			$('#code').val(new_code);
		}
});	*/

$( "#code" ).click(function() {
	
		var carton_type = $('#carton_type').val();
		var packaging_finish_code = $('#packaging_finish_code').val();
		
		
		//alert(carton_type);
		if(packaging_finish_code != '-1'){
			$.ajax({
			url: "pack_carton_type_code.php",
			method: "POST",
            data: { id : packaging_finish_code,car_type : carton_type},
			success: function(data){
				
					$('#code').val(data);
					
				}
			});
		}
			
		
});	


$(document).on('change','#carton_type',function(){
		var carton_type = $("#carton_type").val();
		//alert(carton_type);
		 if(carton_type == '102'){
			$.ajax({
					url: "get_master_carton.php",
					method: "POST",
					data: { carton_type : carton_type },
					success: function(data){
						
						if(data == ''){
							alert('Not Master Carton is created yet!');
							$("#carton_type").val('101');
						}else{
							$('#master_carton_id').append(data);
							$("#mastercartontr").show();	
						}
							
						}
				});
			}else{
				$("#mastercartontr").hide();	
				$("#mastercartontr").val('-1');
			}
		
	});			
		

	//   -----  code by bajrang ---   carton size  - -------
$(document).on('click','#car_s_w',function(){
		var pkg_s_w = $('#pkg_s_w').val();
		var mar_w = $('#mar_w').val();
		var car_s_w = parseInt(pkg_s_w)+parseInt(mar_w);
		$('#car_s_w').val(car_s_w);
		
});
$(document).on('click','#car_s_d',function(){
		var pkg_s_d = $('#pkg_s_d').val();
		var mar_d = $('#mar_d').val();
		var car_s_d = parseInt(pkg_s_d) + parseInt(mar_d);
		$('#car_s_d').val(car_s_d);
		
});
$(document).on('click','#car_s_h',function(){
		var pkg_s_h = $('#pkg_s_h').val();
		var mar_h = $('#mar_h').val();
		var car_s_h = parseInt(pkg_s_h)+parseInt(mar_h);
		$('#car_s_h').val(car_s_h);
		
});	



//  code by bajrang  - --- for carton part section
$(document).on('change','#part_name_id',function(){
		var part_id = $('option:selected', this).attr('data-value');
		$('#part_qty').val(part_id);
		
		 
	});
	
	
/*
$(document).on('blur','#part_qty',function(){
		var part_id = $('option:selected', '#part_name_id').attr('data-value');
		var part_qty = $(this).val();
		if(part_qty > part_id)
		{
			alert("part quantity cannot be greater than "+part_id);
			$(this).val('');
			$(this).focus();
			return false;
		}
		else
		{
			return true;
		}
		
		 
	});*/
//----------------- show pop jquery -------------
function display_carton_part_summary(carton_val, path)
{
	//var carton_val = $("#display_carton").attr('href');
	$("#display_carton").show(); 
	//alert(carton_val);
	var dataObj = JSON.parse(carton_val);
	
	var master_carton_id = dataObj.master_carton_id;
	if(master_carton_id == -1)
	{
		$("#mastercarton_d").hide();
	}
	else
	{
		$('#mastercarton_d').show();
		$('#master_carton_id_d').html(master_carton_id);
	}
	$('#cat_qty_d').html(dataObj.cat_qty);
	$('#mar_w_d').html(dataObj.mar_weight);
	$('#mar_h_d').html(dataObj.mar_height);
	$('#mar_d_d').html(dataObj.mar_density);
	$('#net_weight_d').html(dataObj.gross_weight);
	$('#gross_weight_d').html(dataObj.net_weight);
	$('#code_d').html(dataObj.code);
	$('#pkg_s_w_d').html(dataObj.pkg_s_weight);
	$('#pkg_s_h_d').html(dataObj.pkg_s_height);
	$('#pkg_s_d_d').html(dataObj.pkg_s_density);
	
	//console.log(dataObj[0]);
	
	
	//var len = JSON.parse(dataObj[0]);
	var len = Object.keys(dataObj[0]).length+'';
	var index = 100;
	$('#part_section_table').empty();
	for(var k=0; k<len; k++)
	{
		var part_type  =dataObj[0][index].part_name_id;
		$.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { part_id : part_type },
			success: function(data){
					part_type = data;
				}	
			});
		$('#part_section_table').append("<tr><td>"+part_type+"</td><td>"+dataObj[0][index].part_qty+"</td></tr>");
		index++;
	}
	
	
	$('#car_s_w_d').html(dataObj.car_s_weight);
	$('#car_s_h_d').html(dataObj.car_s_height);
	$('#car_s_d_d').html(dataObj.car_s_density);
	
	//$('#carton_type_id_d').html(dataObj.carton_type_id);
	
	var carton_id = dataObj.carton_id;
	  $.ajax({
			url: "get_carton_type.php",
			method: "POST",
			data: { carton_id : carton_id },
			success: function(data){
				if(data != ''){
					$('#carton_type_d').html(data);
				}
					
				}
		});
		
	var carton_type_id  = dataObj.carton_type_id;
	  $.ajax({
			url: "get_carton_type_name.php",
			method: "POST",
			data: { carton_type_id : carton_type_id },
			success: function(data){
				if(data != ''){
					$("#carton_type_id_d").html(data);
				}
					
				}
		});
		
	var screenimages = dataObj.screenimages.split(',');
	var screenlables = dataObj.screenlables.split(',');
	for(var i=0; i<screenimages.length; i++){
     $("#img_"+i).attr('src',path+screenimages[i]);
	  //$("#label_"+i).html(screenlables[i]);
	  
	  var carton_type_id  = dataObj.carton_type_id;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { label_id : screenlables[i] },
			success: function(data){
				if(data != ''){
					$("#label_"+i).html(data);
				}
					
				}
		});
	  
	 }
	 
	 
	
}
//---------------- end pop jquery------------------

//----------------- show pop jquery -------------
function display_carton_part_summary_search(carton_val, carton_part, path)
{
	
	//var carton_val = $("#display_carton").attr('href');
	$("#display_carton").show(); 
	
	//alert(carton_val);
	var dataObj = JSON.parse(carton_val);
	
	var master_carton_id = dataObj.master_carton_id;
	if(master_carton_id == -1)
	{
		$("#mastercarton_d").hide();
	}
	else
	{
		$('#mastercarton_d').show();
		$('#master_carton_id_d').html(master_carton_id);
	}
	$('#cat_qty_d').html(dataObj.quantity);
	
	var mar_dimension = dataObj.margin_dimension.split(',');
	
	$('#mar_w_d').html(mar_dimension[0]);
	$('#mar_h_d').html(mar_dimension[1]);
	$('#mar_d_d').html(mar_dimension[2]);
	$('#net_weight_d').html(dataObj.gross_weight);
	$('#gross_weight_d').html(dataObj.net_weight);
	$('#code_d').html(dataObj.code);
	
	var pkg_dimension = dataObj.pkg_dimension.split(',');
	$('#pkg_s_w_d').html(pkg_dimension[0]);
	$('#pkg_s_h_d').html(pkg_dimension[1]);
	$('#pkg_s_d_d').html(pkg_dimension[2]);
	
	//console.log(dataObj[0]);
	
	
	//var len = JSON.parse(dataObj[0]);
	
	
	var car_dimension = dataObj.carton_dimension.split(',');
	$('#car_s_w_d').html(car_dimension[0]);
	$('#car_s_h_d').html(car_dimension[1]);
	$('#car_s_d_d').html(car_dimension[2]);
	
	//$('#carton_type_id_d').html(dataObj.carton_type_id);
	
	var carton_id = dataObj.carton_type_id;
	  $.ajax({
			url: "get_carton_type.php",
			method: "POST",
			data: { carton_id : carton_id },
			success: function(data){
				if(data != ''){
					$('#carton_type_d').html(data);
				}
					
				}
		});
		
	var carton_type_id  = dataObj.master_carton_id;
	  $.ajax({
			url: "get_carton_type_name.php",
			method: "POST",
			data: { carton_type_id : carton_type_id },
			success: function(data){
				if(data != ''){
					$("#carton_type_id_d").html(data);
				}
					
				}
		});
		
	var screenimages = dataObj.images.split(',');
	var screenlables = dataObj.lables.split(',');
	for(var i=0; i<screenimages.length; i++){
     $("#img_"+i).attr('src',path+screenimages[i]);
	  //$("#label_"+i).html(screenlables[i]);
	  
	  var carton_type_id  = dataObj.carton_type_id;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { label_id : screenlables[i] },
			success: function(data){
				if(data != ''){
					$("#label_"+i).html(data);
				}
					
				}
		});
	  
	 }
	 var dataPart = JSON.parse(carton_part);
	var len = dataPart.length;
	$('#part_section_table').empty();
	for(var k=0; k<len; k++)
	{
	 var part_type  = dataPart[k].part_type;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { part_id : part_type },
			success: function(data){
					part_type = data;
				}	
			});
		$('#part_section_table').append("<tr><td>"+part_type+"</td><td>"+dataPart[k].quantity+"</td></tr>");
		
	}
	
}

$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});
</script>