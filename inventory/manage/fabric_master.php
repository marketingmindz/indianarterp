<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FABRIC';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Fabric Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/fabric_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['fabric_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The fabric name cannot be empty."));
		set_focus('fabric_name');
	}
	if (strlen($_POST['fabric_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The fabric Reference cannot be empty."));
		set_focus('fabric_reference');
	}

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_update_fabric = check_update_fabric($_POST['fabric_reference']);
		    if($check_update_fabric)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			    update_fabric_master($selected_id, $_POST['fabric_name'], $_POST['fabric_reference'],$_POST['fabric_description']);
			    display_notification(_('Selected fabric master has been updated'));
			  }
    	} 
    	else 
    	{
			$check_fabric = check_fabric($_POST['fabric_reference']);
		    if($check_fabric)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
    		     add_fabric_master($_POST['fabric_name'], $_POST['fabric_reference'],$_POST['fabric_description']);
			     display_notification(_('New fabric master has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_fabric_master($selected_id);
		display_notification(_('Selected fabric master has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_fabric_master(check_value('show_inactive'));

start_form();
start_table(TABLESTYLE, "width=50%");

$th = array(_('Fabric Name'),_('Fabric Reference'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["fabric_name"]);
	label_cell($myrow["fabric_reference"]);
	inactive_control_cell($myrow["fabric_id"], $myrow["inactive"], 'fabric_master', 'fabric_id');
 	edit_button_cell("Edit".$myrow['fabric_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['fabric_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2);

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code

		$myrow = get_fabric_master($selected_id);

		$_POST['fabric_name']  = $myrow["fabric_name"];
		$_POST['fabric_reference']  = $myrow["fabric_reference"];
		$_POST['fabric_description']  = $myrow["fabric_description"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Fabric Name:"), 'fabric_name', null, 40, 40);
text_row(_("Fabric Ref.:"), 'fabric_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec"');
textarea_row(_('Description:'), 'fabric_description', null, 50, 10);

//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
<script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
   
</script>