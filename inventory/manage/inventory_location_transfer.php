<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_INVENTORYLOCATIONTRANSFER';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/inventory_tranfer_class.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/ui/inventory_loction_transfer_ui.inc");
include_once($path_to_root . "/inventory/includes/db/inventory_location_transfer_db.inc");

$js = "";

 //  code by bajrang   17/07/2015
if(!isset($_POST['session_var']))
{
	if(!isset($_POST['ConsumeAddItem']) && !isset($_POST['ProductAddItem']))
	{
		if(!isset($_POST['ConsumeUpdateItem']) && !isset($_POST['ProductUpdateItem']))
		{
			unset($_SESSION['cons_part']);
			unset($_SESSION['pro_part']);
		}
	}
}
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Item Location Transfer"), false, false, "", $js);


simple_page_mode(true);


if(isset($_SESSION['cons_part']) || isset($_SESSION['pro_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['cons_part']);
	 fixObject($_SESSION['pro_part']);
}else{
	
	
   $_SESSION['cons_part'] = $_SESSION['pro_part'] = new item_inventory_transfer;

}


//------------ handle new consumable part ------------

function handle_new_consumable()
{
	
	$check_data = consumable_check_data();
	if($check_data){
		$_SESSION['cons_part']->add_to_cons_part(count($_SESSION['cons_part']->line_con),$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],$_POST['from_quantity'],$_POST['to_quantity']);
		$count_sessiong = count($_SESSION['cons_part']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}
//-------------- end new consumable part ---------------

//------------ handle new product type  ------------
function handle_new_product()
{
	$check_pro_data = product_check_data();
	 if($check_pro_data){
	   	$_SESSION['pro_part']->add_to_product_part(count($_SESSION['pro_part']->line_items),$_POST['category_id'],$_POST['collection_id'],$_POST['range_id'],$_POST['design_id'],$_POST['finish_code_id'],$_POST['pro_unit'],$_POST['pro_qty']);
		unset_form_product_variables();
		line_start_product_focus();
	 }
}
//-------------- end new product type ---------------

//------------ delete consumable type ------------
function handle_consumable_delete_item($line_no)
{
	array_splice($_SESSION['cons_part']->line_con, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end delete consumable type ---------------
//------------ delete product type ------------
function handle_product_delete_item($line_no)
{
	$l = $line_no;
	unset($_SESSION['pro_part']->line_items[$line_no]);
	unset_form_product_variables();
    line_start_product_focus();
}
//-------------- end delete product type ---------------

//------------ update consumable type ------------
function handle_consumable_type_update()
{
   $_SESSION['cons_part']->update_cons_item($_POST['line_no'],$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],$_POST['from_quantity'],$_POST['to_quantity']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end update assemble code ---------------
//------------ update handle_product_type_update ------------
function handle_product_type_update()
{
   $_SESSION['pro_part']->update_product_item($_POST['line_no'],$_POST['category_id'],$_POST['collection_id'],$_POST['range_id'],$_POST['design_id'],$_POST['finish_code_id'],$_POST['pro_unit'],$_POST['pro_qty'] );
	unset_form_product_variables();
    line_start_product_focus();
}
//-------------- end handle_product_type_update ---------------


//*****************  check data    --**************************
function consumable_check_data()
{
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	
	if(!get_post('cons_quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('cons_quntity');
		return false;
	}
	if (!check_num('cons_quntity', 0))
    {
	   	display_error(_("The quntity entered must be numeric and not less than zero."));
		set_focus('cons_quntity');
	   	return false;	   
    }
    return true;	
}

function product_check_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Select product category"));
		set_focus('category_id');
		return false;
	}
	
	if(!get_post('pro_qty')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('pro_qty');
		return false;
	}
	if (!check_num('pro_qty', 0))
    {
	   	display_error(_("The quantity entered must be numeric and not less than zero."));
		set_focus('pro_qty');
	   	return false;	   
    }
	
    return true;	
}

//********************* end check data  ********************

//     ++++++++++     Unset variables    ++++++++++++
function unset_form_cons_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_quntity']);
	unset($_POST['from_quantity']);
	unset($_POST['to_quantity']);

}
function unset_form_product_variables() {
	unset($_POST['category_id']);
    unset($_POST['collection_id']);
    unset($_POST['design_id']);
	unset($_POST['range_id']);
	unset($_POST['finish_code_id']);
	unset($_POST['pro_qty']);
	unset($_POST['pro_unit']);
}

//++++++++++++++++++  end unset variables   +++++++


if($_POST['from_Location_id'] == '-1'){
	
	unset($_SESSION['pro_part']);
	unset($_SESSION['cons_part']);
}

//++++++++++++++++++++++   insert into table ++++++++++

function handle_commit_inventory_transfer()
{
	if(isset($_POST['directToPT']) && $_POST['directToPT'] == "1")
	{
		if($_POST['production_team'] == '-1')
		{
			display_error("Select Production Team.");
			return false;
		}
	}
	$header_design = array();
	$header_design['from_Location_id'] = trim($_POST['from_Location_id']);
	$header_design['from_work_center_id'] = trim($_POST['from_work_center_id']);
	$header_design['to_Location_id'] = trim($_POST['to_Location_id']);
	$header_design['to_work_center_id'] = trim($_POST['to_work_center_id']);
	$header_design['reference_no'] = trim($_POST['reference_no']);
	$header_design['date'] =trim($_POST['date']);
	
	$header_design['stock_type_id'] = trim($_POST['stock_type_id']);
	$header_design['type_id'] = trim($_POST['type_id']);
	$header_design['memo_description'] = trim($_POST['memo_description']);

	
	$pack_sec = &$_SESSION['cons_part'];
	$product_sec = &$_SESSION['pro_part'];
	
	$order_no = add_inventory($header_design,$pack_sec,$product_sec);
	
	
    unset($_SESSION['cons_part']);
	unset($_SESSION['pro_part']);
	unset($_POST);
	display_notification("Items has been transferred.");
	meta_forward($_SERVER['PHP_SELF']);
}



//------------------- line start table ------------
function line_start_consumable_focus() {
  global 	$Ajax;

  $Ajax->activate('cons_table');
  //set_focus('part_name');
}
function line_start_product_focus() {
  global 	$Ajax;

  $Ajax->activate('pro_table');
  //set_focus('cons_type');
}


$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_consumable_delete_item($id);
}
if (isset($_POST['ConsumeAddItem']))
{
	handle_new_consumable();
}
if (isset($_POST['ConsumeUpdateItem'])){
 handle_consumable_type_update();
}
if (isset($_POST['ConsumeCancelItemChanges'])) {
	line_start_consumable_focus();
}

$id = find_submit('Delete');
if ($id != -1)
{
	handle_product_delete_item($id);
}
// - - - - -  handle product type
if (isset($_POST['ProductAddItem']))
{
	handle_new_product();
}
if (isset($_POST['ProductUpdateItem'])){
	handle_product_type_update();
}
if (isset($_POST['ProductCancelItemChanges'])) {
	line_start_product_focus();
}



if (isset($_POST['AddProcess']))
{	
	handle_commit_inventory_transfer();
}
if (isset($_POST['UpdateProcess']))
{
	handle_update_commit_inventory_transfer();
}






//----------------------------------------------------------------------------------
start_form(true);

display_inventory_transfer_header($_SESSION['DCOD']);
display_consumable_summary($_SESSION['cons_part']);
display_product_summary($_SESSION['pro_part']);
display_memo();
/*if ($selected_id != -1) 
{	
	
	echo "<br><center>";
	submit_center_first('UpdateProcess', _("Update Inventory Location"), _('Update to process code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to process code'), 'default');
	echo "</center>";
}
else
{*/
	submit_center_last('AddProcess', _("Process Inventory Location"), '', 'default');
/*}*/


end_form();

end_page();


?>


<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}


</style>


<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(e) {
    $("#pro_table").hide();
	$("#cons_table").show();
	$('#slc_work_center').attr('disabled', 'disabled');
	$('#slc_to_work_center').attr('disabled', 'disabled');
	
	
});
$(document).on('change','#slc_stock_master',function(){
		var stock_type = $("#slc_stock_master").val();
		if(stock_type=='101'){
			//alert(stock_type);
			$("#pro_table").hide();
			$("#cons_table").show();
		}else{
			//alert(stock_type);
		$("#pro_table").show();
		$("#cons_table").hide();
		}
		
		$('#slc_master option:eq(0)').attr('selected','selected');
		$('#sub_master option:eq(0)').attr('selected','selected');
		$('#slc_category option:eq(0)').attr('selected','selected');
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
		
	});



	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_id_code").hide();
			}else{
				$("#range_id_code").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
		// - - ------     location change  ---change work center
	
	

	$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		$('#slc_work_center').attr('disabled', false);
		var slc_location = $(this).val();
		
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					//alert(data);
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		
		return false;
	});
	
	
	
	
	$(document).on('change','#slc_to_location',function(){
		$('#slc_to_work_center').attr('disabled', false);
		var slc_to_location = $(this).val();
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_to_location},
			success: function(data){
					var select_val = $('#slc_to_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
	/*$(document).ready(function(e) {
		
		
		var reference_id = '';
		$.ajax({
			url: "reference_no.php",
			method: "POST",
            data: { id : reference_id},
			success: function(data){
				//alert(data);
					 $('#reference_id').val(data);
					 //reference_id
				}
		});
		return false;
	});
*/
/*$(document).on('click','#AddProcess', function() {
		alert("Stock has been registered...!!");
		return true;
	});*/	
	
$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var consumable_category = $(this).val();
	var consumable_id  = $("#slc_master").val();
	var from_location = $("#slc_location").val();
	var to_location = $("#slc_to_location").val();
	var from_work_center = $("#slc_work_center").val();
	var to_work_center = $("#slc_to_work_center").val();

	$.ajax({
		url: "unit_calling.php",
		method: "POST",
		data: { id : consumable_category},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	
	$.ajax({
		url: "get_quantity_from_work_center.php",
		method: "POST",
		data: { consumable_id:consumable_id, consumable_category:consumable_category, from_location:from_location, from_work_center:from_work_center },
		success: function(data){
				var select_val = $('#from_quantity');
				select_val.val(data);
			}
	});
	$.ajax({
		url: "get_quantity_to_work_center.php",
		method: "POST",
		data: { consumable_id:consumable_id, consumable_category:consumable_category, to_location:to_location, to_work_center:to_work_center },
		success: function(data){
				var select_val = $('#to_quantity');
				select_val.val(data);
			}
	});
});	

function get_production_team_list(to_work_center)
{
	$.ajax({
			url: "production_team_list.php",
			method: "POST",
	        data: { id : to_work_center},
			success: function(data){
				if(data == "")
				{
					$("input[name='directToPT']").attr('checked', false);
					alert("No Production team Available for this Work Center. Please choose Another Work Center.");
				}
				else
				{
					var select_val = $('#production_team');
	                select_val.empty().append(data);
				}
					
			}
		});
		return false;
}


/* Bajrang L. Bidasara - get production team */
$(document).on("change","input[name='directToPT']", function(){	
	var to_work_center = $("#slc_to_work_center").val();
	if(to_work_center == "-1")
	{
		alert("Please Select the Work center First.");
		$(this).attr('checked', false);
		var select_val = $('#production_team');
	    select_val.empty().append("<option value='-1'>- Select Production Team -</option>");
		return false;
	}
	if(this.checked)
	{
		get_production_team_list(to_work_center);
	}
	else{
		return false;	
	}
});

$(document).on("change","#slc_to_work_center", function(){	
	$("input[name='directToPT']").attr('checked', false);
	var select_val = $('#production_team');
	    select_val.empty().append("<option value='-1'>- Select Production Team -</option>");	
	return false;	

});



/*window.onbeforeunload = function() {
    return "Leaving this page will reset the Data";
};
*/
</script>