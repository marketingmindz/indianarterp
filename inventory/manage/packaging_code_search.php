<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FINISH_DETAILS';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/packaging_module_db.inc");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Packaging List"), false, false, "", $js);
}


start_form();
	start_outer_table(TABLESTYLE2, "style=width:80%;min-width:320px;");
		
		table_section(1);
		table_section_title(_("Finish Product Code Selection"));
		design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		
		if($_POST['collection_id'] == "NonCollection"){
			//design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
			sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'));
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
			desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
			desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}
		
		if($_POST['design_id'] == '-1'){
			sub_finish_packaging_code_list_row(_("Finish Product Codes:"),'finish_code_id', null, _('----Select Code---'));	
		}else{
			desgin_finish_prodcut_desing_code_list_row(_("Finish Product Codes:"), $_POST['design_id'], $_POST['finish_code_id'], null, false, true, true, true);
		}
		
			
	end_outer_table(1);
	echo '<center><button type="submit" aspect="default" name="SearchButton" id="SearchButton" value="Search Packaging"><span>Search Packaging</span></button></center>';

end_form();
/*if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} 
	$Ajax->activate('orders_tbl');
	
}


if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}



if(isset($_REQUEST['p_id']))
	{
		$get_packaging_id= $_REQUEST["p_id"];
		packaging_deleted($get_packaging_id);
		header('Location: '.$path_to_root.'/inventory/manage/packaging_code_search.php?'); 
		exit(1);  
	}
	
if (!@$_GET['popup'])
	start_form();
	
//------------- edit functionality ----------
function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
	$pacakagin_id = $row['packaging_pro_id'];
	unset($_SESSION['packaging_part']);
	unset($_SESSION['part_section']);
  	return pager_link( _("Edit"),
		"/inventory/manage/packaging_module.php?PackingingId=".$pacakagin_id, ICON_EDIT);
		
}

function delete_link($row)
{
	if (@$_GET['popup'])
		return '';
		
	$pacakagin_id = $row['packaging_pro_id'];
		
	return pager_link(_("Delete"),
			"/inventory/manage/packaging_code_search.php?p_id=".$pacakagin_id, ICON_DELETE);
}

$sql = get_packaging_search($all_items);
$cols = array(
		_("Packaging Id"),
		_("Finish code"),
		_("Product name"),
		_("Range name"),
		_("catagory name"), 
		//_("Priciple type"), 
		//_("Code ID"), 
		//_("Unit"), 
		//_("Quantity"), 
		//_("Width"), 
		array('insert'=>true, 'fun'=>'edit_link'),
		array('insert'=>true, 'fun'=>'delete_link'),
);

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}*/
?>


<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
	$(document).on('click','#SearchButton', function(){
		var cat_id = $('#slc_category').val();
		var rang_id = $('#range_id_code').val();
		var des_id = $('#sub_design').val();
		var fin_id = $('#packaging_finish_code').val();
		$.ajax({
				url: "get_packaging_search.php",
				method: "POST",
				data: { cat_id : cat_id, rang_id: rang_id, des_id : des_id, fin_id : fin_id },
				success: function(data){
					if(data == ''){
						alert('Not Packaging available');
					}else{
						var pathname = window.location.origin;
						var url = pathname+"/inventory/manage/packaging_module.php?PackingingId="+data;
						window.location.href = url;
					}
						
					}
			});
	});
	
	$(document).on('change','#collection_id', function(){
		var coll_id = $('#collection_id').val();
		if(coll_id == 'NonCollection'){
			var rang_id = $('#range_id_code').val(-1);
		}
		
	});
	


	
	
</script>