<?php
error_reporting(0);
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FINISH_PRODUCT';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/db/finish_product_db.inc");
include_once($path_to_root . "/inventory/includes/finish_product_class.inc");
include_once($path_to_root . "/inventory/includes/ui/finish_product_ui.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Finish Product"), false, false, "", $js);
//--------------------- My Code   -------------------------------------------------------------

if (isset($_GET['FinishId'])){
    unset($_SESSION['fab_part']);
	unset($_SESSION['fcon_part']);
	/*echo "<script>  document.getElementById('product_name').setAttribute('readonly','readonly'); </script>";	
	echo "<script>  $(document).ready(function() { $('#details input').attr('readonly', 'readonly'); 
	$('#details select').attr('disabled', 'disabled');
	$('#gross_weight').removeAttr('readonly');
	$('#items_table button').attr('disabled', 'disabled');
	$('#_fabric_name_sel select').attr('disabled', 'disabled');
	$('#_part_name_sel select').attr('disabled', 'disabled');
	$('#items_table input').attr('readonly', 'readonly');
	$('#FabricAddItem').attr('disabled', 'disabled');
	$('#finish_generate_code').attr('disabled', 'disabled');
	 }); </script>";*/
}
if(isset($_SESSION['fab_part']) || isset($_SESSION['fcon_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['fab_part']);
	 fixObject($_SESSION['fcon_part']);
}else{
  $_SESSION['fcon_part'] = $_SESSION['fab_part'] = new finish_order;
}

//-------------------------------------------------------
if (isset($_GET['FinishId']))
{
	
	$selected_id = $_GET['FinishId'];
	$_SESSION['s_id'] = $selected_id;
	$result = read_fabric($selected_id);
	read_fab_items($selected_id, $_SESSION['fab_part']);	
	read_cons_items($selected_id, $_SESSION['fcon_part']);		
}else{
	$selected_id = -1;	
	
	
}




// - -- -   code by bajrang    ----------

if (isset($_GET['design_id']))
{
	
	$selected_desing_id = $_GET['design_id'];
	$result = read_design($selected_desing_id);
			
}
//    ----- End Code ----




if(isset($_POST['finish_pro_id'])){
			$f_pro_image = get_product_image($_POST['finish_pro_id']);
			$pro_img = $f_pro_image['product_image'];
			$_SESSION['finish_photo'] = $pro_img ;	
			
	}
	
//------------- product image -----------------------------------
//------------ Add Fabric Details ------------

function handle_new_fabric_design()
{	
			$fabric_code_check_data = fabric_code_check_data();
			if($fabric_code_check_data){
			   $_SESSION['fab_part']->add_to_fab_part(count($_SESSION['fab_part']->line_items), $_POST['part_name'],$_POST['fabric_name'],$_POST['percentage']);
				$count_sessiong = count($_SESSION['fab_part']);
				unset_form_variables();
				line_start_focus();
			}
}



//-------------- end new assemble code ---------------

//------------ handle new consume code ------------

function handle_new_fconsume_design()
{
		$consumable_check_data =consumable_check_data();
		if($consumable_check_data){
			
			$_SESSION['fcon_part']->add_to_fcon_part(count($_SESSION['fcon_part']->line_con), $_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],
			$_POST['cons_iscbm']);
			unset_form_con_variables();
			line_con_start_focus();
		}
	 
	
}


//-------------- end new consume code ---------------


//------------ delete Fabric Details code ------------
function handle_fabric_delete_item($line_no)
{
	array_splice($_SESSION['fab_part']->line_items, $line_no, 1);
	unset_form_variables();	
    line_start_focus();
}
//-------------- end delete assemble code ---------------
//------------ delete consume code ------------
function handle_fconsume_delete_item($line_no)
{
	$l = $line_no;
	unset($_SESSION['fcon_part']->line_con[$line_no]);
	unset_form_con_variables();	
    line_con_start_focus();
}
//-------------- end delete consume code ---------------

//------------ Update Fabric Details ------------
function handle_fabric_update_item()
{
   $_SESSION['fab_part']->update_fab_item($_POST['line_no'], $_POST['part_name'],$_POST['fabric_name'],$_POST['percentage']);
	unset_form_variables();
    line_start_focus();
}
//-------------- end update assemble code ---------------
//------------ update consume code ------------
function handle_fconsume_update_item()
{
   $_SESSION['fcon_part']->update_fcon_item($_POST['line_no'], $_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],
	$_POST['cons_iscbm']);
	unset_form_con_variables();
    line_con_start_focus();
}
//-------------- end update consume code ---------------

function fabric_code_check_data()
{
	/*if(get_post('category_id') == -1) {
		display_error( _("Category cannot be empty."));
		set_focus('category_id');
		return false;
	}*/
	if(!get_post('finish_product_name')) {
		display_error( _("Product Name cannot be empty."));
		set_focus('finish_product_name');
		return false;
	}
	if (!check_num('percentage', 0))
    {
	   	display_error(_("The percentage entered must be numeric and not less than zero."));
		set_focus('percentage');
	   	return false;	   
    }  
    return true;	
}
function consumable_check_data()
{
	if(!get_post('finish_product_name')) {
		display_error( _("Product Name cannot be empty."));
		set_focus('finish_product_name');
		return false;
	}
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	if(!get_post('cons_quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('cons_quntity');
		return false;
	}
	if (!check_num('cons_quntity', 0))
    {
	   	display_error(_("The quntity entered must be numeric and not less than zero."));
		set_focus('cons_quntity');
	   	return false;	   
    }
    return true;	
}
function finish_code_check_data()
{
	/*if(get_post('category_id') == -1) {
		display_error( _("Category cannot be empty."));
		set_focus('category_id');
		return false;
	}*/
	if(!get_post('finish_product_name')) {
		display_error( _("Product Name cannot be empty."));
		set_focus('finish_product_name');
		return false;
	}
	
	if(!get_post('finish_comp_code')) {
		display_error( _("Finish Code Cannot Empty."));
		set_focus('finish_comp_code');
		return false;
	}
	
	$finish_code = $_POST['finish_comp_code'];
	$check_finish = check_finish_code($finish_code);
	$finish_code_com = $check_finish['finish_comp_code'];
	if($_POST['finish_comp_code'] == $finish_code_com ){

		display_error( _("Finish Code All Ready Exist."));
		return false;
	}
	
	
	/*if(!get_post('design_code')) {
		display_error( _("Design Code cannot be empty."));
		set_focus('design_code');
		return false;
	}*/
    return true;	
}
function unset_form_variables() {
	unset($_POST['part_name']);
    unset($_POST['fabric_name']);
    unset($_POST['percentage']);
}
function unset_form_con_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_quntity']);
	unset($_POST['iscbm']);

}

//------------------------ add all value --------------------------------

function handle_commit_finish_product()
{
	$finish_code_check_data = finish_code_check_data();
	
	
	
	if($finish_code_check_data){
				/******** collection image *******************/
				$co_image= $_FILES['coll_pic']['name'];
				$image_tmp= $_FILES['coll_pic']['tmp_name'];
				$coll_Imge = company_path().'/collectionImage/'.$co_image;
				move_uploaded_file($image_tmp,$coll_Imge);
				/************ end collection image *****************/
				/************ product image code ***************/
				$session_photo = "";
				$photo_count = trim($_POST['no_finish_pro_img']);
				for($photo_no=1;$photo_no<=$photo_count;$photo_no++){
					$photo_name = "pic_".$photo_no;
					$finish_image = $_FILES[$photo_name]['name'];
					$fimage_tmp= $_FILES[$photo_name]['tmp_name'];
					if(!$session_photo){
					   $session_photo = $finish_image;	
					}else{
						$session_photo .= ",".$finish_image;
					}
					$f_Imge = company_path().'/finishProductImage/'.$finish_image;
					move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
				}
				/*********** end product image code **********/
				$header_design = array();
				$header_design['category_id'] = trim($_POST['category_id']);
				$header_design['range_id'] = trim($_POST['range_id']);
				$header_design['design_id'] = trim($_POST['design_id']);
				$header_design['color_id'] = trim($_POST['color_id']);
				$header_design['leg_id'] = trim($_POST['leg_id']);
				$header_design['wood_id'] = trim($_POST['wood_id']);
				$header_design['skin_id'] = trim($_POST['skin_id']);
				$header_design['fabric_id'] = trim($_POST['fabric_id']);
				$header_design['finish_id'] = trim($_POST['finish_id']);
				$header_design['finish_product_name'] = trim($_POST['finish_product_name']);
				$header_design['finish_code'] = trim($_POST['finish_code']);
				$header_design['finish_comp_code'] = trim($_POST['finish_comp_code']);
				$header_design['gross_weight'] = trim($_POST['gross_weight']);
				$header_design['asb_weight'] = trim($_POST['asb_weight']);
				$header_design['asb_height'] = trim($_POST['asb_height']);
				$header_design['asb_density'] = trim($_POST['asb_density']);
				$header_design['description'] = trim($_POST['description']);
				$header_design['product_image'] = $session_photo;	
				$header_design['collection_image'] = $co_image;
				$header_design['structure_wood_id'] =  trim($_POST['structure_wood_id']);
				$header_design['no_setting'] =  trim($_POST['no_setting']);	
				$fabric = &$_SESSION['fab_part'];
				$consume = &$_SESSION['fcon_part'];
				$order_no = add_finishcode($header_design,$fabric, $consume);
				unset($_SESSION['fab_part']);
				unset($_SESSION['fcon_part']);
				meta_forward($path_to_root.'finish_product_list.php?');
	}
	
		
	
}



function handle_update_commit_finish(){
	
	global $selected_id;
	
	if($_POST['collection_id']  == "NonCollection"){
		$range_id = '-1';
	}else{
		$range_id = $_POST['range_id'];
	}
	
	$selected_id = $_SESSION['s_id'];
	$fin_image = $_SESSION['finish_photo'];
	/******** collection image *******************/
	if(!empty ($_FILES['coll_pic']['tmp_name'])){
		$co_image= $_FILES['coll_pic']['name'];
		$image_tmp= $_FILES['coll_pic']['tmp_name'];
		$coll_Imge = company_path().'/collectionImage/'.$co_image;
		move_uploaded_file($image_tmp,$coll_Imge);
		update_collection_image($selected_id, $co_image );		 
	}
	/************ product image code ***************/
	
	$session_photo = "";
	
	$pro_img_arry = explode(',', $fin_image);
	//display_error(_($pro_img_arry['0']));
	//display_error(_($pro_img_arry['1']));display_error(_($pro_img_arry['2']));	
		
	$photos_array = array(); 
	$photo_count = trim($_POST['no_finish_pro_img']);
	for($photo_no=1; $photo_no <= $photo_count; $photo_no++){
		$photo_name = "pic_".$photo_no;
		
		if(!empty ($_FILES[$photo_name]['name'])){
			$finish_image = $_FILES[$photo_name]['name'];				
			
			$fimage_tmp= $_FILES[$photo_name]['tmp_name'];
			
			$f_Imge = company_path().'/finishProductImage/'.$finish_image;
			move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
		}else{
			$finish_image =  $pro_img_arry[$photo_no-1];
		}
		
		if(!isset($_SESSION['del_img'][$photo_no]) && $_SESSION['del_img'][$photo_no] != 'yes'){
			if($finish_image != ''){	
				$photos_array[] = $finish_image;
			}
		}
		
		
	}
	unset($_SESSION['del_img']);
	if(count($photos_array)>0){
		update_finish_image($selected_id, implode(",", $photos_array) );
	}else{
	
		$photos_array = '';
		update_finish_image($selected_id, $photos_array );
		
	}
	
	/*********** end product image code **********/
	/******** end collection image *******************/
	/************ product image code ***************/
				
				
				
	/*********** end product image code **********/
	$udpate_design = array();
	$udpate_design['category_id'] = trim($_POST['category_id']);
	$udpate_design['range_id'] = trim($range_id);
	$udpate_design['design_id'] = trim($_POST['design_id']);
	$udpate_design['color_id'] = trim($_POST['color_id']);
	$udpate_design['leg_id'] = trim($_POST['leg_id']);
	$udpate_design['wood_id'] = trim($_POST['wood_id']);
	$udpate_design['skin_id'] = trim($_POST['skin_id']);
	$udpate_design['fabric_id'] = trim($_POST['fabric_id']);
	$udpate_design['finish_id'] = trim($_POST['finish_id']);
	$udpate_design['finish_product_name'] = trim($_POST['finish_product_name']);
	$udpate_design['finish_code'] = trim($_POST['finish_code']);
	$udpate_design['finish_comp_code'] = trim($_POST['finish_comp_code']);
	$udpate_design['gross_weight'] = trim($_POST['gross_weight']);
	$udpate_design['asb_weight'] = trim($_POST['asb_weight']);
	$udpate_design['asb_height'] = trim($_POST['asb_height']);
	$udpate_design['asb_density'] = trim($_POST['asb_density']);
	$udpate_design['description'] = trim($_POST['description']);
	$udpate_design['structure_wood_id'] =  trim($_POST['structure_wood_id']);
	$udpate_design['no_setting'] =  trim($_POST['no_setting']);
	

	//$udpate_design['product_image'] = trim($_SESSION['finish_photo']);	
	//$udpate_design['collection_image'] = $_SESSION['$collection_image'];	
	$fabric = &$_SESSION['fab_part'];
	$consume = &$_SESSION['fcon_part'];
	$order_no = update_finishcode($selected_id, $udpate_design, $fabric, $consume);
	unset($_SESSION['fab_part']);
	unset($_SESSION['fcon_part']);
	meta_forward($path_to_root.'finish_product_list.php?');
	
}


//-------------------------------------------------------

//------------------- line start table ------------
function line_start_focus() {
  global 	$Ajax;
  $Ajax->activate('items_table');
  //set_focus('part_name');
}
function line_con_start_focus() {
  global 	$Ajax;

  $Ajax->activate('con_table');
  //set_focus('cons_type');
}

//------------------- end line start table --------------

$id = find_submit('Delete');
if ($id != -1)
{
	handle_fabric_delete_item($id);
}
if (isset($_POST['FabricAddItem']))
{
	handle_new_fabric_design();
}
if (isset($_POST['FabricUpdateItem'])){
	handle_fabric_update_item();
}
if (isset($_POST['FabricCancelItemChanges'])) {
	line_start_focus();
}

$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_fconsume_delete_item($id);
}
if (isset($_POST['FConsumeAddItem']))
{
	handle_new_fconsume_design();
}
if (isset($_POST['FConsumeUpdateItem'])){
 handle_fconsume_update_item();
}
if (isset($_POST['FConsumeCancelItemChanges'])) {
	line_con_start_focus();
}
if (isset($_POST['AddDesign']))
{
	handle_commit_finish_product();
}
if (isset($_POST['UpdateDesign']))
{
	handle_update_commit_finish();
}
//---------------------------------------------------------------------
$myrow = $_POST['finish_product_name'];
if($myrow['finish_product_name']==''){
	echo "<input type='hidden' id='des_id' value=''>";
	unset($_SESSION['fab_part']);
	unset($_SESSION['fcon_part']);
}else{
 echo "<input type='hidden' id='des_id' value='".$myrow."'>";
}

echo "<input type='hidden' id='range_id_code_url' value=''>";
echo "<input type='hidden' id='code_id' value=''>";
echo "<input type='hidden' id='skin_name' value=''>";
echo "<input type='hidden' id='finish_code_name'>";
start_form(true);

//---------------------------- start table ------------------------------------------------------

display_finish_hearder($_SESSION['FINISH_CODE']);

display_fabric_summary($_SESSION['fab_part']);
display_fconsumable_summary($_SESSION['fcon_part']);
display_image_hearder($_SESSION['FINISH_CODE']);

if ($selected_id != -1) 
{
	echo "<br><center>";
	submit_center_first('UpdateDesign', _("Update"), _('Update to design code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to design code'), 'default');
	echo "<br><button><a href='packaging_module.php?finish_pro_id=".$_REQUEST['FinishId']."'>Generate Package Code</a></button>";
	echo "</center>";
}
else
{
	submit_center_last('AddDesign', _("Add Finish Product"), '', 'default');
}
/** detail **/


/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';


end_form();

end_page();

?>

<style>
button#AddDesign {
	align-content: center;
	margin-left: 500px;
}


</style>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_category',function(){
		 //alert( this.value ); // or $(this).val()
		  $('#collection_id').val('SelectCollection');
		var catval = parseInt(this.value);
		if ($.inArray(catval, [28, 41, 60, 64, 77, 79]) > -1) {
			
			  $('#tr_structure_wood').show();
			 $('#tr_no_setting').show();	
		}else{
			
			 $('#tr_structure_wood').hide();
			 $('#tr_no_setting').hide();
			 $('#no_setting').val('');
			 $('#structure_wood_id').val('');
			 	
		}
	});
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
				$('#range_span').val('-1');
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
</script>
<script type="text/javascript">

	$(document).on('change','#skin_id',function(){
		var skin_name = $("#skin_id").val();
		$.ajax({
			url: "range_gen_code.php",
			method: "POST",
            data: { ids : skin_name },
			success: function(data){
					var skin_name = data;
					if(skin_name == 'FABRIC'){
						$("#fabric_conect").show();
						
					}else{
						$("#fabric_conect").hide();
					
					}
				}
		});
		return false;
		
	});
	
	
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "range_gen_code.php",
			method: "POST",
            data: { d_ids : design_id },
			success: function(data){
					$('#product_name').val(data);
				}
		});
		return false;
		
	});
	
</script>
<script type="text/javascript">


	
	$(document).on('click','#finish_generate_code',function(){var slc_design = $('#sub_design').val();
			var wood_id = $('#wood_id').val();
			var color_id = $('#color_id').val();
			var finish_id = $('#finish_id').val();
			var wood_name = $('#wood_id option:selected').text();
			var color_name = $('#color_id option:selected').text();
			var finish_name = $('#finish_id option:selected').text();
			var skin_name = $('#skin_id option:selected').text();
			var skin_id = $('#skin_id').val();
			var struc_wood_id = $('#structure_wood_id').val();
			var no_setting = $('#no_setting').val();
			
			$.ajax({
				url: "finish_pro_gen.php",
				method: "POST",
				data: {id : slc_design, wid : wood_id, cid : color_id, fid : finish_id, struc_wood_id : struc_wood_id, no_setting : no_setting,skin_id :skin_id},
				success: function(data){
					var f_data = data;
					$('#finish_comp_code').val(data);
					$('#finish_code_name').val(data);
					var description = "This is "+data+" Made From "+wood_name+" Wood And "+color_name+" Color And "+finish_name+" Finish And "+skin_name+" Skin ";
					$('#design_description').text(description);
				  }
			  });
		  return false;
	 });
	
	$(document).on('click','#finish_generate_code',function(){
		//alert("data is synchronization successfully .... ")
		var slc_category = $('#slc_category').val();
		var slc_range = $('#range_id_code').val();
		var collection_name = $('#collection_id').val();
		var product_name = $('#product_name').val();
		var range_name = $("#range_id_code_url").val();
		
		$.ajax({
				url: "img_gen_code.php",
				method: "POST",
				data: { id : collection_name, catid: slc_category, rangeid : slc_range },
				success: function(data){
						
						$("#finish_code").val(data);
						
					}
			});
			return false;
	});
	
	$(function(){
    		jQuery('body').on('change', '#coll_pic', function () {
			  var file = $('#coll_pic').val();
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 
						reader.onloadend = function(){ // set image data as background of div
							$('#blah').attr('src', this.result);
						}
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  $('#coll_pic').val('');
				  $('#blah').attr('src', 'img_not.png');
				}
			  }
			});
  	});
				$(function(){
    		jQuery('body').on('change', '#pic', function () {
			  var file = $('#pic').val();
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 
						reader.onloadend = function(){ // set image data as background of div
							$('#blah1').attr('src', this.result);
							var noimages = parseInt($('#no_finish_pro_img').val());
							var newnoimages = noimages+1;
		
							$.ajax({
									url: "finish_pro_addimages.php",
									method: "POST",
									data: { noimages : noimages},
									success: function(data){
										$('#finishpro_addnewimg').remove();
										$('#finish_pro_img_tbl').append(data);
										$('#no_finish_pro_img').val(newnoimages);
									}
								});
								return false;	
						}
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  $('#pic').val('');
				  $('#blah1').attr('src', 'img_not.png');
				}
			  }
			});
  	});
		
		$(function(){
    		jQuery('body').on('change', '.pic', function () {
			var file = $(this).val();
			var num = $(this).attr('data-num');
			//alert(num);
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 			var fileSize = files[0].size/1024;
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 			
						fileSize = Math.round(files[0].size/1024);

						if(fileSize > 300){
							alert("File size must be less than 300 kB");
							this.value = null;
							$('#blah'+num).attr('src', 'img_not.png');
							
						}else{
							reader.onloadend = function(){ // set image data as background of div
								$('#blah'+num).attr('src', this.result);
								var noimages = parseInt($('#no_finish_pro_img').val());
								var newnoimages = noimages+1;
		
								$.ajax({
										url: "finish_pro_addimages.php",
										method: "POST",
										data: { noimages : noimages},
										success: function(data){
											$('#finishpro_addnewimg').remove();
											$('#finish_pro_img_tbl').append(data);
											$('#no_finish_pro_img').val(newnoimages);
										}
									});
								return false;
							}
						}
						
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  //$('#pic2').val('');
				  $('#blah'+num).attr('src', 'img_not.png');
				}
			  }
			});
  	});
		
	$(document).on('click','#finishpro_addnewimg',function(){
		//alert( $('#no_finish_pro_img').val());	
		var noimages = parseInt($('#no_finish_pro_img').val());
		var newnoimages = noimages+1;
		
		$.ajax({
				url: "finish_pro_addimages.php",
				method: "POST",
				data: { noimages : noimages},
				success: function(data){
					$('#finishpro_addnewimg').remove();
					$('#finish_pro_img_tbl').append(data);
					$('#no_finish_pro_img').val(newnoimages);
				}
			});
		return false;			
	});	
	
	$('body').on('click','.del_imge',function(){
		var img_id = $(this).attr("id");
		$('#blah'+img_id).attr('src', '');
		
		$.ajax({
				url: "del_image.php",
				method: "POST",
				data: { img_id : img_id},
				success: function(data){
					
				}
		});
	});
	//--------- show fabric image ----------
	$('body').on('change','#fabric_code', function(){
		var fab_id = $(this).val();
		$.ajax({
			url: "show_fabric_img.php",
			method: "POST",
			data: {fab_id : fab_id},
			success: function(data){
				 $('#fab_img').attr('src', data);
			}
		});
		
	});
		
	//-------------- get packing size --------------
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_size.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
				var size = data.split(',');
					$('#5').val(size[0]);
					$('#6').val(size[1]);
					$('#7').val(size[2]);
				}
		});
		
	});	

</script>