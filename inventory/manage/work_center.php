<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WORKCENTER';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Work Center"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/work_center_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['work_center_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The center name cannot be empty."));
		set_focus('work_center_name');
	}
	//

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
			$production_team = $_POST['production_team_id'];
		  update_work_center($selected_id, $_POST['work_center_name'], $production_team, $_POST['work_center_desc']);
				display_notification(_('Selected team has been updated'));
			
    	} 
    	else 
    	{
				//print_r($_POST['pro_team_id']);
				$production_team = $_POST['production_team_id'];
    		    add_work_center($_POST['work_center_name'],  $production_team,$_POST['work_center_desc']);
				display_notification(_('New team has been added'));
		
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_work_center($selected_id);
		display_notification(_('Selected team has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_work_center(check_value('show_inactive'));
?>



<?php



start_form();

start_table(TABLESTYLE2, "style=min-width:320px;width:40%;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code

		$myrow = get_work_center($selected_id);
		
		$_POST['work_center_name']  = $myrow["work_center_name"];
		$_POST['work_center_desc']  = $myrow["work_center_desc"];
		$pro_id  = $myrow["pro_team_id"];
		$team_name = get_product_id($pro_id);
		$pro_name  = $team_name;
		
		
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Center Name:"), 'work_center_name', null, 40, 40);
production_team_list_row1(_("Production Team:"), null,$pro_id, $pro_name, false, true, true, true);

textarea_row(_('Description:'), 'work_center_desc', null, 50, 6);

//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');



echo "<br>";
start_table(TABLESTYLE, "style=min-width:320px;width:40%;");

$th = array(_('Center Name'),_('Production Team'),_('Description'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	
	label_cell($myrow["work_center_name"]);
	
	$description = substr($myrow["work_center_desc"],0,50)."...";
	$pro_code_id  = $myrow["pro_team_id"];
	$team_name_product = get_product_id($pro_code_id);
	label_cell($team_name_product);
	label_cell($description);
	inactive_control_cell($myrow["work_center_id"], $myrow["inactive"], 'work_center', 'work_center_id');
 	edit_button_cell("Edit".$myrow['work_center_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['work_center_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

end_form();

end_page();

?>
<script src="../../js/jquery/jquery.js" ></script>

<script type="text/javascript">

$('body').on('click','.check', function() {
	var arr = [];

  $.each($("input[name='pro_team']:checked"), function(){            
     arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#production_team_id').val(arr.join(", "));
  $('#production_team_name').val(prod.join(", "));
});
 
</script>


