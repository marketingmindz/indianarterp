<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WOOD';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Wood Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/wood_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------
//***** image code **/

/*$upload_file = "";
if (isset($_FILES['pic']) && $_FILES['pic']['name'] != '') 
{
	$wood_reference = $_POST['wood_reference'];
	$result = $_FILES['pic']['error'];
 	$upload_file = 'Yes'; //Assume all is well to start off with
	$filename = company_path().'/woodImage';
	if (!file_exists($filename))
	{
		mkdir($filename);
	}	
	$filename .= "/".item_img_name($wood_reference).".jpg";
	
	//But check for the worst 
	if ((list($width, $height, $type, $attr) = getimagesize($_FILES['pic']['tmp_name'])) !== false)
		$imagetype = $type;
	else
		$imagetype = false;
	//$imagetype = exif_imagetype($_FILES['pic']['tmp_name']);
	if ($imagetype != IMAGETYPE_GIF && $imagetype != IMAGETYPE_JPEG && $imagetype != IMAGETYPE_PNG)
	{	//File type Check
		display_warning( _('Only graphics files can be uploaded'));
		$upload_file ='No';
	}
	elseif (!in_array(strtoupper(substr(trim($_FILES['pic']['name']), strlen($_FILES['pic']['name']) - 3)), array('JPG','PNG','GIF')))
	{
		display_warning(_('Only graphics files are supported - a file extension of .jpg, .png or .gif is expected'));
		$upload_file ='No';
	} 
	elseif ( $_FILES['pic']['size'] > ($max_image_size * 1024)) 
	{ //File Size Check
		display_warning(_('The file size is over the maximum allowed. The maximum size allowed in KB is') . ' ' . $max_image_size);
		$upload_file ='No';
	} 
	elseif (file_exists($filename))
	{
		$result = unlink($filename);
		if (!$result) 
		{
			display_error(_('The existing image could not be removed'));
			$upload_file ='No';
		}
	}
	
	if ($upload_file == 'Yes')
	{
		$result  =  move_uploaded_file($_FILES['pic']['tmp_name'], $filename);
	}
	$Ajax->activate('details');

}*/
/*if (isset($_POST['wood_reference']) && file_exists(company_path().'/woodImage/'.item_img_name($_POST['wood_reference']).".jpg")) 
{
	$wood_reference = $_POST['wood_reference'].".JPG";
}else{
	$wood_reference = 'No Image';
}*/
/**** end image code *********************/
function upload_image(){
		
}
if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['wood_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Wood name cannot be empty."));
		set_focus('wood_name');
	}
	if (strlen($_POST['wood_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Wood Reference cannot be empty."));
		set_focus('wood_reference');
	}

	if ($input_error != 1) 
	{
		
    	if ($selected_id != -1) 
    	{
      		$check_update_wood = check_update_wood($_POST['wood_reference']);
		    if($check_update_wood)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			  	 if(!empty ($_FILES['pic']['tmp_name'])){
					 	$wood_reference= $_FILES['pic']['name'];
						$image_tmp= $_FILES['pic']['tmp_name'];
						$filename = company_path().'/woodImage/'.$wood_reference;
						move_uploaded_file($image_tmp,$filename);
						update_wood_image($selected_id, $wood_reference );
			 
			     }
			     update_wood_master($selected_id, $_POST['wood_name'], $_POST['wood_reference'], $_POST['wood_description']);
			     display_notification(_('Selected wood master has been updated'));
			  }
    	} 
    	else 
    	{
			$check_wood = check_wood($_POST['wood_reference']);
		    if($check_wood)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
			 	$wood_reference= $_FILES['pic']['name'];
				$image_tmp= $_FILES['pic']['tmp_name'];
				$filename = company_path().'/woodImage/'.$wood_reference;
				move_uploaded_file($image_tmp,$filename);
    		    add_wood_master($_POST['wood_name'], $_POST['wood_reference'], $wood_reference, $_POST['wood_description']);
			    display_notification(_('New wood master has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_wood_master($selected_id);
		$wod_reference = $_POST['wood_refernce'];
		$filename = company_path().'/woodImage/'.item_img_name($wood_reference).".jpg";
		if (file_exists($filename))
			unlink($filename);
		display_notification(_('Selected wood has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_wood_master(check_value('show_inactive'));

start_form(true);

start_table(TABLESTYLE2, "style=width:50%;min-width:320px;");

	if ($selected_id != -1) 
	{
		if ($Mode == 'Edit') {
			//editing an existing status code
			$edit_mod = "readonly";
			$myrow = get_wood_master($selected_id);
	
			$_POST['wood_name']  = $myrow["wood_name"];
			$_POST['wood_reference']  = $myrow["wood_reference"];
			$_POST['wood_description']  = $myrow["wood_description"];
			$imge = $myrow["wood_image"];
			$_POST['del_image'] = 0;
		}
		hidden('selected_id', $selected_id);
	}
	text_row(_("Wood Name:"), 'wood_name', null, 40, 40);
	text_row(_("Reference:"), 'wood_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec" '.$edit_mod.'');
	file_row(_("Image File (.jpg)") . ":", 'pic', 'pic');
	$wood_img_link = "";
	if(!empty($imge)){
		$wood_img_link= "<img id='blah' src='".company_path().'/woodImage/'.$imge."' alt='your image' width='80' height='80' />";
	}else{
		$wood_img_link= "<img id='blah' src='".company_path().'/woodImage/img_not.png'."' alt='your image' width='80' height='80' />";
	}
	label_row("",$wood_img_link);
	
textarea_row(_('Description:'), 'wood_description', null, 50, 5);

//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');


echo "<br>";
start_table(TABLESTYLE, "style=width:50%;min-width:320px;");

$th = array(_('Wood Name'),_('Reference'),_('Description'),_('Image'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["wood_name"]);
	label_cell($myrow["wood_reference"]);
	label_cell($myrow["wood_description"]);
	label_cell($myrow["wood_image"]);
	
	inactive_control_cell($myrow["wood_id"], $myrow["inactive"], 'wood_master', 'wood_id');
 	edit_button_cell("Edit".$myrow['wood_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['wood_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

//-----------------------------------------------------------------------------------



end_form();

//------------------------------------------------------------------------------------

end_page();


?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
		
		/*$(function() {
			$("#pic").on("change", function()
			{
				var files = !!this.files ? this.files : [];
				if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
		 
				if (/^image/.test( files[0].type)){ // only image file
					var reader = new FileReader(); // instance of the FileReader
					reader.readAsDataURL(files[0]); // read the local file
		 
					reader.onloadend = function(){ // set image data as background of div
						$('#blah').attr('src', this.result);
					}
				}
			});
		});*/
		
		
		$(function(){
    		jQuery('body').on('change', '#pic', function () {
			  var file = $('#pic').val();
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 
						reader.onloadend = function(){ // set image data as background of div
							$('#blah').attr('src', this.result);
						}
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  $('#pic').val('');
				  $('#blah').attr('src', 'img_not.png');
				}
			  }
			});
  	});
</script>
<style>
	#imagePreview {
		width: 100px;
		height: 100px;
		background-position: center center;
		background-size: cover;
		-webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
		display: inline-block;
	}
</style>