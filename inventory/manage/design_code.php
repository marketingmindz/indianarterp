<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_DESIGN';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/design_code_db.inc");
include_once($path_to_root . "/inventory/includes/db/design_code.inc");
include_once($path_to_root . "/inventory/includes/ui/design_code_ui.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Design Code"), false, false, "", $js);
//--------------------- My Code   -------------------------------------------------------------
//session_start();
if (isset($_GET['ModifyDesignNumber'])){
    unset($_SESSION['asb_part']);
	unset($_SESSION['con_part']);
		
	echo "<script> $(document).ready(function() { /*$('#design_code').attr('readonly', 'readonly');*/ 	
	$('#slc_category').attr('disabled', 'disabled');
	$('#collection_id').attr('disabled', 'disabled');
	$('#range_id_code').attr('disabled', 'disabled');
	$('#generate_code').attr('disabled', 'disabled');
	 }); </script>";
}
if(isset($_SESSION['asb_part']) || isset($_SESSION['con_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['asb_part']);
	 fixObject($_SESSION['con_part']);
}else{
   $_SESSION['con_part'] = $_SESSION['asb_part'] = new design_order;
}

//-------------------------------------------------------
if (isset($_GET['ModifyDesignNumber']))
{
	
	$selected_id = $_GET['ModifyDesignNumber'];
	$_SESSION['s_id'] = $selected_id;
	$result = read_design($selected_id);
	read_asb_items($selected_id, $_SESSION['asb_part']);	
	read_cons_items($selected_id, $_SESSION['con_part']);		
}else{
	$selected_id = -1;	
}

if (isset($_GET['range_id']))
{
	$range_id = $_GET['range_id'];
}

if(isset($_POST['design_id'])){
	$f_pro_image = get_design_image($_POST['design_id']);
	$pro_img = $f_pro_image['design_image'];
	$_SESSION['design_photo'] = $pro_img ;		
}

//------------ handle new assemble code ------------

function handle_new_assemble_design()
{
	/*if (!check_item_data())
		return;*/

	 $check_data = assemble_check_data();
	if($check_data){
		if($_POST['old_design']==1){
			$assemble_code_value = $_POST['assemble_code_select'];
		}
		else{
			$assemble_code_value = $_POST['assemble_code'];
		}
		$aExtraInfo = getimagesize($_FILES['img_name']['tmp_name']);//shubham code
    $assembleImg = "data:" . $aExtraInfo["mime"] . ";base64," . base64_encode(file_get_contents($_FILES['img_name']['tmp_name']));

		$_SESSION['asb_part']->add_to_asb_part(count($_SESSION['asb_part']->line_items), $_POST['part_category'],$_POST['part_collection'],$_POST['part_range'],$_POST['old_design'], $assemble_code_value, $_POST['part_name'],$_POST['width'],$_POST['density'],
		$_POST['height'],$_POST['iscbm'],$_POST['quntity'],$_POST['net_weight'],$_FILES['img_name']['name'],$assembleImg);
		/*shubham code*/
		$asse_Imge = company_path().'/assembleImg/'.$_FILES['img_name']['name'];
		move_uploaded_file($_FILES['img_name']['tmp_name'], $asse_Imge);
		/*shubham end code */
		$count_sessiong = count($_SESSION['asb_part']);
		unset_form_variables();
	}
	line_start_focus();	
				
			
}

//-------------- end new assemble code ---------------

//------------ handle new consume code ------------

function handle_new_consume_design()
{
	$check_cons_data = consumable_check_data();
	 if($check_cons_data){
	   	$_SESSION['con_part']->add_to_con_part(count($_SESSION['con_part']->line_con), $_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],
		$_POST['cons_iscbm']);
		unset_form_con_variables();
		line_con_start_focus();
	 }
	
}


//-------------- end new consume code ---------------


//------------ delete assemble code ------------
function handle_assemble_delete_item($line_no)
{
	array_splice($_SESSION['asb_part']->line_items, $line_no, 1);
	unset_form_variables();	
    line_start_focus();
}
//-------------- end delete assemble code ---------------
//------------ delete consume code ------------
function handle_consume_delete_item($line_no)
{
	$l = $line_no;
	unset($_SESSION['con_part']->line_con[$line_no]);
	unset_form_con_variables();	
    line_con_start_focus();
}
//-------------- end delete consume code ---------------

//------------ update assemble code ------------
function handle_assemble_update_item()
{
   if($_POST['old_design']==1){
			$assemble_code_value = $_POST['assemble_code_select'];
		}
		else{
			$assemble_code_value = $_POST['assemble_code'];
		}

   $_SESSION['asb_part']->update_abs_item($_POST['line_no'], $_POST['part_category'],$_POST['part_collection'],$_POST['part_range'],$_POST['old_design'], $assemble_code_value, $_POST['part_name'],$_POST['width'],$_POST['density'],
	$_POST['height'],$_POST['iscbm'],$_POST['quntity'],$_POST['net_weight'],$_FILES['img_name']['name']);
   //shubham code
	unset_form_variables();
    line_start_focus();
}
//-------------- end update assemble code ---------------
//------------ update consume code ------------
function handle_consume_update_item()
{
   $_SESSION['con_part']->update_con_item($_POST['line_no'], $_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity'],
	$_POST['cons_iscbm']);
	unset_form_con_variables();
    line_con_start_focus();
}
//-------------- end update consume code ---------------
function assemble_check_data()
{

	if(!get_post('quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('quntity');
		return false;
	}
	/*if(!get_post('width')) {
		display_error( _("Width cannot be empty."));
		set_focus('width');
		return false;
	}
	if (!check_num('width', 0))
    {
	   	display_error(_("The width entered must be numeric and not less than zero."));
		set_focus('width');
	   	return false;	   
    }
	if(!get_post('height')) {
		display_error( _("Height cannot be empty."));
		set_focus('width');
		return false;
	}
	if (!check_num('height', 0))
    {
	   	display_error(_("The height entered must be numeric and not less than zero."));
		set_focus('height');
	   	return false;	   
    }
	if(!get_post('density')) {
		display_error( _("Density cannot be empty."));
		set_focus('density');
		return false;
	}
	if (!check_num('density', 0))
    {
	   	display_error(_("The density entered must be numeric and not less than zero."));
		set_focus('density');
	   	return false;	   
    }
	if(!get_post('assemble_code')) {
		display_error( _("Code cannot be empty."));
		set_focus('assemble_code');
		return false;
	}
	if(!get_post('quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('quntity');
		return false;
	}
	if (!check_num('quntity', 0))
    {
	   	display_error(_("The quantity entered must be numeric and not less than zero."));
		set_focus('quntity');
	   	return false;	   
    }
	if(!get_post('net_weight')) {
		display_error( _("Net Weight cannot be empty."));
		set_focus('net_weight');
		return false;
	}
	if (!check_num('net_weight', 0))
    {
	   	display_error(_("The net weight entered must be numeric and not less than zero."));
		set_focus('net_weight');
	   	return false;	   
    }  */
    return true;	
}

function consumable_check_data()
{
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	if(!get_post('cons_quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('cons_quntity');
		return false;
	}
	if (!check_num('cons_quntity', 0))
    {
	   	display_error(_("The quntity entered must be numeric and not less than zero."));
		set_focus('cons_quntity');
	   	return false;	   
    }
    return true;	
}
function design_code_check_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Category cannot be empty."));
		set_focus('category_id');
		return false;
	}
	if(!get_post('product_name')) {
		display_error( _("Product Name cannot be empty."));
		set_focus('product_name');
		return false;
	}
	if(!get_post('design_code')) {
		display_error( _("Design Code cannot be empty."));
		set_focus('design_code');
		return false;
	}
    return true;	
}
function unset_form_variables() {
	unset($_POST['part_category']);
	unset($_POST['part_collection']);
	unset($_POST['part_range']);
	unset($_POST['old_design']);
	unset($_POST['part_name']);
    unset($_POST['width']);
    unset($_POST['density']);
	unset($_POST['height']);
	unset($_POST['iscbm']);
	unset($_POST['assemble_code']);
	unset($_POST['quntity']);
	unset($_POST['net_weight']);
}
function unset_form_con_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_quntity']);
	unset($_POST['iscbm']);

}
//--------------------------------------------------------------------

					

//------------------------ add all value --------------------------------

function handle_commit_design()
{
	$check_design_code = design_code_check_data();
		$img_code = $_POST['design_code'];
		echo $cart->design_id;
		if($check_design_code){
				/************ product image code ***************/
				$session_photo = "";
				
				$photo_count = trim($_POST['no_finish_pro_img']);
				for($photo_no=1;$photo_no<=$photo_count;$photo_no++){
					$photo_name = "pic_".$photo_no;
					$des_img = $_FILES[$photo_name]['name'];
					//$des_img = $img_code.'_'.$photo_no.'_'.$des_img;
					if($des_img!=''){
						$des_img = $img_code.'_'.$photo_no.'_'.$des_img;
					}
					$des_tmp= $_FILES[$photo_name]['tmp_name'];

						if(!$session_photo){
						   $session_photo = $des_img;	
						}else{
							$session_photo .= ",".$des_img;
						}
					
					$f_Imge = company_path().'/desingImage/'.$des_img;
					move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
				}
				/*********** end product image code **********/
				$header_design = array();
				$header_design['category_id'] = trim($_POST['category_id']);
				$header_design['range_id'] = trim($_POST['range_id']);
				$header_design['product_name'] = trim($_POST['product_name']);
				$header_design['pro_id'] = trim($_POST['pro_id']);
				$header_design['pkg_unit'] = trim($_POST['pkg_unit']);
				$header_design['pkg_w'] = trim($_POST['pkg_w']);
				$header_design['pkg_h'] = trim($_POST['pkg_h']);
				$header_design['pkg_d'] = trim($_POST['pkg_d']);
				$header_design['width'] = trim($_POST['width_design']);
				$header_design['height'] = trim($_POST['height_design']);
				$header_design['density'] = trim($_POST['density_design']);
				$header_design['design_code'] = trim($_POST['design_code']);
				$header_design['description'] = trim($_POST['description']);	
				$header_design['design_image'] = $session_photo;
				$header_design['pro_cbm'] = trim($_POST['pro_cbm']);
				$header_design['pak_cbm'] = trim($_POST['pak_cbm']);
				$assemble = &$_SESSION['asb_part'];
				$consume = &$_SESSION['con_part'];
				$order_no = add_designcode($header_design,$assemble, $consume);
				unset($_SESSION['asb_part']);
				unset($_SESSION['con_part']);
				meta_forward($path_to_root.'design_code_search.php?');
		}
	
}
function handle_update_commit_design(){
	
	global $selected_id;
	$selected_id = $_SESSION['s_id'];
	
	/* /************ product image code ***************
	$deg_image = $_SESSION['design_photo'];
	$session_photo = "";
	
	$deg_img_arry = explode(',', $deg_image);	
	$photos_array = array(); 
	$photo_count = trim($_POST['no_finish_pro_img']);
	for($photo_no=1; $photo_no <= $photo_count; $photo_no++){
		$photo_name = "pic_".$photo_no;
		
		if(!empty ($_FILES[$photo_name]['name'])){
			$design_image = $_FILES[$photo_name]['name'];				
			
			$dimage_tmp= $_FILES[$photo_name]['tmp_name'];
			
				$f_Imge = company_path().'/desingImage/'.$design_image;
				move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
		}else{
					$design_image =  $deg_img_arry[$photo_no-1];
		}
		if(!isset($_SESSION['del_img'][$photo_no]) && $_SESSION['del_img'][$photo_no] != 'yes'){
			if($design_image != ''){	
				$photos_array[] = $design_image;
			}
		}
	}
	unset($_SESSION['del_img']);
	if(count($photos_array)>0){
				update_design_image($selected_id, implode(",", $photos_array) );		
	}else{
		$photos_array = '';
		update_design_image($selected_id, $photos_array );
		
	}
	
	*********** end product image code ********** */
	$udpate_design = array();
	/*$udpate_design['category_id'] = trim($_POST['category_id']);
	$udpate_design['range_id'] = trim($_POST['range_id']);*/
	$udpate_design['product_name'] = trim($_POST['product_name']);
	$udpate_design['pro_id'] = trim($_POST['pro_id']);
	$udpate_design['pkg_unit'] = trim($_POST['pkg_unit']);
	$udpate_design['pkg_w'] = trim($_POST['pkg_w']);
	$udpate_design['pkg_h'] = trim($_POST['pkg_h']);
	$udpate_design['pkg_d'] = trim($_POST['pkg_d']);
	$udpate_design['width'] = trim($_POST['width_design']);
	$udpate_design['height'] = trim($_POST['height_design']);
	$udpate_design['density'] = trim($_POST['density_design']);
	$udpate_design['design_code'] = trim($_POST['design_code']);
	$udpate_design['description'] = trim($_POST['description']);	
	$udpate_design['pro_cbm'] = trim($_POST['pro_cbm']);	
	$udpate_design['pak_cbm'] = trim($_POST['pak_cbm']);	
	$assemble = &$_SESSION['asb_part'];
	$consume = &$_SESSION['con_part'];
	$order_no = update_designcode($selected_id, $udpate_design, $assemble, $consume);
	unset($_SESSION['asb_part']);
	unset($_SESSION['con_part']);
	meta_forward($path_to_root.'design_code_search.php?');	
}


//-------------------------------------------------------
//------------------ update frist section part ---------------
function handle_update_first_design(){
	
	global $selected_id;
	$selected_id = $_SESSION['s_id'];
	$img_code = $_POST['design_code'];
	/************ product image code ***************/
	$deg_image = $_SESSION['design_photo'];
	$session_photo = "";
	
	$deg_img_arry = explode(',', $deg_image);	
	$photos_array = array(); 
	$photo_count = trim($_POST['no_finish_pro_img']);
	for($photo_no=1; $photo_no <= $photo_count; $photo_no++){
		$photo_name = "pic_".$photo_no;
		
		if(!empty ($_FILES[$photo_name]['name'])){
			$design_image = $_FILES[$photo_name]['name'];				
			$design_image = $img_code.'_'.$photo_no.'_'.$design_image;
			$dimage_tmp= $_FILES[$photo_name]['tmp_name'];
			
				$f_Imge = company_path().'/desingImage/'.$design_image;
				move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
		}else{
					$design_image =  $deg_img_arry[$photo_no-1];
		}
		if(!isset($_SESSION['del_img'][$photo_no]) && $_SESSION['del_img'][$photo_no] != 'yes'){
			if($design_image != ''){	
				$photos_array[] = $design_image;
			}
		}
	}
	unset($_SESSION['del_img']);
	unset($_SESSION['design_photo']);
	if(count($photos_array)>0){
				update_design_image($selected_id, implode(",", $photos_array) );		
	}else{
		$photos_array = '';
		update_design_image($selected_id, $photos_array );
		
	}
	
	/*********** end product image code **********/
	$udpate_design = array();
	/*$udpate_design['category_id'] = trim($_POST['category_id']);
	$udpate_design['range_id'] = trim($_POST['range_id']);*/
	$udpate_design['product_name'] = trim($_POST['product_name']);
	$udpate_design['pro_id'] = trim($_POST['pro_id']);
	$udpate_design['pkg_unit'] = trim($_POST['pkg_unit']);
	$udpate_design['pkg_w'] = trim($_POST['pkg_w']);
	$udpate_design['pkg_h'] = trim($_POST['pkg_h']);
	$udpate_design['pkg_d'] = trim($_POST['pkg_d']);
	$udpate_design['width'] = trim($_POST['width_design']);
	$udpate_design['height'] = trim($_POST['height_design']);
	$udpate_design['density'] = trim($_POST['density_design']);
	$udpate_design['design_code'] = trim($_POST['design_code']);
	$udpate_design['description'] = trim($_POST['description']);	
	$udpate_design['pro_cbm'] = trim($_POST['pro_cbm']);	
	$udpate_design['pak_cbm'] = trim($_POST['pak_cbm']);	
	/*$assemble = &$_SESSION['asb_part'];
	$consume = &$_SESSION['con_part'];*/
	$order_no = update_first_designcode($selected_id, $udpate_design);
	/*unset($_SESSION['asb_part']);
	unset($_SESSION['con_part']);*/
	
	//meta_forward($_SERVER['PHP_SELF']);	
	
}
//------------------ end first section part --------------
//------------------- line start table ------------
function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  set_focus('part_name');
}
function line_con_start_focus() {
  global 	$Ajax;

  $Ajax->activate('con_table');
  //set_focus('cons_type');
}

//------------------- end line start table --------------

$id = find_submit('Delete');
if ($id != -1)
{
	handle_assemble_delete_item($id);
}
if (isset($_POST['AssembleAddItem']))
{
	handle_new_assemble_design();
}
if (isset($_POST['AssembleUpdateItem'])){
	handle_assemble_update_item();
}
if (isset($_POST['AssembleCancelItemChanges'])) {
	line_start_focus();
}

$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_consume_delete_item($id);
}
if (isset($_POST['ConsumeAddItem']))
{
	handle_new_consume_design();
}
if (isset($_POST['ConsumeUpdateItem'])){
 handle_consume_update_item();
}
if (isset($_POST['ConsumeCancelItemChanges'])) {
	line_con_start_focus();
}
if (isset($_POST['AddDesign']))
{
	handle_commit_design();
}
if (isset($_POST['UpdateDesign']))
{
	handle_update_commit_design();
}

if (isset($_POST['UpdateFirst']))
{
	handle_update_first_design();
}
//---------------------------------------------------------------------
$myrow = $_POST['design_code'];

if($myrow['design_code']==''){
	echo "<input type='hidden' id='des_id' value=''>";
	unset($_SESSION['asb_part']);
	unset($_SESSION['con_part']);
}else{
 echo "<input type='hidden' id='des_id' value='".$myrow."'>";
}
echo "<input type='hidden' id='range_id_code_url' value=''>";
echo "<input type='hidden' id='code_id' value=''>";
start_form(true);

//---------------------------- start table ------------------------------------------------------

display_design_hearder($_SESSION['DCODE']);


if ($selected_id != -1) 
{
	echo "<br><center>";
	submit_center_first('UpdateFirst', _("Update"), _('Update to design code'), 'default');

 
	echo "</center><br><br>";
}
display_assemble_summary($_SESSION['asb_part']);

display_consumable_summary($_SESSION['con_part']);


if ($selected_id != -1) 
{
	echo "<br><center>";
	submit_center_first('UpdateDesign', _("Update"), _('Update to design code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to design code'), 'default');
	
	
	echo "<br><button><a href='finish_product.php?design_id=".$_POST['design_id']."'>Generate Finish Code</a></button>";
	
	//--  end code -   
	echo "</center>";
}
else
{
	submit_center_last('AddDesign', _("Add Design Code"), '', 'default');
}
/** detail **/


/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';


end_form();
end_page();



?>
<?php
if (isset($_GET['ModifyDesignNumber'])){
?>
<input type="hidden" name="design_code_old" id="design_code_old" value="<?php echo $_POST['design_code']; ?>"  />
<script> $(document).on('change',"#design_code", function(){
	var design_code = $(this).val();
	$("#ajaxmark").css("visibility","visible");
	$.ajax({
			url: "check_availablity.php",
			method: "POST",
            data: { code : design_code },
			success: function(data){
					if(data == 'true')
					{
						alert("Design Code already exists. Try new design code.");
						var design_code_old = $("#design_code_old").val();
                        $("#design_code").val(design_code_old);
						$("#design_code").focus();
					}
					$("#ajaxmark").css("visibility","hidden");
				}
		});
		return false;
}); 
</script>
<?php } ?>
<style>
button#AddDesign {
	align-content: center;
	margin-left: 500px;
}
</style>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $("#collection_id").val();	
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
			
		});
		$(document).on('change','#part_collection_id',function(){
			var collect_name = $("#part_collection_id").val();	

			 if(collect_name == 'NonCollection')
			    {
					$("#part_range_id_code").attr('disabled', true);
				}else{
					$("#part_range_id_code").attr('disabled', false);	
					
				}
				
		});

		$(document).on('change','#part_collection_id',function(){
		var collect_name = $('#part_collection_id').val();	
		var cat_id = $('#part_slc_category').val();
		  if(collect_name == 'NonCollection')
		    {
				$("#part_range_id_code").attr('disabled', true);
			}else{
				$("#part_range_id_code").attr('disabled', false);	
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "part_get_design_code.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#part_code_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	

	$(document).on('change','#part_range_id_code',function(){
		var collect_name = $('#part_collection_id').val();	
		var range_id = $('#part_range_id_code').val();
		var categ_id = $('#part_slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "part_get_design_code.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#part_code_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
		
	});	

		
		
</script>
<script type="text/javascript">
	$(document).on('change','#range_id_code',function(){
		//alert("data is synchronization successfully .... ")
		var slc_range = $(this).val();
		$.ajax({
			url: "range_gen_code.php",
			method: "POST",
            data: { id : slc_range },
			success: function(data){
					$("#range_id_code_url").val(data);
				}
		});
		return false;
	});
	$(document).on('click','#generate_code',function(){
		//alert("data is synchronization successfully .... ")
		var slc_category = $('#slc_category').val();
		var slc_range = $('#range_id_code').val();
		var collection_name = $('#collection_id').val();
		var product_name = $('#product_name').val();
		var range_name = $("#range_id_code_url").val();
		
		$.ajax({
				url: "design_gen_code.php",
				method: "POST",
				data: { id : collection_name, catid: slc_category, rangeid : slc_range },
				success: function(data){
						
						$("#des_id").val(data);
						$("#design_code").val(data);
						if(range_name == ''){
							$("#design_description").val(product_name);
						}else{
							$("#design_description").val(range_name+" "+product_name);
						}
					}
			});
			return false;
	});
	$(function(){
    		jQuery('body').on('change', '#pic', function () {
			  var file = $('#pic').val();
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 
						reader.onloadend = function(){ // set image data as background of div
							$('#blah1').attr('src', this.result);
							$('#get_img1').val(files[0].name);
							var noimages = parseInt($('#no_finish_pro_img').val());
							var newnoimages = noimages+1;
		
							$.ajax({
									url: "finish_pro_addimages.php",
									method: "POST",
									data: { noimages : noimages},
									success: function(data){
										$('#finishpro_addnewimg').remove();
										$('#finish_pro_img_tbl').append(data);
										$('#no_finish_pro_img').val(newnoimages);
									}
								});
								return false;	
							
						}
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  $('#pic').val('');
				  $('#blah1').attr('src', 'img_not.png');
				}
			  }
			});
  	});
	
	$(function(){
    		jQuery('body').on('change', '.pic', function () {
			var file = $(this).val();
			var num = $(this).attr('data-num');
			//alert(num);
			  var exts = ['jpg', 'jpeg'];
			  // first check if file field has any value
			  if ( file ) {
				// split file name at dot
				var get_ext = file.split('.');
				// reverse name to check extension
				get_ext = get_ext.reverse();
				// check file type is valid as given in 'exts' array
				if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 	var files = !!this.files ? this.files : [];
					if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
			 		var fileSize = files[0].size/1024;
					if (/^image/.test( files[0].type)){ // only image file
						var reader = new FileReader(); // instance of the FileReader
						reader.readAsDataURL(files[0]); // read the local file
			 			if(fileSize > 300){
							alert("File size must be less than 300 kB");
							this.value = null;
							$('#blah'+num).attr('src', 'img_not.png');
							
						}else{
						
						reader.onloadend = function(){ // set image data as background of div
							$('#blah'+num).attr('src', this.result);
							$('#get_img'+num).val(files[0].name);
							var noimages = parseInt($('#no_finish_pro_img').val());
							var newnoimages = noimages+1;
		
							$.ajax({
									url: "finish_pro_addimages.php",
									method: "POST",
									data: { noimages : noimages},
									success: function(data){
										$('#finishpro_addnewimg').remove();
										$('#finish_pro_img_tbl').append(data);
										$('#no_finish_pro_img').val(newnoimages);
									}
								});
							return false;	
						}
						}
						
						
					}
				} else {
				  alert( 'Invalid file Extension!' );
				  //$('#pic2').val('');
				  $('#blah'+num).attr('src', 'img_not.png');
				}
			  }
			});
  	});
		
		
		$(document).on('click','#finishpro_addnewimg',function(){	
		var noimages = parseInt($('#no_finish_pro_img').val());
		var newnoimages = noimages+1;
		
		$.ajax({
				url: "finish_pro_addimages.php",
				method: "POST",
				data: { noimages : noimages},
				success: function(data){
					$('#finishpro_addnewimg').remove();
					$('#finish_pro_img_tbl').append(data);
					$('#no_finish_pro_img').val(newnoimages);
				}
			});
		return false;			
	});	
		
	//------------------ delete image ------------------
	
	$('body').on('click','.del_imge',function(){
		var img_id = $(this).attr("id");
		$('#blah'+img_id).attr('src', '');
		$.ajax({
				url: "del_image.php",
				method: "POST",
				data: { img_id : img_id},
				success: function(data){
					
				}
		});
	});
	
		
	
	
</script>
<script type="text/javascript">
/*function generateCode(){
   var design_id = document.getElementById('des_id').value;
   document.getElementById('design_code').value = design_id;
   document.getElementById('design_description').value = "Design Code:-"+design_id;
}*/

$(document).on('change','#part_code_design',function(){
	var design_code = $("#part_code_design").val();
	var current_variable = $(this);
	$.ajax({
		url: "range_gen_code.php",
		method: "POST",
        data: { d_code : design_code },
		success: function(data){
			 	$res = data.split("-");
				var product_name = $res[0];
				var width = $res[1];
				var height = $res[2];
				var density = $res[3];
				current_variable.parents('tr').find('#part_name').val(product_name);
				current_variable.parents('tr').find('#part_width').val(width);
				current_variable.parents('tr').find('#part_density').val(density);
				current_variable.parents('tr').find('#part_height').val(height);
			}
	});
	return false;
	
});
$(document).on('click','#old_code',function(){
	 var check_value = 0;
		$("#old_code:checked").each(function () {
             check_value = $(this).val();
        });
	 if(check_value == 1){
	 	$("#part_code_design").css("display","block");
		$("#assemble_code").css("display","none");
		$("#show_cate").css("display","block");
		var collect_name = $('#part_collection_id').val();	
		var range_id = $('#part_range_id_code').val();
		var categ_id = $('#part_slc_category').val(); 
		if(categ_id != '-1' && collect_name == 'NonCollection' ){
			$.ajax({
			      url: "part_get_design_code.php",
			      method: "POST",
                  data: { cid : categ_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#part_code_design').append(data);
				 }
		       });
		}
		if(categ_id != '-1' &&  range_id != '-1' && collect_name == 'Collection'){
			$.ajax({
			      url: "part_get_design_code.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#part_code_design').append(data);
				 }
		       });
		}
		
	 }else{
	 	$("#part_code_design").css("display","none");
		$("#assemble_code").css("display","block");	
	 }
});

$(document).on('click','#assemble_code',function(){
		//alert("data is synchronization successfully .... ")
		var collect_name = $('#part_collection_id').val();	
		var range_id = $('#part_range_id_code').val();
		var categ_id = $('#part_slc_category').val(); 
		//alert(collect_name+"--"+range_id+"---"+categ_id);
		var current_variable = $(this);
		$.ajax({

				url: "assemble_design_gen_code.php",
				method: "POST",
				data: { id : collect_name, catid: categ_id, rangeid : range_id },
				success: function(data){
					current_variable.parents('tr').find('#assemble_code').val(data);		
						
				}
			});
			return false;
	});
function asb_code_generate(){
     var design_id = document.getElementById('des_id').value;
	 var postfix = document.getElementById('asb_code').value;
		var codeA = postfix.charCodeAt();
	
	 if(!codeA){
		 var postfix = 65;
	 }else{
		 var postfix = parseInt(codeA)+1;
	 }
	 var postfix = String.fromCharCode(postfix);
	 document.getElementById('assemble_code').value = design_id+"#"+postfix;
  
}

function generateProductCBM(){
   var width_design = parseInt(document.getElementById('width_design').value);
    var density_design = parseInt(document.getElementById('density_design').value);
	 var height_design = parseInt(document.getElementById('height_design').value);
	 var pro_cbm_size = (width_design*density_design*height_design)/1000000;
   document.getElementById('pro_cbm').value = pro_cbm_size;
} 

function generatePackagingCBM(){
   var pkg_w = parseInt(document.getElementById('pkg_w').value);
    var pkg_d = parseInt(document.getElementById('pkg_d').value);
	 var pkg_h = parseInt(document.getElementById('pkg_h').value);
	 var cbm_size = (pkg_w*pkg_d*pkg_h)/1000000;
   document.getElementById('pak_cbm').value = cbm_size;
} 

$(document).on('click','#UpdateFirst',function(){
	alert('Data is successfully update');
});
</script>