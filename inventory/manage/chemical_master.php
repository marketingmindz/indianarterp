<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CONSUMABLE';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Consumable Management"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/consumable_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

// ----------------- image code ----------------------------



$upload_file = "";
if (isset($_FILES['pic']) && $_FILES['pic']['name'] != '') 
{
	$consumable_code = $_POST['consumable_code'];
	$result = $_FILES['pic']['error'];
 	$upload_file = 'Yes'; //Assume all is well to start off with
	$filename = company_path().'/consumableImage';
	if (!file_exists($filename))
	{
		mkdir($filename);
	}	
	$filename .= "/".item_img_name($consumable_code).".jpg";
	
	//But check for the worst 
	if ((list($width, $height, $type, $attr) = getimagesize($_FILES['pic']['tmp_name'])) !== false)
		$imagetype = $type;
	else
		$imagetype = false;
	//$imagetype = exif_imagetype($_FILES['pic']['tmp_name']);
	if ($imagetype != IMAGETYPE_GIF && $imagetype != IMAGETYPE_JPEG && $imagetype != IMAGETYPE_PNG)
	{	//File type Check
		display_warning( _('Only graphics files can be uploaded'));
		$upload_file ='No';
	}
	elseif (!in_array(strtoupper(substr(trim($_FILES['pic']['name']), strlen($_FILES['pic']['name']) - 3)), array('JPG','PNG','GIF')))
	{
		display_warning(_('Only graphics files are supported - a file extension of .jpg, .png or .gif is expected'));
		$upload_file ='No';
	} 
	elseif ( $_FILES['pic']['size'] > ($max_image_size * 1024)) 
	{ //File Size Check
		display_warning(_('The file size is over the maximum allowed. The maximum size allowed in KB is') . ' ' . $max_image_size);
		$upload_file ='No';
	} 
	elseif (file_exists($filename))
	{
		$result = unlink($filename);
		if (!$result) 
		{
			display_error(_('The existing image could not be removed'));
			$upload_file ='No';
		}
	}
	
	if ($upload_file == 'Yes')
	{
		$result  =  move_uploaded_file($_FILES['pic']['tmp_name'], $filename);
	}
	$Ajax->activate('details');
 /* EOF Add Image upload for New Item  - by Ori */
}
$consumable_image = $_POST['consumable_code'].".JPG";
if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['consumable_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The consumable name cannot be empty."));
		set_focus('consumable_name');
	}
	if (strlen($_POST['consumable_code']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Code cannot be empty."));
		set_focus('consumable_code');
	}
	
	if ($input_error !=1)
	{
    	if ($selected_id != -1) 
    	{
		    update_consumable_master($selected_id, $_POST['master_id'],
				$_POST['company_id'],	$_POST['unit_id'], 
				$_POST['principal_id'], $_POST['consumable_code'], 
				$_POST['consumable_name'], $_POST['size'],
				$_POST['weight'], $_POST['cbm'],	$_POST['gsm'],	$_POST['description'],
				$_POST['document'], $consumable_image);
			display_notification(_('Selected consumable management has been updated'));
    	} 
    	else 
    	{
		    add_consumable_master($_POST['master_id'],
				$_POST['company_id'],	$_POST['unit_id'], 
				$_POST['principal_id'], $_POST['consumable_code'], 
				$_POST['consumable_name'], $_POST['size'], 
				$_POST['weight'], $_POST['cbm'],	$_POST['gsm'],	
				$_POST['description'],	$_POST['document'], $consumable_image);
			display_notification(_('New consumable management has been added'));
    	}
		$Mode = 'RESET';
	}
}


if ($Mode == 'Delete')
{
		delete_consumable_master($selected_id);
		display_notification(_('Selected consumable has been deleted'));	
	$Mode = 'RESET';
}

/*
if (list_updated('mb_flag')) {
	$Ajax->activate('details');
}*/

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//----------------------------------------------------------------------------------
$result1 = get_generatecode();
while ($myrow1 = db_fetch($result1)) 
{
	
	$con_id = $myrow1['consumable_id'];
}
echo "<input type='hidden' id='cons_id' value='$con_id '>";
//$result = get_item_categories(check_value('show_inactive'));
$result = get_consumable_master(check_value('show_inactive'));
start_form(true);

//---------------------------- start table ------------------------------------------------------

div_start('details');
/** detail **/

start_outer_table(TABLESTYLE2);

table_section(1);
table_section_title(_("Consumable Master"));
if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing item category
		$myrow = get_consumable_master_row($selected_id);

		$_POST['consumable_id'] = $myrow["consumable_id"];
		$_POST['master_id']  = $myrow["master_id"];
		$_POST['company_id']  = $myrow["company_id"];
		$_POST['unit_id']  = $myrow["unit_id"];
		$_POST['principal_id']  = $myrow["principal_id"];
		$_POST['consumable_code']  = $myrow["consumable_code"];
		$_POST['consumable_name']  = $myrow["consumable_name"];
		$_POST['size']  = $myrow["size"];
		$_POST['weight']  = $myrow["weight"];
		$_POST['cbm']  = $myrow["cbm"];
		$_POST['gsm']  = $myrow["gsm"];
		$_POST['description']  = $myrow["description"];
		$_POST['document']  = $myrow["document"];
		$_POST['pic']  = $myrow["consumable_image"];
	} 
	hidden('selected_id', $selected_id);
	hidden('consumable_id');
} 
stock_master_list_row(_("Master:"), 'master_id', null);
text_row(_("Consumable Name:"), 'consumable_name', null, 30, 30, '', '', '', 'id="consumable_name"');  
text_row(_("Weight:"), 'weight', null, 30, 30); 
text_row(_("Document:"), 'document', null, 30, 30);
stock_units_list_row(_("Units:"), 'unit_id', null);
stock_principal_list_row(_("Principal Type:"), 'principal_id', null);
file_row(_("Image File (.jpg)") . ":", 'pic', 'pic');
/** image code **/
/*$stock_img_link = "";
	$check_remove_image = false;
	if (isset($_POST['NewStockID']) && file_exists(company_path().'/images/'
		.item_img_name($_POST['NewStockID']).".jpg")) 
	{
	 // 31/08/08 - rand() call is necessary here to avoid caching problems. Thanks to Peter D.
		$stock_img_link .= "<img id='item_img' alt = '[".$_POST['NewStockID'].".jpg".
			"]' src='".company_path().'/images/'.item_img_name($_POST['NewStockID']).
			".jpg?nocache=".rand()."'"." height='$pic_height' border='0'>";
		$check_remove_image = true;
	} 
	else 
	{
		$stock_img_link .= _("No image");
}*/
/** image code **/
table_section(2);

table_section_title(_("Consumable Master"));

stock_company_list_row(_("Company:"), 'company_id', null);
text_row(_("Size:"), 'size', null, 30, 30); 
text_row(_("CBM:"), 'cbm', null, 30, 30); 
text_row(_("GSM:"), 'gsm', null, 30, 30); 
text_row(_("Code:"), 'consumable_code', null, 30, 30, '','','','id="consumable_code" readonly="true"');
button_code_generate( _(""),'generate_code', 'Generate Code','','','','','','id="generate_code" onclick="generateCode()"');
textarea_row(_('Description:'), 'description', null, 42, 3);

end_outer_table(1);
div_end();
submit_add_or_update_center($selected_id == -1, '', 'both');



/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';


start_table(TABLESTYLE, "width=80%");
$th = array(_("Code"),_("Consumable Name"),_("Company Name"), _("Master Name"), _("Principal Type"), _("Unit"), _("Weight"),
_("Size"), _("CBM"), _("GSM"),_("Description"),_("Image"), "", "");
//inactive_control_column($th);

table_header($th);
$k = 0; //row colour counter

while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);

	label_cell($myrow["consumable_code"]);
	label_cell($myrow["consumable_name"]);
	label_cell($myrow["company_name"]);
	label_cell($myrow["master_name"]);
	label_cell($myrow["principal_name"]);
	label_cell($myrow["unit_id"]);
	label_cell($myrow["weight"]);
	label_cell($myrow["size"]);
	label_cell($myrow["cbm"]);
	label_cell($myrow["gsm"]);
	label_cell($myrow["description"]);
 	label_cell($myrow["consumable_image"]);
	//inactive_control_cell($myrow["consumable_id"], $myrow["inactive"], 'consumable_master', 'consumable_id');
 	edit_button_cell("Edit".$myrow["consumable_id"], _("Edit"));
 	delete_button_cell("Delete".$myrow["consumable_id"], _("Delete"));
	end_row();
}

//inactive_control_row($th);
end_table();
end_form();

end_page();

?>

<script type="text/javascript">
function generateCode(){
   var consumble_name = document.getElementById('consumable_name').value;
    var consumble_id = document.getElementById('cons_id').value;
	var con_id = parseInt(consumble_id)+1;
   var con_cod = consumble_name.substring(0,3);
   document.getElementById('consumable_code').value = con_cod+"00"+con_id;
} 
</script>