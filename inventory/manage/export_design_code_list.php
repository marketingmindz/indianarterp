<?php
$path_to_root="../..";
include_once($path_to_root . "/config_db.php");
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/finish_product_db.inc");


$sql = 'Select d.design_code,d.product_name, CASE WHEN d.range_id=-1 || d.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeNAME, c.cat_name, p.pro_name, d.weight, d.height, d.density, d.description from '.TB_PREF.'design_code d Left Join '.TB_PREF.'item_range t on d.range_id = t.id Left Join '.TB_PREF.'item_category c on d.category_id = c.category_id 
	Left Join '.TB_PREF.'item_products p on d.product_type = p.pro_id';

function cleanData(&$str)
  {
    $str = preg_replace("/\t/", "\\t", $str);
    $str = preg_replace("/\r?\n/", "", $str);
	if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  }
header("Content-Type: text/plain");

$result = db_query($sql);
$result1 = db_fetch_assoc($result);
echo implode("\t", array_keys($result1)) . "\r\n";
while($result1 = db_fetch_assoc($result))
{
	array_walk($result1, 'cleanData');
	echo implode("\t", array_values($result1)) . "\r\n";
}

$filename = "Design_Code_List_" . date('Ymd') . ".xls";
  header("Content-Disposition: attachment; filename=\"$filename\"");
  header("Content-Type: application/vnd.ms-excel; name='excel'");

?>