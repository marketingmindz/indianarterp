<?php
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
include_once($path_to_root . "/inventory/includes/finish_product_class.inc");



	if(isset($_REQUEST['type']) &&  $_REQUEST['type'] == 'procode'){	
		?>
        <center>
                	<table class="tablestyle" colspan="7" width="60%" cellpadding="2" cellspacing="0" width="500">
						<tbody>
                        	<tr>
								<td class="tableheader" width="10%">S.N</td>
								<td class="tableheader" width="20%">Combination Name</td>
                                <td class="tableheader" width="20%">Product Code</td>
                                <td class="tableheader" width="20%">Product Name</td>
								<td class="tableheader" width="30%">Fabric Code</td>
							</tr>
                             <?php 
								#Get existing fabric combination name
								$sql_finishpro = "SELECT finish_pro_id FROM ".TB_PREF."finish_product where finish_comp_code = '".$_REQUEST['code']."' ";
								$result_finishpro = db_query($sql_finishpro, "The fabric detail records could not be get");
								$row_finishpro = db_fetch($result_finishpro);
								$finishpro_id = $row_finishpro['finish_pro_id'];
								$finish_product_name = $row_finishpro['finish_product_name'];
								$con_array = array();
								$full_con_array = array();
								if( $finishpro_id != ''){
									$sql_fabcombid = "SELECT fab_code_id FROM ".TB_PREF."fabric_pro_relation where finish_code_id = ".$finishpro_id." ";
									$result_fabcombid = db_query($sql_fabcombid, "The finish detail records could not be get");
									$row_fabcombid = db_fetch($result_fabcombid);
									$fabcode_id = $row_fabcombid['fab_code_id'];	
										#Get existing fabric combination details
										$sql_fabcode = "SELECT * FROM ".TB_PREF."fabric_combination where fab_code_id = '".$fabcode_id."' ";
										$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
										$num=1;
										while ($row_fabcode = db_fetch($result_fabcode)) {?>
										<tr>
                                        <?php
									$fab_code = $row_fabcode['fab_code_id'];
									$sql_fab_code_name = "SELECT fabric_comb FROM ".TB_PREF."fabric_code where fab_code_id = ".$fab_code;
									$result_fabcode_name = db_query($sql_fab_code_name, " No fabric name");
									$row_fabcode_name = db_fetch($result_fabcode_name);
									
									$sql_product_id = "SELECT finish_code_id FROM ".TB_PREF."fabric_pro_relation where fab_code_id = ".$fab_code;
									$result_product_id = db_query($sql_product_id, " No product id");
									$row_product_id = db_fetch($result_product_id);
									$product_id = $row_product_id['finish_code_id'];
									
									$sql_product_name = "SELECT * FROM ".TB_PREF."finish_product where finish_pro_id = ".$product_id;
									$result_product_name = db_query($sql_product_name, " No product id");
									$row_product_name = db_fetch($result_product_name);
									
									$fab_consu_name = $row_fabcode['fabric_code_name'];
									$sql_fab_consu_name = "SELECT consumable_code FROM ".TB_PREF."consumable_master where consumable_id = ".$fab_consu_name;
									$result_fab_consu_name = db_query($sql_fab_consu_name, " No fabric name");
									$row_fab_consu_name = db_fetch($result_fab_consu_name);
									$con_array  = $row_fab_consu_name['consumable_code'];
									
									
									array_push($full_con_array, $con_array);
									
									 $imploded = implode(",", $full_con_array);
							
									
									 }
								?>
                                            <td><?php echo $num++;?></td>
                                              <td> <?php echo $row_fabcode_name['fabric_comb'];?></td>
                                              <td> <?php echo $row_product_name['finish_comp_code'];?></td>
                                             <td> <?php echo $row_product_name['finish_product_name'];?></td>
                                    		<td> <?php echo $imploded;?></td>	
                                            
                                           	
										</tr>	
										<?php
								
								}else{
								?>
                                    <tr>
                                        <td>No Record</td>
                                        <td>No Record </td>
                                        <td>No Record</td>	
                                    </tr>
                            <?php }?>
						</tbody>
					</table>
				</center>
        <?php
	}else{ ?>
    <center>
                	<table class="tablestyle" colspan="7" width="60%" cellpadding="2" cellspacing="0" width="500">
						<tbody>
                        	<tr>
								<td class="tableheader" width="10%">S.N</td>
								<td class="tableheader" width="20%">Combination Name</td>
                                <td class="tableheader" width="20%">Product Code</td>
                                <td class="tableheader" width="20%">Product Name</td>
								<td class="tableheader" width="30%">Fabric Code</td>
							</tr>
                            <?php 
								#Get existing fabric combination name
								$sql_fabcode = "SELECT * FROM ".TB_PREF."fabric_combination where fab_code_id = ".$_REQUEST['code'];
								$result_fabcode = db_query($sql_fabcode, "The fabric detail records could not be get");
								$num=1;
								$con_array = array();
								$full_con_array = array();
								while ($row_fabcode = db_fetch($result_fabcode)) {?>
								<tr>
                                <?php
									$fab_code = $row_fabcode['fab_code_id'];
									$sql_fab_code_name = "SELECT fabric_comb FROM ".TB_PREF."fabric_code where fab_code_id = ".$fab_code;
									$result_fabcode_name = db_query($sql_fab_code_name, " No fabric name");
									$row_fabcode_name = db_fetch($result_fabcode_name);
									
									$sql_product_id = "SELECT finish_code_id FROM ".TB_PREF."fabric_pro_relation where fab_code_id = ".$fab_code;
									$result_product_id = db_query($sql_product_id, " No product id");
									$row_product_id = db_fetch($result_product_id);
									$product_id = $row_product_id['finish_code_id'];
									
									$sql_product_name = "SELECT * FROM ".TB_PREF."finish_product where finish_pro_id = ".$product_id;
									$result_product_name = db_query($sql_product_name, " No product id");
									$row_product_name = db_fetch($result_product_name);
									
									
									$fab_consu_name = $row_fabcode['fabric_code_name'];
									$sql_fab_consu_name = "SELECT consumable_code FROM ".TB_PREF."consumable_master where consumable_id = ".$fab_consu_name;
									$result_fab_consu_name = db_query($sql_fab_consu_name, " No fabric name");
									$row_fab_consu_name = db_fetch($result_fab_consu_name);
									$con_array  = $row_fab_consu_name['consumable_code'];
									
									array_push($full_con_array, $con_array);
									
									 $imploded = implode(",", $full_con_array);
							
									
								?>
                                <?php }
							?>
                                    <td><?php echo $num++;?></td>
                                    <td> <?php echo $row_fabcode_name['fabric_comb'];?></td>
                                    <td> <?php echo $row_product_name['finish_comp_code'];?></td>	
                                    <td> <?php echo $row_product_name['finish_product_name'];?></td>
                                    <td> <?php echo $imploded;?></td>	
                                   	
								</tr>	
								
							
						</tbody>
					</table>
				</center>
    <?php
		
	}
   
   

?>