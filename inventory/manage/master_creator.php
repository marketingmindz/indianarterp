<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_MASTER';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Master Creation"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/master_creation_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['master_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Master name cannot be empty."));
		set_focus('master_name');
	}
	if (strlen($_POST['master_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Reference cannot be empty."));
		set_focus('master_reference');
	}

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
			
			
			
      		$check_update_master = check_update_master($_POST['master_reference']);
		    if($check_update_master)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			    update_master($selected_id, $_POST['master_name'], $_POST['master_description'], $_POST['master_reference']);
				display_notification(_('Selected Master has been updated'));
			  }
    	} 
    	else 
    	{
			
			
			
			$check_master = check_master($_POST['master_reference']);
		    if($check_master)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
    		    add_master($_POST['master_name'], $_POST['master_description'], $_POST['master_reference']);
				display_notification(_('New Master has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_master($selected_id);
		display_notification(_('Selected Master has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_master(check_value('show_inactive'));

start_form();

//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2, "style=min-width:320px;width:50%;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code
 		$edit_mod = "readonly";
		$myrow = get_master($selected_id);

		$_POST['master_name']  = $myrow["master_name"];
		$_POST['master_description']  = $myrow["master_description"];
		$_POST['master_reference']  = $myrow["master_reference"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Master Name:"), 'master_name', null, 40, 40);
textarea_row(_('Description:'), 'master_description', null, 50, 6);
text_row(_("Reference:"), 'master_reference', null, 40, 40, '','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec" '.$edit_mod.'');
//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');



echo "<br>";
start_table(TABLESTYLE, "style=min-width:320px;width:50%;");

$th = array(_('Master Name'),_('Description'),_('Reference'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	
	label_cell($myrow["master_name"]);
	$description = substr($myrow["master_description"],0,50)."...";
	label_cell($description);
	label_cell($myrow["master_reference"]);
	inactive_control_cell($myrow["master_id"], $myrow["inactive"], 'master_creation', 'master_id');
 	edit_button_cell("Edit".$myrow['master_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['master_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
		$("#ADD_ITEM").click(function(e) {
            var myVar = setInterval(function(){ 
	        clearInterval(myVar);
				window.location="http://erp.khatiart.in/";		
	    }, 500);
        });
	});
       
</script>
 <script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
   
</script>