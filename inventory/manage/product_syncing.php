<?php
	$page_security = 'SA_DESIGN';
	$path_to_root = "../..";
	include_once("../../includes/session.inc");
	include_once($path_to_root . "/includes/ui.inc");

function sync()
{
	
	//$range_name ='';	
	$item_query = "Select * from 0_finish_product Left join 0_item_category on 0_finish_product.category_id = 0_item_category.category_id Left join 0_item_range on 0_finish_product.range_id = 0_item_range.id";
	$result = mysql_query($item_query);	
	$items_details = array();
	$i = $j = $k = 0; 	
	while ($row = mysql_fetch_assoc($result)) {
			$items_details[$i] = $row;
			$i++;
	}

	$category_query = "Select * from 0_item_category";
	$category_result = mysql_query($category_query);	
	$category_details = array();
	$y=0; 	
	while ($row1 = mysql_fetch_assoc($category_result)) {
			$category_details[$y] = $row1;
			$y++;
	}	
	

	$crm_db = mysql_query("select * from 0_crm_setting");
	while($crm_db_res = mysql_fetch_array($crm_db))
	{
		$crm_db_details = $crm_db_res;
	}

	
	#Fetch fabric code from erp
	$fabric_query = "Select consumable_code, consumable_name from 0_consumable_master";
	$fabric_result = mysql_query($fabric_query);	
	$fabric_details = array();
	$fc=0; 	
	while ($fabric_row = mysql_fetch_assoc($fabric_result)) {
		$fabric_details[$fc] = $fabric_row;
		$fc++;
	}
	
	#Fetch Material change from erp
	$material_query = "Select wood_name from 0_wood_master";
	$material_result = mysql_query($material_query);	
	$material_details = array();
	$fm=0; 	
	while ($material_row = mysql_fetch_assoc($material_result)) {
		$material_details[$fm] = $material_row;
		$fm++;
	}
	
	#Fetch Range from erp
	$range_query = "Select range_name from 0_item_range";
	$range_result = mysql_query($range_query);	
	$range_details = array();
	$r=0; 	
	while ($range_row = mysql_fetch_assoc($range_result)) {
		$range_details[$r] = $range_row;
		$r++;
	}


	$hostname = $crm_db_details['crm_db_hostname'];
	$username = $crm_db_details['crm_db_username'];
	$password = $crm_db_details['crm_db_password'];
	$dbname = $crm_db_details['crm_db_name'];
	
	
	if($dbname == ''){
		return json_encode(array("sync"=>"Database Name Not Found. Set database name in CRM Setting"));
		die;	
	}	
	$conn1 = mysql_connect($hostname,$username,$password);
	if(!$conn1){
		return json_encode(array("sync"=>"Invalid CRM Credentials."));
		die;	
	}	

	mysql_select_db($dbname,$conn1);
	$date = date('Y-m-d H:i:s');

	$sync = 0;
	$new_product = 0;
	foreach($items_details as $instvalue)
	{
		$sync+=1;
		if($instvalue[finish_comp_code] != '')
		{
		$check_product = "select * from vtiger_products where serialno='$instvalue[finish_comp_code]'";

		$check_product = mysql_query($check_product);
		$check_product_res = mysql_fetch_array($check_product);
		$product_crm_id = $check_product_res['productid'];
		
		
		if($check_product_res['serialno'] == "" and $check_product_res['serialno'] == NULL)
		{
			$new_product+=1;		
			$crmid = mysql_query("SELECT id FROM vtiger_crmentity_seq");
			$crmid = mysql_fetch_assoc($crmid);
			
			$product_seq_res = mysql_query("SELECT * from vtiger_modentity_num where semodule='Products'");
			while($product = mysql_fetch_array($product_seq_res))
			{
				$product_seq =  $product['cur_id'];
				$product_prefix =  $product['prefix'];
			}
			$product_no = $product_prefix.$product_seq;
			$product_id = $crmid['id']+1;
			$crmid = $crmid['id']+1;		

			 $insert_product = "insert into vtiger_products(productid,product_no,productname,productcategory,discontinued,serialno,imagename,currency_id)values('$product_id','$product_no','$instvalue[finish_product_name]','$instvalue[cat_name]','1','$instvalue[finish_comp_code]','$instvalue[product_image]','1')";
			mysql_query($insert_product);

			$insert_entity = "insert into vtiger_crmentity(crmid,smcreatorid,smownerid,modifiedby,setype,description,createdtime,modifiedtime,version,presence,deleted,label)values('$crmid','1','1','1','Products','$instvalue[description]', '$date','$date','0','1','0','$instvalue[finish_product_name]')";
			mysql_query($insert_entity);

                        if($instvalue['range_id'] == '-1' || $instvalue['range_id'] == '0'){
				$range_name = "Non-Collection";
			}
			else{
				
				$range_name = $instvalue['range_name'];
			}
       

			$insert_productcf = "insert into vtiger_productcf(productid, cf_751) values('$crmid', '$range_name')";
			mysql_query($insert_productcf);
			
			$product_seq = $product_seq+1;
			$update_product_seq = "update vtiger_modentity_num SET cur_id = '$product_seq' where semodule='Products'";
			mysql_query($update_product_seq);
	
			$imagename = $instvalue['product_image'];
			$image = explode(",",$imagename);
			$counter = count($image);

			for($k=0; $k<$counter;$k++)
			{
				$product_imagename = $image[$k];
				$crmid= $crmid+1;
				$update_crmid = "update vtiger_crmentity_seq set id = '$crmid'";
				mysql_query($update_crmid);

				 $insert_img_entity = "insert into vtiger_crmentity(crmid,smcreatorid,smownerid,modifiedby,setype,description,createdtime,modifiedtime,version,presence,deleted)values('$crmid','1','1','0','Products Image','$instvalue[description]', '$date','$date','0','1','0')";
				 mysql_query($insert_img_entity);

				$insert_img = "insert into vtiger_attachments(attachmentsid,name,type,path,subject) values('$crmid','$product_imagename','image/jpeg','storage/2016/March/week4/','NULL')";
				mysql_query($insert_img);

				$insert_rel_img = "insert into vtiger_seattachmentsrel(crmid,attachmentsid) values('$product_id','$crmid')";
					mysql_query($insert_rel_img);

				
				
				$source_folder = "../../company/0/finishProductImage/";
				$destination_folder = "/home/inafpl/public_html/crm/storage/2016/March/week4/";
				
				$s_url = $source_folder.$product_imagename;

					if(file_exists($s_url))
					{
						if($product_imagename != "" && $product_imagename != NULL)
						{
								$data = file_get_contents($s_url);
								$destination = $destination_folder.$crmid."_".$product_imagename;
								$file = fopen($destination, "w+");
								fputs($file, $data);
								fclose($file);
						}
					}
			
			
			}

		}
		
		else
		{
			$product_crm_id;
			$instvalue['product_image'];
			$update_product ="UPDATE vtiger_products SET productname = '$instvalue[finish_product_name]', productcategory = '$instvalue[cat_name]', imagename = '$instvalue[product_image]'  WHERE serialno='$instvalue[finish_comp_code]'";
			mysql_query($update_product);
                        
                        if($instvalue['range_id'] == '-1'|| $instvalue['range_id'] == '0'){
				$range_name = "Non-Collection";
			}
			else{
				$range_name = $instvalue['range_name'];
				
			}
			
			
			//$insert_productcfs = "update vtiger_productcf set cf_751 = '".$range_name."' where productid =".$product_crm_id;
			$insert_productcf = "update vtiger_productcf set cf_751 = '".$range_name."' where productid =".$product_crm_id;
			mysql_query($insert_productcf);
			

			 $update_entity = "UPDATE vtiger_crmentity SET label = '$instvalue[finish_product_name]', deleted = '0', description = '$instvalue[description]', modifiedtime = '$date'  WHERE crmid = '$product_crm_id'";
			mysql_query($update_entity);
	

			$imagename = $instvalue['product_image'];
			

			$image = explode(",",$imagename);
			$counter = count($image);

	
			for($a=0; $a<$counter; $a++)
			{
				$product_crm_id++;
				$product_imagename = $image[$a];
				$update_attachment = "UPDATE vtiger_attachments SET name = '$product_imagename' WHERE attachmentsid = '$product_crm_id'";
				mysql_query($update_attachment);
				$update_entity = "UPDATE vtiger_crmentity SET description = '$instvalue[description]', modifiedtime = '$date'  WHERE crmid = '$product_crm_id'";
				mysql_query($update_entity);
					
				$source_folder = "../../company/0/finishProductImage/";
				$destination_folder = "/home/inafpl/public_html/crm/storage/2016/March/week4/";
		
				$s_url = $source_folder.$product_imagename;
				if(file_exists($s_url))
				{
					if($product_imagename != "" && $product_imagename != NULL)
					{
						$data = file_get_contents($s_url);
						$destination = $destination_folder.$product_crm_id."_".$product_imagename;
						$file = fopen($destination, "w+");
						fputs($file, $data);
						fclose($file);
						
					}
				}
			}
		}
		
		}
		
	}
	
	
	$cat = 0;
	$new_cat = 0;
	foreach($category_details as $catvalue)
	{
		//print_r($category_details);
		$cat+=1;
		$catvalue['cat_name'];
		
		if($catvalue['cat_name'] != '')
		{
			
			$check_cat = "select productcategory from vtiger_productcategory where productcategory='$catvalue[cat_name]'";
			
			$check_cat = mysql_query($check_cat);
			$check_cat_res = mysql_fetch_array($check_cat);
			$cat_check =$check_cat_res['productcategory'];
						
			if($cat_check == "" and $cat_check == NULL)
			{				
				$new_cat+=1;
				$check_cat_res['productcategory'];
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				
				$cat_seq_res = mysql_query("select id from vtiger_productcategory_seq");
				$cat_seq = mysql_fetch_array($cat_seq_res);
				$sort_order_id = $cat_seq['id'];
				
				$insert_cat = "Insert into vtiger_productcategory(productcategory,presence,picklist_valueid,sortorderid) values('$catvalue[cat_name]',1,'$picklist_valueid','$sort_order_id')";	
				
				mysql_query($insert_cat);
				
				$sort_order_id +=1;
				$update_cat_seq = "update vtiger_productcategory_seq set id='$sort_order_id'";
				mysql_query($update_cat_seq);
				
				$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
				
	
			}
		}	
	}

	// interested Category 
	$cat1 = 0;
	$new_cat1 = 0;
	foreach($category_details as $incatvalue)
	{
		$cat1+=1;
		//$catvalue['cat_name'];
		//echo $rngvalue['range_name']; 
				
		if($incatvalue['cat_name'] != '')
		{
			
		 	$check_incat = "select cf_775 from vtiger_cf_775 where cf_775 = '$incatvalue[cat_name]'";
			$result_incategrogy = mysql_query($check_incat);
			$row_incat = mysql_fetch_array($result_incategrogy);
			$incat_check =$row_incat['cf_775'];
						
			if($incat_check == "" and $incat_check == NULL)
			{
				$new_cat1+=1;
				//$row_cat['cf_757'];
				
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				$picklist_valueid +=1;
				$cat_seq_res = mysql_query("select id from vtiger_cf_775_seq");
				$cat_seq = mysql_fetch_array($cat_seq_res);
				$sort_order_id = $cat_seq['id'];
				
				$insert_incat = "Insert into vtiger_cf_775(cf_775,presence,picklist_valueid, sortorderid) values('$incatvalue[cat_name]',1,'$picklist_valueid','$sort_order_id')";	
				mysql_query($insert_incat); 
				
				$sort_order_id +=1;
				$update_incat_seq = "update vtiger_cf_775_seq set id='$sort_order_id'";
				mysql_query($update_incat_seq);
				
				//$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
			
			}
		}	
	}
	
	// Interested Range 


	$rng1 = 0;
	$new_rng1 = 0;
	foreach($range_details as $inrngvalue)
	{
		$rng1+=1;
		//echo $rngvalue['range_name']; 
				
		if($inrngvalue['range_name'] != '')
		{
			
		 	$check_inrng = "select cf_777 from vtiger_cf_777 where cf_777 = '$inrngvalue[range_name]'";
			$result_inrng = mysql_query($check_inrng);
			$row_inrng = mysql_fetch_array($result_inrng);
			$inrng_check =$row_inrng['cf_777'];
						
			if($inrng_check == "" and $inrng_check == NULL)
			{
				$new_rng1+=1;
				
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				$picklist_valueid +=1;
				$rng_seq_res = mysql_query("select id from vtiger_cf_777_seq");
				$rng_seq = mysql_fetch_array($rng_seq_res);
				$sort_order_id = $rng_seq['id'];
				
				$insert_rng = "Insert into vtiger_cf_777(cf_777,presence,picklist_valueid, sortorderid) values('$inrngvalue[range_name]',1,'$picklist_valueid','$sort_order_id')";	
				mysql_query($insert_rng); 
				
				$sort_order_id +=1;
				$update_rng_seq = "update vtiger_cf_777_seq set id='$sort_order_id'";
				mysql_query($update_rng_seq);
				
				//$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
			
			}
		}	
	}
	# Fabric Changes Syncing
	/*$fab = 0;
	$new_fab = 0;
	foreach($fabric_details as $fabvalue)
	{
		$fab+=1;
		//$fabvalue['consumable_code']; 
				
		if($fabvalue['consumable_code'] != '')
		{
			
		 	$check_fab = "select cf_940 from vtiger_cf_940 where cf_940 = '$fabvalue[consumable_code]-$fabvalue[consumable_name]'";
			$result_fab = mysql_query($check_fab);
			$row_fab = mysql_fetch_array($result_fab);
			$fab_check =$row_fab['cf_940'];
						
			if($fab_check == "" and $fab_check == NULL)
			{
				$new_fab+=1;
				$row_fab['cf_940'];
				
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				
				$fab_seq_res = mysql_query("select id from vtiger_cf_940_seq");
				$fab_seq = mysql_fetch_array($fab_seq_res);
				$sort_order_id = $fab_seq['id'];
				
				$cf_940_value = $fabvalue['consumable_code']."-".$fabvalue['consumable_name'];
				
				$insert_fab = "Insert into vtiger_cf_940(cf_940,sortorderid,presence) values('$cf_940_value','$sort_order_id',1)";	 
				mysql_query($insert_fab);
				
				$sort_order_id +=1;
				$update_fab_seq = "update vtiger_cf_940_seq set id='$sort_order_id'";
				mysql_query($update_fab_seq);
				
				$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
				
	
			}
		}	
	}*/
	
	
	# Material Changes Syncing
	/*$mat = 0;
	$new_mat = 0;
	foreach($material_details as $matvalue)
	{
		$mat+=1;
		//echo $matvalue['wood_name']; 
				
		if($matvalue['wood_name'] != '')
		{
			
		 	$check_mat = "select cf_942 from vtiger_cf_942 where cf_942 = '$matvalue[wood_name]'";
			$result_mat = mysql_query($check_mat);
			$row_mat = mysql_fetch_array($result_mat);
			$mat_check =$row_mat['cf_942'];
						
			if($mat_check == "" and $mat_check == NULL)
			{
				$new_mat+=1;
				$row_mat['cf_942'];
				
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				
				$mat_seq_res = mysql_query("select id from vtiger_cf_942_seq");
				$mat_seq = mysql_fetch_array($mat_seq_res);
				$sort_order_id = $mat_seq['id'];
				
				$insert_mat = "Insert into vtiger_cf_942(cf_942,sortorderid,presence) values('$matvalue[wood_name]','$sort_order_id',1)";	
				mysql_query($insert_mat); 
				
				$sort_order_id +=1;
				$update_mat_seq = "update vtiger_cf_942_seq set id='$sort_order_id'";
				mysql_query($update_mat_seq);
				
				$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
			
			}
		}	
	}
	*/
	
	# Range Syncing
	$rng = 0;
	$new_rng = 0;
	foreach($range_details as $rngvalue)
	{
		$rng+=1;
		//echo $rngvalue['range_name']; 
				
		if($rngvalue['range_name'] != '')
		{
			
		 	$check_rng = "select cf_751 from vtiger_cf_751 where cf_751 = '$rngvalue[range_name]'";
			$result_rng = mysql_query($check_rng);
			$row_rng = mysql_fetch_array($result_rng);
			$rng_check =$row_rng['cf_751'];
						
			if($rng_check == "" and $rng_check == NULL)
			{
				$new_rng+=1;
				$row_rng['cf_751'];
				
				$picklist_seq_res = mysql_query("select id from vtiger_picklistvalues_seq");
				$picklist_seq = mysql_fetch_array($picklist_seq_res);
				$picklist_valueid = $picklist_seq['id'];
				
				$rng_seq_res = mysql_query("select id from vtiger_cf_751_seq");
				$rng_seq = mysql_fetch_array($rng_seq_res);
				$sort_order_id = $rng_seq['id'];
				
				$insert_rng = "Insert into vtiger_cf_751(cf_751,sortorderid,presence) values('$rngvalue[range_name]','$sort_order_id',1)";	
				mysql_query($insert_rng); 
				
				$sort_order_id +=1;
				$update_rng_seq = "update vtiger_cf_751_seq set id='$sort_order_id'";
				mysql_query($update_rng_seq);
				
				$picklist_valueid +=1;
			
				$update_picklist_valueid = "update vtiger_picklistvalues_seq set id='$picklist_valueid'";
				mysql_query($update_picklist_valueid);
			
			}
		}	
	}
	
	

return json_encode(array("sync"=>$sync,"new_product"=>$new_product,"New_Cat"=>$new_cat),JSON_UNESCAPED_SLASHES);
}
echo sync();
?>