<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_LOCATIONMASTER';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");

page(_($help_context = "Location Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/location_master_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{
	
	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['location_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The location name cannot be empty."));
		set_focus('location_name');
	}
	if (strlen($_POST['work_center_id']) == 0) 
	{
		$input_error = 1;
		display_error(_("The work center name cannot be empty. select work center."));
		set_focus('work_center_id');
	}
	//


	if ($input_error != 1) 
	{	
    	if ($selected_id != -1) 
    	{
		  update_location_master($selected_id, $_POST['location_code'],$_POST['location_name'],$_POST['work_center_id'],$_POST['location_address'],$_POST['telephone_no'],$_POST['sec_telephone_no'],$_POST['facsimeile_no'],
		  $_POST['email_id']);
				display_notification(_('Selected location name has been updated'));
			
    	} 
    	else 
    	{
			
    		    add_location_master($_POST['location_code'],$_POST['location_name'],$_POST['work_center_id'],$_POST['location_address'],$_POST['telephone_no'],$_POST['sec_telephone_no'],$_POST['facsimeile_no'],$_POST['email_id']);
				display_notification(_('New location name has been added'));
		
    	}
    	
		$Mode = 'RESET';
		
	}
  } 

//-----------------------------------------------------------------------------------



//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_location_master($selected_id);
		display_notification(_('Selected location name has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------
$result = get_all_location_master(check_value('show_inactive'));

start_form();

start_table(TABLESTYLE2, "style=min-width:320px;width:40%;");
start_form();
if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code

		$myrow = get_location_master($selected_id);
		$_POST['location_code']  = $myrow["location_code"];
		$_POST['location_name']  = $myrow["location_name"];
		$work_id  = $myrow["work_center_id"];
		$_POST['location_address']  = $myrow["location_address"];
		$_POST['telephone_no']  = $myrow["telephone_no"];
		$_POST['sec_telephone_no']  = $myrow["sec_telephone_no"];
		$_POST['facsimeile_no']  = $myrow["facsimeile_no"];
		$_POST['email_id']  = $myrow["email_id"];
		
		$work_center_name = get_work_id($work_id);
		$work_name = $work_center_name;
		
	}
	hidden('selected_id', $selected_id);
} 
text_row(_("Location Code:"), 'location_code', null, 40, 40);
text_row(_("Location Name:"), 'location_name', null, 40, 40);
work_center_list_row(_("Work Center Name:"), null,$work_id, $work_name, false, true, true, true);


textarea_row(_('Address:'), 'location_address', null, 40, 6);
text_row(_("Telephone No:"), 'telephone_no', null, 40, 40);
text_row(_("Secondary Phone no:"), 'sec_telephone_no', null, 40, 40);
text_row(_("Facsimile No:"), 'facsimeile_no', null, 40, 40);
text_row(_("E-mail:"), 'email_id', null, 40, 40);
end_table(1);
submit_add_or_update_center($selected_id == -1, '', 'both');


echo "<br>";
start_table(TABLESTYLE, "style=min-width:320px;width:40%;");

$th = array(_('Sl.No.'),_('Location Code'),_('Location Name'),_('Location address'),_('Telephone No.'),_('Email address'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
$i=1;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	
	label_cell($i);
	label_cell($myrow["location_code"]);
	label_cell($myrow["location_name"]);
	
	$location_address = substr($myrow["location_address"],0,50);
	label_cell($location_address);
	label_cell($myrow["telephone_no"]);
	label_cell($myrow["email_id"]);
	inactive_control_cell($myrow["location_id"], $myrow["inactive"], 'location', 'location_id');
 	edit_button_cell("Edit".$myrow['location_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['location_id'], _("Delete"));
	end_row();
	$i++;
}
inactive_control_row($th);
end_table(1);

end_form();
end_page();

?>

<script src="../../js/jquery/jquery.js" ></script>

<script type="text/javascript">

$('body').on('click','.w_check', function() {
	var arr = [];

  $.each($("input[name='work_team']:checked"), function(){            
     arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#work_center_id').val(arr.join(", "));
  $('#work_center_name').val(prod.join(", "));
});
 
</script>
