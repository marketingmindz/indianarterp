<?php
error_reporting(0);
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
?>
<style type="text/css">
	.search_code
	{
		width:105px;
		height: 20px;
		line-height: 20px;
		padding-bottom: 21px;
		vertical-align: middle;
		font-family: "Lucida Grande", Geneva, Verdana, Arial, Helvetica, sans-serif;
		font-size: 13px;
		font-weight: bold;
		color:white;
		background-color:#012;
	}
	
	.code_s {
		font-size:15px !important;
		width: 175px;
		height: 25px;
	}
	.code_in{
		font-size:15px !important; 
		font-weight:bold !important;
	}
	.img{
		
		font-weight:bold;
		margin-left: 10px !important; 
		margin-bottom:10px !important; 
		margin-top:10px !important;
		font-size:15px !important;
	}
	.img1{
		
		font-weight:bold;
		margin-left: 10px !important; 
		margin-bottom:10px !important; 
		margin-top:10px !important;
		font-size:25px !important;
	}
	img{
		  margin-left: 10px !important;
	}
	div#finish_product {
  		
		width: auto;
	}
	div#fd {
		width: 100%;
		float: left;
	}
	.main_div{
		width:100%;
		float:left;
		margin-left:50px;
	}
	.enable{display: block !important;}
	.disable{display: none !important;}
	.live_search{
		margin-left: 92px !important;
		max-height: 120px;
		overflow-y: scroll;
		position: absolute;
		background-color: white;
		border: 1px solid #ddd;	
	}
	.check_class{margin-left:50px;}
	.check-mark-info{ width:100%; float:left; margin:0px; padding:0px; overflow:hidden; display:block; }
	.check-mark-info.mark{ margin-top:140px;}
	
</style>
<link rel="stylesheet" href="css/colorbox.css" />
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery(".product_img").colorbox({rel:'product_img' , transition:"none", slideshow:true});
		jQuery(".design_image").colorbox({rel:'design_image'});
		jQuery(".wood_image").colorbox({rel:'wood_image'});
	});
</script>
<?php
$page_security = 'PHOTO_BANK';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
page(_($help_context = "Photo Bank"));
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/fabric_master.inc");

simple_page_mode(true);
?>

<div class="check_class" >
<form method="post" action="" enctype="multipart/form-data">
    <table>
        <tr >
            <td  class="code_in">Enter Code : <input type="text" id="code_s" name="code_s" class="code_s"
           <?php 
            if(isset($_GET['finish_comp_code']))
			{
				echo "value='".$_GET['finish_comp_code']."'";
			}          
            ?>
             /><div id="livesearch" class="live_search"></div></td>
           
            <td><input type="button" name="search_code" class="search_code" id="search_code" value="Search" /></td>
            
        </tr>
        
    </table>
</form>


<div class="check-mark-info">


            <input type="checkbox" id="finished_image" name="coreComp[]" value="1" />Finished Image
            <input type="checkbox" id="design_image" name="coreComp[]" value="2" />Design Image
            <input type="checkbox" id="wood_image" name="coreComp[]" value="3" />Wood Image
             <input type="checkbox" id="fabric_image" name="coreComp[]" value="4" />Fabric Image
     
</div>
</div>

<script type="text/javascript">
jQuery( document ).ready(function($) { 
	
	
		 jQuery('#code_s').on('keyup', function(){
			var info= $( "#code_s" ).val();
			//var info= "This is my info";
			if(info.length<2)
			{
				jQuery("#livesearch").html("");
			}
			else
			{
			      $.ajax({
					type: "POST",
					 dataType:"JSON",
					url: 'code_search.php',
					data: {id : info}, 
					success: function(msg){
						
								if (msg.finish_comp_code!='' && msg.finish_comp_code != undefined ) {
									//jQuery("#fd").show();
								 jQuery("#livesearch").html(msg.finish_comp_code);
								// alert("here");
								}
								
								
					}
				});
			}
	   });
	   
	   //alert("hereee");
	   jQuery('#finish_img').addClass("disable");
	   jQuery('#pro_img').addClass("disable");
	   jQuery('#desg_img').addClass("disable");
	   jQuery('#wood_img').addClass("disable");
	   
	   jQuery('#finished_image').click(function(){
		if (this.checked) {
			jQuery('#finish_img').removeClass("disable");
	   		jQuery('#pro_img').removeClass("disable");
			jQuery('#finish_img').addClass("enable");
	   		jQuery('#pro_img').addClass("enable");
		} else {
			jQuery('#finish_img').addClass("disable");
	   		jQuery('#pro_img').addClass("disable");
			jQuery('#finish_img').removeClass("enable");
	   		jQuery('#pro_img').removeClass("enable");
		}
}) 
	    jQuery('#design_image').click(function(){
		if (this.checked) {
			jQuery('#desg_img').removeClass("disable");
			jQuery('#desg_img').addClass("enable");
		} else {
			jQuery('#desg_img').addClass("disable");
			jQuery('#desg_img').removeClass("enable");
	   		
		}
}) 
 jQuery('#wood_image').click(function(){
		if (this.checked) {
			jQuery('#wood_img').removeClass("disable");
			jQuery('#wood_img').addClass("enable");
		} else {
			jQuery('#wood_img').addClass("disable");
			jQuery('#wood_img').removeClass("enable");
	   		
		}
}) 
jQuery('#fabric_image').click(function(){
		if (this.checked) {
			jQuery('#fab_img').removeClass("disable");
			jQuery('#fab_img').addClass("enable");
		} else {
			jQuery('#fab_img').addClass("disable");
			jQuery('#fab_img').removeClass("enable");
	   		
		}
}) 
   
   
jQuery('#code_s').keyup(function(){
	var val = jQuery(this).val();
		if (val.length >= 2) {
			jQuery('.check-mark-info').addClass("mark");
		} else {
			jQuery('.check-mark-info').removeClass("mark");	   		
		}
}) 

});


</script>

<div class="main_div">

<?php

if(isset($_REQUEST['finish_comp_code']))
{
$path_to_root = "../..";

include_once($path_to_root . "/inventory/includes/inventory_db.inc");
	if(isset($_REQUEST['finish_comp_code']))
	{
	
	 $code=trim($_REQUEST['finish_comp_code']);
	}
	if(!empty($code))
	{
		$result_array=array();
		//echo $code;
		/* Get Finished product image */
		
		$sql = "SELECT * FROM `0_finish_product` WHERE `finish_comp_code` LIKE '%$code%'";
		
		$query1 = db_query($sql,"get the design code.");
		//$row = db_fetch($query1,MYSQL_NUM);
		//$finish_pro_id=$row['finish_pro_id'];
		//$wood_id=$row['wood_id'];
		//$design_id=$row['design_id'];
		//echo $design_id;
		//echo $row['collection_image'];
		
		
	?>
 
   <?php ?>
   <div id="finish_img" class="img1">
      <p>Finish Product :</p>
       <!-- <div id="finish_product">-->
        <?php 
          while($row = mysql_fetch_array($query1))
		  {
			  /*if($row['collection_image'] !='' && $row['collection_image'] !='No Image')
			  {
			  echo '<img src="../../company/0/collectionImage/' . $row['collection_image'] . '" alt="finish_product_image" width="100" height="100" />';
			  }*/
			  ?>
<!--         </div>
 </div>
-->  <!--<div id="pro_img" >
     <p class="img">Product Image :</p>-->
        <div id="Product_image">
       <?php
       $product_image=$row['product_image'];
		if($product_image !=''  && $product_image !='No Image')
		{	
			$product_image = explode(",", $product_image);
			$result = count($product_image);
			$i=0;
			while($result>$i)
			{
				echo '<a href="../../company/0/finishProductImage/' . $product_image[$i] . '" class="product_img"><img  src="../../company/0/finishProductImage/' . $product_image[$i] . '" alt="finish_product1_image" width="100" height="100" /></a>';
				$i++;
			}
		}
		$wood_id=$row['wood_id'];
		$design_id=$row['design_id'];
		$fabric_id=$row['fabric_id'];
	 }
	?>
        </div>
       
</div>
 <div id="desg_img" class="img1" >
    <p>Design :</p>
      <div id="design_code">
   
    <?php
		/* Get design image */
		$sql = "SELECT * FROM `0_design_code` WHERE `design_id` = ".db_escape($design_id);
		$query1 = db_query($sql,"get the design code.");
		//$row = db_fetch($query1,MYSQL_NUM);
		//$design_image=$row['design_image'];
		while($design_image = mysql_fetch_array($query1))
		{	
        $design_image=$design_image['design_image'];
		if($design_image !=''  && $design_image !='No Image')
		{	
			$design_image = explode(",", $design_image);
			$result = count($design_image);
			$i=0;
			while($result>$i)
			{
			echo '<a href="../../company/0/desingImage/' . $design_image[$i] . '" class="design_image"><img src="../../company/0/desingImage/' . $design_image[$i]. '" alt="design_code_image" width="100" height="100" /></a>';
				$i++;
			}
		}
		else
		{
		    echo "<h2>No Image</h2>";
		}
		}
		?>
       
        </div>
    </div>
       
    <div id="wood_img" class="img" >
       <p>Wood :</p>
        <div id="wood_master">  
       
        <?php
		/* Get wood image */
		
		
		$sql = "SELECT * FROM `0_wood_master` WHERE `wood_id` = ".db_escape($wood_id);
		$query1 = db_query($sql,"get the design code.");
		$row = db_fetch($query1,MYSQL_NUM);
		$wood_image=$row['wood_image'];
		if($wood_image !=''  && $wood_image !='No Image')
		{
			echo '<a href="../../company/0/woodImage/' . $wood_image[$i] . '" class="wood_image"><img src="../../company/0/woodImage/' . $wood_image . '" alt="wood_master_image" width="100" height="100" /></a>';
		}
		else
		{
			echo "<h2>No Image</h2>";
		}
		
		

	}

}

?>
       </div>
    </div>
    
    

</div>