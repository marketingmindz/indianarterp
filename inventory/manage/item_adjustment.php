<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMADJUSTMENT';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/item_adjustment_class.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/ui/item_adjustment_ui.inc");
include_once($path_to_root . "/inventory/includes/db/item_adjustment_db.inc");
include_once($path_to_root . "/inventory/includes/db/open_stock_db.inc");

$js = "";
 //  code by bajrang   17/07/2015
if(!isset($_POST['session_var']))
{
	if(!isset($_POST['ConsumeAddItem']) && !isset($_POST['ProductAddItem']))
	{
		if(!isset($_POST['ConsumeUpdateItem']) && !isset($_POST['ProductUpdateItem']))
		{
			unset($_SESSION['cons_part']);
			unset($_SESSION['pro_part']);
		}
	}
}
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Stock Register"), false, false, "", $js);

simple_page_mode(true);


if(isset($_SESSION['cons_part']) || isset($_SESSION['pro_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['cons_part']);
	 fixObject($_SESSION['pro_part']);
}else{
   $_SESSION['cons_part'] = $_SESSION['pro_part'] = new item_adjustment;
}


//------------ handle new consumable part ------------

function handle_new_consumable()
{
	
	$check_data = consumable_check_data();
	if($check_data){
		$_SESSION['cons_part']->add_to_cons_part(count($_SESSION['cons_part']->line_con),$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity']);
		$count_sessiong = count($_SESSION['cons_part']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}
//-------------- end new consumable part ---------------

//------------ handle new product type  ------------
function handle_new_product()
{
	$check_pro_data = product_check_data();
	 if($check_pro_data){
	   	$_SESSION['pro_part']->add_to_product_part(count($_SESSION['pro_part']->line_items),$_POST['category_id'],$_POST['collection_id'],$_POST['range_id'],$_POST['design_id'],$_POST['finish_code_id'],$_POST['pro_unit'],$_POST['pro_qty']);
		unset_form_product_variables();
		line_start_product_focus();
	 }
}
//-------------- end new product type ---------------

//------------ delete consumable type ------------
function handle_consumable_delete_item($line_no)
{
	array_splice($_SESSION['cons_part']->line_con, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end delete consumable type ---------------
//------------ delete product type ------------
function handle_product_delete_item($line_no)
{
	$l = $line_no;
	unset($_SESSION['pro_part']->line_items[$line_no]);
	unset_form_product_variables();
    line_start_product_focus();
}
//-------------- end delete product type ---------------

//------------ update consumable type ------------
function handle_consumable_type_update()
{
   $_SESSION['cons_part']->update_cons_item($_POST['line_no'],$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_quntity']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end update assemble code ---------------
//------------ update handle_product_type_update ------------
function handle_product_type_update()
{
   $_SESSION['pro_part']->update_product_item($_POST['line_no'],$_POST['category_id'],$_POST['collection_id'],$_POST['range_id'],$_POST['design_id'],$_POST['finish_code_id'],$_POST['pro_unit'],$_POST['pro_qty'] );
	unset_form_product_variables();
    line_start_product_focus();
}
//-------------- end handle_product_type_update ---------------


//*****************  check data    --**************************
function consumable_check_data()
{
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	
	if(!get_post('cons_quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('cons_quntity');
		return false;
	}
	if (!check_num('cons_quntity', 0))
    {
	   	display_error(_("The quntity entered must be numeric and not less than zero."));
		set_focus('cons_quntity');
	   	return false;	   
    }
    return true;	
}

function product_check_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Select product category"));
		set_focus('category_id');
		return false;
	}
	
	if(!get_post('pro_qty')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('pro_qty');
		return false;
	}
	if (!check_num('pro_qty', 0))
    {
	   	display_error(_("The quantity entered must be numeric and not less than zero."));
		set_focus('pro_qty');
	   	return false;	   
    }
	
    return true;	
}

//********************* end check data  ********************

//     ++++++++++     Unset variables    ++++++++++++
function unset_form_cons_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_quntity']);

}
function unset_form_product_variables() {
	unset($_POST['category_id']);
    unset($_POST['collection_id']);
    unset($_POST['design_id']);
	unset($_POST['range_id']);
	unset($_POST['finish_code_id']);
	unset($_POST['pro_qty']);
	unset($_POST['pro_unit']);
}

//++++++++++++++++++  end unset variables   +++++++

//------------------- line start table ------------
function line_start_consumable_focus() {
  global 	$Ajax;

  $Ajax->activate('cons_table');
  //set_focus('part_name');
}
function line_start_product_focus() {
  global 	$Ajax;

  $Ajax->activate('pro_table');
  //set_focus('cons_type');
}


//      handle consumable



$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_consumable_delete_item($id);
}
if (isset($_POST['ConsumeAddItem']))
{
	handle_new_consumable();
}
if (isset($_POST['ConsumeUpdateItem'])){
 handle_consumable_type_update();
}
if (isset($_POST['ConsumeCancelItemChanges'])) {
	line_start_consumable_focus();
}

$id = find_submit('Delete');
if ($id != -1)
{
	handle_product_delete_item($id);
}
// - - - - -  handle product type
if (isset($_POST['ProductAddItem']))
{
	handle_new_product();
}
if (isset($_POST['ProductUpdateItem'])){
	handle_product_type_update();
}
if (isset($_POST['ProductCancelItemChanges'])) {
	line_start_focus();
}


//------------------------ add all value --------------------------------
function check_adjustment_data()
{
	if(get_post('location_id') == -1) {
		display_error( _("Select location."));
		set_focus('location_id');
		return false;
	}
	if(!get_post('work_center_id')) {
		display_error( _("Select work center."));
		set_focus('work_center_id');
		return false;
	}
	if(!get_post('pro_team_id')) {
		display_error( _("select production team."));
		set_focus('pro_team_id');
		return false;
	}
	
    return true;	
}
function handle_adjustment()
{
	
	$check_adjustment = check_adjustment_data();
		
		$cart->design_id;
		if($check_adjustment){
				
				$header_adjustment = array();
				$header_adjustment['location_id'] = trim($_POST['location_id']);
				$header_adjustment['work_center_id'] = trim($_POST['work_center_id']);
				$header_adjustment['pro_team_id'] = trim($_POST['pro_team_id']);
				$header_adjustment['reference_name'] = trim($_POST['reference_name']);
				$header_adjustment['date'] = trim($_POST['date']);
				$header_adjustment['stock_type_id'] = trim($_POST['stock_type_id']);
				$header_adjustment['type_id'] = trim($_POST['type_id']);
				$header_adjustment['memo_description'] = trim($_POST['memo_description']);
				
				$product_type = &$_SESSION['pro_part'];
				$consumable_type = &$_SESSION['cons_part'];
				if($_POST['adj_type_id'] == 1)
				{
					$order_no = do_adjustment($header_adjustment,$consumable_type, $product_type);
				}
				else
				{
					$order_no = do_negative_adjustment($header_adjustment,$consumable_type, $product_type);
				}
				unset($_POST);
				unset($_SESSION['pro_part']);
				unset($_SESSION['cons_part']);
				display_notification("Stock has been registered.");
			
				meta_forward($path_to_root.'item_adjustment.php');
				
	
		}
	
}

if(isset($_POST['AddProcess']))
{
	handle_adjustment();
}

//----------------------------------------------------------------------------------

div_start("stock_register");
global 	$Ajax;

  $Ajax->activate('stock_register');
start_form(true);



display_item_adjustment_hearder($_SESSION['DCODE']);

display_consumable_summary($_SESSION['cons_part']);

display_product_summary($_SESSION['pro_part']);
display_memo();
/* if ($selected_id != -1) 
{
	echo "<br><center>";
	submit_center_first('UpdateDesign', _("Update"), _('Update to design code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to design code'), 'default');
	echo "</center>";
}
else
{ */

//start_row();
//echo '<input type="submit" name="AddProcess"  value="Process Adjustment" />';
	submit_center_last('AddProcess', _("Process Adjustment"), '', 'default');
//end_row();

/*}
*/

end_form();
div_end();
end_page();



?>


<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}


</style>


<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(e) {
    $("#pro_table").hide();
	$("#cons_table").show();
});
$(document).on('change','#slc_stock_master',function(){
		var stock_type = $("#slc_stock_master").val();
		if(stock_type=='101'){
			//alert(stock_type);
		$("#pro_table").hide();
		$("#cons_table").show();
		
		}else{
			//alert(stock_type);
		$("#pro_table").show();
		$("#cons_table").hide();
		}
		
		$('#slc_master option:eq(0)').attr('selected','selected');
		$('#sub_master option:eq(0)').attr('selected','selected');
		$('#slc_category option:eq(0)').attr('selected','selected');
		$('#collection_id option:eq(0)').attr('selected','selected');
		$('#range_id_code option:eq(0)').attr('selected','selected');
		$('#sub_design option:eq(0)').attr('selected','selected');
		$('#packaging_finish_code option:eq(0)').attr('selected','selected');
		
	});



	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_id_code").hide();
			}else{
				$("#range_id_code").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
	
	// - - ------     location change  ---change work center
	
	
	$(document).on('change','#slc_work_center',function(){
		//alert("data is synchronization successfully .... ")
		var slc_work_center = $(this).val();
		$.ajax({
			url: "slc_center_calling.php",
			method: "POST",
            data: { id : slc_work_center},
			success: function(data){
					var select_val = $('#slc_production');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
/*	$(document).on('change','#slc_location',function(){
		var slc_location = $('#slc_location').val();
		if(slc_location != '-1'){
			$.ajax({
				url: "slc_location_calling.php",
				method: "POST",
				data: { id : slc_location },
				success: function(data){
						$( "option" ).remove( ".work_extra" );
						$('#slc_work_center').append(data);
					}
			});
		}else{
			$( "option" ).remove( ".work_extra" );
		}
		
	});*/
	
	$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
	
	/*$(document).ready(function(e) {
		
		
		var r_id = '';
		$.ajax({
			url: "item_adjustment_reference_no.php",
			method: "POST",
            data: { r_id : r_id},
			success: function(data){
				//alert(data);
					 $('#product_name').val(data);
					 //reference_id
				}
		});
		return false;
	});*/
$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});

	
$(document).on('click','#AddProcess', function() {
		//alert("Stock has been registered...!!");
		return true;
	});	
/*window.onbeforeunload = function() {
    return "Leaving this page will reset the wizard";
};*/
</script>