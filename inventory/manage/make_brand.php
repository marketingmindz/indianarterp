<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_COMPANY_MASTER';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Company Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/make_brand_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------
simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Brand Name cannot be empty."));
		set_focus('name');
	}


	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_range = check_brand($_POST['name']);
		    if($check_range)
             {
	             display_notification(_('Brand already exist.'));	 
	          }else{ 
			    update_brand($selected_id, $_POST['name']);
			    display_notification(_('Selected Brand has been updated'));
			  }
    	} 
    	else 
    	{
			$check_range = check_brand($_POST['name']);
		    if($check_range)
             { 
	             display_notification(_('Brand already exist.'));	 
	         }else{ 
    		     add_brand($_POST['name']);
			     display_notification(_('New Brand been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_brand($selected_id);
		display_notification(_('Selected Brand has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_brand(check_value('show_inactive'));

start_form();
start_table(TABLESTYLE, "width=30%");

$th = array(_('Company Name'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["name"]);
	inactive_control_cell($myrow["brand_id"], $myrow["inactive"], 'make_brand', 'brand_id');
 	edit_button_cell("Edit".$myrow['brand_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['brand_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2, "style=width:30%;min-width:320px;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		
		$myrow = get_brand($selected_id);

		$_POST['name']  = $myrow["name"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Company Name:"), 'name', null, 40, 40);

end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
