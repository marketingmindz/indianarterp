<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FINISH_DETAILS';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/finish_product_db.inc");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Finish Product List"), false, false, "", $js);
}


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} 
	$Ajax->activate('orders_tbl');
}




	

if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

if(isset($_REQUEST['id']))
	{
		$code_id = trim($_REQUEST['id']);
		$get_id = get_finish_code_id($code_id);
		$get_design_id = $get_id["finish_pro_id"];
		finish_product_deleted($code_id);
		header('Location: '.$path_to_root.'/inventory/manage/finish_product_list.php?'); 
		exit(1);  
	}
//-----------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------
?>

<!--      code by Bajrang L. Bidasara   ---->
  
<div style="float:right; width:300px; height:auto; width:auto; background:#96C; font-size:15px !important; border:1px solid #000; cursor:pointer; margin:10px 20px;" id="product_Sync">Sync Product From ERP to CRM</div>
<div id="loader_div" style="position:fixed; margin:auto; color:#000; margin-left:200px; width:300px; height:100px; overflow:hidden !important; z-index:999999; opacity:0.80; text-align:center; display:none; background:#666; border:3px solid #000;">
<h1 style="color:#000; font-size:18px; font-weight:bold; padding-top:30px;  ">Please Wait....</h1>
</div>
<div id="sync_result" style="position:absolute; margin:auto; color:#000; margin-left:200px; width:300px; height:100px; overflow:hidden !important; z-index:999999; opacity:0.80; text-align:center; display:none; background:#666; border:3px solid #000;"">
<div id="sync_result_div" style=" margin-top:10px; color:#000;height:60px;text-align:center;color:#FFF;; font-size:18px; font-weight:bold; "></div>

<span id="close_btn" style="display:block; width:30px; cursor:pointer; height:20px; margin-left:120px; color:#000; font-size:18px; font-weight:bold; background-color:#CCC;  ">OK</span></div>


<div><a href="export_to_excel.php">Export To EXCEl</a></div>
<?php

if (!@$_GET['popup'])
	start_form();

	start_table(TABLESTYLE_NOBORDER);
	start_row();
	ref_cells(_("Finish Code / Product Name:"), 'order_number', '',null, '', true);
	submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
	end_row();
	end_table();
	
	start_table(TABLESTYLE_NOBORDER);
	start_row();
	
	end_row();
	end_table(1);
	//---------------------------------------------------------------------------------------------
	/*if (isset($_POST['order_number']))
	{
		$order_number = $_POST['order_number'];	
	}*/
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];
}




function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
	$finish_code = $row['finish_comp_code'];
	$myrow = get_finish_code_row($finish_code);
	$finish_id = $myrow['finish_pro_id'];
	unset($_SESSION['fab_part']);
	unset($_SESSION['fcon_part']);
  	return pager_link( _("Edit"),
		"/inventory/manage/finish_product.php?FinishId=".$finish_id  , ICON_EDIT);
		
}

function delete_link($row)
{
	if (@$_GET['popup'])
		return '';
	$finish_code = $row['finish_comp_code'];
	$myrow = get_finish_code_row($finish_code);
	$finish_id = $myrow['finish_pro_id'];
	return pager_link(_("Delete"),
			"/inventory/manage/finish_product_list.php?id=".$finish_id, ICON_DELETE);
}
/*function prt_link($row)
{
	return print_document_link($row['order_no'], _("Print"), true, 18, ICON_PRINT);
}
*/
//---------------------------------------------------------------------------------------------

$sql = get_finish_search($all_items);

$cols = array(
		_("Finish Code")=> array('ord'=>asc),
		_("Product Name"),
		_("Range Name"),
		_("Category Name"), 
		_("Width"),
		_("Depth"), 
		_("Height"),
		_("Description"),//shubham code
		array('insert'=>true, 'fun'=>'edit_link'),
		array('insert'=>true, 'fun'=>'delete_link'),
);


//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}	
?>


<script src="../../js/jquery/jquery.js"></script>
<script>
$("#close_btn").click(function(){
	$("#sync_result").hide();
	});
$("#product_Sync").click(function()
{
	$.ajax({
		  type:"GET",
		  url:"product_syncing.php",
		  dataType:"json",
		  data:"sync"&&"new_product"&&"New_Cat",
		  beforeSend: function () {
                    $("#loader_div").show();
                },
		  success:function(data){
			  $("#loader_div").hide();
			  $("#sync_result").show();
			  if(data.New_Cat != "" && data.New_Cat != "NULL")
			  {
				$("#sync_result_div").html(data.sync+" Products have been synced.<br>"+data.new_product+ " New Products Found.<br>"+data.New_Cat+ " New Product categories found.");  
			  }
			  else
			  {
		  $("#sync_result_div").html(data.sync+" Products have been synced.<br>"+data.new_product+ " New Products Found.");
			  }
		
	  }
  });
});

</script>