<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_LABEL';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Label Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/label_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['label_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Label name cannot be empty."));
		set_focus('label_name');
	}
	if (strlen($_POST['label_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Label Reference cannot be empty."));
		set_focus('label_reference');
	}

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_update_label = check_update_label($_POST['label_reference']);
		    if($check_update_label)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
			    update_label_master($selected_id, $_POST['label_name'], $_POST['label_description'],$_POST['label_reference']);
			    display_notification(_('Selected label has been updated'));
			  }
    	} 
    	else 
    	{
			$check_label = check_label($_POST['label_reference']);
		    if($check_label)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
    		     add_label_master($_POST['label_name'], $_POST['label_description'],$_POST['label_reference']);
			     display_notification(_('New label has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_label_master($selected_id);
		display_notification(_('Selected Label has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_label_master(check_value('show_inactive'));

start_form();

start_table(TABLESTYLE2, "style=width:50%;min-width:320px;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code
 		$edit_mod = "readonly";
		$myrow = get_label_master($selected_id);

		$_POST['label_name']  = $myrow["label_name"];
		$_POST['label_description']  = $myrow["label_description"];
		$_POST['label_reference']  = $myrow["label_reference"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Label Name:"), 'label_name', null, 40, 40);
textarea_row(_('Description:'), 'label_description', null, 50, 5);
text_row(_("Reference:"), 'label_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec" '.$edit_mod.'');
//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');


echo "<br>";
start_table(TABLESTYLE, "style=width:50%;min-width:320px;");

$th = array(_('Label Name'),_('Description'),_('Reference'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["label_name"]);
	label_cell($myrow["label_description"]);
	label_cell($myrow["label_reference"]);
	inactive_control_cell($myrow["label_id"], $myrow["inactive"], 'label_master', 'label_id');
 	edit_button_cell("Edit".$myrow['label_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['label_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
 <script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}
   
</script>