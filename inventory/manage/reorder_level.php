<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_REORDER_LEVEL';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/db/reorder_level_db.inc");
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Consumable Reorder Level"), false, false, "", $js);
simple_page_mode();

if ($Mode=='ADD_ITEM') 
{
	$input_error = 0;
	if ($_POST['master_id'] == -1) 
	{
		$input_error = 1;
		display_error(_("Select Consumable"));
		set_focus('master_id');
	}
	if (strlen($_POST['sub_category']) == 0) 
	{
		$input_error = 1;
		display_error(_("Select consumable category."));
		set_focus('sub_category');	
	}

	if ($input_error != 1) 
	{
		 add_reorder_level($_POST['master_id'], $_POST['sub_category'], $_POST['level']);
		 display_notification(_('New re-order has been set.'));		 
    }
		$Mode = 'RESET';
} 
if ($Mode=='UPDATE_ITEM') 
{
	$input_error = 0;
	if ($_POST['master_id'] == -1) 
	{
		$input_error = 1;
		display_error(_("Select Consumable"));
		set_focus('master_id');
	}
	if (strlen($_POST['sub_category']) == 0) 
	{
		$input_error = 1;
		display_error(_("Select consumable category."));
		set_focus('sub_category');	
	}

	if ($input_error != 1) 
	{
		 update_reorder_level($_POST['level_id'],$_POST['master_id'], $_POST['sub_category'], $_POST['level']);
		 display_notification(_('Re-order level has been updated.'));		 
    }
		$Mode = 'RESET';
}
if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
if(isset($_GET['level_id']))
{
	$row = get_reorder_level($_GET['level_id']);
	$_POST['master_id'] = $row['consumable_id'];
	$_POST['sub_category'] = $row['consumable_category'];
	$_POST['level'] = $row['level'];
}
start_form();
start_table(TABLESTYLE2);
if(isset($_GET['level_id']))
{
hidden('level_id', $_GET['level_id']);
}
stock_master_list_row(_("Consumable Name:"), 'master_id', null, _('----Select---'));
if($_POST['master_id'] == '-1'){
	sub_consumable__master_list_row(_("Consumable Category:"), 'sub_category' ,null, _('----Select---'),false,'id="consumable_cat"');
}else{
	customer_consumable_list_row(_("Consumable Category:"), $_POST['master_id'],'sub_category', null, false, true, true, true, 'id="consumable_cat"');
}
text_row(_("Level:"), 'level', null, 30, 30);
end_table(1);
if(isset($_GET['level_id']))
{
 submit_center_first("UPDATE_ITEM", _("Update Reorder Level"));
}
else
{
	submit_center_first("ADD_ITEM", _("Set Reorder Level"));
}
submit_center_last("RESET", _("Cancel"));
end_form();
end_page();


?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
    </script>
