<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_COLOR';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Color Master"));

include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/inventory/includes/db/color_master.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['color_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Color name cannot be empty."));
		set_focus('color_name');
	}
	if (strlen($_POST['color_reference']) == 0) 
	{
		$input_error = 1;
		display_error(_("The Color Reference cannot be empty."));
		set_focus('color_reference');
	}

	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
      		$check_update_color = check_update_color($_POST['color_reference']);
		    if($check_update_color)
             {
	             display_notification(_('Reference already exist.'));	 
	          }else{ 
	          	$getFile = $_POST['getFile'];
	          	$getFile = implode(',',$getFile);
	          	if($getFile != ''){
	          		$g_file = $getFile.",";
	          	}else{
	          		$g_file ='';
	          	}
	          	$color_photo = "";
		         	for($i = 0; $i < count($_FILES['file']['name']); $i++){
		         		
						//$photo_name = "pic_".$i;
						$color_img = $_FILES['file']['name'][$i];
						
						$color_tmp= $_FILES['file']['tmp_name'][$i];

							if(!$color_photo){
							   $color_photo = $color_img;	
							}else{
								$color_photo .= ",".$color_img;
							}
					
						$c_Image = company_path().'/colorImage/'.$color_img;
						
						
						move_uploaded_file($_FILES['file']['tmp_name'][$i], $c_Image);
					}
					if($color_photo !=''){
						$color_photo = $g_file .$color_photo;
					}else{
						$color_photo = $g_file;
					}
					$color_photo = trim($color_photo, ",");
			    update_color_master($selected_id, $_POST['color_name'], $_POST['color_description'],$_POST['color_reference'],$color_photo);
			    display_notification(_('Selected color has been updated'));
			  }
    	} 
    	else 
    	{
			$check_color = check_color($_POST['color_reference']);
		    if($check_color)
             { 
	             display_notification(_('Reference already exist.'));	 
	         }else{ 
	         	$color_photo = "";
		         	for($i = 0; $i < count($_FILES['file']['name']); $i++){
		         		
						//$photo_name = "pic_".$i;
						$color_img = $_FILES['file']['name'][$i];
						
						$color_tmp= $_FILES['file']['tmp_name'][$i];

							if(!$color_photo){
							   $color_photo = $color_img;	
							}else{
								$color_photo .= ",".$color_img;
							}
					
						$c_Image = company_path().'/colorImage/'.$color_img;
						
						
						move_uploaded_file($_FILES['file']['tmp_name'][$i], $c_Image);
					}
					$color_photo = trim($color_photo, ",");
    		     add_color_master($_POST['color_name'], $_POST['color_description'],$_POST['color_reference'],$color_photo);
			     display_notification(_('New color has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

//-----------------------------------------------------------------------------------

/*function can_delete($selected_id)
{
	if (item_range_used($selected_id))
	{
		display_error(_("Cannot delete this Ranage because items have been created using this Range."));

	}
	
	return true;
}*/


//-----------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_color_master($selected_id);
		display_notification(_('Selected Color has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_color_master(check_value('show_inactive'));

start_form(true);

start_table(TABLESTYLE2, "style=width:50%;min-width:320px;");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code
 		$edit_mod = "readonly";
		$myrow = get_color_master($selected_id);

		$_POST['color_name']  = $myrow["color_name"];
		$_POST['color_description']  = $myrow["color_description"];
		$_POST['color_reference']  = $myrow["color_reference"];
		$col_image  = $myrow["color_image"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Color Name:"), 'color_name', null, 40, 40);
textarea_row(_('Description:'), 'color_description', null, 50, 10);
text_row(_("Reference:"), 'color_reference', null, 40, 40,'','','','onkeypress="return noNumerics();" onkeyup="upper(this)" id="refernec" '.$edit_mod.'');
if($col_image !=''){
		$col_img = explode(',', $col_image);
	
		$k=0;
		echo '<td></td><td>';
		for($j = 1; $j <= count($col_img); $j++){
			$img_convert =  base64_encode();
			echo '<div id="filediv"><div id="abcd'.$j.'" class="abcd"><img id="previewimg'.$j.'" src="../../company/0/colorImage/'.$col_img[$k].'" width="50" height="50"><img onclick="ab('.$j.')" id="img'.$j.'" src="../../company/0/images/del.png" alt="delete"></div><input name="getFile[]" type="hidden" id="file" value="'.$col_img[$k].'"><br></div>';
			$k++;
		}
		$j =$j-1;
		echo '<input type="hidden" value="'.$j.'" id="couImg"><br>
	<input type="button" id="add_more" class="upload" value="Add More Files"/>';
		echo '</td>';

	}else{
	echo '<td></td><td><input type="hidden" value="" id="couImg"><div id="filediv"><input name="file[]" type="file" id="file"/></div><br>
	<input type="button" id="add_more" class="upload" value="Add More Files"/></td>';
	}
//record_status_list_row(_("Range status:"), 'inactive');
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

echo "<br>";
start_table(TABLESTYLE, "style=width:50%;min-width:320px;");

$th = array(_('Color Name'),_('Description'),_('Reference'), "", "");
inactive_control_column($th);
table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["color_name"]);
	label_cell($myrow["color_description"]);
	label_cell($myrow["color_reference"]);
	inactive_control_cell($myrow["color_id"], $myrow["inactive"], 'color_master', 'color_id');
 	edit_button_cell("Edit".$myrow['color_id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['color_id'], _("Delete"));
	end_row();
}
inactive_control_row($th);
end_table(1);

end_form();

//------------------------------------------------------------------------------------

end_page();


?>
 <script type="text/javascript">	
       function noNumerics(evt)
         {
        	 var e = event || evt;
			 var charCode = e.which || e.keyCode;
			 if ((charCode >= 48) && (charCode <= 57))
				return false;
			 return true;
         }
		 function upper(ustr)
		{
			var str = document.getElementById('refernec').value;
			document.getElementById('refernec').value = str.toUpperCase();
		}

		$(document).ready(function() {

			var abc = 0; 
		    $('#add_more').click(function() {
		    	$count_img = $('#couImg').val();
		    	if($count_img != ''){
		    		abc = parseInt($count_img);
		    	}
		    	//alert(abc);
		        $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
		                $("<input/>", {name: 'file[]', type: 'file', id: 'file'}),        
		                $("<br/><br/>")
		                ));
		    });

		//following function will executes on change event of file input to select different file	
		$('body').on('change', '#file', function(){
	            if (this.files && this.files[0]) {
	                abc += 1; //increementing global variable by 1
					$count_img ='';
					$('#couImg').val($count_img);
					var z = abc - 1;
	                var x = $(this).parent().find('#previewimg' + z).remove();
	                $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src='' width='50' height='50'/></div>");
	               
				    var reader = new FileReader();
	                reader.onload = imageIsLoaded;
	                reader.readAsDataURL(this.files[0]);
	               
				    $(this).hide();
	                $("#abcd"+ abc).append($("<img/>", {id: 'img'+ abc, src: '<?php echo $path_to_root; ?>/company/0/images/del.png', alt: 'delete'}));
	               
	                $('#filediv #img'+ abc).click(function(){
	                	$(this).parent().parent().remove();
	                });
	            }
	        });


	//To preview image     
	    function imageIsLoaded(e) {
	        $('#previewimg' + abc).attr('src', e.target.result);
	    };

	    $('#upload').click(function(e) {
	        var name = $(":file").val();
	        if (!name)
	        {
	            alert("First Image Must Be Selected");
	            e.preventDefault();
	        }
	    });
	});
  	function ab(count){
	  $("#img"+count).parent().parent().remove();
	}
   
</script>