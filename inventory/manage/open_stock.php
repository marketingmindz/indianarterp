<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_OPEN_STOCK';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");



include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/inventory/includes/db/open_stock_db.inc");
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Opening Stock Master"), false, false, "", $js);
simple_page_mode();
//----------------------------------------------------------------------------------

//----------------------- end sub category code ---------------------------------------

if ($Mode=='ADD_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if ($_POST['master_id'] == -1) 
	{
		$input_error = 1;
		display_error(_("Select category"));
		set_focus('master_id');
	}
	
	if (strlen($_POST['stock_quantity']) == 0) 
	{
		$input_error = 1;
		display_error(_("The quantity cannot be empty."));
		set_focus('stock_quantity');	
	}

	if ($input_error != 1) 
	{
	
    		     add_open_stock_category($_POST['master_id'], $_POST['sub_category'], $_POST['stock_quantity'], $_POST['date'], 'cr');
			     display_notification(_('New Open stock has been added'));		 
    }
    	
		$Mode = 'RESET';
	
} 

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
start_form();
start_table(TABLESTYLE2, "style=min-width:320px;width:40%;");
stock_master_list_row(_("Category:"), 'master_id', null, _('----Select---'));
sub_consumable__master_list_row(_("Sub Category:"), 'sub_category',null, _('----Select---'));
text_row(_("Available Quantity:"), 'avail_qty', null, 30, 30, '', '', '', 'id="available_qty"');
text_row(_("Quantity:"), 'stock_quantity', null, 30, 30);
date_row(_("Date:") . ":", 'date');
end_table(1);

submit_center_first("ADD_ITEM", _("Add Stock"));
submit_center_last("RESET", _("Cancel"));
end_form();
end_page();


?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	$(document).on('change','#sub_master',function(){
		//alert("data is synchronization successfully .... ")
		var sub_master = $(this).val();
		$.ajax({
			url: "get_qty.php",
			method: "POST",
            data: { ids : sub_master },
			success: function(data){
					 $('#available_qty').val(data);
                    //select_val.empty().append(data);
				}
		});
		return false;
	});
    </script>
<style type="text/css">
	#RESET {
		margin-left: 58px !important;
		top: -15px !important;
		margin-top: -16px !important;
	}
	#ADD_ITEM {
		margin-left: -100px !important;
	}
</script>