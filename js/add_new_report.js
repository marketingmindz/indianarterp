/*Multiselesct Script*/

var expanded = false;
$(document).on("click",'.selectBox', function() {

	var checkboxes = $(this).parent().find("div.checkboxes");
    if (!expanded) {
        checkboxes.show();
        expanded = true;
    } else {
        checkboxes.hide();
        expanded = false;
    }
});

$(document).on('focusout','.selectBox',function(){
		var checkboxes = document.getElementById("checkboxes");
		checkboxes.style.display = "none";
            expanded = false;
		return true;
		
	});
	
$(document).ready(function()
{
    $(".multiselect").mouseup(function(e)
    {
        var subject = $(".multiselect"); 

        if(e.target.id != subject.attr('id') && !subject.has(e.target).length)
        {
            subject.fadeOut();
        }
    });
});

$(document).click(function(e) {

	if($(e.target).attr('class') != 'overSelect')
	{
		if($(e.target).attr('class') != $('.selectBox'))
		{
			if ($(e.target).attr('class') != $('.checkboxes') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = $("div.checkboxes");
				checkboxes.hide();
				$('.checkboxes').fadeOut('slow');
					expanded = false; 
			}
		}
		}
	}
	
});


$(document).on('click','#select_all',function(){
		if(this.checked){
            $('.check').each(function(){
                this.checked = true;
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
            });
        }else{
             $('.check').each(function(){
                this.checked = false;
				
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
				
            });
        }
	});



$('body').on('click','.check', function() {
	
	if($('.check:checked').length == $('.check').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
		
	
	var arr = [];
	$.each($("input[name^='category_id']:checked"), function(){            
    	arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#pro_categories').val(prod.join(", "));
  $('#pro_cat').html(prod.join(", "));
});


$(document).on("click","#GetFinishProduct", function() {
	  window.onbeforeunload = null;
	  }); 


  window.onbeforeunload = function(e) {
	  if($(e.target).attr('class') != 'finish_code' && $(e.target).attr('class') != 'ajaxsubmit' && $(e.target).attr('class') != 'man_finish_code' && $(e.target).attr('class') != 'ajaxtabs')
	  {
		  return 'You have not saved this costing list.';
	  }
	  //  
	 
  };


  /*end of multiselect script*/