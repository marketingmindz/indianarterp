<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/sales/includes/cart_class.inc");

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");

include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);

if ($_GET['trans_type'] == ST_SALESQUOTE)
{
	page(_($help_context = "View Sales Quotation"), true, false, "", $js);
	display_heading(sprintf(_("Sales Quotation #%d"),$_GET['trans_no']));
}	
else
{
	page(_($help_context = "View Sales Order"), true, false, "", $js);
	display_heading(sprintf(_("Sales Order #%d"),$_GET['trans_no']));
}

if (isset($_SESSION['View']))
{
	unset ($_SESSION['View']);
}

$_SESSION['View'] = new Cart($_GET['trans_type'], $_GET['trans_no']);

start_table(TABLESTYLE2, "width=95%", 5);

if ($_GET['trans_type'] != ST_SALESQUOTE)
{
	echo "<tr valign=top><td>";
	display_heading2(_("Order Information"));
	echo "</td><td>";
	display_heading2(_("Deliveries"));
	echo "</td><td>";
	display_heading2(_("Invoices/Credits"));
	echo "</td></tr>";
}	

echo "<tr valign=top><td>";

start_table(TABLESTYLE, "width=95%");
start_row();
label_cells(_("Customer Name"), $_SESSION['View']->customer_name, "class='tableheader2'",
	"colspan=1");
label_cells(_("Reference"), $_SESSION['View']->reference, "class='tableheader2'", "colspan=3");
end_row();
start_row();
label_cells(_("Ordered On"), $_SESSION['View']->document_date, "class='tableheader2'");
if ($_GET['trans_type'] == ST_SALESQUOTE)
	label_cells(_("Valid until"), $_SESSION['View']->due_date, "class='tableheader2'");
else
	label_cells(_("Requested Delivery"), $_SESSION['View']->due_date, "class='tableheader2'");

end_row();

start_row();
	label_cells(_("Delivery Address"), nl2br($_SESSION['View']->delivery_address),
		"class='tableheader2'");
	label_cells(_("Consignee Information"), nl2br($_SESSION['View']->consignee),
		"class='tableheader2'");
end_row();
start_row();
	label_cells(_("Telephone"), $_SESSION['View']->phone, "class='tableheader2'");
	label_cells(_("Shipment Information"), nl2br($_SESSION['View']->shipment),
			"class='tableheader2'");
end_row();
label_row(_("E-mail"), "<a href='mailto:" . $_SESSION['View']->email . "'>" . $_SESSION['View']->email . "</a>",
	"class='tableheader2'", "colspan=3");

end_table();

if ($_GET['trans_type'] != ST_SALESQUOTE)
{
	echo "</td><td valign='top'>";

	start_table(TABLESTYLE);
	display_heading2(_("Delivery Notes"));


	$th = array(_("#"), _("Ref"), _("Date"), _("Total"));
	table_header($th);

	$dn_numbers = array();
	$delivery_total = 0;

	if ($result = get_sales_child_documents(ST_SALESORDER, $_GET['trans_no'])) {

		$k = 0;
		while ($del_row = db_fetch($result))
		{

			alt_table_row_color($k);
			$dn_numbers[] = $del_row["trans_no"];
			$this_total = $del_row["ov_freight"]+ $del_row["ov_amount"] + $del_row["ov_freight_tax"]  + $del_row["ov_gst"] ;
			$delivery_total += $this_total;

			label_cell(get_customer_trans_view_str($del_row["type"], $del_row["trans_no"]));
			label_cell($del_row["reference"]);
			label_cell(sql2date($del_row["tran_date"]));
			amount_cell($this_total);
			end_row();
		}
	}

	label_row(null, price_format($delivery_total), " ", "colspan=4 align=right");

	end_table();
	echo "</td><td valign='top'>";

	start_table(TABLESTYLE);
	display_heading2(_("Sales Invoices"));

	$th = array(_("#"), _("Ref"), _("Date"), _("Total"));
	table_header($th);
	
	$inv_numbers = array();
	$invoices_total = 0;

	if ($result = get_sales_child_documents(ST_CUSTDELIVERY, $dn_numbers)) {

		$k = 0;

		while ($inv_row = db_fetch($result))
		{
			alt_table_row_color($k);

			$this_total = $inv_row["ov_freight"] + $inv_row["ov_freight_tax"]  + $inv_row["ov_gst"] + $inv_row["ov_amount"];
			$invoices_total += $this_total;

			$inv_numbers[] = $inv_row["trans_no"];
			label_cell(get_customer_trans_view_str($inv_row["type"], $inv_row["trans_no"]));
			label_cell($inv_row["reference"]);
			label_cell(sql2date($inv_row["tran_date"]));
			amount_cell($this_total);
			end_row();
		}
	}
	label_row(null, price_format($invoices_total), " ", "colspan=4 align=right");

	end_table();

	display_heading2(_("Credit Notes"));

	start_table(TABLESTYLE);
	$th = array(_("#"), _("Ref"), _("Date"), _("Total"));
	table_header($th);
	
	$credits_total = 0;
	
	if ($result = get_sales_child_documents(ST_SALESINVOICE, $inv_numbers)) {
		$k = 0;

		while ($credits_row = db_fetch($result))
		{

			alt_table_row_color($k);

			$this_total = $credits_row["ov_freight"] + $credits_row["ov_freight_tax"]  + $credits_row["ov_gst"] + $credits_row["ov_amount"];
			$credits_total += $this_total;

			label_cell(get_customer_trans_view_str($credits_row["type"], $credits_row["trans_no"]));
			label_cell($credits_row["reference"]);
			label_cell(sql2date($credits_row["tran_date"]));
			amount_cell(-$this_total);
			end_row();

		}

	}
	label_row(null, "<font color=red>" . price_format(-$credits_total) . "</font>",
		" ", "colspan=4 align=right");

	end_table();

	echo "</td></tr>";

	end_table();
}
echo "<center>";
if ($_SESSION['View']->so_type == 1)
	display_note(_("This Sales Order is used as a Template."), 0, 0, "class='currentfg'");
display_heading2(_("Line Details"));

start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_("S. No."), _("Product Image"), _("Finish Code"),  _("Product Name"), _("Category Name"), _("Range Name"), _("Design Code"),_("Size"),_("Box Size"),_("Box CBM"),_("QTY 40` HQ"),_("QTY 40`"),_("QTY 20`"),_("MOQ"),_("Price"));
		table_header($th);
		$i = 1; 
		foreach ($_SESSION['View']->pro as $pro) {
			start_row();
				label_cell($i);
				$pro['product_image'] = trim($pro['product_image'], ',');
				$images = explode(",", $pro['product_image']);
				
				echo "<td>";
				foreach ($images as $img) {
					echo " <img width='60px' src='".$path_to_root."../../../company/0/finishProductImage/".$img."' />";
				}
				echo "</td>";
				label_cell($pro['finish_comp_code']);
				label_cell($pro['finish_product_name']);
				label_cell($pro['cat_name']);
				label_cell($pro['range_name']);
				label_cell($pro['design_code']);
				
				label_cell($pro['asb_weight']."*".$pro['asb_density']."*".$pro['asb_height']);
				label_cell($pro['pkg_w']."*".$pro['pkg_d']."*".$pro['pkg_h']);
				label_cell(round($pro['pro_cbm'], 2));
				label_cell(round(65/$pro['pro_cbm'], 2));
				label_cell(round(52/$pro['pro_cbm'], 2));
				label_cell(round(26/$pro['pro_cbm'], 2));
				label_cell($pro['quantity']);
				label_cell($pro['unit_price']);
				
			end_row();
			$i++;
		}
		
		end_table();

end_page(true, false, false, $_GET['trans_type'], $_GET['trans_no']);

?>
