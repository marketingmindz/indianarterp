<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//-----------------------------------------------------------------------------
//
//	Entry/Modify Sales Quotations
//	Entry/Modify Sales Order
//	Entry Direct Delivery
//	Entry Direct Invoice
//
$path_to_root = "..";
$page_security = 'SA_SALESORDER';

include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/ui/sales_order_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_types_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");

include_once($path_to_root . "/sales/includes/ui/bpc_ui.inc");
include_once($path_to_root . "/sales/includes/db/bpc_db.inc");


set_page_security( @$_SESSION['Items']->trans_type,
	array(	ST_SALESORDER=>'SA_SALESORDER',
			ST_SALESQUOTE => 'SA_SALESQUOTE',
			ST_CUSTDELIVERY => 'SA_SALESDELIVERY',
			ST_SALESINVOICE => 'SA_SALESINVOICE'),
	array(	'NewOrder' => 'SA_SALESORDER',
			'ModifyOrderNumber' => 'SA_SALESORDER',
			'AddedID' => 'SA_SALESORDER',
			'UpdatedID' => 'SA_SALESORDER',
			'NewQuotation' => 'SA_SALESQUOTE',
			'ModifyQuotationNumber' => 'SA_SALESQUOTE',
			'NewQuoteToSalesOrder' => 'SA_SALESQUOTE',
			'AddedQU' => 'SA_SALESQUOTE',
			'UpdatedQU' => 'SA_SALESQUOTE',
			'NewDelivery' => 'SA_SALESDELIVERY',
			'AddedDN' => 'SA_SALESDELIVERY', 
			'NewInvoice' => 'SA_SALESINVOICE',
			'AddedDI' => 'SA_SALESINVOICE'
			)
);

?>

<style type="text/css">
	
	.multiselect {  width: 200px;    }
    .selectBox {  position: relative; }
    .selectBox select { width: 100%; font-weight: bold; }
    .overSelect { position: absolute; left: 0; right: 0; top: 0; bottom: 0; }
    #checkboxes { display: none; width: 200px; border: 1px #dadada solid; position: absolute;  z-index: 999999999;  background-color: #fff; max-height:300px; overflow:scroll;	overflow-x:hidden;	overflow-wrap: break-word;   }
    #checkboxes label { display: block;   }
    #checkboxes label:hover { background-color: #1e90ff; }
    #checkboxes1 {display: none; width: 200px; border: 1px #dadada solid; position: absolute;  z-index: 999999999;  background-color: #fff;	max-height:300px;	overflow:scroll;	overflow-x:hidden;	overflow-wrap: break-word;    }
    #checkboxes1 label {  display: block;    }
    #checkboxes1 label:hover { background-color: #1e90ff;  }

    #popup_div {      padding: 20px;   top: 350px;    left: 33%;    position:absolute; max-width:80%;     min-width:400px;    min-height:150px;     margin:auto;
background:#FFF;     border:3px solid #000;     border-radius:5px; }

</style>



<?php
$js = '';

if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}
page($_SESSION['page_title'], false, false, "", $js);

if (isset($_GET['ModifyOrderNumber']) && is_numeric($_GET['ModifyOrderNumber'])) 
{
	check_bpc_created($_GET['ModifyOrderNumber'], ST_SALESORDER);
	$help_context = 'Modifying Sales Order';
	$_SESSION['page_title'] = sprintf( _("Create BPCs for Sales Order # %d"), $_GET['ModifyOrderNumber']);
	create_cart(ST_SALESORDER, $_GET['ModifyOrderNumber']);

} 


if (isset($_GET['CreatedBPC'])) {
	$order_no = $_GET['CreatedBPC'];
	display_notification_centered(sprintf( _("BPCs has been created for Order # %d ."),$order_no));

	submenu_option(_("Go to BPCs Inquiry"), "/sales/inquiry/bpc_inquiry.php");

	display_footer_exit();

} else
	check_edit_conflicts();



/*-----BPC Form Validation ---------------*/
function checkBPCForm()
{
	if (!get_post('no_of_bpc') && get_post('creationType') == "auto") 
	{
		display_error(_("Please enter No. of BPCs."));
		set_focus('no_of_bpc');
		return false;
	}
}

//--------------------------------------------------------------------------------
function can_process() 
{
	global $Refs, $SysPrefs;

	//copy_to_cart();

	if (!get_post('customer_id')) 
	{
		display_error(_("There is no customer selected."));
		set_focus('customer_id');
		return false;
	} 
	
	return true;
}


if (isset($_POST['SaveBPC']) && can_process()) 
{
	$trans_no = key($_SESSION['Items']->trans_no);
	$trans_type = $_SESSION['Items']->trans_type;
	save_bpcs($trans_no);
	processing_end();
	meta_forward($_SERVER['PHP_SELF'], "CreatedBPC=$trans_no");	
}


function showItemsCSS(){
	echo "<style type='text/css'>";
	echo ".itemRows { display : none; }";
	echo "</style>";
}

function copy_from_cart()
{
	$cart = &$_SESSION['Items'];
	$_POST['ref'] = $cart->reference;
	$_POST['Comments'] = $cart->Comments;

	$_POST['OrderDate'] = $cart->document_date;
	$_POST['delivery_date'] = $cart->due_date;
	$_POST['cust_ref'] = $cart->cust_ref;

	$_POST['deliver_to'] = $cart->deliver_to;
	$_POST['delivery_address'] = $cart->delivery_address;
	$_POST['phone'] = $cart->phone;
	$_POST['Location'] = $cart->Location;
	$_POST['ship_via'] = $cart->ship_via;

	$_POST['customer_id'] = $cart->customer_id;

	$_POST['branch_id'] = $cart->Branch;
	$_POST['sales_type'] = $cart->sales_type;
	// POS 
	$_POST['payment'] = $cart->payment;
	
	$_POST['cart_id'] = $cart->cart_id;
	$_POST['_ex_rate'] = $cart->ex_rate;
}

function create_cart($type, $trans_no)
{ 
	global $Refs;

	if (!$_SESSION['SysPrefs']->db_ok) // create_cart is called before page() where the check is done
		return;

	processing_start();

	$_SESSION['Items'] = new Cart($type, array($trans_no));
	copy_from_cart();
}

//--------------------------------------------------------------------------------
check_db_has_stock_items(_("There are no inventory items defined in the system."));

check_db_has_customer_branches(_("There are no customers, or there are no customers with branches. Please define customers and customer branches."));


$idate = _("Order Date:");
$orderitems = _("Sales Order Items");
$deliverydetails = _("Enter Delivery Details and Confirm Order");
$cancelorder = _("Cancel BPCs");
$porder = _("Place Order");
$corder = _("Save BPCs");



start_form();

hidden('cart_id');
$customer_error = display_order_header($_SESSION['Items'], false, $idate);

div_start("OrderItems");
if($_POST['itemHidden'] == "1"){
	showItemsCSS();
}
start_outer_table(TABLESTYLE1);
	table_section(1);

	$hideButton = $_POST['itemHidden'] == 0 ?  "Hide Sales Order Items" : "Show Sales Order Items";
	echo "<tr><td colspan=2 class='tableheader' style='text-align:left; border-right:0px;'>Sales Order Items</td>";
	echo "<td colspan=5 class='tableheader' style='border-left:0px;'><a href='javascript:void(0)' id='hideItems' >$hideButton</a></td></tr>";
	
	$itemHidden = isset($_POST['itemHidden']) ? $_POST['itemHidden'] : 0;
	hidden("itemHidden", $itemHidden);
	display_OrderItems($_SESSION['Items']);
	

end_outer_table(1);
div_end();

if ($customer_error == "") 
{
	div_start("bpc_form");
		bpc_form();
	div_end();
	br();

	if(isset($_POST['createBPC']))
	{
		checkBPCForm();
		$Ajax->activate('bpcs');
	}

/*	if(isset($_POST['no_of_bpc']) && !empty($_POST['no_of_bpc']))
	{
		if(!isset($_POST['no_of_bpc']) OR !check_num('no_of_bpc', 1))
		{
			display_error("Please enter valid number of BPCs.");
		}*/
		display_bpcs();
	/*}*/

	
	echo "<br>";
	submit_center_first('SaveBPC', $corder,
	    _('Validate changes and update document'), 'default');
	submit_center_last('CancelBPC', $cancelorder,
   		_('Cancels document entry or removes sales order when editing an old document'), true);
	if ($_SESSION['Items']->trans_type==ST_SALESORDER)
		submit_js_confirm('CancelOrder', _('You are about to cancel undelivered part of this order.\nDo you want to continue?'));
	else
		submit_js_confirm('CancelOrder', _('You are about to void this Document.\nDo you want to continue?'));


} else {
	display_error($customer_error);
}

end_form();
end_page();
?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="sale_order.js"></script>
<script type="text/javascript">

function update_total_bpc(bpc)
{
	var total_bpc_cbm = 0;
	$("input[name^=pro_cbm_bpc_"+bpc+"]").each(
		function(){
			total_bpc_cbm += parseFloat($(this).val());
		});

	$("input[name=total_cbm_bpc_"+bpc+"]").val((total_bpc_cbm).toFixed(4));
}

function update_from_bpc(from, to, bpc_mode)
{	
	var i = 1;
	$("input[name^=pro_id_bpc_"+from+"]").each(function()	{
			$(this).attr("name", "pro_id_bpc_"+from+"_"+i);
			i++;
		});
	var i = 1;
	$("input[name^=pro_name_bpc_"+from+"]").each(function()	{
			$(this).attr("name", "pro_name_bpc_"+from+"_"+i);
			i++;
		});
	var i = 1;
	$("input[name^=pro_code_bpc_"+from+"]").each(function()	{
			$(this).attr("name", "pro_code_bpc_"+from+"_"+i);
			i++;
		});
	var i = 1;
	$("input[name^=pro_qty_bpc_"+from+"]").each(function()	{
			$(this).attr("name", "pro_qty_bpc_"+from+"_"+i);
			i++;
		});

	if(bpc_mode == "cbm_mode")
	{
		var i = 1;
		$("input[name^=cbm_per_pro_bpc_"+from+"]").each(function()	{
				$(this).attr("name", "cbm_per_pro_bpc_"+from+"_"+i);
				i++;
			});
		var i = 1;
		$("input[name^=pro_cbm_bpc_"+from+"]").each(function()	{
				$(this).attr("name", "pro_cbm_bpc_"+from+"_"+i);
				i++;
			});		
	}
}

	function update_bpc(self, exchangeQty, from, to, to_bpc_pro, from_bpc_pro, bpc_mode) {

		var n = self.attr("name").split("_");

		var prod_val = parseInt($("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val());
        if(prod_val < parseInt(exchangeQty))
		{
			alert("Enter Less no. of Quantity.");
			focus($("#exchange_qty"));
			return false;
		}
		else
		{
			var newVal = parseInt(prod_val) - parseInt(exchangeQty);
			$("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val(newVal);

			if(bpc_mode == "cbm_mode")
			{
				var cbm_per_bpc = parseFloat($("input[name=cbm_per_pro_bpc_"+n[3]+"_"+n[4]+"]").val());
				$("input[name=pro_cbm_bpc_"+n[3]+"_"+n[4]+"]").val((newVal*cbm_per_bpc).toFixed(4));
			}

			if(newVal == 0)
			{
				$("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").closest("tr").remove();
				$("input[name=bpc_pro_"+from+"]").val(from_bpc_pro-1);
				$("#CancelExchange").click();
			}
		}		
	}

function clone_bpc(self, exchangeQty, from, to, to_bpc_pro, from_bpc_pro, bpc_mode){
	
	var n = self.attr("name").split("_");

	var prod_val = parseInt($("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val());
    if(prod_val < parseInt(exchangeQty))
	{
		alert("Enter Less no. of Quantity.");
		focus($("#exchange_qty"));
		return false;
	}

	var newVal = parseInt(prod_val) - parseInt(exchangeQty);
	/*remove if quanatity left is 0*/
	if(newVal == 0)
	{
		$("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").closest("tr").remove();
		$("input[name=bpc_pro_"+from+"]").val(from_bpc_pro-1);
		$("#CancelExchange").click();
		update_from_bpc(from);
	}
	else
	{
		$("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val(newVal);

		var cbm_per_bpc = parseFloat($("input[name=cbm_per_pro_bpc_"+n[3]+"_"+n[4]+"]").val());
		$("input[name=pro_cbm_bpc_"+n[3]+"_"+n[4]+"]").val((newVal*cbm_per_bpc).toFixed(4));
	}

	var rowHtml = self.parent("tr").clone();

	if(bpc_mode == "cbm_mode")
	{
		$("#bpc"+to+" tr:last").before(rowHtml);
	}
	else
	{
		$("#bpc"+to+" tr:last").after(rowHtml);
	}
	

	$("#bpc"+to+" input[name^=pro_id_bpc_"+from+"]").each(function(){$(this).attr("name","pro_id_bpc_"+to+"_"+(to_bpc_pro+1))});

	$("#bpc"+to+" input[name^=pro_name_bpc_"+from+"]").each(function(){$(this).attr("name","pro_name_bpc_"+to+"_"+(to_bpc_pro+1))});

	$("#bpc"+to+" input[name^=pro_code_bpc_"+from+"]").each(function(){$(this).attr("name","pro_code_bpc_"+to+"_"+(to_bpc_pro+1))});

	$("#bpc"+to+" input[name^=pro_qty_bpc_"+from+"]").each(function(){$(this).attr("name","pro_qty_bpc_"+to+"_"+(to_bpc_pro+1))});

	$("input[name=pro_qty_bpc_"+to+"_"+(to_bpc_pro+1)+"]").val(exchangeQty);

	if(bpc_mode == "cbm_mode")
	{
		$("#bpc"+to+" input[name^=cbm_per_pro_bpc_"+from+"]").each(function(){$(this).attr("name","cbm_per_pro_bpc_"+to+"_"+(to_bpc_pro+1))});

		$("#bpc"+to+" input[name^=pro_cbm_bpc_"+from+"]").each(function(){$(this).attr("name","pro_cbm_bpc_"+to+"_"+(to_bpc_pro+1))});


		var cbm_per_bpc = parseFloat($("input[name=cbm_per_pro_bpc_"+to+"_"+(to_bpc_pro+1)+"]").val());
		$("input[name=pro_cbm_bpc_"+to+"_"+(to_bpc_pro+1)+"]").val((exchangeQty*cbm_per_bpc).toFixed(4));
		
	}

	$("input[name=bpc_pro_"+to+"]").val(to_bpc_pro+1);

	if(newVal == 0)
	{
		update_from_bpc(from, to, bpc_mode);
	}

	$("#CancelExchange").click();
	
}

	
	$.fn.exchangePro = function() {
		
        var exchangeQty = $("#exchange_qty").val();
        var from = $("input[name=exchange_from_bpc]").val();
        var to = $("#exchange_to_bpc").val();
        var exchange_pro_id = $("#exchange_pro_id").val();

        var bpc_mode = $("select[name=bpc_mode]").val();

        var to_bpc_pro = parseInt($("input[name=bpc_pro_"+to+"]").val());
        var from_bpc_pro = parseInt($("input[name=bpc_pro_"+from+"]").val());

        if(parseInt(exchangeQty) <= 0)
        {
        	alert("Product Quantity cannot be less than 0.");
        	focus($("#exchange_qty"));
        	return false;
        }

        if(exchange_pro_id == '-1')
        {
        	alert("Please Select Product to Exchange.");
        	focus($("#exchange_pro_id"));
        	return false;
        }
        if(to == '-1')
        {
        	alert("Please Select BPCs to Exchange.");
        	focus($("#exchange_to_bpc"));
        	return false;
        }
        
        if(exchangeQty.length > 0)
        {
        	var productExist = false;
        	
        	$("input[name^=pro_id_bpc_"+to+"]").each(
        		function(){
        			/*if product exists in another bpc*/
        			if($(this).val() == exchange_pro_id)
        			{
        				/*reduce existing quantity*/
        				$("input[name^=pro_id_bpc_"+from+"]").each(function(){
        					if($(this).val() == exchange_pro_id)
							{
								update_bpc($(this), exchangeQty, from, to, to_bpc_pro, from_bpc_pro, bpc_mode);
								productExist = true;
							}
        				});

        				/*Add new quantity*/
        				var n = $(this).attr("name").split("_");
        				var newVal = parseInt($("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val()) + parseInt(exchangeQty);
        				$("input[name=pro_qty_bpc_"+n[3]+"_"+n[4]+"]").val(newVal);

        				if(bpc_mode == "cbm_mode")
						{
							var cbm_per_bpc = parseFloat($("input[name=cbm_per_pro_bpc_"+n[3]+"_"+n[4]+"]").val());
							$("input[name=pro_cbm_bpc_"+n[3]+"_"+n[4]+"]").val((newVal*cbm_per_bpc).toFixed(4));
						}

        				$("#CancelExchange").click();
        			}
        		});

				if(bpc_mode == "cbm_mode" && productExist == true)
				{	
					update_total_bpc(from);
					update_total_bpc(to);
				}	


			/*if product does not exist in another bpc*/
        	if(productExist === false)
        	{ 
				/*reduce existing quantity*/
				$("input[name^=pro_id_bpc_"+from+"]").each(function(){
					if($(this).val() == exchange_pro_id)
					{
	        			clone_bpc($(this), exchangeQty, from, to, to_bpc_pro, from_bpc_pro, bpc_mode);
	        		}

	        		if(bpc_mode == "cbm_mode")
					{	
						update_total_bpc(from);
						update_total_bpc(to);
					}
	        	});
        	}	
        }
        else
        {
        	alert("Please Enter Product Quantity to Exchange.");
        	focus($("#exchange_qty"));
        	return false;
        }
        return false;
    };
$(document).on("click", "#hideItems", function(){
	$('.itemRows').toggle();
	var text = $(this).html() == "Hide Sales Order Items" ? "Show Sales Order Items":"Hide Sales Order Items";
	var newVal = $("input[name=itemHidden]").val() == "0" ? 1 : 0;
	$("input[name=itemHidden]").val(newVal);
	$(this).html(text);
});

</script>