<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//-----------------get product details -------------

function get_order_crm_id()
{
	$sql = "select crm_id from ".TB_PREF."sales_orders";
	$result = db_query($sql, "could not get Sales Order CRM Id");
	$crm_id = Array();

	while($row = db_fetch($result))
	{
		$crm_id[] = $row['crm_id'];
	}
	return $crm_id;
}

function get_catgory_details($catId)
{
	$sql = "select category_id, cat_name from ".TB_PREF."item_category where category_id = ".db_escape($catId);
	$result = db_query($sql,"Could not get products. #123");
	$cat = db_fetch_assoc($result);
	return $cat;
}



function get_products_details($cat, $range_id)
{
	$category = implode(",", $cat);
	$sql = "select f.category_id,f.range_id, f.finish_pro_id, f.finish_comp_code, d.design_code, c.cat_name,
			(CASE WHEN f.range_id = -1 then 'Non-Collection' ELSE i.range_name END) AS range_name from ".TB_PREF."finish_product f 
			LEFT JOIN ".TB_PREF."design_code d ON d.design_id = f.design_id
			LEFT JOIN ".TB_PREF."item_category c ON c.category_id = f.category_id 
			LEFT JOIN ".TB_PREF."item_range i ON i.id = f.range_id
			where f.category_id in (".$category.") && f.range_id in (".$range_id.") 
			ORDER BY c.cat_name ASC, i.range_name ASC";

	$result = db_query($sql,"Could not get products. #123");
	$products = array();
	while($row = db_fetch_assoc($result))
	{
		$products[] = $row;
	} 
	return $products;
}

function display_final_list($finish_pro_id)
{
	$sql = "select f.category_id, f.product_image, f.finish_pro_id, d.design_code, c.cat_name,
			f.range_id,	(CASE WHEN f.range_id = -1 then 'Non-Collection' ELSE i.range_name END) AS range_name,
			 f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_density, f.asb_height, d.pkg_w, d.pkg_h, d.pkg_d, d.pro_cbm 
			 from ".TB_PREF."finish_product f 
			LEFT JOIN ".TB_PREF."design_code d ON d.design_id = f.design_id
			LEFT JOIN ".TB_PREF."item_category c ON c.category_id = f.category_id 
			LEFT JOIN ".TB_PREF."item_range i ON i.id = f.range_id
			where f.finish_pro_id in (".$finish_pro_id.") ORDER BY f.finish_pro_id ASC";

	$result = db_query($sql,"Could not get products. #123");
	$products = array();
	while($row = db_fetch_assoc($result))
	{
		$products[] = $row;
	} 
	return $products;
}



//----------------------------------------------------------------------------------------
/*function add_sales_order(&$order)
{
	global $loc_notification, $path_to_root, $Refs;

	begin_transaction();
	
	$order_no = get_next_trans_no($order->trans_type);
	$del_date = date2sql($order->due_date);
	$order_type = 0; // this is default on new order
	$total = $order->get_trans_total();
	$sql = "INSERT INTO ".TB_PREF."sales_orders (order_no, type, debtor_no, trans_type, branch_code, customer_ref, reference, comments, ord_date,
		order_type, ship_via, deliver_to, delivery_address, contact_phone,
		freight_cost, from_stk_loc, delivery_date, payment_terms, total)
		VALUES (" .db_escape($order_no) . "," .db_escape($order_type) . "," . db_escape($order->customer_id) .
		 ", " .db_escape($order->trans_type) . "," .db_escape($order->Branch) . ", ".
			db_escape($order->cust_ref) .",". 
			db_escape($order->reference) .",". 
			db_escape($order->Comments) .",'" . 
			date2sql($order->document_date) . "', " .
			db_escape($order->sales_type) . ", " .
			db_escape($order->ship_via)."," . 
			db_escape($order->deliver_to) . "," .
			db_escape($order->delivery_address) . ", " .
			db_escape($order->phone) . ", " . 
			db_escape($order->freight_cost) .", " . 
			db_escape($order->Location) .", " .
			db_escape($del_date) . "," .
			db_escape($order->payment) . "," .
			db_escape($total). ")";

	db_query($sql, "order Cannot be Added");

	$order->trans_no = array($order_no=>0);

	if ($loc_notification == 1)
	{
		include_once($path_to_root . "/inventory/includes/inventory_db.inc");
		$st_ids = array();
		$st_names = array();
		$st_num = array();
		$st_reorder = array();
	}
	foreach ($order->line_items as $line)
	{
		if ($loc_notification == 1 && is_inventory_item($line->stock_id))
			$loc = calculate_reorder_level($order->Location, $line, $st_ids, $st_names, $st_num, $st_reorder); 

		$sql = "INSERT INTO ".TB_PREF."sales_order_details (order_no, trans_type, stk_code, description, unit_price, quantity, discount_percent) VALUES (";
		$sql .= $order_no . ",".$order->trans_type .
				",".db_escape($line->stock_id).", "
				.db_escape($line->item_description).", $line->price,
				$line->quantity,
				$line->discount_percent)";
		db_query($sql, "order Details Cannot be Added");

	// Now mark quotation line as processed
		if ($order->trans_type == ST_SALESORDER && $line->src_id)
			update_parent_line(ST_SALESORDER, $line->src_id, $line->qty_dispatched); // clear all the quote despite all or the part was ordered
	}*/ /* inserted line items into sales order details */
	/*add_audit_trail($order->trans_type, $order_no, $order->document_date);
	$Refs->save($order->trans_type, $order_no, $order->reference);

	hook_db_postwrite($order, $order->trans_type);
	commit_transaction();

	if ($loc_notification == 1 && count($st_ids) > 0)
		send_reorder_email($loc, $st_ids, $st_names, $st_num, $st_reorder);
	return $order_no;
}*/

function add_sales_order(&$order)
{
	global $loc_notification, $path_to_root, $Refs;

	begin_transaction();
	hook_db_prewrite($order, $order->trans_type);
	$del_date = date2sql($order->due_date);
	$order_no = get_next_trans_no($order->trans_type);
	$OrderDate = date2sql($order->document_date);
	if($order->trans_type == ST_SALESQUOTE OR $order->trans_type == ST_SALESORDER)
	{
		$del_date = date2sql($_POST['delivery_date']);
		$OrderDate = date2sql($_POST['OrderDate']); 
	}
	$range_id = implode(",",$_POST['range_id']);
	$category_id = implode(",",$_POST['category_id']);

	$order_type = 0; // this is default on new order
	$total = $order->get_trans_total();
	$sql = "INSERT INTO ".TB_PREF."sales_orders (order_no, type, debtor_no, trans_type, category_id, range_id,  branch_code, customer_ref, reference, comments, ord_date,
		order_type, ship_via, deliver_to, delivery_address, contact_phone, contact_email, consignee, shipment,
		freight_cost, from_stk_loc, delivery_date, payment_terms, total)
		VALUES (" .db_escape($order_no) . "," .
			db_escape($order_type) . "," . 
			db_escape($order->customer_id) . ", " .
			db_escape($order->trans_type) . "," .
			db_escape($category_id) . "," .
			db_escape($range_id) . "," .
			db_escape($order->Branch) . ", ".
			db_escape($order->cust_ref) .",". 
			db_escape($order->reference) .",". 
			db_escape($order->Comments) .",'" . 
			$OrderDate. "', " .
			db_escape($order->sales_type) . ", " .
			db_escape($order->ship_via)."," . 
			db_escape($order->deliver_to) . "," .
			db_escape($order->delivery_address) . ", " .
			db_escape($order->phone) . ", " .
			db_escape($order->email) . ", " . 
			db_escape($_POST['consignee']) . ", " . 
			db_escape($_POST['shipment']) . ", " . 
			db_escape($order->freight_cost) .", " . 
			db_escape($order->Location) .", " .
			db_escape($del_date) . "," .
			db_escape($order->payment) . "," .
			db_escape($total). ")";

	db_query($sql, "order Cannot be Added");

	$order->trans_no = array($order_no=>0);

	$products = $_POST['select'];
	foreach ($products as $pro)
	{	
		$sql = "INSERT INTO ".TB_PREF."sales_order_details (order_no, trans_type, product_id, quantity, unit_price) VALUES (";
		$sql .= $order_no . ",".$order->trans_type .
				",".db_escape($pro).", "
				.db_escape($_POST["moq$pro"]).","
				.db_escape($_POST["price$pro"]).")";
		db_query($sql, "order Details Cannot be Added");

	} /* inserted line items into sales order details */
	add_audit_trail($order->trans_type, $order_no, $order->document_date);
	$Refs->save($order->trans_type, $order_no, $order->reference);
	hook_db_postwrite($order, $order->trans_type);
	commit_transaction();

	
	return $order_no;
}

//----------------------------------------------------------------------------------------

function delete_sales_order($order_no, $trans_type)
{
	begin_transaction();
	hook_db_prevoid($trans_type, $order_no);

	$sql = "DELETE FROM ".TB_PREF."sales_orders WHERE order_no=" . db_escape($order_no) 
		. " AND trans_type=".db_escape($trans_type);

	db_query($sql, "order Header Delete");

	$sql = "DELETE FROM ".TB_PREF."sales_order_details WHERE order_no =" 
		.db_escape($order_no) . " AND trans_type=".db_escape($trans_type);
	db_query($sql, "order Detail Delete");

	delete_reference($trans_type, $order_no);

	add_audit_trail($trans_type, $order_no, Today(), _("Deleted."));
	commit_transaction();
}

//----------------------------------------------------------------------------------------
// Mark changes in sales_order_details
//
function update_sales_order_version($order)
{
  foreach ($order as $so_num => $so_ver) {
  $sql= 'UPDATE '.TB_PREF.'sales_orders SET version=version+1 WHERE order_no='. db_escape($so_num).
	' AND version='.$so_ver . " AND trans_type=30";
  db_query($sql, 'Concurrent editing conflict while sales order update');
  }
}

//----------------------------------------------------------------------------------------

function update_sales_order($order)
{
	global $loc_notification, $path_to_root, $Refs;

	$del_date = date2sql($order->due_date);
	$ord_date = date2sql($order->document_date);
	if($order->trans_type == ST_SALESQUOTE)
	{
		$del_date = date2sql($_POST['delivery_date']);
		$OrderDate = date2sql($_POST['OrderDate']); 
	}
	$range_id = implode(",",$_POST['range_id']);
	$category_id = implode(",",$_POST['category_id']);

	$order_no =  key($order->trans_no);
	$version= current($order->trans_no);
	$total = $order->get_trans_total();

	begin_transaction();
	hook_db_prewrite($order, $order->trans_type);

	$sql = "UPDATE ".TB_PREF."sales_orders SET type =".db_escape($order->so_type)." ,
		debtor_no = " . db_escape($order->customer_id) . ",
		customer_ref = ". db_escape($order->cust_ref) .",
		reference = ". db_escape($order->reference) .",
		ord_date = " . db_escape($ord_date) . ",
		deliver_to = " . db_escape($order->deliver_to) . ",
		delivery_address = " . db_escape($order->delivery_address) . ",
		contact_phone = " .db_escape($order->phone) . ",
		delivery_date = " .db_escape($del_date). ",
		version = ".($version+1).",
		total = ". db_escape($total) ."
	 WHERE order_no=" . db_escape($order_no) ."
	 AND trans_type=".$order->trans_type." AND version=".$version;
	db_query($sql, "order Cannot be Updated, this can be concurrent edition conflict");

/*	$id_tokeep = array();
	foreach ($order->line_items as $line) {
		array_push($id_tokeep , $line->id);
	}
	$id_list = implode(', ', $id_tokeep);
	*/
	$sql = "DELETE FROM ".TB_PREF."sales_order_details WHERE order_no =" . db_escape($order_no) . " AND trans_type=".$order->trans_type;
	/*$sql .= " AND id NOT IN ($id_list)";*/

	db_query($sql, "Old order Cannot be Deleted");

	/*if ($loc_notification == 1)
	{
		include_once($path_to_root . "/inventory/includes/inventory_db.inc");
		$st_ids = array();
		$st_names = array();
		$st_num = array();
		$st_reorder = array();
	}*/

	$products = $_POST['select'];
	foreach ($products as $pro)
	{
		$sql = "INSERT INTO ".TB_PREF."sales_order_details (order_no, trans_type, product_id, quantity, unit_price) VALUES (";
		$sql .= $order_no . ",".$order->trans_type .
				",".db_escape($pro).", "
				.db_escape($_POST["moq$pro"]).","
				.db_escape($_POST["price$pro"]).")";
		db_query($sql, "order Details Cannot be Added");

	} /* inserted line items into sales order details */

	add_audit_trail($order->trans_type, $order_no, $order->document_date, _("Updated."));
	$Refs->save($order->trans_type, $order_no, $order->reference);

	hook_db_postwrite($order, $order->trans_type);
	commit_transaction();
}

//----------------------------------------------------------------------------------------

function get_sales_order_header($order_no, $trans_type)
{
	$sql = "SELECT sorder.*, "
	  ."cust.name, "
	  ."cust.curr_code, "
	  ."cust.address, "
	  ."cust.discount, "
	  ."cust.tax_id "
	."FROM ".TB_PREF."sales_orders sorder LEFT JOIN "
	  .TB_PREF."debtors_master cust on cust.debtor_no = sorder.debtor_no
	WHERE sorder.trans_type = " . db_escape($trans_type) ."
		AND sorder.order_no = " . db_escape($order_no );

	$result = db_query($sql, "order Retreival");
$row = db_fetch($result);

	$num = db_num_rows($result);
	if ($num > 1)
	{
		display_warning("You have duplicate document in database: (type:$trans_type, number:$order_no).");
	}
	else if ($num == 1)
	{ 
		return $row;
	}
	else
		display_warning("You have missing or invalid sales document in database (type:$trans_type, number:$order_no).");

}

//----------------------------------------------------------------------------------------

function get_sales_order_details($order_no, $trans_type) {
	$sql = "select f.category_id, f.product_image, f.finish_pro_id,f.gross_weight, cm.color_name,wm.wood_name,d.design_code, c.cat_name,
			f.range_id,	(CASE WHEN f.range_id = -1 then 'Non-Collection' ELSE i.range_name END) AS range_name,
			 f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_density, f.asb_height, d.pkg_w, d.pkg_h, d.pkg_d, d.pro_cbm, sorder.quantity, sorder.unit_price 
			 from ".TB_PREF."finish_product f 
			LEFT JOIN ".TB_PREF."design_code d ON d.design_id = f.design_id
			LEFT JOIN ".TB_PREF."wood_master wm ON wm.wood_id = f.wood_id
			LEFT JOIN ".TB_PREF."color_master cm ON cm.color_id = f.color_id
			LEFT JOIN ".TB_PREF."item_category c ON c.category_id = f.category_id 
			LEFT JOIN ".TB_PREF."item_range i ON i.id = f.range_id
			LEFT JOIN ".TB_PREF."sales_order_details sorder on sorder.product_id = f.finish_pro_id
			where  sorder.order_no =" . db_escape($order_no) 
		." AND sorder.trans_type = " . db_escape($trans_type) . " ORDER BY i.id";

	return db_query($sql, "Retreive order Line Items");
}
//----------------------------------------------------------------------------------------

function read_sales_order($order_no, &$order, $trans_type)
{

	$myrow = get_sales_order_header($order_no, $trans_type);

	$order->trans_type = $myrow['trans_type'];
	$order->so_type =  $myrow["type"];
	$order->trans_no = array($order_no=> $myrow["version"]);

	$order->set_customer($myrow["debtor_no"], $myrow["name"],
	  $myrow["curr_code"], $myrow["discount"], $myrow["payment_terms"]);

	$order->set_branch($myrow["branch_code"], $myrow["tax_group_id"],
	  $myrow["tax_group_name"], $myrow["contact_phone"]);

	$order->set_sales_type($myrow["sales_type_id"], $myrow["sales_type"], 
	    $myrow["tax_included"], $myrow["factor"]); // no default price calculations on edit

	$order->set_location($myrow["from_stk_loc"], $myrow["location_name"]);

	$order->set_delivery($myrow["ship_via"], $myrow["deliver_to"],
	  $myrow["delivery_address"], $myrow["freight_cost"]);

	$order->sales_type =$myrow["order_type"];
	$order->reference = $myrow["reference"];
	$order->phone = $myrow["contact_phone"];
	$order->email = $myrow["contact_email"];
	$order->consignee = $myrow["consignee"];
	$order->shipment = $myrow["shipment"];

	$order->category_id = $myrow["category_id"];
	$order->range_id = $myrow["range_id"];

	$order->due_date = sql2date($myrow["delivery_date"]);
	$order->document_date = sql2date($myrow["ord_date"]);

	$result = get_sales_order_details($order_no, $order->trans_type);
	if (db_num_rows($result) > 0)
	{
		$line_no=0;
		while ($myrow = db_fetch($result))
		{
			$order->pro[] = $myrow;
		}
	}

	return true;
}

//----------------------------------------------------------------------------------------

function sales_order_has_deliveries($order_no)
{
	$sql = "SELECT SUM(qty_sent) FROM ".TB_PREF.
	"sales_order_details WHERE order_no=".db_escape($order_no)
	." AND trans_type=".ST_SALESORDER;

	$result = db_query($sql, "could not query for sales order usage");

	$row = db_fetch_row($result);

	if ($row[0] > 0)
		return true;  // 2010-04-21 added check for eventually voided deliveries, Joe Hunt
	$sql = "SELECT order_ FROM ".TB_PREF."debtor_trans WHERE type=".ST_CUSTDELIVERY." AND order_=".db_escape($order_no);
	$result = db_query($sql,"The related delivery notes could not be retreived");
	return (db_num_rows($result) > 0);	
}

//----------------------------------------------------------------------------------------

function close_sales_order($order_no)
{
	// set the quantity of each item to the already sent quantity. this will mark item as closed.
	$sql = "UPDATE ".TB_PREF."sales_order_details
		SET quantity = qty_sent WHERE order_no = ".db_escape($order_no)
		." AND trans_type=".ST_SALESORDER;

	db_query($sql, "The sales order detail record could not be updated");
}

//---------------------------------------------------------------------------------------------------------------

function get_invoice_duedate($terms, $invdate)
{
	if (!is_date($invdate))
	{
		return new_doc_date();
	}
	
	$myrow = get_payment_terms($terms);
	
	if (!$myrow)
		return $invdate;

	if ($myrow['day_in_following_month'] > 0)
		$duedate = add_days(end_month($invdate), $myrow['day_in_following_month']);
	else
		$duedate = add_days($invdate, $myrow['days_before_due']);
	return $duedate;
}

function get_customer_to_order($customer_id) {

	// Now check to ensure this account is not on hold */
	$sql = "SELECT *,cust.name, 
		  cust.address, "
		  .TB_PREF."credit_status.dissallow_invoices, 
		  cust.sales_type AS salestype,
		  cust.dimension_id,
		  cust.dimension2_id,
		  stype.sales_type,
		  stype.tax_included,
		  stype.factor,
		  cust.curr_code,
		  cust.discount,
		  cust.payment_terms,
		  cust.pymt_discount,
		  cust.credit_limit - Sum(IFNULL(IF(trans.type=11 OR trans.type=12 OR trans.type=2,
			-1, 1) * (ov_amount + ov_gst + ov_freight + ov_freight_tax + ov_discount),0)) as cur_credit
		FROM ".TB_PREF."debtors_master cust 
		  LEFT JOIN ".TB_PREF."cust_branch bra ON bra.debtor_no = cust.debtor_no 
		  LEFT JOIN ".TB_PREF."debtor_trans trans ON trans.type!=".ST_CUSTDELIVERY." AND trans.debtor_no = cust.debtor_no,"
		  .TB_PREF."credit_status, "
		  .TB_PREF."sales_types stype
		WHERE cust.sales_type=stype.id
			AND cust.credit_status=".TB_PREF."credit_status.id
			AND cust.debtor_no = ".db_escape($customer_id)
		." GROUP by cust.debtor_no";

	$result =db_query($sql,"Customer Record Retreive");
	return 	db_fetch($result);
}

function get_branch_to_order($customer_id, $branch_id) {

    	// the branch was also selected from the customer selection so default the delivery details from the customer branches table cust_branch. The order process will ask for branch details later anyway
	 	$sql = "SELECT ".TB_PREF."cust_branch.br_name, "
			.TB_PREF."cust_branch.br_address, "
			.TB_PREF."cust_branch.br_post_address, "
			." default_location, location_name, default_ship_via, "
			.TB_PREF."tax_groups.name AS tax_group_name, "
			.TB_PREF."tax_groups.id AS tax_group_id
			FROM ".TB_PREF."cust_branch, "
			  .TB_PREF."tax_groups, "
			  .TB_PREF."locations
			WHERE ".TB_PREF."cust_branch.tax_group_id = ".TB_PREF."tax_groups.id
				AND ".TB_PREF."locations.loc_code=default_location
				AND ".TB_PREF."cust_branch.branch_code=".db_escape($branch_id)."
				AND ".TB_PREF."cust_branch.debtor_no = ".db_escape($customer_id);

  	    return db_query($sql,"Customer Branch Record Retreive");
}

function get_sql_for_sales_orders_view($selected_customer, $trans_type, $trans_no, $filter, 
	$stock_item=null, $from='', $to='', $ref='', $location='', $customer_id=ALL_TEXT, $range_id)
{

	$sql = "SELECT 
			sorder.order_no,
			sorder.reference,
			debtor.name,"
			/*.($filter=='InvoiceTemplates' 
				|| $filter=='DeliveryTemplates' ?
			 "sorder.comments, " : "sorder.customer_ref, ")*/
			."sorder.ord_date,
			sorder.delivery_date,
			sorder.deliver_to,
			Sum(line.unit_price*line.quantity) AS OrderValue
		FROM ".TB_PREF."sales_orders as sorder, "
			.TB_PREF."sales_order_details as line, "
			.TB_PREF."debtors_master as debtor, "
			.TB_PREF."finish_product as product
			WHERE sorder.order_no = line.order_no
			AND sorder.trans_type = line.trans_type
			AND product.finish_pro_id = line.product_id
			AND sorder.trans_type = ".db_escape($trans_type)."
			AND sorder.debtor_no = debtor.debtor_no";

	if (isset($trans_no) && $trans_no != "")
	{
		// search orders with number like 
		$number_like = "%".$trans_no;
		$sql .= " AND sorder.order_no LIKE ".db_escape($number_like);
//				." GROUP BY sorder.order_no";
	}
	elseif ($ref != "")
	{
		// search orders with reference like 
		$number_like = "%".$ref."%";
		$sql .= " AND sorder.reference LIKE ".db_escape($number_like);
//				." GROUP BY sorder.order_no";
	}
	else	// ... or select inquiry constraints
	{
		if ($filter!='DeliveryTemplates' && $filter!='InvoiceTemplates' && $filter!='OutstandingOnly')
		{
			$date_after = date2sql($from);
			$date_before = date2sql($to);

			$sql .=  " AND sorder.ord_date >= '$date_after'"
					." AND sorder.ord_date <= '$date_before'";
		}
	}
		/*if ($trans_type == ST_SALESQUOTE && !check_value('show_all'))
			$sql .= " AND sorder.delivery_date >= '".date2sql(Today())."' AND line.qty_sent=0"; // show only outstanding, not realized quotes
*/
		/*if ($selected_customer != -1)
			$sql .= " AND sorder.debtor_no=".db_escape($selected_customer);
*/
		if (isset($stock_item))
			$sql .= " AND line.stk_code=".db_escape($stock_item);

		if (isset($range_id) && $range_id != '-1')
			$sql .= " AND product.range_id=".db_escape($range_id);

		if ($location)
			$sql .= " AND sorder.from_stk_loc = ".db_escape($location);

		if ($filter=='OutstandingOnly')
			$sql .= " AND line.qty_sent < line.quantity";

		elseif ($filter=='InvoiceTemplates' || $filter=='DeliveryTemplates')
			$sql .= " AND sorder.type=1";
			
		//Chaiatanya : New Filter
		if ($customer_id != ALL_TEXT)
			$sql .= " AND sorder.debtor_no = ".db_escape($customer_id);		

		$sql .= " GROUP BY sorder.order_no,
					sorder.ord_date";
	return $sql;
}


function range_sales_order_details($order_no, $trans_type, $range_id)
{

	$sql = "select f.category_id, f.product_image, f.finish_pro_id, d.design_code, c.cat_name,
			f.range_id,	(CASE WHEN f.range_id = -1 then 'Non-Collection' ELSE i.range_name END) AS range_name,
			 f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_density, f.asb_height, d.pkg_w, d.pkg_h, d.pkg_d, d.pro_cbm, sorder.quantity, sorder.unit_price 
			 from ".TB_PREF."finish_product f 
			LEFT JOIN ".TB_PREF."design_code d ON d.design_id = f.design_id
			LEFT JOIN ".TB_PREF."item_category c ON c.category_id = f.category_id 
			LEFT JOIN ".TB_PREF."item_range i ON i.id = f.range_id
			LEFT JOIN ".TB_PREF."sales_order_details sorder on sorder.product_id = f.finish_pro_id
			where  sorder.order_no =" . db_escape($order_no) 
		." AND sorder.trans_type = " . db_escape($trans_type)
		." AND f.range_id = " . db_escape($range_id)
		. " ORDER BY i.id";

	return db_query($sql, "Retreive order Line Items");

}

function save_bpcs($order_no)
{
	$BPCs = $_POST['total_no_bpcs'];

	for($i = 1; $i <= $BPCs; $i++)
	{
		$sql = "insert into ".TB_PREF."bpc(order_id, bpc_name, date_created, date_updated) VALUES("
				.db_escape($order_no).","
				.db_escape($_POST['bpc_name_'.$i]).","
				.date().","
				.db_escape(date()).")";
		db_query($sql,"Could not save BPCs");

		$bpc_id = db_insert_id();

		$total_pro = $_POST["bpc_pro_".$k];
		for($k = 1; $k <= $total_pro; $k++)
		{
			$sql = "insert into ".TB_PREF."bpc_details(bpc_id, finish_pro_id, pro_qty) VALUES("
				.db_escape($bpc_id).","
				.db_escape($_POST["pro_id_bpc_".$k."_".$i]).","
				.db_escape($_POST["pro_qty_bpc_".$k."_".$i]).
				")";
			db_query($sql,"Could not save BPCs Details");
		}
	}
}


?>