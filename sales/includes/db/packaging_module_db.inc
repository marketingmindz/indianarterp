<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//----------------------------------------------------------------------------------------
function get_desgin_part_qty($part_name_id, $de_id){
	$sql = "SELECT quantity FROM ".TB_PREF."assemble_part WHERE assemble_id=".db_escape($part_name_id)." AND design_id=".db_escape($de_id);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_pconss_select_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_pconss_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_assemble_carton_part($part_name_id){
	$sql = "SELECT * FROM ".TB_PREF."assemble_part WHERE assemble_id=".db_escape($part_name_id);
	$result = db_query($sql);
	return db_fetch($result);
}
//--------------------------  Add Packagin Module Code  --------------------------------
//--------------------------  Insert Packaging Module   -------------------------------

function add_packaging_module1111(&$header_design,&$packaging_past,&$corton_session)
{
	global $Refs;
	
	/*foreach ($corton_session as $carton => $carton_line){
		foreach ($carton_line as $master => $master_line){
			foreach ($master_line as $arr => $arr_line){
		 		echo "<pre>"; print_r($arr_line);
				
			}
		}
	 }*/
	
	begin_transaction();
	
	//print_r($packaging_past);
	
	//hook_db_prewrite($dc_obj, ST_DESIGN);
		
			$sql = "INSERT INTO ".TB_PREF."customer_packaging_product (category_id, range_id, design_id, finish_code_id) VALUES(";
		    $sql .= db_escape($header_design['category_id']) . "," .
			 db_escape($header_design['range_id']) . "," .
			 db_escape($header_design['design_id']) . ", " .
			 db_escape($header_design['finish_code_id']) . ")";
	
			db_query($sql, "The packaging finish code record could not be inserted");
		
		#Last insert id 
		$packaging_pro_id = db_insert_id();

	 foreach ($packaging_past->line_items as $line_no => $asb_line)
     {
		
		 
		$sql = "INSERT INTO ".TB_PREF."customer_packaging_part (principle_id, packaging_pro_id, code, unit, quantity, width, height, density) VALUES (";
		$sql .=  db_escape($asb_line->principle_type). ", " . $packaging_pro_id. "," .
		db_escape($asb_line->code_id). "," .
		db_escape($asb_line->unit). "," .
		db_escape($asb_line->quantity). "," .
		db_escape($asb_line->width). "," .
		db_escape($asb_line->height). "," .
		db_escape($asb_line->density). ")";
		db_query($sql, "One of the Packaging part records could not be inserted");

		$packaging_past->line_items[$line_no]->packing_detail_rec = db_insert_id();
		
     }
	 
 
	foreach ($corton_session as $carton => $carton_line){
		foreach ($carton_line as $master => $master_line){
			foreach ($master_line as $arr => $arr_line){
				
				$margin_dimension = $arr_line['mar_weight'].",".$arr_line['mar_height'].",".$arr_line['mar_density'];
				$pkg_dimension = $arr_line['pkg_s_weight'].",".$arr_line['pkg_s_height'].",".$arr_line['pkg_s_density'];
				$carton_dimension = $arr_line['car_s_weight'].",".$arr_line['car_s_height'].",".$arr_line['car_s_density'];
				
		 		$sql = "INSERT INTO ".TB_PREF."customer_carton_details(packaging_pro_id, carton_type_id, master_carton_id, margin_dimension, pkg_dimension, carton_dimension, code, gross_weight, net_weight, carton_type, images) VALUES (";
				$sql .= $packaging_pro_id . ", " . db_escape($arr_line['carton_id']). "," .
				db_escape($arr_line['master_carton_id']). "," .
				db_escape($margin_dimension) . "," .
				db_escape($pkg_dimension) . ", " .
				db_escape($carton_dimension). ",".
				db_escape($arr_line['code']). "," .
				db_escape($arr_line['gross_weight']). "," .
				db_escape($arr_line['net_weight']). "," .
				db_escape($arr_line['carton_type_id']). "," .
				db_escape('images'). ")";
				
				
				db_query($sql, "One of the consumable detail records could not be inserted");
				
				//$consumable_obj->line_con[$line_no]->fcon_detail_rec = db_insert_id();					
			}
		}
	 }
	 
	 
	 
	commit_transaction();

	return $packaging_pro_id;
} 
//code by keshav 20 oct//
function add_packaging_module(&$header_design,&$packaging_past,&$corton_session)
{
	global $Refs;
	
	//echo "<pre>";
	//print_r($corton_session);
	
	//echo "Master<br>";
	//print_r($corton_session['master']);
	//echo "Inner<br>";
	//print_r($corton_session['inner']);
	//die('------------------');
  	
	
	begin_transaction();
	
	//print_r($packaging_past);
	//hook_db_prewrite($dc_obj, ST_DESIGN);
	
		
		$chk_sql = "SELECT * FROM 0_customer_packaging_product WHERE buyers_code =".db_escape($header_design['buyers_code']);
		$result = db_query($chk_sql,'no record');
		
		if(mysql_num_rows($result) < 1 )
		{
		$sql = "INSERT INTO ".TB_PREF."customer_packaging_product (customer_id,category_id, range_id, design_id, finish_code_id, buyers_code) VALUES(";
		    $sql .= db_escape($header_design['customer_id']) . "," .
			 db_escape($header_design['category_id']) . "," .
			 db_escape($header_design['range_id']) . "," .
			 db_escape($header_design['design_id']) . ", " .
			 db_escape($header_design['finish_code_id']) . ", ".
			 db_escape($header_design['buyers_code']) . ")";
	
			db_query($sql, "The packaging finish code record could not be inserted");
		
		#Last insert id 
		$packaging_pro_id = db_insert_id();
		
	 foreach ($packaging_past->line_items as $line_no => $asb_line)
     {
		
		$sql = "INSERT INTO ".TB_PREF."customer_packaging_part (principle_id, packaging_pro_id, code, unit, quantity, width, height, density) VALUES (";
		$sql .=  db_escape($asb_line->principle_type). ", " . $packaging_pro_id. "," .
		db_escape($asb_line->code_id). "," .
		db_escape($asb_line->unit). "," .
		db_escape($asb_line->quantity). "," .
		db_escape($asb_line->width). "," .
		db_escape($asb_line->height). "," .
		db_escape($asb_line->density). ")";
		db_query($sql, "One of the Packaging part records could not be inserted");

		$packaging_past->line_items[$line_no]->packing_detail_rec = db_insert_id();
		
     }
	 
 	
	 #Insert Carton Section
		
	foreach ($corton_session as $carton => $carton_line){
		$i=1;
		foreach ($carton_line as $master => $master_line){
			# $i for insert into carton table
			if($i == 1){
			foreach ($master_line as $arr => $arr_line){
				
				if(isset($arr_line['carton_detail_id'])){
					
					$margin_dimension = $arr_line['margin_dimension'];
					$pkg_dimension = $arr_line['pkg_dimension'];
					$carton_dimension = $arr_line['carton_dimension'];
					$arr_line['cat_qty'] = $arr_line['quantity'];
					//$arr_line['screenimages'] = $arr_line['images'] ;
					//$arr_line['screenlables'] = $arr_line['lables'] ;
					
				}else{
					$margin_dimension = $arr_line['mar_weight'].",".$arr_line['mar_height'].",".$arr_line['mar_density'];
					$pkg_dimension = $arr_line['pkg_s_weight'].",".$arr_line['pkg_s_height'].",".$arr_line['pkg_s_density'];
					$carton_dimension = $arr_line['car_s_weight'].",".$arr_line['car_s_height'].",".$arr_line['car_s_density'];
				}
					$sql = "INSERT INTO ".TB_PREF."customer_carton_details(packaging_pro_id, carton_type_id, master_carton_id, quantity, margin_dimension, pkg_dimension, carton_dimension, code, gross_weight, net_weight, carton_type, images, lables) VALUES (";
					$sql .=  $packaging_pro_id. ", " .db_escape($arr_line['carton_type_id']). "," .
					db_escape($arr_line['master_carton_id']). "," .
					db_escape($arr_line['cat_qty']). "," .
					db_escape($margin_dimension). "," .
					db_escape($pkg_dimension). "," .
					db_escape($carton_dimension). "," .
					db_escape($arr_line['code']). "," .
					db_escape($arr_line['gross_weight']). "," .
					db_escape($arr_line['net_weight']). "," .
					db_escape($arr_line['carton_type']). "," .
					db_escape($arr_line['images']). "," .
					db_escape($arr_line['lables']). ")";
					
					db_query($sql, "One of the consumable detail records could not be inserted");
					
					if($carton=="master"){
						$_SESSION['inserteditem']['master'][]= db_insert_id();
					}
					
					if($carton=="inner"){
						$_SESSION['inserteditem']['inner'][]= db_insert_id();
					}
					
					#Inserting Carton Images
					$temp_images = $arr_line['images'];
					$temp_images = explode(',',$temp_images);
					
					foreach($temp_images as $temp_img){
						if($temp_img != ''){
							$f_Imge = company_path().'/CartonImage/'.$temp_img;
							move_uploaded_file($temp_img, $f_Imge);
						}
					}
					
				}
				
			
			}
			$i=$i+1;
		} 
	 }
	 $_SESSION['master_cart_section']['master']['carton'] = '';
		$_SESSION['master_cart_section']['inner']['carton'] = '';
		
	 #Insert Part Section
	 
			foreach ($corton_session as $carton => $carton_line){
				$i=1;
				foreach ($carton_line as $master => $master_line){
					# $i for insert into carton table
					
						if($i == 2){
							$k=0;
							foreach ($master_line as $arr => $arr_line){
								foreach ($arr_line as $aa => $aa_line){
									$aa_line = (array) $aa_line;	
									//echo $aa_line['part_qty'];
									$sql = "INSERT INTO ".TB_PREF."customer_carton_part_section(carton_detail_id, part_type, quantity) VALUES (";
									
									if($carton=="master"){
										$sql .= db_escape($_SESSION['inserteditem']['master'][$k]). ",";
									}
									
									if($carton=="inner"){
										$sql .= db_escape($_SESSION['inserteditem']['inner'][$k]). ",";
									}
									
									
									$sql .= db_escape($aa_line['part_name_id']). "," .
									db_escape($aa_line['part_qty']). ")";
									
									db_query($sql, "One of the part details for carton could not be inserted");
								}
								$k++;
							}
						}
						$i=$i+1;
					}
			}

	unset($_SESSION['inserteditem']);

display_notification("Packaging added for customer");
	commit_transaction();
	
	/*echo "<pre>";
	print_r($_SESSION['inserteditem']);
	unset($_SESSION['inserteditem']);
	die();*/
	
	return $packaging_pro_id;
	}
		else{
			display_error("Buyers Code Already Exist");
			$_SESSION['master_cart_section']['master']['carton'] = '';
		$_SESSION['master_cart_section']['inner']['carton'] = '';
		}
} 
//code by keshav 20 oct end here//

function get_packaging_search($all_items)
{
	global $order_number;
	 $sql ='Select pm.packaging_pro_id, d.finish_comp_code,d.finish_product_name, CASE WHEN pm.range_id=-1 || pm.range_id=0 THEN "Non Collection" ELSE t.range_name END as rangeNAME, c.cat_name from
	'.TB_PREF.'packaging_product pm Left Join '.TB_PREF.'finish_product d on d.finish_pro_id =pm.finish_code_id Left Join
	'.TB_PREF.'item_category c on pm.category_id = c.category_id  Left Join '.TB_PREF.'item_range t on pm.range_id = t.id';
	
	return $sql;
}

//------------------------ get packaging code header -------------------
function read_packaging_header($packaging_pro_id){
	$result = get_packaging_header($packaging_pro_id, $order);
}

function get_packaging_header($packaging_pro_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."packaging_product WHERE packaging_pro_id=".db_escape($packaging_pro_id);
   	$result = db_query($sql, "The packaging code cannot be retrieved");
	if (db_num_rows($result) == 1)
	{
      	$myrow = db_fetch($result);
				$order->packaging_pro_id = $packaging_pro_id;
      			$_POST['packaging_pro_id'] = $myrow["packaging_pro_id"];
				$_POST['category_id']  = $myrow["category_id"];
				$_POST['collection_id']  = $myrow["collection_id"];
				$_POST['range_id']  = $myrow["range_id"];
				$_POST['design_id']  = $myrow["design_id"];
				$_POST['finish_code_id']  = $myrow["finish_code_id"];

      	return true;
	}
	return false;
}
//--------------- code by chetan (read packing part)---------------------
function read_packaging_part($packaging_pro_id, &$asb)
{
	
	$sql = "SELECT * FROM ".TB_PREF."packaging_part WHERE packaging_pro_id=".db_escape($packaging_pro_id); 
	$result = db_query($sql, "packaging part cannot be retrive");
	
	if (db_num_rows($result) > 0)
		{
			while ($myrow = db_fetch($result))
			{
	
				if ($asb->add_to_pacakaging_part($asb->lines_on_packing, $myrow["principle_id"],
					$myrow["code"],$myrow["unit"],$myrow["quantity"],$myrow["width"],$myrow["height"], $myrow["density"])) {
						$newline = &$asb->line_items[$asb->lines_on_packing-1];
						$newline->packing_detail_rec = $myrow["packing_line_details"];			
				}
			} 
			
		} 
}
// code by bajrang -  - --  carton name   -----22-07-2015

function get_catron_name($carton_id){
	
	$sql = "SELECT carton_name FROM ".TB_PREF."carton_master WHERE carton_id=".db_escape($carton_id)."&& inactive = 0";
	$result = db_query($sql);
	return $result;
}


function get_all_catron_list($packagin_id){
	
	$sql = "SELECT * FROM ".TB_PREF."carton_details WHERE packaging_pro_id=".db_escape($packagin_id);
	$result = db_query($sql);
	return $result;
}
function get_carton_type_name($carton_type){
	
	$sql = "SELECT * FROM ".TB_PREF."carton_master WHERE carton_id=".db_escape($carton_type);
	$result = db_query($sql);
	return db_fetch($result);
}

// -------------- Code by Chetan -----------

function get_finish_header($finish_pro_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_pro_id);

     $result = db_query($sql, "The design code cannot be retrieved");
  if (db_num_rows($result) == 1)
  {

            $myrow = db_fetch($result);
            $_POST['finish_pro_id'] = $myrow["finish_pro_id"];
        $_POST['category_id']  = $myrow["category_id"];
        //$_POST['collection_id']  = $myrow["collection_id"];
		$_POST['range_id']  = $myrow["range_id"];
        if($myrow["category_id"] > 0 && $myrow["range_id"]>0)
           {
              $_POST['collection_id']  = "Collection";
           }else if($myrow["category_id"] > 0 && $myrow["range_id"]<=0){
              $_POST['collection_id']  = "NonCollection";
           }else{
             $_POST['collection_id']  = "SelectCollection";
        }
        
         
        $_POST['design_id']  = $myrow["design_id"];
        $_POST['finish_code_id'] = $myrow["finish_code_id"]; ;
        
    return true;
  }

  
  return false;
}

function read_finish($finish_pro_id){
	
	$result = get_finish_header($finish_pro_id, $order);
	
}
function get_all_carton_part_list($caton_id){
	$sql = "SELECT * FROM ".TB_PREF."carton_part_section WHERE carton_detail_id=".db_escape($caton_id);
	$result = db_query($sql);
	return $result;
}
//-------------------- deleted function --------------

function packaging_deleted($id)
{
  #delete this packaging module product details
  $sql = "DELETE FROM ".TB_PREF."packaging_product WHERE packaging_pro_id =".db_escape($id);
  db_query($sql, "The assemble part could not be deleted.");
  
  #delete this packaging module product assembled part
  $sql = "DELETE FROM ".TB_PREF."packaging_part WHERE packaging_pro_id =".db_escape($id);
  db_query($sql, "The assemble part could not be deleted.");
  
  #get carton id for this packaging module and delete their part section
  $sql = "SELECT carton_detail_id FROM ".TB_PREF."carton_details WHERE packaging_pro_id=".db_escape($id);
  $result = db_query($sql);
  while ($row = db_fetch($result)){
    $sql = "DELETE FROM ".TB_PREF."carton_part_section WHERE carton_detail_id =".db_escape($row['carton_detail_id']);
    db_query($sql, "The assemble part could not be deleted.");       
  }
  
  #delete carton for this packaging module
  $sql = "DELETE FROM ".TB_PREF."carton_details WHERE packaging_pro_id =".db_escape($id);
  db_query($sql, "The assemble part could not be deleted.");
  
}
//-------------packaging list function code by keshav-----------------------------------//
function get_all_packaging_list($customer_id){
	
		$sql ='Select cp.packaging_pro_id, cat.cat_name, d.design_code, f.finish_comp_code, cp.category_id, cp.design_id, cp.finish_code_id, cp.buyers_code,CASE WHEN cp.range_id=-1 || cp.range_id=0 THEN "Non Collection" ELSE r.range_name END as rangeNAME from '.TB_PREF.'customer_packaging_product cp
		Left Join '.TB_PREF.'finish_product f on f.finish_pro_id = cp.finish_code_id
		Left Join '.TB_PREF.'design_code d on d.design_id = cp.design_id 
		Left Join '.TB_PREF.'item_range r on r.id = cp.range_id 
		Left Join '.TB_PREF.'item_category cat on cat.category_id = cp.category_id where cp.customer_id='.db_escape($customer_id);
return $sql;
}

function view_selected_packaging_list($selected_id){
	$id = $selected_id;
	$sql = "SELECT * FROM 0_customer_packaging_product WHERE packaging_pro_id = $id";
	$result = db_query($sql,"Not retrived");
	return $result;
}

function view_selected_packaging_part_list($selected_id){
	$id = $selected_id;
	$sql2 = "SELECT * FROM 0_customer_packaging_part WHERE packaging_pro_id = $id";
	$result2 = db_query($sql2,"not retived");
	return $result2;
}

function view_selected_carton_list($selected_id){
	$id = $selected_id;
	$sql3 = "SELECT * FROM 0_customer_carton_details WHERE packaging_pro_id = $id";
	$result3 = db_query($sql3,"not retrived");
	return $result3;
}
//--------------------------------------------------------------------------------------//
?>