<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function update_sales_orderStatus($order_no)
{
	$sql = "update ".TB_PREF."sales_orders set bpcCreated = '1' where order_no = ".db_escape($order_no)." && trans_type = '30'"; 
	db_query($sql, "Could not update sales order bpcStatus");
}

function save_bpcs($order_no)
{
	$BPCs = $_POST['total_no_bpcs'];

	for($i = 1; $i <= $BPCs; $i++)
	{
		$date = date2sql(Today());
		$sql = "insert into ".TB_PREF."bpc(order_id, bpc_name, date_created, date_updated) VALUES("
				.db_escape($order_no).","
				.db_escape($_POST['bpc_name_'.$i]).","
				.db_escape($date).","
				.db_escape($date).")";
		db_query($sql,"Could not save BPCs");

		$bpc_id = db_insert_id();

		$total_pro = $_POST["bpc_pro_".$i];
		for($k = 1; $k <= $total_pro; $k++)
		{
			if(!check_num("pro_id_bpc_".$i."_".$k, 1)){
				display_notificatoin($_POST["pro_id_bpc_".$i."_".$k]);
				continue;
			}

			$sql = "insert into ".TB_PREF."bpc_details(bpc_id, finish_pro_id, pro_qty) VALUES("
				.db_escape($bpc_id).","
				.db_escape($_POST["pro_id_bpc_".$i."_".$k]).","
				.db_escape($_POST["pro_qty_bpc_".$i."_".$k]).
				")";
			db_query($sql,"Could not save BPCs Details");
		}
	}

	update_sales_orderStatus($order_no);
}


function get_sql_for_bpcs_view($order_no, $bpcNumber, $from='', $to='')
{

	$sql = "SELECT bpc.bpc_id, bpc.order_id, bpc.bpc_name, bpc.date_created, sorder.ord_date, sorder.delivery_date
		FROM ".TB_PREF."bpc bpc LEFT JOIN "
			.TB_PREF."sales_orders sorder ON sorder.order_no = bpc.order_id".
			" WHERE 1 = 1 AND sorder.trans_type = 30 ";

	if (isset($bpcNumber) && $bpcNumber != "")
	{
		// search bpc with number like 
		$number_like = "%".$bpcNumber;
		$sql .= " AND bpc.bpc_id LIKE ".db_escape($number_like);
	}
	elseif(isset($order_no) && $order_no != "")
	{
		// search orders with number like 
		$number_like = "%".$order_no;
		$sql .= " AND bpc.order_id LIKE ".db_escape($number_like);
	}
	else	// ... or select inquiry constraints
	{
		if ($filter!='DeliveryTemplates' && $filter!='InvoiceTemplates' && $filter!='OutstandingOnly')
		{
			$date_after = date2sql($from);
			$date_before = date2sql($to);

			$sql .=  " AND sorder.ord_date >= '$date_after'"
					." AND sorder.ord_date <= '$date_before'";
		}
	}
	$sql .= " GROUP BY bpc.bpc_id ORDER BY sorder.delivery_date";
	return $sql;
}

function read_bpc_header($bpc_id)
{
	$sql = "select * from ".TB_PREF."bpc bpc LEFT JOIN ".TB_PREF."sales_orders sorder on sorder.order_no = bpc.order_id where bpc.bpc_id = ".db_escape($bpc_id);
	$result = db_query($sql, "Could not Select BPC");
	return db_fetch($result);
}

function read_bpc_details($bpc_id)
{
	$sql = "select * from ".TB_PREF."bpc_details bpc LEFT JOIN ".TB_PREF."finish_product f ON bpc.finish_pro_id =  f.finish_pro_id where bpc.bpc_id = ".db_escape($bpc_id);
	$result = db_query($sql, "Could not read BPC Details.");
	$bpc_details = Array();
	while($row = db_fetch($result))
	{
		$bpc_details[] = $row;
	}
	return $bpc_details;
}

?>
