<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/manufacturing.inc");

//------- display sheet type and offer options row -----

function sheet_type_row()
{
	start_row();
		echo "<td class='tableHeader'>Select Sheet Type</td>";
		echo "<td><select name='sheetType' id='sheetType' >";
		echo "<option value='singeSheet' >Single Sheet</option>";
		echo "<option value='multiSheet' >Multiple Sheet</option>";
		echo "<select></td>";
	end_row();

}

function offer_options_row()
{
	start_row();
		echo "<td class='tableHeader'>Select Offer Options</td>";
		echo "<td><select name='offerOption' id='offerOption' >";
		echo "<option value='-1' >-- Select Offer Option --</option>";
		echo "<option value='wprice' >W/O PRICE</option>";
		echo "<option value='wquantity' >W/O QTY DETAIL</option>";
		echo "<option value='wcbmquantity' >W/O CBM AND QUANTITY</option>";
		echo "<option value='wmoqprice' >W/O MOQ AND PRICE</option>";
		echo "<select></td>";
	end_row();
}


//-------------display Products-------------------------
function display_products()
{
	
	if($_POST['collection_id'] == "SelectCollection"){
		display_error("Select Product Collection Type.");
		set_focus('collection_id');
		return false;
	}

	if(!isset($_POST['category_id'])){
		display_error("Select Product Category.");
		set_focus('category_id');
		return false;
	}
	$category_id = $_POST['category_id'];

	if($_POST['collection_id'] == 'Collection')	{
		if(!isset($_POST['range_id'])){
			display_error("Select Product Range.");
			set_focus('range_id');
			return false;
		}
		$range_id = implode(",", $_POST['range_id']);
	}
	else
	{
		$range_id = '-1';
	}
	if($_POST['collection_id'] == 'NonCollection')	{
		$range_id = '-1';
	}
	div_start("ProductsList123", "style='max-height:450px; width:70%; overflow-y:scroll;'");
	echo "<br>";
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");
	$th = array(_("Category Name"), _("Range Name"), _("Design Code"),_("Finish Code"),_("Select Product"));
	table_header($th);

	$products = get_products_details($category_id, $range_id);

	/*$category = array();
	foreach ($category_id as $catId) {
		$category[] = get_catgory_details($catId);
	}
	function compareByName($category, $b) {
		return strcmp($category["cat_name"], $b["cat_name"]);
	}
	sort($category, 'compareByName'); */
	start_row();
		echo "<td colspan='5' style='text-align: right;position:relative;right:67px;'>Select All<input type='checkbox' id='all_product'></td>";
	end_row();
	foreach ($products as $pro) {
		start_row();
			label_cell($pro['cat_name']);
			label_cell($pro['range_name']);
			label_cell($pro['design_code']);
			label_cell($pro['finish_comp_code']);
			$checked = "";
			if(in_array($pro['finish_pro_id'], $_POST['select']))
			{
				$checked = "checked";
			}
			echo "<td><input type='checkbox' ".$checked." class='single_product' name='select[".$pro['finish_pro_id']."]' value='".$pro['finish_pro_id']."'></td>";
		end_row();
	}
	
	end_table();
	div_end();
	submit_center_first('process', "Process", _('Process'), 'default');
}



//--------------------------------------------------------------------------------
function add_to_order(&$order, $new_item, $new_item_qty, $price, $discount, $description='')
{
	// calculate item price to sum of kit element prices factor for 
	// value distribution over all exploded kit items
	 $std_price = get_kit_price($new_item, $order->customer_currency, 
		$order->sales_type,	$order->price_factor, get_post('OrderDate'), true);

	if ($std_price == 0)
		$price_factor = 0;
	else
		$price_factor = $price/$std_price;

	$kit = get_item_kit($new_item);
	$item_num = db_num_rows($kit);

	while($item = db_fetch($kit)) {
		$std_price = get_kit_price($item['stock_id'], $order->customer_currency, 
			$order->sales_type,	$order->price_factor, get_post('OrderDate'), true);

		// rounding differences are included in last price item in kit
		$item_num--;
		if ($item_num) {
			$price -= $item['quantity']*$std_price*$price_factor;
			$item_price = $std_price*$price_factor;
		} else {
			if ($item['quantity']) 
				$price = $price/$item['quantity'];
			$item_price = $price;
		}
		$item_price = round($item_price, user_price_dec());

	 if (!$item['is_foreign'] && $item['item_code'] != $item['stock_id'])
	 {	// this is sales kit - recurse 
		add_to_order($order, $item['stock_id'], $new_item_qty*$item['quantity'],
			$item_price, $discount);
	 }
	 else
	 {	// stock item record eventually with foreign code

		// check duplicate stock item
		foreach ($order->line_items as $order_item)
		{
			if (strcasecmp($order_item->stock_id, $item['stock_id']) == 0)
			{
				display_warning(_("For Part :").$item['stock_id']. " " 
					. _("This item is already on this document. You have been warned."));
				break;
			}
		}
		$order->add_to_cart (count($order->line_items),	$item['stock_id'], 
			$new_item_qty*$item['quantity'], $item_price, $discount, 0,0, $description);
	 }
	}

}
//---------------------------------------------------------------------------------

function get_customer_details_to_order(&$order, $customer_id, $branch_id)
{
	global $SysPrefs;
	
	$ret_error = "";

	$myrow = get_customer_to_order($customer_id);

	$name = $myrow['name'];
	$res = get_crm_persons('customer', $action, $customer_id);
	while($contact = db_fetch($res)){
		$phone = $contact['phone'];
		$email = $contact['email'];
	}
	if ($myrow['dissallow_invoices'] == 1)
		$ret_error = _("The selected customer account is currently on hold. Please contact the credit control personnel to discuss.");

	$deliver = $myrow['address']; // in case no branch address use company address

	$order->set_customer($customer_id, $name, $phone, 
		$myrow['discount'], $myrow['payment_terms'], $myrow['pymt_discount'], $email);
	


	// FIX - implement editable contact selector in sales order 
	$contact = get_branch_contacts($branch_id, 'order', $customer_id);
	$order->set_branch($branch_id, $myrow["tax_group_id"],
	$myrow["tax_group_name"], @$contact["email"]);

	$address = trim($myrow["br_post_address"]) != '' ? $myrow["br_post_address"]
		: (trim($myrow["br_address"]) != '' ? $myrow["br_address"]:$deliver);

	$order->set_delivery($myrow["default_ship_via"], $myrow["br_name"],
		$address);
	if ($order->trans_type == ST_SALESINVOICE) {
		$order->due_date = get_invoice_duedate($order->payment, $order->document_date);
	}
	elseif ($order->trans_type == ST_SALESORDER)
		$order->due_date = add_days($order->document_date, $SysPrefs->default_delivery_required_by());
	if($order->payment_terms['cash_sale']) {
		$order->set_location($order->pos["pos_location"], $order->pos["location_name"]);
	} else
		$order->set_location($myrow["default_location"], $myrow["location_name"]);

	return $ret_error;
}

//---------------------------------------------------------------------------------

function display_order_summary($title, &$order, $editable_items=false)
{	
	if (isset($_GET['ModifyQuotationNumber']) && is_numeric($_GET['ModifyQuotationNumber']))
	{
		$_POST['category_id'] = explode(",",$order->category_id);
		if($order->range_id == '-1')
		{
			$_POST['collection_id'] = 'NonCollection';
			$_POST['range_id'] = '-1';
		}
		else
		{
			$_POST['collection_id'] = 'Collection';
			$_POST['range_id'] = explode(",",$order->range_id);
		}
		$_POST['SelectItem'] = "Select Products";
		$result = get_sales_order_details($_GET['ModifyQuotationNumber'], $order->trans_type);
		if (db_num_rows($result) > 0)
		{	
			$products = array();
			$line_no=0;
			while ($myrow = db_fetch($result))
			{
				$product_id = $myrow['finish_pro_id'];
				$order->pro[] = $myrow; 
				$products[] = $product_id;
				$_POST['moq'.$product_id] = $myrow['quantity'];
				$_POST['price'.$product_id] = $myrow['unit_price'];
			} 
			$_POST['select'] = $products;
		}
	}
	
    div_start('items_table');	
    display_heading($title);
    div_start('product', "style=min-height:100px;");
	echo '<p id="pro_cat" style="max-width:700px; margin:auto;">'.$_POST['pro_categories'].'</p>
			<input type="hidden" name="pro_categories" id="pro_categories" value="'.$_POST['pro_categories'].'" >';	
	global $Ajax;
	
	start_table(TABLESTYLE, "style='width:50%;'");	
		multi_category_list_row(_("Product Category:"), 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){
			multi_range_list_row(_("Range:"), 'range_id', null, _('----Select---'), null, 'id="range_span" style=display:none');
		}else{
			multi_range_list_row(_("Range:"), 'range_id', null, _('----Select---'), null,'id="range_span"');
		}
	end_table();
	echo "<br>";
	submit_center_first('SelectItem', "Select Products", _('Select Products'), 'default');
	echo "<br>";
	if(isset($_POST['SelectItem']))
    {	
    	$Ajax->activate('ProductsList');
    	$Ajax->activate('items_table');
    	$Ajax->activate('final_list');
    	div_start("ProductsList");
    		if (isset($_GET['ModifyQuotationNumber']) && is_numeric($_GET['ModifyQuotationNumber']))
			{
				display_products($_POST['select']);
				$_POST['process'] = "Process";
			}
			else
			{
				display_products();
			}
    	div_end();
    	
    }
  
	
    div_end();
div_end();
    
    
    

   
    echo "<br><br>";
    div_start("final_list");
    if(isset($_POST['process']))
    {	
    	$Ajax->activate('final_list');
    	div_start("final_list1", "style='max-height:450px; overflow-y:scroll;'");

    	 
    	if(!isset($_POST['select'])){
    		display_error("Select Products.");
    		return false;
    	}
    	$finish_pro_id = implode(",",$_POST['select']);
    	$products = display_final_list($finish_pro_id);


    	start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_("S. No."), _("Product Image"), _("Finish Code"),  _("Product Name"), _("Category Name"), _("Range Name"), _("Design Code"),_("Size"),_("Box Size"),_("Box CBM"),_("QTY 40`HQ"),_("QTY 40`"),_("QTY 20`"),_("MOQ"),_("Price"));
		table_header($th);
		$i = 1; 
		foreach ($products as $pro) {
			start_row();
				label_cell($i);
				$pro['product_image'] = rtrim($pro['product_image'],',');//shubham Code
				$images = explode(",", $pro['product_image']);
				echo "<td>";
				foreach ($images as $img) {
					echo " <img width='60px' src='".$path_to_root."../company/0/finishProductImage/".$img."' />";
				}
				echo "</td>";
				label_cell($pro['finish_comp_code']);
				label_cell($pro['finish_product_name']);
				label_cell($pro['cat_name']);
				label_cell($pro['range_name']);
				label_cell($pro['design_code']);
				
				label_cell($pro['asb_weight']."*".$pro['asb_density']."*".$pro['asb_height']);
				label_cell($pro['pkg_w']."*".$pro['pkg_d']."*".$pro['pkg_h']);
				label_cell(round($pro['pro_cbm'], 2));
				label_cell(round(65/$pro['pro_cbm'], 2));
				label_cell(round(52/$pro['pro_cbm'], 2));
				label_cell(round(26/$pro['pro_cbm'], 2));
				$product_id = $pro['finish_pro_id']; 
				$moq = 'moq'.$product_id;
				$price = 'price'.$product_id;
				text_cells("","moq".$product_id,$_POST[$moq],"10");
				text_cells("","price".$product_id,$_POST[$price],"10");
				
			end_row();
			$i++;
		}
		
		end_table();

    	div_end();
    } 
    
    div_end();	
}

// ------------------------------------------------------------------------------

function display_order_header(&$order, $editable, $date_text)
{
	global $Ajax, $SysPrefs;
	$_POST['OrderDate'] = today();
	if(isset($_GET['ModifyQuotationNumber']) && is_numeric($_GET['ModifyQuotationNumber']))
	{
		$editable = 0;
	}

	start_outer_table(TABLESTYLE2, "width=80%");
	table_section(1);
	
	$customer_error = "";
	$change_prices = 0;

	if (isset($order) && !$editable)
	{
		// can't change the customer/branch if items already received on this order
		//echo $order->customer_name . " - " . $order->deliver_to;
		label_row("Customer", $order->customer_name . " - " . $order->deliver_to);
		hidden('customer_id', $order->customer_id);

	
	}
	else
	{
		customer_list_row(_("Customer:"), 'customer_id', null, false, true, false, true);
		label_row(null, "Press F2 for New Customer.");

		$old_order = (PHP_VERSION<5) ? $order : clone( $order );

		$customer_error = get_customer_details_to_order($order, $_POST['customer_id'], "");
		$_POST['Location'] = $order->Location;
		$_POST['deliver_to'] = $order->deliver_to;
		$_POST['delivery_address'] = $order->delivery_address;
		$_POST['phone'] = $order->phone;
		$_POST['delivery_date'] = today();
		$_POST['email'] = $order->email;
		set_global_customer($_POST['customer_id']);
		
	}

	ref_row(_("Reference").':', 'ref', _('Reference number unique for this document type'), null, '');
	text_row(_("Contact Phone Number:"), 'phone', $order->phone, 25, 25,
		    _('Phone number of ordering person. Defaults to branch phone number'));
	text_row(_("Email:"), 'email', $order->email, null, null, _('Email of customer.'));


	table_section(2);

	text_row(_("Consignee Information:"), 'consignee', $order->consignee, null, null, _('Consignee Information'));
	text_row(_("Shipment Information:"), 'shipment',$order->shipment, null, null, _('Shipment Information'));

	if ($editable)
	{
		if (!isset($_POST['OrderDate']) || $_POST['OrderDate'] == "")
			$_POST['OrderDate'] = $order->document_date;

		date_row($date_text, 'OrderDate', _('Date of order receive'),
			$order->trans_no==0, 0, 0, 0, null, true);
		if (isset($_POST['_OrderDate_changed']) || list_updated('payment')) {
			if (!is_company_currency($order->customer_currency) 
				&& (get_base_sales_type()>0)) {
					$change_prices = 1;
			}
			$Ajax->activate('_ex_rate');
			if ($order->trans_type == ST_SALESINVOICE) {
				$_POST['delivery_date'] = get_invoice_duedate(get_post('payment'), get_post('OrderDate'));
			} else 
				$_POST['delivery_date'] = add_days(get_post('OrderDate'), $SysPrefs->default_delivery_required_by());
			$Ajax->activate('items_table');
			$Ajax->activate('delivery_date');
		}
		
	}
	else
	{
		label_row($date_text, $order->document_date);
		hidden('OrderDate', $order->document_date);
	}
	if ($order->trans_type==ST_SALESINVOICE)
		{
			$title = _("Delivery Details");
			$delname = _("Due Date").':';
		}
		elseif ($order->trans_type==ST_CUSTDELIVERY)
		{
			$title = _("Invoice Delivery Details");
			$delname = _("Invoice before").':';
		}
		elseif ($order->trans_type==ST_SALESQUOTE)
		{
			$title = _("Quotation Delivery Details");
			$delname = _("Valid until").':';
		}
		else
		{
			$title = _("Order Delivery Details");
			$delname = _("Required Delivery Date").':';
		}
	date_row($delname, 'delivery_date',
			$order->trans_type==ST_SALESORDER ?  _('Enter requested day of delivery') 
				: $order->trans_type==ST_SALESQUOTE ? _('Enter Valid until Date') : '');



	table_section(3);

		text_row(_("Deliver To:"), 'deliver_to', $order->deliver_to, 40, 40,
			_('Additional identifier for delivery e.g. name of receiving person'));

		textarea_row(_("Address:"), 'delivery_address', $order->delivery_address, 35, 5,
			_('Delivery address. Default is address of customer branch'));

	

	end_outer_table(1); // outer table

	if ($change_prices != 0) {
		foreach ($order->line_items as $line_no=>$item) {
			$line = &$order->line_items[$line_no];
			$line->price = get_kit_price($line->stock_id, $order->customer_currency,
				$order->sales_type, $order->price_factor, get_post('OrderDate'));
		//		$line->discount_percent = $order->default_discount;
		}
	    $Ajax->activate('items_table');
	}

	return $customer_error;
}

//-------------display Products-------------------------
function display_OrderItems($order)
{
	//start_table(TABLESTYL2, "colspan=7 style='width:100%;'");
	echo '<tr class=itemRows><td class="tableheader">Product Name</td>
					<td class="tableheader">Finish Code</td>
					<td class="tableheader">Category Name</td>
					<td class="tableheader">Range Name</td>
					<td class="tableheader">Design Code</td>
					<td class="tableheader">Quantity</td>
					<td class="tableheader">CBM</td>
				</tr>';
	foreach ($order->pro as $pro) {
		start_row("class=itemRows");
			label_cell($pro['finish_product_name']);
			label_cell($pro['finish_comp_code']);
			label_cell($pro['cat_name']);
			label_cell($pro['range_name']);
			label_cell($pro['design_code']);
			label_cell($pro['quantity']);
			label_cell($pro['pro_cbm']);
		end_row();
	}
	
	//end_table();
	br();
}
?>