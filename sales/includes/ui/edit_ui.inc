<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function text_cells_cost($label, $name, $value=null, $size="", $max="", $title=false, 
	$labparams="", $post_label="", $inparams="", $width="")
{
  	global $Ajax;

	default_focus($name);
	if ($label != null)
		label_cell($label, $labparams);
	echo "<td width=".$width.">";

	if ($value === null)
		$value = get_post($name);
	echo "<input $inparams type=\"text\" name=\"$name\" size=\"$size\" maxlength=\"$max\" value=\"$value\""
	    .($title ? " title='$title'" : '')
	    .">";

	if ($post_label != "")
		echo " " . $post_label;

	echo "</td>\n";
	$Ajax->addUpdate($name, $name, $value);
}


function display_design_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_design_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_design_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_design_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['consumable_name']);
			label_cell($row['consumable_category']);
			label_cell($row['unit']);
			text_cells(null, 'quantity', $row['quantity'], 10, 10, null, null, null,"id=quantity");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_design_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="5">Total Design Consumables Cost</td>';
		 text_cells(null, 'total_design_cons_cost', $total_design_cons_cost, 10, 10, null, null, null,"id=total_design_cons_cost");    
	end_row();
	end_table();
	
	
}
function display_finish_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_finish_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_finish_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_finish_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['consumable_id']);
			label_cell($row['consumable_category']);
			label_cell($row['unit']);
			text_cells(null, 'quantity', $row['quantity'], 10, 10, null, null, null,"id=quantity");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_finish_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="5">Total Finish Consumables Cost</td>';
		 text_cells(null, 'total_finish_cons_cost', $total_finish_cons_cost, 10, 10, null, null, null,"id=total_finish_cons_cost");    
	end_row();
	end_table();
}
function display_fabric_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_fabric_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_fabric_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Fabric Name"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_fabric_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['fabric_id']);
			text_cells(null, 'percentage', $row['percentage'], 10, 10, null, null, null,"id=percentage");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_fabric_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="4">Total Fabric Consumables Cost</td>';
		 text_cells(null, 'total_fabric_cons_cost', $total_fabric_cons_cost, 10, 10, null, null, null,"id=total_fabric_cons_cost");    
	end_row();
	end_table();
}
function display_wood_costing($mfrg_id)
{
	$sql = "select * from ".TB_PREF."cust_manufacturing_wood_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_wood_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Wood"), _("CFT"), _("Rate"), _("Amount"));         
	//table_header($th);
	$total_wood_cost = 0;
	$i = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['amount'];
			$wood = get_wood_name($row['wood_id']);
			label_cell($wood['consumable_name'], "width=152px");
			echo '<input type="hidden" name="wood_id'.$i.'" class="w_in" value="'.$row['wood_id'].'" id="wood_id"'.$i.' >';
			text_cells(null, 'cft'.$i, $row['cft'], 10, 10, null, null, null,"class='w_in' id=cft_".$i);	
			text_cells(null, 'rate'.$i, $row['rate'], 10, 10, null, null, null,"class='w_in' id=rate_".$i);	
			text_cells(null, 'amount'.$i, $cost, 10, 10, null, null, null,"class='w_in' id=amount_".$i);	
		end_row();	
			$total_wood_cost += $cost;
			$i++;
	}
	/*start_row();
	 echo '<td colspan="3">Total Wood Cost</td>';
		 text_cells(null, 'total_wood_cost', $total_wood_cost, 10, 10, null, null, null,"readonly id=total_wood_cost");    
	end_row();*/
	end_table();	
}

function display_special_labour_costing($mfrg_id, $basic_price)
{
	$sql = "select * from ".TB_PREF."cust_manufacturing_labour_special_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_special_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Labout Type"), _("special Rate"), _("Amount"), _("Cost"));         
	//table_header($th);
	$total_special_labour_cost = 0;
	$i = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$special_labour_cost = $row['special_labour_cost'];
			text_cells(null, 'labour_type'.$i, $row['labour_type'], 20, null, null, null, null,"class='l_in' id=labour_type_".$i);	
			text_cells(null, 'special_rate_'.$i, $row['special_rate'], 10, 10, null, null, null,"class='l_in' id=special_rate_".$i);	
			text_cells(null, 'labour_amount_'.$i, $row['labour_amount'], 10, 10, null, null, null,"class='l_in' id=labour_amount_".$i);	
			text_cells(null, 'special_labour_cost_'.$i, $special_labour_cost, 10, 10, null, null, null,"class='l_in' id=special_labour_cost_".$i);	
		end_row();	
			$total_special_labour_cost += $special_labour_cost;
			$i++;
	}
	end_table();
	 //echo '<td colspan="3">Total Special Cost</td>';
	echo "<td width=142>";
	echo '<input readonly="readonly" id="total_special_labour_cost" type="text" name="total_special_labour_cost" size="19" maxlength="10" value="'.$total_special_labour_cost.'">';
	echo "</td>";    
	//end_row();
	
	
	/*$total_labour_cost = $basic_price + $total_special_labour_cost;	
	//label_cell($total_labour_cost);
		//echo '<input type="hidden" name="total_labour_cost" id="total_labour_cost" value="'.$total_labour_cost.'" />';		
		label_cell($total_labour_cost,null,"total_labour_cost_label");
		echo '<input type="hidden" readonly name="total_labour_cost" id="total_labour_cost" value="'.$total_labour_cost.'" />';	
*/

}

function basic_labour_price($mfrg_id)
{
	$sql = "select basic_price from ".TB_PREF."cust_manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_cost ###edit_cost");
	while($row = db_fetch($result))
	{
		$basic_price = $row['basic_price'];		
	}
	return $basic_price;
}
function display_labour_costing($mfrg_id)
{	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Basic Price"),_("Special Labour Cost"),_("Total Labour Cost"));         
	//table_header($th);
	start_row();
	
	
	$sql = "select * from ".TB_PREF."cust_manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_cost ###edit_cost");
	
	//start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Basic Price"));         
	//table_header($th);
	//start_row();
	while($row = db_fetch($result))
	{
		$basic_price = $row['basic_price'];
		text_cells(null, 'basic_price', $basic_price, 10, 10, null, null, null,"id=basic_price");		
	}
	//end_row();
	//end_table();
	echo "<td  class='special_labour_cost'>";
	display_special_labour_costing($mfrg_id, $basic_price);
	echo "</td>";
	end_row();
	end_table();
}


function display_mfrg_cost($finish_code_id)
{
	$sql = "select * from ".TB_PREF."cust_manufacturing_cost where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve manufacturing_cost ###edit_cost");
	
	//start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(/*_("Design Consumables"),_("Finish Consumables"), _("Fabric Consumables"),*/ _("Wood"), _("Labour"),  _("Extra %"), _("Extra Amount"), _("Total Cost"), _("Admin %"), _("Admin Cost"), _("Total CP"), _("Profit %"), _("Profit"), _("Total MFRG Cost"));         
	//table_header($th);
	
	while($row = db_fetch($result))
	{
		//start_row();
		$mfrg_id = $row['id'];
		/*echo "<td>";
		display_design_consumables($mfrg_id);
		echo "</td><td>";
		display_finish_consumables($mfrg_id);
		echo "</td><td>";
		display_fabric_consumables($mfrg_id);
		
		echo "</td>";*/
		//echo "<td class='wood_cost'>";
		echo '<input type="hidden" name="total_cons_cost" id="total_cons_cost" value="0" />';
		echo '<input type="hidden" name="total_finish_cons_cost" id="total_finish_cons_cost" value="0" />';
		echo '<input type="hidden" name="total_fabric_cost" id="total_fabric_cost" value="0" />';
		//display_wood_costing($mfrg_id);
		//echo "</td><td>";
		//display_labour_costing($mfrg_id);
		//echo "</td>";
		text_cells_cost(null, 'extra_percent', $row['extra_percent'], 10, 10, null, null, null,"id=extra_percent","89");
		text_cells_cost(null, 'extra_amount', $row['extra_amount'], 10, 10, null, null, null,"id=extra_amount","89");

		//echo "<td>".$row['total_cost']."</td>";
		label_cell($row['total_cost'],"width=89","total_cost_label");	
		echo '<input type="hidden" name="total_cost" id="total_cost" value="'.$row['total_cost'].'" />';
		
		text_cells_cost(null, 'admin_percent', $row['admin_percent'], 10, 10, null, null, null,"id=admin_percent","89");
		text_cells_cost(null, 'admin_cost', $row['admin_cost'], 10, 10, null, null, null,"id=admin_cost","89");
				
		//echo "<td>".$row['total_cp'];
		label_cell($row['total_cp'],"width=89","total_cp_label");	
		echo '<input type="hidden" name="total_cp" id="total_cp" value="'.$row['total_cp'].'" />';
		
		text_cells_cost(null, 'profit_percent', $row['profit_percent'], 10, 10, null, null, null,"id=profit_percent","89");
		text_cells_cost(null, 'profit', $row['profit'], 10, 10, null, null, null,"id=profit","89");

		//echo "<td>".$row['total_mfrg'];
		//echo "</td>";
		label_cell($row['total_mfrg'],"width=89","total_mfrg_label");	
		echo '<input type="hidden" readonly name="total_mfrg" id="total_mfrg" value="'.$row['total_mfrg'].'" />';
		//end_row();	
		
	}
	//end_table();
}



function display_finishing_cost($finish_code_id)
{	
	/*start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Sanding %"),_("Sending Cost"),_("Polish %"),_("Polish Cost"),_("Packaging %"),_("Packaging Cost"),_("Forwarding %"),_("Forwarding Cost"), _("Special Finishing Cost"), _("Other Cost"), _("Total Finishing Cost"));            
	table_header($th);
	start_row();*/
	
	
	$sql = "select * from ".TB_PREF."cust_finishing_cost where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve finishing_cost ###edit_cost");

	while($row = db_fetch($result))
	{
		$finishing_cost_id = $row['id'];
		$sanding_percent = $row['sanding_percent'];
		$sanding_cost = $row['sanding_cost'];
		$polish_percent = $row['polish_percent'];
		$polish_cost = $row['polish_cost'];
		$packaging_percent = $row['packaging_percent'];
		$packaging_cost = $row['packaging_cost'];
		$forwarding_percent = $row['forwarding_percent'];
		$forwarding_cost = $row['forwarding_cost'];
		$other_cost = $row['other_cost'];
		text_cells_cost(null, 'sanding_percent', $sanding_percent, 10, 10, null, null, null,"id=sanding_percent","89");		
		text_cells_cost(null, 'sanding_cost', $sanding_cost, 10, 10, null, null, null,"id=sanding_cost","89");		
		text_cells_cost(null, 'polish_percent', $polish_percent, 10, 10, null, null, null,"id=polish_percent","89");		
		text_cells_cost(null, 'polish_cost', $polish_cost, 10, 10, null, null, null,"id=polish_cost","89");		
		text_cells_cost(null, 'packaging_percent', $packaging_percent, 10, 10, null, null, null,"id=packaging_percent","89");		
		text_cells_cost(null, 'packaging_cost', $packaging_cost, 10, 10, null, null, null,"id=packaging_cost","89");		
		text_cells_cost(null, 'forwarding_percent', $forwarding_percent, 10, 10, null, null, null,"id=forwarding_percent","89");		
		text_cells_cost(null, 'forwarding_cost', $forwarding_cost, 10, 10, null, null, null,"id=forwarding_cost","89");
		$total_finishing = $sanding_cost + $polish_cost + $packaging_cost + $forwarding_cost;		
	}

	echo "<td  class='special_finishing_cost' width='418' colspan='4'>";
		$sql = "select * from ".TB_PREF."cust_finishing_special_cost where finishing_cost_id = ".db_escape($finishing_cost_id);
		$result = db_query($sql, "Could not retrieve special finishing cost##edit cost");
		start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_("Special Cost Reference"),_("Special Cost Rate"),_("Special Cost Amount"),_("Total"));         
		//table_header($th);
		$total_finishing_special_cost = 0;
		$i = 0;
		while($row =  db_fetch($result))
		{
			start_row();
				$special_amount = $row['special_amount'];
				$total = $row['total'];
				text_cells_cost(null, 'special_cost_ref_'.$i, $row['special_cost_ref'], 18, null, null, null, null,"class='f_in' id=special_cost_ref_".$i,"89");
				text_cells_cost(null, 'special_frate_'.$i, $row['special_rate'], 10, 10, null, null, null,"class='f_in' id=special_frate_".$i,"89");
				text_cells_cost(null, 'special_famount_'.$i, $special_amount, 10, 10, null, null, null,"class='f_in' id=special_famount_".$i,"89");
				text_cells_cost(null, 'special_ftotal_'.$i, $total, 10, 10, null, null, null,"class='f_in' id=special_ftotal_".$i,"89");
			end_row();
			$total_finishing_special_cost += $total;
			$i++;
		}
		
				
			
		end_table();
		text_cells_cost(null, 'total_finishing_special_cost', $total_finishing_special_cost, 11, 10, null, null, null,"readonly id=total_finishing_special_cost","97");

	echo "</td>";
		text_cells_cost(null, 'other_cost', $other_cost, 10, 10, null, null, null,"id=other_cost","90");
		$total_finishing_cost = $other_cost + $total_finishing_special_cost + $total_finishing;		
		//text_cells(null, 'total_finishing_cost', $total_finishing_cost, 10, 10, null, null, null,"id=total_finishing_cost");		
		label_cell($total_finishing_cost,"width=111","total_finishing_cost_label");	
		echo '<input type="hidden" readonly name="total_finishing_cost" id="total_finishing_cost" value="'.$total_finishing_cost.'" />';		


	/*end_row();
	end_table();*/
}


function display_final_costing($finish_code_id)
{	
	/*start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Final Profit %"),_("Final Profit"),_("Total %"),_("Total Export Cost"),_("Total FOB Cost"),_("Finishing EXP %"),_("Final Finishing Cost"),_("SP By Process"), _("SP By Lumsum"), _("Final Total COst"), _("Currency Rates"));            
	table_header($th);
	start_row();*/
	
	
	$sql = "select * from ".TB_PREF."cust_final_costing where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve finishing_cost ###edit_cost");

	while($row = db_fetch($result))
	{
		$total_amount = $row['total_amount'];
		$final_profit_percent = $row['final_profit_percent'];
		$final_profit = $row['final_profit'];
		$total_percent = $row['total_percent'];
		$total_export_cost = $row['total_export_cost'];
		$total_fob_cost = $row['total_fob_cost'];
		$finishing_exp_percent = $row['finishing_exp_percent'];
		$final_finishing_cost = $row['final_finishing_cost'];
		$sp_by_process = $row['sp_by_process'];
		$sp_by_lumsum = $row['sp_by_lumsum'];
		$final_total_cost = $row['final_total_cost'];
		text_cells_cost(null, 'final_profit_percent', $final_profit_percent, 10, 10, null, null, null,"id=final_profit_percent","90");		
		text_cells_cost(null, 'final_profit', $final_profit, 10, 10, null, null, null,"id=final_profit","90");		
		text_cells_cost(null, 'total_percent', $total_percent, 10, 10, null, null, null,"readonly id=total_percent","90");		
		text_cells_cost(null, 'total_export_cost', $total_export_cost, 10, 10, null, null, null,"readonly id=total_export_cost","90");		
		text_cells_cost(null, 'total_fob_cost', $total_fob_cost, 10, 10, null, null, null,"readonly id=total_fob_cost","90");		
		text_cells_cost(null, 'finishing_exp_percent', $finishing_exp_percent, 10, 10, null, null, null,"id=finishing_exp_percent","90");		
		text_cells_cost(null, 'final_finishing_cost', $final_finishing_cost, 13, 10, null, null, null,"id=final_finishing_cost","107");		
		text_cells_cost(null, 'sp_by_process', $sp_by_process, 10, 10, null, null, null,"readonly id=sp_by_process","90");
		text_cells_cost(null, 'sp_by_lumsum', $sp_by_lumsum, 10, 10, null, null, null,"readonly id=sp_by_lumsum","90");
		text_cells_cost(null, 'final_total_cost', $final_total_cost, 10, 10, null, null, null,"readonly id=final_total_cost","90");
		$currency = $row['currency_price'];		
	}

	echo "<td width='900' class='curre_div'>";
	start_table();
	start_row();
		$sql = "select * from ".TB_PREF."currency_master";
		$result = db_query($sql, "Could not get currencies.");
		$currencies = array();
		$total_currency = 0;
		while($row = db_fetch($result))
		{
			
			$aaa = html_entity_decode($currency);
			$currency_arr = json_decode($aaa,true);
			
			$id = $row['id'];
			foreach($currency_arr as $key=>$curr)
			{
				foreach($curr as $key1=>$curr1)
				{
					$curr_name = get_currency_name($key1);
					if($key1 == $id)
					{
							$curr2 = $curr1;
					}
				}
			}
			
			$currencies[] = $id;
			
			label_cell("Price in: ".$row['currency_name'],"colspan=3 align=right");
			text_cells_cost(null, "currency".$id,$curr2, 10, 10, null, null, null,"readonly id=currency".$id,"89");	
			
			$total_currency++;
		}
	end_row();
	$currency = implode(",",$currencies);
	echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
	echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	end_table();
	echo "</td>";
		
	/*end_row();
	
	end_table();*/
}


function display_costing_details($customer_id)
{
	
	/*$sql = "select ref_id form ".TB_PREF."customer_reletion_product where ";
	$result= db_query($sql,"not retrive");
*/

	div_start("display_costing");
	echo "<div id='inner_div'>";
	start_table(TABLESTYLE, "width=5815");
echo "<input type='hidden' name='customer_id' id='customer_id' value='".$customer_id."' >";
	echo "<tr class='tableheader'><td width='250' >Product Details</td><td width='154' >Wood</td><td width='89' >CFT</td><td width='89' >Rate</td><td width='94' >Amount</td><td  width='89' >Total Wood Cost</td><td  width='99' >Basic Labour Price</td><td width='149'>Labour Type</td><td width='89'>Labour Rate</td><td width='89'>Labour Amount</td><td width='94'>Labour Cost</td><td width='142'>Total Special Labour Cost</td><td width='96'>Total Labour Cost</td><td width='89'>Extra %</td><td width='89'>Extra Amount</td><td width='89'>Total Cost</td><td width='89'>Admin %</td>
			<td width='89'>Admin Cost</td><td width='89'>Total CP</td><td width='89'>Profit %</td><td width='89'>Profit</td><td width='89'>Total MFRG Cost</td><td width='89'>Sanding %</td>
			<td width='89'>Sanding Cost</td><td width='89'>Polish %</td><td width='89'>Polish Cost</td><td width='89'>Packaging %</td><td width='89'>Packaging Cost</td><td width='89'>Forwarding %</td><td width='89'>Forwarding Cost</td><td width='138'>Special Cost Reference</td><td width='89'>Special Rate</td><td width='89'>Amount</td><td width='89'>Special Cost</td><td width='96'>Total Special Cost</td><td width='89'>Other Cost</td><td width='110'>Total Finishing Cost</td>
			<td width='89'>Proft %</td><td width='89'>Final Profit</td><td width='89'>Total %</td><td width='89'>Totla Expot Cost</td><td width='89'>Total FOB Cost</td><td width='89'>Finishing EXP%</td><td width='102'>Final Finishing Cost</td><td  width='89'>SP by Process</td><td  width='89'>SP By Lumsum</td><td width='89'>Final Total Cost</td><td width='900'>Currency Rate</td> </tr>";      


	end_table();
	echo "</div>";
	echo "<div id='inner_table'>";
	echo "<div id='inner_div2'>";
	echo '<input type="hidden" id="reference_id" value="'.$ref_id.'" >';
	start_table(TABLESTYLE, "id='product_table' width='5804'");
	$th = array(_("Product Details"),_("Manufacturing Cost"), _("Finishing Cost"), _("Final Cost"));

			
			$finish_code_id = $row1['finish_pro_id'];
			$sql = "select finish_code_id from ".TB_PREF."cust_final_costing where customer_id = ".db_escape($customer_id);
			$res1 = db_query($sql, "could not match with final costing");
			if(db_num_rows($res1) > 0)
			{
				while($row12 = db_fetch($res1))
				{
					$finish_code_id = $row12['finish_code_id'];
					$product = get_product_details($finish_code_id);
					echo "<tr class='finish_product' id='".$finish_code_id."'>";
					echo '<td class="pro_details" width="251" ><table><tr><td class="tableheader">Product Name</td><td>';
					echo '<input type="hidden" name="finish_code_id" value="'.$finish_code_id.'" >';
					echo $product['finish_product_name'].'</td></tr>';
					echo '<tr><td class="tableheader">Finish Code</td><td>'.$product['finish_comp_code'].'</td></tr></table></td>';
					echo '<td colspan="4" width="436"  class="wood_cost">';
					$sql = "select * from ".TB_PREF."cust_manufacturing_cost where finish_code_id = ".db_escape($finish_code_id);
					$result = db_query($sql, "Could not retrieve manufacturing_cost ###edit_cost");
					while($row = db_fetch($result))
					{
						$mfrg_id = $row['id'];
						display_wood_costing($mfrg_id);
						$total_wood_cost = $row['total_wood_cost'];
						$total_labour_cost = $row['total_labour_cost'];
					}
						//display_mfrg_cost($finish_code_id);
					  echo ' </td>';
					  echo "<td width='89'>";
					  echo '<input readonly="readonly" id="total_wood_cost" type="text" name="total_wood_cost" size="10" maxlength="10" value="'.$total_wood_cost.'">';
					  
					  echo "</td>";

					  echo "<td width='100'>";
					  $basic_price = basic_labour_price($mfrg_id);
					  echo '<input width="104" id="basic_price" type="text" name="basic_price" size="11" maxlength="10" value="'.$basic_price.'" >';
					  echo "</td>";


					  echo '<td colspan="4" width="431" class="special_labour_cost">';
					  display_special_labour_costing($mfrg_id, $basic_price);
					  echo '</td>';

					  echo "<td width='96'>";
							echo '<input readonly="readonly" id="total_labour_cost" type="text" name="total_labour_cost" size="11" maxlength="10" value="'.$total_labour_cost.'">';
					  echo '</td>';    
			
					  display_mfrg_cost($finish_code_id);
					  
						display_finishing_cost($finish_code_id);
					
						display_final_costing($finish_code_id);
					  
					echo "</tr>";
				}
			}
			else
			{
				$product = get_product_details($finish_code_id);
				echo "<tr class='finish_product' id='".$finish_code_id."'>";
				echo '<td  width="250" ><table><tr><td class="tableheader">Product Name</td><td>';
				echo '<input type="hidden" name="finish_code_id" value="'.$finish_code_id.'" >';
				echo $product['finish_product_name'].'</td></tr>';
				echo '<tr><td class="tableheader">Finish Code</td><td>'.$product['finish_comp_code'].'</td></tr></table></td>';
				
				echo "<td colspan='100'><h1>Costing is not entered for this product</h1></td></tr>";
			}
	
	end_table();
    echo "</div></div>";
	div_end();
	
	div_start();
	echo "<div id='lock_popup'>
			<div id='save_list_content'></div><div style='margin:auto; text-align:center;'>
			<h2>Are you sure to lock this price?</h2>		
				<div id='controls'><span id='close'><a href='lock_price.php?ref_id=".$ref_id."' >YES</a></span><span id='no'>NO</span> </div>
			</div>
			
		  </div>";
	//echo '<center><a id="lock" style="font-size:15px; width:100px;" href="#lock_popup" >Lock Price List</a></center>';
	
	div_end();
	
}


?>
