<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/manufacturing.inc");

function exchange_bpc_form($bpcName, $bpc)
{
	global $Ajax;
	div_start("popup_div");
		display_heading("Exchange Product from ".$bpcName);
		div_start("popup");
		hidden("exchange_from_bpc", $bpc);
			start_table(TABLESTYLE, "");

			$th = array(_("Select Product"), _("Select BPC"),_("Quantity"), _(" "));
			table_header($th, "style='text-align: left;border:1px solid #cccccc'");
			
			start_row();
			echo "<td><select name='exchange_pro_id' id='exchange_pro_id' >
					<option value='-1'>- Select Product -</option>";
				for($i = 1; $i <=$_POST['bpc_pro_'.$bpc]; $i++)
				{
					echo "<option value='".$_POST["pro_id_bpc_".$bpc."_".$i]."' > "
							.$_POST["pro_name_bpc_".$bpc."_".$i]." - ".$_POST["pro_code_bpc_".$bpc."_".$i]
						."</option>";
				}
			echo "</select></td>";

			echo "<td><select name='exchange_to_bpc' id='exchange_to_bpc' >
					<option value='-1'>- Select BPC -</option>";
				for($i = 1; $i <= $_POST['total_no_bpcs']; $i++)
				{
					if($i==$bpc)
						continue;
					echo "<option value='".$i."' > ".$_POST["bpc_name_".$i]."</option>";
				}
			echo "</select></td>";
			text_cells(NULL, "exchange_qty", NULL, 10, "", false, false, "", "id=exchange_qty");
	
			end_row();

			end_table();

		div_end();
		
		br();
		div_start("popup_controls");
			echo "<input type='button' name='ExchangeProduct' value='Exchange' onclick=$(this).exchangePro();>";
			submit_center_last('CancelExchange', "Cancel",	_('Cancels product exchange within BPCs'), true);
		div_end();
		$Ajax->addFocus("popup_div");
	div_end();	  
}

/*Exchange product cbm wise */
function exchange_cbm_form($bpcName, $bpc)
{
	global $Ajax;
	div_start("popup_div");
		display_heading("Exchange Product from ".$bpcName);
		div_start("popup");
		hidden("exchange_from_bpc", $bpc);
			start_table(TABLESTYLE, "");

			$th = array(_("Select Product"), _("Select BPC"),_("Quantity"), _(" "));
			table_header($th, "style='text-align: left;border:1px solid #cccccc'");
			
			start_row();
			echo "<td><select name='exchange_pro_id' id='exchange_pro_id' >
					<option value='-1'>- Select Product -</option>";
				for($i = 1; $i <=$_POST['bpc_pro_'.$bpc]; $i++)
				{
					echo "<option value='".$_POST["pro_id_bpc_".$bpc."_".$i]."' > "
							.$_POST["pro_name_bpc_".$bpc."_".$i]." - ".$_POST["pro_code_bpc_".$bpc."_".$i]
						."</option>";
				}
			echo "</select></td>";

			echo "<td><select name='exchange_to_bpc' id='exchange_to_bpc' >
					<option value='-1'>- Select BPC -</option>";
				for($i = 1; $i <= $_POST['total_no_bpcs']; $i++)
				{
					if($i==$bpc)
						continue;
					echo "<option value='".$i."' > ".$_POST["bpc_name_".$i]."</option>";
				}
			echo "</select></td>";
			text_cells(NULL, "exchange_qty", NULL, 10, "", false, false, "", "id=exchange_qty");
	
			end_row();

			end_table();

		div_end();
		
		br();
		div_start("popup_controls");
			echo "<input type='button' name='ExchangeProduct' value='Exchange' onclick=$(this).exchangePro();>";
			submit_center_last('CancelExchange', "Cancel",	_('Cancels product exchange within BPCs'), true);
		div_end();
		$Ajax->addFocus("popup_div");
	div_end();	  
}


function bpc_form()
{
	global $Ajax;
	$Ajax->activate('bpc_form');
	start_table(TABLESTYLE, "style=width:50%", 10);

		start_row();
		label_cell("BPC Creation Type","class=label");
		echo "<td>";

		$items = array("auto" => "Auto", 'manual' => "Manual");
		echo array_selector("creationType", $_POST['creationType'], $items, array( 'select_submit'=> true ) );

		echo "</td>\n";
		end_row();
		if($_POST["creationType"] != "manual"){
			text_row(_("Enter No. of BPCs:"), 'no_of_bpc', $_POST['no_of_bpc'], 25, 25, _('Enter the no. of BPCs you want to create.'));
		}
		
		bpc_mode_list_row("BPC Mode", "bpc_mode", $_POST['bpc_mode'], "Quantity Mode", "CBM Mode", "Container Mode", true);
	end_table(1);
	submit_center("createBPC", "Show BPCs", true, "Create BPCs", true, ICON_SUBMIT);
}

function display_bpc_pro($pro, $bpc, $pro_no)
{
	label_cell($pro['finish_product_name']);hidden("pro_id_bpc_".$bpc."_".$pro_no, $pro['finish_pro_id']);
		hidden("pro_name_bpc_".$bpc."_".$pro_no, $pro['finish_product_name']);

	label_cell($pro['finish_comp_code']);
		hidden("pro_code_bpc_".$bpc."_".$pro_no, $pro['finish_comp_code']);

	text_cells(NULL, "pro_qty_bpc_".$bpc."_".$pro_no, $pro['quantity'], 10, "", false, false, "", "readonly");

	if($_POST['bpc_mode'] == "cbm_mode")
	{
		text_cells(NULL, "cbm_per_pro_bpc_".$bpc."_".$pro_no, $pro['pro_cbm'], 10, "", false, false, "", "readonly");
		text_cells(NULL, "pro_cbm_bpc_".$bpc."_".$pro_no, ($pro['quantity']*$pro['pro_cbm']), 10, "", false, false, "", "readonly");

	}
}

function display_bpc_cbm($pro, $bpc, $pro_no)
{
	label_cell($pro['pro_cbm']);hidden("cbm_per_pro_bpc_".$bpc."_".$pro_no, $pro['pro_cbm']);
	label_cell(($pro['quantity']*$pro['pro_cbm']));hidden("pro_cbm_bpc_".$bpc."_".$pro_no, ($pro['quantity']*$pro['pro_cbm']));
}

function quantity_wise_bpc($no_of_bpc)
{
	global $Ajax;

	$total_products = 0;
	foreach ($_SESSION['Items']->pro as $pro) {
		$total_products += $pro['quantity'];
	}

	if($total_products < $no_of_bpc)
	{
		display_error("Enter less no. of BPCs. Number of cannot be more than Products ");
		return true;
	}

	$pro_per_bpc = floor($total_products/$no_of_bpc);
	$pro_last_bpc = $total_products%$no_of_bpc;
	if($pro_last_bpc != 0)
	{
		$pro_last_bpc += $pro_per_bpc;
	}

	hidden("total_no_bpcs", $no_of_bpc);
	$order_products = $_SESSION['Items']->pro;
	for($i = 1; $i <= $no_of_bpc; $i++)
	{
		$bpc = $i; $pro_no = 1;
		if($i%2 != 0 OR $i == 1)
		{
			start_table(TABLESTYLE, "style=width:auto;", 10);
			start_row();
		}
		echo "<td>";

		$bpcName = "BPC-".key($_SESSION['Items']->trans_no)."-".$bpc;

		$exchange = find_submit('Exchange');
		div_start("ExchangeBPC".$bpc);
		if ($exchange != -1 && $exchange == $bpc)
		{
			$Ajax->activate("ExchangeBPC".$exchange);
			exchange_bpc_form($bpcName, $bpc);
			set_focus('ExchangeProduct');
		}
		echo button("Exchange$bpc", _("Exchange Quantity"), _('Edit document line'), false);
		div_end();

		if (isset($_POST['CancelExchange']))
		{
			$Ajax->activate("ExchangeBPC".$bpc);
		}

		start_table(TABLESTYLE, "style=width:500px; id=bpc".$bpc, 10);

		hidden("bpc_name_".$bpc, $bpcName);
		table_section_title($bpcName, 3);

		$th = array(_("Product Name"), _("Finish Code"),_("Quantity"));
		table_header($th, "style='text-align: left;border:1px solid #cccccc'");

		$pro_in_this_bpc = 0;
		$pro_can_come = 0;

		if($i < $no_of_bpc)
		{
			foreach ($order_products as $key => $pro) 
			{	
				start_row();
				if($pro_in_this_bpc < $pro_per_bpc)
				{
					$pro_can_come = $pro_per_bpc - $pro_in_this_bpc;	
					
					if($pro_can_come >= $pro['quantity'])
					{
						$pro_in_this_bpc += $pro['quantity'];
						display_bpc_pro($pro, $bpc, $pro_no);
						unset($order_products[$key]);
					}
					else
					{
						$remaining_qty = $pro['quantity'] - $pro_can_come;
						$pro['quantity'] = $pro_can_come;
						$pro_in_this_bpc += $pro_can_come;
						display_bpc_pro($pro, $bpc, $pro_no);
						$order_products[$key]['quantity'] = $remaining_qty;
					}
				}
				else
				{
					break;
				}

				end_row();
				$pro_no++;//Increament Product No.
			}
		}
		elseif($i == $no_of_bpc)
		{
			foreach ($order_products as $key => $pro) 
			{
				start_row();
				display_bpc_pro($pro, $bpc, $pro_no);
				unset($order_products[$key]);
				end_row();
				$pro_no++;//Increament Product No.
			}
		}
		end_table();

		echo "</td>";
		if($i%2 == 0)
		{
			end_row();
			end_table();
		}

		hidden("bpc_pro_".$bpc, ($pro_no-1));		
	}
}

function cbm_wise_bpc($no_of_bpc)
{
	global $Ajax;
	$total_cbm = 0;

	echo "<pre>";
	///print_r($_SESSION['Items']->pro);
	echo "</pre>";

	foreach ($_SESSION['Items']->pro as $pro) {
		$total_cbm += ($pro['quantity']*$pro['pro_cbm']);
	}

	$cbm_per_bpc = floor($total_cbm/$no_of_bpc);


	$cbm_last_bpc = fmod ($total_cbm , $no_of_bpc );

	if($cbm_last_bpc != 0)
	{
		$cbm_last_bpc += $cbm_per_bpc;
	}

	hidden("total_no_bpcs", $no_of_bpc);
	$order_products = $_SESSION['Items']->pro;
	for($i = 1; $i <= $no_of_bpc; $i++)
	{
		$bpc = $i; $pro_no = 1;
		if($i%2 != 0 OR $i == 1)
		{
			start_table(TABLESTYLE, "style=width:auto;", 10);
			start_row();
		}
		echo "<td>";

		$bpcName = "BPC-".key($_SESSION['Items']->trans_no)."-".$bpc;

		$exchange = find_submit('Exchange');
		div_start("ExchangeBPC".$bpc);
		if ($exchange != -1 && $exchange == $bpc)
		{
			$Ajax->activate("ExchangeBPC".$exchange);
			exchange_cbm_form($bpcName, $bpc);
			set_focus('ExchangeProduct');
		}
		echo button("Exchange$bpc", _("Exchange Quantity"), _('Edit document line'), false);
		div_end();

		if (isset($_POST['CancelExchange']))
		{
			$Ajax->activate("ExchangeBPC".$bpc);
		}


		start_table(TABLESTYLE, "style=width:500px; id=bpc".$bpc, 10);

		$bpcName = "BPC-".key($_SESSION['Items']->trans_no)."-".$bpc;
		hidden("bpc_name_".$bpc, $bpcName);
		table_section_title($bpcName, 5);

		$th = array(_("Product Name"), _("Finish Code"),_("Quantity"), _("CBM per Product"), _("CBM"));
		table_header($th, "style='text-align: left;border:1px solid #cccccc'");

		$cbm_in_this_bpc = 0;
		$cbm_can_come = 0;

		if($i < $no_of_bpc)
		{
			foreach ($order_products as $key => $pro) 
			{	
				start_row();
				if($cbm_in_this_bpc < $cbm_per_bpc)
				{
					$cbm_can_come = $cbm_per_bpc - $cbm_in_this_bpc;	
					$pro_cbm = ($pro['quantity']*$pro['pro_cbm']);

					if($cbm_can_come >= $pro_cbm)
					{
						$cbm_in_this_bpc += $pro_cbm;
						display_bpc_pro($pro, $bpc, $pro_no);
						unset($order_products[$key]);
					}
					else
					{
						$req_quantity = ceil($cbm_can_come / $pro['pro_cbm']);
						$remaining_qty = $pro['quantity'] - $req_quantity;

						$pro['quantity'] = $req_quantity;
						$cbm_came = $pro['quantity']*$pro['pro_cbm'];
						$cbm_in_this_bpc += $cbm_came;
						display_bpc_pro($pro, $bpc, $pro_no);
						$order_products[$key]['quantity'] = $remaining_qty;
						
						if($req_quantity == $pro['quantity'])
						{
							//unset($order_products[$key]);
						}

						if($remaining_qty == 0)
						{
							unset($order_products[$key]);
						}

					}
				}
				else
				{
					break;
				}

				end_row();
				$pro_no++;//Increament Product No.
			}
		}
		elseif($i == $no_of_bpc)
		{
			foreach ($order_products as $key => $pro) 
			{
				start_row();
					$cbm_in_this_bpc += $pro['quantity']*$pro['pro_cbm'];
					display_bpc_pro($pro, $bpc, $pro_no);
					unset($order_products[$key]);
				end_row();
				$pro_no++;//Increament Product No.
			}
		}

		start_row();
			label_cell("Total CBM", "colspan=4 class=tableheader align=left");
			//label_cell($cbm_in_this_bpc, "class=tableheader");
			text_cells(NULL, "total_cbm_bpc_".$bpc, $cbm_in_this_bpc, 10, "", false, false, "", "readonly class=tableheader");
		end_row();

		end_table();

		echo "</td>";
		if($i%2 == 0)
		{
			end_row();
			end_table();
		}

		hidden("bpc_pro_".$bpc, ($pro_no-1));		
	}
}


/*manual BPC*/
function displayManualBPC($bpc)
{	
	global $Ajax;
	
	div_start("manualbpc".$bpc);
	$Ajax->activate("manualbpc".$bpc);

	start_table(TABLESTYLE, "style=width:500px; id=bpc".$bpc, 10);

	$bpcName = "BPC-".key($_SESSION['Items']->trans_no)."-".$bpc;
	hidden("bpc_name_".$bpc, $bpcName);
	table_section_title($bpcName, 5);

	$th = array(_("Product Name"), _("Finish Code"),_("Quantity"));

	if($_POST['bpc_mode'] == "cbm_mode")
	{
		array_push($th, _("CBM per Product"));
		array_push($th, _("CBM"));
	}

	table_header($th, "style='text-align: left;border:1px solid #cccccc'");

	$i = 1;
	foreach ($_SESSION['Items']->pro as $pro) {

		start_row();
		label_cell($pro['finish_product_name']);hidden("pro_id_bpc_".$bpc."_".$i, $pro['finish_pro_id']);
			hidden("pro_name_bpc_".$bpc."_".$i, $pro['finish_product_name']);

		label_cell($pro['finish_comp_code']);
			hidden("pro_code_bpc_".$bpc."_".$i, $pro['finish_comp_code']);

		$proConsumed = 0;
		for($k = 1; $k <= $_POST['total_no_bpcs']; $k++)
		{
			$proConsumed += $_POST["pro_qty_bpc_$k"."_$i"];
		}
		if($proConsumed > $pro['quantity']){
			/*echo "<pre>";print_r($_POST);*/
			if (isset($_POST["_pro_qty_bpc_$bpc"."_$i"."_changed"])){
				$_POST["pro_qty_bpc_".$bpc."_".$i] = 0;
				//set_focus("pro_qty_bpc_".$bpc."_".$i);
				set_focus("customer_id");
				display_error("Please Enter Less Quantity. Total Quantity of ".$pro['finish_product_name']." is ".$pro['quantity']);
			}
		}
		text_cells_ex(NULL, "pro_qty_bpc_".$bpc."_".$i, 10, null, 0, "Product Quantity", null, $post_label=null, true, "id=pro_qty_bpc_".$bpc."_".$i);


		if($_POST['bpc_mode'] == "cbm_mode"){

			text_cells(NULL, "cbm_per_pro_bpc_".$bpc."_".$i, $pro['pro_cbm'], 10, "", false, false, "", "readonly");
			$_POST["pro_cbm_bpc_".$bpc."_".$i] = $_POST["pro_qty_bpc_".$bpc."_".$i]*$pro['pro_cbm'];
			text_cells(NULL, "pro_cbm_bpc_".$bpc."_".$i, $_POST["pro_cbm_bpc_".$bpc."_".$i], 10, "", false, false, "", "readonly");
		}
		end_row();
		$i++;
	}
	hidden("bpc_pro_$bpc", $i-1);
	start_row();
	button_cell("nextBPC$bpc", _("Create Another New BPC"), _('Create Another New BPC'), false);
	
	if($bpc == $_POST['total_no_bpcs'] && $bpc != 1)
		button_cell("cancelBPC$bpc", _("Cancel This BPC"), _('Cancel This BPC'), false);

	if($bpc == $_POST['total_no_bpcs'])
		hidden("total_no_bpcs", $bpc);
	elseif (!isset($_POST['total_no_bpcs']) OR $_POST['total_no_bpcs'] == 1) {
				hidden("total_no_bpcs", $bpc);
			}

	end_row();
	end_table();

	div_end();
	br();
}

function display_bpcs()
{
	global $Ajax;
	//echo "<pre>";
	$no_of_bpc = $_POST['no_of_bpc'];
	$bpc_mode = $_POST['bpc_mode'];
	$creationType = $_POST['creationType'];

	div_start("bpcs");

	if($creationType == "auto")
	{
		if($bpc_mode == "quantity_mode")
			quantity_wise_bpc($no_of_bpc);
	
		if($bpc_mode == "cbm_mode")
			cbm_wise_bpc($no_of_bpc);
	
		if($bpc_mode == "container_mode")
			container_wise_bpc($no_of_bpc);
	}
	else{

		div_start("ManualBPC");

		$nextBPC = find_submit('nextBPC');
		if ($nextBPC != -1)
			$_POST['total_no_bpcs'] += 1;

		$cancelBPC = find_submit('cancelBPC');
		if ($cancelBPC != -1)
			$_POST['total_no_bpcs'] -= 1;

		displayManualBPC(1);	

		for ($i=2; $i <= $_POST['total_no_bpcs']; $i++) 
		{ 
			if ($cancelBPC != -1 && $cancelBPC == $i)
				continue;

			div_start("manualbpc".$i);
			$Ajax->activate("ManualBPC");
			displayManualBPC($i);
			div_end();
		}
		
		div_end();
	}

	div_end();
}

function check_bpc_created($orderId, $trans_type)
{
	global $Ajax;
	$sql = "select bpcCreated from ".TB_PREF."sales_orders where order_no = ".db_escape($orderId)." && trans_type = ".db_escape($trans_type);
	$result = db_query($sql, "Could not check BPC status of Order.");
	$bpcCreated = db_fetch($result);
	$bpcCreated['bpcCreated'];
	if($bpcCreated['bpcCreated'] == "1")
	{
		display_error("BPCs already created for this sales order #".$orderId);
		$Ajax->activate('_page_body');
		display_footer_exit();
	}
}

?>