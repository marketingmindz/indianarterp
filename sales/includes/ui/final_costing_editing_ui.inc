<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


function display_final_costing_form(&$order, $editable=true)
{
	if(!isset($_POST['final_profit_percent']))
		$_POST['final_profit_percent'] = 0;
	if(!isset($_POST['final_profit']))
		$_POST['final_profit'] = 0;
	if(!isset($_POST['total_percent']))
		$_POST['total_percent'] = 0;
	if(!isset($_POST['total_export_cost']))
		$_POST['total_export_cost'] = 0;
	if(!isset($_POST['total_fob_cost']))
		$_POST['total_fob_cost'] = 0;
	if(!isset($_POST['finishing_exp_percent']))
		$_POST['finishing_exp_percent'] = 0;
	if(!isset($_POST['final_finishing_cost']))
		$_POST['final_finishing_cost'] = 0;
	if(!isset($_POST['sp_by_process']))
		$_POST['sp_by_process'] = 0;
	if(!isset($_POST['sp_by_lumsum']))
		$_POST['sp_by_lumsum'] = 0;
	if(!isset($_POST['total_cost']))
		$_POST['total_cost'] = 0;
		
	
	div_start('final_table');
	global 	$Ajax;
    $Ajax->activate('final_table');
	
	calculate_final_cost();
	
	display_heading("Final Costing");
	start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_(" "),_(" "),_(" "),_(" "));
		table_header($th);
		//$_POST['total_mfrg_cost'] = get_total_mfrg_cost($_POST['final_finish_code_id']);
		//$_POST['total_finishing_cost'] = get_total_finishing_cost($_POST['final_finish_code_id']);
		//$_POST['total_amount'] = $_POST['total_mfrg_cost'] + $_POST['total_finishing_cost'];


	start_row();
	    label_cell("Total Amount : ","colspan=3 align=right");
     	label_cell($_POST['total_amount'],null,"total_amount_label");	
		echo '<input type="hidden" name="total_amount" id="total_amount" value="'.$_POST['total_amount'].'" />';	
	end_row();
	start_row();
	    label_cell("Profit % : ","colspan=3 align=right");
     	text_cells(null, 'final_profit_percent', $_POST['final_profit_percent'], 10, 10, null, null, null,"id=final_profit_percent");	
	end_row();
	start_row();
	    label_cell("Profit : ","colspan=3 align=right");
		text_cells(null, 'final_profit', $_POST['final_profit'], 10, 10, null, null, null,"id=final_profit");
	end_row();
	
		start_row();
		echo '<input type="hidden" name="old_total_percent" id="old_total_percent" value="'.$_POST['total_percent'].'">';
	    label_cell("Total % : ","colspan=3 align=right");
     	text_cells(null, 'total_percent', $_POST['total_percent'], 10, 10, null, null, null,"id=total_percent");	
	end_row();
	start_row();
	echo '<input type="hidden" name="old_total_export_cost" id="old_total_export_cost" value="'.$_POST['total_export_cost'].'">';
	    label_cell("Total Export Cost : ","colspan=3 align=right");
     	text_cells(null, 'total_export_cost', $_POST['total_export_cost'], 10, 10, null, null, null,"id=total_export_cost");	
	end_row();
	
	$_POST['total_fob_cost'] = $_POST['total_mfrg_cost'] + $_POST['total_export_cost'];
	start_row();
	    echo '<input type="hidden" name="old_total_fob" id="old_total_fob">';
	    label_cell("Total FOB Cost (Process Method) : ","colspan=3 align=right");
     	text_cells(null, 'total_fob_cost', $_POST['total_fob_cost'], 10, 10, null, null, null,"id=total_fob_cost");		
	end_row();
	start_row();
	    label_cell("Finishing EXP% : ","colspan=3 align=right");
     	text_cells(null, 'finishing_exp_percent', $_POST['finishing_exp_percent'], 10, 10, null, null, null,"id=finishing_exp_percent");	
	end_row();
	start_row();
	    label_cell("Finishing Cost : ","colspan=3 align=right");
     	text_cells(null, 'final_finishing_cost', $_POST['final_finishing_cost'], 10, 10, null, null, null,"id=final_finishing_cost");		
	end_row();
	start_row();
	    label_cell("SP By Process : ","colspan=3 align=right");
     	text_cells(null, 'sp_by_process', $_POST['sp_by_process'], 10, 10, null, null, null,"id=sp_by_process");	
	end_row();
	start_row();
	    label_cell("SP By Lumsum : ","colspan=3 align=right");
     	text_cells(null, 'sp_by_lumsum', $_POST['sp_by_lumsum'], 10, 10, null, null, null,"id=sp_by_lumsum");	
	end_row();
	start_row();
	    label_cell("Total Costs : ","colspan=3 align=right");
     	text_cells(null, 'final_total_cost', $_POST['final_total_cost'], 10, 10, null, null, null,"id=final_total_cost");	
	end_row();
	
	$sql = "select * from ".TB_PREF."currency_master";
	$result = db_query($sql, "Could not get currencies.");
	$currencies = array();
	$total_currency = 0;
	while($row = db_fetch($result))
	{
		$currency = $_POST['currency_price'];
		$aaa = html_entity_decode($currency);
		$currency_arr = json_decode($aaa,true);
		
		$id = $row['id'];
		foreach($currency_arr as $key=>$curr)
		{
			foreach($curr as $key1=>$curr1)
			{
				$curr_name = get_currency_name($key1);
				if($key1 == $id)
				{
					if(!isset($_POST['currency'.$id]))
						$_POST['currency'.$id] = $curr2 = $curr1;
				}
			}
		}
		
		$currencies[] = $id;
		start_row();
	    label_cell("Price in: ".$row['currency_name'],"colspan=3 align=right");
     	text_cells(null, "currency".$id, $_POST['currency'.$id], 10, 10, null, null, null,"id=currency".$id);	
		end_row();
		$total_currency++;
	}
	
	$currency = implode(",",$currencies);
	echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
	echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	
	end_table();
 	
}



function unset_final_cost_variables()
{
	unset($_POST['final_profit_percent']);
	unset($_POST['final_profit']);
	unset($_POST['total_percent']);
	unset($_POST['total_export_cost']);
	unset($_POST['total_fob_cost']);
	unset($_POST['finishing_exp_percent']);
	unset($_POST['final_finishing_cost']);
	unset($_POST['sp_by_process']);
	unset($_POST['sp_by_lumsum']);
	unset($_POST['final_total_cost']);
	unset($_POST['price_in_inr']);
}


function display_final_costing($finish_code_id)
{
	div_start('final_table');
	display_heading("Final Costing");
	start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_(" "),_(" "),_(" "),_(" "));
		table_header($th);
	$sql = "select * from ".TB_PREF."final_costing where finish_code_id =".$finish_code_id;
	$result = db_query($sql, "Could not get final cost");
	while($row = db_fetch($result))
	{
		start_row();
	    label_cell("Total MFRG Cost : ","colspan=3 align=right");
     	label_cell($row['total_mfrg_cost']);	
		end_row();
		start_row();
			label_cell("Total Finishing Cost : ","colspan=3 align=right");
			label_cell($row['total_finishing_cost']);	
		end_row();
		start_row();
			label_cell("Total Amount : ","colspan=3 align=right");
			label_cell($row['total_amount']);		end_row();
		start_row();
			label_cell("Profit % : ","colspan=3 align=right");
			label_cell($row['final_profit_percent']);
		end_row();
		start_row();
			label_cell("Profit : ","colspan=3 align=right");
			label_cell($row['final_profit']);
		end_row();
			
		start_row();
			label_cell("Total % : ","colspan=3 align=right");
			label_cell($row['total_percent']);
		end_row();
		start_row();
			label_cell("Total Export Cost : ","colspan=3 align=right");
			label_cell($row['total_export_cost']);
		end_row();
		start_row();
			label_cell("Total FOB Cost (Process Method) : ","colspan=3 align=right");
			label_cell($row['total_fob_cost']);
		end_row();
		start_row();
			label_cell("Finishing EXP% : ","colspan=3 align=right");
			label_cell($row['finishing_exp_percent']);
		end_row();
		start_row();
			label_cell("Finishing Cost : ","colspan=3 align=right");
			label_cell($row['final_finishing_cost']);
		end_row();
		start_row();
			label_cell("SP By Process : ","colspan=3 align=right");
			label_cell($row['sp_by_process']);
		end_row();
		start_row();
			label_cell("SP By Lumsum : ","colspan=3 align=right");
			label_cell($row['sp_by_lumsum']);
		end_row();
		start_row();
			label_cell("Total Costs : ","colspan=3 align=right");
			label_cell($row['final_total_cost']);
		end_row();
		
		$currency = $row['currency_price'];
		$aaa = html_entity_decode($currency);
		$currency_arr = json_decode($aaa,true);
		foreach($currency_arr as $key=>$curr)
		{
			foreach($curr as $key1=>$curr1)
			{
				$curr_name = get_currency_name($key1);
				start_row();
					label_cell("price in  : ".$curr_name,"colspan=3 align=right");
					label_cell($curr1);
				end_row();
			}
		}
	}
	end_table();

    div_end();
	
}

function read_final_costing($finish_code_id, $version)
{
	$sql = "select * from ".TB_PREF."final_costing where finish_code_id=".$finish_code_id." && version=".db_escape($version);
	$result = db_query($sql, "Could not read finishing cost.");
	if(db_num_rows($result) > 0)
	{
		while($row = db_fetch($result))
		{
			$_POST['total_mfrg_cost'] = $row['total_mfrg_cost'];
			$_POST['total_finishing_cost'] = $row['total_finishing_cost'];
			$_POST['total_amount'] = $row['total_amount'];
			$_POST['final_profit_percent'] = $row['final_profit_percent'];
			$_POST['final_profit'] = $row['final_profit'];
			$_POST['total_percent'] = $row['total_percent'];
			$_POST['total_export_cost'] = $row['total_export_cost'];
			$_POST['total_fob_cost'] = $row['total_fob_cost'];
			$_POST['finishing_exp_percent'] = $row['finishing_exp_percent'];
			$_POST['final_finishing_cost'] = $row['final_finishing_cost'];
			$_POST['sp_by_process'] = $row['sp_by_process'];
			$_POST['sp_by_lumsum'] = $row['sp_by_lumsum'];
			$_POST['final_total_cost'] = $row['final_total_cost'];
			$_POST['currency_price'] = $row['currency_price'];
		}
	}
	else
	{
		display_error("Finising Cost is not entered for this product.");
	}
}

?>
