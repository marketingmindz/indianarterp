<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


// costing module - multiple category list
function multiple_categories_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	$sql = "SELECT category_id, cat_name, cat_reference, inactive FROM 0_item_category order by cat_name ASC";
	$result = db_query($sql);
	echo '<div class="multiselect">
           <div class="selectBox" onclick="showCheckboxes()">';
	echo '<select autocomplete="off" class="combo" title="Select Category" id="slc_category" >
	  <option value="-1" >- select category - </option>';
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$selected = "";
		
		echo '<option value="'.$category_id.'">'.$row['cat_name'].'</option>';	
	}
	echo "</select>";
	echo '<div class="overSelect"></div>
        </div>
        <div id="checkboxes">';
	$result = db_query($sql);
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$checked = "";	
		if(in_array($category_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		
		echo '<label><input type="checkbox" value="'.$category_id.'" '.$checked.' name="category_id['.$category_id.']" />'.$row['cat_name'].'</label>';
	}	

    echo ' </div>
    </div>';
}
function multiple_category_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_categories_list($name, $selected_id, $spec_opt, $submit_on_change);
	echo "</td>\n";
}

function multiple_category_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_category_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change);
	echo "</tr>\n";
}



function display_header(){
	div_start('product', "style=height:100px;");
	start_table(TABLESTYLE_NOBORDER, "width=300px");
	global $Ajax;
	//$Ajax->activate('product');
	//$Ajax->activate('GetFinishProduct');
		multiple_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'));
		
		
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'), null, 'style=display:none');
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		echo "<td colspan='2'>";
		submit_center('GetFinishProduct', _("Submit"), true, '', 'default');
		echo "</td>";
		
	end_table();

	div_end();
	
}


//  Wood part //
function display_wood_summary(&$order,$editable=true)
{
    div_start('wood_table');
	display_heading("Wood Costing");
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Wood"),_("CFT"), _("Rate"), _("Amount"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
	unset($_SESSION['total_wood_cost']);
	global $total_wood_cost;
	$total_wood_cost = 0;
	$id = find_submit('Edit');
	$k = 0; 
	foreach ($order->line_items as $line_con => $con_line)
   	{
		$total_wood_cost += $con_line->amount;
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$wood_id = $con_line->wood_id;
			$get_wood_name = get_wood_name($wood_id);
			$wood_name = $get_wood_name['consumable_name'];
			
        	label_cell($wood_name);
    		label_cell($con_line->cft);
    		label_cell($con_line->rate);
			label_cell($con_line->amount);
    		
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		}
		else
		{
			design_consumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_consumable_item_controls($order, $k);
	start_row();
		$_POST['total_wood_cost'] = $total_wood_cost;
	    label_cell("Total Wood Cost : ","colspan=3 align=right");
     	label_cell($total_wood_cost);
		hidden('total_wood_cost', $total_wood_cost);	
	end_row();
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    
	
}


function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['wood_id'] = $order->line_items[$id]->wood_id;
		$_POST['cft'] = $order->line_items[$id]->cft;
		$_POST['rate'] = $order->line_items[$id]->rate;
		$_POST['amount'] = $order->line_items[$id]->amount;
		

	    $Ajax->activate('wood_table');
	}
	 
	wood_list_cells(null, 'wood_id', null, _('----Select---'));
	text_cells_ex(null, 'cft', 10, 10);
	
	text_cells_ex(null, 'rate', 10, 10);
	text_cells_ex(null, 'amount', 10, 10);
	if ($id != -1)
	{
		button_cell('WoodUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('WoodCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		//set_focus('amount');
	} 
	else 
		submit_cells('WoodAddItem', _("Add"), "colspan=2",
			_('Add new line to Wood Section'), true);
	end_row();
	
}

function display_design_consumables($finish_code_id, $mfrg_id)
{
	div_start("design_consumables");
	display_heading("Design Consumables");
	/*global $Ajax;
	$Ajax->activate('design_consumables');*/
	$sql = "select * from ".TB_PREF."man_design_cons_cost where mfrg_id=".$mfrg_id;	
	$result = db_query($sql, "cannot retrieve design consumables.");
	$i = 0;
	unset($_SESSION['design_consumables']);
	while($row = db_fetch($result))
	{
		$_SESSION['design_consumables'][$i] = $row;
		$i++;
	}
	$j = count($_SESSION['design_consumables']);
	$i = 0;
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("Unit"), _("Quantity"),_("Rate"), _("Cost"));
	table_header($th);
	$total_cons_cost = 0;
	foreach($_SESSION['design_consumables'] as $des_cons)
	{
		start_row();
		$cons_type = get_cons_type_name($des_cons['consumable_name']);
		$cons_select = get_cons_select_name($des_cons['consumable_category']);
		
		echo '<input type="hidden" name="des_master_name_'.$i.'" id="des_master_name_'.$i.'" value="'.$des_cons['consumable_name'].'" />';
		label_cell($cons_type['master_name']);
		echo '<input type="hidden" name="des_consumable_name_'.$i.'" id="des_consumable_name_'.$i.'" value="'.$des_cons['consumable_category'].'" />';
		label_cell($cons_select['consumable_name']);
		echo '<input type="hidden" name="des_cons_unit_'.$i.'" id="des_cons_unit_'.$i.'" value="'.$des_cons['unit'].'" />';
		label_cell($des_cons['unit']);
		echo '<input type="hidden" name="des_cons_qty_'.$i.'" id="des_cons_qty_'.$i.'" value="'.$des_cons['quantity'].'" />';
		label_cell($des_cons['quantity']);
		text_cells(null, 'des_rate_'.$i, $des_cons['rate'], 10, 10, null, null, null,"class=des_rate id=des_rate_".$i);
		text_cells(null, 'des_cost_'.$i, $des_cons['cost'], 10, 10, null, null, null,"id=des_cost_".$i);
		$total_cons_cost += $des_cons['cost'];
		end_row();
		$i++;
	}
	$_SESSION['total_cons_cost'] = $total_cons_cost;
	//display_error($_POST['total_cons_cost']);
	if(!isset($_POST['total_cons_cost']))
	{
		$_POST['total_cons_cost'] = $_SESSION['total_cons_cost'];
	}
	echo '<input type="hidden" name="design_cons_no" id="design_cons_no" value="'.$i.'" />';
	start_row();
	    label_cell("Total Design Consumable Cost : ","colspan=5 align=right");
     	label_cell($total_cons_cost,null,"total_cons_cost_label");	
		echo '<input type="hidden" name="total_cons_cost" id="total_cons_cost" value="'.$total_cons_cost.'" />';
	end_row();
	end_table();
	div_end();
}

function display_finish_consumables($finish_code_id, $mfrg_id)
{
	div_start("finish_consumables");
	display_heading("Finish Consumables");
	/*global $Ajax;
	$Ajax->activate('finish_consumables');*/
	$sql = "select * from ".TB_PREF."man_finish_cons_cost where mfrg_id=".$mfrg_id;
	$result = db_query($sql);
	$i = 0;
	unset($_SESSION['finish_consumables']);
	while($row = db_fetch($result))
	{
		$_SESSION['finish_consumables'][$i] = $row;
		$i++;
	}
	$j = count($_SESSION['finish_consumables']);
	$i = 0;
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("Unit"), _("Quantity"),_("Rate"), _("Cost"));
	table_header($th);
	$total_finish_cons_cost = 0;
	foreach($_SESSION['finish_consumables'] as $finish_cons)
	{
		start_row();
		$cons_type = get_cons_type_name($finish_cons['consumable_id']);
		$cons_select = get_cons_select_name($finish_cons['consumable_category']);
		
		echo '<input type="hidden" name="finish_master_name_'.$i.'" id="finish_master_name_'.$i.'" value="'.$finish_cons['consumable_id'].'" />';
		label_cell($cons_type['master_name']);
		echo '<input type="hidden" name="finish_consumable_name_'.$i.'" id="finish_consumable_name_'.$i.'" value="'.$finish_cons['consumable_category'].'" />';
		label_cell($cons_select['consumable_name']);
		echo '<input type="hidden" name="finish_cons_unit_'.$i.'" id="finish_cons_unit_'.$i.'" value="'.$finish_cons['unit'].'" />';
		label_cell($finish_cons['unit']);
		echo '<input type="hidden" name="finish_cons_qty_'.$i.'" id="finish_cons_qty_'.$i.'" value="'.$finish_cons['quantity'].'" />';
		label_cell($finish_cons['quantity']);
		
		text_cells(null, 'finish_cons_rate_'.$i, $finish_cons['rate'], 10, 10, null, null, null,"class=finish_cons_rate id=finish_cons_rate_".$i);
		
		text_cells(null, 'finish_cost_'.$i, $finish_cons['cost'], 10, 10, null, null, null,"id=finish_cost_".$i);
		$total_finish_cons_cost += $finish_cons['cost'];
		end_row();
		$i++;
	}
	$_SESSION['total_finish_cons_cost'] = $total_finish_cons_cost;
	if(!isset($_POST['total_finish_cons_cost']))
	{
		$_POST['total_finish_cons_cost'] = $_SESSION['total_finish_cons_cost'];
	}
	echo '<input type="hidden" name="finish_cons_no" id="finish_cons_no" value="'.$i.'" />';
	start_row();
	    label_cell("Total Finish Consumable Cost : ","colspan=5 align=right");
     	label_cell($total_finish_cons_cost,null,"total_finish_cons_cost_label");	
		echo '<input type="hidden" name="total_finish_cons_cost" id="total_finish_cons_cost" value="'.$total_finish_cons_cost.'" />';
	end_row();
	end_table();
	div_end();
	
}
function display_finish_fabrics($finish_code_id, $mfrg_id)
{
	div_start("fabric_consumables");
	display_heading("Fabric Consumables");
	/*global $Ajax;
	$Ajax->activate('fabric_consumables');*/
	$sql = "select * from ".TB_PREF."man_fabric_cons_cost where mfrg_id=".$mfrg_id;
	$result = db_query($sql, "Could not retrieve fabric cost details.");
	$i = 0;
	unset($_SESSION['fabric_consumables']);
	while($row = db_fetch($result))
	{
		$_SESSION['fabric_consumables'][$i] = $row;
		$i++;
	}
	$j = count($_SESSION['fabric_consumables']);
	$i = 0;
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Fabric Name"), _("Quantity"), _("Rate"), _("Cost"));
	table_header($th);
	$total_fabric_cost = 0;
	foreach($_SESSION['fabric_consumables'] as $fabric)
	{
		start_row();
		$cons_select = get_cons_select_name($fabric['fabric_id']);
		
		echo '<input type="hidden" name="fabric_name_'.$i.'" id="fabric_name_"'.$i.' value="'.$fabric['fabric_id'].'" />';
		label_cell($cons_select['consumable_name']);
		echo '<input type="hidden" name="perecentage_'.$i.'" id="perecentage_"'.$i.' value="'.$fabric['percentage'].'" />';
		label_cell($fabric['percentage']);
		text_cells(null, 'fabric_rate_'.$i, $fabric['rate'], 10, 10, null, null, null,"class=fabric_rate id=fabric_rate_".$i);
		
		text_cells(null, 'fabric_cost_'.$i, $fabric['cost'], 10, 10, null, null, null,"id=fabric_cost_".$i);		
		$total_fabric_cost += $fabric['cost'];
		end_row();
		$i++;
	}
	$_SESSION['total_fabric_cost'] = $total_fabric_cost;
	if(!isset($_POST['total_fabric_cost']))
	{
		$_POST['total_fabric_cost'] = $_SESSION['total_fabric_cost'];
	}
	echo '<input type="hidden" name="fabric_cons_no" id="fabric_cons_no" value="'.$i.'" />';
	start_row();
	    label_cell("Total Fabric Cost : ","colspan=3 align=right");
     	label_cell($total_fabric_cost,null,"total_fabric_cost_label");	
		echo '<input type="hidden" name="total_fabric_cost" id="total_fabric_cost" value="'.$total_fabric_cost.'" />';
	end_row();
	end_table();
	div_end();
	
}
// labour part

function display_labour_summary(&$order, $editable=true)
{
	div_start('labour_table');
	display_heading("Labour Costing");
	start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_(" "),_(" "));
		table_header($th);
		start_row();
			text_row(_("Basic Price:"),'basic_price', $_POST['basic_price'], 30, 30, null,null,null,"id= basic_price");
	    end_row();
	end_table();
    
	
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Labour Type"), _("Special Labour Rate"), _("Labour Amount"), _("Special Labour Cost"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);

	global $total_special_amount;
	$total_special_amount = 0;
	$id = find_submit('Edit');
	$k = 0;
	foreach ($order->line_items as $line_con => $con_line)
   	{
		if($line_con < 100){ 
		   $line_con += 100;
		   //continue; 
		}
		$total_special_amount += $con_line->special_labour_cost;
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
    		label_cell($con_line->labour_type);
    		label_cell($con_line->special_rate);
			label_cell($con_line->labour_amount);
			label_cell($con_line->special_labour_cost);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			labour_controls($order, $k, $line_con);
		}
		$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		labour_controls($order, $k);
		
	$_POST['total_labour_cost'] = $total_special_amount + $_POST['basic_price'];
	//display_error($_POST['total_mfrg_cost']);
	calculate_cost();
	
	start_row();
	    label_cell("Total Special Cost Amount : ","colspan=3 align=right");
     	label_cell($total_special_amount);	
		hidden('total_special_amount', $total_special_amount);
	end_row();
	start_row();
	    label_cell("Total Labour Cost : ","colspan=3 align=right");
     	label_cell($_POST['total_labour_cost'],null,"total_labour_cost_label");
		echo '<input type="hidden" name="total_labour_cost" id="total_labour_cost" value="'.$_POST['total_labour_cost'].'" />';	
	end_row();
	start_row();
	    label_cell("Extra % : ","colspan=3 align=right");
     	text_cells(null, 'extra_percent', $_POST['extra_percent'], 10, 10, null, null, null,"id=extra_percent");	
	end_row();
	start_row();
	    label_cell("Extra Amount : ","colspan=3 align=right");
		text_cells(null, 'extra_amount', $_POST['extra_amount'], 10, 10, null, null, null,"id=extra_amount");
	end_row();
	start_row();
	    label_cell("Total Cost : ","colspan=3 align=right");
     	label_cell($_POST['total_cost'],null,"total_cost_label");	
		echo '<input type="hidden" name="total_cost" id="total_cost" value="'.$_POST['total_cost'].'" />';
	end_row();
	start_row();
	    label_cell("Admin % : ","colspan=3 align=right");
     	text_cells(null, 'admin_percent', $_POST['admin_percent'], 10, 10, null, null, null,"id=admin_percent");	
	end_row();
	start_row();
	    label_cell("Admin Cost : ","colspan=3 align=right");
     	text_cells(null, 'admin_cost', $_POST['admin_cost'], 10, 10, null, null, null,"id=admin_cost");	
	end_row();
	start_row();
	    label_cell("Total CP : ","colspan=3 align=right");
     	label_cell($_POST['total_cp'],null,"total_cp_label");	
		echo '<input type="hidden" name="total_cp" id="total_cp" value="'.$_POST['total_cp'].'" />';	
	end_row();
	start_row();
	    label_cell("Profit% : ","colspan=3 align=right");
     	text_cells(null, 'profit_percent', $_POST['profit_percent'], 10, 10, null, null, null,"id=profit_percent");		
	end_row();
	start_row();
	    label_cell("Profit : ","colspan=3 align=right");
     	text_cells(null, 'profit', $_POST['profit'], 10, 10, null, null, null,"id=profit");	
	end_row();
	start_row();
	    label_cell("Total MFRG Amount : ","colspan=3 align=right");
     	label_cell($_POST['total_mfrg'],null,"total_mfrg_label");	
		echo '<input type="hidden" name="total_mfrg" id="total_mfrg" value="'.$_POST['total_mfrg'].'" />';		
	end_row();
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table();

	
}

function labour_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		
		$_POST['labour_type'] = $order->line_items[$id]->labour_type;
		$_POST['special_rate'] = $order->line_items[$id]->special_rate;
		$_POST['labour_amount'] = $order->line_items[$id]->labour_amount;
		$_POST['special_labour_cost'] = $order->line_items[$id]->special_labour_cost;
		

	    $Ajax->activate('labour_table');
	}
	text_cells_ex(null, 'labour_type', null, null);
	
	text_cells_ex(null, 'special_rate', 10, 10);
	text_cells_ex(null, 'labour_amount', 10, 10);
	text_cells_ex(null, 'special_labour_cost', 10, 10);
	if ($id != -1)
	{
		button_cell('LabourUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('LabourCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		//set_focus('amount');
	} 
	else 
		submit_cells('LabourAddItem', _("Add"), "colspan=2",
			_('Add new line to Labour Section'), true);
	end_row();
	
}


function read_wood_costing($mfrg_id, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT wood_id,cft,rate,amount FROM ".TB_PREF."manufacturing_wood_cost
	       WHERE mfrg_id =".db_escape($mfrg_id);
	$result = db_query($sql, "The lines on the wood costing cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_wood_part($order->lines_on_order, $myrow["wood_id"],
            	$myrow["cft"],$myrow["rate"],$myrow["amount"])) {
            		$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

function read_labour_costing($mfrg_id)
{
	$sql = "SELECT basic_price FROM ".TB_PREF."manufacturing_labour_cost WHERE mfrg_id =".db_escape($mfrg_id);
	$result = db_query($sql, "Labour costing cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$_POST['basic_price'] = $myrow['basic_price'];
        }
    } 
}

function read_labour_special_costing($mfrg_id, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT labour_type,special_rate,labour_amount, special_labour_cost FROM ".TB_PREF."manufacturing_labour_special_cost
	       WHERE mfrg_id =".db_escape($mfrg_id);
	$result = db_query($sql, "The lines on the special labour costing cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_labour_part($order->lines_on_order, $myrow["labour_type"],
            	$myrow["special_rate"],$myrow["labour_amount"],$myrow["special_labour_cost"])) {
            		//$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

function read_manufacturing_cost($mfrg_id)
{
	$sql = "SELECT * FROM ".TB_PREF."manufacturing_cost WHERE id =".db_escape($mfrg_id);
	$result = db_query($sql, "manufacturing cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$_POST['extra_percent'] = $myrow['extra_percent'];
			$_POST['extra_amount'] = $myrow['extra_amount'];
			$_POST['total_cost'] = $myrow['total_cost'];
			$_POST['admin_percent'] = $myrow['admin_percent'];
			$_POST['admin_cost'] = $myrow['admin_cost'];
			$_POST['total_cp'] = $myrow['total_cp'];
			$_POST['profit_percent'] = $myrow['profit_percent'];
			$_POST['profit'] = $myrow['profit'];
        }
    } 
}

function calculate_cost()
{

	$total_cost = round($_POST['total_cons_cost'] + $_POST['total_finish_cons_cost'] + $_POST['total_fabric_cost'] + $_POST['total_labour_cost'] + $_POST['total_wood_cost'], 2);
	$extra_percent = $_POST['extra_percent'];
	//display_error($_POST['total_cons_cost']);
	$_POST['extra_amount'] = round(($total_cost*$extra_percent)/100, 2);
	//display_error($_POST['extra_amount']);
	$_POST['total_cost'] = round($total_cost+$_POST['extra_amount'], 2);
	$_POST['admin_cost'] = round(($_POST['total_cost']*$_POST['admin_percent'])/100, 2);

	$_POST['total_cp'] = round($_POST['total_cost'] + $_POST['admin_cost'], 2);
	
	$_POST['profit'] = round(($_POST['total_cp']*$_POST['profit_percent'])/100, 2);
	$_POST['total_mfrg'] = round($_POST['total_cp'] + $_POST['profit'], 2);
}

function calculate_finishing_cost()
{	
	$total_mfrg_cost = $_POST['total_mfrg_cost'];
	$sanding_percent = $_POST['sanding_percent'];
	$_POST['sanding_cost'] = round(($total_mfrg_cost*$sanding_percent)/100, 2);
	
	$_POST['polish_cost'] = round(($total_mfrg_cost*$_POST['polish_percent'])/100, 2);
	$_POST['packaging_cost'] = round(($total_mfrg_cost*$_POST['packaging_percent'])/100, 2);
	$_POST['forwarding_cost'] = round(($total_mfrg_cost*$_POST['forwarding_percent'])/100, 2);
	$_POST['total_finishing_cost'] = round($_POST['sanding_cost'] + $_POST['polish_cost'] + $_POST['packaging_cost'] + $_POST['forwarding_cost'] + $_POST['total_special_cost'] + $_POST['other_cost'], 2);         
}

function calculate_currency()
{
	$sql = "select * from ".TB_PREF."currency_master";
	$result = db_query($sql, "Could not get currencies.");
	$currencies = array();
	$total_currency = 0;
	while($row = db_fetch($result))
	{
		$_POST['currency'.$row['id']] = round($row['rate']*$_POST['final_total_cost'], 2);
	}
}

function calculate_final_cost()
{	
	$_POST['total_amount'] = round($_POST['total_mfrg_cost'] + $_POST['total_finishing_cost'], 2);;
	
	$final_profit_percent = $_POST['final_profit_percent'];
	$_POST['final_profit'] = round(($_POST['total_amount']*$final_profit_percent)/100, 2);;
	$_POST['total_percent'] = round($_POST['sanding_percent'] + $_POST['polish_percent'] + $_POST['packaging_percent'] + $_POST['forwarding_percent'] + $_POST['final_profit_percent'], 2);         
	
	$_POST['total_export_cost'] = round($_POST['sanding_cost'] + $_POST['polish_cost'] + $_POST['packaging_cost'] + $_POST['forwarding_cost'] + $_POST['final_profit'], 2);
	$_POST['total_fob_cost'] = round($_POST['total_mfrg_cost'] + $_POST['total_export_cost'], 2);;
	
	$_POST['final_finishing_cost'] = round(($_POST['total_mfrg_cost']*$_POST['finishing_exp_percent'])/100, 2);
	$_POST['sp_by_process'] = round($_POST['total_fob_cost'], 2);
	
	$_POST['sp_by_lumsum'] = round($_POST['total_mfrg_cost'] + $_POST['final_finishing_cost'], 2);
	
	$_POST['final_total_cost'] = max($_POST['sp_by_process'] , $_POST['sp_by_lumsum']);
	
	calculate_currency();
}

?>
