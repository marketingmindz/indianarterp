<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/




class packaging_order 
{

	var $line_items = array();
	var $line_con = array();
	
	var $line_no;
	var $packaging_id;
	var $principle_type;
	var $code_id;  
	var $unit;
	var $quantity;
	var $width;
	var $height;
	var $density;
	
	var $part_name_id;
	var $part_qty;
	var $lines_on_packing = 0;
	var $lines_on_con = 0;
	
	//*****************************************************
	function packaging_order()
	{
		$this->line_items = array();
		$this->line_con = array();
		$this->lines_on_packing = $this->packaging_id = 0;
		$this->lines_on_con = $this->packaging_id = 0;
	}
	
	//********************************************************
	
	function add_to_pacakaging_part($line_no, $principle_type, $code_id, $unit, $quantity, $width, $height, $density)
	{
           
			$this->line_items[$line_no] = new packing_line_details($line_no, $principle_type, $code_id, $unit, $quantity, $width, $height, $density);
			$this->lines_on_packing++;
		
			return 1;
		
		}
   
    function update_pacakaging_part($line_no, $principle_type, $code_id, $unit, $quantity, $width, $height, $density)
	{
		$this->line_items[$line_no]->principle_type = $principle_type;
		$this->line_items[$line_no]->code_id = $code_id;
		$this->line_items[$line_no]->unit = $unit;
		$this->line_items[$line_no]->quantity = $quantity;
		$this->line_items[$line_no]->width = $width;
		$this->line_items[$line_no]->height = $height;
		$this->line_items[$line_no]->density = $density;
	}
	
	 function add_to_carton_part($line_no, $part_name_id, $part_qty)
	{
		
		
			$line_no += 100; 
			$this->line_con[$line_no] = new part_line_details($line_no, $part_name_id, $part_qty);
			$this->lines_on_con++;
			return 1;
		
	}
   	function update_carton_part_item($line_no, $part_name_id, $part_qty)
	{
		if($line_no < 100){ 
		   $line_no += 100; 
		}
		$this->line_con[$line_no]->part_name_id = $part_name_id;
		$this->line_con[$line_no]->part_qty = $part_qty;
	}
   
   
   
   
	//**************** ***************
	function order_packing_has_items() 
	{
		return count($this->line_items) != 0;
	}
	
	function order_con_has_items() 
	{
		return count($this->line_con) != 0;
	}
	function clear_packing_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		
		$this->lines_on_packing = 0;  
		$this->packaging_id = 0;
	}
	function clear_con_items() 
	{
    	unset($this->line_con);
		$this->line_con = array();
		
		$this->lines_on_con = 0;  
		$this->packaging_id = 0;
	}
	
	//***********************************
   
} /* end of class defintion */

class packing_line_details 
{

	var $line_no;
	var $principle_type;
	var $code_id;
	var $packing_detail_rec;
	var $unit;
	var $quantity;
	var $width;
	var $height;
	var $density;
	

	function packing_line_details($line_no, $principle_type, $code_id, $unit, $quantity, $width, $height, $density)
	{
	
		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->principle_type = $principle_type;
		$this->code_id = $code_id;
		$this->unit = $unit;
		$this->quantity = $quantity;
		$this->width = $width;
		$this->height = $height;
		$this->density = $density;
	
	}
}

class part_line_details 
{

	var $line_no;
	var $con_detail_rec;
	var $part_name_id;
	var $part_qty;

	function part_line_details($line_no, $part_name_id, $part_qty)
	{

		$this->line_no = $line_no;
		$this->part_name_id = $part_name_id;
		$this->part_qty = $part_qty;	
	}

}


?>

