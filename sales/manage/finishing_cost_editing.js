// JavaScript Document

	$(document).on('change','#sanding_percent',function(){
		var sanding_percent = $(this).val();
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		var sanding_cost =0;
		sanding_cost = (total_mfrg_cost * sanding_percent)/100;
		$("#sanding_cost").val(sanding_cost.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	$(document).on('change','#sanding_cost',function(){
		var sanding_cost = $(this).val();
		var sanding_percent = 0;
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		sanding_percent = (sanding_cost * 100)/total_mfrg_cost;
		$("#sanding_percent").val(sanding_percent.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});	
	
	
	$(document).on('change','#polish_percent',function(){
		var polish_percent = $(this).val();
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		var polish_cost =0;
		polish_cost = (total_mfrg_cost * polish_percent)/100;
		$("#polish_cost").val(polish_cost.toFixed(2));
		
		var total_finishing_cost = 0;
		var sanding_cost = $("#sanding_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	$(document).on('change','#polish_cost',function(){
		var polish_cost = $(this).val();
		var polish_percent = 0;
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		polish_percent = (polish_cost * 100)/total_mfrg_cost;
		$("#polish_percent").val(polish_percent.toFixed(2));
		
		var total_finishing_cost = 0;
		var sanding_cost = $("#sanding_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	
	
	$(document).on('change','#packaging_percent',function(){
		var packaging_percent = $(this).val();
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		var packaging_cost =0;
		packaging_cost = (total_mfrg_cost * packaging_percent)/100;
		$("#packaging_cost").val(packaging_cost.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var sanding_cost = $("#sanding_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	$(document).on('change','#packaging_cost',function(){
		var packaging_cost = $(this).val();
		var packaging_percent = 0;
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		packaging_percent = (packaging_cost * 100)/total_mfrg_cost;
		$("#packaging_percent").val(packaging_percent.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var sanding_cost = $("#sanding_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	
	
	$(document).on('change','#forwarding_percent',function(){
		var forwarding_percent = $(this).val();
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		var forwarding_cost =0;
		forwarding_cost = (total_mfrg_cost * forwarding_percent)/100;
		$("#forwarding_cost").val(forwarding_cost.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var sanding_cost = $("#sanding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});
	$(document).on('change','#forwarding_cost',function(){
		var forwarding_cost = $(this).val();
		var forwarding_percent = 0;
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		forwarding_percent = (forwarding_cost * 100)/total_mfrg_cost;
		$("#forwarding_percent").val(forwarding_percent.toFixed(2));
		
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var sanding_cost = $("#sanding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});	
	
	$(document).on('change','#other_cost',function(){
		var other_cost = $(this).val();
				
		var total_finishing_cost = 0;
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_special_cost = $("#total_special_cost").val();
		var sanding_cost = $("#sanding_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
		total_amount();
		final_cost();
	});	
	
	
	$(document).on('blur','input[name=special_amount]',function(){
		var special_amount = $(this).val();
		var special_rate = $("input[name=special_finish_rate]").val();
		var total = special_amount * special_rate;
		$("input[name=total]").val(total);
		
	});
	$(document).on('blur','input[name=special_finish_rate]',function(){
		var special_rate = $(this).val();
		var special_amount = $("input[name=special_amount]").val();
		var total = special_amount * special_rate;
		$("input[name=total]").val(total);
		
	});
	$(document).on('blur','input[name=total]',function(){
		var special_rate = $("input[name=special_finish_rate]").val();
		var special_amount = $("input[name=special_amount]").val();
		var total = special_amount * special_rate;
		$("input[name=total]").val(total);
		
	});
	
	function finish_costing(){
		var sanding_percent = $("#sanding_percent").val();
		var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
		var sanding_cost =0;
		sanding_cost = (total_mfrg_cost * sanding_percent)/100;
		$("#sanding_cost").val(sanding_cost.toFixed(2));
		
		var total_finishing_cost = 0;
		
		var polish_percent = $("#polish_percent").val();
		var polish_cost =0;
		polish_cost = (total_mfrg_cost * polish_percent)/100;
		$("#polish_cost").val(polish_cost.toFixed(2));
		
		var packaging_percent = $("#packaging_percent").val();
		var packaging_cost =0;
		packaging_cost = (total_mfrg_cost * packaging_percent)/100;
		$("#packaging_cost").val(packaging_cost.toFixed(2));
		
		var forwarding_percent = $("#forwarding_percent").val();
		var forwarding_cost =0;
		forwarding_cost = (total_mfrg_cost * forwarding_percent)/100;
		$("#forwarding_cost").val(forwarding_cost.toFixed(2));
		
		var total_special_cost = $("#total_special_cost").val();
		var other_cost = $("#other_cost").val();
		
		total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
		$("#total_finishing_cost").val(total_finishing_cost.toFixed(2));
		$("#total_finishing_cost_label").html(total_finishing_cost.toFixed(2));
	}