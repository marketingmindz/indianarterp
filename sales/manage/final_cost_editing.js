// JavaScript Document

	$(document).on('change','#final_profit_percent',function(){
		var final_profit_percent = $(this).val();
		var total_amount = $("input[name=total_amount]").val();
		var final_profit =0;
		final_profit = (total_amount * final_profit_percent)/100;
		$("#final_profit").val(final_profit.toFixed(2));
	
		var sanding_percent = $("#sanding_percent").val();
		var polish_percent = $("#polish_percent").val();
		var packaging_percent = $("#packaging_percent").val();
		var forwarding_percent = $("#forwarding_percent").val();
		var total_percent = parseFloat(sanding_percent) + parseFloat(polish_percent) + parseFloat(packaging_percent) + parseFloat(forwarding_percent) +  parseFloat(final_profit_percent);     
		$("#total_percent").val(total_percent.toFixed(2));
		
		var sanding_cost = $("#sanding_cost").val();
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_export_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) + parseFloat(forwarding_cost) + parseFloat(final_profit);
		$("#total_export_cost").val(total_export_cost.toFixed(2));

		var total_mfrg_cost = $("#total_mfrg_cost").val();
		
		var total_fob_cost = parseFloat(total_mfrg_cost) + parseFloat(total_export_cost);
		$("#total_fob_cost").val(total_fob_cost.toFixed(2)); 
		$("#sp_by_process").val(total_fob_cost.toFixed(2));
		var final_finishing_cost = $("#final_finishing_cost").val();
		var sp_by_lumsum = parseFloat(total_mfrg_cost) + parseFloat(final_finishing_cost);
		$("#sp_by_lumsum").val(sp_by_lumsum.toFixed(2));
		var final_total_cost = Math.max(sp_by_lumsum, total_fob_cost);
		$("#final_total_cost").val(final_total_cost.toFixed(2));
		
		$.ajax({
			url: "convert_currency.php",
			method: "POST",
			dataType:"json",
			data:{total_cost:final_total_cost},
			success: function(data){
				var total_currency = $("input[name=total_currency]").val();
				var currencies = $("input[name=currencies]").val();
				var curr = currencies.split(",");
				for(var k=0; k<total_currency; k++)
				{
					$("#currency"+curr[k]).val(data[k].toFixed(2));
				}
				}
		});
	});
	$(document).on('change','#final_profit',function(){
		var final_profit = $(this).val();
		var final_profit_percent = 0;
		var total_amount = $("input[name=total_amount]").val();
		final_profit_percent = (final_profit * 100)/total_amount;

		$("#final_profit_percent").val(final_profit_percent.toFixed(2));
		
		var sanding_percent = $("#sanding_percent").val();
		var polish_percent = $("#polish_percent").val();
		var packaging_percent = $("#packaging_percent").val();
		var forwarding_percent = $("#forwarding_percent").val();
		var total_percent = parseFloat(sanding_percent) + parseFloat(polish_percent) + parseFloat(packaging_percent) + parseFloat(forwarding_percent) +  parseFloat(final_profit_percent);     
		$("#total_percent").val(total_percent.toFixed(2));
		
		var sanding_cost = $("#sanding_cost").val();
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_export_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) + parseFloat(forwarding_cost) + parseFloat(final_profit);
		$("#total_export_cost").val(total_export_cost.toFixed(2));
		
		var total_mfrg_cost = $("#total_mfrg_cost").val();
		
		var total_fob_cost = parseFloat(total_mfrg_cost) + parseFloat(total_export_cost);
		$("#total_fob_cost").val(total_fob_cost.toFixed(2)); 
		$("#sp_by_process").val(total_fob_cost.toFixed(2)); 
		var final_finishing_cost = $("#final_finishing_cost").val();
		var sp_by_lumsum = parseFloat(total_mfrg_cost) + parseFloat(final_finishing_cost);
		$("#sp_by_lumsum").val(sp_by_lumsum.toFixed(2));
		var final_total_cost = Math.max(sp_by_lumsum, total_fob_cost);
		$("#final_total_cost").val(final_total_cost.toFixed(2));
		
		$.ajax({
			url: "convert_currency.php",
			method: "POST",
			dataType:"json",
			data:{total_cost:final_total_cost},
			success: function(data){
				var total_currency = $("input[name=total_currency]").val();
				var currencies = $("input[name=currencies]").val();
				var curr = currencies.split(",");
				for(var k=0; k<total_currency; k++)
				{
					$("#currency"+curr[k]).val(data[k].toFixed(2));
				}
				}
		});
	});	
	
	
	$(document).on('change','#finishing_exp_percent',function(){
		var finishing_exp_percent = $(this).val();
		var total_mfrg_cost = $("#total_mfrg_cost").val();
		var final_finishing_cost =0;
		final_finishing_cost = (total_mfrg_cost * finishing_exp_percent)/100;
		$("#final_finishing_cost").val(final_finishing_cost.toFixed(2));
		var sp_by_lumsum = parseFloat(total_mfrg_cost) + parseFloat(final_finishing_cost);
		$("#sp_by_lumsum").val(sp_by_lumsum.toFixed(2)); 
		var total_fob_cost = $("#total_fob_cost").val();
		var final_total_cost = Math.max(sp_by_lumsum, total_fob_cost);
		$("#final_total_cost").val(final_total_cost.toFixed(2));
		
		$.ajax({
			url: "convert_currency.php",
			method: "POST",
			dataType:"json",
			data:{total_cost:final_total_cost},
			success: function(data){
				var total_currency = $("input[name=total_currency]").val();
				var currencies = $("input[name=currencies]").val();
				var curr = currencies.split(",");
				for(var k=0; k<total_currency; k++)
				{
					$("#currency"+curr[k]).val(data[k].toFixed(2));
				}
				}
		});
				
	});
	$(document).on('change','#final_finishing_cost',function(){
		var final_finishing_cost = $(this).val();
		var finishing_exp_percent = 0;
		var total_mfrg_cost = $("#total_mfrg_cost").val();
		finishing_exp_percent = (final_finishing_cost * 100)/total_mfrg_cost;

		$("#finishing_exp_percent").val(finishing_exp_percent.toFixed(2));
		var sp_by_lumsum = parseFloat(total_mfrg_cost) + parseFloat(final_finishing_cost);
		$("#sp_by_lumsum").val(sp_by_lumsum.toFixed(2)); 
		
		var total_fob_cost = $("#total_fob_cost").val();
		var final_total_cost = Math.max(sp_by_lumsum, total_fob_cost);
		$("#final_total_cost").val(final_total_cost.toFixed(2));
		
		$.ajax({
			url: "convert_currency.php",
			method: "POST",
			dataType:"json",
			data:{total_cost:final_total_cost},
			success: function(data){
				var total_currency = $("input[name=total_currency]").val();
				var currencies = $("input[name=currencies]").val();
				var curr = currencies.split(",");
				for(var k=0; k<total_currency; k++)
				{
					$("#currency"+curr[k]).val(data[k].toFixed(2));
				}
				}
		});
	});
	
	
	
	$(document).on('click','.finish_code',function(){
		var finish_code = $(this).attr('id');
		var finish_code_id = $("input[name="+finish_code+"]").val();
		$("input[name=finish_code_id]").val(finish_code_id);
		$("#finish_code").val(finish_code);
		$(this).css("color","red");
		$.ajax({
			url: "destroy_session.php",
			method: "POST",
			data:{finish_code:finish_code},
			success: function(data){
		//alert(data);
				}
		});
		return true;
		
	});	
	
	$(document).on('click','#GetFinishProduct',function(){
		$("#finish_code").val('');
		return true;
		
	});	

function final_cost()
{
		var final_profit_percent = $('#final_profit_percent').val();
		var total_amount = $("input[name=total_amount]").val();
		var final_profit =0;
		final_profit = (total_amount * final_profit_percent)/100;
		$("#final_profit").val(final_profit.toFixed(2));
		
		var sanding_percent = $("#sanding_percent").val();
		var polish_percent = $("#polish_percent").val();
		var packaging_percent = $("#packaging_percent").val();
		var forwarding_percent = $("#forwarding_percent").val();
		
		var total_percent = parseFloat(sanding_percent) + parseFloat(polish_percent) + parseFloat(packaging_percent) + parseFloat(forwarding_percent) +  parseFloat(final_profit_percent);     
		$("#total_percent").val(total_percent.toFixed(2));
		
		var sanding_cost = $("#sanding_cost").val();
		var polish_cost = $("#polish_cost").val();
		var packaging_cost = $("#packaging_cost").val();
		var forwarding_cost = $("#forwarding_cost").val();
		var total_export_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) + parseFloat(forwarding_cost) + parseFloat(final_profit);
		$("#total_export_cost").val(total_export_cost.toFixed(2));
		var total_mfrg_cost = $("#total_mfrg_cost").val();
		
		var total_fob_cost = parseFloat(total_mfrg_cost) + parseFloat(total_export_cost);
		$("#total_fob_cost").val(total_fob_cost.toFixed(2)); 
		$("#sp_by_process").val(total_fob_cost.toFixed(2));

		var finishing_exp_percent = $("#finishing_exp_percent").val();
		var total_mfrg_cost = $("#total_mfrg_cost").val();
		var final_finishing_cost =0;
		final_finishing_cost = (total_mfrg_cost * finishing_exp_percent)/100;
		$("#final_finishing_cost").val(final_finishing_cost.toFixed(2));
		
		var sp_by_lumsum = parseFloat(total_mfrg_cost) + parseFloat(final_finishing_cost);
		$("#sp_by_lumsum").val(sp_by_lumsum.toFixed(2));
		var final_total_cost = Math.max(sp_by_lumsum, total_fob_cost);
		$("#final_total_cost").val(final_total_cost.toFixed(2));
		
		$.ajax({
			url: "convert_currency.php",
			method: "POST",
			dataType:"json",
			data:{total_cost:final_total_cost},
			success: function(data){
				var total_currency = $("input[name=total_currency]").val();
				var currencies = $("input[name=currencies]").val();
				var curr = currencies.split(",");
				for(var k=0; k<total_currency; k++)
				{
					$("#currency"+curr[k]).val(data[k].toFixed(2));
				}
				}
		});
}
function total_amount()
{
	var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
	var total_finishing_cost = $("#total_finishing_cost").val();
	var total_amount = parseFloat(total_mfrg_cost) + parseFloat(total_finishing_cost);
	$("#total_amount").val(total_amount.toFixed(2));
	$("#total_amount_label").html(total_amount.toFixed(2));
}