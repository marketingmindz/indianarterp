<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTOMER';
$path_to_root = "../..";

include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Customers"), @$_REQUEST['popup'], false, "", $js); 

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/banking.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");

?>

<!-- style for multiple category  ---->
<style type="text/css">
.multiselect {
        width: 200px;
		    
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
		width: 200px;
        border: 1px #dadada solid;
		position: absolute;
    z-index: 999999999;
    background-color: #fff;
	max-height:300px;
	overflow:scroll;
	overflow-x:hidden;
	overflow-wrap: break-word;

    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
</style>

<?php

if (isset($_GET['debtor_no'])) 
{
	$_POST['customer_id'] = $_GET['debtor_no'];
}

$selected_id = get_post('customer_id','');
//--------------------------------------------------------------------------------------------

function can_process()
{
	if (strlen($_POST['CustName']) == 0) 
	{
		display_error(_("The customer name cannot be empty."));
		set_focus('CustName');
		return false;
	} 

	if (strlen($_POST['cust_ref']) == 0) 
	{
		display_error(_("The customer short name cannot be empty."));
		set_focus('cust_ref');
		return false;
	} 
	
	if (!check_num('credit_limit', 0))
	{
		display_error(_("The credit limit must be numeric and not less than zero."));
		set_focus('credit_limit');
		return false;		
	} 
	
	if (!check_num('pymt_discount', 0, 100)) 
	{
		display_error(_("The payment discount must be numeric and is expected to be less than 100% and greater than or equal to 0."));
		set_focus('pymt_discount');
		return false;		
	} 
	
	if (!check_num('discount', 0, 100)) 
	{
		display_error(_("The discount percentage must be numeric and is expected to be less than 100% and greater than or equal to 0."));
		set_focus('discount');
		return false;		
	} 

	return true;
}

//--------------------------------------------------------------------------------------------

function handle_submit(&$selected_id)
{
	global $path_to_root, $Ajax, $auto_create_branch;

	if (!can_process())
		return;
		
	if ($selected_id) 
	{
		update_customer($_POST['customer_id'], $_POST['CustName'], $_POST['cust_ref'], $_POST['address'],
			$_POST['tax_id'], $_POST['curr_code'], $_POST['dimension_id'], $_POST['dimension2_id'],
			$_POST['credit_status'], $_POST['payment_terms'], input_num('discount') / 100, input_num('pymt_discount') / 100,
			input_num('credit_limit'), $_POST['sales_type'], $_POST['notes']);

		update_record_status($_POST['customer_id'], $_POST['inactive'],
			'debtors_master', 'debtor_no');

		$Ajax->activate('customer_id'); // in case of status change
		display_notification(_("Customer has been updated."));
	} 
	else 
	{ 	//it is a new customer

		begin_transaction();
		add_customer($_POST['CustName'], $_POST['cust_ref'], $_POST['address'],
			$_POST['tax_id'], $_POST['curr_code'], $_POST['dimension_id'], $_POST['dimension2_id'],
			$_POST['credit_status'], $_POST['payment_terms'], input_num('discount') / 100, input_num('pymt_discount') / 100,
			input_num('credit_limit'), $_POST['sales_type'], $_POST['notes']);

		$selected_id = $_POST['customer_id'] = db_insert_id();
         
		if (isset($auto_create_branch) && $auto_create_branch == 1)
		{
        	add_branch($selected_id, $_POST['CustName'], $_POST['cust_ref'],
                $_POST['address'], $_POST['salesman'], $_POST['area'], $_POST['tax_group_id'], '',
                get_company_pref('default_sales_discount_act'), get_company_pref('debtors_act'), get_company_pref('default_prompt_payment_act'),
                $_POST['location'], $_POST['address'], 0, 0, $_POST['ship_via'], $_POST['notes']);
                
        	$selected_branch = db_insert_id();
        
			add_crm_person($_POST['CustName'], $_POST['cust_ref'], '', $_POST['address'], 
				$_POST['phone'], $_POST['phone2'], $_POST['fax'], $_POST['email'], '', '');

			$pers_id = db_insert_id();
			add_crm_contact('cust_branch', 'general', $selected_branch, $pers_id);

			add_crm_contact('customer', 'general', $selected_id, $pers_id);
		}
		commit_transaction();

		display_notification(_("A new customer has been added."));

		if (isset($auto_create_branch) && $auto_create_branch == 1)
			display_notification(_("A default Branch has been automatically created, please check default Branch values by using link below."));
		
		$Ajax->activate('_page_body');
	}
}
//--------------------------------------------------------------------------------------------

if (isset($_POST['submit'])) 
{
	handle_submit($selected_id);
}
//-------------------------------------------------------------------------------------------- 

if (isset($_POST['delete'])) 
{

	$cancel_delete = 0;

	// PREVENT DELETES IF DEPENDENT RECORDS IN 'debtor_trans'

	if (key_in_foreign_table($selected_id, 'debtor_trans', 'debtor_no'))
	{
		$cancel_delete = 1;
		display_error(_("This customer cannot be deleted because there are transactions that refer to it."));
	} 
	else 
	{
		if (key_in_foreign_table($selected_id, 'sales_orders', 'debtor_no'))
		{
			$cancel_delete = 1;
			display_error(_("Cannot delete the customer record because orders have been created against it."));
		} 
		else 
		{
			if (key_in_foreign_table($selected_id, 'cust_branch', 'debtor_no'))
			{
				$cancel_delete = 1;
				display_error(_("Cannot delete this customer because there are branch records set up against it."));
				//echo "<br> There are " . $myrow[0] . " branch records relating to this customer";
			}
		}
	}
	
	if ($cancel_delete == 0) 
	{ 	//ie not cancelled the delete as a result of above tests
	
		delete_customer($selected_id);

		display_notification(_("Selected customer has been deleted."));
		unset($_POST['customer_id']);
		$selected_id = '';
		$Ajax->activate('_page_body');
	} //end if Delete Customer
}

function customer_settings($selected_id) 
{
	global $SysPrefs, $path_to_root, $auto_create_branch;
	
	if (!$selected_id) 
	{
	 	if (list_updated('customer_id') || !isset($_POST['CustName'])) {
			$_POST['CustName'] = $_POST['cust_ref'] = $_POST['address'] = $_POST['tax_id']  = '';
			$_POST['dimension_id'] = 0;
			$_POST['dimension2_id'] = 0;
			$_POST['sales_type'] = -1;
			$_POST['curr_code']  = get_company_currency();
			$_POST['credit_status']  = -1;
			$_POST['payment_terms']  = $_POST['notes']  = '';

			$_POST['discount']  = $_POST['pymt_discount'] = percent_format(0);
			$_POST['credit_limit']	= price_format($SysPrefs->default_credit_limit());
		}
	}
	else 
	{
		$myrow = get_customer($selected_id);

		$_POST['CustName'] = $myrow["name"];
		$_POST['cust_ref'] = $myrow["debtor_ref"];
		$_POST['address']  = $myrow["address"];
		$_POST['tax_id']  = $myrow["tax_id"];
		$_POST['dimension_id']  = $myrow["dimension_id"];
		$_POST['dimension2_id']  = $myrow["dimension2_id"];
		$_POST['sales_type'] = $myrow["sales_type"];
		$_POST['curr_code']  = $myrow["curr_code"];
		$_POST['credit_status']  = $myrow["credit_status"];
		$_POST['payment_terms']  = $myrow["payment_terms"];
		$_POST['discount']  = percent_format($myrow["discount"] * 100);
		$_POST['pymt_discount']  = percent_format($myrow["pymt_discount"] * 100);
		$_POST['credit_limit']	= price_format($myrow["credit_limit"]);
		$_POST['notes']  = $myrow["notes"];
		$_POST['inactive'] = $myrow["inactive"];
	}

	start_outer_table(TABLESTYLE2);
	table_section(1);
	table_section_title(_("Name and Address"));

	text_row(_("Customer Name:"), 'CustName', $_POST['CustName'], 40, 80);
	text_row(_("Customer Short Name:"), 'cust_ref', null, 30, 30);
	textarea_row(_("Address:"), 'address', $_POST['address'], 35, 5);

	text_row(_("GSTNo:"), 'tax_id', null, 40, 40);


	if (!$selected_id || is_new_customer($selected_id) || (!key_in_foreign_table($selected_id, 'debtor_trans', 'debtor_no') &&
		!key_in_foreign_table($selected_id, 'sales_orders', 'debtor_no'))) 
	{
		currencies_list_row(_("Customer's Currency:"), 'curr_code', $_POST['curr_code']);
	} 
	else 
	{
		label_row(_("Customer's Currency:"), $_POST['curr_code']);
		hidden('curr_code', $_POST['curr_code']);				
	}
	sales_types_list_row(_("Sales Type/Price List:"), 'sales_type', $_POST['sales_type']);

	if($selected_id)
		record_status_list_row(_("Customer status:"), 'inactive');
	elseif (isset($auto_create_branch) && $auto_create_branch == 1)
	{
		table_section_title(_("Branch"));
		text_row(_("Phone:"), 'phone', null, 32, 30);
		text_row(_("Secondary Phone Number:"), 'phone2', null, 32, 30);
		text_row(_("Fax Number:"), 'fax', null, 32, 30);
		email_row(_("E-mail:"), 'email', null, 35, 55);
		sales_persons_list_row( _("Sales Person:"), 'salesman', null);
	}
	table_section(2);

	table_section_title(_("Sales"));

	percent_row(_("Discount Percent:"), 'discount', $_POST['discount']);
	percent_row(_("Prompt Payment Discount Percent:"), 'pymt_discount', $_POST['pymt_discount']);
	amount_row(_("Credit Limit:"), 'credit_limit', $_POST['credit_limit']);

	payment_terms_list_row(_("Payment Terms:"), 'payment_terms', $_POST['payment_terms']);
	credit_status_list_row(_("Credit Status:"), 'credit_status', $_POST['credit_status']); 
	$dim = get_company_pref('use_dimension');
	if ($dim >= 1)
		dimensions_list_row(_("Dimension")." 1:", 'dimension_id', $_POST['dimension_id'], true, " ", false, 1);
	if ($dim > 1)
		dimensions_list_row(_("Dimension")." 2:", 'dimension2_id', $_POST['dimension2_id'], true, " ", false, 2);
	if ($dim < 1)
		hidden('dimension_id', 0);
	if ($dim < 2)
		hidden('dimension2_id', 0);

	if ($selected_id)  {
		start_row();
		echo '<td class="label">'._('Customer branches').':</td>';
	  	hyperlink_params_td($path_to_root . "/sales/manage/customer_branches.php",
			'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
			"debtor_no=".$selected_id.(@$_REQUEST['popup'] ? '&popup=1':''));
		end_row();
	}

	textarea_row(_("General Notes:"), 'notes', null, 35, 5);
	if (!$selected_id && isset($auto_create_branch) && $auto_create_branch == 1)
	{
		table_section_title(_("Branch"));
		locations_list_row(_("Default Inventory Location:"), 'location');
		shippers_list_row(_("Default Shipping Company:"), 'ship_via');
		sales_areas_list_row( _("Sales Area:"), 'area', null);
		tax_groups_list_row(_("Tax Group:"), 'tax_group_id', null);
	}
	end_outer_table(1);

	div_start('controls');
	if (!$selected_id)
	{
		submit_center('submit', _("Add New Customer"), true, '', 'default');
	} 
	else 
	{
		submit_center_first('submit', _("Update Customer"), 
		  _('Update customer data'), @$_REQUEST['popup'] ? true : 'default');
		submit_return('select', $selected_id, _("Select this customer and return to document entry."));
		submit_center_last('delete', _("Delete Customer"), 
		  _('Delete customer data if have been never used'), true);
	}
	div_end();
}

//--------------------------------------------------------------------------------------------

check_db_has_sales_types(_("There are no sales types defined. Please define at least one sales type before adding a customer."));
 
start_form();

if (db_has_customers()) 
{
	start_table(TABLESTYLE_NOBORDER);
	start_row();
	customer_list_cells(_("Select a customer: "), 'customer_id', null,
		_('New customer'), true, check_value('show_inactive'));
	check_cells(_("Show inactive:"), 'show_inactive', null, true);
	end_row();
	end_table();

	if (get_post('_show_inactive_update')) {
		$Ajax->activate('customer_id');
		set_focus('customer_id');
	}
} 
else 
{
	hidden('customer_id');
}

if (!$selected_id || list_updated('customer_id'))
	unset($_POST['_tabs_sel']); // force settings tab for new customer

tabbed_content_start('tabs', array(
		'settings' => array(_('&General settings'), $selected_id),
		'contacts' => array(_('&Contacts'), $selected_id),
		'transactions' => array(_('&Transactions'), $selected_id),
		'orders' => array(_('Sales &Orders'), $selected_id),
		'products' => array(_('&Products'), $selected_id),
		'consumables' => array(_('&Consumables'), $selected_id),
		'packaging' => array(_('&Packaging'), $selected_id),
		'costing' => array(_('&Costing'), $selected_id),
		'Quotation' => array(_('&Quotation'), $selected_id),
	));
	
	switch (get_post('_tabs_sel')) {
		default:
		case 'settings':
			$page_security = 'SA_CUSTOMERGNRLTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			customer_settings($selected_id); 
			break;
		case 'contacts':
			$page_security = 'SA_CUSTOMERCNCTTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$contacts = new contacts('contacts', $selected_id, 'customer');
			$contacts->show();
			break;
		case 'transactions':
			$page_security = 'SA_CUSTOMERTRSNTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			include_once($path_to_root."/sales/inquiry/customer_inquiry.php");
			break;
		case 'orders':
			$page_security = 'SA_CUSTOMERSORDTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			$_POST['type'] = ST_SALESORDER;
			include_once($path_to_root."/sales/inquiry/sales_orders_view.php");
			break;
		case 'products':
			$page_security = 'SA_CUSTOMERPRODTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			include_once($path_to_root."/sales/inquiry/sales_products_view.php");
			break;	
		case 'consumables':
			$page_security = 'SA_CUSTOMERCONSTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			include_once($path_to_root."/sales/inquiry/sales_consumable_view.php");
			break;
		case 'packaging':
			$page_security = 'SA_CUSTOMERPACKTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			include_once($path_to_root."/sales/inquiry/packaging.php");
			break;
		case 'costing':
			$page_security = 'SA_CUSTOMERCOSTTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			include_once($path_to_root."/sales/manage/costing.php");
			break;
		case 'Quotation':
			$page_security = 'SA_CUSTOMERQUOTTAB';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['customer_id'] = $selected_id;
			$_GET['popup'] = 1;
			$_POST['type'] = ST_SALESQUOTE;
			include_once($path_to_root."/sales/inquiry/sales_orders_view.php");
			break;	

	};
br();
tabbed_content_end();

hidden('popup', @$_REQUEST['popup']);
end_form();
end_page(@$_REQUEST['popup']);
//-----------------  Popup code (keshav) ------------------------
echo "<div class='' id='dialog' style='position: absolute;min-width: 700px;left: 300px;display: block;background: #EEEEEE;top: 65px;max-height: 500px;
overflow-y:scroll;'></div>";
//--------------------- end code ---------------------------
?>

<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#collection_id',function(){
		var collect_name = $("#collection_id").val();	
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
				$("#range_id_code").val('-1');
			}else{
				$("#range_span").show();		
				
			}
			
		});
		
</script>


<!---------------- show pop window --------------------------->
<center>
<div id="display_carton">
<center><span class="headingtext">Carton:</span></center>
<div id="carton_table"><center><table class="tablestyle2" cellpadding="2" cellspacing="0">
<tbody><tr valign="top"><td>
<table class="tablestyle">
<tbody><tr><td class="label">Type:</td><td><span id="carton_type_d">
</span>
</td>
</tr>
    <tr id="mastercarton_d"><td class="label">Master Carton:</td><td>
            <span id="master_carton_id_d" ></span></td>
     </tr>
    <tr>
          <td class="label">Carton qty:</td><td><span id="cat_qty_d"></span></td>
    </tr>
   <tr>    <td class="label">Margin:</td><td>
			<label style="margin-left:10px;">W = <span id="mar_w_d"></span></label>
            <label style="margin-left:10px;">D = <span id="mar_h_d"></span></label>
            <label style="margin-left:10px;">H = <span id="mar_d_d"></span></label> 
            </td>
   </tr>
   <tr><td class="label">Carton type:</td>
		<td><span id="_carton_type_id_sel"><span id="carton_type_id_d"></span>
		</span> </td>
  </tr>
  <tr><td class="label">Gross weight :</td><td><span id="gross_weight_d"></span></td>
</tr>
</tbody></table>
</td><td style="border-left:1px solid #cccccc;">
<table class="tablestyle">
<tbody><tr><td class="label">Code:</td>
			<td><span id="code_d"></span></td>
  </tr>
  <tr><td class="label">Pkg. size for catoon:</td>
      <td><label style="margin-left:10px;">W = <span id="pkg_s_w_d"></span></label>
          <label style="margin-left:10px;">D = <span id="pkg_s_h_d"></span></label>
          <label style="margin-left:10px;">H = <span id="pkg_s_d_d"></span></label>
       </td>
   </tr>
   <tr><td class="label">Carton Size:</td>
       <td><label style="margin-left:10px;">W = <span id="car_s_w_d"></span></label>
       <label style="margin-left:10px;">D = <span id="car_s_h_d"></span></label>
       <label style="margin-left:10px;">H = <span id="car_s_d_d"></span></label>
      </td>
   </tr>
   <tr><td class="label">Net weight :</td>
       <td><span id="net_weight_d"></span></td>
    </tr>
</tbody></table>
</td></tr>
</tbody></table></center>

</div>

<center><span class="headingtext">Carton Part Sectoin</span></center>
<div id="con_table"><center>
<table class="tablestyle" colspan="7" width="40%" cellpadding="2" cellspacing="0">
        <tr>
            <td class="tableheader">Part Type</td>
            <td class="tableheader">Qty</td>
        </tr>
   </table>
<table class="tablestyle" id="part_section_table" colspan="7" width="40%" cellpadding="2" cellspacing="0">
        
   </table></center>
<br></div>

<div>
<center><span class="headingtext">Screen</span></center>

	<table class="tablestyle" id="screen_img_table"  cellspacing="5">
    	<tr>
        <td class="screen_img"><label id="label_0"></label><img src="" id="img_0" /> </td>
        <td class="screen_img"><label id="label_1"></label><img src="" id="img_1" /> </td>
        <td class="screen_img"><label id="label_2"></label><img src="" id="img_2"  /> </td>
        </tr>
        <tr>
        <td class="screen_img"><label id="label_3"></label><img src="" id="img_3"  /> </td>
        <td class="screen_img"><label id="label_4"></label><img src="" id="img_4"  /> </td>
        <td class="screen_img"><label id="label_5"></label><img src="" id="img_5"  /> </td>
        </tr>    
    </table>
</div>


<center> 
    <table class="tablestyle" >
        <tr><td class="tableheader"  onclick="$('#display_carton').hide();" style="cursor:pointer" ><strong>Close</strong></td>
        </tr>
   </table>
</center>
</div>

</center>
<!------------------ end pop window ---------------------------->
<style>
button#Addtolist {
	align-content: center;
	margin-left: 500px;
}
body{ position:relative; }
#display_carton {
display:none;
margin:auto !important;
left:10%;
top:10% !important;
height:500px;
background:#FDFCFF;
border:#000 2px solid;
position:fixed;
width:80%;
}

#screen_img_table
{
	height:200px !important;
}


.screen_img
{
	width:90px !important;
	height:90px !important;
}
.screen_img img
{
	max-width:100%;
	max-height: 100%;
}

</style>

<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
</script>
<script type="text/javascript">
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	// santosh
	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
					$('#packaging_finish_code').append(data);
				}
		});
		
	});
	
		$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_packaging_size.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
				var size = data.split(',');
					$('#pkg_s_w').val(size[0]);
					$('#pkg_s_d').val(size[1]);
					$('#pkg_s_h').val(size[2]);
				}
		});
		
	});

	$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		if(design_id != '-1'){
			$.ajax({
				url: "get_carton_part_section.php",
				method: "POST",
				data: { f_id : design_id },
				success: function(data){
						$( "option" ).remove( ".part_extra" );
						$('#part_name_id').append(data);
					}
			});
		}else{
			$( "option" ).remove( ".part_extra" );
		}
		
	});

	$(document).on('change','#collection_id', function(){
		var coll_id = $('#collection_id').val();
		if(coll_id == 'NonCollection'){
			var rang_id = $('#range_id_code').val(-1);
		}
		
	});
/*
$( "#code" ).click(function() {
	
		var carton_type = $('#carton_type :selected').text();
		
		//var packaging_finish_code = $('#packaging_finish_code').val();
		var packaging_finish_code = $('#packaging_finish_code :selected').text();
		
		//alert(carton_type);
		//alert(packaging_finish_code);
		var result_code = packaging_finish_code.split('-');
		
    	//alert(result[0]);
		//alert(packaging_finish_code);
		//alert(carton_type);
		if(carton_type == 'Master Carton')
	    {
			var new_code = "MC" + result_code[0] + "-A";
			//alert(new_code);
			$('#code').val(new_code);
		}else
		{
			var new_code = "IC" + result_code[0] + "-A";
			//alert(new_code);
			$('#code').val(new_code);
		}
});	*/

$( "#code" ).click(function() {
	
		var carton_type = $('#carton_type').val();
		var packaging_finish_code = $('#packaging_finish_code').val();
		
		
		//alert(carton_type);
		if(packaging_finish_code != '-1'){
			$.ajax({
			url: "pack_carton_type_code.php",
			method: "POST",
            data: { id : packaging_finish_code,car_type : carton_type},
			success: function(data){

				
					$('#code').val(data);
					
				}
			});
		}
			
		
});	


$(document).on('change','#carton_type',function(){
		var carton_type = $("#carton_type").val();
		//alert(carton_type);
		 if(carton_type == '102'){
			$.ajax({
					url: "get_master_carton.php",
					method: "POST",
					data: { carton_type : carton_type },
					success: function(data){
						
						if(data == ''){
							alert('Not Master Carton is created yet!');
							$("#carton_type").val('101');
						}else{
							$('#master_carton_id').append(data);
							$("#mastercartontr").show();	
						}
							
						}
				});
			}else{
				$("#mastercartontr").hide();	
				$("#mastercartontr").val('-1');
			}
		
	});			
		

	//   -----  code by bajrang ---   carton size  - -------
$(document).on('click','#car_s_w',function(){
		var pkg_s_w = $('#pkg_s_w').val();
		var mar_w = $('#mar_w').val();
		var car_s_w = parseInt(pkg_s_w)+parseInt(mar_w);
		$('#car_s_w').val(car_s_w);
		
});
$(document).on('click','#car_s_d',function(){
		var pkg_s_d = $('#pkg_s_d').val();
		var mar_d = $('#mar_d').val();
		var car_s_d = parseInt(pkg_s_d) + parseInt(mar_d);
		$('#car_s_d').val(car_s_d);
		
});
$(document).on('click','#car_s_h',function(){
		var pkg_s_h = $('#pkg_s_h').val();
		var mar_h = $('#mar_h').val();
		var car_s_h = parseInt(pkg_s_h)+parseInt(mar_h);
		$('#car_s_h').val(car_s_h);
		
});	



//  code by bajrang  - --- for carton part section
/*$(document).on('change','#part_name_id',function(){
		var part_id = $('option:selected', this).attr('data-value');
		$('#part_qty').val(part_id);
		
		 
	});
	
*/	

$(document).on('blur','#part_qty',function(){
		var part_id = $('option:selected', '#part_name_id').attr('data-value');
		var part_qty = $(this).val();
		if(part_qty > part_id)
		{
			alert("part quantity cannot be greater than "+part_id);
			$(this).val('');
			$(this).focus();
			return false;
		}
		else
		{
			return true;
		}
		
		 
	});
	
$(document).on('click','#CPartAddItem',function(){
		var part_id = $('#part_name_id').attr('data-value');
		var part_qty = $(this).val();
		if(part_qty > part_id)
		{
			alert("part quantity cannot be greater than "+part_id);
			$(this).val('');
			$(this).focus();
			return false;
		}
		else
		{
			return true;
		}
		
		 
	});
//----------------- show pop jquery -------------
//----------------- show pop jquery -------------
function display_carton_part_summary(carton_val, path)
{
	//var carton_val = $("#display_carton").attr('href');
	$("#display_carton").show(); 
	//alert(carton_val);
	var dataObj = JSON.parse(carton_val);
	
	var master_carton_id = dataObj.master_carton_id;
	if(master_carton_id == -1)
	{
		$("#mastercarton_d").hide();
	}
	else
	{
		$('#mastercarton_d').show();
		$('#master_carton_id_d').html(master_carton_id);
	}
	var mar_dimension = dataObj.margin_dimension.split(',');
	
	$('#mar_w_d').html(mar_dimension[0]);
	$('#mar_h_d').html(mar_dimension[1]);
	$('#mar_d_d').html(mar_dimension[2]);
	$('#net_weight_d').html(dataObj.net_weight);
	$('#gross_weight_d').html(dataObj.gross_weight);
	$('#code_d').html(dataObj.code);
	
	var pkg_dimension = dataObj.pkg_dimension.split(',');
	$('#pkg_s_w_d').html(pkg_dimension[0]);
	$('#pkg_s_h_d').html(pkg_dimension[1]);
	$('#pkg_s_d_d').html(pkg_dimension[2]);
	
	
	var car_dimension = dataObj.carton_dimension.split(',');
	$('#car_s_w_d').html(car_dimension[0]);
	$('#car_s_h_d').html(car_dimension[1]);
	$('#car_s_d_d').html(car_dimension[2]);
	//console.log(dataObj[0]);
	
	$('#cat_qty_d').html(dataObj.quantity);
	//var len = JSON.parse(dataObj[0]);
	var len = Object.keys(dataObj[0]).length+'';
	var index = 100;
	$('#part_section_table').empty();
	for(var k=0; k<len; k++)
	{
		//$('#part_section_table').append("<tr><td>"+dataObj[0][index].part_name_id+"</td><td>"+dataObj[0][index].part_qty+"</td></tr>");
		index++;
	}
	
	
	
	//$('#carton_type_id_d').html(dataObj.carton_type_id);
	
	var carton_id = dataObj.carton_type_id;
	  $.ajax({
			url: "get_carton_type.php",
			method: "POST",
			data: { carton_id : carton_id },
			success: function(data){
				if(data != ''){
					$('#carton_type_d').html(data);
				}
					
				}
		});
		
	var carton_type_id  = dataObj.carton_type;
	  $.ajax({
			url: "get_carton_type_name.php",
			method: "POST",
			data: { carton_type_id : carton_type_id },
			success: function(data){
				if(data != ''){
					$("#carton_type_id_d").html(data);
				}
					
				}
		});
		
	var screenimages = dataObj.images.split(',');
	var screenlables = dataObj.lables.split(',');
	for(var i=0; i<screenimages.length; i++){
     $("#img_"+i).attr('src',path+screenimages[i]);
	  //$("#label_"+i).html(screenlables[i]);
	  
	  var carton_type_id  = dataObj.carton_type_id;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { label_id : screenlables[i] },
			success: function(data){
				if(data != ''){
					$("#label_"+i).html(data);
				}
					
				}
		});
	  
	 }
	 
	 
	
}


//---------------- end pop jquery------------------
//------------------------------------------------------------//
//----------------- show pop jquery -------------
function display_carton_part_summary_search(carton_val, carton_part, path)
{
	
	//var carton_val = $("#display_carton").attr('href');
	$("#display_carton").show(); 
	
	//alert(carton_val);
	var dataObj = JSON.parse(carton_val);
	
	var master_carton_id = dataObj.master_carton_id;
	if(master_carton_id == -1)
	{
		$("#mastercarton_d").hide();
	}
	else
	{
		$('#mastercarton_d').show();
		$('#master_carton_id_d').html(master_carton_id);
	}
	$('#cat_qty_d').html(dataObj.quantity);
	
	var mar_dimension = dataObj.margin_dimension.split(',');
	
	$('#mar_w_d').html(mar_dimension[0]);
	$('#mar_h_d').html(mar_dimension[1]);
	$('#mar_d_d').html(mar_dimension[2]);
	$('#net_weight_d').html(dataObj.gross_weight);
	$('#gross_weight_d').html(dataObj.net_weight);
	$('#code_d').html(dataObj.code);
	
	var pkg_dimension = dataObj.pkg_dimension.split(',');
	$('#pkg_s_w_d').html(pkg_dimension[0]);
	$('#pkg_s_h_d').html(pkg_dimension[1]);
	$('#pkg_s_d_d').html(pkg_dimension[2]);
	
	//console.log(dataObj[0]);
	
	
	//var len = JSON.parse(dataObj[0]);
	
	
	var car_dimension = dataObj.carton_dimension.split(',');
	$('#car_s_w_d').html(car_dimension[0]);
	$('#car_s_h_d').html(car_dimension[1]);
	$('#car_s_d_d').html(car_dimension[2]);
	
	//$('#carton_type_id_d').html(dataObj.carton_type_id);
	
	var carton_id = dataObj.carton_type_id;
	  $.ajax({
			url: "get_carton_type.php",
			method: "POST",
			data: { carton_id : carton_id },
			success: function(data){
				if(data != ''){
					$('#carton_type_d').html(data);
				}
					
				}
		});
		
	var carton_type_id  = dataObj.carton_type;
	  $.ajax({
			url: "get_carton_type_name.php",
			method: "POST",
			data: { carton_type_id : carton_type_id },
			success: function(data){
				if(data != ''){
					$("#carton_type_id_d").html(data);
				}
					
				}
		}); 

		var images = dataObj.images.split(',');
		var lables = dataObj.lables.split(',');
		for(var i=0; i<images.length; i++){
     $("#img_"+i).attr('src',path+images[i]);
	  //$("#label_"+i).html(screenlables[i]);
	  
	  var carton_type_id  = dataObj.carton_type_id;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { label_id : lables[i] },
			success: function(data){
				if(data != ''){
					$("#label_"+i).html(data);
				}
					
				}
		});
	  
	 }
	
	 var dataPart = JSON.parse(carton_part);
	var len = dataPart.length;
	$('#part_section_table').empty();
	for(var k=0; k<len; k++)
	{
	 var part_type  = dataPart[k].part_type;
	  $.ajax({
		    url: "get_label_name.php",
			async:false,
			method: "POST",
			data: { part_id : part_type },
			success: function(data){
					part_type = data;
				}	
			});
		$('#part_section_table').append("<tr><td>"+part_type+"</td><td>"+dataPart[k].quantity+"</td></tr>");
		
	}
	
}

$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});


	$(document).on('click','.view_all',function(){
		var id = $(this).attr("id");
		var k = id.split('_')
		var datastring = 'finish_id='+k[1];
		
		$.ajax({
		type: "POST",
		url: "../inquiry/view_details.php",
		data: datastring,
			success: function(result){
			$("#dialog").show().html(result);
			}
		
		});
	$(document).on('click','#close',function(){
			$("#dialog").hide();
		});
	 
	});
	/*
	$(document).on('click','#save',function(){
	    
		var new_master_id = $("#slc_master").val();
		var new_cons_id = $("#sub_master").val();
		var new_quantity = $("#new_quantity").val();
		var new_customer_id = $("#new_customer_id").val();
		var datastring = 'customer_id='+new_customer_id + '&master_id='+new_master_id + '&cons_id='+ new_cons_id + '&quantity='+ new_quantity;
		
		$.ajax({
		type: "POST",
		url: "../inquiry/customer_consumable_relation.php",
		data: datastring,
			success: function(result){
			alert(result);
			}
		});
	});*/
<!-----------Keshav 13 oct 2015 ----------------------------------->	
$(document).on('change','#packaging_finish_code',function(){
    var finish_code_id = $("#packaging_finish_code").val();
	var datastring = 'finish_code_id='+finish_code_id;
	$.ajax({
		type: "POST",
		url: "../inquiry/auto_fill_buyer.php",
		data: datastring,
			success: function(result){
			$("input[name=buyers_code]").val(result);
			}
		});
	});
	
	$(document).on('click','#pic',function(){
    	$("#frm_data").attr('enctype','multipart/form-data');
		
	});

<!--end here-->

// multiple category script goes here
var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }
$(document).click(function(e) {

	if($(e.target).attr('class') != 'overSelect')
	{
		if($(e.target).attr('class') != $('.selectBox'))
		{
			if ($(e.target).attr('id') != $('#checkboxes') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = document.getElementById("checkboxes");
				checkboxes.style.display = "none";
				$('#checkboxes').fadeOut('slow');
					expanded = false; 
			}
		}
		}
	}
});

$(document).on('click','#select_all',function(){
		if(this.checked){
            $('.check').each(function(){
                this.checked = true;
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
            });
        }else{
             $('.check').each(function(){
                this.checked = false;
				
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
				
            });
        }
	});



$('body').on('click','.check', function() {
	
	if($('.check:checked').length == $('.check').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
		
	
	var arr = [];
	$.each($("input[name^='category_id']:checked"), function(){            
    	arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#pro_categories').val(prod.join(", "));
  $('#pro_cat').html(prod.join(", "));
});




$(document).on('click','.view_submit',function(){
	
	$("#view_pack").val("view");
	
});
$(document).on('click','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
			}else{
				$("#range_span").show();		
				
			}
			 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
});

$(document).on('change','#packaging_finish_code',function(){
    var finish_code_id = $("#packaging_finish_code").val();
  var datastring = 'finish_code_id='+finish_code_id;
  $.ajax({
    type: "POST",
    url: "../inquiry/auto_fill_buyer.php",
    data: datastring,
      success: function(result){
        if(result == null || result == ""){
          $("input[name=buyers_code]").val("");
          alert("No Buyers Code Found");
          }
          else
      $("input[name=buyers_code]").val(result);
      }
    });
  });
  
  $(document).on('click','#pic',function(){
      $("#frm_data").attr('enctype','multipart/form-data');
    
  });
  
</script>

<script type="text/javascript">
	
$(document).on("click","#edit-cost", function(){
	var customer_id = $("#customer_id").val();
	var left  = ($(window).width()/2)-(1150/2);
    var top   = ($(window).height()/2)-(600/2);
	 myWindow = window.open("edit.php?customer_id="+customer_id, "toolbar=yes, scrollbars=yes", "width=1150, height=600, top="+top+", left="+left);   // Opens a new window
     var timer = setInterval(function() {   
	    if(myWindow.closed) {  
	        clearInterval(timer);  
	        $("button.current").click();
	    }  
	}, 500);  
});



</script>