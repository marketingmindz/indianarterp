<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_finish_product_list($category_id, $range_id)
{	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get finish product list");
	return $result;
}
function get_product_details($finish_code_id)
{	$sql = "SELECT finish_product_name, finish_comp_code FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_code_id);
	$result = db_query($sql, "could not get finish product list");
	$row = db_fetch($result);
	return $row;
}

function get_product_complete_details($finish_code_id)
{	$sql = "SELECT *, finish_product_name, finish_comp_code FROM ".TB_PREF."finish_product f
			LEFT JOIN ".TB_PREF."item_category m on f.category_id = m.category_id 
			LEFT JOIN ".TB_PREF."item_range r on r.id = f.range_id

			 WHERE finish_pro_id=".db_escape($finish_code_id);
	$result = db_query($sql, "could not get finish product list");
	$row = db_fetch($result);
	//print_r($row);
	return $row;
}

function get_product_cost($ref_id, $version){
	$sql = "select final_total_cost from ".TB_PREF."locked_final_costing where ref_id = ".db_escape($ref_id)." && version =".db_escape($version);
	$result = db_query($sql, "Could not get final total cost.");
	if(db_num_rows($result) > 0){
		$cost = db_fetch($result);
	}
	else{
		return "Cost is not entered.";
	}
	return $cost['final_total_cost'];
}

function get_costing_version($finish_code_id)
{
	$sql = "select final_total_cost, c.ref_id from ".TB_PREF."locked_final_costing f left join ".TB_PREF."costing_reference c on c.ref_id = f.ref_id where finish_code_id = ".db_escape($finish_code_id)." && f.version = c.current_version";
	$result = db_query($sql, "Costing version could not be retrieved");
	if(db_num_rows($result) > 0)
	{
		$row = db_fetch($result);
	}else{
		return false;
	}
	return $row;
}

function get_customer_cost($finish_code_id, $customer_id)
{
	$sql = "select final_total_cost from ".TB_PREF."cust_final_costing where finish_code_id = ".db_escape($finish_code_id)." && customer_id = ".db_escape($customer_id);          
	$result = db_query($sql, "Costing could not be retrieved");
	if(db_num_rows($result) > 0)
	{
		$row = db_fetch($result);
	}else{
		return false;
	}
	return $row;
}




function get_wood_name($wood_id){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($wood_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_rate($consumable_id)
{
	$sql = "SELECT max(purchase_price) FROM ".TB_PREF."supp_product where sub_category = ".db_escape($consumable_id);
	$query1 = db_query($sql,"get the ref code.");
	
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $purchase_price = $row['0'];     
	}
	if($purchase_price != "" && $purchase_price != NULL)
	{
		return $purchase_price;
	}
	else
	{
		$purchase_price = 0;
		return $purchase_price;
	}
}

function get_cons_select_name($cons_select){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_type_name($cons_type){
	$sql = "SELECT master_name FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}


function check_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_mfrg_id($finish_pro_id, $version)
{
	$sql = "SELECT id FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id)." && version=".db_escape($version);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function get_finishing_cost_id($finish_code_id, $version)
{
	$sql = "SELECT id FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code_id)." && version=".db_escape($version);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function insert_manufacturing_cost($manuf_cost, &$wood_session, &$labour_session)
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($manuf_cost['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version']+1;
	
	$sql = "INSERT INTO ".TB_PREF."manufacturing_cost(finish_code_id,total_cons_cost,total_finish_cons_cost,total_fabric_cost,total_wood_cost,
	total_labour_cost, extra_percent, extra_amount, total_cost,admin_percent,admin_cost,total_cp,profit_percent,profit,total_mfrg, version, date) VALUES(";
	$sql .= db_escape($manuf_cost['finish_code_id']) . "," .
	db_escape($manuf_cost['total_cons_cost']) . "," .
	db_escape($manuf_cost['total_finish_cons_cost']) . "," .
	db_escape($manuf_cost['total_fabric_cost']) . "," .
	db_escape($manuf_cost['total_wood_cost']) . "," .
	db_escape($manuf_cost['total_labour_cost']) . "," .
	db_escape($manuf_cost['extra_percent']) . "," .
	db_escape($manuf_cost['extra_amount']) . "," .
	db_escape($manuf_cost['total_cost']) . "," .
	db_escape($manuf_cost['admin_percent']) . "," .
	db_escape($manuf_cost['admin_cost']) . "," .
	db_escape($manuf_cost['total_cp']) . "," .
	db_escape($manuf_cost['profit_percent']) . "," .
	db_escape($manuf_cost['profit']) . "," .
	db_escape($manuf_cost['total_mfrg']) . ",'$version','".date2sql($_POST['date'])."')";
	
	db_query($sql, "Manufacturing cost is not inserted.");
	$mfrg_id = db_insert_id();
	
	$sql = "update ".TB_PREF."costing_version set version = ".db_escape($version)." where finish_code_id = ".db_escape($manuf_cost['finish_code_id']);
	db_query($sql, "Costing version is not updated");
	
	
	
	for($i = 0;$i < $_POST['design_cons_no'];$i++ )
	{
		$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['des_master_name_'.$i]). "," .
		db_escape($_POST['des_consumable_name_'.$i]). "," .
		db_escape($_POST['des_cons_unit_'.$i]). "," .
		db_escape($_POST['des_cons_qty_'.$i]). "," .
		db_escape($_POST['des_rate_'.$i]). "," .
		db_escape($_POST['des_cost_'.$i]). ")";
		db_query($sql, "Design consumable Costing is not inserted");
	}
	 
	 for($i = 0;$i < $_POST['finish_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['finish_master_name_'.$i]). "," .
		db_escape($_POST['finish_consumable_name_'.$i]). "," .
		db_escape($_POST['finish_cons_unit_'.$i]). "," .
		db_escape($_POST['finish_cons_qty_'.$i]). "," .
		db_escape($_POST['finish_cons_rate_'.$i]). "," .
		db_escape($_POST['finish_cost_'.$i]). ")";
		db_query($sql, "Finish consumable Costing is not inserted");
     }
	
	for($i = 0;$i < $_POST['fabric_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['fabric_name_'.$i]). "," .
		db_escape($_POST['perecentage_'.$i]). "," .
		db_escape($_POST['fabric_rate_'.$i]). "," .
		db_escape($_POST['fabric_cost_'.$i]). ")";
		db_query($sql, "Fabric consumable Costing is not inserted");
     }
	
	
	foreach ($wood_session->line_items as $line_no => $wood_line)
     {
		$sql = "INSERT INTO ".TB_PREF."manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($wood_line->wood_id). "," .
		db_escape($wood_line->cft). "," .
		db_escape($wood_line->rate). "," .
		db_escape($wood_line->amount). ")";
		db_query($sql, "Wood Costing is not inserted");
     }
	 
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_cost(mfrg_id, basic_price, total_special_cost, total_labour_cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($manuf_cost['basic_price']). "," .
		db_escape($manuf_cost['total_special_cost']). "," .
		db_escape($manuf_cost['total_labour_cost']). ")";
		db_query($sql, "Labour Costing is not inserted");

	 
	 foreach ($labour_session->line_items as $line_no => $labour_line)
     {
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($labour_line->labour_type). "," .
		db_escape($labour_line->special_rate). "," .
		db_escape($labour_line->labour_amount). "," .
		db_escape($labour_line->special_labour_cost). ")";
		db_query($sql, "Labour Costing is not inserted");
     } 
	return true; 
}

function get_finish_product_list_from_manufacturing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."manufacturing_cost m on m.finish_code_id = f.finish_pro_id WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get product list");
	return $result;
}

function get_total_mfrg_cost($finish_code)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total MFRG");
	$total_mfrg = db_fetch($result);
	return $total_mfrg['total_mfrg'];
}

function get_total_finishing_cost($finish_code)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total finishing");
	$total_finishing_cost = db_fetch($result);
	return $total_finishing_cost['total_finishing_cost'];
}

function get_total_percent($finish_code)
{
	$sql = "SELECT sanding_percent, polish_percent, packaging_percent, forwarding_percent FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total percent");
	$total_percent = db_fetch($result);
	return $total_percent;
}

function get_total_export_cost($finish_code)
{
	$sql = "SELECT sanding_cost, polish_cost, packaging_cost, forwarding_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total export cost");
	$total_export_cost = db_fetch($result);
	return $total_export_cost;
}


function check_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function insert_finishing_cost($finishing_cost, &$finishing_session)
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($finishing_cost['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version'];
	
	$sql = "INSERT INTO ".TB_PREF."finishing_cost(finish_code_id,sanding_percent, sanding_cost,polish_percent,polish_cost,
	packaging_percent,packaging_cost,forwarding_percent,forwarding_cost,total_special_cost,other_cost,total_finishing_cost,
	 total_amount, version, date) VALUES(";
	$sql .= db_escape($finishing_cost['finish_code_id']) . "," .
			db_escape($finishing_cost['sanding_percent']) . "," .
			db_escape($finishing_cost['sanding_cost']) . "," .
			db_escape($finishing_cost['polish_percent']) . "," .
			db_escape($finishing_cost['polish_cost']) . "," .
			db_escape($finishing_cost['packaging_percent']) . "," .
			db_escape($finishing_cost['packaging_cost']) . "," .
			db_escape($finishing_cost['forwarding_percent']) . "," .
			db_escape($finishing_cost['forwarding_cost']) . "," .
			db_escape($finishing_cost['total_special_cost']) . "," .
			db_escape($finishing_cost['other_cost']) . "," .
			db_escape($finishing_cost['total_finishing_cost']) . "," .
			db_escape($finishing_cost['total_amount']) . ",'$version','".date2sql($_POST['date'])."')";
	db_query($sql, "finishing cost is not inserted.");
	
	$finishing_cost_id = db_insert_id();
	
	foreach ($finishing_session->line_items as $line_no => $finish_line)
     {
		$sql = "INSERT INTO ".TB_PREF."finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
		$sql .= db_escape($finishing_cost_id). ", " . 
				db_escape($finish_line->special_cost_ref). "," .
				db_escape($finish_line->special_rate). "," .
				db_escape($finish_line->special_amount). "," .
				db_escape($finish_line->total). ")";
		db_query($sql, "finishing special cost Costing is not inserted");
     }
	return true; 
}




function insert_final_cost()
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($_POST['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version'];

	$currency_price = array();
	$curr = explode(",",$_POST['currencies']);
	for($i=0; $i<$_POST['total_currency']; $i++)
	{
		$currency_price[][$curr[$i]] = $_POST['currency'.$curr[$i]];
	}

	$currency = json_encode($currency_price);

	$sql = "INSERT INTO ".TB_PREF."final_costing(finish_code_id,total_mfrg_cost,
			total_finishing_cost, total_amount, final_profit_percent, final_profit, total_percent, total_export_cost,
			total_fob_cost, finishing_exp_percent, final_finishing_cost, sp_by_process, sp_by_lumsum, final_total_cost, currency_price, version, date) VALUES(";
	$sql .= db_escape($_POST['finish_code_id']) . "," .
			db_escape($_POST['total_mfrg_cost']) . "," .
			db_escape($_POST['total_finishing_cost']) . "," .
			db_escape($_POST['total_amount']) . "," .
			db_escape($_POST['final_profit_percent']) . "," .
			db_escape($_POST['final_profit']) . "," .
			db_escape($_POST['total_percent']) . "," .
			db_escape($_POST['total_export_cost']) . "," .
			db_escape($_POST['total_fob_cost']) . "," .
			db_escape($_POST['finishing_exp_percent']) . "," .
			db_escape($_POST['final_finishing_cost']) . "," .
			db_escape($_POST['sp_by_process']) . "," .
			db_escape($_POST['sp_by_lumsum']) . "," .
			db_escape($_POST['final_total_cost']) . "," .
			db_escape($currency).",'$version','".date2sql($_POST['date'])."')";
	db_query($sql, "final cost is not inserted.");
	return true; 
}

function get_finish_product_list_from_finishing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."finishing_cost m on m.finish_code_id = f.finish_pro_id WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get product list");
	return $result;
}
function check_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT total_final_cost FROM ".TB_PREF."final_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check Final Cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT final_total_cost FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_currency_name($id)
{
	$sql = "select currency_name from ".TB_PREF."currency_master where id=".$id;
	$result = db_query($sql);
	$row = db_fetch($result);
	return $row['currency_name'];
}

?>