<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
 //$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
//include($path_to_root . "/includes/db_pager.inc");

include($path_to_root . "/sales/includes/db/cost_editing_db.inc");
include($path_to_root . "/sales/includes/ui/costing_editing_ui.inc");
include($path_to_root . "/sales/includes/ui/finishing_cost_editing_ui.inc");
include($path_to_root . "/sales/includes/ui/final_costing_editing_ui.inc");
include($path_to_root . "/sales/includes/wood_cost_editing_class.inc");
include($path_to_root . "/sales/includes/labour_cost_editing_class.inc");
include($path_to_root . "/sales/includes/finish_cost_editing_class.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();



start_form();
	global $Ajax;
	
	$Ajax->activate('finish_code');
	$Ajax->activate('finish_code_id');
	
	
	
	$Ajax->activate('finish_product_div');

	
	
	echo "<input type='hidden' id='customer_id' value='".$_POST['customer_id']."'>";
	

	$sql = get_all_customer_product_relation($_POST['customer_id']);
	$result= db_query($sql,"not retrive");

	if(db_num_rows($result) > 0)
	{
		div_start('finish_product_div');
		start_table(TABLESTYLE, "width=700");
		$k = 0;
		$th = Array("Category", "Range", "Product Code", "Product Name", "Buyer Code", "Price");
		start_row();
		table_header($th);
		end_row();
		while ($myrow = db_fetch($result)){
			alt_table_row_color($k);
			$cost = "";
			$finish_pro_id = $myrow['finish_pro_id'];
			$product = get_product_complete_details($finish_pro_id);

			$cost = get_customer_cost($finish_pro_id, $_POST['customer_id']);
			if($cost){
				$cost = $cost['final_total_cost'];
			}
			else{
		    	$cost =  "Not Entered.";		
			}

			if($product['range_id'] == '-1'){
				$range_name = "Non-Collection";
			}
			else{
				$range_name = $product['range_name'];
			}

			label_cell($product['cat_name']);
			label_cell($range_name);
			label_cell($product['finish_comp_code']);
			label_cell($product['finish_product_name']);
			label_cell($myrow['buyers_code']);
			label_cell($cost);
			
			end_row();

			}
		end_table();
	 	div_end();

	    div_start();
	 		echo "<center><input type='button' id='edit-cost' value='Edit Cost'></center>";
	    div_end();
	}
	else{
		display_notification("No any product selected for this customer.");
	}
hidden('popup', @$_REQUEST['popup']);
end_form();

//end_page(@$_REQUEST['popup']);

?>

