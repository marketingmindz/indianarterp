<?php
$path_to_root = "..";
$page_security = 'SA_EXPORTSALESQUOTE';
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/ui/sales_order_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_order_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_types_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/sales/Classes/PHPExcel/IOFactory.php");

$js = '';

if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}


$order_no = $_REQUEST['trans_no'];
$trans_type = $_REQUEST['trans_type'];
$sheetType = $_REQUEST['sheetType'];
$offerOption = $_REQUEST['offerOption'];
$sale_header = get_sales_order_header($order_no, $trans_type);
$details = get_sales_order_details($order_no, $trans_type);

$product = Array();
while ($myrow = db_fetch($details))
{
	$product[] = $myrow;
}

foreach ($product as $pro) 
{
	$ranges[] = $pro["range_id"];
}
$range = array_unique($ranges);


$objPHPExcel = new PHPExcel();
$objTpl = PHPExcel_IOFactory::load("template.xls"); //load Excel template file

$ord_date = sql2date($sale_header['ord_date']);

$objTpl->setActiveSheetIndex(0);  //set first sheet as active

$objTpl->getActiveSheet()->setCellValue('C3', $sale_header['name']);  //set C1 to current date
$objTpl->getActiveSheet()->setCellValue('E3', $sale_header['reference']);

$objTpl->getActiveSheet()->setCellValue('C4', $ord_date);
$objTpl->getActiveSheet()->setCellValue('E4', sql2date($sale_header['delivery_date']));

$objTpl->getActiveSheet()->setCellValue('C5', $sale_header['address']);
$objTpl->getActiveSheet()->setCellValue('E5', $sale_header['consignee']);

$objTpl->getActiveSheet()->setCellValue('C6', $sale_header['contact_phone']);
$objTpl->getActiveSheet()->setCellValue('E6', $sale_header['shipment']);

$objTpl->getActiveSheet()->setCellValue('C7', $sale_header['contact_email']);

//---- set style of header items  -- -------
$objTpl->getActiveSheet()->getStyle('C5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objTpl->getActiveSheet()->getStyle('C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objTpl->getActiveSheet()->getStyle('C7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objTpl->getActiveSheet()->getStyle('E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$objTpl->getActiveSheet()->getStyle('A1:A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

$styleArray = array('font'  => array('bold'  => true));
$objTpl->getActiveSheet()->getStyle('A9:R9')->applyFromArray($styleArray);

$objTpl->setActiveSheetIndex(0)->mergeCells('A3:B3');
$objTpl->setActiveSheetIndex(0)->mergeCells('A4:B4');
$objTpl->setActiveSheetIndex(0)->mergeCells('A5:B5');
$objTpl->setActiveSheetIndex(0)->mergeCells('A6:B6');
$objTpl->setActiveSheetIndex(0)->mergeCells('A7:B7');
$objTpl->setActiveSheetIndex(0)->mergeCells('A8:B8');
$objTpl->getActiveSheet()->getColumnDimension('B:F')->setAutoSize(true);
$objTpl->getActiveSheet()->getColumnDimension('J:R')->setAutoSize(true);
$objTpl->getActiveSheet()->getColumnDimension('A')->setWidth(7);
$objTpl->getActiveSheet()->getColumnDimension('C')->setWidth(41);
$objTpl->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objTpl->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objTpl->getActiveSheet()->getRowDimension('9')->setRowHeight(30);

$objTpl->getActiveSheet()->getStyle('G9:I9')->getAlignment()->setWrapText(true);
$objTpl->getActiveSheet()->getStyle('L9:M9')->getAlignment()->setWrapText(true);
$objTpl->getActiveSheet()->getStyle('R9')->getAlignment()->setWrapText(true);
$objTpl->getActiveSheet()->getColumnDimension('G')->setWidth(13);
$objTpl->getActiveSheet()->getColumnDimension('H')->setWidth(13);
$objTpl->getActiveSheet()->getStyle('A3:E3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc0c0c0');
$objTpl->getActiveSheet()->getStyle('A5:E5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc0c0c0');
$objTpl->getActiveSheet()->getStyle('A7:E7')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc0c0c0');
    
$c = 0;
$r = 9;
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "S.NO.");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "RANGE NAME");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "IMAGES");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "PRODUCT NAME");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "CATEGORY NAME");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "PRODUCT CODE");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "ITEM SIZE (CM)");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "ITEM SIZE (INCH)");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "PKG BOX SIZE (CM)");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "MATERIAL");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "COLOR-FINISH");
$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "GROSS WT (KGS)");
if($offerOption != "wcbmquantity")
{
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, "BOX CBM(CM3)");
}
if($offerOption != "wcbmquantity" && $offerOption != "wquantity")
{	
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, 'QTY 20`');
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, 'QTY 40`');
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, 'QTY 40`HQ ');
}
if($offerOption != "wmoqprice")
{
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, 'MOQ');
}
if($offerOption != "wprice" && $offerOption != "wmoqprice")
{
	$objTpl->getActiveSheet()->setCellValueByColumnAndRow($c++, $r, 'FOB PRICE (USD)');
}


if($sheetType == "multiSheet")
{

	$totalSheet = count($range);

	$sheetIndex = $objTpl->getIndex($objTpl->getSheet(1));
	$objTpl->removeSheetByIndex($sheetIndex);
	$sheetIndex = $objTpl->getIndex($objTpl->getSheet(1));
	$objTpl->removeSheetByIndex($sheetIndex);

	for($k=1; $k<$totalSheet; $k++)
	{
		$objClonedWorksheet = clone $objTpl->getSheet(0);
		$objClonedWorksheet->setTitle($k.'Copy of Worksheet 1');
		$objTpl->addSheet($objClonedWorksheet);
	}

	$sheetNo = 0;
	foreach($range as $rangeId)
	{
		
		$objTpl->setActiveSheetIndex($sheetNo);  //set sheet as active
		$sheetNo++;

		$col = 0;
		$row = 10;
		$i = 1;
		$pro_count = count($product);
		foreach ($product as $pro) 
		{
			if($rangeId == $pro['range_id'])
			{
				$col = 0;

				$objTpl->getActiveSheet()->setTitle($pro['range_name'].' Range');
				if($pro['range_name'] == "Non-Collection")
				{
					$objTpl->getActiveSheet()->setTitle($pro['range_name']);
				}

				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $i);

				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['range_name']);
				$images = explode(",", $pro['product_image']);
				$y = 0;
				$image_count = 0;
				foreach ($images as $img) 
				{
					if(file_exists($path_to_root."/company/0/finishProductImage/".$img) && $img != "")
					{
						$gdImage = imagecreatefromjpeg($path_to_root."/company/0/finishProductImage/".$img);
						$info = getimagesize($path_to_root."/company/0/finishProductImage/".$img);
						$width = $info[0];
						$height = $info[1];
						if($width>$height)
						{
						    $ratio = $width/$height;
						    $set_width = $ratio*140;
						    $set_height = 1*140;
						}
						else
						{
						    $ratio = $height/$width;
						    $set_height = $ratio*140;
						    $set_width = 1*140;
						}
						$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
						$objDrawing->setName('Sample image');
						$objDrawing->setDescription('Sample image');
						$objDrawing->setImageResource($gdImage);
						$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
						$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
						$objDrawing->setHeight($set_height);
						$objDrawing->setWidth($set_width);
						$cord = PHPExcel_Cell::stringFromColumnIndex($col).$row;
						$objDrawing->setCoordinates($cord);
						$objDrawing->setWorksheet($objTpl->getActiveSheet());
						$objDrawing->setOffsetY($y);
						$y = $y+160;
						$image_count++;
						
					}
					
				}
				$col++;
				
				$objTpl->getActiveSheet()->getStyle('A9:A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('B9:B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objTpl->getActiveSheet()->getStyle('C9:C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objTpl->getActiveSheet()->getStyle('D9:D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objTpl->getActiveSheet()->getStyle('E9:E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('F9:F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $objTpl->getActiveSheet()->getStyle('G9:G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('H9:H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('I9:I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('J9:J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('K9:K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                
                $objTpl->getActiveSheet()->getStyle('A9:A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('B9:B'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('C9:C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('D9:D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('E9:E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('F9:F'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('G9:G'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('H9:H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('I9:I'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('J9:J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('K9:K'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
                $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
				
				
				$objTpl->getActiveSheet()->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc0c0c0');
				$objTpl->getActiveSheet()->getStyle('D'.$row.':Q'.$row)->getAlignment()->setWrapText(true);
				if($image_count == 0)
    			{
    			    $image_count = 1;
    			}
				$objTpl->getActiveSheet()->getRowDimension($row)->setRowHeight(180*$image_count);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['finish_product_name']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['cat_name']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['finish_comp_code']);
				
				
				$inch_asb_weight = $pro['asb_weight']/2.539998628;
    			$inch_asb_density = $pro['asb_density']/2.539998628;
    			$inch_asb_height = $pro['asb_height']/2.539998628;
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['asb_weight']."x".$pro['asb_density']."x".$pro['asb_height']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor($inch_asb_weight)."x".floor($inch_asb_density)."x".floor($inch_asb_height));
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['pkg_w']."x".$pro['pkg_d']."x".$pro['pkg_h']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['wood_name']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['color_name']);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['gross_weight']);
				
				if($offerOption != "wcbmquantity")
				{
					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, round($pro['pro_cbm'], 2));
				}
				if($offerOption != "wcbmquantity" && $offerOption != "wquantity")
				{
					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(65/$pro['pro_cbm'], 2)));

					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(52/$pro['pro_cbm'], 2)));

					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(26/$pro['pro_cbm'], 2)));
				}
				if($offerOption != "wmoqprice")
				{
					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['quantity']);
				}
				if($offerOption != "wprice" && $offerOption != "wmoqprice")
				{
					$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['unit_price']);
				}
				$row++;
				$i++;
				if($i == $pro_count+1){
        			$objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    			}
			}
		}
	}
}
else
{
	$totalSheet = count($range);

	/*$sheetIndex = $objTpl->getIndex($objTpl->getSheet(1));
	$objTpl->removeSheetByIndex($sheetIndex);
	$sheetIndex = $objTpl->getIndex($objTpl->getSheet(1));
	$objTpl->removeSheetByIndex($sheetIndex);
*/
	/*for($k=1; $k<$totalSheet; $k++)
	{
		$objClonedWorksheet = clone $objTpl->getSheet(0);
		$objClonedWorksheet->setTitle($k.'Copy of Worksheet 1');
		$objTpl->addSheet($objClonedWorksheet);
	}*/

	
	$col = 0;
	$row = 10;
	$i = 1;
	$pro_count = count($product);
	foreach ($product as $pro) 
	{

			$col = 0;

			$objTpl->getActiveSheet()->setTitle("Indian Art Quotation");

			$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $i);

			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['range_name']);
			$images = explode(",", $pro['product_image']);
			$y = 0;
			$image_count = 0;
			foreach ($images as $img) 
			{

				if(file_exists($path_to_root."/company/0/finishProductImage/".$img) && $img != "")
				{

					$gdImage = imagecreatefromjpeg($path_to_root."/company/0/finishProductImage/".$img);
					$info = getimagesize($path_to_root."/company/0/finishProductImage/".$img);
					$width = $info[0];
					$height = $info[1];
					if($width>$height)
					{
					    $ratio = $width/$height;
					    $set_width = $ratio*140;
					    $set_height = 1*140;
					}
					else
					{
					    $ratio = $height/$width;
					    $set_height = $ratio*140;
					    $set_width = 1*140;
					}
					$objDrawing = new PHPExcel_Worksheet_MemoryDrawing(); 
					$objDrawing->setName('Sample image');
					$objDrawing->setDescription('Sample image');
					$objDrawing->setImageResource($gdImage);
					$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
					$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
					$objDrawing->setHeight($set_height);
					$objDrawing->setWidth($set_width);
					$cord = PHPExcel_Cell::stringFromColumnIndex($col).$row;
					$objDrawing->setCoordinates($cord);
					$objDrawing->setWorksheet($objTpl->getActiveSheet());
					$objDrawing->setOffsetY($y);
					$y = $y+160;
					$image_count++;
				}
			}
			$col++;
			
			$objTpl->getActiveSheet()->getStyle('A9:A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('B9:B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objTpl->getActiveSheet()->getStyle('C9:C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objTpl->getActiveSheet()->getStyle('D9:D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objTpl->getActiveSheet()->getStyle('E9:E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('F9:F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objTpl->getActiveSheet()->getStyle('G9:G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('H9:H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('I9:I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('J9:J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('K9:K'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $objTpl->getActiveSheet()->getStyle('A9:A'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('B9:B'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('C9:C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('D9:D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('E9:E'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('F9:F'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('G9:G'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('H9:H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('I9:I'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('J9:J'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('K9:K'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
            $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			
			
			$objTpl->getActiveSheet()->getStyle('B'.$row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFc0c0c0');
			$objTpl->getActiveSheet()->getStyle('D'.$row.':Q'.$row)->getAlignment()->setWrapText(true);
			if($image_count == 0)
			{
			    $image_count = 1;
			}
			$objTpl->getActiveSheet()->getRowDimension($row)->setRowHeight(180*$image_count);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['finish_product_name']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['cat_name']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['finish_comp_code']);
			
			$inch_asb_weight = $pro['asb_weight']/2.539998628;
			$inch_asb_density = $pro['asb_density']/2.539998628;
			$inch_asb_height = $pro['asb_height']/2.539998628;
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['asb_weight']."x".$pro['asb_density']."x".$pro['asb_height']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor($inch_asb_weight)."x".floor($inch_asb_density)."x".floor($inch_asb_height));
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['pkg_w']."x".$pro['pkg_d']."x".$pro['pkg_h']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['wood_name']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['color_name']);
			$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['gross_weight']);
			
			if($offerOption != "wcbmquantity")
			{
				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, round($pro['pro_cbm'], 2));
	
			}
			if($offerOption != "wcbmquantity" && $offerOption != "wquantity")
			{
				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(65/$pro['pro_cbm'], 2)));

				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(52/$pro['pro_cbm'], 2)));

				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, floor(round(26/$pro['pro_cbm'], 2)));
			}
			
			if($offerOption != "wmoqprice")
			{
				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['quantity']);
			}
			if($offerOption != "wprice" && $offerOption != "wmoqprice")
			{
				$objTpl->getActiveSheet()->getStyleByColumnAndRow($col, $row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				$objTpl->getActiveSheet()->setCellValueByColumnAndRow($col++, $row, $pro['unit_price']);
			}

			$row++;
			$i++;
			if($i == $pro_count+1){
    			$objTpl->getActiveSheet()->getStyle('L9:L'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('M9:M'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('N9:N'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('O9:O'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('P9:P'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('Q9:Q'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objTpl->getActiveSheet()->getStyle('R9:R'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			}

	}

}


//prepare download
$filename = $sale_header['name']."-".$ord_date."-".mt_rand(1,100000).'.xls'; //just some random filename
header('Content-Type: application/vnd.ms-excel');
header('Content-Type: image/jpg');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
 
$objWriter = PHPExcel_IOFactory::createWriter($objTpl, 'Excel5');  //downloadable file is in Excel 2003 format (.xls)
$objWriter->save('php://output'); 

?>