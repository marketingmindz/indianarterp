$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
	var slc_master = $(this).val();
	$.ajax({
		url: "sub_master_calling.php",
		method: "POST",
		data: { id : slc_master},
		success: function(data){
				var select_val = $('#sub_master');
				//alert(data);
				select_val.empty().append(data);
			}
	});
	return false;
});
	

	$(document).on('change','#slc_category',function(){
		 //alert( this.value ); // or $(this).val()
		  $('#collection_id').val('SelectCollection');
		var catval = parseInt(this.value);
		if ($.inArray(catval, [28, 41, 60, 64, 77, 79]) > -1) {
			
			  $('#tr_structure_wood').show();
			 $('#tr_no_setting').show();	
		}else{
			
			 $('#tr_structure_wood').hide();
			 $('#tr_no_setting').hide();
			 $('#no_setting').val('');
			 $('#structure_wood_id').val('');
			 	
		}
	});
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
				$('#range_span').val('-1');
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	



var expanded = false;
    function showCheckboxes(check) {
    	$('.multipleSelect').hide();
        if (!expanded) {
            $("#"+check).show();
            expanded = true;
        } else {
            $("#"+check).hide();
            expanded = false;
        }
    }

$(document).on('focusout','.selectBox',function(){
		var checkboxes = document.getElementById("checkboxes");
		checkboxes.style.display = "none";
            expanded = false;
		return true;
		
	});
	
$(document).ready(function()
{
    $(".multiselect").mouseup(function(e)
    {
        var subject = $(".multiselect"); 

        if(e.target.id != subject.attr('id') && !subject.has(e.target).length)
        {
            subject.fadeOut();
        }
    });
});

$(document).click(function(e) {

	if($(e.target).attr('class') != 'overSelect')
	{
		if($(e.target).attr('class') != $('.selectBox'))
		{
			if ($(e.target).attr('id') != $('#checkboxes') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = document.getElementById("checkboxes");
				checkboxes.style.display = "none";
				$('#checkboxes').fadeOut('slow');
				$('.multipleSelect').hide();
				$('.multipleSelect').fadeOut('slow');
					expanded = false; 
				}

			}
			if ($(e.target).attr('id') != $('#checkboxes1') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = document.getElementById("checkboxes1");
				checkboxes.style.display = "none";
				$('#checkboxes').fadeOut('slow');
				$('.multipleSelect').hide();
				$('.multipleSelect').fadeOut('slow');
					expanded = false; 
				}

			}
		}
	}
	
});


$(document).on('click','.select_all',function(){
		if(this.checked){
            $(this).parents("div.multipleSelect").find('.check').each(function(){
                this.checked = true;
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
            });
        }else{
             $(this).parents("div.multipleSelect").find('.check').each(function(){
                this.checked = false;
				
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
				
            });
        }
	});



$('body').on('click','.check', function() {
	
	if($('.check:checked').length == $('.check').length){
            $(this).parents("div.multipleSelect").find('.select_all').prop('checked',true);
        }else{
            $(this).parents("div.multipleSelect").find('.select_all').prop('checked',false);
        }
		
	
	var arr = [];
	$.each($("input[name^='category_id']:checked"), function(){            
    	arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#pro_categories').val(prod.join(", "));
  $('#pro_cat').html(prod.join(", "));
});



/*----- export quotation ------ */
$(document).on('click','#expQuotation',function(){
	//alert("Hi, Test.");
	var href = $(this).attr("href");
	var target = $(this).attr("target");
	var trans_no = $("#trans_no").val();
	var trans_type = $("#trans_type").val();

	var sheetType = $("#sheetType").val();
	var offerOption = $("#offerOption").val();

	href = href+"?trans_no="+trans_no+"&trans_type="+trans_type+"&sheetType="+sheetType+"&offerOption="+offerOption;

	window.location=href;
	//openWindow(href,target);
	return false;
});
$(document).on('click','#all_product',function(){
		if(this.checked){
            $(this).parents("div#ProductsList123").find('.single_product').each(function(){
                this.checked = true;
            });
        }
        else
        {
        	$(this).parents("div#ProductsList123").find('.single_product').each(function(){
                this.checked = false;
            });
        }
	});
