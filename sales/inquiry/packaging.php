<?php
//error_reporting(0);
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PACKAGING';
$path_to_root = "../..";

include_once($path_to_root . "/sales/includes/db/packaging_module_db.inc");
include_once($path_to_root . "/sales/includes/packaging_module_class.inc");
include_once($path_to_root . "/sales/includes/ui/packaging_module_ui.inc");

simple_page_mode(true);
// unset($_SESSION['master_cart_section']);
//display_error(_(count($_SESSION['master_cart_section'])));
/*if(!isset($_SESSION['master_cart_section'])){
	$_SESSION['master_cart_section']= '';
}*/
if (isset($_GET['PackingingId'])){
    unset($_SESSION['packaging_part']);
	unset($_SESSION['part_section']);

}
if(isset($_SESSION['packaging_part']) && isset($_SESSION['part_section'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['packaging_part']);
	 fixObject($_SESSION['part_section']);
}else{
	$_SESSION['part_section'] = $_SESSION['packaging_part'] = new packaging_order;
}


if (isset($_GET['PackingingId']))
{
	
	$selected_id = $_GET['PackingingId'];
	$_SESSION['s_id'] = $selected_id;
	$result = read_packaging_header($selected_id);
	read_packaging_part($selected_id, $_SESSION['packaging_part']);
}else{
	$selected_id = -1;	

}

if (isset($_GET['finish_pro_id']))
{
  $selected_finish_pro_id = $_GET['finish_pro_id'];
  $result = read_finish($selected_finish_pro_id);

}
 
//------------ handle new packaging code ----------

function handle_new_packaging_consume_design()
{
	$check_cons_data = packaging_check_data();
	
	 if($check_cons_data){
		
	   	$_SESSION['packaging_part']->add_to_pacakaging_part(count($_SESSION['packaging_part']->line_items), $_POST['principle_type'],$_POST['code_id'],
		$_POST['unit'],$_POST['quantity'],$_POST['width'],$_POST['height'],$_POST['density']); // code by keshav 20oct
		

		unset_packaging_variables();
		line_packaging_start_focus();
	 } 
	
}

//-------------- end new packaging code ---------------
function handle_packing_delete_item($line_no)
{
	array_splice($_SESSION['packaging_part']->line_items, $line_no, 1);
	unset_packaging_variables();	
    line_packaging_start_focus();
}
//------------ update packaging code ------------
function handle_packaging_update_item()
{
   $_SESSION['packaging_part']->update_pacakaging_part($_POST['line_no'], $_POST['principle_type'],$_POST['code_id'],$_POST['unit'],$_POST['quantity'],
	$_POST['width'],$_POST['height'],$_POST['density']);
	unset_packaging_variables();
    line_packaging_start_focus();
}
//-------------- end update packaging code ---------------
//------------------- add carton part section -----------

function handle_new_carton_part_section()
{
	display_error(_("stock is over"));
	$allow_update = true;
	$qty = array();
	$get_qty = array();
	$de_id = $_POST['design_id'];
	
		if (count($_SESSION['part_section']->line_con) > 0)
		{

			foreach ($_SESSION['part_section']->line_con as $order_item) 
			{
					$qty[$order_item->part_name_id] += $order_item->part_qty;
			} 
		}
		$qty[$_POST['part_name_id']] +=$_POST['part_qty'];
		$get_qty = get_desgin_part_qty($_POST['part_name_id'],$de_id );
		$qty_part[$_POST['part_name_id']] = $get_qty['quantity'];	
		$q_key = key($qty_part);
		$p_key = key($qty);
		if (array_key_exists($q_key, $qty)) {
   				$total_qty = $qty_part[$q_key]-$qty[$q_key];
		}
		if($total_qty < 0){
			display_error(_("stock is over"));
			$allow_update = false;
		}
		//print_r($_SESSION['t_sess']);
		if($_SESSION['t_sess'] != ''){
		
				if (array_key_exists($p_key, $_SESSION['t_sess'])) {
						$to_qty[$q_key] = $qty[$p_key]+$_SESSION['t_sess'][$q_key];
				
				}	
		}
		if (array_key_exists($q_key, $to_qty)) {
   				$to_qty = $qty_part[$q_key]-$to_qty[$q_key];
		};
		if($to_qty < 0){
			display_error(_("stock is over"));
			$allow_update = false;
		}
		if($allow_update){
			$_SESSION['part_section']->add_to_carton_part(count($_SESSION['part_section']->line_con), $_POST['part_name_id'],$_POST['part_qty']);
			unset_form_part_variables();
			line_part_start_focus();
		}else{
			display_warning(_("The selected Part is already exist"));
		}
}
//----------------------- end part section -------------

//------------------ update carton part section ----------
function handle_carton_part_section_update()
{
   $_SESSION['part_section']->update_carton_part_item($_POST['line_no'], $_POST['part_name_id'],$_POST['part_qty']);
	unset_form_part_variables();
    line_part_start_focus();
}
//-------------------- end part section --------------

//------------------- delete part section -------------
function handle_carton_part_section_delete($line_no)
{
	$l = $line_no;
	unset($_SESSION['part_section']->line_con[$line_no]);
	unset_form_part_variables();	
    line_part_start_focus();
}
//-------------------- end part section ---------------

//-------------------- start carton manage section ---------------
function handle_carton_manage_section(){
	
	$_POST['margin_dimension'] = $_POST['mar_weight'].",".$_POST['mar_height'].",".$_POST['mar_density'];
	$_POST['pkg_dimension'] = $_POST['pkg_s_weight'].",".$_POST['pkg_s_height'].",".$_POST['pkg_s_density'];
	$_POST['carton_dimension'] = $_POST['car_s_weight'].",".$_POST['car_s_height'].",".$_POST['car_s_density'];
	
	$session_photo = '';
	$session_lable = $_POST['label_id_0'].','.$_POST['label_id_1'].','.$_POST['label_id_2'].','.
	$_POST['label_id_3'].','.$_POST['label_id_4'].','.$_POST['label_id_5'];

	for($photo_no=0;$photo_no<=5;$photo_no++){
		$photo_name = "pic_".$photo_no;
		$finish_image = $_FILES[$photo_name]['name'];
		$fimage_tmp= $_FILES[$photo_name]['tmp_name'];
		if(!$session_photo){
			$session_photo = $finish_image;
		}else{
			$session_photo .= ",".$finish_image;
		}
		$f_Imge = company_path().'/CartonImage/'.$finish_image;
		move_uploaded_file($_FILES[$photo_name]['tmp_name'], $f_Imge);
	}


		//array_push($_POST['screenimages'], $session_photo);
		//array_push($_POST['screenlables'], $session_photo);
		$_POST['images'] = $session_photo; //code by keshav
		$_POST['lables'] = $session_lable; //code by keshav
		$this_carton_part_section = $_SESSION['part_section']->line_con;
		$par_qty = array();
		$to_qty =array();
		foreach($this_carton_part_section as $par_val){
			$par_qty[$par_val->part_name_id] += $par_val->part_qty;
			$_SESSION['p_sess'][$par_val->part_name_id] =$par_qty[$par_val->part_name_id];
		}
		foreach($_SESSION['p_sess'] as $key => $s_v){
			if (array_key_exists($key, $_SESSION['p_sess'])) {			
					$to_qty[$key] += $_SESSION['p_sess'][$key];
			}
			$_SESSION['t_sess'][$key] += $to_qty[$key];
		}
	
	if($_POST['carton_type_id'] == '101'){ //code by keshav
		if(!isset($_SESSION['master_cart_section']['master']['carton'])){
			$_SESSION['master_cart_section']['master']['carton'] = '';
			$_SESSION['master_cart_section']['master']['carton'][1] = $_POST;
			array_push($_SESSION['master_cart_section']['master']['carton'][1], $this_carton_part_section);
			$_SESSION['master_cart_section']['master']['part'][1] = $this_carton_part_section;
			$_SESSION['master_cart_section']['master']['photo'][1] = $session_photo;
			
		}else{
			$newcarton = count($_SESSION['master_cart_section']['master']['carton'])+1;
				
			$_SESSION['master_cart_section']['master']['carton'][$newcarton] = $_POST;
			array_push($_SESSION['master_cart_section']['master']['carton'][$newcarton], $this_carton_part_section);
			$_SESSION['master_cart_section']['master']['part'][$newcarton] = $this_carton_part_section;
			$_SESSION['master_cart_section']['master']['photo'][$newcarton] = $session_photo;
			
		}
	}else{
		if(!isset($_SESSION['master_cart_section']['inner'])){
			$_SESSION['master_cart_section']['inner']['carton'] = '';
			$_SESSION['master_cart_section']['inner']['carton'][1] = $_POST;
			array_push($_SESSION['master_cart_section']['inner']['carton'][1], $this_carton_part_section);
			$_SESSION['master_cart_section']['inner']['part'][1] = $this_carton_part_section;
			$_SESSION['master_cart_section']['inner']['photo'][1] = $session_photo;
			
		}else{
			$newcarton = count($_SESSION['master_cart_section']['inner']['carton'])+1;
			$_SESSION['master_cart_section']['inner']['carton'][$newcarton] = $_POST;
			array_push($_SESSION['master_cart_section']['inner']['carton'][$newcarton], $this_carton_part_section);
			$_SESSION['master_cart_section']['inner']['part'][$newcarton] = $this_carton_part_section;
			$_SESSION['master_cart_section']['inner']['photo'][$newcarton] = $session_photo;
			
		}	
	}
	
	
	unset($_SESSION['p_sess']);
	unset($_SESSION['part_section']->line_con);
	#unset carton and part section
	#unset_carton_part_variables();
}


//------------ end carton manage section ----------


function packaging_module_code_check_data()
{
	if(!get_post('finish_code_id')) {
		display_error( _("Finish code cannot be empty."));
		set_focus('finish_code_id');
		return false;
	}
	
    return true;	
}


function packaging_check_data()
{ 
	if(get_post('principal_type') == -1) {
		display_error( _("Principal Type cannot be empty."));
		set_focus('principal_type');
		return false;
	}
	if(get_post('code_id') == 0) {
		display_error( _("Code id cannot be empty."));
		set_focus('code_id');
		return false;
	}
	if(!get_post('unit')) {
		display_error( _("Unit cannot be empty."));
		set_focus('unit');
		return false;
	}
	if (!check_num('quantity', 0))
    {
	   	display_error(_("The quantity entered must be numeric and not less than zero."));
		set_focus('quantity');
	   	return false;	   
    }
	/*if (!check_num('width', 0))
    {
	   	display_error(_("The width entered must be w,h,d"));
		set_focus('width');
	   	return false;	   
    }*/
   return true;	
}

//------------------- line start table ------------

function line_packaging_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  //set_focus('cons_type');
}
//------------------------ add all value --------------------------------
function line_part_start_focus() {
  global 	$Ajax;

  $Ajax->activate('con_table');
  //set_focus('cons_type');
}
function unset_form_part_variables() {
	
	unset($_POST['part_name_id']);
    unset($_POST['part_qty']);

}

function unset_packaging_variables() {
	
	unset($_POST['principle_type']);
    unset($_POST['code_id']);
    unset($_POST['unit']);
    unset($_POST['quantity']); // code by keshav 20oct
	unset($_POST['width']);
	unset($_POST['height']);
	unset($_POST['density']);

}

function unset_carton_part_variables() {
	
	unset($_POST['carton_type_id']);
    unset($_POST['cat_qty']); //code by keshav
    unset($_POST['mar_weight']);
    unset($_POST['mar_height']);
	unset($_POST['mar_density']);
	unset($_POST['carton_type_id']);
    unset($_POST['gross_weight']);
    unset($_POST['code']);
	unset($_POST['pkg_s_weight']);
	unset($_POST['carton_type_id']);
    unset($_POST['gross_weight']);
    unset($_POST['pkg_s_height']);
	unset($_POST['pkg_s_density']);
	unset($_POST['car_s_weight']);
    unset($_POST['car_s_height']);
	unset($_POST['car_s_density']);
	unset($_POST['net_weight']);
}

function handle_commit_packaging_product()
{
	$header_design = array();
	
	$header_design['packaging_id'] = trim($_POST['packaging_id']);
	$header_design['customer_id'] = trim($_POST['customer_id']);
	$header_design['category_id'] = trim($_POST['category_id']);
	$header_design['range_id'] = trim($_POST['range_id']);
	$header_design['design_id'] = trim($_POST['design_id']);
	$header_design['finish_code_id'] = trim($_POST['finish_code_id']);
	$header_design['buyers_code'] = trim($_POST['buyers_code']); //code by keshav
	$packing_sessiong = &$_SESSION['packaging_part'];
	$mstercarton = &$_SESSION['master_cart_section'];
	$order_no = add_packaging_module($header_design,$packing_sessiong,$mstercarton);
	unset($_SESSION['packaging_part']);
	
	//meta_forward($_SERVER['PHP_SELF']);
}

/* update packaging*/

function handle_update_commit_packaging()
{
	global $selected_id;
	$selected_id = $_SESSION['s_id'];
	$udpate_design = array();
	$udpate_design['customer_id'] = trim($_POST['customer_id']);
	$udpate_design['category_id'] = trim($_POST['category_id']);
	$udpate_design['range_id'] = trim($_POST['range_id']);
	$udpate_design['design_id'] = trim($_POST['design_id']);
	$udpate_design['finish_code_id'] = trim($_POST['finish_code_id']);
	$udpate_design['buyers_code'] = trim($_POST['buyers_code']); //code by keshav
	
	$pack_sec = &$_SESSION['packaging_part'];
	$order_no = update_packaging($selected_id, $udpate_design, $pack_sec);
	unset($_SESSION['packaging_part']);
}

//------------------- end line start table --------------

$id = find_submit('Delete');
if ($id != -1)
{
	handle_packing_delete_item($id);
}
if (isset($_POST['FPackagingAddItem']))
{
	handle_new_packaging_consume_design();
}
if (isset($_POST['FPackagingUpdateItem'])){
	
	handle_packaging_update_item();
}

if (isset($_POST['FPackagingCancelItemChanges'])) {
	line_packaging_start_focus();
}

$id = find_submits_consume('Delete');
if ($id != -1)
{
	handle_carton_part_section_delete($id);
}
if (isset($_POST['CPartAddItem']))
{
	handle_new_carton_part_section();
}
if (isset($_POST['CPartUpdateItem'])){
 	handle_carton_part_section_update();
}
if (isset($_POST['CPartCancelItemChanges'])) {
	line_part_start_focus();
}

/*if (isset($_POST['Addtolist']))
{
	handle_commit_packaging_product();
}*/

if (isset($_POST['AddthisCarton']))
{	
	
	handle_carton_manage_section();
}


if (isset($_POST['Addtolist']))
{
	if($_POST['buyers_code'] == ""){display_error("Please Enter Buyers Code");}
	else{handle_commit_packaging_product();
	}
}
/*if (isset($_POST['UpdatePackaging']))
{
	handle_update_commit_packaging();
}
*/
$myrow = $_POST['finish_code_id'];
if($myrow['finish_code_id']==''){
	echo "<input type='hidden' id='des_id' value=''>";
	unset($_SESSION['packaging_part']);
	unset($_SESSION['part_section']);
	unset($_SESSION['master_cart_section']);
	unset($_SESSION['p_sess']);
	unset($_SESSION['t_sess']);
}else{
 echo "<input type='hidden' id='des_id' value='".$myrow."'>";
}

if ($Mode == 'Delete')
{
	$selected_id = find_submit('Delete_pack_list');
	if ($selected_id != -1){
	delete_packaging($selected_id);
	}
	$Mode = 'RESET';
}
//-----------------------------------------------------	
	 /* echo "<pre>";
	  print_r($_POST);
	  echo "</pre>";*/

if(isset($_POST['view'])){
	if($_POST['view']== "view"){
		
		
		
	$selected_id = find_submit('view');
	//display_notification($selected_id);
		if ($selected_id != -1){
			
			   $result = view_selected_packaging_list($selected_id);
			   $myrow = db_fetch($result);
				$_POST['category_id'] = $myrow['category_id'];
				$_POST['range_id'] = $myrow['range_id'];
		  
		  if($myrow['range_id']== -1){
			  $_POST['collection_id']="NonCollection";
		  }
		  
		  else{
			  $_POST['collection_id']="Collection";
		  }
				$_POST['design_id'] = $myrow['design_id'];
				$_POST['finish_code_id'] = $myrow['finish_code_id'];
				$_POST['buyers_code'] = $myrow['buyers_code'];
				$_POST['packaging_pro_id'] = $myrow['packaging_pro_id'];
		  
				$result_part = view_selected_packaging_part_list($selected_id);
				unset($_SESSION['packaging_part']->line_items);
				while($myrow_part=db_fetch($result_part)){
				
					$_SESSION['packaging_part']->add_to_pacakaging_part(count($_SESSION['packaging_part']->line_items), $myrow_part['principle_id'],$myrow_part['code'],
					$myrow_part['unit'],$myrow_part['quantity'],$myrow_part['width'],$myrow_part['height'],$myrow_part['density']);
					
				}
				
				$result_carton = view_selected_carton_list($selected_id);
				$_POST['packaging_pro_id'] = '';
				unset($_SESSION['master_cart_section']['master']['carton']);
				unset($_SESSION['master_cart_section']['inner']['carton']);
				while($myrow_crt = db_fetch($result_carton)){
					//display_notification("$myrow_crt");
					 if($myrow_crt['carton_type_id'] == '101'){ //code by keshav
						  if(!isset($_SESSION['master_cart_section']['master']['carton'])){
							  $_SESSION['master_cart_section']['master']['carton'] = '';
							  $_SESSION['master_cart_section']['master']['carton'][1] = $myrow_crt;
							  array_push($_SESSION['master_cart_section']['master']['carton'][1], $this_carton_part_section);
							  $_SESSION['master_cart_section']['master']['part'][1] = $this_carton_part_section;
							  $_SESSION['master_cart_section']['master']['photo'][1] = $session_photo;
							  
						  }else{
							  $newcarton = count($_SESSION['master_cart_section']['master']['carton'])+1;
								  
							  $_SESSION['master_cart_section']['master']['carton'][$newcarton] = $myrow_crt;
							  array_push($_SESSION['master_cart_section']['master']['carton'][$newcarton], $this_carton_part_section);
							  $_SESSION['master_cart_section']['master']['part'][$newcarton] = $this_carton_part_section;
							  $_SESSION['master_cart_section']['master']['photo'][$newcarton] = $session_photo;
							  
						  }
					  }else{
						  if(!isset($_SESSION['master_cart_section']['inner'])){
							  $_SESSION['master_cart_section']['inner']['carton'] = '';
							  $_SESSION['master_cart_section']['inner']['carton'][1] = $myrow_crt;
							  array_push($_SESSION['master_cart_section']['inner']['carton'][1], $this_carton_part_section);
							  $_SESSION['master_cart_section']['inner']['part'][1] = $this_carton_part_section;
							  $_SESSION['master_cart_section']['inner']['photo'][1] = $session_photo;
							  
						  }else{
							  $newcarton = count($_SESSION['master_cart_section']['inner']['carton'])+1;
							  $_SESSION['master_cart_section']['inner']['carton'][$newcarton] = $myrow_crt;
							  array_push($_SESSION['master_cart_section']['inner']['carton'][$newcarton], $this_carton_part_section);
							  $_SESSION['master_cart_section']['inner']['part'][$newcarton] = $this_carton_part_section;
							  $_SESSION['master_cart_section']['inner']['photo'][$newcarton] = $session_photo;
							  
						  }	
					  }
				}
		}

	}
}
		/*echo "<pre>";
	  print_r($_SESSION['master_cart_section']);
	  echo "</pre>";*/
//-------------------------------------------------------------


function delete_packaging($selected_id){
	$del_id = $selected_id;
	
	$sql ="DELETE FROM 0_customer_packaging_product WHERE packaging_pro_id=$del_id";
	$sql2 ="DELETE FROM 0_customer_carton_details WHERE packaging_pro_id=$del_id";
	$sql3 ="DELETE FROM 0_customer_packaging_part WHERE packaging_pro_id=$del_id";
	
	db_query($sql,"not deleted");
	db_query($sql2,"Not Deleted");
	db_query($sql3,"Not Deleted");
	
	display_notification("Deleted");
}


//---------------------------- start table ------------------------------------------------------
start_form(true); 
display_packaging_list();
display_packaging_hearder($_SESSION['PACKK']);
display_packaging_list_summary($_SESSION['packaging_part']);
display_packaging_carton($_SESSION['carton_list']);
display_carton_part_section_summary($_SESSION['part_section']);
display_packaging_screen($_SESSION['screen_list']);
display_carton_session_summary($_POST);

if ($selected_id != -1) 
{
	echo "<br><center>";
	/*submit_center_first('UpdatePackaging', _("Update"), _('Update to packaging code'), 'default');
	submit_center_last('CANCEL_ITEM', _("Cancel"), _('Cancel to packaging code'), 'default');*/
	echo "</center>";
}
else
{   echo "<center>";
	echo "<input type='submit' name='Addtolist' id='Addtolist' value='Add To List'></button>"; //code by keshav
	echo "</center>";
}
/** detail **/


/** fetch consumable record **/

echo '<br>';
echo '<br>';
echo '<br>';


end_form();
 
?>
