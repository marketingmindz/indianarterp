<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "../..";

include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

include_once($path_to_root . "/sales/includes/db/bpc_db.inc");

$page_security = 'SA_SALESTRANSVIEW';

set_page_security( @$_POST['tabs_contacts'],
	array(	'OutstandingOnly' => 'SA_SALESDELIVERY',
			'InvoiceTemplates' => 'SA_SALESINVOICE'),
	array(	'OutstandingOnly' => 'SA_SALESDELIVERY',
			'InvoiceTemplates' => 'SA_SALESINVOICE')
);

$_SESSION['page_title'] = _($help_context = "Search All BPCs");

if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 600);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page($_SESSION['page_title'], false, false, "", $js);
}

if (isset($_GET['selected_customer']))
{
	$selected_customer = $_GET['selected_customer'];
}
elseif (isset($_POST['selected_customer']))
{
	$selected_customer = $_POST['selected_customer'];
}
else
	$selected_customer = -1;

//---------------------------------------------------------------------------------------------

if (isset($_POST['SelectStockFromList']) && ($_POST['SelectStockFromList'] != "") &&
	($_POST['SelectStockFromList'] != ALL_TEXT))
{
 	$selected_stock_item = $_POST['SelectStockFromList'];
}
else
{
	unset($selected_stock_item);
}
//---------------------------------------------------------------------------------------------
//	Query format functions
//
function check_overdue($row)
{
	global $trans_type;
	if ($trans_type == ST_SALESQUOTE)
		return (date1_greater_date2(Today(), sql2date($row['delivery_date'])));
	else
		return ($row['type'] == 0
			&& date1_greater_date2(Today(), sql2date($row['delivery_date']))
			&& ($row['TotDelivered'] < $row['TotQuantity']));
}


function view_link($row)
{
	if (@$_GET['popup'])
		return '';
	//return pager_link( _("View BPC"), "/sales/view/view_bpc.php?bpc=" . $row['bpc_id'], false, "viewBPCLinkButton");
    return viewer_link(_("View BPC"), "/sales/view/view_bpc.php?bpc=" . $row['bpc_id'], "", "viewBPCLinkButton",  false);
}

function prt_link($row)
{
	global $trans_type;
	return print_document_link($row['bpc_id'], _("Print"), true, $trans_type, ICON_PRINT, "printLinkButton");
}

function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
  return pager_link( _("Edit"),
    "/sales/sales_order_entry.php?bpc_id=" . $row['bpc_id'], ICON_EDIT, "editLinkButton");
}

function dispatch_link($row)
{
	global $trans_type;
	if ($trans_type == ST_SALESORDER)
  		return pager_link( _("Dispatch"),
			"/sales/customer_delivery.php?OrderNumber=" .$row['order_no'], ICON_DOC);
	else		
  		return pager_link( _("Sales Order"),
			"/sales/sales_order_entry.php?OrderNumber=" .$row['order_no'], ICON_DOC, 'makeOrderLinkButton');
}

function invoice_link($row)
{
	global $trans_type;
	if ($trans_type == ST_SALESORDER)
  		return pager_link( _("Invoice"),
			"/sales/sales_order_entry.php?NewInvoice=" .$row["order_no"], ICON_DOC);
	else
		return '';
}

function furniture_pr($row)
{
  return pager_link( _("Furniture PR"),
	"/purchasing/purchase_request_entry.php?bpcid=" .$row['bpc_id']."&OrderNumber=".$row['order_id']);
}
function carton_pr($row)
{
  return pager_link( _("Carton PR"),
	"/purchasing/purchase_request_entry.php?bpcid=" .$row['bpc_id']."&OrderNumber=".$row['order_id']);
}
function consumable_pr($row)
{
  return pager_link( _("Consumable PR"),
	"/purchasing/purchase_request_entry.php?bpcid=" .$row['bpc_id']."&OrderNumber=".$row['order_id']);
}

function order_link($row)
{
  return pager_link( _("Sales Order"),
	"/sales/sales_order_entry.php?NewQuoteToSalesOrder=" .$row['order_no'], ICON_DOC, 'makeOrderLinkButton');
}

//---------------------------------------------------------------------------------------------
// Update db record if respective checkbox value has changed.
//
function change_tpl_flag($id)
{
	global	$Ajax;
	
  	$sql = "UPDATE ".TB_PREF."sales_orders SET type = !type WHERE order_no=$id";

  	db_query($sql, "Can't change sales order type");
	$Ajax->activate('orders_tbl');
}

$id = find_submit('_chgtpl');
if ($id != -1)
	change_tpl_flag($id);

if (isset($_POST['Update']) && isset($_POST['last'])) {
	foreach($_POST['last'] as $id => $value)
		if ($value != check_value('chgtpl'.$id))
			change_tpl_flag($id);
}

//---------------------------------------------------------------------------------------------
if (get_post('_bpcNumber_changed') || get_post('_OrderNumber_changed')) // enable/disable selection controls
{
	$Ajax->addDisable(true, 'OrderNumber', false);
	$Ajax->addDisable(true, 'bpcNumber', false);

	$disable = get_post('bpcNumber') !== '' || get_post('OrderNumber') !== '';

	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);
	$Ajax->activate('orders_tbl');
	if(get_post('bpcNumber') !== '')
		$Ajax->addDisable(true, 'OrderNumber', $disable);
	if(get_post('OrderNumber') !== '')
		$Ajax->addDisable(true, 'bpcNumber', $disable);
}

if (!@$_GET['popup'])
	start_form();

start_table(TABLESTYLE_NOBORDER);
start_row();
ref_cells(_("BPC #:"), 'bpcNumber', '',null, '', true);
ref_cells(_("Order NO."), 'OrderNumber', '',null, '', true);

date_cells(_("from:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("to:"), 'OrdersToDate', '', null, 1);


if($show_dates) {
	end_row();
	end_table();

	start_table(TABLESTYLE_NOBORDER);
	start_row();
}

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
hidden('order_view_mode', $_POST['order_view_mode']);
hidden('type', $trans_type);

end_row();

end_table(1);
//---------------------------------------------------------------------------------------------
//	Orders inquiry table
//
$sql = get_sql_for_bpcs_view($_POST['OrderNumber'], $_POST['bpcNumber'], @$_POST['OrdersAfterDate'], @$_POST['OrdersToDate']);

$cols = array(
	_("BPC #") => array('type' => 'bpc.bpc_id', 'ord' => 'desc') ,
	_("Order No.") => array('type' => 'bpc.order_id', 'ord' => '') ,
	_("BPC Name"),
	_("BPC Created") => array('type' =>  'date', 'ord' => ''),
	_("Order Date") => array('type' =>  'date', 'ord' => ''),
	_("Delivery Date") =>array('type'=>'date', 'ord'=>'')
);


/*array_append($cols,array(
				array('insert'=>true, 'fun'=>'view_link'),
			array('insert'=>true, 'fun'=>'edit_link'),
			array('insert'=>true, 'fun'=>'order_link'),
			array('insert'=>true, 'fun'=>'prt_link')));*/

array_append($cols,array(
				array('insert'=>true, 'fun'=>'view_link'),
			array('insert'=>true, 'fun'=>'furniture_pr'),
			array('insert'=>true, 'fun'=>'carton_pr'),
			array('insert'=>true, 'fun'=>'consumable_pr')));

$table =& new_db_pager('orders_tbl', $sql, $cols);
//$table->set_marker('check_overdue', _("Marked items are overdue."));

$table->width = "80%";

display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}
?>
