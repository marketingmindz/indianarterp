<?php
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

$finish_id = $_POST['finish_id'];

$sql="SELECT f.gross_weight,f.asb_weight,f.asb_height,f.asb_density,f.no_sitting,f.description,f.finish_comp_code,f.product_image,c.cat_name,d.design_code,f.finish_product_name,w.wood_name,col.color_name,leg.leg_name,
CASE WHEN f.range_id=-1 || f.range_id=0 THEN 'Non Collection' ELSE t.range_name END as rangeNAME, 
CASE WHEN f.finish_id=-1 || f.finish_id=0 THEN 'No Finish' ELSE fin.finish_name END as finishNAME,
CASE WHEN f.structure_wood_id=-1 || f.structure_wood_id=0 THEN 'No Structure Wood' ELSE strct.wood_name END as struct_wood_name,
CASE WHEN f.skin_id=-1 || f.skin_id=0 THEN 'No Skin' ELSE skin.skin_name END as skinNAME
FROM 0_finish_product f 
LEFT JOIN 0_item_category c on f.category_id = c.category_id 
Left Join 0_item_range t on f.range_id = t.id 
Left Join 0_design_code d on f.design_id = d.design_id
Left Join 0_wood_master w on f.wood_id = w.wood_id
Left Join 0_color_master col on f.color_id = col.color_id
Left Join 0_finish_master fin on f.finish_id = fin.finish_id
Left Join 0_wood_master strct on f.wood_id = strct.wood_id
Left Join 0_leg_master leg on f.leg_id = leg.leg_id
Left Join 0_skin_master skin on f.skin_id = skin.skin_id
where finish_pro_id =".db_escape($finish_id);


$sql2="SELECT con.consumable_name,f.cons_qty,m.master_name FROM 0_fconsumable_part f 
Left Join 0_consumable_master con on f.cons_select=con.consumable_id
LEFT JOIN 0_master_creation m on f.cons_type = m.master_id
WHERE finish_pro_id =".db_escape($finish_id);
$result = db_query($sql,"NOT RETRIVE");
$result2 = db_query($sql2,"NOT RETRIVE");
$myrow = db_fetch($result);
$img_arr = explode(",",$myrow['product_image']);
?>
<div id="close" style="cursor:pointer; font-size:larger; float:right"><strong>X</strong></div>
<table border="0" width="100%" cellspacing="4" cellpadding="">
<tr>
	<th colspan="4" ><center>Finish Product</center></th>
</tr>
<tr>
	<th>Category</th>
    <td><?php echo $myrow['cat_name']; ?></td>
	<th>Wood</th>
    <td><?php echo $myrow['wood_name']; ?></td>
</tr>
<tr>
	<th>Collection</th>
    <td><?php echo $myrow['rangeNAME']; ?></td>
    <th>Color</th>
    <td><?php echo $myrow['color_name']; ?></td>
</tr>
<tr>
	<th>Design Code</th>
    <td><?php echo $myrow['design_code']; ?></td>
    <th>Finish</th>
    <td><?php echo $myrow['finishNAME']; ?></td>
</tr>
<tr>
	<th>Finish Product Name</th>
    <td><?php echo $myrow['finish_product_name']; ?></td>
    <th>Structure of Wood</th>
    <td><?php echo $myrow['struct_wood_name']; ?></td>
</tr>
<tr>
	<th>Assemble Packing Size</th>
    <td>W:&nbsp&nbsp<?php echo $myrow['asb_weight']; ?>&nbspD:&nbsp&nbsp<?php echo $myrow['asb_density']; ?>&nbspH:&nbsp&nbsp<?php echo $myrow['asb_height']; ?></td>
    <th>No. Sitting</th>
    <td><?php echo $myrow['no_sitting']; ?></td>
</tr>

<tr>
	<th>Code</th>
    <td><?php echo $myrow['finish_comp_code']; ?></td>
	<th>Leg Name</th>
    <td><?php echo $myrow['leg_name']; ?></td>
</tr>
<tr>
	<th>Skin Name</th>
    <td><?php echo $myrow['skinNAME']; ?></td>
    <th>Gross Weight</th>
    <td><?php echo $myrow['gross_weight']; ?></td>
</tr>
<tr>
	<th>Product Description</th>
    <td colspan="3"><?php echo $myrow['description']; ?></td>
</tr>
<tr>
	<th>Procduct Image</th>
    <td colspan="3">
    <?php for($i=0;$i<count($img_arr);$i++){ ?>
    <img id='blah' src='../../company/0/finishProductImage/<?php echo $img_arr[$i]; ?>' alt='your image' width='80' height='80' />
    <?php } ?>
    </td>
</tr>
<tr></tr>
</table>
<table border="0" width="100%" cellspacing="4" cellpadding="">
<?php if(mysql_num_rows($result2) >= 1 ) { ?>
<tr>
	<th colspan="3"><center>Consumable Part</center></th>
</tr>
<tr>
	<th>Consumable Type</th>
    <th>Consumable Category</th>
    <th>Quantity</th>
</tr>
<?php while($myrow2 = db_fetch($result2)) { ?>
<tr>
	<td><?php echo $myrow2['master_name']; ?></td>
    <td><?php echo $myrow2['consumable_name']; ?></td>
    <td><?php echo $myrow2['cons_qty']; ?></td>
</tr>
<?php } ?>
<?php } ?>
</table>
<!--<img id='blah' src='".company_path().'/collectionImage/'.$_POST['coll_pic ']."' alt='your image' width='80' height='80' />-->