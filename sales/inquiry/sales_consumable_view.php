<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$page_security = 'SA_SALE_PRODUCT';
$path_to_root = "../..";
include($path_to_root . "../includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");

include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/sales/includes/db/customers_db.inc");
if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Finish Product List"), false, false, "", $js);
}


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} 
elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
			
			if ($disable) 
			{
					$Ajax->addFocus(true, 'order_number');
			} 
	
	$Ajax->activate('orders_tbl');	
}

//--------------add conumable code by keshav------------------
if(isset($_POST['save'])){
	//display_notification("Added");
	$finish_code = $_POST['finish_code_id'];
	$buyer_code = $_POST['buyers_code'];
	$catagory = $_POST['catagory'];
	$sub_category = $_POST['sub_category'];
	$new_quantity = $_POST['new_quantity'];
	$customer_id = $_POST['customer_id'];
	$msg = add_customer_cosumable_relation($catagory,$sub_category,$new_quantity,$customer_id,$finish_code,$buyer_code);
	display_notification($msg);
}
//-------------------------code end-------------------------------

/*start_table(TABLESTYLE, "width=60%");
	$th = array(_('Consumable Name'),_('Consumable Category'),_('Quantity'));
	inactive_control_column($th);
	table_header($th);
	$sql=get_all_customer_consumable_relation($_POST['customer_id']);
	$result= db_query($sql,"not retrive");
	$k = 0;
	
	while ($myrow = db_fetch($result)){
		
	alt_table_row_color($k);
	label_cell($myrow['0']);
	label_cell($myrow['1']);
	label_cell($myrow['2']);
	end_row();
	}
echo"<br>";*/


/*start_table(TABLESTYLE2);
echo "<br>";
table_section(1);
table_section_title(_("Search Consumable List"));
design_category_list_row(_("Category:"), 'category_id', null, _('----Select---'));
sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
if($_POST['collection_id'] == "NonCollection"){

}else{
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
}
sub_desing_code_list_row(_("Design Code:"), 'design_code', null, _('----Select---'));	
desgin_finish_code_list_row(_("Finish Product Codes:"), 'finish_code_id', null, _('----Select---'));	
design_category_list_row(_("Buyers Code:"), 'buyers_code', null, _('----Select---'));	
	
end_table(1);
start_table(TABLESTYLE_NOBORDER, "'width=70%' 'align = centre'");
	start_row();
		echo '<input type="submit" name="Search" value="Search">';
	end_row();
end_table(1);
*/

start_form();
start_outer_table(TABLESTYLE2);
		
		table_section(1);
		table_section_title(_("Search Consumable List"));
		
		
		/*design_category_list_row(_("Category:"), 'category_id', null, _('----Select Category---'));
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}
		if($_POST['category_id'] == '-1' && $_POST['range_id'] == '-1'){
			sub_desing_code_list_row(_("Design Code:"), 'design_id', null, _('----Select Code---'));
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] == '-1'){
			desgin_finish_code_list_row(_("Design Code:"), $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}else if($_POST['category_id'] != '-1' && $_POST['range_id'] != '-1'){
			desgin_finish_range_code_list_row(_("Design Code:"), $_POST['range_id'], $_POST['category_id'],$_POST['design_id'], null, false, true, true, true);
		}	
		if($_POST['design_id'] == '-1'){
			sub_finish_packaging_code_list_row(_("Finish Product Codes:"),'finish_code_id', null, _('----Select Code---'));	
		}else{
			desgin_finish_prodcut_desing_code_list_row(_("Finish Product Codes:"), $_POST['design_id'], $_POST['finish_code_id'], null, false, true, true, true);
		}*/
		
		$sql = get_all_customer_product_relation($_POST['customer_id']);
		$result= db_query($sql,"not retrive");
		$k = 0;
		start_row();
		label_cell("Customer Products : ");
		echo '<td><select name="finish_code_id" id="finish_code_id" >';
		while ($myrow = db_fetch($result)){
		
			echo '<option value="'.$myrow['finish_pro_id'].'">'.$myrow['finish_comp_code'].'</option>';

		}
		echo "</select></td>";
		end_row();
		start_row();
		echo'<input type="hidden" value="'.$_POST['customer_id'].'" id="new_customer_id">';
		text_cells_ex('Buyers Code','buyers_code');
		end_row();
		echo '<input type="hidden" name="action" value="action" >';
		end_outer_table(1);
		start_table(TABLESTYLE_NOBORDER, "'width=70%' 'align = centre'");
			start_row();
				echo '<input type="submit" name="Search" value="Search">';
			end_row();
		end_table(1);


if (!@$_GET['popup'])
{
	end_form();
	end_page();
}


if(isset($_POST['action'])){
	$finish_id = $_POST['finish_code_id'];
	$buyers_id = $_POST['buyers_code'];
	$customer_id = $_POST['customer_id'];
	
	$check_buyer_code = check_buyer_code($finish_id,$buyers_id,$customer_id);
	if($check_buyer_code){
		$sql = get_customers_consumable_list_buyer($finish_id,$buyers_id,$customer_id);
	}else{
	    $sql = get_customers_consumable_list_finish($finish_id);
	}
	
	
	
	
	//
	$resut = db_query($sql,"not retrive");
	start_table(TABLESTYLE, "width=60%");
	$th = array(_('Consumable Name'),_('Consumable Category'),_('Quantity'), _('Add new'));
	inactive_control_column($th);
	table_header($th);
	$k = 0;
	while ($myrow = db_fetch($resut)) 
	{
	
		alt_table_row_color($k);	
	
		label_cell($myrow['master_name']);
		label_cell($myrow['consumable_name']);
		label_cell('<input type="text" name="quantity" id="quantity_'.$myrow[2].''.$myrow[3].'" value="'.$myrow['quantity'].'">');
		label_cell('<input type="button" name="cons_submit" value="Add" class="cons_submit" id="cons_submit_'.$myrow[2].''.$myrow[3].'">');
		echo'<input type="hidden" value="'.$myrow[2].'" name="cons_id" id="cons_id_'.$myrow[2].''.$myrow[3].'">';
		echo'<input type="hidden" value="'.$myrow[3].'" name="master_id" id="master_id_'.$myrow[2].''.$myrow[3].'">';
		echo'<input type="hidden" value="'.$_POST['customer_id'].'" name="customer_id" id="customer_id_'.$myrow[2].''.$myrow[3].'">';
	
		
		end_row();
	}
		stock_master_list_cells(null, 'catagory', null, _('----Select---'));
		if($_POST['catagory'] == '-1'){
			sub_consumable_master_list_cells(null, 'sub_category',null, _('----Select---'));
		}else{
			customer_consumable_list_cells(null, $_POST['catagory'],'sub_category', null, false, true, true, true);
		}
		label_cell('<input type="text" name="new_quantity" value="" id="new_quantity">');
		//label_cell('<input type="button" name="save" value="Add" id="save">');
		label_cell('<input type="submit" name="save" value="Add" id="save">'); //code by keshav
	end_table(1);
	end_form();
}

if(!isset($_POST['action'])){
start_table(TABLESTYLE2);
$th = array(_("Category"),_("Sub Category "),_("Quantity"),_("Save"));
	table_header($th);
	stock_master_list_cells(null, 'catagory', null, _('----Select---'));
		if($_POST['catagory'] == '-1'){
			sub_consumable_master_list_cells(null, 'sub_category',null, _('----Select---'));
		}else{
			customer_consumable_list_cells(null, $_POST['catagory'],'sub_category', null, false, true, true, true);
		}
		label_cell('<input type="text" name="new_quantity" value="" id="new_quantity">');
		//label_cell('<input type="button" name="save" value="Add" id="save">');
		label_cell('<input type="submit" name="save" value="Add" id="save">'); // code by keshav
	
	end_table(1);
}
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
$(".cons_submit").click(function(){
	var id = $(this).attr("id");
	var k = id.split('_');
	var buyers_code = $("input[name=buyers_code]").val();
	var finish_code = $("#packaging_finish_code").val();

	var customer_id = $("#customer_id_"+k['2']).val();
	var master_id = $("#master_id_"+k['2']).val();
	var cons_id = $("#cons_id_"+k['2']).val();
	var quantity = $("#quantity_"+k['2']).val();
	var datastring = 'customer_id='+customer_id + '&master_id='+master_id + '&cons_id='+ cons_id + '&quantity='+ quantity + '&buyers_code='+ buyers_code + '&finish_code='+ finish_code;
		
	$.ajax({
		type: "POST",
		url: "../inquiry/customer_consumable_relation.php",
		data: datastring,
			success: function(result){
			alert(result);
			}
		});
		
});
});	
</script>

