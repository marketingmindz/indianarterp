<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$page_security = 'SA_SALE_PRODUCT';
$path_to_root = "../..";
include($path_to_root . "../includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/date_functions.inc");

include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/sales/includes/db/customers_db.inc");


if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Finish Product List"), false, false, "", $js);
}


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} 
elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';
			
			if ($disable) 
			{
					$Ajax->addFocus(true, 'order_number');
			} 
	
	$Ajax->activate('orders_tbl');	
}
function multiple_categories_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	$sql = "SELECT category_id, cat_name, cat_reference, inactive FROM 0_item_category order by cat_name ASC";
	$result = db_query($sql);
	echo '<div class="multiselect">
           <div class="selectBox" onclick="showCheckboxes()">';
	echo '<select autocomplete="off" class="combo" title="Select Category" id="slc_category" >
	  <option value="-1" >- select category - </option>';
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$selected = "";
		echo '<option value="'.$category_id.'">'.$row['cat_name'].'</option>';	
	}
	echo "</select>";
	
	if(isset($_POST['select_all']))
	{
		$checked = 'checked="checked"';
	}
	echo '<div class="overSelect"></div>
        </div>
        <div id="checkboxes"><label><input type="checkbox" id="select_all" name="select_all" '.$checked.' /> Select all</label>';
	$result = db_query($sql);
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$checked = "";	
		if(in_array($category_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		echo '<label><input type="hidden" id="'.$category_id.'" value="'.$row['cat_name'].'" >
		<input type="checkbox" class="check"  value="'.$category_id.'" '.$checked.' name="category_id['.$category_id.']" />'.$row['cat_name'].'</label>';
	}	
    echo ' </div>
    </div>';
}
function multiple_category_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_categories_list($name, $selected_id, $spec_opt, $submit_on_change);
	echo "</td>\n";
}

function multiple_category_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_category_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change);
	echo "</tr>\n";
}


start_form(true);

function show_check_box($row) 
{
	if (@$_GET['popup'])
		return '';
  	return pager_link( _("Edit"),
		"/inventory/manage/opening_stock_master.php?stock_id=", ICON_EDIT);
		
}
if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}


start_table(TABLESTYLE, "width=60%");
	$th = array(_('Catgory Name'),_('Range Name'),_('Finish Code'),_('Buyers Code'), _('SKU'),_('Bar Code'),_('BAR Code Image'),_('View'));
	inactive_control_column($th);
	table_header($th);
	$sql=get_all_customer_product_relation($_POST['customer_id']);
	$result= db_query($sql,"not retrive");
	$k = 0;
	
	while ($myrow = db_fetch($result)){
	
	alt_table_row_color($k);
	label_cell($myrow['cat_name']);
	label_cell($myrow['rangeNAME']);
	label_cell($myrow['finish_comp_code']);
	label_cell($myrow['buyers_code']);
	label_cell($myrow['sku']);
	label_cell($myrow['bar_code']);
	label_cell($myrow['bar_code_image']);
	label_cell('<input type="button" value="View" class="view_all" id="view_'.$myrow['finish_pro_id'].'">');
	end_row();
	}

echo "<br>";
echo '<table><tr><td><p id="pro_cat" style="max-width:700px; margin:auto;">'.$_POST['pro_categories'].'</p>
			<input type="hidden" name="pro_categories" id="pro_categories" value="'.$_POST['pro_categories'].'" >
		</td></tr></table>';
start_table(TABLESTYLE2);
echo "<br>";
table_section(1);
table_section_title(_("Search Product List"));
multiple_category_list_row(_("Category:"), 'category_id', null, _('----Select---'));
sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
if($_POST['collection_id'] == "NonCollection"){

}else{
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
}
	
	
end_table(1);
start_table(TABLESTYLE_NOBORDER, "'width=70%' 'align = centre'");
	start_row();
		echo '<input type="submit" name="Search" value="Search">';
	end_row();
end_table(1);
end_form();
echo "<br>";
echo $Mode;

//--------------------------------------------------------------------

if(isset($_POST['Process'])){
	
	$selproduct = $_POST['sel_product'];

	if (empty($selproduct)) {
    display_error("You must select consumables to process");
	$_POST['Search'] = Search;
	}
	else{
	start_table(TABLESTYLE, "width=90%");
	$th = array(_('Category Name'),_('Range Name'),_('Design Code'), _('Finish Code'),_('Buyers Code'),_('SKU'),_('BAR Code'),_('BAR Code Image'),_('Save'));
	inactive_control_column($th);
	table_header($th);
	//echo "<input type='hidden' name='add' id='add_cons' value=''>";
	
	foreach($selproduct as $val){
	$sql = get_search_finish_code_list2($val);
	$resut = db_query($sql,"not retrive");
	$k = 0;
	alt_table_row_color($k);	
	start_form_cust(true,false,"","tbs","new_form");
	$myrow = db_fetch($resut);
	
	echo '<input type="hidden" name="customer_id" id="customer_id_'.$myrow['finish_pro_id'].'" value="'.$_POST['customer_id'].'" >';
	echo '<input type="hidden" name="code_id" id="code_id_'.$myrow['finish_pro_id'].'" value="'.$myrow['finish_pro_id'].'" >';
	hidden("customer_id".$myrow['finish_pro_id'],$_POST['customer_id'],"fafaf","fsafs");
	
	label_cell($myrow['cat_name']);
	label_cell($myrow['rangeNAME']);
	label_cell($myrow['design_code']);
	label_cell($myrow['finish_comp_code']);
	label_cell('<input type="text" value="" id="buyers_code_'.$myrow['finish_pro_id'].'" name="buyers_code">');
	label_cell('<input type="text" value="" id="SKU_'.$myrow['finish_pro_id'].'" name="SKU">');
	label_cell('<input type="file" value="" id="bar_Code_'.$myrow['finish_pro_id'].'" name="bar_Code">');
	label_cell('<input type="file" value="" id="bar_code_image_'.$myrow['finish_pro_id'].'" name="bar_code_image">');
	
	label_cell('<input type="submit" value="Add" name="submit'.$myrow['finish_pro_id'].'" class="code_submit" id="submit_'.$myrow['finish_pro_id'].'">');
	end_form();
	end_row();
	
}
}
}

if(isset($_POST['Search'])){
$category_id = array();
$category_id = $_POST['category_id'];
	
	div_start('customer_product_inner_div','style="height:300px;  overflow-y: scroll; width: 100%;"' );
	start_table(TABLESTYLE, "width=60%");
	$th = array(_('Category Name'),_('Range Name'),_('Design Code'), _('Finish Code'),_('Select Product'));
	inactive_control_column($th);
	table_header($th);
	$k = 0;
	
	foreach($category_id as $cat_id)
	{
		$sql = get_search_finish_code_list($cat_id);
		$resut = db_query($sql,"not retrive");
		
		while ($myrow = db_fetch($resut)) 
		{
			alt_table_row_color($k);	
			label_cell($myrow['cat_name']);
			label_cell($myrow['rangeNAME']);
			label_cell($myrow['design_code']);
			label_cell($myrow['finish_comp_code']);
			label_cell('<input type="checkbox" name="sel_product[]" value="'.$myrow['finish_pro_id'].'">');
			hidden('code_id',$myrow[4]);
			hidden('view_list',view_list);
			
		}
	}
end_table();
div_start('process_button');
		echo '<center><input type="submit" name="Process" value="Process" id="process"></center>';
div_end();
div_end();
end_form();
}

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
 $('form[id^=new_form]').submit(function (event) {
    event.preventDefault();
    //grab all form data  
     var formData = new FormData($(this)[0]);
  
    $.ajax({
        url: "../inquiry/customer_reletion_product.php",
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (returndata) {
        alert(returndata);
        },
        error: function(){
            alert("error in ajax form submission");
            }
    });
	location.reload();
    return false;
    });
});
</script>

