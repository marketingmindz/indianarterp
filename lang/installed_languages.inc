<?php

/* How to make new entries here for non-packaged languages:

-- 'code' should match the name of the directory for the language under \lang
.-- 'name' is the name that will be displayed in the language selection list (in Users and Display Setup)
-- 'rtl' only needs to be set for right-to-left languages like Arabic and Hebrew
-- 'encoding' used in translation file
-- 'version' always set to '' for manually installed languages.
-- 'path' installation path related to FA root (e.g. 'lang/en_US').
*/


$installed_languages = array (
  0 => 
  array (
    'code' => 'C',
    'name' => 'English',
    'encoding' => 'iso-8859-1',
  ),
  1 => 
  array (
    'name' => 'Arabic',
    'package' => 'ar_EG',
    'code' => 'ar_EG',
    'encoding' => 'utf-8',
    'version' => '2.3.12-1',
    'path' => 'lang/ar_EG',
    'rtl' => true,
  ),
  2 => 
  array (
    'name' => 'Bulgarian',
    'package' => 'bg_BG',
    'code' => 'bg_BG',
    'encoding' => 'utf-8',
    'version' => '2.3.0-2',
    'path' => 'lang/bg_BG',
  ),
  3 => 
  array (
    'name' => 'Bosnian',
    'package' => 'bs_BA',
    'code' => 'bs_BA',
    'encoding' => 'iso-8859-2',
    'version' => '2.3.0-2',
    'path' => 'lang/bs_BA',
  ),
  4 => 
  array (
    'name' => 'Danish',
    'package' => 'da_DK',
    'code' => 'da_DK',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-3',
    'path' => 'lang/da_DK',
  ),
  5 => 
  array (
    'name' => 'Swiss German',
    'package' => 'de_CH',
    'code' => 'de_CH',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/de_CH',
  ),
  6 => 
  array (
    'name' => 'German',
    'package' => 'de_DE',
    'code' => 'de_DE',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/de_DE',
  ),
  7 => 
  array (
    'name' => 'Greek',
    'package' => 'el_GR',
    'code' => 'el_GR',
    'encoding' => 'UTF-8',
    'version' => '2.3.19-1',
    'path' => 'lang/el_GR',
  ),
  8 => 
  array (
    'name' => 'Australian English',
    'package' => 'en_AU',
    'code' => 'en_AU',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.2-1',
    'path' => 'lang/en_AU',
  ),
  9 => 
  array (
    'name' => 'English (IN)',
    'package' => 'en_IN',
    'code' => 'en_IN',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.15-1',
    'path' => 'lang/en_IN',
  ),
  10 => 
  array (
    'name' => 'English (NZ)',
    'package' => 'en_NZ',
    'code' => 'en_NZ',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.2-2',
    'path' => 'lang/en_NZ',
  ),
  11 => 
  array (
    'name' => 'English (US)',
    'package' => 'en_US',
    'code' => 'en_US',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.12-1',
    'path' => 'lang/en_US',
  ),
  12 => 
  array (
    'name' => 'English (South Africa)',
    'package' => 'en_ZA',
    'code' => 'en_ZA',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.17-1',
    'path' => 'lang/en_ZA',
  ),
  13 => 
  array (
    'name' => 'Spanish (Argentina)',
    'package' => 'es_AR',
    'code' => 'es_AR',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/es_AR',
  ),
  14 => 
  array (
    'name' => 'Spanish (Chile)',
    'package' => 'es_CL',
    'code' => 'es_CL',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.13-2',
    'path' => 'lang/es_CL',
  ),
  15 => 
  array (
    'name' => 'Spanish (Colombia)',
    'package' => 'es_CO',
    'code' => 'es_CO',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/es_CO',
  ),
  16 => 
  array (
    'name' => 'Spanish (Spain)',
    'package' => 'es_ES',
    'code' => 'es_ES',
    'encoding' => 'utf-8',
    'version' => '2.3.12-1',
    'path' => 'lang/es_ES',
  ),
  17 => 
  array (
    'name' => 'Spanish (Mexico)',
    'package' => 'es_MX',
    'code' => 'es_MX',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/es_MX',
  ),
  18 => 
  array (
    'name' => 'Spanish (Nicaragua)',
    'package' => 'es_NI',
    'code' => 'es_NI',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.10-1',
    'path' => 'lang/es_NI',
  ),
  19 => 
  array (
    'name' => 'Spanish (Panama)',
    'package' => 'es_PA',
    'code' => 'es_PA',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/es_PA',
  ),
  20 => 
  array (
    'name' => 'French',
    'package' => 'fr_FR',
    'code' => 'fr_FR',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/fr_FR',
  ),
  21 => 
  array (
    'name' => 'Hungarian',
    'package' => 'hu_HU',
    'code' => 'hu_HU',
    'encoding' => 'iso-8859-2',
    'version' => '2.3.0-2',
    'path' => 'lang/hu_HU',
  ),
  22 => 
  array (
    'name' => 'Indonesian',
    'package' => 'id_ID',
    'code' => 'id_ID',
    'encoding' => 'utf-8',
    'version' => '2.3.0-4',
    'path' => 'lang/id_ID',
  ),
  23 => 
  array (
    'name' => 'Italian',
    'package' => 'it_IT',
    'code' => 'it_IT',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-1',
    'path' => 'lang/it_IT',
  ),
  24 => 
  array (
    'name' => 'Georgian',
    'package' => 'ka_GE',
    'code' => 'ka_GE',
    'encoding' => 'utf-8',
    'version' => '2.3.1-5',
    'path' => 'lang/ka_GE',
  ),
  25 => 
  array (
    'name' => 'Norwegian',
    'package' => 'nb_NO',
    'code' => 'nb_NO',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.11-1',
    'path' => 'lang/nb_NO',
  ),
  26 => 
  array (
    'name' => 'Dutch (Belgium)',
    'package' => 'nl_BE',
    'code' => 'nl_BE',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/nl_BE',
  ),
  27 => 
  array (
    'name' => 'Dutch',
    'package' => 'nl_NL',
    'code' => 'nl_NL',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.19-2',
    'path' => 'lang/nl_NL',
  ),
  28 => 
  array (
    'name' => 'Polish',
    'package' => 'pl_PL',
    'code' => 'pl_PL',
    'encoding' => 'iso-8859-2',
    'version' => '2.3.1-1',
    'path' => 'lang/pl_PL',
  ),
  29 => 
  array (
    'name' => 'Portuguese (Brazil)',
    'package' => 'pt_BR',
    'code' => 'pt_BR',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-2',
    'path' => 'lang/pt_BR',
  ),
  30 => 
  array (
    'name' => 'Portuguese',
    'package' => 'pt_PT',
    'code' => 'pt_PT',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.17-1',
    'path' => 'lang/pt_PT',
  ),
  31 => 
  array (
    'name' => 'Romanian',
    'package' => 'ro_RO',
    'code' => 'ro_RO',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-1',
    'path' => 'lang/ro_RO',
  ),
  32 => 
  array (
    'name' => 'Russian',
    'package' => 'ru_RU',
    'code' => 'ru_RU',
    'encoding' => 'UTF-8',
    'version' => '2.3.15-1',
    'path' => 'lang/ru_RU',
  ),
  33 => 
  array (
    'name' => 'Swedish',
    'package' => 'sv_SE',
    'code' => 'sv_SE',
    'encoding' => 'iso-8859-1',
    'version' => '2.3.0-3',
    'path' => 'lang/sv_SE',
  ),
  34 => 
  array (
    'name' => 'Urdu',
    'package' => 'ur_PK',
    'code' => 'ur_PK',
    'encoding' => 'utf-8',
    'version' => '2.3.14-1',
    'path' => 'lang/ur_PK',
    'rtl' => true,
  ),
  35 => 
  array (
    'name' => 'Chinese (Simplified)',
    'package' => 'zh_CN',
    'code' => 'zh_CN',
    'encoding' => 'utf-8',
    'version' => '2.3.0-4',
    'path' => 'lang/zh_CN',
  ),
  36 => 
  array (
    'name' => 'Chinese (Traditional)',
    'package' => 'zh_TW',
    'code' => 'zh_TW',
    'encoding' => 'utf-8',
    'version' => '2.3.0-2',
    'path' => 'lang/zh_TW',
  ),
);

$dflt_lang = 'C';
?>
