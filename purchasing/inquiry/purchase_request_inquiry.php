<?php

	/**********************************************************************
	Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
	See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
	***********************************************************************/

$page_security = 'SA_PURCHASEREQUESTAPPROVAL';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_request_db.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_request_ui.inc");
if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
	page(_($help_context = "Approve Purchase Requests"), false, false, "", $js);
}
if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';

	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);
	//$Ajax->addDisable(true, 'StockLocation', $disable);
	//$Ajax->addDisable(true, '_SelectStockFromList_edit', $disable);
	//$Ajax->addDisable(true, 'SelectStockFromList', $disable);

	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} else
		$Ajax->addFocus(true, 'OrdersAfterDate');

	$Ajax->activate('orders_tbl');
}
//---------------------------------------------------------------------------------------------

if (!@$_GET['popup'])
	start_form();

start_table(TABLESTYLE_NOBORDER, "style=min-width:320px;width:60%;");
start_row();
	ref_cells(_("Purchase Request#:"), 'order_number', '',null, '', true);
	date_cells(_("Date from:"), 'OrdersAfterDate', '', null, -60);
	date_cells(_("to:"), 'OrdersToDate');
		
end_row();

start_row();
	purchase_request_type();
	purchase_request_status();
end_row();

    design_location_list_row(_("Location:"), 'location_id', null, _('------Select Location-----'));
	if($_POST['location_id'] == '-1'){
		design_work_center_list_row(_("Work Center:"), 'work_center_id', null, _('-----Select Work Center----'));
	}
	else
	{
		stock_work_center_list_row(_("Work Center:"), 'work_center_id', null, _('-----Select Work Center----'));
	}


/*stock_master_list_cells("Consumable Type", 'cons_type', null, _('----Select---'));
if($_POST['cons_type'] == '-1'){
	sub_consumable_master_list_cells("Consumable Category", 'cons_select',null, _('----Select---'));
}else{
	customer_consumable_list_cells("Consumable Category", $_POST['cons_type'],'cons_select', null, false, true, true, true);
}*/

end_table();

start_table(TABLESTYLE_NOBORDER, "style=min-width:320px;width:60%;");
start_row();

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
end_row();
end_table(1);
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];
}

if (isset($_POST['SelectStockFromList']) &&	($_POST['SelectStockFromList'] != "") &&
	($_POST['SelectStockFromList'] != ALL_TEXT))
{
 	$selected_stock_item = $_POST['SelectStockFromList'];
}
else
{
	unset($selected_stock_item);
}

//---------------------------------------------------------------------------------------------
function trans_view($trans)
{
	return get_trans_view_str(ST_PURCHORDER, $trans["order_no"]);
}

function edit_link($row) 
{
	if (@$_GET['popup'])
		return '';
  	return pager_link( _("Edit"),
		"/purchasing/purchase_request_entry.php?" . SID 
		. "PurchaseRequestId=" . $row["pr_no"], ICON_EDIT, "editLinkButton");
}
function view_link($row) 
{
  	return pager_link( _("View"),
		"/purchasing/view/view_purchase_request.php?trans_no=". $row["pr_no"], ICON_VIEW, 'viewLinkButton');
}
function approve_link($row) 
{
  	return pager_link( _("Approve"),
		"/purchasing/approve_purchase_request.php?RequestID=". $row["pr_no"], false, "approve_request");
}
function prt_link($row)
{
	return print_document_link($row['order_no'], _("Print"), true, 18, ICON_PRINT);
}

//---------------------------------------------------------------------------------------------

$sql = get_sql_for_pr_search_completed(!@$_GET['popup'] ? $_POST['supplier_id'] : ALL_TEXT);

$cols = array(
		//_("#") => array('fun'=>'trans_view', 'ord'=>''), 
		_("PR_No#") => array('ord'=>''), 
		_("Location") => array('ord'=>''),
		_("Work Center"),
		_("Order Date") => array('name'=>'ord_date', 'type'=>'date', 'ord'=>'desc'),
		_("Status") => array('ord'=>''),
		_("Type") => array('ord'=>''),
		array('insert'=>true, 'fun'=>'view_link'),
		array('insert'=>true, 'fun'=>'edit_link'),
		//array('insert'=>true, 'fun'=>'prt_link'),
		array('insert'=>true, 'fun'=>'approve_link'),
);
if (get_post('StockLocation') != $all_items) {
	$cols[_("Location")] = 'skip';
}
if ($_GET['location'] == "store") {
	unset($cols[3]); 
}

//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);
$table->width = "80%";
display_db_pager($table);

if (!@$_GET['popup'])
{
	end_form();
	end_page();
}	
?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">

$(document).on('change','#slc_master',function(){
//alert("data is synchronization successfully .... ");
	var slc_master = $(this).val();
	$.ajax({
		url: "sub_master_calling.php",
		method: "POST",
		data: { id : slc_master},
		success: function(data){
				var select_val = $('#sub_master');
				select_val.empty().append(data);
			}
	});
	return false;
});
$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "../manage/slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>