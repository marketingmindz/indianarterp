<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
$page_security = 'SA_PURCHASEREQUEST';

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/purchase_request_class.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_request_db.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_request_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
//include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");

set_page_security( @$_SESSION['PO']->trans_type,
	array(	ST_PURCHREQUEST => 'SA_PURCHASEREQUEST')
);

$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Purchase request"), false, false, "", $js);
if(!isset($_POST['session_var']))
{
	if(isset($_GET['PurchaseRequestId'])){
		unset($_SESSION['cons_part']);		 
	}
}
if(isset($_SESSION['cons_part'])){
	function fixObject (&$object)
	{
		if (!is_object ($object) && gettype ($object) == 'object')
			return ($object = unserialize (serialize ($object)));
		return $object;
	}
	fixObject($_SESSION['cons_part']);
}else{
	unset($_SESSION['cons_part']);
   $_SESSION['cons_part'] = new purchase_cons_request;
}

//  code by bajrang   17/07/2015
if(!isset($_GET['PurchaseRequestId']))
{
	if(!isset($_POST['session_var']))
	{
		if(!isset($_POST['ConsumeAddItem']))
		{
			unset($_SESSION['cons_part']);
		}
	}
}
if(isset($_REQUEST['PurchaseRequestId']))
{
	$selected_id = $_REQUEST['PurchaseRequestId'];

	if(isset($_GET['location']) && $_GET['location'] == "store")
    {
    	$isSubmitted = isSubmittedForApproval($selected_id);
		if ($isSubmitted) {
			display_error("You cannot edit this request. This is already submitted for approval.");
			return false;
		}
    }

	$editable = checkEditable($selected_id);
	if (!$editable) {
		display_error("You cannot edit this request. This is already processed.");
		return false;
	}
	else{
	    $_SESSION['s_id'] = $selected_id;
	    $result = read_purchase_request_header($selected_id);
	    read_cons_purchase_item($selected_id, $_SESSION['cons_part']);
	}
}
else{
	$selected_id = -1;
}
//------------ handle new consumable part ------------

function handle_new_consumable()
{
	$check_data = consumable_check_data();
	if($check_data){
		$_SESSION['cons_part']->add_to_consuable_part(count($_SESSION['cons_part']->line_con),$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['in_stock'],$_POST['cons_quntity']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}
//-------------- end new consumable part ---------------

//------------ delete consumable type ------------
function handle_consumable_delete_item($line_no)
{
	array_splice($_SESSION['cons_part']->line_con, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end delete consumable type ---------------

//------------ update consumable type ------------
function handle_consumable_type_update()
{
   $_SESSION['cons_part']->update_cons_item($_POST['line_no'],$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['in_stock'],$_POST['cons_quntity']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}
//-------------- end update assemble code ---------------
//*****************  check data    --**************************
function consumable_check_data()
{
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	
	if(!get_post('cons_quntity')) {
		display_error( _("Quantity cannot be empty."));
		set_focus('cons_quntity');
		return false;
	}
	if (!check_num('cons_quntity', 0))
    {
	   	display_error(_("The quntity entered must be numeric and not less than zero."));
		set_focus('cons_quntity');
	   	return false;	   
    }
    return true;	
}
//********************* end check data  ********************

function request_check_data()
{
	if(get_post('location_id') == -1) {
		display_error( _("location cannot be empty."));
		set_focus('location_id');
		return false;
	}
	if(get_post('work_center_id') == -1) {
		display_error( _("Work Center cannot be empty."));
		set_focus('work_center_id');
		return false;
	}
	if(get_post('memo_description') == '') {
		display_error( _("memo description cannot be empty."));
		set_focus('memo_description');
		return false;
	}
    return true;	
}


//     ++++++++++     Unset variables    ++++++++++++
function unset_form_cons_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_quntity']);
    unset($_POST['in_stock']);
}

//++++++++++++++++++  end unset variables   +++++++

function handle_purchase_request()
{
	$check_data = request_check_data();
	if($check_data){

	$header_request = array();
	$header_request['pr_no'] = trim($_POST['pr_no']);
	$header_request['date'] = trim($_POST['date']);
	$header_request['location_id'] = trim($_POST['location_id']);
	$header_request['work_center_id'] = trim($_POST['work_center_id']);
	$header_request['memo_description'] = trim($_POST['memo_description']);
	$consumable_type = &$_SESSION['cons_part'];
	$trans_no = new_purchase_request($header_request,$consumable_type);
	unset($_SESSION['cons_part']);
	
	meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
	}
}

function handle_update_purchase_request()
  {
    global $selected_id;
    $selected_id = $_SESSION['s_id'];
    $check_data = request_check_data();
	if($check_data){
	    $header_request = array();
	    $header_request['pr_no'] = trim($_POST['pr_no']);
	    $header_request['location_id'] = trim($_POST['location_id']);
	    $header_request['work_center_id'] = trim($_POST['work_center_id']);
	    $header_request['date'] = trim($_POST['date']);
		$header_request['memo_description'] = trim($_POST['memo_description']);
	    $consumable_type = &$_SESSION['cons_part'];
	    $trans_no = update_purchase_request($selected_id,$header_request,$consumable_type);
	    unset($_SESSION['cons_part']);
	    
	    //meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
	    if(isset($_POST['location']) && $_POST['location'] == "store")
	    {
	    	meta_forward($path_to_root.'inquiry/store_pr_inquiry.php?location=store&erp');
	    }
	    else
	    {
	    	meta_forward($path_to_root.'inquiry/purchase_request_inquiry.php');
	    }
	}
  }

//------------------- line start table ------------
function line_start_consumable_focus() {
  global 	$Ajax;

  $Ajax->activate('cons_table');
  //set_focus('part_name');
}



$id = find_submit('Delete');
if ($id != -1)
{
	handle_consumable_delete_item($id);
}
if (isset($_POST['ConsumeAddItem']))
{
	handle_new_consumable();
}
if (isset($_POST['ConsumeUpdateItem'])){
 handle_consumable_type_update();
}
if (isset($_POST['ConsumeCancelItemChanges'])) {
	line_start_consumable_focus();
}

if(isset($_POST['AddProcess']))
{
	handle_purchase_request();
}

if (isset($_POST['UpdateProcess']))
  {
    handle_update_purchase_request();
    //unset($_SESSION['cons_part']);
  }
if (isset($_POST['CancelProcess']))
{
  unset($_SESSION['cons_part']);
  meta_forward('inquiry/purchase_request_inquiry.php?');
}
//----------------------------------------------------------------------------------

//  notification  - purchase request entered
if (isset($_GET['AddedID'])) {
	$order_no = $_GET['AddedID'];
	$trans_type = ST_PURCHREQUEST;
	display_notification_centered(sprintf( _("Purchase Request # %d has been entered."),$order_no));
	display_note(get_trans_view_str($trans_type, $order_no, _("&View this Purchase Request")), 0, 1);
	//submenu_print(_("&Print This Order"), ST_SALESORDER, $order_no, 'prtopt');
	//submenu_print(_("&Email This Order"), ST_SALESORDER, $order_no, null, 1);
	submenu_option(_("Enter a &Another Purchase Request"),	"/purchasing/purchase_request_entry.php");
	submenu_option(_("Select an outstanding Purchase request"),	"/purchasing/purchase_request_entry.php");
	//display_footer_exit();

}
else
{
	
	start_form(true);
	if(isset($_GET['location']) &&  $_GET['location'] == "store")
	{
		echo "<input type='hidden' name='location' value='store' >";
	}
	display_hearder($_SESSION['PUSH']);
	
	display_consumable_summary($_SESSION['cons_part']);

	if($selected_id == '-1'){
		
		  submit_center_last('AddProcess', _("Submit Purchase Request"), '', 'default');
		
	 }else{
		echo "<br><center>";
		submit_center_first('UpdateProcess', _("Update "), '', 'default');
		submit_center_last('CancelProcess', _("Cancel "), '', 'default');
		echo "</center>";
	 }

	end_form();
}
end_page();

?>

<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}

</style>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">



$(document).ready(function(e) {
var pr_no = '';
	$.ajax({
		url: "purchase_request_no.php",
		method: "POST",
		data: { r_id : pr_no},
		success: function(data){
				 $('#pr_no').val(data);
			}
	});
	return false;
});
$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "manage/slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});

$(document).on('change','#slc_master',function(){
		//alert("data is synchronization successfully .... ")
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
	
$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	var consumable_id  = $("#slc_master").val();
	var location = $("#slc_location").val();
	var work_center = $("#slc_work_center").val();
	$.ajax({
		url: "manage/unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	$.ajax({
		url: "manage/in_stock.php",
		method: "POST",
		data: { consumable_id:consumable_id, consumable_category:cons_cat_id, from_location:location, from_work_center:work_center },
		success: function(data){
				var select_val = $('#in_stock');
				select_val.val(data);
			}
	});
	return false;
});
</script>