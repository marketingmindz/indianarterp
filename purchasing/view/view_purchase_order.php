<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PURCHASEORDERINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/purchasing/includes/purchase_order_class.inc");

include($path_to_root . "/includes/session.inc");
include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/purchasing/includes/ui/po_order_ui.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_order_db.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");

?>
<style type="text/css">
	@media print {
    #printbtn {
        display :  none;
    }
    #closebtn
    {
    	display :  none;
    }

}

table.logoutBar {
    display: none;
}

table td {
	max-width:200px;
}
</style>


<?php


$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
page(_($help_context = "View Purchase Order"), true, true, true, $js);


if (!isset($_GET['trans_no']))
{
	die ("<br>" . _("This page must be called with a purchase request number to review."));
}

/*display_heading(_("Purchase Order"));
*/
$purchase_request = new purchase_order;

read_purchase_order($_GET['trans_no'], $purchase_request);

display_purchase_order_summary($purchase_request, true);

	//start_table(TABLESTYLE, "style=min-width:320px;width:60%;", 6);
	
	start_row();
	echo "<td valign=top colspan=2>"; // outer table

	display_heading(_("Item Details"));

	start_table("", "colspan=9 class='items' style=min-width:320px;width:100%;");

	$th = array(_("Consumable Category"), _("Consumable Name"), _("Unit"),_("Quantity")); /* , _("Price"),_("Total")*/
	table_header($th);
	$total = $k = 0;
	$overdue_items = false;
	/*$unit_total = $total_order_cost = 0;*/
	foreach ($purchase_request->line_items as $stock_item)
	{

		$line_total = $stock_item->quantity * $stock_item->price;

		// if overdue and outstanding quantities, then highlight as so
		/*if (($stock_item->quantity - $stock_item->qty_received > 0)	&&
			date1_greater_date2(Today(), $stock_item->req_del_date))
		{
	    	start_row("class='overduebg'");
	    	$overdue_items = true;
		}
		else
		{
			alt_table_row_color($k);
		}*/
		start_row();
		label_cell($stock_item->cons_name, "align=left");
		$brand = $stock_item->company_name == "" ? "No Brand" : $stock_item->company_name;
		$consumable_name = $stock_item->cons_cat." - ".$brand." - ".$stock_item->consumable_code;
	 	label_cell($consumable_name, "align=left");
		label_cell($stock_item->unit, "align=center");
		label_cell($stock_item->quantity, "align=center");
		/*label_cell($stock_item->purchase_price, "align=center");
		$unit_total = $stock_item->quantity*$stock_item->purchase_price;
		label_cell($unit_total, "align=center");
		$total_order_cost += $unit_total;*/
		end_row();

		/*$total_order_cost += $line_total;*/
	}

	/* start_row();		
		label_cell("Total :", " class='tableheader' style='text-align:right;' colspan=5");
		label_cell($total_order_cost, "class='tableheader' align=center");
	end_row();
	end_table(); */

	$k = 0;

	echo "</td>";
	end_row();

//end_table(1); // outer table

$myrow = get_company_pref();
start_row();
label_cell("<br>RECEIVER NAME","colspan=3");
label_cell("<br>FOR ".strtoupper($myrow['coy_name']),"align=right");
end_row();

echo "<tr  rowspan=5><td colspan=2><br></td></tr>";

//start_table(TABLESTYLE1, "style=min-width:320px;width:70%;", 6);
start_row();
label_cell("RECEIVER'S SIGNATURE/DATE","colspan=3");
label_cell("AUTHORISED SIGNATORY","align=right");
end_row();
//end_table();

echo "<tr  rowspan=5><td  colspan=2> <br></td></tr>";

//start_table(TABLESTYLE1, "width=70%", 6);
start_row();
echo "<td> <img src='".$path_to_root."/purchasing/view/po_note.gif"."' /> </td>";
end_row();
end_table();

end_page(true, false, false, ST_PURCHORDER, $_GET['trans_no']);

?>