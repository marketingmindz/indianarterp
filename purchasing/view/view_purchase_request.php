<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PURCHASEREQUEST';
$path_to_root = "../..";
include($path_to_root . "/purchasing/includes/pr_class.inc");

include($path_to_root . "/includes/session.inc");
include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_request_ui.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_request_db.inc");

?>
<style type="text/css">
	@media print {
    #printbtn {
        display :  none;
    }
    #closebtn
    {
    	display :  none;
    }

    @page {
	    size: auto;   /* auto is the initial value */
	    margin: 0;  /* this affects the margin in the printer settings */
	}
}

</style>


<?php



$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
page(_($help_context = "View Purchase Request"), true, false, "", $js);


if (!isset($_GET['trans_no']))
{
	die ("<br>" . _("This page must be called with a purchase request number to review."));
}

display_heading(_("Purchase Request") . " #" . $_GET['trans_no']);

$purchase_request = new purch_request;

read_pr($_GET['trans_no'], $purchase_request);
echo "<br>";
display_pr_summary($purchase_request, true);

start_table(TABLESTYLE, "style=min-width:320px;width:50%;");
echo "<tr><td valign=top>"; // outer table

display_heading(_("Item Details"));

start_table(TABLESTYLE, "colspan=9 style=min-width:320px;width:100%;");

$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"),_("Quantity"));
table_header($th);
$total = $k = 0;
$overdue_items = false;

foreach ($purchase_request->line_items as $stock_item)
{

	$line_total = $stock_item->quantity * $stock_item->price;

	// if overdue and outstanding quantities, then highlight as so
	if (($stock_item->quantity - $stock_item->qty_received > 0)	&&
		date1_greater_date2(Today(), $stock_item->req_del_date))
	{
    	start_row("class='overduebg'");
    	$overdue_items = true;
	}
	else
	{
		alt_table_row_color($k);
	}

	label_cell($stock_item->cons_name);
	$brand = $stock_item->company_name == "" ? "No Brand" : $stock_item->company_name;
	$consumable_name = $stock_item->cons_cat." - ".$brand." - ".$stock_item->consumable_code;
 	label_cell($consumable_name);
	label_cell($stock_item->unit);
	label_cell($stock_item->quantity);
	end_row();

	$total += $line_total;
}


end_table();
echo "<br>";
div_start('memo');
      start_table(TABLESTYLE, "style=min-width:320px;width:100%;");
		textarea_row(_('Memo:'), 'memo_description',  $purchase_request->memo_description, 50, 3, null);
		hidden('session_var', '1');  //  code by bajrang   17/07/2015
		end_table(1);
    div_end();
$k = 0;



echo "</td></tr>";

end_table(1); // outer table

//----------------------------------------------------------------------------------------------------

end_page(true, false, false, ST_PURCHORDER, $_GET['trans_no']);

?>
