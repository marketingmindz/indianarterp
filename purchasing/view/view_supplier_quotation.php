<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PURCHASEORDERINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/purchasing/includes/purchase_order_class.inc");

include($path_to_root . "/includes/session.inc");
include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/purchasing/includes/ui/supplier_quotation_ui.inc");
include_once($path_to_root . "/purchasing/includes/db/supplier_db.inc");
include_once($path_to_root . "/purchasing/includes/consumables_class.inc");

?>
<style type="text/css">
	@media print {
    #printbtn {
        display :  none;
    }
    #closebtn
    {
    	display :  none;
    }

}

table td {
	max-width:200px;
}
</style>


<?php


$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
page(_($help_context = "View Supplier Quotation"), true, false, "", $js);


if (!isset($_GET['trans_no']))
{
	die ("<br>" . _("This page must be called with a purchase request number to review."));
}

display_heading(_("Supplier Quotation"));

$cons = new consumable;
read_quotation_items($_GET['trans_no'], $cons);	

$row = get_supplier_quotation($_GET['trans_no']);
	start_outer_table(TABLESTYLE2, "style=min-width:320px;width:100%;");
	table_section(1);
	
	start_row();
		label_cells(_("Date"), $row['date'], true);
		label_cells(_("Delivery Date"), $row['del_date']);
	end_row();
	start_row();		
		label_cells(_("Quote No.:"), $row['quote_no']);

	end_row();
	end_outer_table(1);

start_table(TABLESTYLE, "style=min-width:320px;width:100%;", 6);
	
	start_row();
	echo "<td valign=top colspan=2>"; // outer table

	display_heading(_("Item Details"));

	start_table("", "colspan=9 class='items' style=min-width:320px;width:100%;");

	$th = array(_("Consumable Category"), _("Consumable Name"), _("Unit"),_("Rate"));
	table_header($th);
	foreach ($cons->line_con as $con_line)
	{
		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_cons_category($cons_type);
			$const_type = $get_cons_type['master_name'];
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_cons_name($cons_select);
			
			$const_select = $get_cons_select['consumable_name'];
			$brand = $get_cons_select['company_name'] == "" ? "No Brand" : $get_cons_select['company_name'];
			$consumable_name = $const_select." - ".$brand." - ".$get_cons_select['consumable_code'];
        	label_cell($const_type);
    		label_cell($consumable_name);
    		label_cell($con_line->cons_unit);
			label_cell($con_line->cons_rate);
		end_row();
	}

	end_table();

	$k = 0;

	echo "</td>";
	end_row();

end_table(1); // outer table




end_page(true, false, false, ST_PURCHORDER, $_GET['trans_no']);

?>
