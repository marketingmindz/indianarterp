<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
$page_security = 'SA_PURCHASEREQUESTUSINGREORDER';

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/purchase_request_class.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_request_using_reorder_db.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_request_using_reorder_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
//include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");

//include_once($path_to_root . "/reporting/includes/reporting.inc");

set_page_security( @$_SESSION['PO']->trans_type,
	array(	ST_PURCHREQUEST => 'SA_PURCHASEREQUESTUSINGREORDER')
);

$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Purchase request"), false, false, "", $js);


function request_check_data()
{
	if(get_post('location_id') == -1) {
		display_error( _("location cannot be empty."));
		set_focus('location_id');
		return false;
	}
	if(get_post('work_center_id') == -1) {
		display_error( _("Work Center cannot be empty."));
		set_focus('work_center_id');
		return false;
	}
    return true;	
}
function handle_purchase_request()
{
	$check_data = request_check_data();
	if($check_data){
	$header_request = array();
	$header_request['date'] = trim($_POST['date']);
	$header_request['location_id'] = trim($_POST['location_id']);
	$header_request['work_center_id'] = trim($_POST['work_center_id']);
	$trans_no = new_purchase_request($header_request);

    meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
	}
}

//  notification  - purchase request entered
if (isset($_GET['AddedID'])) {
	$order_no = $_GET['AddedID'];
	$trans_type = ST_PURCHREQUEST;
	display_notification_centered(sprintf( _("Purchase Request # %d has been entered."),$order_no));
	display_note(get_trans_view_str($trans_type, $order_no, _("&View this Purchase Request")), 0, 1);
	//submenu_print(_("&Print This Order"), ST_SALESORDER, $order_no, 'prtopt');
	//submenu_print(_("&Email This Order"), ST_SALESORDER, $order_no, null, 1);
	submenu_option(_("Enter a &Another Purchase Request"),	"/purchasing/purchase_request_entry.php");

	//display_footer_exit();
}
else
{
	global 	$Ajax;
	$Ajax->activate('details');
	div_start('details');

	start_form(true);
		display_hearder($_SESSION['PUSH']);


	if(isset($_POST['AddProcess']))
	{
		handle_purchase_request();
	}
	if(isset($_POST['ProcessRequest']))
	{
		$check_data = request_check_data();
		if($check_data){		
			div_start('cons_table');

			$sql = "SELECT * from  ".TB_PREF."opening_stock_master where location_id = ".db_escape($_POST['location_id'])." AND work_center_id = ".db_escape($_POST['work_center_id']). " and stock_qty < level";
			$result = db_query($sql,"stock cannot be retrived - #ioioio");
			if(db_num_rows($result) > 0 )
			{
				echo "<br>";
				display_heading("Requested Products");
				start_table(TABLESTYLE, "colspan=7 style=min-width:320px;width:60%;");
				$th = array(_("Consumable category"),_("Consumable Name"), _(" Unit "),  _(" In Stock "), _("Requested Quantity"));
				if (count($order->line_con)) $th[] = '';
				table_header($th);
										
				while($row = db_fetch($result))
				{
					$request_quantity = $row['level'] - $row['stock_qty'];
					$cons_type = $row['consumable_id'];
					$get_cons_type = get_cons_type_name($cons_type);
					$const_type = $get_cons_type['master_name'];
					$cons_select =  $row['consumable_category'];
					$get_cons_select = get_cons_select_name($cons_select);
					$const_select = $get_cons_select['consumable_name'];
					label_cell($const_type);
					label_cell($const_select);
					label_cell($row['unit']);
					label_cell($row['stock_qty']);
					label_cell($request_quantity);
					hidden('session_var', '1');
				end_row();
				
				}
				end_table(1);
				submit_center_last('AddProcess', _("Generate Purchase Request"), '', 'default');
			}
			else
			{
				echo "<br>";
				display_heading("No Products Available.");
			}
		}
		
	    div_end();
	}
		
	end_form();
	div_end();
}		
end_page();

?>

<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}

</style>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">

$(document).ready(function(e) {
var pr_no = '';
	$.ajax({
		url: "purchase_request_no.php",
		method: "POST",
		data: { r_id : pr_no},
		success: function(data){
				 $('#pr_no').val(data);
			}
	});
	return false;
});
$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "manage/slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});


$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "manage/unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});
</script>
