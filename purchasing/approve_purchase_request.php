<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
$page_security = 'SA_APPROVEDPURCHASEREQUESTS';

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/ui/approve_request_ui.inc");
include_once($path_to_root . "/purchasing/includes/db/approve_purchase_request_db.inc");

set_page_security( @$_SESSION['PO']->trans_type,
	array(	ST_PURCHORDER => 'SA_APPROVEDPURCHASEREQUESTS')
);


$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();


page(_($help_context = "Approve purchase Request"), false, false, "", $js);
global 	$Ajax;
$Ajax->activate('items_table');

if(isset($_GET['RequestID']))
{
	$editable = checkEditable($_GET['RequestID']);
	if (!$editable) {
		display_error("This request is already approved completely.");
		return false;
	}
}

if(isset($_POST['RemoveButton']))
{
	global 	$Ajax;
	$Ajax->activate('items_table');
	$Ajax->activate('RemoveButton1');
	$count = $_POST['count'];
	for($i=0; $i<=$count; $i++)
	{
		if($_POST['supp_id_'.$i] == -1 OR $_POST['supp_id_'.$i] == "") {
			handle_consumable_delete_item($_POST['pr_no'], $_POST['consumable_id_'.$i], $_POST['consumable_category_'.$i]);
			unset($_POST['consumable_id_'.$i]);
			unset($_POST['consumable_category_'.$i]);
			unset($_POST['unit_'.$i]);
			unset($_POST['requested_quantity_'.$i]);
			unset($_POST['app_qty_'.$i]);
			unset($_POST['supp_id_'.$i]);
		}
	}

}


/*$id = find_submit('Delete');
if ($id != -1)
{
	global 	$Ajax;
	$Ajax->activate('items_table');
	handle_consumable_delete_item($_POST['pr_no'], $_POST['consumable_id_'.$id], $_POST['consumable_category_'.$id]);
	unset($_POST['consumable_id_'.$id]);
	unset($_POST['consumable_category_'.$id]);
	unset($_POST['unit_'.$id]);
	unset($_POST['requested_quantity_'.$id]);
	unset($_POST['app_qty_'.$id]);
}*/

function request_check_data()
{ 
	$count = $_POST['count'];
	for($i=1; $i<=$count; $i++)
	{	
		if($_POST['supp_id_'.$i] == -1 || empty($_POST['supp_id_'.$i])) 
		{
			global 	$Ajax;
			//$Ajax->activate('RemoveButton');
			$Ajax->activate('items_table');
			return false;
		}
	}
    return true;	
}

if(isset($_POST['ApprovePurchaseRequest']))
{
	$check_data = request_check_data();
	if($check_data)
	{	
		$editable = checkEditable($_POST['pr_no']);
		if (!$editable) {
			meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
		}	

		$trans_no = approve_purchase_request($_POST);
		meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
	}
}
if (isset($_GET['AddedID'])) {
	$order_no = $_GET['AddedID'];
	$trans_type = ST_PURCHORDER;
	display_notification_centered(sprintf( _("This request has been Approved."),$order_no));
	submenu_option(_("&View Approved Requests"),	"/purchasing/inquiry/approved_purchase_requests.php");
	submenu_option(_("Approve &Another Purchase Requests"),	"/purchasing/inquiry/purchase_request_inquiry.php");
	display_footer_exit();
}
else
{ 
	start_form();
	$Ajax->activate('RemoveButton1');
	div_start("RemoveButton1");
	if(isset($_POST['ApprovePurchaseRequest']))
	{	$check = request_check_data();
		if(!$check)
		{	$Ajax->activate('RemoveButton1');
			$Ajax->activate('items_table');
			//echo "<center>Please Select Supplier Name for All the Items or Remove Them.</center><br>";
			display_notification("Please Select Supplier Name for All the Items or Remove Them.");
			set_focus('RemoveButton');
			display_products_without_supplier();
			echo "<center><br><input type='submit' class='ajaxsubmit' id='RemoveButton' name='RemoveButton' value='Remove Items' >
			<input type='submit'class='ajaxsubmit' id='CancelButton' name='CancelButton' value='Cancel' > </center><br><br>";
		}
	}
	div_end();

	div_start("items_table");
		
		display_purchase_order_header();
		echo "<br>";
		
		display_supp_details_header();

		echo "<br>";
		div_start('controls', 'items_table');
		
		submit_center('ApprovePurchaseRequest', 'Approve Purchase Request', true, false, 'gen_po_order');
		div_end();
		//---------------------------------------------------------------------------------------------------
		
	
	div_end();
	end_form();
}
end_page();
?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">

$(".appqtyclass").blur(function(){
	  var id = $(this).attr('id');
	  
	  var arr = id.split('_');	
	 
	var approved_qty = $('#'+id).val();
	var requested_qty = $('#reqqty_'+arr[1]).val();
	 
	if(parseInt(approved_qty) > parseInt(requested_qty)){
		alert("Approved Quantity Should Be Less Then Requested Quantity");
		$('#'+id).val('');
		$('#'+id).focus();
		
	}
	 
	 
});

/*$("#app_qty").blur(function(){
    var approved_qty = $('#app_qty').val();
	var requested_qty = $('#req_qty').val();
	
	if(parseInt(approved_qty) > parseInt(requested_qty)){
		alert("Approved Quantity Should Be Less Then Requested Quantity");
	}
	
});*/

$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "manage/slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});

$(document).on("click","#ApprovePurchaseRequest", function() {
	  window.onbeforeunload = null;
	  }); 


window.onbeforeunload = function(e) {
  if($(e.target).attr('class') != 'ajaxsubmit')
  {
	  return 'You have not approved this request completely.';
  } 
 
};

</script>