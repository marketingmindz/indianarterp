<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/purchasing/includes/ui/supplier_quotation_ui.inc");
include_once($path_to_root . "/purchasing/includes/consumables_class.inc");
include_once($path_to_root . "/purchasing/includes/db/suppliers_db.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

if(!isset($_POST['session_var']))
{
	unset($_SESSION['cons_part']);		 
}

if(isset($_SESSION['cons_part'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['cons_part']);	
}else{
	unset($_SESSION['cons_part']);
   $_SESSION['cons_part'] = new consumable;
}

if (isset($_GET['supplier_id'])) 
{
	$_POST['supplier_id'] = $_GET['supplier_id'];
}
$supplier_id = get_post('supplier_id'); 


function handle_new_consumable()
{
	$check_data = consumable_check_data();
	if($check_data){
		$_SESSION['cons_part']->add_to_consuable_part(count($_SESSION['cons_part']->line_con),$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_rate']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}

function handle_consumable_delete_item($line_no)
{
	array_splice($_SESSION['cons_part']->line_con, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

function handle_consumable_type_update()
{
   $_SESSION['cons_part']->update_cons_item($_POST['line_no'],$_POST['cons_type'],$_POST['cons_select'],$_POST['cons_unit'],$_POST['cons_rate']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

//*****************  check data    --**************************
function consumable_check_data()
{
	if(get_post('cons_type') == -1) {
		display_error( _("Type cannot be empty."));
		set_focus('cons_type');
		return false;
	}
	if(get_post('cons_select') == 0) {
		display_error( _("Select cannot be empty."));
		set_focus('cons_select');
		return false;
	}
	
	if(!get_post('cons_rate')) {
		display_error( _("Rate cannot be empty."));
		set_focus('cons_rate');
		return false;
	}
	if (!check_num('cons_rate', 0))
    {
	   	display_error(_("The Rate entered must be numeric and not less than zero."));
		set_focus('cons_rate');
	   	return false;	   
    }
    return true;	
}
//********************* end check data  ********************
function unset_form_cons_variables() {
	unset($_POST['cons_type']);
    unset($_POST['cons_select']);
    unset($_POST['cons_unit']);
    unset($_POST['cons_rate']);
}

function line_start_consumable_focus() {
  global 	$Ajax;
  $Ajax->activate('cons_table');
}


/*view file*/

if (isset($_GET['vw']))
	$view_id = $_GET['vw'];
else
	$view_id = find_submit('view');

if ($view_id != -1)
{ 
	$row = get_quotation_attachment($view_id);
	if ($row['filename'] != "")
	{
		if(in_ajax()) {
			$Ajax->popup($path_to_root."/purchasing/manage/supplier_quotation.php".'?vw='.$view_id);
		} else {
			$type = ($row['filetype']) ? $row['filetype'] : 'application/octet-stream';	
    		header("Content-type: ".$type);
    		header('Content-Length: '.$row['filesize']);
	    	/*if ($type == 'application/octet-stream')
    			header('Content-Disposition: attachment; filename='.$row['filename']);
    		else*/
    			header('Content-Transfer-Encoding: binary');
  				header('Accept-Ranges: bytes');
	 			header("Content-Disposition: inline");
	    	echo file_get_contents(company_path(). "/quotations/".$row['unique_name']);
    		exit();
		}
	}	
}

/*end view file*/

$quote_id_for_po = find_submit('SubmitPO');
if ($quote_id_for_po != -1)
{
	make_pr_for_po($quote_id_for_po);
}

$quote_id = find_submit('QuoteEdit');
if ($quote_id != -1)
{
	$_POST['quotation_id'] = $quote_id;
	global 	$Ajax;
	$Ajax->activate('_page_body');
	set_focus("quote_no");
	read_quotation($quote_id);
	$_SESSION['cons_part'] = new consumable;
	read_quotation_items($quote_id, $_SESSION['cons_part']);
}

$id = find_submit('Delete');
if ($id != -1)
{
	handle_consumable_delete_item($id);
}
if (isset($_POST['ConsumeAddItem']))
{
	handle_new_consumable();
}
if (isset($_POST['ConsumeUpdateItem'])){
 handle_consumable_type_update();
}
if (isset($_POST['ConsumeCancelItemChanges'])) {
	line_start_consumable_focus();
}


function request_check_data()
{
	if(!get_post('quote_no')) {
		display_error( _("Quote No. cannot be empty."));
		set_focus('quote_no');
		return false;
	}
    return true;	
}

function unset_form_header()
{
	unset($_POST['quote_no']);
	unset($_POST['date']);
	unset($_POST['del_date']);
	unset($_POST['remark']);
	unset($_POST['quotation_id']);
}

function handle_quotation()
{
	$check_data = request_check_data();
	if($check_data){

		$tmpname = $_FILES['filename']['tmp_name'];

		$dir =  company_path()."/quotations";
		if (!file_exists($dir))
		{
			mkdir ($dir,0777);
			$index_file = "<?php\nheader(\"Location: ../index.php\");\n?>";
			$fp = fopen($dir."/index.php", "w");
			fwrite($fp, $index_file);
			fclose($fp);
		}

		$filename = basename($_FILES['filename']['name']);
		$filesize = $_FILES['filename']['size'];
		$filetype = $_FILES['filename']['type'];
		$unique_name = uniqid('');

		//save the file
		move_uploaded_file($tmpname, $dir."/".$unique_name);
	chmod($dir."/".$unique_name, 0777);

		$quote = array();
		$quote['quote_no'] = trim($_POST['quote_no']);
		$quote['supplier_id'] = trim($_POST['supplier_id']);
		$quote['date'] = trim($_POST['date']);
		$quote['del_date'] = trim($_POST['del_date']);
		$quote['remark'] = trim($_POST['remark']);
		$quote['filename'] = trim($filename);
		$quote['filesize'] = trim($filesize);
		$quote['filetype'] = trim($filetype);
		$quote['unique_name'] = trim($unique_name);
		$consumables = &$_SESSION['cons_part'];
		$ret = new_quotation($quote,$consumables);
		if($ret)
		{
			unset($_SESSION['cons_part']);
			unset_form_header();
			global 	$Ajax;
			$Ajax->activate('_page_body');
			display_notification_centered(sprintf( _("Supplier Quotation # %d has been entered."),$quote['quote_no']));
		}
	}
}

function handle_update_quotation()
  {
    global $selected_id;
    $selected_id = $_SESSION['s_id'];
    $check_data = request_check_data();
	if($check_data){

		$tmpname = $_FILES['filename']['tmp_name'];
		$dir =  company_path()."/quotations";
		if (!file_exists($dir))
		{
			mkdir ($dir,0777);
			$index_file = "<?php\nheader(\"Location: ../index.php\");\n?>";
			$fp = fopen($dir."/index.php", "w");
			fwrite($fp, $index_file);
			fclose($fp);
		}

		$filename = basename($_FILES['filename']['name']);
		$filesize = $_FILES['filename']['size'];
		$filetype = $_FILES['filename']['type'];
		
		$row = get_quotation_attachment($_POST['quotation_id']);
	    if ($row['filename'] == "")
    		exit();
		$unique_name = $row['unique_name'];
		if ($filename && file_exists($dir."/".$unique_name))
			unlink($dir."/".$unique_name);

		//save the file
		move_uploaded_file($tmpname, $dir."/".$unique_name);
		chmod($dir."/".$unique_name, 0777);


	    $quote = array();
		$quote['quote_no'] = trim($_POST['quote_no']);
		$quote['supplier_id'] = trim($_POST['supplier_id']);
		$quote['date'] = trim($_POST['date']);
		$quote['del_date'] = trim($_POST['del_date']);
		$quote['remark'] = trim($_POST['remark']);
		$quote['filename'] = trim($filename);
		$quote['filesize'] = trim($filesize);
		$quote['filetype'] = trim($filetype);
		$quote['unique_name'] = trim($unique_name);
	    $consumables = &$_SESSION['cons_part'];
	    update_supplier_quotation($_POST['quotation_id'],$quote,$consumables);
	    unset($_SESSION['cons_part']);
	    unset_form_header();
	    global 	$Ajax;
		$Ajax->activate('_page_body');
		display_notification_centered(sprintf( _("Supplier Quotation # %d has been updated."),$quote['quote_no']));
	}
  }


if(isset($_POST['AddProcess']))
{
	handle_quotation();
}

if (isset($_POST['UpdateProcess']))
{
	handle_update_quotation();
	//unset($_SESSION['cons_part']);
}
if (isset($_POST['CancelProcess']))
{
	unset($_SESSION['cons_part']);
	unset_form_header();
	$Ajax->activate('_page_body');
}


start_form();
echo "<br>";
start_outer_table(TABLESTYLE2, 'style=min-width:320px;width:70%;');

	display_header();
	display_consumable_summary($_SESSION['cons_part']);
	if(!get_post('quotation_id')){
		
		  submit_center_first('AddProcess', _("Submit Quotation"), '', 'default');
		
	 }else{
		echo "<br><center>";
		submit_center_first('UpdateProcess', _("Update "), '', 'default');
		submit_center_last('CancelProcess', _("Cancel "), '', 'default');
		echo "</center>";
	 }
end_outer_table();
echo "<br>";

function view_file($row)
{
  	return button('view'.$row["quote_id"], _("File"), _("View File or Download"));
}

function edit_link($row) 
{
  	return button("QuoteEdit$row[quote_id]", _("Edit"),
					  _('Edit document line'), ICON_EDIT, "", "custom_edit");
}

function submit_for_PO($row) 
{
  	return '<button type="submit" class="custom_edit" name="SubmitPO4" value="1" title="Generate Purchsae Order Using this Quotation">Submit for PO</button>';
}

function view_link($row) 
{
  	return "<a target='_blank' href='../view/view_supplier_quotation.php?trans_no=".$row['quote_id']."'' onclick=\"javascript:openWindow(this.href,this.target); return false;\" >View</a>";
		
}
start_outer_table(TABLESTYLE2, 'style=min-width:320px;width:70%;');
	
	display_filters();
	$sql = get_sql_for_supplier_quotation(!@$_GET['popup'] ? $_POST['supplier_id'] : ALL_TEXT);

	$cols = array(
			_("#") => array('fun'=>'trans_view', 'ord'=>''), 
			_("Quote No#") => array('ord'=>''), 
			_("date") => array('ord'=>'', 'type'=>'date'),
			_("Delivery Date") => array('name'=>'del_date', 'type'=>'date', 'ord'=>'desc'),
			array('insert'=>true, 'fun'=>'view_file'),
			array('insert'=>true, 'fun'=>'view_link'),
			array('insert'=>true, 'fun'=>'edit_link'),
			array('insert'=>true, 'fun'=>'submit_for_PO'),
	);

	$cols[_("#")] = 'skip';

	$table =& new_db_pager('orders_tbl', $sql, $cols);
	$table->width = "80%";
	display_db_pager($table);
	
end_outer_table();

end_form();
?>
