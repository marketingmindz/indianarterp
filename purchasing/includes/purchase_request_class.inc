<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



class purchase_cons_request 
{

	//var $line_items = array();
	var $line_con = array();
	var $line_no;
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	var $pur_req_id;
	var $in_stock;
     
   
	var $lines_on_con = 0;

	//*****************************************************
	function purchase_cons_request()
	{
		$this->line_con = array();

		$this->lines_on_con = $this->pur_req_id = 0;
	}
	
	//-------------- add consumable part --------------------
	function add_to_consuable_part($line_no, $cons_type, $cons_select, $cons_unit, $in_stock, $cons_quntity)
	{
		
			$this->line_con[$line_no] = new cons_line_details($line_no, $cons_type, $cons_select, $cons_unit, $in_stock, $cons_quntity);
			$this->lines_on_con++;
			return 1;
		
	}
	

   
    function update_cons_item($line_no, $cons_type, $cons_select, $cons_unit, $in_stock, $cons_quntity)
	{
		$this->line_con[$line_no]->cons_type = $cons_type;
		$this->line_con[$line_no]->cons_select = $cons_select;
		$this->line_con[$line_no]->cons_unit = $cons_unit;
		$this->line_con[$line_no]->cons_quntity = $cons_quntity;
		$this->line_con[$line_no]->in_stock = $in_stock;
	}
   
   
   
   
    
	//**************** ***************
	function order_cons_has_items() 
	{
		return count($this->line_con) != 0;
	}
	
	
	function clear_cos_items() 
	{
    	unset($this->line_con);
		$this->line_con = array();	
		$this->lines_on_con = 0;  
		
	}
	
	//***********************************
   
} /* end of class defintion */

class cons_line_details 
{

	var $line_no;
	var $con_detail_rec;
	var $cons_type;
	var $cons_select;
	var $cons_unit;
	var $cons_quntity;
	var $in_stock;

	function cons_line_details($line_no, $cons_type, $cons_select, $cons_unit, $in_stock, $cons_quntity)
	{

		
		$this->line_no = $line_no;
		$this->cons_type = $cons_type;
		$this->cons_select = $cons_select;
		$this->cons_unit = $cons_unit;
		$this->cons_quntity = $cons_quntity;
		$this->in_stock = $in_stock;
		
		
	}

}
?>

