<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// checkm if request if processed completely.
function checkEditable($apr_id)
{
	$sql = "select purchase_order from  ".TB_PREF."approved_request where apr_id = ".db_escape($apr_id);
	$result =  db_query($sql, "Could not check status of Approved Purchase Request.");
	while($row = db_fetch($result)){
		$status = $row['purchase_order'] == '1'?true:false;
	}
	return $status;
}



//  code by bajrang -  request details - - -- - 
function get_request_details($pr_id)
{
  	$sql = "SELECT *, l.location_name,w.work_center_name FROM ".TB_PREF."approved_request pr LEFT JOIN ".TB_PREF."location_master l on pr.location_id = l.location_id LEFT JOIN ".TB_PREF."work_center w on pr.work_center_id = w.work_center_id WHERE apr_id = ".db_escape($pr_id);

   	$result = db_query($sql, "The Purchase_request_details can not be retrieved");
	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	$_POST['pr_no'] = $myrow["pr_no"];
		$_POST['location_id'] = $myrow["location_id"];
		$_POST['location_id_0'] = $myrow["location_name"];
		$_POST['work_center_id'] = $myrow["work_center_id"];
		$_POST['work_center_id_0'] = $myrow["work_center_name"];
      	//$_POST['pr_date'] = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no." FATAL : Purchase_request_details can not be retrieved", "", true);
	return false;
}

function get_item_details($pr_no)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT pr.*,m.master_name,c.consumable_name, c.consumable_code, i.company_name, unit,approved_quantity,supplier_id FROM ".TB_PREF."approved_request_item pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
	       WHERE apr_id =".db_escape($pr_no);
	$result = db_query($sql, "The item details cannot be retrieved");
	return $result;
}

function insert_purchase_order(&$POST)
{
	$pr_no = $POST['pr_no'];
	$apr_id = $POST['apr_id'];
	$date = date2sql($POST['po_date']);

	$editable = checkEditable($apr_id);
	if ($editable) {
		display_error("Purchase Order is already generated for this request.");
		return false;
	}
	else
	{
	
		$sql = "INSERT INTO ".TB_PREF."purchase_order(pr_id,location_id,work_center_id,date) VALUES(";
		$sql .= db_escape($pr_no) . "," .
		db_escape($POST['location_id']) . "," .
		db_escape($POST['work_center_id']) . "," .
		db_escape($date).")";
		db_query($sql, "Purchase order is not created");
		
		$po_id = db_insert_id();
			
		$byCashOrd = array();
		$uni_supplier = array();

		$count = $POST['count'];
		for($i=0; $i<=$count; $i++)
		{	
			$new_po_id = $po_id;
			if($POST['supp_id_'.$i] == "23")
			{
				$sql = "INSERT INTO ".TB_PREF."purchase_order(pr_id,location_id,work_center_id,date) VALUES(";
				$sql .= db_escape($pr_no) . "," .
				db_escape($POST['location_id']) . "," .
				db_escape($POST['work_center_id']) . "," .
				db_escape($date).")";
				db_query($sql, "Purchase order is not created");
				
				$new_po_id = db_insert_id();
				$byCashOrd[] = $new_po_id;
			}
			$sql = "INSERT INTO ".TB_PREF."purchase_order_details(po_id, consumable_id, consumable_category,unit, approved_quantity,  remaining_quantity, supplier_id) VALUES (";
			$sql .= $new_po_id . ", " . 
			db_escape($POST['consumable_id_'.$i]).",".
			db_escape($POST['consumable_category_'.$i]) . "," . 		
			db_escape($POST['unit_'.$i]) . ", " .
			db_escape($POST['approved_quantity_'.$i]) . ", " .
			db_escape($POST['approved_quantity_'.$i]) . ", " .
			db_escape($POST['supp_id_'.$i]). ")";
			db_query($sql, " purchase order details are not inserted in database");
			if($POST['requested_quantity_'.$i] != 0)
			{
				$remaining_qty = $POST['requested_quantity_'.$i] - $POST['app_qty_'.$i];
			}
			else
			{
				$remaining_qty = $POST['requested_quantity_'.$i];
			}
			$sql = "update ".TB_PREF."approved_request set purchase_order = '1' where apr_id='$apr_id'";
			db_query($sql, " Approved request status is not updated");

			if(!in_array($POST['supp_id_'.$i], $uni_supplier) || $POST['supp_id_'.$i] == "23")
			{
				$uni_supplier[] = $POST['supp_id_'.$i];
			}
		}
		/*
		$sql = "select quantity from ".TB_PREF."purchase_request_item where pr_id='$pr_no'";
		$sel_result = db_query($sql, " error while updating status of purchase request");
		while($row = db_fetch($sel_result))
		{
			if($row['quantity'] != 0)
			{
			 $status = 2;
			 break;
			}
		}
		if($status == 2)
		{
			$sql = "update ".TB_PREF."purchase_request set status = '2' where pr_no='$pr_no'";
			db_query($sql, " purchase request status is not updated error #1");
		}
		else
		{
			$sql = "update ".TB_PREF."purchase_request set status = '1' where pr_no='$pr_no'";
			db_query($sql, " purchase request status is not updated error #2");
		}
		*/

		$i=0;
		$k=0;
		foreach($uni_supplier as $suppId){
			$new_po_id = $po_id;
			if($suppId == "23")
			{
				$new_po_id = $byCashOrd["$i"];
				$i++;
			}

			$k++;
			$po_no = "PO_".$k;
			$sql = "INSERT INTO ".TB_PREF."purchase_supp_rel(po_id, supplier_id, po_no) VALUES (";
			$sql .= $new_po_id . ", " . 
			db_escape($suppId).",".
			db_escape($po_no).")";
			db_query($sql, " purchase relation");
			
		}
		return $po_id;
	}
		 
}

function get_purchase_order_search($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT po_supp_rel,l.pr_id,s.supp_name, lo.location_name,wo.work_center_name,st.selection_name,l.date
	  FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on l.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."selection st on pr.status = st.selection_id where ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.po_supp_rel LIKE ".db_escape('%'. $order_number . '%');
	}
	elseif (isset($_POST['pr_number']) && $_POST['pr_number'] != "")
	{
		$sql .= " l.pr_id LIKE ".db_escape('%'. $_POST['pr_number'] . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " l.date >= '$data_after'";
		$sql .= " AND l.date <= '$date_before'";

		if (isset($_POST['supplier_id']) && $_POST['supplier_id'] != ALL_TEXT)
		{
			$sql .= " AND pr.supplier_id = ".db_escape($_POST['supplier_id']);
		}
		/*if (isset($selected_stock_item))
		{
			$sql .= " AND line.item_code=".db_escape($selected_stock_item);
		}*/
		 //end not order number selected
		if(isset($_POST['status']) && $_POST['status'] != -1 && $_POST['status'] != 0)
		{
			$sql .= " AND pr.status = ".db_escape($_POST['status']);
		}
		elseif(!isset($_POST['status']))
		{
			$sql .= " AND pr.status != '1'";
		}

	$sql .= " GROUP BY po_supp_rel ORDER BY po_supp_rel DESC";
	

	}

	/*$result = db_query($sql, "fsfsfs");
	while($row  =db_fetch($result))
	{
		print_r($row);
	}	
	*/
	return $sql;
}





function read_purchase_order_header($order_no, &$order)
{
	$sql = "SELECT l.pr_id,po_supp_rel,s.supp_name,s.supplier_id,s.supp_address, s.tin_no, lo.location_name,wo.work_center_name,l.date FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on l.work_center_id = wo.work_center_id
     WHERE  po_supp_rel= ".db_escape($order_no);

   	$result = db_query($sql, "The order cannot be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	//$order->trans_type = ST_PURCHORDER;
      	$order->order_no = $order_no;
		$order->po_no = $myrow["pr_id"];
		$order->location_id = $myrow["location_name"];
		$order->work_center_id = $myrow["work_center_name"];
		$order->supp_name = $myrow["supp_name"];
		$order->supplier_id = $myrow["supplier_id"];
		$order->supp_address = $myrow["supp_address"];
		$order->tin_no = $myrow["tin_no"];
      	$order->po_date = sql2date($myrow["date"]);
	}

	$contacts1 = get_crm_persons("supplier", '', $order->supplier_id);
	$last = $myrow = db_fetch($contacts1);
	while ($last)
	{			
		if ($myrow['id'] != $last['id']) {
			$phone1 = $last["phone"];
			$phone2 = $last["phone2"];
			$contactName = $last["name"].' '.$last["name2"];

			$description = array();
			$last = $myrow;
		}
		if ($myrow) {
			$description[] = $myrow['description'];
			$myrow = db_fetch($contacts1);
		}
	}

	$order->contactName = $contactName;
	$order->phone = $phone1;
	if($phone1 == "")
	{
		$order->phone = $phone2;
	}
	return true;
}

//----------------------------------------------------------------------------------------

function read_purchase_order_items($order_no, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT po_id,supplier_id FROM ".TB_PREF."purchase_supp_rel where po_supp_rel =".db_escape($order_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$po_id = $myrow['po_id'];
			$supplier_id = $myrow['supplier_id'];
		}
	}
	$sql = "SELECT pr.po_id,m.master_name,c.consumable_name, c.consumable_code, i.company_name, unit,approved_quantity, sp.purchase_price FROM ".TB_PREF."purchase_order_details pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
	       LEFT JOIN ".TB_PREF."supp_product sp on sp.sub_category = pr.consumable_category && sp.supplier_id = pr.supplier_id
	       WHERE po_id =".db_escape($po_id)." && pr.supplier_id = ".db_escape($supplier_id)." && sp.supplier_id = ".db_escape($supplier_id);
		   
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_order($order->lines_on_order, $myrow["master_name"],
            	$myrow["consumable_name"],$myrow["unit"],$myrow["approved_quantity"],$myrow["consumable_code"],$myrow["company_name"],$myrow["purchase_price"])) {
            		$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

//----------------------------------------------------------------------------------------

function read_purchase_order($order_no, &$order, $open_items_only=false)
{
	$result = read_purchase_order_header($order_no, $order);

	if ($result)
		read_purchase_order_items($order_no, $order, $open_items_only);
}



function get_supplier_purchase_order($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT po_supp_rel,l.pr_id,s.supp_name, lo.location_name,wo.work_center_name,d.date, st.selection_name FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.po_id
	 LEFT JOIN ".TB_PREF."selection st on pr.status = st.selection_id where ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.po_supp_rel LIKE ".db_escape('%'. $order_number . '%');
	}
	elseif (isset($_POST['pr_number']) && $_POST['pr_number'] != "")
	{
		$sql .= " l.pr_id LIKE ".db_escape('%'. $_POST['pr_number'] . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " d.date >= '$data_after'";
		$sql .= " AND d.date <= '$date_before'";

	}
	if(isset($_POST['status']) && $_POST['status'] != 0)
	{
		$sql .= " AND pr.status = ".db_escape($_POST['status']);
	}
	$sql .= " AND pr.supplier_id = ".db_escape($_POST['supplier_id'])." GROUP BY po_supp_rel";
	//$result = db_query($sql, "cannot search suppliers purchase order");
	//$row  =db_fetch($result);

	return $sql;
}

function purchase_order_status()
{
	echo "<td>Purchase Order Status</td><td>";
	$sql = "SELECT * FROM ".TB_PREF."selection";
	$result = db_query($sql, "Purchase request status could not be retrieved.");
	echo "<select name='status' ><option value = '-1'> -- select status -- </option>";
	while($row = db_fetch($result))
	{
		echo "<option value=".$row['selection_id']." >".$row['selection_name']."</option>";
	}
	echo "</select></td>";
}
?>