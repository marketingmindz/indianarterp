<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



//delete consumbales item when approving 
function handle_consumable_delete_item($pr_no, $consumable_id, $consumable_category)
{
	$sql = "delete from  ".TB_PREF."purchase_request_item where pr_id = ".db_escape($pr_no)." AND consumable_id = ".db_escape($consumable_id)." AND consumable_category = ".db_escape($consumable_category);
	$result =  db_query($sql, "Could not delete this item.");
	 return true;
}


function get_cons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql, "could not get Consumable Type");
	return db_fetch($result);
}
function get_cons_select_name($cons_select){
	$sql = "SELECT c.consumable_id, c.consumable_name, c.consumable_code, i.company_name FROM ".TB_PREF."consumable_master c LEFT JOIN ".TB_PREF."item_company i on i.company_id = c.company_id WHERE c.consumable_id=".db_escape($cons_select);
	$result = db_query($sql, "could not get consumable category");
	return db_fetch($result);
}


// checkm if request if processed completely.
function checkEditable($pr_no)
{

	$sql = "select * from  ".TB_PREF."purchase_request where pr_no = ".db_escape($pr_no);
	$result =  db_query($sql, "Could not check status of Purchase Request.");
	while($row = db_fetch($result)){
			$status = $row['status'] == '3'?true:false;
	}
	 return $status;
}



//  code by bajrang -  request details - - -- - 
function get_request_details($pr_id)
{
   $sql = "SELECT *, l.location_name,w.work_center_name FROM ".TB_PREF."purchase_request pr LEFT JOIN ".TB_PREF."location_master l on pr.location_id = l.location_id LEFT JOIN ".TB_PREF."work_center w on pr.work_center_id = w.work_center_id WHERE pr_no = ".db_escape($pr_id);

   	$result = db_query($sql, "The Purchase_request_details can not be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	$_POST['pr_no'] = $myrow["pr_no"];
		$_POST['location_id'] = $myrow["location_id"];
		$_POST['location_id_0'] = $myrow["location_name"];
		$_POST['work_center_id'] = $myrow["work_center_id"];
		$_POST['work_center_id_0'] = $myrow["work_center_name"];
      	//$_POST['pr_date'] = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no."FATAL : Purchase_request_details can not be retrieved", "", true);
	return false;
}

function get_item_details($pr_no)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT pr.*,m.master_name,c.consumable_name, c.consumable_code, i.company_name, unit, quantity FROM ".TB_PREF."purchase_request_item pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
	       WHERE pr_id =".db_escape($pr_no);
	$result = db_query($sql, "The item details cannot be retrieved");
	return $result;
}

function get_suppliers($consumable_id, $consumable_category)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT s.supplier_id, s.supp_name,purchase_price,lead_time FROM ".TB_PREF."supp_product pr
	       Left Join ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id
	       WHERE s.inactive = '0' && catagory =".db_escape($consumable_id)." && sub_category = ".db_escape($consumable_category)
		   ." group by s.supplier_id order by purchase_price ASC";
	$result = db_query($sql, "The suppliers details cannot be retrieved");
	return $result;
}


function get_supplier_price_details($supplier_id, $consumable_id, $consumable_category)
{
    $sql = "SELECT s.supp_name,purchase_price,lead_time FROM ".TB_PREF."supp_product pr
	       Left Join ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id
	       WHERE catagory =".db_escape($consumable_id)." && sub_category = ".db_escape($consumable_category)."
		    && s.supplier_id = ".db_escape($supplier_id) ." order by purchase_price ASC";
	$result = db_query($sql, "The suppliers details cannot be retrieved");
	$supplier = "";
	while($myrow = db_fetch($result))
	{
		$supplier_id = $myrow['supplier_id'];
		$supplier = "". $myrow['supp_name']." - Price:-".$myrow['purchase_price']." - LeadTime:-".$myrow['lead_time'];
	    echo "<td>".$supplier."</td>";
	}


}
function approve_purchase_request(&$POST)
{
	$pr_no = $POST['pr_no'];
	$date = date2sql($POST['po_date']);
	$sql = "INSERT INTO ".TB_PREF."approved_request(pr_no,location_id,work_center_id,date) VALUES(";
	$sql .= db_escape($pr_no) . "," .
	db_escape($POST['location_id']) . "," .
	db_escape($POST['work_center_id']) . "," .
	db_escape($date).")";
	db_query($sql, "ERROR ### - Purchase request is not approved");
	
	$pr_id = db_insert_id(); 
	
	$count = $POST['count'];
	
	for($i=1; $i<=$count; $i++)
	{	
		
		$sql = "INSERT INTO ".TB_PREF."approved_request_item(apr_id, consumable_id, consumable_category,unit,requested_quantity, supplier_id, approved_quantity) VALUES (";
			$sql .= $pr_id . ", " . 
			db_escape($POST['consumable_id_'.$i]).",".
			db_escape($POST['consumable_category_'.$i]) . "," . 		
			db_escape($POST['unit_'.$i]) . ", " .
			db_escape($POST['requested_quantity_'.$i]) . ", " .
			db_escape($POST['supp_id_'.$i]) . ", " .
			db_escape($POST['app_qty_'.$i]).")";
			db_query($sql, " ERROR ###-Approved Request details are not inserted in database");
			//display_error($sql);
			
		if($POST['requested_quantity_'.$i] != 0)
		{
			$remaining_qty = $POST['requested_quantity_'.$i] - $POST['app_qty_'.$i];
		}
		else
		{
			$remaining_qty = $POST['requested_quantity_'.$i];
		}
		$sql = "update ".TB_PREF."purchase_request_item set quantity = '$remaining_qty' where pr_id='$pr_no' && consumable_id = '".$POST['consumable_id_'.$i]."' &&  consumable_category= '".$POST['consumable_category_'.$i]."'";
		db_query($sql, " purchase request item details are not updated");
	}


	$sql = "select quantity from ".TB_PREF."purchase_request_item where pr_id='$pr_no'";
	$sel_result = db_query($sql, " error while updating status of purchase request");
	while($row = db_fetch($sel_result))
	{
		if($row['quantity'] > 0)
		{
			$status = 2;
			break;
		}
	}
	
	if($status == 2)
	{
		//set complete if reuqest is partial - changed by client
		$sql = "update ".TB_PREF."purchase_request set status = '1' where pr_no='$pr_no'";
		db_query($sql, " purchase request status is not updated error #1");
	}
	else
	{
		$sql = "update ".TB_PREF."purchase_request set status = '1' where pr_no='$pr_no'";
		db_query($sql, " purchase request status is not updated error #2");
	}
	 /*
	$sel_supplier = "select supplier_id from ".TB_PREF."approved_request_item where pr_no=".db_escape($pr_id);
	 $sel_result = db_query($sel_supplier, "Get Supplier id");
	 $i=0;
	 $uni_supplier = array();
		
	 while($row = db_fetch($sel_result)){
		 $uni_supplier[] = $row[0];
	 }
	 $uni_supplier = array_unique($uni_supplier);
	 $k=0;
		 foreach($uni_supplier as $uni_val){
			 $k++;
			 $po_no = "PO_".$k;
			 $sql = "INSERT INTO ".TB_PREF."approved_request_supp(pr_no, supplier_id, po_no) VALUES (";
			$sql .= $po_id . ", " . 
			db_escape($uni_val).",".
			db_escape($po_no).")";
			db_query($sql, "ERROR- #### approved request relation with supplier.");
		 } */
	return $po_id;
		 
}
function get_sql_for_pr_search_completed($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT apr_id,pr_no, l.location_name, w.work_center_name, (case when (purchase_order = 1)  THEN
      'Completed' ELSE 'Pending'  END) as status , date FROM ".TB_PREF."approved_request pr LEFT JOIN ".TB_PREF."location_master l on pr.location_id = l.location_id
	 LEFT JOIN ".TB_PREF."work_center w on pr.work_center_id = w.work_center_id 
			WHERE ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.pr_no LIKE ".db_escape('%'. $order_number . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " pr.date >= '$data_after'";
		$sql .= " AND pr.date <= '$date_before'";

		if(isset($_POST['location_id']) && $_POST['location_id'] != '-1')
		 {
			 $sql .= " AND l.location_id = ".db_escape($_POST['location_id']) ;
		 }
		 if(isset($_POST['work_center_id']) && $_POST['work_center_id'] != '-1')
		 {
			 $sql .= " AND w.work_center_id = ".db_escape($_POST['work_center_id']) ;
		 }

		 if(isset($_POST['purchase_order']) && $_POST['purchase_order'] != '-1' )
		 {
			 $sql .= " AND pr.purchase_order = ".db_escape($_POST['purchase_order']);
		 }
		 elseif(!isset($_POST['purchase_order']))
		 {
		 	$_POST['purchase_order'] = 0;
		 	$sql .= " AND pr.purchase_order = ".db_escape($_POST['purchase_order']);
		 }
	$sql .= " GROUP BY pr_no ORDER BY pr_no DESC";
	}
	return $sql;
}	

function read_apr_header($order_no, &$order)
{
   	$sql = "SELECT *, l.location_name, w.work_center_name FROM ".TB_PREF."approved_request p 
	LEFT JOIN ".TB_PREF."location_master l on p.location_id = l.location_id 
	LEFT JOIN ".TB_PREF."work_center w on p.work_center_id = w.work_center_id WHERE apr_id = ".db_escape($order_no);

   	$result = db_query($sql, "The order cannot be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	//$order->trans_type = ST_PURCHORDER;
      	$order->order_no = $order_no;
		$order->pr_no = $myrow["pr_no"];
		$order->location_id = $myrow["location_name"];
		$order->work_center_id = $myrow["work_center_name"];
      	$order->pr_date = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no."FATAL : duplicate purchase requests found", "", true);
	return false;
}

//----------------------------------------------------------------------------------------

function read_apr_items($order_no, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT m.master_name,pr.consumable_id,c.consumable_name,pr.consumable_category, c.consumable_code, i.company_name, unit,supplier_id,approved_quantity FROM ".TB_PREF."approved_request_item pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
	       WHERE apr_id =".db_escape($order_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_order($order->lines_on_order, $myrow["master_name"],$myrow["consumable_id"], 
            	$myrow["consumable_name"], $myrow["consumable_category"],$myrow["unit"],$myrow["supplier_id"],$myrow["approved_quantity"],$myrow["consumable_code"],$myrow["company_name"])) {
            		$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

//----------------------------------------------------------------------------------------

function read_apr($order_no, &$order, $open_items_only=false)
{
	$result = read_apr_header($order_no, $order);

	if ($result)
		read_apr_items($order_no, $order, $open_items_only);
}



function get_supplier_purchase_order($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT po_supp_rel,l.pr_id,s.supp_name, lo.location_name,wo.work_center_name,d.date, st.selection_name FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.po_id
	 LEFT JOIN ".TB_PREF."selection st on pr.status = st.selection_id where ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.po_supp_rel LIKE ".db_escape('%'. $order_number . '%');
	}
	elseif (isset($_POST['pr_number']) && $_POST['pr_number'] != "")
	{
		$sql .= " l.pr_id LIKE ".db_escape('%'. $_POST['pr_number'] . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " d.date >= '$data_after'";
		$sql .= " AND d.date <= '$date_before'";

	}
	if(isset($_POST['status']) && $_POST['status'] != 0)
	{
		$sql .= " AND pr.status = ".db_escape($_POST['status']);
	}
	$sql .= " AND pr.supplier_id = ".db_escape($_POST['supplier_id'])." GROUP BY po_supp_rel";
	$result = db_query($sql, "cannot search suppliers purchase order");
	$row  =db_fetch($result);

	return $sql;
}


function purchase_request_status()
{
	echo "<td>Purchase Order Status</td><td>";
	echo "<select name='purchase_order' >";
		echo "<option value='-1' >All</option>";	
		echo "<option value='0' selected >Pending</option>";
		echo "<option value='1' >Completed</option>";
	echo "</select></td>";
}

?>