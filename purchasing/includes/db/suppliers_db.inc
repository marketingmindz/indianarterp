<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_supplier($supp_no,$supp_name, $supp_ref, $address, $supp_address,$tin_no,$cin_no,$pan_no,$can_no,$st_no,
	 $credit_limit,$curr_code, $payment_terms, $tax_included)  
{
	$sql = "INSERT INTO ".TB_PREF."suppliers (supp_no,supp_name, supp_ref, address, supp_address, tin_no, cin_no,
		pan_no, can_no,st_no,credit_limit, curr_code,payment_terms,tax_included)
		VALUES (".db_escape($supp_no). ", "
		.db_escape($supp_name). ", "
		.db_escape($supp_ref). ", "
		.db_escape($address) . ", "
		.db_escape($supp_address) . ", "
		.db_escape($tin_no). ", "
		.db_escape($cin_no). ", "
		.db_escape($pan_no). ", "
		.db_escape($can_no). ", "
		.db_escape($st_no). ", "
		.db_escape($credit_limit). ", "
		.db_escape($curr_code). ", "
		.db_escape($payment_terms). ", "
		.db_escape($tax_included). ")";

	db_query($sql,"The supplier could not be added");
}

function update_supplier($supplier_id,$supp_no,$supp_name, $supp_ref, $address, $supp_address, $tin_no, 
	$cin_no, $pan_no, $can_no,$st_no, $credit_limit,$curr_code, $payment_terms,$tax_included)
{
	$sql = "UPDATE ".TB_PREF."suppliers SET supp_no=".db_escape($supp_no) . ",
		supp_name=".db_escape($supp_name) . ",
		supp_ref=".db_escape($supp_ref) . ",
		address=".db_escape($address) . ",
		supp_address=".db_escape($supp_address) . ",
		tin_no=".db_escape($tin_no) . ",
		cin_no=".db_escape($cin_no) . ",
		pan_no=".db_escape($pan_no) . ",
		can_no=".db_escape($can_no) . ",
		st_no=".db_escape($st_no) . ",
		credit_limit=".$credit_limit . ",
		curr_code=".db_escape($curr_code).",
		payment_terms=".db_escape($payment_terms) . ",
		tax_included=".db_escape($tax_included)
		." WHERE supplier_id = ".db_escape($supplier_id);

	db_query($sql,"The supplier could not be updated");
}

function delete_supplier($supplier_id)
{
	$sql="DELETE FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);
	db_query($sql,"check failed");
}

function get_supplier_details($supplier_id, $to=null, $all=true)
{

	if ($to == null)
		$todate = date("Y-m-d");
	else
		$todate = date2sql($to);
	$past1 = get_company_pref('past_due_days');
	$past2 = 2 * $past1;
	// removed - supp_trans.alloc from all summations

	if ($all)
    	$value = "(trans.ov_amount + trans.ov_gst + trans.ov_discount)";
    else	
    	$value = "IF (trans.type=".ST_SUPPINVOICE." OR trans.type=".ST_BANKDEPOSIT.",
    		(trans.ov_amount + trans.ov_gst + trans.ov_discount - trans.alloc),
    		(trans.ov_amount + trans.ov_gst + trans.ov_discount + trans.alloc))";
	$due = "IF (trans.type=".ST_SUPPINVOICE." OR trans.type=".ST_SUPPCREDIT.",trans.due_date,trans.tran_date)";
    $sql = "SELECT supp.supp_name, supp.curr_code, ".TB_PREF."payment_terms.terms,

		Sum(IFNULL($value,0)) AS Balance,

		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= 0,$value,0)) AS Due,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past1,$value,0)) AS Overdue1,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past2,$value,0)) AS Overdue2,
		supp.credit_limit - Sum(IFNULL(IF(trans.type=".ST_SUPPCREDIT.", -1, 1) 
			* (ov_amount + ov_gst + ov_discount),0)) as cur_credit

		FROM ".TB_PREF."suppliers supp
			 LEFT JOIN ".TB_PREF."supp_trans trans ON supp.supplier_id = trans.supplier_id AND trans.tran_date <= '$todate',
			 ".TB_PREF."payment_terms

		WHERE
			 supp.payment_terms = ".TB_PREF."payment_terms.terms_indicator
			 AND supp.supplier_id = $supplier_id ";
	if (!$all)
		$sql .= "AND ABS(trans.ov_amount + trans.ov_gst + trans.ov_discount) - trans.alloc > ".FLOAT_COMP_DELTA." ";  
	$sql .= "GROUP BY
			  supp.supp_name,
			  ".TB_PREF."payment_terms.terms,
			  ".TB_PREF."payment_terms.days_before_due,
			  ".TB_PREF."payment_terms.day_in_following_month";

    $result = db_query($sql,"The customer details could not be retrieved");
    $supp = db_fetch($result);

    return $supp;
}

function get_supplier($supplier_id)
{
	$sql = "SELECT * FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	return db_fetch($result);
}

function get_supplier_name($supplier_id)
{
	$sql = "SELECT supp_name AS name FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_supplier_accounts($supplier_id)
{
	$sql = "SELECT payable_account,purchase_account,payment_discount_account FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	return db_fetch($result);
}

function get_supplier_contacts($supplier_id, $action=null)
{
	$results = array();
	$res = get_crm_persons('supplier', $action, $supplier_id);
	while($contact = db_fetch($res))
		$results[] = $contact;

	return $results;
}

function get_current_supp_credit($supplier_id)
{
	$suppdet = get_supplier_details($supplier_id);
	return $suppdet['cur_credit'];

}

function is_new_supplier($id)
{
	$tables = array('supp_trans', 'grn_batch', 'purch_orders', 'purch_data');

	return !key_in_foreign_table($id, $tables, 'supplier_id');
}

/*- Bajrang L. Bidasara - 18 feb 2016*/
function is_exist($supplier_id,$catagory, $sub_category)
{
	$sql = "select * from ".TB_PREF."supp_product where supplier_id = ".db_escape($supplier_id)." AND catagory = ".db_escape($catagory)." AND sub_category = ".db_escape($sub_category);
	$result = db_query($sql, "Could not check supplier's product");
	if(db_num_rows($result) > 0)
		return true;
	else
		return false;
}

function add_product($supplier_id,$catagory, $sub_category, $purchase_price, $lead_time)
{
	$is_exist = is_exist($supplier_id,$catagory, $sub_category);
	if(!$is_exist)
	{
		$sql = "INSERT INTO ".TB_PREF."supp_product(supplier_id,catagory, sub_category, purchase_price,lead_time)
			VALUES ("  . db_escape($supplier_id) . ", " . db_escape($catagory) . ", " .db_escape($sub_category) . ", " . db_escape($purchase_price) . ", " . db_escape($lead_time). ")";		
	        
	   	db_query($sql,"could not add new product");
	}
	else
		display_error("Product aleady exist for this supplier.");		  
}

//--------------------------------------------------------------------------------------------------

function update_product($id, $purchase_price, $lead_time)
{
	$sql = "UPDATE ".TB_PREF."supp_product SET purchase_price=".db_escape($purchase_price)
		.",lead_time=".db_escape($lead_time)
		."WHERE product_id=".db_escape($id);
	db_query($sql, "could not update product");
}
//--------------------------------------------------------------------------------------------------

function delete_product($id)
{
	 $sql = "DELETE FROM ".TB_PREF."supp_product WHERE product_id=".db_escape($id);
	 db_query($sql, "could not delete product");
}
//--------------------------------------------------------------------------------------------------
function get_all_product_list($supp_id) {
  $sql = "SELECT * FROM ".TB_PREF."supp_product where supplier_id = ".db_escape($supp_id);
	//if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get product");
}

function get_product($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."supp_product WHERE product_id=".db_escape($selected_id);
	$result = db_query($sql,"product could not be retrieved");
	return db_fetch($result);
}
//------------ get supplier id (code by chetan)--------------
function get_product_supplier_id($supp_id){
	
	$sql="SELECT * FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supp_id);
	$result = db_query($sql,"supplier could not be retrieved");
	return db_fetch($result);
}

//------------ get consuable category ( code by chetan )----------

function get_product_cons_category($cons_id){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_product_cons_subcategory($sub_cons_id){
	$sql = "SELECT consumable_name, consumable_code, i.company_name FROM ".TB_PREF."consumable_master c 
       LEFT JOIN ".TB_PREF."item_company i on c.company_id = i.company_id WHERE consumable_id=".db_escape($sub_cons_id);
	$result = db_query($sql);
	return db_fetch($result);
}



// ----------------------- get supplier no. ( code by bajrang ) -------------------
function get_supplier_id()
{
	$supp_no = '';
	$sql = "SELECT supp_no FROM ".TB_PREF."suppliers ORDER BY supp_no DESC LIMIT 1";
	
	$query1 = db_query($sql,"cannot get purchase request no.");
	
	while($row = db_fetch($query1,MYSQL_NUM))
	   {
		   $supp_no= $row['supp_no'];
		   $supp_no++;
		   
		}
	if($supp_no == '' && $supp_no == NULL)
	{
		$supp_no = 1;
	}
	return $supp_no;	
}


//supplier-quotation module - code by bajrang -------
function get_cons_category($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql, "Could not get consumable category");
	return db_fetch($result);
}
function get_cons_name($cons_select){
	$sql = "SELECT c.consumable_name, c.consumable_code, i.company_name FROM ".TB_PREF."consumable_master c
			Left Join ".TB_PREF."item_company i on i.company_id = c.company_id
			 WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql, "Could bot get consumable name");
	return db_fetch($result);
}


function new_quotation(&$quote,&$consumables)
{
	$quote_no = $quote['quote_no'];
	$sql = "select quote_no from ".TB_PREF."supplier_quotation where quote_no = ".db_escape($quote['quote_no']);
	$result = db_query($sql, "Could not check Quotation No.");
	if(db_num_rows($result) > 0)
	{
		display_error("Something went wrong. This Quotation number may be already entered. Please check in Quotation List.");
		return false;
	}
	else
	{
		$date = date2sql($quote['date']);
		$del_date = date2sql($quote['del_date']);
		$sql = "INSERT INTO ".TB_PREF."supplier_quotation(quote_no,supplier_id,date,del_date,remark, unique_name, filename, filesize, filetype) VALUES(";
		$sql .= db_escape($quote['quote_no']) . "," .
		db_escape($quote['supplier_id']) . "," .
		db_escape($date) . "," .
		db_escape($del_date) . "," .
		db_escape($quote['remark']) . "," .
		db_escape($quote['unique_name']) . "," .
		db_escape($quote['filename']) . "," .
		db_escape($quote['filesize']) . "," .
		db_escape($quote['filetype']).")";
		db_query($sql, "Quotation is not entered.");
		$quote_id = db_insert_id();
		foreach ($consumables->line_con as $line_no => $lines_on_con)
	 	{		
			$sql = "INSERT INTO ".TB_PREF."supplier_quotation_item(quote_id, consumable_id, consumable_category,unit, rate) VALUES (";
			$sql .= db_escape($quote_id) . ", " . 
			db_escape($lines_on_con->cons_type).",".
			db_escape($lines_on_con->cons_select) . "," . 		
			db_escape($lines_on_con->cons_unit) . ", " .
			db_escape($lines_on_con->cons_rate). ")";
			db_query($sql, " Quotation items details are not inserted in database");
		}
		return true;		 
	}
}

function update_supplier_quotation($quote_id,&$quote,&$consumables)
{
   	$date = date2sql($quote['date']);
	$del_date = date2sql($quote['del_date']);

    $sql = "UPDATE ".TB_PREF."supplier_quotation SET 
    quote_no = ". db_escape($quote['quote_no']).",
    supplier_id = ". db_escape($quote['supplier_id']).",
    date = ". db_escape($date).",
    del_date = ". db_escape($del_date).",
    unique_name = ". db_escape($quote['unique_name']).",
    filename = ". db_escape($quote['filename']).",
    filesize = ". db_escape($quote['filesize']).",
    filetype = ". db_escape($quote['filetype']).",
	remark = ". db_escape($quote['remark']).",
    date = ". db_escape($date);
  
	$sql .= " WHERE quote_id = ".$quote_id;
	db_query($sql, "Supplier Quotation is not updated");
  
	$sql = "DELETE FROM ".TB_PREF."supplier_quotation_item WHERE quote_id=".db_escape($quote_id);
    db_query($sql, "could not delete old item details");
	foreach ($consumables->line_con as $line_no => $lines_on_con)
	{		
		$sql = "INSERT INTO ".TB_PREF."supplier_quotation_item(quote_id, consumable_id, consumable_category,unit, rate) VALUES (";
		$sql .= $quote_id . ", " . 
		db_escape($lines_on_con->cons_type).",".
		db_escape($lines_on_con->cons_select) . "," . 		
		db_escape($lines_on_con->cons_unit) . ", " .
		db_escape($lines_on_con->cons_rate). ")";
		db_query($sql, " supplier items could not be updated");
	 }
	 return $pr_no;
}

function get_sql_for_supplier_quotation($supplier_id=ALL_TEXT)
{

	$sql = "SELECT quote_id, quote_no,date,del_date FROM ".TB_PREF."supplier_quotation WHERE ";
	if (isset($_POST['quotation_no']) && $_POST['quotation_no'] != "")
	{
		$sql .= " quote_no LIKE ".db_escape('%'. $_POST['quotation_no'] . '%')."  AND supplier_id = ".db_escape($_POST['supplier_id'])." ORDER BY del_date ASC";
	}
	else
	{

		$date_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " del_date >= '$date_after'";
		$sql .= " AND del_date <= '$date_before'";
		 
		$sql .= " AND supplier_id = ".db_escape($_POST['supplier_id'])." ORDER BY del_date ASC";
	}
	return $sql;
}	

function read_quotation($quote_id)
{
	$sql = "select * from ".TB_PREF."supplier_quotation where quote_id = ".db_escape($quote_id);
	$result = db_query($sql, "Could not read Supplier Quotation.");
	$row = db_fetch($result);
	$_POST['quote_no'] = $row['quote_no'];
	$_POST['date'] = sql2date($row['date']);
	$_POST['del_date'] = sql2date($row['del_date']);
	$_POST['remark'] = $row['remark'];
}

function read_quotation_items($quote_id, &$cons)
{
	#get consumable_part id from pr_id
   	$sql = "SELECT * FROM ".TB_PREF."supplier_quotation_item WHERE quote_id=".db_escape($quote_id); 
	$result = db_query($sql, "Items cannot be retrived");
	
	if (db_num_rows($result) > 0)
	{
		while ($myrow1 = db_fetch($result))
		{			
			if ($cons->add_to_consuable_part($cons->lines_on_con, $myrow1["consumable_id"], $myrow1["consumable_category"], $myrow1["unit"], $myrow1["rate"])) {
					$newline = &$cons->line_con[$cons->lines_on_con-1];
					$newline->con_detail_rec = $myrow1["cons_line_details"];	
			}
		} 
	}  
}


function get_supplier_quotation($quote_id)
{
	$sql = "select * from ".TB_PREF."supplier_quotation where quote_id = ".db_escape($quote_id);
	$result = db_query($sql, "Could not read Supplier Quotation.");
	return $row = db_fetch($result);
}


function insert_purchase_request_items($pr_no, $consumable_id, $consumable_category, $request_quantity, $unit, $date)
{
	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
	$sql .= $pr_no . ", " . 
	db_escape($consumable_id).",".
	db_escape($consumable_category) . "," . 		
	db_escape($unit) . ", " .
	db_escape($request_quantity). ")";
	db_query($sql, " purchase request items are not inserted in database can not be made");

}

function make_pr_for_po($quote_id_for_po)
{
	$date = date2sql(Today());
	
	$pr_no = '';
	$sql = "SELECT pr_no FROM ".TB_PREF."purchase_request ORDER BY pr_no DESC LIMIT 1";
	$query1 = db_query($sql,"cannot get purchase request no.");
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $pr_no= $row['pr_no'];
	}
	$pr_no += 1;
	if($pr_no == '' && $pr_no == NULL)
	{
		$pr_no = 1;
	}
	$sql = "INSERT INTO ".TB_PREF."purchase_request(pr_no,location_id,work_center_id,type,date,memo_description) VALUES(";
		$sql .= db_escape($pr_no) . "," .
		db_escape($header_adjustment['location_id']) . "," .
		db_escape($header_adjustment['work_center_id']) . "," ."'1',".
		db_escape($date).",'This Purchase Request is Generated Using ReOrder Level.')";
	db_query($sql, "Purchase request is not created");

	$sql = "select * from ".TB_PREF."supplier_quotation_item where quote_id = ".db_escape($quote_id_for_po);
	$result = db_query($sql, "Could not get Quotation Items.");
	while($row = db_fetch($result))
	{
		$request_quantity = 1;
		insert_purchase_request_items($pr_no, $row['consumable_id'], $row['consumable_category'], $request_quantity,$row['unit'], $date);
	}

}


function get_quotation_attachment($quotation_id)
{
	$sql = "SELECT * FROM ".TB_PREF."supplier_quotation WHERE quote_id=".db_escape($quotation_id);
	$result = db_query($sql, "Could not retrieve attachments");
	return db_fetch($result);
}



?>