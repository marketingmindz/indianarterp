<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


//-------Bajrang L. Bidasara - display_products_without_supplier()-----------------
function display_products_without_supplier()
{
	div_start();
	$purchase_request_id = $_POST['pr_no'];
	$result = get_item_details($purchase_request_id);
	$cons = Array();
	while($myrow = db_fetch($result))
	{
		$cons[] = $myrow;
	}
	$count = $_POST['count'];
	start_table(TABLESTYLE2, "width=80%");
	$th = array(_("Consumable Category"), _("Consumable Name"));
	table_header($th);
	for($i=0; $i<=$count; $i++)
	{
		if($_POST['supp_id_'.$i] == -1) 
		{
			$items = $cons;
			start_row();
			foreach ($items as $value) {
				if($_POST['consumable_category_'.$i] == $value['consumable_category'])
				{
					$brand = $value['consumable_name'] == "" ? "No Brand" : $value['consumable_name'];
					$consumable_category = $value['consumable_name']." - ".$brand." - ".$value['consumable_code'];
					label_cell($value['master_name']);
					label_cell($consumable_category);
					break;
				}
			}
			end_row();
			
		}
	}
	end_table();
	div_end();
}


function display_purchase_order_header(&$order)
{
	global 	$Ajax;

	$Ajax->activate('cons_table');
	div_start('cons_table');
	start_outer_table(TABLESTYLE2, 'width=80%');

	table_section(1);
    if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_request_details($purchase_request_id);
	}
	text_row(_("PR No:"), 'pr_no', null, 30, 30, '','','','id="pr_no" readonly="true"');
	//date_row(_("Date:"), 'po_date', null, 30, 30, '','','','id="po_date" readonly="true"');
	date_row(_("Date") . ":", 'po_date');

	table_section(2);
	hidden("location_id", $_POST['location_id']);
	text_row(_("Location Name:"), 'location_id_0', null, 30, 30, '','','','id="location_id" readonly="true"');
	
	hidden("work_center_id", $_POST['work_center_id']);
	text_row(_("Work Center Name:"), 'work_center_id_0', null, 30, 30, '','','','id="work_center_id" readonly="true"');
	end_outer_table(); // outer table
}

//---------------------------------------------------------------------------------------------------
function approved_qty($i, $app_qty)
{
	echo "<td><input type='text' value='".$app_qty."' name='app_qty_".$i."' id='appqty_".$i."' class='appqtyclass' ></td>";
}
function select_supplier_list($consumable_id, $consumable_category, $i, $n=null)
{
	if($n == null)
	{
		$n = $i;
	}

	$default_supplier = "";
	$sql_check = "select default_supplier from ".TB_PREF."default_consumable_supplier where consumable_cat = ".db_escape($consumable_id)." && consumable_name = ".db_escape($consumable_category);
	$check_result = db_query($sql_check, "Could not check Default supplier.");
	if(db_num_rows($check_result) > 0)
	{
		$row2 = db_fetch($check_result);		
		if($row2['default_supplier'] != '0' && $row2['default_supplier'] != '-1')
		{
			$default_supplier = $row2['default_supplier'];
		}					
	}

	$result = get_suppliers($consumable_id, $consumable_category);
	$supplier = "";
	echo "<td><select required class='supplier' style = 'font-size:15px; font-weight: bolder;' id='supp_id_".$n."' name='supp_id_".$n."'><option value='-1'>Select Supplier</option>";
	while($myrow = db_fetch($result))
	{
		$supplier_id = $myrow['supplier_id'];
		$purchase_price = ($supplier_id == "23")?0:$myrow['purchase_price'];

		$supplier = "". $myrow['supp_name']." - Price:-".$purchase_price." - LeadTime:-".$myrow['lead_time'];
		$selected = "";
		
		if($supplier_id == $default_supplier)
		{
			$selected = "selected=selected";
		}
		
		if($_POST['supp_id_'.$i] == $supplier_id)
		{
			$selected = "selected=selected";
		}
	    echo "<option value='".$supplier_id."' ".$selected."><h1 style='margin-right:20px;'>".$supplier."</h1></option>";
	}
	echo "</select></td>";
}


function display_supp_details_header()
{
	global 	$Ajax;
	$Ajax->activate('items');
	div_start("items");
	if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_item_details($purchase_request_id);

		start_table(TABLESTYLE2, 'width=95%');
		$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Requested Qty"),  _("Supplier - Price -  Time"), _("Approved Qty"));
		table_header($th);
		$k = 0;
		$i=1;
		while($myrow = db_fetch($result)){ 
			$consumable_id = $myrow["consumable_id"];
			$consumable_category = $myrow["consumable_category"];
			$_POST['consumable_id_'.$i] = $_POST['cons_id'] = $myrow["master_name"];
			$brand = $myrow['company_name'] == "" ? "No Brand" : $myrow['company_name'];
			$_POST['consumable_cat'] = $myrow["consumable_name"]." - ".$brand." - ".$myrow['consumable_code'];
			
			$_POST['unit_'.$i] = $_POST['unit'] = $myrow["unit"];
			$_POST['requested_quantity_'.$i] = $_POST['requested_qty'] = $myrow["quantity"];
			if(!isset($_POST['app_qty_'.$i]))
			{
				$_POST['app_qty_'.$i] = $myrow["quantity"];
			}		
			alt_table_row_color($k);
			
			    hidden("consumable_id_".$i, $consumable_id);
				//text_cells_ex(null, 'cons_id', 30, 30, null,'','','', '','id="consumable_id", readonly="true"');
				label_cell($_POST['cons_id']);
				
				hidden("consumable_category_".$i, $consumable_category);
				//text_cells_ex(null, 'consumable_cat', 50, 50,null,'','','', '','id="consumable_category", readonly="true"');
				label_cell($_POST['consumable_cat']);
				
				hidden("unit_".$i, $_POST['unit']);
				//text_cells_ex(null, 'unit_'.$i, 10, 10,null,'','','', '','id="cons_unit" readonly="true"');
				label_cell($_POST['unit_'.$i]);

				hidden("requested_quantity_".$i, $_POST['requested_quantity_'.$i]);
				label_cell($_POST['requested_quantity_'.$i]);
				select_supplier_list($consumable_id, $consumable_category, $i);
				approved_qty($i, $_POST['app_qty_'.$i]);

				/*echo '<td align="center"><button type="submit" class="ajaxsubmit" name="Delete'.$i.'" value="1" ><img src="../themes/exclusive/images/delete.gif" height="16" border="0"></button>
					</td>';*/

			end_row();			
			
			$i++;
		}
		hidden("count", $i-1);
		end_table(); // outer table
	}
	else
	{
		start_table(TABLESTYLE2, 'width=95%');
		$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Requested Qty"),  _("Supplier - Price -  Time"), _("Approved Qty"));
		table_header($th);
		$k = 0;
		$i = 0;
		$n = 1;
		$count = $_POST['count'];
		for($i=0; $i<=$count; $i++)
		{
			if(isset($_POST["consumable_id_".$i]))
			{	
				$consumable_id = $_POST["consumable_id_".$i];
				$consumable_category = $_POST["consumable_category_".$i];
				$master = get_cons_type_name($consumable_id);
				$cat = get_cons_select_name($consumable_category);
				$brand = $cat['company_name'] == "" ? "No Brand" : $cat['company_name'];
				$_POST['consumable_name'] = $cat["consumable_name"]." - ".$brand." - ".$cat['consumable_code'];

				alt_table_row_color($k);
				
			    hidden("consumable_id_".$n, $consumable_id);
				//text_cells_ex(null, $master['master_name'], 30, 30, $master['master_name'],'','','', '','id="consumable_id", readonly="true"');
				label_cell($master['master_name']);
				
				hidden("consumable_category_".$n, $consumable_category);
				//text_cells_ex(null, "consumable_name", 50, 50,null,'','','', '','id="consumable_category", readonly="true"');
				label_cell($_POST['consumable_name']);
				
				hidden("unit_".$n, $_POST['unit_'.$i]);
				//text_cells_ex(null, 'unit_', 10, 10,$_POST["unit_".$i],'','','', '','id="cons_unit" readonly="true"');
				label_cell($_POST["unit_".$i]);


				$_POST['requested_qty_'.$i] =  $_POST['requested_quantity_'.$i];
				hidden("requested_quantity_".$n, $_POST['requested_quantity_'.$i]);
				label_cell($_POST['requested_quantity_'.$i]);
				
				select_supplier_list($consumable_id, $consumable_category, $i, $n);
				approved_qty($n, $_POST['app_qty_'.$i]);
/*
				echo '<td align="center"><button type="submit" class="ajaxsubmit" name="Delete'.$i.'" value="1" ><img src="../themes/exclusive/images/delete.gif" height="16" border="0"></button></td>';*/

				end_row();
				$n++;						
			}

		}
		hidden("count", $n-1);
		end_table();
	}
	div_end();
}


function display_apr_summary(&$pr, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "style=min-width:320px;width:50%;");
    start_row();
    label_cells(_("PR No"), $pr->pr_no, "class='tableheader2'");
    label_cells(_("Date"), $pr->pr_date, "class='tableheader2'");
    end_row();
    start_row();
	label_cells(_("Location"), $pr->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $pr->work_center_id, "class='tableheader2'");
	end_row();

    end_table(1);
}

?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>