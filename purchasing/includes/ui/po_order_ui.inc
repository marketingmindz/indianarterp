<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


//---------------------------------------------------------------------------------------------------

function display_purchase_order_header(&$order)
{
	global 	$Ajax;

  $Ajax->activate('cons_table');
div_start('cons_table');
	start_outer_table(TABLESTYLE2, 'width=80%');

	table_section(1);
    if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_request_details($purchase_request_id);
	}
	text_row(_("PR No:"), 'pr_no', null, 30, 30, '','','','id="pr_no" readonly="true"');
	//date_row(_("Date:"), 'po_date', null, 30, 30, '','','','id="po_date" readonly="true"');
	date_row(_("Date") . ":", 'po_date');


	table_section(2);
	hidden("location_id", $_POST['location_id']);
	text_row(_("Location Name:"), 'location_id_0', null, 30, 30, '','','','id="location_id" readonly="true"');
	
	hidden("work_center_id", $_POST['work_center_id']);
	text_row(_("Work Center Name:"), 'work_center_id_0', null, 30, 30, '','','','id="work_center_id" readonly="true"');


	end_outer_table(); // outer table
}

//---------------------------------------------------------------------------------------------------
function approved_qty($i){
	echo "<td><input type='text' value='' name='app_qty_".$i."' id='appqty_".$i."' class='appqtyclass' ></td>";
}
function select_supplier_list($consumable_id, $consumable_category, $i, $supplier_id){
	$sql = "SELECT s.supplier_id, s.supp_name,purchase_price,lead_time FROM ".TB_PREF."supp_product pr
	       Left Join ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id
	       WHERE catagory =".db_escape($consumable_id)." && sub_category = ".db_escape($consumable_category)." && s.supplier_id = ".db_escape($supplier_id)
		   ." group by s.supplier_id order by purchase_price ASC";
	$result = db_query($sql, "The suppliers details cannot be retrieved");
	$supplier = "";
	echo "<td><input type='hidden' name='supp_id_".$i."' value='".$supplier_id."' >";
	while($myrow = db_fetch($result)){
		$supplier_id = $myrow['supplier_id'];
		//echo $supplier_id;
		$supplier = "". $myrow['supp_name'];
		//- Price:-".$myrow['purchase_price']." - LeadTime:-".$myrow['lead_time'];
	    echo "<h4>".$supplier."</h4>";
	}


}

function display_supp_details_header()
{
	if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_item_details($purchase_request_id);
	}

	start_table(TABLESTYLE2, 'width=95%');
	$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Approved Qty"), _("Supplier"));
	table_header($th);
	hidden("apr_id", $_GET['RequestID']);	

	$k = 0;
	$i=0;
	while($myrow = db_fetch($result)){
		$consumable_id = $myrow["consumable_id"];
		$consumable_category = $myrow["consumable_category"];
		$_POST['consumable_id_'.$i] = $_POST['cons_id'] = $myrow["master_name"];
		
		$brand = $myrow['company_name'] == "" ? "No Brand" : $myrow['company_name'];
		$_POST['consumable_cat'] = $myrow["consumable_name"]." - ".$brand." - ".$myrow['consumable_code'];
			
		$_POST['unit_'.$i] = $_POST['unit'] = $myrow["unit"];
		$_POST['approved_quantity_'.$i] = $_POST['approved_quantity'] = $myrow["approved_quantity"];
		$_POST['supplier_id'.$i] =  $myrow["supplier_id"];
		
		alt_table_row_color($k);
				
		    hidden("consumable_id_".$i, $consumable_id);
			text_cells_ex(null, 'cons_id', 25, 30, null,'','','', '','id="consumable_id", readonly="true"');
			
			hidden("consumable_category_".$i, $consumable_category);
			text_cells_ex(null, 'consumable_cat', 60, null,null,'','','', '','id="consumable_category", readonly="true"');
			
			hidden("unit_".$i, $_POST['unit']);
			text_cells_ex(null, 'unit_'.$i, 10, 10,null,'','','', '','id="cons_unit" readonly="true"');
			
			hidden("approved_quantity_".$i, $_POST['approved_quantity_'.$i]);
			text_cells_ex(null, 'approved_quantity', 10, 10,null,'','','', '','id=reqqty_'.$i, 'readonly="true"');
			
			select_supplier_list($consumable_id, $consumable_category, $i,$_POST['supplier_id'.$i] );
			//approved_qty($i);
							
		end_row();
		
		hidden("count", $i);
		$i++;
	}
	
	end_table(); // outer table
}

//---------------------------------------------------------------------------------------------------

function display_purchase_order_summary(&$po, $is_self=false, $editable=false)
{
	$myrow = get_company_pref();

    start_table("", "class='outer'");
    start_row();

    echo "<td>";
    	start_table("", "class='inner'");
	    start_row();echo "<td class=tableheader colspan=2 style='text-align:left;'>";echo $myrow['coy_name'];
	    echo "</td>";end_row();

	    start_row();
	    $logo = company_path() . "/images/" .$myrow['coy_logo'];
	    label_cell("<img src='".$logo."' />");
	    end_row();

	    start_row();
	    	label_cell("");
	    end_row();
	    start_row();
	    	label_cell("");
	    end_row();
	    /*start_row();
	    label_cell($myrow['postal_address'], "colspan=2");
		//label_cells(_("Address"), $myrow['postal_address'], "class='tableheader2' style='min-width:120px;'");
		end_row();
		start_row();
		label_cell("TIN NO. : ", "style=''");		
		end_row();*/

		start_row();echo "<td class=tableheader colspan=2 style='text-align:left;'>";echo "Supplier";
	    echo "</td>";end_row();
	    start_row();
	    	label_cell($po->supp_name, "colspan=2");
	    end_row();
	    start_row();
	    	label_cell($po->supp_address, "colspan=2");
	    end_row();
	    start_row();
			label_cell("TIN NO. : ".$po->tin_no, "");		
		end_row();
		start_row();
	    	label_cell("Contact Name : ".$po->contactName, "colspan=3");
	    end_row();
	    start_row();
	    	label_cell("Phone : ".$po->phone, "colspan=3");
	    end_row();

		end_table();
	echo "</td>";

	echo "<td>";
    	start_table("", "class='inner'");
    	start_row();
	    	label_cell("PURCHASE ORDER", "class='tableheader' colspan=3");
	    end_row();
	    start_row();
	    	label_cell();
		    label_cell("Date");
		    label_cell($po->po_date, "");
	    end_row();
	    start_row();
	    	label_cell();
		    label_cell("Purchase Order No.");
		    label_cell($_GET['trans_no'], "");	    
	    end_row();
	    start_row();
	    	label_cell();
		    label_cell("Purchase Request No.");
		    label_cell($po->po_no, "");
	    end_row();

	    start_row();
	    	label_cell("", "class='tableheader'");
		    label_cell("Ship to : ", "class='tableheader' style='text-align:left'");
		    label_cell($myrow['coy_name'], "class='tableheader' style='text-align:left'");
	    end_row();
	    start_row();
   			label_cell();
		    label_cell("Location");
		    label_cell($po->location_id, "");	    
	    end_row();
	    start_row();
	    	label_cell();
		    label_cell("Work Center");
		    label_cell($po->work_center_id, "colspan=2 ");
	    end_row();
	    start_row();
	    label_cell();
	    label_cell("Address");
	    label_cell($myrow['postal_address'], "");
		//label_cells(_("Address"), $myrow['postal_address'], "class='tableheader2' style='min-width:120px;'");
		end_row();
		start_row();
		label_cell();
		label_cell("TIN NO. : ", "style=''");
		label_cell($myrow['tin_no'], "");		
		end_row();

	   /* start_row();echo "<td class=tableheader  style='text-align:left;' colspan=3>";echo "Supplier Contact";
	    echo "</td>";end_row();
	    start_row();
	    label_cell("Name : ".$po->contactName, "colspan=3");
	    end_row();
	    start_row();
	    label_cell("Phone : ".$po->phone, "colspan=3");
	    end_row();*/

		end_table();
	echo "</td>";
	end_row();


    //end_table(1);
}



?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>