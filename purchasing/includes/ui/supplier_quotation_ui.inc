<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_header(&$order){
	global $selected_id;
	echo "<br>";
	div_start('details');
		/** detail **/
		start_outer_table(TABLESTYLE2, "style=min-width:320px;width:100%;");
		if(get_post('quotation_id'))
		{
			hidden("quotation_id", $_POST['quotation_id']);
		}
		table_section(1);
		
		start_row();
			date_cells(_("Date"), 'date', $_POST['date'], true);
			date_cells(_("Delivery Date"), 'del_date', $_POST['del_date'], true);
		end_row();
		start_row();		
			text_cells(_("Quote No.:"), 'quote_no', $_POST['quote_no'], 22, 20, '','','','id="quote_no"');
		end_row();
		file_row(_("Attached File") . ":", 'filename', 'filename');
		end_outer_table(1);
		div_end();
		
}


//  consumable part //

function display_consumable_summary(&$order, $editable=true)
{
    div_start('cons_table');
	display_heading("Consumable Part");
	start_table(TABLESTYLE, "colspan=7 style=min-width:320px;width:100%;");
	$th = array(_("Type"),_("Select"), _("Unit"), _("Rate"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);

	$id = find_submit('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{

    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_cons_category($cons_type);
			$const_type = $get_cons_type['master_name'];
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_cons_name($cons_select);
			
			$const_select = $get_cons_select['consumable_name'];
			$brand = $get_cons_select['company_name'] == "" ? "No Brand" : $get_cons_select['company_name'];
			$consumable_name = $const_select." - ".$brand." - ".$get_cons_select['consumable_code'];
        	label_cell($const_type);
    		label_cell($consumable_name);
    		label_cell($con_line->cons_unit);
			label_cell($con_line->cons_rate);
    		
    	hidden('session_var', '1');
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_consumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_consumable_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;

	end_table(1);
	div_start('memo');
      start_table(TABLESTYLE2, "style=min-width:320px;width:60%;");
		textarea_row(_('Remark:'), 'remark', $_POST['remark'], 50, 3);
		hidden('session_var', '1');  //  code by bajrang   17/07/2015
		end_table(1);
    div_end();
	div_end();
	
}

function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_rate'] = $order->line_con[$id]->cons_rate;

	    $Ajax->activate('cons_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	
	if($_POST['cons_select'] == '-1')
	{
		unit_list_cells(null, 'cons_unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['cons_select'], 'cons_unit', null, false, true, true, true);
	}
	text_cells_ex(null, 'cons_rate', 10, 10);
		if ($id != -1)
		{
			button_cell('ConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('ConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
		} 
		else 
			submit_cells('ConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}

function display_filters()
{
	start_table();
	start_row();
	ref_cells(_("Quote No.#:"), 'quotation_no', '',null, '', true);
	date_cells(_("Delivery Date from:"), 'OrdersAfterDate', '', null, -60);
	date_cells(_("to:"), 'OrdersToDate');
	submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
	end_row();
    end_table(1);
}


function display_pr_summary(&$pr, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "style=min-width:320px;width:60%;");
    start_row();
    label_cells(_("PR No"), $pr->pr_no, "class='tableheader2'");	
	label_cells(_("Date"), $pr->pr_date, "class='tableheader2'");
	end_row();

	start_row();
	label_cells(_("Location"), $pr->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $pr->work_center_id, "class='tableheader2'");
	end_row();

    end_table(1);
}





?>
