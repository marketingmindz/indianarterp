<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_hearder(&$order){
	global $selected_id;
	global $Ajax;
	$Ajax->activate('details');
	div_start('details');
		/** detail **/
		start_outer_table(TABLESTYLE2);
		table_section(1);
			if($selected_id == -1){
				text_row(_("Purchase Order No:"), 'po_no', null, 22, 20, '','','','id="po_no"');
			}else{
				text_row(_("Purchase Order No:"), 'po_no', null, 22, 20);
			}
			submit_cells('GetDetails', _("Get Details"), "colspan=2 style='text-align: center'",
				_('Get Purchase Order Details'), true);
		end_outer_table(1);
		
		start_outer_table(TABLESTYLE2);
			table_section(1);
				hidden("supplier_id", $_POST['supplier_id']);
	            text_row(_("Supplier:"), 'supp_name', null, 30, 30, '','','','id="supp_name" readonly="true"');
	            date_row(_("Order Date:"), 'po_date', null, 30, 30, '','','','id="po_date" readonly="true"');
	            date_row(_("Receiving Date:"), 'receiving_date', null, 30, 30, '','','','id="receiving_date" readonly="true"');

			table_section(2);
				hidden("location_id", $_POST['location_id']);
				text_row(_("Location Name:"), 'location_id_0', null, 30, 30, '','','','id="location_id" readonly="true"');
				
				hidden("work_center_id", $_POST['work_center_id']);
				text_row(_("Work Center Name:"), 'work_center_id_0', null, 30, 30, '','','','id="work_center_id" readonly="true"');
	            date_row(_("Returning Date:"), 'returning_date', null, 30, 30, '','','','id="returning_date"');
			
		end_outer_table(1);
		div_end();
		
}


//  consumable part //

function display_consumable_summary(&$order, $editable=true)
{
    if(isset($_POST['po_no']))
	{
		$purchase_order_id = $_POST['po_no'];
		$result = get_purchase_order_item_details($purchase_order_id);
	}

	start_table(TABLESTYLE2, 'width=60%');
	$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Ordered Qty"), _("Received Qty"), _("OK Quantity"), _("Rejected Qty"), _("Chalan No"), _("Invoice No"));
	table_header($th);
	$k = 0;
	$i=0;
	while($myrow = db_fetch($result)){
		$consumable_id = $myrow["consumable_id"];
		$consumable_category = $myrow["consumable_category"];
		$_POST['consumable_id_'.$i] = $_POST['cons_id'] = $myrow["master_name"];
		$_POST['consumable_category_'.$i] = $_POST['consumable_cat'] = $myrow["consumable_name"];
		$_POST['unit_'.$i] = $_POST['unit'] = $myrow["unit"];
		$_POST['ordered_quantity_'.$i] = $_POST['ordered_quantity_'] = $myrow["approved_quantity"];
		
		alt_table_row_color($k);
		
		    hidden("consumable_id_".$i, $consumable_id);
			text_cells_ex(null, 'cons_id', 30, 30, null,'','','', '','id="consumable_id" readonly="true"');
			
			hidden("consumable_category_".$i, $consumable_category);
			text_cells_ex(null, 'consumable_cat', 30, 30,null,'','','', '','id="consumable_category" readonly="true"');
			
			hidden("unit_".$i, $_POST['unit']);
			text_cells_ex(null, 'unit_'.$i, 10, 10,null,'','','', '','id="cons_unit" readonly="true"');
			
			hidden("ordered_quantity_".$i, $_POST['ordered_quantity_'.$i]);
			text_cells_ex(null, 'ordered_quantity_', 10, 10,null,'','','', '','id="ordered_quantity_'.$i.'"  readonly="true"');
			
			//select_supplier_list($consumable_id, $consumable_category, $i);
			text_cells_ex(null, 'received_quantity_'.$i, 10, 10,null,'','','', '','id="received_quantity_'.$i.'" class="received_quantity"');
			text_cells_ex(null, 'ok_quantity_'.$i, 10, 10,null,'','','', '','id="ok_quantity_'.$i.'" class="ok_quantity"');
			text_cells_ex(null, 'rejected_quantity_'.$i, 10, 10,null,'','','', '','id="rejected_quantity_'.$i.'"');
			text_cells_ex(null, 'chalan_no_'.$i, 10, 10,null,'','','', '','id="chalan_no_'.$i.'"');
			text_cells_ex(null, 'invoice_no_'.$i, 10, 10,null,'','','', '','id="invoice_no_'.$i.'"');

		end_row();
		
		hidden("count", $i);
		$i++;
	}
	
	end_table(); // outer table
	
}




function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_quntity'] = $order->line_con[$id]->cons_quntity;
		

	    $Ajax->activate('cons_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	
	if($_POST['cons_select'] == '-1')
	{
		unit_list_cells(null, 'cons_unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['cons_select'], 'cons_unit', null, false, true, true, true);
	}
	text_cells_ex(null, 'cons_quntity', 5, 5);
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
		if ($id != -1)
		{
			button_cell('ConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('ConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
			//set_focus('amount');
		} 
		else 
			submit_cells('ConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}

function display_pr_summary(&$pr, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "width=60%");
    start_row();
    label_cells(_("PR No"), $pr->pr_no, "class='tableheader2'");
	label_cells(_("Location"), $pr->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $pr->work_center_id, "class='tableheader2'");
	label_cells(_("Date"), $pr->pr_date, "class='tableheader2'");
	end_row();

    end_table(1);
}





?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>
