<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/*
	Security sections groups various areas on both functionality and privilege levels.
	Often analytic inquires are available only for management, and configuration
	for administration or management staff. This is why we have those three
	section type inside near every FA module.

	Section codes 0-99 are reserved for core FA functionalities.
	Every security section can contain up to 256 different areas.
	External modules can extend security roles system by adding rows to 
	$security_sections and $security_areas using section codes >=100.
	Security areas and sections created by extension modules/plugins
	have dynamically assigned 3-byte integer codes. The highest byte is zero
	for sections/areas defined in this file, and extid+1 for those defined 
	by extensions 
*/
define('SS_DESIGN',	1<<8);	
define('SS_SETUP',	2<<8);	// company level setup
define('SS_SADMIN', 3<<8);
define('SS_PURCHASE', 4<<8);
define('SS_STORE',  11<<8); 
define('SS_ACTION',	12<<8);
define('SS_REPORT',	13<<8);	

define('SS_PURCH_C',21<<8);
define('SS_PURCH',	22<<8);
define('SS_PURCH_A',23<<8);

define('SS_ITEMS_C',31<<8);
define('SS_ITEMS',	32<<8);
define('SS_ITEMS_A',33<<8);

define('SS_MANUF_C',41<<8);
define('SS_MANUF',	42<<8);
define('SS_MANUF_A',43<<8);

define('SS_DIM_C',	51<<8);
define('SS_DIM',	52<<8);
define('SS_DIM_A',	53<<8);

define('SS_GL_C',	61<<8);
define('SS_GL',		62<<8);
define('SS_GL_A',	63<<8);

define('SS_COST_C', 64<<8);
define('SS_STORE', 65<<8);
define('SS_CONS', 66<<8);
define('SS_PACKAGING', 67<<8);

/*$security_sections = array(
 SS_SADMIN => _("System administration"),
 SS_SETUP => _("Company setup"),
 SS_SPEC => _("Special maintenance"),
 SS_SALES_C => _("Sales configuration"),
 SS_SALES => _("Sales transactions"),
 SS_SALES_A => _("Sales related reports"),
 SS_PURCH_C => _("Purchase configuration"),
 SS_PURCH => _("Purchase transactions"),
 SS_PURCH_A => _("Purchase analytics"),
 SS_COST_C => _("Costing"),
 SS_STORE => _("Store"),
 SS_CONS => _("Consumables"),
 SS_PACKAGING => _("Packaging"),
 SS_ITEMS_C => _("Inventory configuration"),
 SS_ITEMS => _("Inventory operations"),
 SS_ITEMS_A => _("Inventory analytics"),
 SS_MANUF_C => _("Manufacturing configuration"),
 SS_MANUF => _("Manufacturing transactions"),
 SS_MANUF_A => _("Manufacturing analytics"),
 SS_DIM_C => _("Dimensions configuration"),
 SS_DIM => _("Dimensions"),
 SS_GL_C => _("Banking & GL configuration"),
 SS_GL => _("Banking & GL transactions"),
 SS_GL_A => _("Banking & GL analytics")
);*/

$security_sections = array(
 SS_DESIGN => _("Design & Development - Coding, Masters, Lists and Packaging"),
 SS_SADMIN => _("Admin - Sales and Costing"),
 SS_PURCHASE => _("Purchases - PR, PR Approval, PO and Consumbales"),
 SS_STORE => _("Store - Inventory and Inventory Adjustments"),
 SS_ACTION => _("Accounts - Suppliers"),
 SS_SETUP => _("Setup - Company Setup"),
 SS_REPORT => _("Report Builder Module")
 
);


/*
	This table stores security areas available in FA. 
	Key is area identifier used to check user rights, values are
	codes stored for each role in security_roles table and description used
	in roles editor.

	Set of allowed access areas codes is retrieved during user login from
	security_roles table, and cached in user profile.

	Special value 'SA_OPEN' is used for publicly available pages like login/logout.
*/
$security_areas =array(

//	Site administration


	//	Company setup
	'SA_SETUPCOMPANY' => array(SS_SETUP|1, _("Company parameters")),
	'SA_SECROLES' => array(SS_SETUP|2, _("Access levels edition")),
	'SA_USERS' => array(SS_SETUP|3, _("Users setup")),
	'SA_CREATECOMPANY' =>array(SS_SETUP|4, _("Install/update companies")),
	'SA_CRM' => array(SS_SETUP|5, _("CRM Database Setup")),
	'SA_SETUPDISPLAY' => array(SS_SETUP|6, _("Display preferences")),
	'SA_CRM_SYNCING' => array(SS_SETUP|7, _("CRM Syncing")),
	
	/*'SA_CREATELANGUAGE' => array(SS_SADMIN|2, _("Install/update languages")),
	'SA_CREATEMODULES' => array(SS_SADMIN|3, _("Install/upgrade modules")),
	'SA_SOFTWAREUPGRADE' => array(SS_SADMIN|4, _("Software upgrades")),*/

	
	/*'SA_POSSETUP' => array(SS_SETUP|4, _("Point of sales definitions")),
	'SA_PRINTERS' => array(SS_SETUP|5, _("Printers configuration")),
	'SA_PRINTPROFILE' => array(SS_SETUP|6, _("Print profiles")),
	'SA_PAYTERMS' => array(SS_SETUP|7, _("Payment terms")),
	'SA_SHIPPING' => array(SS_SETUP|8, _("Shipping ways")),
	'SA_CRSTATUS' => array(SS_SETUP|9, _("Credit status definitions changes")),
	'SA_INVENTORYLOCATION' => array(SS_SETUP|10, _("Inventory locations changes")),
	'SA_INVENTORYMOVETYPE'  => array(SS_SETUP|11, _("Inventory movement types")),
	'SA_WORKCENTRES' => array(SS_SETUP|12, _("Manufacture work centres")),
	'SA_FORMSETUP' => array(SS_SETUP|13, _("Forms setup")),
	'SA_CRMCATEGORY' => array(SS_SETUP|14, _("Contact categories")),*/
//
// Special and common functions
    
	/*'SA_VOIDTRANSACTION' => array(SS_SPEC|2, _("Voiding transactions")),
	'SA_BACKUP' => array(SS_SPEC|3, _("Database backup/restore")),
	'SA_VIEWPRINTTRANSACTION' => array(SS_SPEC|4, _("Common view/print transactions interface")),
	'SA_ATTACHDOCUMENT' => array(SS_SPEC|5, _("Attaching documents")),
	 //???
	'SA_CHGPASSWD' => array(SS_SPEC|7, _("Password changes")), //???
*/

// Sales related functionality

	'SA_SALESTRANSVIEW' => array(SS_SADMIN|1,  _("Sales Order and Quotation Inquiry")),
	'SA_SALESQUOTE' => array(SS_SADMIN|2, _("Sales Quotations Entry")),
	'SA_EXPORTSALESQUOTE' => array(SS_SADMIN|3, _("Export Sales Quotations")),


	'SA_CUSTOMER' => array(SS_SADMIN|4,  _("Customers/Buyers")),
	'SA_CUSTOMERGNRLTAB' => array(SS_SADMIN|5,  _("Buyers - General Setting Tab")),
	'SA_CUSTOMERCNCTTAB' => array(SS_SADMIN|6,  _("Buyers - Contact Tab")),
	'SA_CUSTOMERTRSNTAB' => array(SS_SADMIN|7,  _("Buyers - Transactions Tab")),
	'SA_CUSTOMERSORDTAB' => array(SS_SADMIN|8,  _("Buyers - Sales Order Tab")),
	'SA_CUSTOMERPRODTAB' => array(SS_SADMIN|9,  _("Buyers - Customer's Product Tab")),
	'SA_CUSTOMERCONSTAB' => array(SS_SADMIN|10,  _("Buyers - Consumables Tab")),
	'SA_CUSTOMERPACKTAB' => array(SS_SADMIN|11,  _("Buyers - Packaging Tab")),
	'SA_CUSTOMERCOSTTAB' => array(SS_SADMIN|12,  _("Buyers - Costing Tab")),
	'SA_CUSTOMERQUOTTAB' => array(SS_SADMIN|13,  _("Buyers - Quotation Tab")),

	'SA_SALESORDER' => array(SS_SADMIN|14, _("Sales Order Entry")),
	'SA_SALESORDERINQUIRY' => array(SS_SADMIN|15, _("Sales Order Inquiry")),
	'SA_BPCINQUIRY' => array(SS_SADMIN|16, _("BPC Inquiry")),

	
	

// Costing
	
	'SA_COSTING' => array(SS_SADMIN|20, _("Costing")),
	'SA_MFRGCOSTING' => array(SS_SADMIN|21, _("Manufacturing Costing")),
	'SA_FINISHCOSTING' => array(SS_SADMIN|22, _("Finish Costing")),
	'SA_FINALCOSTING' => array(SS_SADMIN|23, _("Final Costing")),
	'SA_SAVED_COSTING' => array(SS_SADMIN|24, _("Saved Costing")),
	'SA_FREEZE_COSTING' => array(SS_SADMIN|25, _("Freeze Costing")),
	'SA_LOCKED_COSTING' => array(SS_SADMIN|26, _("Locked Costing")),
	'SA_EDIT_RANGE_COST' => array(SS_SADMIN|27, _("Edit Range Cost")),
	'SA_PRICE_LIST' => array(SS_SADMIN|28, _("Price List")),
	'SA_CURRENCY_MASTER' => array(SS_SADMIN|29, _("Currency Master")),
	'SA_CONSUMABLE_COSTING' => array(SS_SADMIN|30, _("Consumable Costing")),
	'SA_SAVED_CONSUMABLE_COSTING' => array(SS_SADMIN|31	, _("Saved Consumable Costing")),
	'SA_CONSUMABLE_COSTING_FOR_PO' => array(SS_SADMIN|32	, _("Consumable Costing for PO")),
	/*'SA_SALESTYPES' => array(SS_SALES_C|1, _("Sales types")),
	'SA_SALESPRICE' => array(SS_SALES_C|2, _("Sales prices edition")),
	'SA_SALESMAN' => array(SS_SALES_C|3, _("Sales staff maintenance")),
	'SA_SALESAREA' => array(SS_SALES_C|4, _("Sales areas maintenance")),
	'SA_SALESGROUP' => array(SS_SALES_C|5, _("Sales groups changes")),
	'SA_STEMPLATE' => array(SS_SALES_C|6, _("Sales templates")),
	'SA_SRECURRENT' => array(SS_SALES_C|7, _("Recurrent invoices definitions")),*/

	
	/*'SA_SALESORDER' => array(SS_SALES|3, _("Sales orders edition")),
	'SA_SALESDELIVERY' => array(SS_SALES|4, _("Sales deliveries edition")),
	'SA_SALESINVOICE' => array(SS_SALES|5, _("Sales invoices edition")),
	'SA_SALESCREDITINV' => array(SS_SALES|6, _("Sales credit notes against invoice")),
	'SA_SALESCREDIT' => array(SS_SALES|7, _("Sales freehand credit notes")),
	'SA_SALESPAYMNT' => array(SS_SALES|8, _("Customer payments entry")),
	'SA_SALESALLOC' => array(SS_SALES|9, _("Customer payments allocation")),

	'SA_SALESANALYTIC' => array(SS_SALES_A|1, _("Sales analytical reports")),
	'SA_SALESBULKREP' => array(SS_SALES_A|2, _("Sales document bulk reports")),
	'SA_PRICEREP' => array(SS_SALES_A|3, _("Sales prices listing")),
	'SA_SALESMANREP' => array(SS_SALES_A|4, _("Sales staff listing")),
	'SA_CUSTBULKREP' => array(SS_SALES_A|5, _("Customer bulk listing")),
	'SA_CUSTSTATREP' => array(SS_SALES_A|6, _("Customer status report")),
	'SA_CUSTPAYMREP' => array(SS_SALES_A|7, _("Customer payments report")),*/

//
// Purchase related functions
//
	
	'SA_SUPPLIER' => array(SS_ACTION|1, _("Suppliers Module - General Setting Tab")),
	'SA_SUPPCONTACTS' => array(SS_ACTION|2, _("Suppliers Contacts Tab")),
	'SA_SUPPTRANSVIEW' => array(SS_ACTION|3, _("Suppliers Transactions Tab")),
	'SA_SUPPPURCHORDER' => array(SS_ACTION|4, _("Suppliers Purchase Order Tab")),
	'SA_SUPPPRODUCT' => array(SS_ACTION|5, _("Suppliers Products Tab")),
	'SA_SUPPPRICE' => array(SS_ACTION|6, _("Suppliers Products Price Tab")),
	'SA_SUPPLIER_ADD' => array(SS_ACTION|7, _("Add Suppliers")),


	/*'SA_PURCHASEORDER' => array(SS_PURCHASE|3, _("Purchase order entry")),*/
	'SA_PURCHASEREQUEST' => array(SS_PURCHASE|2, _("Purchase Request entry")),
	'SA_PUCHORDER' => array(SS_PURCHASE|3, _("Purchase Order Entry")),	
	'SA_PURCHASEREQUESTUSINGREORDER' => array(SS_PURCHASE|4, _("Purchase Request Entry Using Reoder Level")),
	'SA_PURCHASEREQUESTINQUIRY' => array(SS_PURCHASE|5, _("Purchase Request Inquiry - For Store")),
	'SA_PURCHASEREQUESTAPPROVAL' => array(SS_PURCHASE|6, _("Purchase Request Approval")),
	'SA_APPROVEDPURCHASEREQUESTS' => array(SS_PURCHASE|7, _("Approved Purchase Requests")),
	'SA_PURCHASEORDERINQUIRY' => array(SS_PURCHASE|8, _("Purchase Order Inquiry")),	
	'SA_PURCHASEORDERMAINTENANCE' => array(SS_PURCHASE|9, _("Purchase Order Maintenance")),
	//'SA_PUCHORDER' => array(SS_PURCHASE|10, _("Manual Purchase Order Entry")),

// Consumable management
	'SA_CONSUMABLE' => array(SS_PURCHASE|11, _("Consumable Management")),
	'SA_BRAND' => array(SS_PURCHASE|12, _("Make Brand")),
	'SA_PRINCIPAL' => array(SS_PURCHASE|13, _("Principal Master")),
	'SA_OPEN_STOCK' => array(SS_PURCHASE|14, _("Opening Stock Master")),

	/*'SA_PURCHASEPRICING' => array(SS_PURCH_C|1, _("Purchase price changes")),*/
	/*'SA_SUPPTRANSVIEW' => array(SS_PURCH|1, _("Supplier transactions view")),*/
	/*'SA_GRN' => array(SS_PURCH|4, _("Purchase receive")),
	'SA_SUPPLIERINVOICE' => array(SS_PURCH|5, _("Supplier invoices")),
	'SA_GRNDELETE' => array(SS_PURCH|9, _("Deleting GRN items during invoice entry")),
	'SA_SUPPLIERCREDIT' => array(SS_PURCH|6, _("Supplier credit notes")),
	'SA_SUPPLIERPAYMNT' => array(SS_PURCH|7, _("Supplier payments")),
	'SA_SUPPLIERALLOC' => array(SS_PURCH|8, _("Supplier payments allocations")),

	'SA_SUPPLIERANALYTIC' => array(SS_PURCH_A|1, _("Supplier analytical reports")),
	'SA_SUPPBULKREP' => array(SS_PURCH_A|2, _("Supplier document bulk reports")),
	'SA_SUPPPAYMREP' => array(SS_PURCH_A|3, _("Supplier payments report")),*/

//
// Inventory 
//

	//coding
	'SA_DESIGN' => array(SS_DESIGN|1, _("Design code")),
	'SA_FINISH_PRODUCT' => array(SS_DESIGN|2, _("Finish Product")),
	'PHOTO_BANK' => array(SS_DESIGN|3, _("Photo Bank")),
	'SA_FAB_COMBINATION' => array(SS_DESIGN|4, _("Fabric Combination")),
	
	//Lists
	'SA_SEARCH_DESIGN' => array(SS_DESIGN|5, _("Design Code List")),
	'SA_FINISH_DETAILS' => array(SS_DESIGN|6, _("Finish Product List")),

	//Master
	'SA_MASTER' => array(SS_DESIGN|7, _("Master Creator")),	
	'SA_COMPANY_MASTER' => array(SS_DESIGN|8, _("Company Master")),
	'SA_RANGE' => array(SS_DESIGN|9, _("Range Master")),	
	'SA_PRODUCT_TYPE' => array(SS_DESIGN|10, _("Product Type Master")),
	'SA_CATEGORY' => array(SS_DESIGN|11, _("Category Master")),	
	'SA_LEG' => array(SS_DESIGN|12, _("Leg Master")),
	'SA_COLOR' => array(SS_DESIGN|13, _("Color Master")),
	'SA_FINISH' => array(SS_DESIGN|14, _("Finish Master")),
	'SA_WOOD' => array(SS_DESIGN|15, _("Wood Master")),
	'SA_PART' => array(SS_DESIGN|16, _("Part Master")),
	'SA_UOM' => array(SS_DESIGN|17, _("Units of measure")),
	'SA_FABRIC' => array(SS_DESIGN|18, _("Fabric Master")),
	'SA_LABEL' => array(SS_DESIGN|19, _("Label Master")),
	'SA_CARTON' => array(SS_DESIGN|20, _("Carton Master")),

	
	'SA_SKIN' => array(SS_DESIGN|21, _("Skin Master")),
	'SA_ITEM' => array(SS_DESIGN|24, _("Stock items add/edit")),	
	'SA_SALESKIT' => array(SS_DESIGN|25, _("Sales kits")),
	'SA_ITEMCATEGORY' => array(SS_DESIGN|26, _("Item categories")),	
	'SA_ELECTRICAL' => array(SS_DESIGN|28, _("Electrical")),
	
	
	//packaging
	'SA_PACKAGING' => array(SS_DESIGN|30, _("Packaging Module")),
	'SA_PACKING_DETAILS' => array(SS_DESIGN|31, _("Packaging List")),
	
	//Store
	'SA_PRODUCTION' => array(SS_STORE|1, _("Production Team")),
	'SA_WORKCENTER' => array(SS_STORE|2, _("Work Center")),
	'SA_STOCKMASTER' => array(SS_STORE|3, _("Opening Stock Master")),
	'SA_OPENINGSTOCKLIST' => array(SS_STORE|4, _("Opening Stock List")),
	'SA_LOCATIONMASTER' => array(SS_STORE|5, _("Location Master")),
	'SA_ITEMADJUSTMENT' => array(SS_STORE|6, _("Item Adjustment Note")),
	'SA_INVENTORYLOCATIONTRANSFER' => array(SS_STORE|7, _("Inventory Location Transfer")),
	'SA_INVENTORYITEMSTATUS' => array(SS_STORE|8, _("Inventory Item Status")),
	'SA_LOCATIONTRANSFER' => array(SS_STORE|9, _("Inventory location transfers")),
	'SA_INVENTORYADJUSTMENT' => array(SS_STORE|10, _("Inventory adjustments")),
	'SA_REORDER_LEVEL_INQUIRY' => array(SS_STORE|11, _("Reorder Level Inquiry")),
	'SA_STOCKTRANSACTION' => array(SS_STORE|12, _("Inventory Stock Transactions")),
	'SA_REORDER' => array(SS_STORE|13, _("Reorder levels")),


	//report builder module
	'SA_NEW_REPORT' => array(SS_REPORT|1, _("Add New Report")),
	'SA_SAVED_REPORT' => array(SS_REPORT|2, _("View Saved Reports")),
	'SA_VIEW_REPORT' => array(SS_REPORT|3, _("View Report")),
	'SA_PRINT_FINISH_PRODUCT' => array(SS_REPORT|4, _("Print Finsh Product"))
	
	/*'SA_ITEMSSTATVIEW' => array(SS_ITEMS|1, _("Stock status view")),
	'SA_ITEMSTRANSVIEW' => array(SS_ITEMS|2, _("Stock transactions view")),
	'SA_FORITEMCODE' => array(SS_ITEMS|3, _("Foreign item codes entry")),*/
	/*'SA_ITEMSANALYTIC' => array(SS_ITEMS_A|2, _("Items analytical reports and inquiries")),
	'SA_ITEMSVALREP' => array(SS_ITEMS_A|3, _("Inventory valuation report")),*/

//
// Manufacturing module 
//
	/*'SA_BOM' => array(SS_MANUF_C|1, _("Bill of Materials")),
	'SA_MANUFTRANSVIEW' => array(SS_MANUF|1, _("Manufacturing operations view")),
	'SA_WORKORDERENTRY' => array(SS_MANUF|2, _("Work order entry")),
	'SA_MANUFISSUE' => array(SS_MANUF|3, _("Material issues entry")),
	'SA_MANUFRECEIVE' => array(SS_MANUF|4, _("Final product receive")),
	'SA_MANUFRELEASE' => array(SS_MANUF|5, _("Work order releases")),
	'SA_WORKORDERANALYTIC' => array(SS_MANUF_A|1, _("Work order analytical reports and inquiries")),
	'SA_WORKORDERCOST' => array(SS_MANUF_A|2, _("Manufacturing cost inquiry")),
	'SA_MANUFBULKREP' => array(SS_MANUF_A|3, _("Work order bulk reports")),
	'SA_BOMREP' => array(SS_MANUF_A|4, _("Bill of materials reports")),*/
//
// Dimensions
//
	/*'SA_DIMTAGS' => array(SS_DIM_C|1, _("Dimension tags")),
	'SA_DIMTRANSVIEW' => array(SS_DIM|1, _("Dimension view")),
	'SA_DIMENSION' => array(SS_DIM|2, _("Dimension entry")),
	'SA_DIMENSIONREP' => array(SS_DIM|3, _("Dimension reports")),*/
//
// Banking and General Ledger
//
	/*'SA_ITEMTAXTYPE' => array(SS_GL_C|1, _("Item tax type definitions")),
	'SA_GLACCOUNT' => array(SS_GL_C|2, _("GL accounts edition")),
	'SA_GLACCOUNTGROUP' => array(SS_GL_C|3, _("GL account groups")),
	'SA_GLACCOUNTCLASS' => array(SS_GL_C|4, _("GL account classes")),
	'SA_QUICKENTRY' => array(SS_GL_C|5, _("Quick GL entry definitions")),
	'SA_CURRENCY' => array(SS_GL_C|6, _("Currencies")),
	'SA_BANKACCOUNT' => array(SS_GL_C|7, _("Bank accounts")),
	'SA_TAXRATES' => array(SS_GL_C|8, _("Tax rates")),
	'SA_TAXGROUPS' => array(SS_GL_C|12, _("Tax groups")),
	'SA_FISCALYEARS' => array(SS_GL_C|9, _("Fiscal years maintenance")),
	'SA_GLSETUP' => array(SS_GL_C|10, _("Company GL setup")),
	'SA_GLACCOUNTTAGS' => array(SS_GL_C|11, _("GL Account tags")),
	'SA_MULTIFISCALYEARS' => array(SS_GL_C|13, _("Allow entry on non closed Fiscal years")),

	'SA_BANKTRANSVIEW' => array(SS_GL|1, _("Bank transactions view")),
	'SA_GLTRANSVIEW' => array(SS_GL|2, _("GL postings view")),
	'SA_EXCHANGERATE' => array(SS_GL|3, _("Exchange rate table changes")),
	'SA_PAYMENT' => array(SS_GL|4, _("Bank payments")),
	'SA_DEPOSIT' => array(SS_GL|5, _("Bank deposits")),
	'SA_BANKTRANSFER' => array(SS_GL|6, _("Bank account transfers")),
	'SA_RECONCILE' => array(SS_GL|7, _("Bank reconciliation")),
	'SA_JOURNALENTRY' => array(SS_GL|8, _("Manual journal entries")),
	'SA_BANKJOURNAL' => array(SS_GL|11, _("Journal entries to bank related accounts")),
	'SA_BUDGETENTRY' => array(SS_GL|9, _("Budget edition")),
	'SA_STANDARDCOST' => array(SS_GL|10, _("Item standard costs")),
	'SA_ACCRUALS' => array(SS_GL|12, _("Revenue / Cost Accruals")),

	'SA_GLANALYTIC' => array(SS_GL_A|1, _("GL analytical reports and inquiries")),
	'SA_TAXREP' => array(SS_GL_A|2, _("Tax reports and inquiries")),
	'SA_BANKREP' => array(SS_GL_A|3, _("Bank reports and inquiries")),
	'SA_GLREP' => array(SS_GL_A|4, _("GL reports and inquiries")),*/
);



//-----------------------------------sub areas-----------------------------------------------------


$security_subareas =array(

// Sales related functionality


	// Sales Quotation Inquiry
	SS_SADMIN|1 => array(array(SS_SADMIN|330,  _("View"), "class"=>"viewLinkButton"),
						  array(SS_SADMIN|331,  _("Edit"), "class"=>"editLinkButton"),
						  array(SS_SADMIN|332,  _("Make Sales Order"), "class"=>"makeOrderLinkButton"),
						  array(SS_SADMIN|333,  _("Print"), "class"=>"printLinkButton")),
	// Purchase Request Inquiry - Store
	SS_PURCHASE|5 => array(array(SS_PURCHASE|30,  _("View"), "class"=>"viewLinkButton"),
						  array(SS_PURCHASE|31,  _("Edit"), "class"=>"editLinkButton"),
						  array(SS_PURCHASE|33,  _("Submit for Approval"), "class"=>"submit_for_approval")),
	//Purchase Request Inquiry - Purchasing
	SS_PURCHASE|6 => array(array(SS_PURCHASE|40,  _("View"), "class"=>"viewLinkButton"),
						  array(SS_PURCHASE|41,  _("Edit"), "class"=>"editLinkButton"),
						  array(SS_PURCHASE|42,  _("Approve Request"), "class"=>"approve_request")),
	// Approved Purchase Request
	SS_PURCHASE|7 => array(array(SS_PURCHASE|70,  _("View"), "class"=>"viewLinkButton"),
						  array(SS_PURCHASE|71,  _("Make Order"), "class"=>"makeOrderButton")),
	// Purchase Order Maintainance
	SS_PURCHASE|9 => array(array(SS_PURCHASE|90,  _("View Order"), "class"=>"viewLinkButton"),
						  array(SS_PURCHASE|91,  _("Receive Items"), "class"=>"recieveOrderButton")),

	// Supplier - General Setting Tab
	SS_ACTION|1 => array(array(SS_ACTION|30,  _("Add Supplier"), "id"=>"submit"),						
						array(SS_ACTION|31,  _("Update Supplier"), "id"=>"submit"),
						array(SS_ACTION|32,  _("Delete Suppliers"), "id"=>"delete")),
	// Supplier - Contacts Tab
	SS_ACTION|2 => array(array(SS_ACTION|40,  _("Add Contact"), "id"=>"contactsNEW"),						
						array(SS_ACTION|41,  _("Edit Contact"), "class"=>"editbutton"),
						array(SS_ACTION|42,  _("Delete Contact"), "class"=>"editbutton")),

	/*SS_ACTION|3 => array(array(SS_ACTION|40,  _("Add Contact"), "id"=>"contactsNEW"),						
						array(SS_ACTION|41,  _("Edit Contact"), "class"=>"editbutton"),
						array(SS_ACTION|42,  _("Delete Contact"), "class"=>"editbutton")),*/

	SS_ACTION|4 => array(array(SS_ACTION|60,  _("View Purchase Order"), "id"=>"contactsNEW")),

	SS_ACTION|5 => array(array(SS_ACTION|70,  _("Add Product"), "id"=>"ADD_ITEM"),
						array(SS_ACTION|71,  _("Delete Product"), "class"=>"editbutton")),

	SS_ACTION|6 => array(array(SS_ACTION|80,  _("Edit Product Price"), "class"=>"editbutton"),
						array(SS_ACTION|81,  _("Delete Product Price"), "class"=>"editbutton")),



	//buyers - module
	SS_SADMIN|5 => array(array(SS_SADMIN|50,  _("Add New Customer"), "id"=>"submit"),
						array(SS_SADMIN|51,  _("Update Customer"), "id"=>"submit"),
						array(SS_SADMIN|52,  _("Delete Customer"), "id"=>"delete")),

	SS_SADMIN|6 => array(array(SS_SADMIN|60,  _("Add New Customer contact"), "id"=>"contactsNEW"),
						array(SS_SADMIN|61,  _("Update Customer Contact"), "class"=>"editbutton"),
						array(SS_SADMIN|62,  _("Delete Customer Contact"), "class"=>"editbutton")),

	SS_SADMIN|9 => array(array(SS_SADMIN|90,  _("View Product Details"), "class"=>"view_all")),

	SS_SADMIN|12 => array(array(SS_SADMIN|120,  _("Edit Customer Costing"), "id"=>"edit-cost")),

	SS_SADMIN|13 => array(array(SS_SADMIN|130,  _("View Quotation"), "class"=>"viewLinkButton"),
						array(SS_SADMIN|131,  _("Make Sales Order"), "class"=>"makeOrderLinkButton"),
						array(SS_SADMIN|132,  _("Print Quotation"), "class"=>"printLinkButton")),
	//saved costing
	SS_SADMIN|24 => array(array(SS_SADMIN|240,  _("Freeze Costing"), "class"=>"freeze_costing"),
						array(SS_SADMIN|241,  _("Open Costing"), "class"=>"openCostingButton"),
						array(SS_SADMIN|242,  _("Cancel Costing"), "class"=>"cancel_costing")),

	//freeze costing
	SS_SADMIN|25 => array(array(SS_SADMIN|250,  _("View Freezed Costing"), "class"=>"viewLinkButton"),
						array(SS_SADMIN|251,  _("Edit Freezed Costing"), "class"=>"editLinkButton")),
	//locked costing
	SS_SADMIN|26 => array(array(SS_SADMIN|260,  _("View Locked Costing"), "class"=>"viewLinkButton"),
						array(SS_SADMIN|261,  _("Create New Version"), "class"=>"create_version"),
						array(SS_SADMIN|262,  _("Edit Costing Version"), "class"=>"editLinkButton")),
	//currency Master
	SS_SADMIN|29 => array(array(SS_SADMIN|290,  _("Add New Currency"), "id"=>"ADD_ITEM"),
						array(SS_SADMIN|291,  _("Edit Currency"), "class"=>"editbutton"),
						array(SS_SADMIN|292,  _("Delete Currency"), "class"=>"editbutton")),
	//saved consumable costing
	SS_SADMIN|31 => array(array(SS_SADMIN|310,  _("Open Consumable Costing"), "class"=>"openCostingButton"),
						array(SS_SADMIN|311,  _("Edit Consumable Costing"), "class"=>"edit_costing"),
						array(SS_SADMIN|312,  _("Cancel Consumable Costing"), "class"=>"cancel_costing")),

);

/*
	This function should be called whenever we want to extend core access level system
	with new security areas and/or sections i.e.: 
	. on any page with non-standard security areas
	. in security roles editor
	The call should be placed between session.inc inclusion and page() call.
	Up to 155 security sections and 155 security areas for any extension can be installed.
*/
function add_access_extensions()
{
	global $security_areas, $security_sections, $installed_extensions;

	foreach($installed_extensions as $extid => $ext) {
		$accext = hook_invoke($ext['package'], 'install_access', $dummy);
		if ($accext == null) continue;

		$scode = 100;
		$acode = 100;
		$extsections = $accext[1];
		$extareas = $accext[0];
		$extcode = $extid<<16;
		
		$trans = array();
		foreach($extsections as $code =>$name) {
			$trans[$code] = $scode<<8;
			// reassign section codes
			$security_sections[$trans[$code]|$extcode] = $name;
			$scode++;
		}
		foreach($extareas as $code => $area) {
			$section = $area[0]&0xff00;
			// extension modules:
			// if area belongs to nonstandard section
			// use translated section codes and
			// preserve lower part of area code
			if (isset($trans[$section])) {
				$section = $trans[$section];
			} 
				// otherwise assign next available
				// area code >99
			$area[0] = $extcode | $section | ($acode++);
			$security_areas[$code] = $area;
		}
	}
}
/*
	Helper function to retrieve extension access definitions in isolated environment.
*/
/*
function get_access_extensions($id) {
	global $path_to_root, $installed_extensions;
	
	$ext = $installed_extensions[$id];
	
	$security_sections = $security_areas = array();
	
	if (isset($ext['acc_file']))
		include_once($path_to_root.'/'.$ext['path'].'/'.$ext['acc_file']);

	return array($security_areas, $security_sections);
}
*/
?>