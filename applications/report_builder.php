<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class report_builder_app extends application 
{
	function report_builder_app() 
	{
		$this->application("report_builder", _($this->help_context = "&Report Builder"));

		$this->add_module(_("Reports & Analysis"));

		$this->add_lapp_function(0, _("Add New &Report"), 
			"reporting/add_new_report.php", 'SA_NEW_REPORT', MENU_REPORT);

		$this->add_lapp_function(0, _("View Saved &Reports"), 
			"reporting/view_saved_report.php", 'SA_VIEW_REPORT', MENU_REPORT);

		$this->add_lapp_function(0, _("Print Finish Product Sheet"), 
			"reporting/print_finish_product_sheet.php", 'SA_PRINT_FINISH_PRODUCT', MENU_REPORT);

		$this->add_extensions();
	}
}


?>