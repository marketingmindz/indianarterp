<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class suppliers_app extends application 
{
	function suppliers_app() 
	{
		$this->application("AP", _($this->help_context = "&Purchases"));

		//------------- PR Section -----------------
		$this->add_module(_("PR"));
		
		$this->add_lapp_function(0, _("Purchase &Request Entry"),
			"purchasing/purchase_request_entry.php", 'SA_PURCHASEREQUEST', MENU_TRANSACTION);

		$this->add_lapp_function(0, _("Purchase &Request Entry Using Reorder"),
			"purchasing/purchase_request_entry_using_reorder.php", 'SA_PURCHASEREQUESTUSINGREORDER', MENU_TRANSACTION);

		$this->add_lapp_function(0, _("Purchase &Request Inquiry"),
			"purchasing/inquiry/store_pr_inquiry.php?location=store&erp", 'SA_PURCHASEREQUESTINQUIRY', MENU_TRANSACTION);
		
		//--------------- PR Approval Section -------------------

		$this->add_module(_("PR Approval"));

		$this->add_lapp_function(1, _("Purchase &Request Approval"),
			"purchasing/inquiry/purchase_request_inquiry.php?", 'SA_PURCHASEREQUESTAPPROVAL', MENU_TRANSACTION);

		$this->add_lapp_function(1, _("Approved Purchase &Requests "),
			"purchasing/inquiry/approved_purchase_requests.php?", 'SA_APPROVEDPURCHASEREQUESTS', MENU_TRANSACTION);

		//------------------- PO Section ------------------
		$this->add_module(_("PO"));

		$this->add_lapp_function(2, _("Manual Purchase &Order Entry"),
			"purchasing/manual_po.php", 'SA_PURCHASEORDERMAINTENANCE', MENU_TRANSACTION);

		$this->add_lapp_function(2, _("Purchase Orders &Inquiry"),
			"purchasing/inquiry/purchase_order_inquiry.php?", 'SA_PURCHASEORDERINQUIRY', MENU_TRANSACTION);

		$this->add_lapp_function(2, _("Purchase &Order Maintenance"),
			"purchasing/purchase_order_maintenance.php", 'SA_PURCHASEORDERMAINTENANCE', MENU_TRANSACTION);

		//------------------- Consumable Section ------------------
		$this->add_module(_("Consumable"));


		
			
		$sql="SELECT * FROM 0_master_creation";
		$result = mysql_query($sql);
		while ($myrow = mysql_fetch_array($result)) 
		{
			$master_name = $myrow['master_name'];
			$this->add_lapp_function(3, _($master_name),
				"inventory/manage/master_category.php?id=".$master_name, 'SA_CONSUMABLE', MENU_MAINTENANCE);
		}

		$this->add_rapp_function(3, _("Consumable Management"),
			"inventory/manage/consumable_management.php?", 'SA_CONSUMABLE', MENU_MAINTENANCE);

		$this->add_rapp_function(3, _("Make Brand"),
			"inventory/manage/item_company.php?", 'SA_BRAND', MENU_MAINTENANCE);

		$this->add_rapp_function(3, _("Principal Master"),
			"inventory/manage/principal_master.php?", 'SA_PRINCIPAL', MENU_MAINTENANCE);

		$this->add_rapp_function(3, _("Opening Stock Master"),
			"inventory/manage/open_stock.php?", 'SA_OPEN_STOCK', MENU_MAINTENANCE);







		/*$this->add_lapp_function(0, _("Purchase &Request Approval"),
			"purchasing/inquiry/purchase_request_inquiry.php?", 'SA_PURCHASEREQUESTINQUIRY', MENU_TRANSACTION);

		$this->add_lapp_function(0, _("Approved Purchase &Requests "),
			"purchasing/inquiry/approved_purchase_requests.php?", 'SA_APPROVEDPURCHASEREQUESTS', MENU_TRANSACTION);
		
		$this->add_lapp_function(0, _("Purchase Orders &Inquiry"),
			"purchasing/inquiry/purchase_order_inquiry.php?", 'SA_PURCHASEORDERINQUIRY', MENU_TRANSACTION);
			
		$this->add_lapp_function(0, _("Purchase &Order Maintenance"),
			"purchasing/purchase_order_maintenance.php", 'SA_PURCHASEORDERMAINTENANCE', MENU_TRANSACTION);

		$this->add_lapp_function(0, _("&Suppliers"),
			"purchasing/manage/suppliers.php?", 'SA_SUPPLIER', MENU_TRANSACTION);*/
		
	
		
		/*$this->add_lapp_function(0, _("Purchase &Order Entry"),
			"purchasing/purchase_order.php", 'SA_PUCHORDER', MENU_TRANSACTION);*/
		/*$this->add_lapp_function(0, _("Purchase &Order Entry"),
			"purchasing/po_entry_items.php?NewOrder=Yes", 'SA_PURCHASEORDER', MENU_TRANSACTION);*/
		
			
		/*$this->add_lapp_function(0, _("&Outstanding Purchase Orders Maintenance"),
			"purchasing/inquiry/po_search.php?", 'SA_GRN', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("Direct &GRN"),
			"purchasing/po_entry_items.php?NewGRN=Yes", 'SA_GRN', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("Direct &Invoice"),
			"purchasing/po_entry_items.php?NewInvoice=Yes", 'SA_SUPPLIERINVOICE', MENU_TRANSACTION);

		$this->add_rapp_function(0, _("&Payments to Suppliers"),
			"purchasing/supplier_payment.php?", 'SA_SUPPLIERPAYMNT', MENU_TRANSACTION);
		$this->add_rapp_function(0, "","");
		$this->add_rapp_function(0, _("Supplier &Invoices"),
			"purchasing/supplier_invoice.php?New=1", 'SA_SUPPLIERINVOICE', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("Supplier &Credit Notes"),
			"purchasing/supplier_credit.php?New=1", 'SA_SUPPLIERCREDIT', MENU_TRANSACTION);
		$this->add_rapp_function(0, _("&Allocate Supplier Payments or Credit Notes"),
			"purchasing/allocations/supplier_allocation_main.php?", 'SA_SUPPLIERALLOC', MENU_TRANSACTION);
*/
	
			
		/*$this->add_lapp_function(1, _("Supplier Transaction &Inquiry"),
			"purchasing/inquiry/supplier_inquiry.php?", 'SA_SUPPTRANSVIEW', MENU_INQUIRY);
		$this->add_lapp_function(1, "","");
		$this->add_lapp_function(1, _("Supplier Allocation &Inquiry"),
			"purchasing/inquiry/supplier_allocation_inquiry.php?", 'SA_SUPPLIERALLOC', MENU_INQUIRY);

		$this->add_rapp_function(1, _("Supplier and Purchasing &Reports"),
			"reporting/reports_main.php?Class=1", 'SA_SUPPTRANSVIEW', MENU_REPORT);
*/
		//$this->add_module(_("Maintenance"));
		
		$this->add_extensions();
	}
}


?>