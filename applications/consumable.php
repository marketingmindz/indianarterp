<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class consumable_app extends application 
{
	function consumable_app() 
	{
		$this->application("consumables", _($this->help_context = "&Consumables"));
	
		$this->add_module(_("Consumable Master"));
		
		$this->add_lapp_function(0, _("Consumable Management"),
			"inventory/manage/consumable_management.php?", 'SA_CONSUMABLE', MENU_MAINTENANCE);
			
		$sql="SELECT * FROM 0_master_creation";
		$result = mysql_query($sql);
		while ($myrow = mysql_fetch_array($result)) 
		{
			$master_name = $myrow['master_name'];
			$this->add_lapp_function(0, _($master_name),
				"inventory/manage/master_category.php?id=".$master_name, 'SA_CONSUMABLE', MENU_MAINTENANCE);
		}
		/*$this->add_lapp_function(4, _("Hardware"),
				"inventory/manage/master_category.php?id=hardware", 'SA_CONSUMABLE', MENU_MAINTENANCE);
	
		$this->add_lapp_function(4, _("Stationary"),
			"inventory/manage/master_category.php?id=stationary", 'SA_CONSUMABLE', MENU_MAINTENANCE);
		$this->add_lapp_function(4, _("Packaging"),
			"inventory/manage/master_category.php?id=packaging", 'SA_CONSUMABLE', MENU_MAINTENANCE);
		$this->add_lapp_function(4, _("Chemical"),
			"inventory/manage/master_category.php?id=chemical", 'SA_CONSUMABLE', MENU_MAINTENANCE);
		$this->add_lapp_function(4, _("Electrical"),
			"inventory/manage/master_category.php?id=electrical", 'SA_ELECTRICAL', MENU_MAINTENANCE);
		$this->add_lapp_function(4, _("Fabric Master"),
			"inventory/manage/master_category.php?id=fabric", 'SA_CONSUMABLE', MENU_MAINTENANCE);*/
		/*$this->add_lapp_function(4, _("Consumable"),
			"inventory/manage/consumable_management.php?", 'SA_CONSUMABLE', MENU_MAINTENANCE);*/
		
		$this->add_lapp_function(0, _("Make Brand"),
			"inventory/manage/item_company.php?", 'SA_COMPANY_MASTER', MENU_MAINTENANCE);
		$this->add_lapp_function(0, _("Principal Master"),
			"inventory/manage/principal_master.php?", 'SA_PRINCIPAL', MENU_MAINTENANCE);
		$this->add_lapp_function(0, _("Opening Stock Master"),
			"inventory/manage/open_stock.php?", 'SA_OPEN_STOCK', MENU_MAINTENANCE);
		

		$this->add_extensions();
	}
}


?>