<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

class inventory_app extends application
{
	function inventory_app()
	{
		$this->application("stock", _($this->help_context = "&Design & Development"));

		//--------------------- code section -------------------

		$this->add_module(_("Coding"));
		
		$this->add_lapp_function(0, _("&Design Code"),
			"inventory/manage/design_code.php?", 'SA_DESIGN', MENU_ENTRY);
	
		$this->add_lapp_function(0, _("&Finish Product"),
			"inventory/manage/finish_product.php?", 'SA_FINISH_PRODUCT', MENU_ENTRY);
			
		$this->add_lapp_function(0, _("&Photo Bank"),
			"inventory/manage/photo_bank.php?", 'PHOTO_BANK', MENU_ENTRY);
		
		$this->add_lapp_function(0, _("Fabric Combination"),
			"inventory/manage/fabric_combination.php?", 'SA_FAB_COMBINATION', MENU_MAINTENANCE);

		//------------ list section -------------------------------	
		$this->add_module(_("Lists"));

		$this->add_lapp_function(1, _("&Design Code List"),
			"inventory/manage/design_code_search.php?", 'SA_SEARCH_DESIGN', MENU_ENTRY);
		
		$this->add_lapp_function(1, _("&Finish Product List"),
			"inventory/manage/finish_product_list.php?", 'SA_FINISH_DETAILS', MENU_ENTRY);

        //------------------------ Master Section -----------------

		$this->add_module(_("Master"));
		
		$this->add_lapp_function(2, _("Master Creation"),
			"inventory/manage/master_creator.php?", 'SA_MASTER', MENU_MAINTENANCE);
		
		$this->add_lapp_function(2, _("Company Master"),
			"inventory/manage/make_brand.php?", 'SA_COMPANY_MASTER', MENU_MAINTENANCE);		
			
		$this->add_lapp_function(2, _("Range Master"),
			"inventory/manage/item_range.php?", 'SA_RANGE', MENU_MAINTENANCE);
		
		$this->add_lapp_function(2, _("Product Type Master"),
			"inventory/manage/item_product_type.php?", 'SA_PRODUCT_TYPE', MENU_MAINTENANCE);
		
		$this->add_lapp_function(2, _("Category Master"),
			"inventory/manage/item_category.php?", 'SA_CATEGORY', MENU_MAINTENANCE);
		
		$this->add_lapp_function(2, _("Leg Master"),
			"inventory/manage/leg_master.php?", 'SA_LEG', MENU_MAINTENANCE);	
		
		$this->add_lapp_function(2, _("Color Master"),
			"inventory/manage/color_master.php?", 'SA_COLOR', MENU_MAINTENANCE);

		$this->add_rapp_function(2, _("Finish Master"),
			"inventory/manage/finish_master.php?", 'SA_FINISH', MENU_MAINTENANCE);
		
		$this->add_rapp_function(2, _("Wood Master"),
			"inventory/manage/wood_master.php?", 'SA_WOOD', MENU_MAINTENANCE);
			
		$this->add_rapp_function(2, _("Part Master"),
			"inventory/manage/part_master.php?", 'SA_PART', MENU_MAINTENANCE);
		
		$this->add_rapp_function(2, _("&Units of Measure"),
			"inventory/manage/item_units.php?", 'SA_UOM', MENU_MAINTENANCE);

		$this->add_rapp_function(2, _("Label Master"),
			"inventory/manage/label_master.php?", 'SA_LABEL', MENU_MAINTENANCE);

		$this->add_rapp_function(2, _("Carton Master"),
			"inventory/manage/carton_master.php?", 'SA_CARTON', MENU_MAINTENANCE);
		
		//------------------- Packaging Section -----------------------

		$this->add_module(_("Packaging"));
		
		$this->add_lapp_function(3, _("Packaging Module"),
			"inventory/manage/packaging_module.php?", 'SA_PACKAGING', MENU_MAINTENANCE);

		$this->add_lapp_function(3, _("Packaging List"),
			"inventory/manage/packaging_code_search.php?", 'SA_PACKING_DETAILS', MENU_MAINTENANCE);


		$this->add_extensions();
	}
}


?>