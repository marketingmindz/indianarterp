<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class costing_app extends application 
{
	function costing_app() 
	{
		$this->application("costing", _($this->help_context = "&Accounts"));
		
		$this->add_module(_("Suppliers"));
		
		$this->add_lapp_function(0, _("&Suppliers"),
			"purchasing/manage/suppliers.php?", 'SA_SUPPLIER', MENU_TRANSACTION);
		/*$this->add_lapp_function(0, _("&Costing"),
			"costing/manage/costing.php?", 'SA_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Currency Master"),
			"costing/manage/currency_master.php?", 'SA_CURRENCY_MASTER', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Saved Costing"),
			"costing/manage/saved_costing.php?", 'SA_SAVED_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Freeze Costing"),
			"costing/manage/freeze_costing.php?", 'SA_FREEZE_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Locked Costing"),
			"costing/manage/locked_costing.php?", 'SA_LOCKED_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Edit Range Cost"),
			"costing/manage/edit_range_cost.php?", 'SA_EDIT_RANGE_COST', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Price List"),
			"costing/manage/price_list.php?", 'SA_PRICE_LIST', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Consumable Costing"),
			"costing/manage/consumable_costing.php?", 'SA_CONSUMABLE_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Saved Consumable Costing"),
			"costing/manage/saved_consumable_costing.php?", 'SA_SAVED_CONSUMABLE_COSTING', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("&Consumable Costing for PO"),
			"costing/manage/consumable_costing_for_po.php?", 'SA_CONSUMABLE_COSTING_FOR_PO', MENU_TRANSACTION);*/

	
		
		$this->add_extensions();
	}
}


?>