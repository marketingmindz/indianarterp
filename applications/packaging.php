<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class packaging_app extends application 
{
	function packaging_app() 
	{
		$this->application("packaging", _($this->help_context = "&Packaging"));
	
		$this->add_module(_("Packaging Master"));	
		
		$this->add_lapp_function(0, _("Label Master"),
			"inventory/manage/label_master.php?", 'SA_LABEL', MENU_MAINTENANCE);
		$this->add_lapp_function(0, _("Carton Master"),
			"inventory/manage/carton_master.php?", 'SA_CARTON', MENU_MAINTENANCE);	
		$this->add_lapp_function(0, _("Packaging Module"),
			"inventory/manage/packaging_module.php?", 'SA_PACKAGING', MENU_MAINTENANCE);
		$this->add_lapp_function(0, _("Packaging List"),
			"inventory/manage/packaging_code_search.php?", 'SA_PACKING_DETAILS', MENU_MAINTENANCE);
	

		$this->add_extensions();
	}
}


?>