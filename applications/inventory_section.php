<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
class inventory_section_app extends application 
{
	function inventory_section_app() 
	{
		$this->application("inventory_section", _($this->help_context = "&Store"));

		//-------------------- Inventory Section -----------
		
		$this->add_module(_("Inventory"));

		$this->add_lapp_function(0, _("&Production Team"),
				"inventory/manage/prodcution_team.php?", 'SA_PRODUCTION', MENU_MAINTENANCE);

		$this->add_lapp_function(0, _("&Work Center"),
				"inventory/manage/work_center.php?", 'SA_WORKCENTER', MENU_MAINTENANCE);

		$this->add_lapp_function(0, _("&Locations"),
			"inventory/manage/location_master.php?", 'SA_LOCATIONMASTER', MENU_MAINTENANCE);


		//-------------- Inventory Adjustments
		$this->add_module(_("Inventory Adjustments"));

		$this->add_lapp_function(1, _("Opening Stock Master"),
				"inventory/manage/opening_stock_master.php?", 'SA_STOCKMASTER', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Stock Register"),
			    "inventory/manage/item_adjustment.php?", 'SA_ITEMADJUSTMENT', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Item Location Transfer"),
			    "inventory/manage/inventory_location_transfer.php?", 'SA_INVENTORYLOCATIONTRANSFER', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Opening Stock List"),
				"inventory/manage/opening_stock_list.php?", 'SA_OPENINGSTOCKLIST', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Inventory Item Status"),
			"inventory/manage/inventory_item_status.php?", 'SA_INVENTORYITEMSTATUS', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Inventory Stock Transactions"),
			"inventory/manage/stock_transaction.php?", 'SA_STOCKTRANSACTION', MENU_MAINTENANCE);

		$this->add_lapp_function(1, _("Reorder Level Inquiry"),
			"inventory/manage/reorder_level_inquiry.php?", 'SA_REORDER_LEVEL_INQUIRY', MENU_MAINTENANCE);

		/*$this->add_module(_("Purchase Requests"));
		
		$this->add_lapp_function(0, _("Purchase &Request Entry"),
			"purchasing/purchase_request_entry.php", 'SA_PURCHASEREQUEST', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("Purchase &Request Entry Using Reorder"),
			"purchasing/purchase_request_entry_using_reorder.php", 'SA_PURCHASEREQUESTUSINGREORDER', MENU_TRANSACTION);
		$this->add_lapp_function(0, _("Purchase &Request Inquiry"),
			"purchasing/inquiry/store_pr_inquiry.php?location=store&erp", 'SA_PURCHASEREQUESTINQUIRY', MENU_TRANSACTION);
		$this->add_module(_("Locations"));
		
		$this->add_lapp_function(1, _("&Production Team"),
				"inventory/manage/prodcution_team.php?", 'SA_PRODUCTION', MENU_MAINTENANCE);
		$this->add_lapp_function(1, _("&Work Center"),
				"inventory/manage/work_center.php?", 'SA_WORKCENTER', MENU_MAINTENANCE);
		$this->add_lapp_function(1, _("&Locations"),
			"inventory/manage/location_master.php?", 'SA_LOCATIONMASTER', MENU_MAINTENANCE);
			
			
		$this->add_module(_("Inventory Management"));
			
			$this->add_lapp_function(2, _("Opening Stock Master"),
				"inventory/manage/opening_stock_master.php?", 'SA_STOCKMASTER', MENU_MAINTENANCE);	
			$this->add_lapp_function(2, _("Stock Register"),
			    "inventory/manage/item_adjustment.php?", 'SA_ITEMADJUSTMENT', MENU_MAINTENANCE);
			$this->add_lapp_function(2, _("Item Location Transfer"),
			    "inventory/manage/inventory_location_transfer.php?", 'SA_INVENTORYLOCATIONTRANSFER', MENU_MAINTENANCE);	
				
			$this->add_module(_("Item Status"));
				
			$this->add_lapp_function(3, _("Opening Stock List"),
				"inventory/manage/opening_stock_list.php?", 'SA_OPENINGSTOCKLIST', MENU_MAINTENANCE);
			
			
			$this->add_lapp_function(3, _("Inventory Item Status"),
			"inventory/manage/inventory_item_status.php?", 'SA_INVENTORYITEMSTATUS', MENU_MAINTENANCE);

			$this->add_lapp_function(3, _("Reorder Level Inquiry"),
			"inventory/manage/reorder_level_inquiry.php?", 'SA_REORDER_LEVEL_INQUIRY', MENU_MAINTENANCE);
		
*/
		$this->add_extensions();
	}
}


?>