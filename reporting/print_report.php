<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
    Released under the terms of the GNU General Public License, GPL,
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function getReportItems($sql)
{
    $result = db_query($sql, "Could not get Report Items");
    return $result;
}

function reportLayoutSetup($module)
{
    switch ($module) {
        case 'design_code':
            $cols = array(0, 60, 110, 175, 275, 350, 420, 490, 550);
            break;
        case 'finishProduct':
            $cols = array(0, 70, 190, 320, 390, 460, 520);
            break;
        case 'itemCategory':
            $cols = array(0, 160);
            break;
        case 'itemRange':
            $cols = array(0, 160);
            break;
        case 'masterCreator':
            $cols = array(0, 160, 290);
            break;
        case 'consumableStock':
            $cols = array(0, 60, 120, 220, 320, 400, 480, 560);
            break;
        case 'stockTransaction':
            $cols = array(0, 40, 80, 120, 200, 300, 340, 380, 420, 460, 500, 520);
            break;
        case 'reorderLevelInquiry':
            $cols = array(0, 60, 120, 240, 320, 400, 480, 560);
            break;
        case 'consumablesReport':
            $cols = array(0, 60, 140, 180, 260, 300, 340, 380, 420, 460);
            break;
        case 'approvedPurchaseRequest':
            $cols = array(0, 120, 220, 350, 450);
            break;
        case 'purchaseRequestInquiry':
            $cols = array(0, 40, 120, 200, 280, 360);
            break;
        case 'purchaseOrderInquiry':
            $cols = array(0, 40, 80, 200, 260, 320, 400);
            break;
        case 'suppliers':
            $cols = array(0, 60, 100, 140, 180, 220, 260, 300, 340, 380, 420, 460, 500, 540, 580);
            break;
        case 'supplierProducts':
            $cols = array(0, 100, 200, 300, 400, 480);
            break;
        case 'itemReceive':
            $cols = array(0, 20, 90, 130, 170, 230, 280, 310, 340, 370, 400, 430, 460, 490, 520, 550);
            break;
        default:
            $cols = array(0, 60, 100, 140, 180, 220, 260, 300, 340, 380, 420, 460, 500, 540, 580);
            break;
    }
    return $cols;
}

function print_report($module, $orientation, $destination, $selectd_columns, $sql, $title="")
{
    global $path_to_root, $systypes_array, $primary_modules, $site_url;
    
    if($title == "")
        $title = $_POST['report_name'];

    $des = getReportItems($sql);

    if ($destination)
        include_once($path_to_root . "/reporting/includes/excel_report.inc");
    else
        include_once($path_to_root . "/reporting/includes/pdf_report.inc");

    $orientation = ($orientation ? 'P' : 'L');
    $dec = user_price_dec();

    $cols = reportLayoutSetup($module);

    $headers = Array();
    $aligns = Array();

    $rep = new FrontReport($title, $title, user_pagesize(), 9, $orientation);

    foreach ($primary_modules["$module"]['columns'] as $key => $value) {
        if(in_array($key, $selectd_columns))
        {
            $headers[] = $value;
            $aligns[] = "left";
        }
    }

    // rearrange the column width
    $seleced_cols = count($headers);
    $required_cols = array_slice($cols, 0, $seleced_cols);


    $usr = get_user($user);
    $user_id = $usr['user_id'];
    $params =   array(  0 => $comments,
                        1 => array('text' => _('Period'), 'from' => $from,'to' => $to),
                        2 => array('text' => _('Type'), 'from' => ($systype != -1 ? $systypes_array[$systype] : _('All')), 'to' => ''),
                        3 => array('text' => _('User'), 'from' => ($user != -1 ? $user_id : _('All')), 'to' => ''));

    
    if ($orientation == 'L')
        recalculate_cols($cols);

    $rep->Font();
    $rep->Info($params, $required_cols, $headers, $aligns);
    $rep->NewPage();

    switch ($module) 
    {
        case 'design_code':
            $img_folder = "desingImage";
            break;
        case 'finishProduct':
            $img_folder = "finishProductImage";
            break;
        default:
            $img_folder = "";
            break;
    }
    
    $image_base_path = $site_url."/company/0/".$img_folder."/";

    while ($myrow=db_fetch_assoc($des))
    {
        $i = 0;
        $k = 1;
        $prod_size = '';

        foreach ($myrow as $pro_key => $pro_value) 
        {
            if($pro_key == 'product_image' OR $pro_key == 'design_image')
            {
                $product_images = explode(',', $pro_value);

                if($product_images[0]!=''){             
                    $image_url = trim($image_base_path.$product_images[0]);
                    //$image_url = trim($image_base_path);
                    $image_url = str_replace(" ","%20", $image_url);

                    $rep->AddImage($image_url, $i + 50, $rep->row - 25, 40, 40);
                    // $rep->AddImage($image_url, $i , $rep->row , 40, 40);
                }
                else{

                    $image_url = trim($image_base_path.'no-image.jpg');
                    //$image_url = trim($image_base_path);
                    $image_url = str_replace(" ","%20", $image_url);

                    $rep->AddImage($image_url, $i + 50, $rep->row - 25, 40, 40);
                    //$rep->TextCol($i, $k, 'No Image');
                }

            } else if($pro_key == 'asb_weight' OR $pro_key == 'weight') {
                    $prod_size .= $pro_value;
                    $i--;
                    $k--;
            } else if($pro_key == 'asb_height' OR $pro_key == 'height') {
                    if($pro_value != '') { $prod_size .= "*".$pro_value; }
                    $i--;
                    $k--;
            } else if($pro_key == 'asb_density' OR $pro_key == 'density') {
                    if($pro_value != '') { $prod_size .= "*".$pro_value; }
                    $rep->TextCol($i, $k, $prod_size); 
            } else if($pro_key == 'description') {
                    //Custom function to call multicell() to wrap long text
                    $rep->TextCol($i, $k, $pro_value);
            } else {
                $rep->TextCol($i, $k, $pro_value);
                //$rep->TextCol($i, $k, $pro_value, 0, 0, 0, 0, NULL, 0);
                //$rep->TextCol($i, $k, $pro_value);
            }

            $i++;           
            $k++;
        }       
       
        if($destination) {
            $rep->NewLine(1, 2);
        }else{
             $rep->NewLine(5, 2);
        }
      
    }
    $rep->Line($rep->row + 5);
    $rep->End();
}