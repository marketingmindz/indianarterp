<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_itemReceive_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_itemReceive_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('po_no', $_POST['select_column']) ? 'r.po_no, ' : "";
	$sel_cols .= in_array('supp_name', $_POST['select_column']) ? 's.supp_name, ' : "";
	$sel_cols .= in_array('location_name', $_POST['select_column']) ? 'l.location_name, ' : "";
	$sel_cols .= in_array('work_center_name', $_POST['select_column']) ? 'w.work_center_name, ' : "";
	$sel_cols .= in_array('pro_team_name', $_POST['select_column']) ? 'pt.pro_team_name, ' : "";
	$sel_cols .= in_array('master_name', $_POST['select_column']) ? 'm.master_name, ' : "";
	$sel_cols .= in_array('consumable_name', $_POST['select_column']) ? 'c.consumable_name, ' : "";
	$sel_cols .= in_array('company_name', $_POST['select_column']) ? 'i.company_name, ' : "";
	$sel_cols .= in_array('ordered_quantity', $_POST['select_column']) ? 'it.ordered_quantity, ' : "";
	$sel_cols .= in_array('received_quantity', $_POST['select_column']) ? 'it.received_quantity, ' : "";
	$sel_cols .= in_array('ok_quantity', $_POST['select_column']) ? 'it.ok_quantity, ' : "";
	$sel_cols .= in_array('rejected_quantity', $_POST['select_column']) ? 'it.rejected_quantity, ' : "";
	$sel_cols .= in_array('unit', $_POST['select_column']) ? 'it.unit, ' : "";
	$sel_cols .= in_array('chalan_no', $_POST['select_column']) ? 'it.chalan_no, ' : "";
	$sel_cols .= in_array('invoice_no', $_POST['select_column']) ? 'it.invoice_no, ' : "";
	$sel_cols .= in_array('po_date', $_POST['select_column']) ? 'r.po_date, ' : "";
	$sel_cols .= in_array('receive_date', $_POST['select_column']) ? 'r.receive_date, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'purchase_order_receive r 
	 LEFT JOIN '.TB_PREF.'purchase_order_received_items it on it.receiving_id = r.rec_id
	 Left Join '.TB_PREF.'suppliers s on s.supplier_id = r.supplier_id
	 Left Join '.TB_PREF.'master_creation m on it.consumable_id = m.master_id
	 Left Join '.TB_PREF.'consumable_master c on it.consumable_category = c.consumable_id 
	 Left Join '.TB_PREF.'item_company i on i.company_id = c.company_id
	 Left Join '.TB_PREF.'location_master l on r.location_id = l.location_id 
	 Left Join '.TB_PREF.'work_center w on r.work_center_id = w.work_center_id';


	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		switch ($_POST["columns$i"]) {
			case 'master_id':
				$alias = "m";
				break;
			case 'consumable_id':
				$alias = "c";
				break;
			case 'company_id':
				$alias = "i";
				break;
			default:
				$alias = "r";
				break;
		}

		$newFilter =  filterColumn($alias, $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'supp_name':
			$alias = "s";
			break;
		case 'company_name':
			$alias = "i";
			break;
		case 'po_no':
		case 'po_date':
		case 'receive_date':
			$alias = "r";
			break;
		case 'location_name':
			$alias = "l";
			break;
		case 'work_center_name':
			$alias = "w";
			break;
		case 'master_name':
			$alias = "m";
			break;
		case 'consumable_name':
			$alias = "c";
			break;
		case 'pro_team_name':
			$alias = "pt";
			break;
		default:
			$alias = "it";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}