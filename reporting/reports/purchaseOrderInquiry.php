<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{	
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];

	$sql = build_purchaseOrder_query();
	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}



function build_purchaseOrder_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('po_supp_rel', $_POST['select_column']) ? 'prs.po_supp_rel, ' : "";
	$sel_cols .= in_array('pr_id', $_POST['select_column']) ? 'po.pr_id, ' : "";
	$sel_cols .= in_array('supp_name', $_POST['select_column']) ? 's.supp_name, ' : "";
	$sel_cols .= in_array('location_name', $_POST['select_column']) ? 'lo.location_name, ' : "";
	$sel_cols .= in_array('work_center_name', $_POST['select_column']) ? 'w.work_center_name, ' : "";
	$sel_cols .= in_array('selection_name', $_POST['select_column']) ? 'st.selection_name, ' : "";
	$sel_cols .= in_array('date', $_POST['select_column']) ? 'po.date, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF."purchase_supp_rel prs
			LEFT JOIN ".TB_PREF."suppliers s on prs.supplier_id = s.supplier_id 
			LEFT JOIN ".TB_PREF."purchase_order po on prs.po_id = po.po_id
			LEFT JOIN ".TB_PREF."location_master lo on po.location_id = lo.location_id 
			LEFT JOIN ".TB_PREF."work_center w on po.work_center_id = w.work_center_id
			LEFT JOIN ".TB_PREF."selection st on prs.status = st.selection_id";


	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		if($_POST["columns$i"] == 'status' && $_POST["colValue$i"] == '0')
			continue;

		switch ($_POST["columns$i"]) {
			case 'pr_id':
				$alias = "po";
				break;
			case 'supplier_id':
				$alias = "s";
				break;
			case 'location_id':
				$alias = "lo";
				break;
			case 'work_center_id':
				$alias = "w";
				break;
			default:
				$alias = "prs";
				break;
		}

		$newFilter =  filterColumn($alias, $i);

		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'pr_id':
			$alias = "po";
			break;
		case 'supp_name':
			$alias = "s";
			break;
		case 'location_name':
			$alias = "lo";
			break;
		case 'work_center_name':
			$alias = "w";
			break;
		case 'selection_name':
			$alias = "st";
			break;
		default:
			$alias = "prs";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}