<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_desgin_code_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}

function build_desgin_code_query()
{
	$sel_cols = "";
	$sel_cols .= in_array('design_image', $_POST['select_column']) ? 'd.design_image, ' : "";
	$sel_cols .= in_array('design_id', $_POST['select_column']) ? 'd.design_id, ' : "";
	$sel_cols .= in_array('design_code', $_POST['select_column']) ? 'd.design_code, ' : "";
	$sel_cols .= in_array('product_name', $_POST['select_column']) ? 'd.product_name, ' : "";
	$sel_cols .= in_array('cat_name', $_POST['select_column']) ? 'c.cat_name, ' : "";
	$sel_cols .= in_array('range_name', $_POST['select_column']) ? 'i.range_name, ' : "";
	$sel_cols .= in_array('pro_name', $_POST['select_column']) ? 'pt.pro_name, ' : "";
	$sel_cols .= in_array('weight', $_POST['select_column']) ? 'd.weight, ' : "";
	$sel_cols .= in_array('weight', $_POST['select_column']) ? 'd.height, ' : "";
	$sel_cols .= in_array('weight', $_POST['select_column']) ? 'd.density, ' : "";
	$sel_cols .= in_array('description', $_POST['select_column']) ? 'd.description, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF."design_code d 
			LEFT JOIN ".TB_PREF."item_range i ON i.id = d.range_id
			LEFT JOIN ".TB_PREF."item_category c ON c.category_id = d.category_id
			LEFT JOIN ".TB_PREF."item_products pt ON pt.pro_id = d.product_type ";

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$newFilter =  filterColumn('d', $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'cat_name':
			$alias = "c";
			break;
		case 'range_name':
			$alias = "i";
			break;
		case 'pro_name':
			$alias = "pt";
			break;
		default:
			$alias = "d";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}


