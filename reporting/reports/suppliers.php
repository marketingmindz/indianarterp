<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_supplier_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_supplier_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('supp_name', $_POST['select_column']) ? 's.supp_name, ' : "";
	$sel_cols .= in_array('supp_ref', $_POST['select_column']) ? 's.supp_ref, ' : "";
	$sel_cols .= in_array('address', $_POST['select_column']) ? 's.address, ' : "";
	$sel_cols .= in_array('supp_address', $_POST['select_column']) ? 's.supp_address, ' : "";
	$sel_cols .= in_array('tin_no', $_POST['select_column']) ? 's.tin_no, ' : "";
	$sel_cols .= in_array('cin_no', $_POST['select_column']) ? 's.cin_no, ' : "";
	$sel_cols .= in_array('pan_no', $_POST['select_column']) ? 's.pan_no, ' : "";
	$sel_cols .= in_array('can_no', $_POST['select_column']) ? 's.can_no, ' : "";
	$sel_cols .= in_array('st_no', $_POST['select_column']) ? 's.st_no, ' : "";
	$sel_cols .= in_array('phone', $_POST['select_column']) ? 'p.phone, ' : "";
	$sel_cols .= in_array('phone2', $_POST['select_column']) ? 'p.phone2, ' : "";
	$sel_cols .= in_array('fax', $_POST['select_column']) ? 'p.fax, ' : "";
	$sel_cols .= in_array('email', $_POST['select_column']) ? 'p.email, ' : "";
	$sel_cols .= in_array('notes', $_POST['select_column']) ? 'p.notes, ' : "";
	$sel_cols .= in_array('curr_code', $_POST['select_column']) ? 's.curr_code, ' : "";



	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'suppliers s
			LEFT JOIN '.TB_PREF.'crm_contacts c ON  c.id=s.supplier_id 
			LEFT JOIN '.TB_PREF.'crm_persons p ON  p.id = c.person_id';

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$newFilter =  filterColumn('s', $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}
	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'phone':
		case 'phone2':
		case 'fax':
		case 'email':
		case 'notes':
			$alias = "p";
			break;
		default:
			$alias = "s";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}