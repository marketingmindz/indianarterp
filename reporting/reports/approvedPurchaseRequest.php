<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_approvedRequest_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_approvedRequest_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('pr_no', $_POST['select_column']) ? 'pr.pr_no, ' : "";
	$sel_cols .= in_array('location_name', $_POST['select_column']) ? 'l.location_name, ' : "";
	$sel_cols .= in_array('work_center_name', $_POST['select_column']) ? 'w.work_center_name, ' : "";
	$sel_cols .= in_array('selection_name', $_POST['select_column']) ? 'st.selection_name, ' : "";
	$sel_cols .= in_array('date', $_POST['select_column']) ? 'pr.date, ' : "";



	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF."purchase_request pr 
			LEFT JOIN ".TB_PREF."location_master l on pr.location_id = l.location_id
			LEFT JOIN ".TB_PREF."selection st on pr.status = st.selection_id
			LEFT JOIN ".TB_PREF."work_center w on pr.work_center_id = w.work_center_id";

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$alias = ($_POST["columns$i"] == "supp_name") ? "s" : "pr";
		$newFilter =  filterColumn($alias, $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}
	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'location_name':
			$alias = "l";
			break;
		case 'work_center_name':
			$alias = "w";
			break;
		case 'selection_name':
			$alias = "st";
			break;
		default:
			$alias = "pr";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}
