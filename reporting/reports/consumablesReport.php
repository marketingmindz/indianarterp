<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_consumables_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_consumables_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('consumable_code', $_POST['select_column']) ? 'cm.consumable_code, ' : "";
	$sel_cols .= in_array('consumable_name', $_POST['select_column']) ? 'cm.consumable_name, ' : "";
	$sel_cols .= in_array('company_name', $_POST['select_column']) ? 'c.company_name, ' : "";
	$sel_cols .= in_array('master_name', $_POST['select_column']) ? 'm.master_name, ' : "";
	$sel_cols .= in_array('name', $_POST['select_column']) ? 'pm.name, ' : "";
	$sel_cols .= in_array('unit_id', $_POST['select_column']) ? 'cm.unit_id, ' : "";
	$sel_cols .= in_array('weight', $_POST['select_column']) ? 'cm.weight, ' : "";
	$sel_cols .= in_array('size', $_POST['select_column']) ? 'cm.size, ' : "";
	$sel_cols .= in_array('cbm', $_POST['select_column']) ? 'cm.cbm, ' : "";
	$sel_cols .= in_array('gsm', $_POST['select_column']) ? 'cm.gsm, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'consumable_master cm
			LEFT JOIN '.TB_PREF.'master_creation m ON  cm.master_id=m.master_id 
			LEFT JOIN '.TB_PREF.'item_company c ON  cm.company_id = c.company_id
			LEFT JOIN '.TB_PREF.'principal_master pm ON cm.principal_id = pm.principal_id';

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$alias = ($_POST["columns$i"] == "company_id") ? "c" : "cm";
		$newFilter =  filterColumn($alias, $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'company_name':
			$alias = "c";
			break;
		case 'name':
			$alias = "pm";
			break;
		case 'master_name':
			$alias = "m";
			break;
		default:
			$alias = "cm";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}
