<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_finishProduct_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_finishProduct_query()
{	
	$sel_cols = "";
	$sel_cols .= in_array('product_image', $_POST['select_column']) ? 'd.product_image, ' : "";
	$sel_cols .= in_array('finish_comp_code', $_POST['select_column']) ? 'd.finish_comp_code, ' : "";
	$sel_cols .= in_array('finish_product_name', $_POST['select_column']) ? 'd.finish_product_name, ' : "";
	$sel_cols .= in_array('range_name', $_POST['select_column']) ? 't.range_name, ' : "";
	$sel_cols .= in_array('cat_name', $_POST['select_column']) ? 'c.cat_name, ' : "";
	$sel_cols .= in_array('asb_weight', $_POST['select_column']) ? 'd.asb_weight, ' : "";
	$sel_cols .= in_array('asb_weight', $_POST['select_column']) ? 'd.asb_height, ' : "";
	$sel_cols .= in_array('asb_weight', $_POST['select_column']) ? 'd.asb_density, ' : "";
	$sel_cols .= in_array('description', $_POST['select_column']) ? 'd.description, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	echo $sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'finish_product d 
			Left Join '.TB_PREF.'item_range t on d.range_id = t.id
			Left Join '.TB_PREF.'item_category c on d.category_id = c.category_id';

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$newFilter =  filterColumn('d', $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
		case 'cat_name':
			$alias = "c";
			break;
		case 'range_name':
			$alias = "t";
			break;
		default:
			$alias = "d";
			break;
	}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}