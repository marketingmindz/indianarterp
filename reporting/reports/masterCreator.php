<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_masterCreator_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_masterCreator_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('master_name', $_POST['select_column']) ? 'mc.master_name, ' : "";
	$sel_cols .= in_array('master_description', $_POST['select_column']) ? 'mc.master_description, ' : "";
	$sel_cols .= in_array('master_reference', $_POST['select_column']) ? 'mc.master_reference, ' : "";

	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'master_creation mc';

	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$newFilter =  filterColumn('d', $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}
	
	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	$orderby = " ORDER BY mc.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}