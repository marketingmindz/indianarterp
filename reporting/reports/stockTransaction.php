<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

if(isset($_POST['save_report']))
{
	$orientation = $_POST['orientation'];
	$destination = $_POST['destination'];
	$sql = build_stockTransaction_query();

	insert_reportDetails($sql);

	print_report($_POST['primary_module'], $orientation, $destination, $_POST['select_column'], $sql);
}


function build_stockTransaction_query()
{	
	$sel_cols = "";

	$sel_cols .= in_array('location_name', $_POST['select_column']) ? 'l.location_name, ' : "";
	$sel_cols .= in_array('work_center_name', $_POST['select_column']) ? 'w.work_center_name, ' : "";
	$sel_cols .= in_array('pro_team_name', $_POST['select_column']) ? 'pt.pro_team_name, ' : "";
	$sel_cols .= in_array('master_name', $_POST['select_column']) ? 'm.master_name, ' : "";
	$sel_cols .= in_array('consumable_name', $_POST['select_column']) ? 'c.consumable_name, ' : "";
	$sel_cols .= in_array('company_name', $_POST['select_column']) ? 'i.company_name, ' : "";
	$sel_cols .= in_array('opening_stock', $_POST['select_column']) ? 'txn.opening_stock, ' : "";
	$sel_cols .= in_array('closing_stock', $_POST['select_column']) ? 'txn.closing_stock, ' : "";
	$sel_cols .= in_array('trans_quantity', $_POST['select_column']) ? 'txn.trans_quantity, ' : "";
	$sel_cols .= in_array('unit', $_POST['select_column']) ? 'txn.unit, ' : "";
	$sel_cols .= in_array('type', $_POST['select_column']) ? 'txn.type, ' : "";
	$sel_cols .= in_array('date', $_POST['select_column']) ? 'txn.date, ' : "";


	$sel_cols = rtrim($sel_cols,', ');

	$sql = "SELECT ".$sel_cols." FROM ".TB_PREF.'stock_transaction txn 
	 Left Join '.TB_PREF.'master_creation m on txn.consumable_category = m.master_id
	 Left Join '.TB_PREF.'consumable_master c on txn.consumable_id = c.consumable_id 
	 Left Join '.TB_PREF.'item_company i on i.company_id = c.company_id
	 Left Join '.TB_PREF.'location_master l on txn.location_id = l.location_id 
	 Left Join '.TB_PREF.'work_center w on txn.work_center_id = w.work_center_id
	 Left Join '.TB_PREF.'production_team pt on txn.pro_team_id = pt.pro_team_id';


	$filter = "";
	for($i = 1; $i <= $_POST['total_filters']; $i++)
	{
		$alias = ($_POST["columns$i"] == "company_id") ? "i" : "txn";

		$newFilter =  filterColumn($alias, $i);
	
		if($newFilter)
			$filter .= ($i == 1) ? $newFilter : " && ".$newFilter;
	}

	if($filter != "") 
	{
		$sql .= " WHERE ".$filter;
	}

	switch ($_POST["group_by"]) {
			case 'location_name':
				$alias = "l";
				break;
			case 'work_center_name':
				$alias = "w";
				break;
			case 'pro_team_name':
				$alias = "pt";
				break;
			case 'master_name':
				$alias = "m";
				break;
			case 'consumable_name':
				$alias = "c";
				break;
			case 'company_name':
				$alias = "i";
				break;
			default:
				$alias = "txn";
				break;
		}

	$orderby = " ORDER BY $alias.".$_POST['group_by']." ".$_POST['order_by'];

	$sql .= $groupBy.$orderby;
	return $sql;
}