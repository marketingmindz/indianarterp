<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root="..";
$page_security = 'SA_NEW_REPORT';
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/reporting/includes/db/report_db.inc");
include_once($path_to_root . "/reporting/includes/ui/reports_ui.inc");
include_once($path_to_root . "/reporting/primary_modules.php");
include_once($path_to_root . "/reporting/print_report.php");
include_once($path_to_root . "/reporting/includes/dropdown_methods.php");

?>
<style type="text/css">
	.multiselect { width: 200px; } 
	.selectBox { position: relative; } 
	.selectBox select { width: 100%; font-weight: bold; } 
	.overSelect { position: absolute; left: 0; right:0; top: 0; bottom: 0; } 
	.checkboxes { display: none; width: 200px; border: 1px #dadada solid; position: absolute; z-index: 999999999; background-color: #fff; max-height:300px; overflow:scroll; overflow-x:hidden;overflow-wrap: break-word; font-size: 14px;}
	.checkboxes label { display: block; border-bottom: 1px solid #ccc;  height: 21px;}
	.checkboxes label:hover { background-color: #1e90ff;}
	.optionGroup{ background-color: #ccc; white-space: nowrap;}
</style>


<?php


$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

add_js_file('reports.js');


global $Ajax;
page(_($help_context = "Add New Report"), false, false, "", $js);


$columnTypes = array(

		/* Design & Development Module */

		"design_code" => array(
					"design_id" => array('Design ID', "text"),
					"design_code"=> array("Design Code", "text"),
					"product_type"=> array("Product Type", "dropdown", "method"=>"product_type_dropdown"),
					'category_id'=> array("Category", "dropdown", "method"=>"product_category_dropdown"),
					'range_id'=> array("Range", "dropdown", "method"=>"product_range_dropdown")
					),

		"finishProduct" => array(
					"finish_comp_code" => array('Finish Code', "text"),
					"finish_product_name"=> array("Product Name", "text"),
					'range_id'=> array("Range", "dropdown", "method"=>"product_range_dropdown"),
					'category_id'=> array("Category Name", "dropdown", "method"=>"product_category_dropdown")
					),


		/* Store Module */

		"consumableStock" => array(
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"consumable_id"=> array("Consumable Category", "dropdown", "method"=>"consumable_name_list_dropdown"),
					"consumable_category"=> array("Consumable Name", "dropdown", "method"=>"consumable_category_list_dropdown"),
					"company_id"=> array("Brand Name", "dropdown", "method"=>"company_name_list_dropdown"),
					"stock_qty" => array('Quantity', "text"),
					"date" => array('Date', "date")
					),

		"stockTransaction" => array(
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"pro_team_id"=> array("Production Team", "dropdown", "method"=>"productionTeam_list_dropdown"),
					"consumable_id"=> array("Consumable Category", "dropdown", "method"=>"consumable_category_list_dropdown"),
					"consumable_category"=> array("Consumable Name", "dropdown", "method"=>"consumable_name_list_dropdown"),
					"company_id"=> array("Brand Name", "dropdown", "method"=>"company_name_list_dropdown"),
					"type"=> array("Transaction Type", "dropdown", "method"=>"stock_transactionType_dropdown"),
					"date" => array('Date', "date")
					),

		"reorderLevelInquiry" => array(
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"consumable_id"=> array("Consumable Category", "dropdown", "method"=>"consumable_name_list_dropdown"),
					"consumable_category"=> array("Consumable Name", "dropdown", "method"=>"consumable_category_list_dropdown")
					),


		/* Purchase Module */

		"consumablesReport" => array(
					"consumable_code" => array('Consumable Code', "text"),
					"consumable_name" => array('Consumable Name', "text"),
					"company_id"=> array("Brand Name", "dropdown", "method"=>"company_name_list_dropdown"),
					"master_id"=> array("Consumable Category", "dropdown", "method"=>"consumable_name_list_dropdown")
					),

		"approvedPurchaseRequest" => array(
					"pr_no" => array('PR No', "text"),
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"status"=> array("Status", "dropdown", "method"=>"purchase_request_status_list_dropdown"),
					"date" => array('Date', "date")
					),

		"purchaseRequestInquiry" => array(
					"pr_no" => array('PR No', "text"),
					"supplier_id"=> array("Supplier", "dropdown", "method"=>"supplier_name_list_dropdown"),
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"status"=> array("Status", "dropdown", "method"=>"purchase_request_status_list_dropdown"),
					"type"=> array("Type", "dropdown", "method"=>"purchase_request_type_list_dropdown"),
					"date" => array('Order Date', "date")
					),

		"purchaseOrderInquiry" => array(
					"po_supp_rel" => array('PO No', "text"),
					"pr_id" => array('PR No', "text"),
					"supplier_id"=> array("Supplier", "dropdown", "method"=>"supplier_name_list_dropdown"),
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"status"=> array("Status", "dropdown", "method"=>"purchase_request_status_list_dropdown"),
					"date" => array('Order Date', "date")
					),


		/* Account Module */

		"suppliers" => array(
					"supplier_id"=> array("Supplier Name", "dropdown", "method"=>"supplier_name_list_dropdown")
					),

		"supplierProducts" => array(
					"supplier_id"=> array("Supplier Name", "dropdown", "method"=>"supplier_name_list_dropdown"),
					"catagory"=> array("Consumable Category", "dropdown", "method"=>"consumable_name_list_dropdown"),
					"sub_category"=> array("Consumable Name", "dropdown", "method"=>"consumable_category_list_dropdown"),
					"company_id"=> array("Brand Name", "dropdown", "method"=>"company_name_list_dropdown"),
					"consumable_code" => array('Consumable Code', "text")
					),

		"itemReceive" => array(
					"po_no" => array('Purchase Order', "text"),
					"supplier_id"=> array("Supplier Name", "dropdown", "method"=>"supplier_name_list_dropdown"),
					"location_id"=> array("Location", "dropdown", "method"=>"location_list_dropdown"),
					"work_center_id"=> array("Work Center", "dropdown", "method"=>"workcenter_list_dropdown"),
					"consumable_id"=> array("Consumable Name", "dropdown", "method"=>"consumable_category_list_dropdown"),
					"master_id"=> array("Consumable Category", "dropdown", "method"=>"consumable_name_list_dropdown"),
					"company_id"=> array("Brand Name", "dropdown", "method"=>"company_name_list_dropdown"),
					"po_date"=> array("Order Date", "date"),
					"receive_date" => array('Receive Date', "date")
					),

		);

$conditions = Array(
					"Equals" => "Equals",
					"Greater Than" => "Greater Than",
					"Less Than" => "Less Than",
					"Similar" => "Similar",
					"Starts with" => "Starts with",
					"Ends with" => "Ends with"
				);

function filterColumn($alias, $filterNo)
{
	if($_POST["columns$filterNo"] == '0')
	{
		return false;
	}

	$col = $_POST["columns$filterNo"];
	$conditions = $_POST["conditions$filterNo"];
	$colValue = $_POST["colValue$filterNo"];

	switch ($conditions) {
		case 'Equals':
			$filter = $alias.'.'.$col.'= "'.$colValue.'" ';
			break;
		case 'Greater Than':
			$filter = $alias.'.'.$col.'>"'.$colValue.'" ';
			break;
		case 'Less Than':
			$filter = $alias.'.'.$col.'<"'.$colValue.'" ';
			break;
		case 'Similar':
			$filter = $alias.'.'.$col.' LIKE "%'.$colValue.'%" ';
			break;
		case 'Starts with':
			$filter = $alias.'.'.$col.' LIKE "'.$colValue.'%" ';
			break;
		case 'Ends with':
			$filter = $alias.'.'.$col.' LIKE "%'.$colValue.'" ';
			break;
		default:
			$filter = $alias.'.'.$col.'= "'.$colValue.'" ';
			break;
	}

	return $filter;
}


function check_report_details_data()
{
	if(!get_post('report_name')) {
		display_error( _("Report name cannot be empty.").$_POST['report_name']);
		set_focus('report_name');
		return false;
	}
	if(!get_post('primary_module')) {
		display_error( _("Primary module cannot be empty."));
		set_focus('primary_module');
		return false;
	}
	/*if(!get_post('secondary_module')) {
		$required = $newReport->check_secondaryModule(get_post('primary_module'));
		if($required)
		{
			display_error( _("Secondary module cannot be empty."));
			set_focus('secondary_module');
			return false;
		}
	}*/
	if(!get_post('description')) {
		display_error( _("Report description cannot be empty."));
		set_focus('description');
		return false;
	}
    return true;
}

function check_select_columns_data()
{
	if(!get_post('select_column')) {
		display_error( _("Column selection cannot be empty."));
		set_focus('report_name');
		return false;
	}
	if(!get_post('group_by')) {
		display_error( _("Group by cannot be empty."));
		set_focus('primary_module');
		return false;
	}
	if(!get_post('order_by')) {
		display_error( _("Order by cannot be empty."));
		set_focus('secondary_module');
		return false;
	}
    return true;
}



if(isset($_POST['primary_module']) && $_POST['primary_module'] != "")
{
	global $path_to_root;
	$primary_module = $_POST['primary_module'];
	include_once($path_to_root . "/reporting/reports/".$primary_module.".php");
}



start_form(true);
	getDisplayReportFrom($primary_modules, $columnTypes, $conditions);
end_form();


function tableColumnCSS($id, $value) {
	echo "<style type='text/css'>";

	echo "#".$id." { display : ".$value."; }";

	echo "</style>";
}

end_page();
?>



<script src="<?php echo $path_to_root . "/js/chosen/chosen.jquery.js"; ?>"></script>
<script src="<?php echo $path_to_root . "/js/add_new_report.js"; ?>"></script>
<script type="text/javascript">
	
	/* get get secondary module based on primary module */
	$(document).on("change", "#primary_module", function(){
	    
	    var primary_module = $('#primary_module').val(); 

	    $.ajax({
	        url:"includes/secondary_module.php",
	        method:"POST",
	        data:{prim_module:primary_module, action:"get_secondary_modules"},
	        success: function(data)
	        {
	            $("#secondary_module").empty().append(data);
	            $("#secondary_module").trigger("chosen:updated");
	        },
	        error: function(data)
	        {
	            alert(data);
	        }   
	    });
	});

	/* display input box on change columns */
	/*$(document).on("change", "#columns", function(){
	    
	    var column_name = $('#columns').val();
	    var primary_module = $('#primary_module').val(); 
	    var formData = $(this.form).serialize();
	    $.ajax({
	        url:"includes/secondary_module.php",
	        method:"POST",
	        data:{primary_module:primary_module, column:column_name, action:"get_inputbox_type"},
	        success: function(data)
	        {
	        	$('#inputBoxDiv').html(data);
	        },
	        error: function(data)
	        {
	            alert(data);
	        }   
	    });
	});
*/

function add(type) {

	//Create an input type dynamically.
	var element = document.createElement("input");

	//Assign different attributes to the element.
	element.setAttribute("type", type);
	element.setAttribute("value", type);
	element.setAttribute("name", type);


	var foo = document.getElementById("fooBar");

	//Append the element in page (in span).
	foo.appendChild(element);

}
</script>

<link href="<?php echo $path_to_root . "/js/chosen/chosen.css"; ?>" rel='stylesheet' type='text/css'>