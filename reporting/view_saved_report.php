<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root="..";
$page_security = 'SA_SAVED_REPORT';
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/includes/db_pager.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/reporting/includes/db/report_db.inc");
include_once($path_to_root . "/reporting/includes/ui/reports_ui.inc");

$js = "";

if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);

if ($use_date_picker)
	$js .= get_js_date_picker();

add_js_file('reports.js');

page(_($help_context = "View Saved Reports"), false, false, "", $js);

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('SearchReports')) 
{
	$Ajax->activate('reports_tbl');
} 
elseif (get_post('_report_id_changed')) 
{
	$disable = get_post('report_id') !== '';

	$Ajax->addDisable(true, 'reportAfterDate', $disable);
	$Ajax->addDisable(true, 'reportToDate', $disable);
	
	if ($disable) {
		$Ajax->addFocus(true, 'report_id');
	} else
		$Ajax->addFocus(true, 'reportAfterDate');

	$Ajax->activate('reports_tbl');
}

start_form();

start_table(TABLESTYLE_NOBORDER, "style=min-width:320px;width:60%;");
start_row();
ref_cells(_("Report #:"), 'report_id', '',null, '', true);

date_cells(_("from:"), 'reportAfterDate', '', null, -60);
date_cells(_("to:"), 'reportToDate');

end_row();


start_row();
	submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
end_row();
end_table(1);


function view_link($row) 
{
  	return viewer_link( _("View"), "reporting/view/view_report.php?report_id=". $row["report_id"], ICON_VIEW);
}


$sql = getSavedReports_Sql($_POST['report_id']);

$cols = array(
		_("Report Id#") => array('ord'=>''),
		_("Report Name") => array('ord'=>''),  
		_("Date") => array('ord'=>''),
		_("Description") => array('ord'=>''),
		array('insert'=>true, 'fun'=>'view_link')
);


$table =& new_db_pager('reports_tbl', $sql, $cols);

$table->width = "100%";

display_db_pager($table);

end_form();

end_page();
?>
