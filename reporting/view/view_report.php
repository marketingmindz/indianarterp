<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL,
	as published by the Free Software Foundation, either version 3
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root="../..";
$page_security = 'SA_VIEW_REPORT';
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/reporting/primary_modules.php");
include_once($path_to_root . "/reporting/print_report.php");

include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/reporting/includes/db/report_db.inc");
include_once($path_to_root . "/reporting/includes/ui/reports_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
page(_($help_context = "View Report"), true, false, "", $js);

if(isset($_POST['view_report']))
{
	$rep = get_report_details($_POST['report_id']);
	$query = htmlspecialchars_decode($rep['reportQuery']);
	$reportColumns = json_decode($rep['reportColumns'], true);
	print_report($rep['module'], $_POST['orientation'], $_POST['destination'], $reportColumns, $query, $rep['reportName']);
}

if (!isset($_GET['report_id']))
{
	die ("<br>" . _("This page must be called with a Report number to review."));
}

display_heading(_("Report No.") . " #" . $_GET['report_id']);

start_form(true);
hidden("report_id", $_GET['report_id']);
start_table(TABLESTYLE2, "id='create_report_table'");
start_row();
	label_cell("Destination:");

	echo "<td>";
	$sel = array(_("PDF/Printer"), "Excel");
	$def = 0;
	if (isset($def_print_destination) && $def_print_destination == 1)
		$def = 1;
	echo array_selector("destination", $def, $sel);
	echo "</td>";

	end_row();

	start_row();
	label_cell("Orientation:");

	echo "<td>";
	$sel = array(_("Landscape"),_("Portrait"));
	$def = 0;
	if (isset($def_print_orientation) && $def_print_orientation == 1)
		$def = 1;
	echo array_selector("orientation", $def, $sel);
	echo "</td>";

	end_row();

	start_row();
		echo "<td colspan=2>";
		submit_center('view_report', _("View Report"), "colspan=2", _('View Report'), true);
		echo "</td>";
	end_row();

	end_table();
end_form();

end_page();
?>
