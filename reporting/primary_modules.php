<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$primary_modules = array(

		/* Design & Development Module */

		"design_code" => array(
				"dbTable" => "design_code",
				"primaryKey" => "design_id",
				"label" => "Design Code",
				"sec_module"=>array('finishProduct', 'itemCategory'),
				"columns" => array(
								'design_image'=>'Design Image',
								'design_id'=>'Design ID', 
								'design_code'=>"Design Code",
								'product_name'=>"Product Name", 
								'cat_name'=>"Category", 
								'range_name'=>"Range", 
								'pro_name'=>"Product Type",
								'weight'=>"Size",
								'description'=>"Description"
							  ),
				"filter_columns" => array(
										'design_id'=>'Design ID', 
										'design_code'=>"Design Code", 
										'category_id'=>"Category", 
										'range_id'=>"Range",
										'product_type'=>"Product Type",
									),
						),

		"finishProduct" => array(
				"dbTable" => "finish_product",
				"primaryKey" => "finish_pro_id",
				"label" => "Finish Product",
				"columns" => array(
								'product_image'=>'Product Image',
								'finish_comp_code'=>'Finish Code', 
								'finish_product_name'=>"Product Name", 
								'range_name'=>"Range", 
								'cat_name'=>"Category", 
								'asb_weight'=>"Size",
								'description'=>"Description"
							  ),
				"filter_columns" => array(
										'finish_comp_code'=>'Finish Code', 
										'finish_product_name'=>"Product Name",
										'range_id'=>"Range Name", 
										'category_id'=>"Category Name"
									),
						),

		"itemCategory" => array(
				"dbTable" => "item_category",
				"primaryKey" => "category_id",
				"label" => "Category Master",
				"columns" => array(
								'cat_name'=>'Category Name', 
								'cat_reference'=>"Reference"
							  )
						),

		"itemRange" => array(
				"dbTable" => "item_range",
				"primaryKey" => "id",
				"label" => "Range Master",
				"columns" => array(
								'range_name'=>'Range Name', 
								'reference'=>"Reference"
							  )
						),

		"masterCreator" => array(
				"dbTable" => "master_creation",
				"primaryKey" => "master_id",
				"label" => "Consumable Category",
				"columns" => array(
								'master_name'=>'Master Name', 
								'master_description'=>"Description",
								'master_reference'=>"Reference"
							  )
						),

		
		/* Store Module */

		"consumableStock" => array(
				"dbTable" => "opening_stock_master",
				"primaryKey" => "open_stock_id",
				"label" => "Consumable Stock",
				"columns" => array(
								'location_name'=>'Location', 
								'work_center_name'=>"Work Center", 
								'consumable_name'=>"Consumable Name", 
								'master_name'=>"Consumable Category",
								'company_name'=>"Brand Name",
								'stock_qty'=>"Quantity", 
								'date'=>"Date"
							  ),
				"filter_columns" => array(
										'location_id'=>'Location', 
										'work_center_id'=>"Work Center", 
										'consumable_id'=>"Consumable Category", 
										'consumable_category'=>"Consumable Name",
										'company_id'=>"Brand Name",
										'stock_qty'=>"Quantity",
										'date'=>"Date"
									  ),
						),

		"stockTransaction" => array(
				"dbTable" => "stock_transaction",
				"primaryKey" => "trans_id",
				"label" => "Stock Transaction",
				"columns" => array(
								'location_name'=>'Location', 
								'work_center_name'=>"Work Center",
								'pro_team_name'=>"Production Team", 
								'master_name'=>"Consumable Category", 
								'consumable_name'=>"Consumable Name",
								'company_name'=>"Brand Name",
								'opening_stock'=>"Existing Stock",
								'closing_stock'=>"Closing Stock",
								'trans_quantity'=>"TXN Quantity",
								'unit'=>"Unit",
								'type'=>"Type",
								'date'=>"Date"
							  ),
				"filter_columns" => array(
										'location_id'=>'Location', 
										'work_center_id'=>"Work Center",
										'pro_team_id'=>"Production Team", 
										'consumable_category'=>"Consumable Category", 
										'consumable_id'=>"Consumable Name",
										'type'=>"Transaction Type",
										'date'=>"Date"
									  ),
						),

		"reorderLevelInquiry" => array(
				"primaryKey" => "open_stock_id",
				"label" => "Reorder Level Inquiry",
				"columns" => array(
								'location_name'=>'Location', 
								'work_center_name'=>"Work Center", 
								'consumable_name'=>"Consumable Name", 
								'master_name'=>"Consumable Category",
								'unit'=>"Unit",
								'level'=>"Reorder Level", 
								'stock_qty'=>"Quantity"
							  ),
				"filter_columns" => array(
										'location_id'=>'Location', 
										'work_center_id'=>"Work Center", 
										'consumable_id'=>"Consumable Category", 
										'consumable_category'=>"Consumable Name"
									  ),
						),

		
		/* Purchase Module */

		"consumablesReport" => array(
				"dbTable" => "consumable_master",
				"primaryKey" => "consumable_id",
				"label" => "Consumables",
				"columns" => array(
								'consumable_code'=>'Consumable Code', 
								'consumable_name'=>"Consumable Name", 
								'company_name'=>"Brand Name", 
								'master_name'=>"Consumable Category",
								'name'=>"Principal Type",
								'unit_id'=>"Unit",
								'weight'=>"Weight",
								'size'=>"Size",
								'cbm'=>"CBM",
								'gsm'=>"GSM"
							  ),
				"filter_columns" => array(
										'consumable_code'=>'Consumable Code', 
										'consumable_name'=>"Consumable Name", 
										'company_id'=>"Brand Name", 
										'master_id'=>"Consumable Category"
									  ),
						),

		"approvedPurchaseRequest" => array(
				"dbTable" => "purchase_request",
				"primaryKey" => "pr_id",
				"label" => "Approved Purchase Request",
				"columns" => array(
								'pr_no'=>'PR No', 
								'location_name'=>"Location", 
								'work_center_name'=>"Work Center", 
								'selection_name'=>"Status",
								'date'=>"Order Date"
							  ),
				"filter_columns" => array(
										'pr_no'=>'PR No', 
										'location_id'=>"Location", 
										'work_center_id'=>"Work Center", 
										'status'=>"Status",
										'date'=>"Order Date"
									  ),
						),

		"purchaseRequestInquiry" => array(
				"dbTable" => "purchase_request",
				"primaryKey" => "pr_id",
				"label" => "Purchase Request Inquiry",
				"columns" => array(
								'pr_no'=>'PR No', 
								'location_name'=>"Location", 
								'work_center_name'=>"Work Center", 
								'selection_name'=>"Status",
								'request_type'=>"Type",
								'date'=>"Order Date"
							  ),
				"filter_columns" => array(
										'pr_no'=>'PR No', 
										'location_id'=>"Location", 
										'work_center_id'=>"Work Center", 
										'status'=>"Status",
										'type'=>"Type",
										'date'=>"Order Date"
									  ),
						),

		"purchaseOrderInquiry" => array(
				"dbTable" => "purchase_request",
				"primaryKey" => "pr_id",
				"label" => "Purchase Order Inquiry",
				"columns" => array(
								'po_supp_rel'=>'PO No',
								'pr_id'=>'PR No',
								'supp_name'=>'Supplier Name',  
								'location_name'=>"Location", 
								'work_center_name'=>"Work Center", 
								'selection_name'=>"Status",
								'date'=>"Order Date"
							  ),
				"filter_columns" => array(
										'po_supp_rel'=>'PO No',
										'pr_id'=>'PR No',
										'supplier_id'=>'Supplier',  
										'location_id'=>"Location", 
										'work_center_id'=>"Work Center", 
										'status'=>"Status",
										'date'=>"Order Date"
									  ),
						),

		
		/* Account Module */

		"suppliers" => array(
				"dbTable" => "suppliers",
				"primaryKey" => "supplier_id",
				"label" => "Suppliers",
				"columns" => array(
								'supp_name'=>'Supplier Name', 
								'supp_ref'=>"Reference", 
								'address'=>"Mailing Address", 
								'supp_address'=>"Physical Address",
								'tin_no'=>"TIN No",
								'cin_no'=>"CIN No",
								'pan_no'=>"PAN No",
								'can_no'=>"CAN No",
								'st_no'=>"ST No:",
								'phone'=>"Phone",
								'phone2'=>"Secondary Phone",
								'fax'=>"Fax",
								'email'=>"Email",
								'notes'=>"Notes",
								'curr_code'=>"Currency"
							  ),
				"filter_columns" => array(
										'supplier_id'=>'Supplier Name',
									  ),
						),

		"supplierProducts" => array(
				"dbTable" => "supp_product",
				"primaryKey" => "product_id",
				"label" => "Supplier Products",
				"columns" => array(
								'supp_name'=>'Supplier Name', 
								'master_name'=>"Category", 
								'consumable_name'=>"Consumable Name", 
								'company_name'=>"Brand Name",
								'consumable_code'=>"Consumable Code"
							  ),
				"filter_columns" => array(
										'supplier_id'=>'Supplier Name',
										'catagory'=>"Category", 
										'sub_category'=>"Consumable Name", 
										'company_id'=>"Brand Name",
										'consumable_code'=>"Consumable Code"
									  ),
						),

		"itemReceive" => array(
				"dbTable" => "purchase_order_receive",
				"primaryKey" => "rec_id",
				"label" => "Purchase Order Receiveing",
				"columns" => array(
								'po_no'=>'#PO',
								'supp_name'=>'Supplier Name',
								'location_name'=>'Location', 
								'work_center_name'=>"Work Center", 
								'master_name'=>"Consumable Category", 
								'consumable_name'=>"Consumable Name",
								'company_name'=>"Brand",
								'ordered_quantity'=>"Ordered Qty",
								'received_quantity'=>"Received Qty",
								'ok_quantity'=>"Ok Qty",
								'rejected_quantity'=>"Rejected Qty",
								'unit'=>"Unit",
								'chalan_no'=>'Chalan No',
								'invoice_no'=>'Invoice No',
								'po_date'=>"Order Date",
								'receive_date'=>"Receive Date"
							  ),
				"filter_columns" => array(
										'po_no'=>'Purchase Order', 
										'supplier_id'=>'Supplier',
										'location_id'=>'Location',
										'work_center_id'=>"Work Center", 
										'master_id'=>"Consumable Category", 
										'consumable_id'=>"Consumable Name",
										'po_date'=>"Order Date",
										'receive_date'=>"Receive Date"
									  ),
						)

	);