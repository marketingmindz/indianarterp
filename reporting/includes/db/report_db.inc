<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//  code by rahul -  report details - - -- - 

function insert_reportDetails($query)
{
	global $db_connections;

	$date = dateTimeNow();
	$reportColumns = json_encode($_POST["select_column"]);
	
	// set current db prefix
	$cur_prefix = $db_connections[$_SESSION["wa_current_user"]->cur_con]['tbpref'];
	$query = str_replace(TB_PREF, $cur_prefix, $query);


	$sql = "INSERT INTO ".TB_PREF."reports(module, reportName, description, reportColumns, reportQuery, date) VALUES(";
	$sql .= db_escape($_POST['primary_module']) . "," .
		db_escape($_POST['report_name']) . "," .
		db_escape($_POST['description']) . ", '" .
		$reportColumns . "'," .
		db_escape($query) . "," .
		db_escape($date) .")";

	db_query($sql, "ERROR ### - Report not inserted");
	
	$pr_id = db_insert_id(); 
		 
}


function getSavedReports_Sql($report_id)
{
	$sql = "select report_id, reportName, date, description from ".TB_PREF."reports where ";

	if(isset($report_id))
		$sql .= " report_id LIKE ".db_escape('%'. $report_id . '%');
	else
	{
		$date_after = date2sql($_POST['reportAfterDate']);
		$date_before = date2sql($_POST['reportToDate']);

		$sql .= " date >= '$date_after'";
		$sql .= " AND date <= '$date_before'";

		$sql .= " GROUP BY report_id ORDER BY report_id DESC";
	}
	return $sql;
}

function get_report_details($report_id)
{
	$sql = "select module, reportName, reportQuery, reportColumns from ".TB_PREF."reports where report_id = ".db_escape($report_id);
	$result = db_query($sql, "Could not get report detilas.");
	return db_fetch($result);
}

