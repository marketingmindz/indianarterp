<?php

/* Design & Development Module */

function product_type_dropdown($name)
{
	echo desgin_product_type_list($name, $_POST["$name"], "Select Product Type");
}
function product_category_dropdown($name)
{
	echo desgin_categories_list($name, $_POST["$name"], "Select Category");
}

function product_range_dropdown($name)
{
	echo desgin_range_list($name, $_POST["$name"], "Select Range");
}


/* Store Module */

function location_list_dropdown($name)
{
	echo desgin_location_list($name, $_POST["$name"], "Select Location");
}
function workcenter_list_dropdown($name)
{
	echo work_center_list($name, $_POST["$name"], "Select Work Center");
}

function consumable_name_list_dropdown($name)
{
	echo stock_master_list($name, $_POST["$name"], "Select Consumable Category");
}

function consumable_category_list_dropdown($name)
{
	echo consumable_name_list($name, $_POST["$name"], "Select Consumable Name");
}

function company_name_list_dropdown($name)
{
	echo stock_company_list($name, $_POST["$name"], "Select Brand Name");
}


/* Account Module */

function supplier_name_list_dropdown($name)
{
	echo supplier_list($name, $_POST["$name"], "Select Supplier");
}

function purchase_request_status_list_dropdown($name)
{
	$sql = "SELECT * FROM ".TB_PREF."selection";
	$result = db_query($sql, "Purchase request status could not be retrieved.");

	echo '<span id="_'.$name.'_sel">';
	echo "<select name=$name >";
		echo "<option value = '-1'> -- Select Status -- </option>";
		while($row = db_fetch($result))
		{
			echo "<option value=".$row['selection_id']." >".$row['selection_name']."</option>";
		}
	echo "</select>";
}

function purchase_request_type_list_dropdown($name)
{
	$sql = "SELECT * FROM ".TB_PREF."purchase_request_type";
	$result = db_query($sql, "Purchase request type could not be retrieved.");

	echo '<span id="_'.$name.'_sel">';
	echo "<select name=$name >";
		echo "<option value = '-1'> -- Select Type -- </option>";
		while($row = db_fetch($result))
		{
			echo "<option value=".$row['request_type_id']." >".$row['request_type']."</option>";
		}
	echo "</select>";
}


function stock_transactionType_dropdown($name)
{
	echo stock_transactionType_list($name, $_POST["$name"]);
}

function productionTeam_list_dropdown($name)
{
	echo stock_production_list($name, $_POST["$name"]);
}

?>