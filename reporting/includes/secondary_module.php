<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root="../..";
include_once($path_to_root . "/reporting/includes/new_report_classes.inc");
include_once($path_to_root . "/includes/session.inc");

if(isset($_REQUEST['action']))
{
	$action = $_POST['action'];
	$action();
}

/* get secondary module based on primary module */
function get_secondary_modules()
{ 
	global $mysqli;
	$primary_module = $_REQUEST['prim_module'];

	$newRep = new addNewReport;

	foreach ($newRep->primary_modules as $module) 
	{
		if($module['field_name'] == $primary_module)
		{
			foreach ($module['rel_modules'] as $sec_module) 
			{
				// get module name	
				$field_name = $newRep->get_sub_module_label($sec_module);	
				echo "<option value='".$sec_module."' ".$selected.">".$field_name."</option>";
			}
		}
	}
}




function select_box()
{
	$newRep = new addNewReport;
	
	$column = $_POST['column'];
	$primary_module = $_POST['primary_module'];
	if($primary_module == "design_code" && $column == "item_range.range_name")
	{
		product_range_dropdown();
	}
	if($primary_module == "design_code" && $column == "item_category.cat_name")
	{
		product_category_dropdown();
	}

}



/* display input box on change columns */
function get_inputbox_type()
{ 
	global $mysqli;
	$req_column = $_REQUEST['column'];

	$newRep = new addNewReport;

	foreach ($newRep->columns as $column) 
	{
		foreach ($column as $sub_column) 
		{
			if($sub_column['field_name'] == $req_column)
			{
				$inputtype = $sub_column['inputtype'];

				switch ($inputtype) {
					case 'textbox':
						echo '<input type="text" name="conditionValue" value="" />';
						break;

					case 'textarea':
						echo '<textarea name="conditionValue"> </textarea>';
						break;

					case 'date':
						echo '<input type="date" name="conditionValue" value="" />';
						break;

					case 'dropdown':
						select_box();
						break;
					
					default:
						# code...
						break;
				}
			}
		}
	}
}

function product_range_dropdown(){
	$sql = "select * from ".TB_PREF."item_range";
	$result = db_query($sql, "Could not fetch Item Range");
	echo "<select name='conditionValue'>";
	while($ranges = db_fetch($result))
	{
		echo "<option value='".$ranges['range_name']."'>".$ranges['range_name']."</option>";
	}
	echo "</select>";
}

function product_category_dropdown(){
	$sql = "select * from ".TB_PREF."item_category";
	$result = db_query($sql, "Could not fetch Item Category");
	echo "<select name='conditionValue'>";
	while($ranges = db_fetch($result))
	{
		echo "<option value='".$ranges['cat_name']."'>".$ranges['cat_name']."</option>";
	}
	echo "</select>";
}



?>