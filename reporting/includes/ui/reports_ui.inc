<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// Reporting module - multiple select list
function multiple_modules_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $primary_modules)
{
	
	echo '<div class="multiselect">
           <div class="selectBox" >';

	echo '<select autocomplete="off" class="combo" title="Select Secondary Modules" id="secondary_module"  style="width:200px;"  >'; 
						echo "<option value=''>Select...</option>";

					$prim_module = $_POST['primary_module'];
					foreach ($primary_modules as $module) 
					{
						if($module['field_name'] == $prim_module)
						{
							foreach ($module['rel_modules'] as $sec_module) 
							{
								// get module name	
								//$field_name = $get_sub_module_label($sec_module);

								$selected = "";
								if(in_array($sec_module , $_POST['secondary_module']))
								{
									$selected = "selected";
								}

								echo "<option value='".$sec_module."' ".$selected.">".$field_name."</option>";
							}
						}
					}
			echo '</select>';

			echo '<div id="sec_module_alert" style="color:red;">You can select maximum 2 modules.</div>';


	if(isset($_POST['select_all']))
	{
		$checked = 'checked="checked"';
	}
	echo '<div class="overSelect"></div>
        </div>
        <div class="checkboxes"> ';
	$result = db_query($sql);
	foreach ($primary_modules as $module) 
					{
		$category_id = $row['category_id'];
		$checked = "";	
		if(in_array($category_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		
		echo '<label><input type="hidden" id="'.$category_id.'" value="'.$row['cat_name'].'" >
		<input type="checkbox" class="check" value="'.$category_id.'" '.$checked.' name="category_id['.$category_id.']" />'.$row['cat_name'].'</label>';
	}	

    echo ' </div>
    </div>';
}
function multiple_modules_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $primary_modules)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_modules_list($name, $selected_id, $spec_opt, $submit_on_change, $primary_modules);
	echo "</td>\n";
}

function multiple_modules_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param, $primary_modules)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_modules_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change, $primary_modules);
	echo "</tr>\n";
}

	


function getDisplayReportFrom($primary_modules, $columnTypes, $conditions)
{
	global $Ajax;
	div_start('create_report');
	start_table(TABLESTYLE2, "id='create_report_table'");

		start_row();
		echo "<td>";

			start_table();
			table_section_title(_("Create Report"));
			echo "<td>";
				report_details_form($primary_modules);
			echo "</td>";
			end_table();

		echo "</td>";

		echo "<td id='selectColumn'>";
			div_start('select_column_form');

			if(isset($_POST['save_details']) OR isset($_POST['sectionFirst']))
			{
				$Ajax->activate("select_column_form");
				tableColumnCSS("selectColumn", "block");

				start_table();
				table_section_title(_("Select Column"));
				echo "<td>";

					select_column_form($primary_modules);
					
				echo "</td>";
				end_table();
			}
			else{
				tableColumnCSS("selectColumn", "none");
			}

			div_end();	
		echo "</td>";

		echo "<td id='filer_form_columns'>";

			div_start('add_filters_form');
			
			if(isset($_POST['sectionSecond']) OR isset($_POST['save_report']))
			{
				$Ajax->activate("add_filters_form");
				tableColumnCSS("filer_form_columns", "block");

				start_table();
				table_section_title(_("Filters"));
				echo "<td>";
					start_table();
					
					$nextFilter = find_submit('nextFilter');
					if ($nextFilter != -1)
						$_POST['total_filters'] += 1;

					$cancelFilter = find_submit('cancelFilter');
					if ($cancelFilter != -1)
						$_POST['total_filters'] -= 1;

					filters_form($primary_modules, $columnTypes, $conditions, 1);

					for ($i=2; $i <= $_POST['total_filters']; $i++) 
					{ 
						if ($cancelFilter != -1 && $cancelFilter == $i)
							continue;

						div_start("filterDiv".$i);
						$Ajax->activate("add_filters_form");
						filters_form($primary_modules, $columnTypes, $conditions, $i);
						div_end();
					}

					hidden("sectionThird","3");

					start_row();
					label_cell("Destination:");

					echo "<td>";
					$sel = array(_("PDF/Printer"), "Excel");
					$def = 0;
					if (isset($def_print_destination) && $def_print_destination == 1)
						$def = 1;
					echo array_selector("destination", $def, $sel);
					echo "</td>";

					end_row();

					start_row();
					label_cell("Orientation:");

					echo "<td>";
					$sel = array( _("Landscape"), _("Portrait"));
					$def = 0;
					if (isset($def_print_orientation) && $def_print_orientation == 1)
						$def = 1;
					echo array_selector("orientation", $def, $sel);
					echo "</td>";

					end_row();

					start_row();
						submit_cells('save_report', _("Save Report"), "colspan=2", _('Save Report'), true);
					end_row();

					end_table();

				echo "</td>";
				end_table();
			}
			else{
				tableColumnCSS("filer_form_columns", "none");
			}

			div_end();	
		echo "</td>";
	
		end_row();
	end_table();
	div_end();
}

function report_details_form($primary_modules)
{
	div_start("report_details_div");
	start_table();
	text_row(_("Report Name:"), 'report_name', $_POST['report_name'], 31, null, '','','','id="report_name"');
	
	start_row();
	echo '<td class="label">Primary Module: </td>';
	echo '<td>';

		echo '<select id="primary_module" name="primary_module" style="width:200px;" >'; 
			echo "<option value=''>Select Primary Module</option>";
				foreach ($primary_modules as $moduleKey => $module) 
				{
					$selected = "";
					if($_POST['primary_module'] == $moduleKey){
						$selected = "selected";
					}
					echo "<option value='".$moduleKey."' ".$selected.">".$module['label']."</option>";
				}
		echo '</select>';

	echo '</td>';
	end_row();

	//multiple_modules_list_row(_("Secondary Module:"), 'category_id', null, _('----Select Category---'), $primary_modules);

	textarea_row(_('Description:'), 'description', null, 26, 3);

	start_row();
		submit_cells('save_details', _("Save & Next"), "colspan=2", _('Save Report Details'), true);
	end_row();

	end_table();

	hidden("sectionFirst","1");
	div_end();
}

function select_column_form($primary_modules)
{	
	global $Ajax;
	

	if(isset($_POST['save_details']) OR isset($_POST['sectionFirst']))
	{
		echo "<pre>";

		if(check_report_details_data())
		{		
			// selected primary and secondary modules
			$selected_module = array();
			
			array_push($selected_module, $_POST['primary_module']);
			
			if(isset($_POST['secondary_module'])){
				$selected_module = array_merge($selected_module, $_POST['secondary_module']);
			}

			start_table();
			
			start_row();
			echo '<td class="label">Select Columns: </td>';
			echo '<td>';

				echo '<div class="multiselect">
	           		<div class="selectBox" >';

				echo '<select autocomplete="off" class="combo" title="Select Report Columns" id="select_column"  style="width:200px;" >'; 
						echo "<option value=''>Select...</option>";

						foreach($selected_module as $sel_module)
						{

							echo "<optgroup label='".$primary_modules["$sel_module"]['label']."'>";

							foreach ($primary_modules["$sel_module"]['columns'] as $column_name => $column_value) 
							{
								if(in_array($column_name, $_POST['select_column'])){
									$selected = "selected";
								}
								else{
									$selected = "";
								}
								echo "<option value='".$column_name."' ".$selected.">".$column_value."</option>";
							}
						}
				echo '</select>';

				if(isset($_POST['select_all']))
				{
					$checked = 'checked="checked"';
				}
				echo '<div class="overSelect"></div>
		        	</div>
		       
		        <div class="checkboxes"> <label><input type="checkbox" id="select_all" name="select_all" '.$checked.' /> Select all</label>';
			
				foreach($selected_module as $sel_module)
				{
					echo "<label class='optionGroup'>".$primary_modules["$sel_module"]['label']."</label>";
					
					foreach ($primary_modules["$sel_module"]['columns'] as $column_name => $column_value) 
					{
						if(in_array($column_name, $_POST['select_column'])){
							$checked = "checked";
						} else {
							$checked = "";
						}
					
					echo '<label>
							<input type="hidden" id="'.$column_name.'" value="'.$column_value.'" >
							<input type="checkbox" class="check" value="'.$column_name.'" '.$checked.' name="select_column['.$column_name.']" />'.$column_value.'</label>';
					}
				}	

			    echo ' </div>
			    </div>';


			echo '</td>';
			end_row();

			start_row();

			echo '<td class="label">Group By </td>';
			echo '<td class="label">Order By </td>';
			
			end_row();
			
			start_row();

			echo '<td>';

				echo '<select name="group_by" style="width:200px;" class="chosen-select form-control" data-placeholder="select columns">'; 
						foreach($selected_module as $sel_module)
						{
							echo "<optgroup label='".$primary_modules["$sel_module"]['label']."'>";

							foreach ($primary_modules["$sel_module"]['columns'] as $column_name => $column_value) 
							{								
								// $field_name = $sel_module.".".$column_name;
								if($column_name == $_POST['group_by']){
									$selected = "selected";
								}
								else{
									$selected = "";
								}
								echo "<option value='".$column_name."' ".$selected.">".$column_value."</option>";
							}
						}
				echo '</select>';

			echo '</td>';
			
			echo '<td>';
				$checked1 = "checked";
				if($_POST['order_by'] == 'asc') {
					$checked1 = "checked";
					$checked2 = "";
				}
				if($_POST['order_by'] == 'desc') {
					$checked1 = "";
					$checked2 = "checked";
				}

				echo '<input type="radio" name="order_by" value="asc" '.$checked1.' />Ascending';
				echo '<input type="radio" name="order_by" value="desc" '.$checked2.' />Descending';
			
			echo '</td>';
			end_row();

			
			start_row();
			submit_cells('save_columns', _("Save & Next"), "colspan=2", _('Save Report Columns'), true);
			end_row();

			end_table();
			hidden("sectionSecond","2");
		}
	}
}

function filters_form($primary_modules, $columnTypes, $conditions, $filterNO)
{
	global $Ajax;

	$PrimModule = $_POST['primary_module'];

	if(isset($_POST['sectionSecond']))
	{
		if(check_select_columns_data())
		{
			// selected primary and secondary modules
			$selected_module = array();
			
			array_push($selected_module, $PrimModule);
			
			if(isset($_POST['secondary_module']) && !empty($_POST['secondary_module'])){
				$selected_module = array_merge($selected_module, $_POST['secondary_module']);
			}
			
			start_row();
			echo "<td class='tableheader' colspan=2>Filter: $filterNO </td>";
			end_row();

			start_row();
			echo '<td class="label">Columns: </td>';
			echo '<td>';

				$columns = array();
				foreach($selected_module as $sel_module)
				{
					$columns = array_merge($columns, $primary_modules["$sel_module"]['filter_columns']);
				}

				echo filter_columns_list("columns".$filterNO, @$_POST['columns'], "-Select-", $columns, true);

			echo '</td>';
			end_row();

			start_row();
			label_cell("Conditions", "class='label'");
			echo '<td>';
				$items = $conditions;

				echo array_selector("conditions".$filterNO, $_POST['conditions'], $items, $arrayName = array('select_submit' => true ));

			echo '</td>';
			end_row();

			
			start_row();

			echo '<td class="label">Value</td>';
			echo '<td>';
				div_start("inputBoxDiv".$filterNO);

					if(isset($_POST["columns$filterNO"]))
					{
						$Ajax->activate("inputBoxDiv".$filterNO);
						$col = $_POST["columns$filterNO"];
						$inputType = $columnTypes["$PrimModule"]["$col"][1];

						switch ($inputType) {
							case 'text':
								$value = isset($_POST["colValue$filterNO"])?$_POST["colValue$filterNO"] : "";
								echo "<input type='text' name='colValue".$filterNO."' value='".$value."'>";
								break;

							case 'radio':
								# code...
								break;

							case 'date':
								$value = isset($_POST["colValue$filterNO"])?$_POST["colValue$filterNO"] : "";
								echo "<input type='date' name='colValue".$filterNO."' value='".$value."'>";
								break;

							case 'dropdown':
								$methodName = $columnTypes["$PrimModule"]["$col"]["method"];
								echo $methodName("colValue".$filterNO);
								break;
							
							default:
								# code...
								break;
						}
					}
					else
						echo 'Choose a column';

				div_end();

			echo '</td>';
			end_row();

			start_row();
			button_cell("nextFilter$filterNO", _("Add New Filter"), _('Add new field for Filter'), false);

			if($filterNO == $_POST['total_filters'] && $filterNO != 1)
				button_cell("cancelFilter$filterNO", _("Cancel This Filter"), _('Cancel This Filter'), false);
			
			end_row();
			
			if($filterNO == $_POST['total_filters'])
				hidden("total_filters", $filterNO);	
			elseif (!isset($_POST['total_filters'])) {
					hidden("total_filters", $filterNO);
				}		
		}
	}		
}

?>