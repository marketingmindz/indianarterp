<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function product_sheet_types()
{
	echo '<select autocomplete="off" class="combo" name="product_sheet_type" id="product_sheet_type" style="width:300px;">'; 
		echo "<option value='-1'>--Select Sheet Type--</option>";
		echo "<option value='1'>Single Product Sheet</option>";
		echo "<option value='2'>Double Product Sheet</option>";
	echo '</select>';
}

function finish_product_single_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_arr)
{
	echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 
	
		echo "<option value=''>".$spec_opt."</option>";

		foreach ($product_arr as $product) 
		{	
			$selected = "";
			echo "<option value='".$product['finish_pro_id']."' ".$selected.">".$product['finish_product_name']."</option>";
		}
	echo '</select>';
}

// Reporting module - multiple select list
function finish_product_multiple_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $product_arr)
{
	echo '<div class="multiselect">';
    	echo '<div class="selectBox" onclick="showCheckboxes()">';

			echo '<select autocomplete="off" class="combo" name="'.$name.'" id="'.$name.'" style="width:300px;">'; 
			
				echo "<option value=''>".$spec_opt."</option>";

				foreach ($product_arr as $product) 
				{	
					$selected = "";
					echo "<option value='".$product['finish_pro_id']."' ".$selected.">".$product['finish_product_name']."</option>";
				}
			echo '</select>';

			//echo '<div id="sec_module_alert" style="color:red;">You can select maximum 2 products.</div>';

			echo '<div class="overSelect"></div>';
		echo '</div>';
    
    	echo '<div class="checkboxes" style="width: 35%;" >';
			
			foreach ($product_arr as $prod)
			{
				echo '<label>
							<input type="hidden" id="'.$prod['finish_pro_id'].'" value="'.$prod['finish_pro_id'].'" >
							<input type="checkbox" id="'.$prod['finish_pro_id'].'"  class="check" name="'.$name.'" value="'.$prod['finish_pro_id'].'" />'.$prod['finish_product_name'].
					'</label>';
			}	
    	echo '</div>';

    echo '</div>';
}


function showProductSelectionForm($primary_modules)
{
	div_start("report_details_div");
	start_table(TABLESTYLE_NOBORDER, "style=width:50%;");
	
	start_row();
		echo '<td class="label">Product Sheet Type : </td>';
		echo '<td>';
			product_sheet_types();
		echo '</td>';
	end_row();
	/**** shubham code start for filter *********/
	design_category_list_row(_("Category:"), 'category_id', null, _('----Select---'));
	sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
	echo '<tr><td class="label">Product Design Code</td>';
	echo '<td><span id="_design_id_sel"><select autocomplete="off" name="design_id" class="combo" title="" id="sub_design" _last="0"><option selected="selected" value="-1">----Select Code---</option></select></span></td></tr>';

	echo '<tr id="finish_code_list"><td class="label">Product Finish Code</td>';
	echo '<td><span id="_finish_code_id_sel"><select autocomplete="off" name="finish_code_id" class="combo" title="" id="packaging_finish_code" _last="0"><option selected="selected" value="-1">----Select Code---</option></select></span></td></tr>';
	/**** shubham code end for filter *********/


	start_row('id="print_button"');
		submit_cells('print_sheet', _("Print"), "colspan=2", _('Print Product Sheet'), true);
	//echo '<input type="submit" name="print_sheet" />';
	end_row();

	end_table();

	div_end();

	/**** shubham code start for double sheet **/

	div_start("report_details_div2");
	start_table(TABLESTYLE_NOBORDER, "style=width:50%;");
	echo '<h3>Select Second Product</h3>';
	design_category_list_row(_("Category:"), 'category_id', null, _('----Select---'));
	sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
	design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
	echo '<tr><td class="label">Product Design Code</td>';
	echo '<td><span id="_design_id_sel"><select autocomplete="off" name="design_id" class="combo" title="" id="sub_design" _last="0"><option selected="selected" value="-1">----Select Code---</option></select></span></td></tr>';

	echo '<tr id="finish_code_list"><td class="label">Product Finish Code</td>';
	echo '<td><span id="_finish_code_id_sel"><select autocomplete="off" name="finish_code_id2" class="combo" title="" id="packaging_finish_code" _last="0"><option selected="selected" value="-1">----Select Code---</option></select></span></td></tr>';

	start_row('id="print_button"');
		submit_cells('print_sheet', _("Print"), "colspan=2", _('Print Product Sheet'), true);
	end_row();

	end_table();

	div_end();
	/**** shubham code end for double sheet **/

}

?>