<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
$page_security = 'SA_PURCHASEREQUESTUSINGREORDER';

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/purchase_request_class.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_request_using_reorder_db.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_request_using_reorder_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
//include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");

//include_once($path_to_root . "/reporting/includes/reporting.inc");

set_page_security( @$_SESSION['PO']->trans_type,
	array(	ST_PURCHREQUEST => 'SA_PURCHASEREQUESTUSINGREORDER')
);

$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Purchase request"), false, false, "", $js);
global 	$Ajax;
$Ajax->activate('details');
div_start('details');
function handle_purchase_request()
{

	$header_request = array();
	$header_request['date'] = trim($_POST['date']);
	$header_request['location_id'] = trim($_POST['location_id']);
	$header_request['work_center_id'] = trim($_POST['work_center_id']);
	$trans_no = new_purchase_request($header_request);
	
	meta_forward($_SERVER['PHP_SELF']);
}


if(isset($_POST['AddProcess']))
{
	handle_purchase_request();
}
	
	start_form(true);
	display_hearder($_SESSION['PUSH']);
	
      submit_center_last('AddProcess', _("Generate Purchase Request"), '', 'default');

	end_form();
div_end();
		
end_page();

?>

<style>
button#AddProcess {
	align-content: center;
	margin-left: 500px;
}

</style>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">



$(document).ready(function(e) {
var pr_no = '';
	$.ajax({
		url: "purchase_request_no.php",
		method: "POST",
		data: { r_id : pr_no},
		success: function(data){
				 $('#pr_no').val(data);
			}
	});
	return false;
});
$(document).on('change','#slc_location',function(){
		//alert("data is synchronization successfully .... ")
		var slc_location = $(this).val();
		$.ajax({
			url: "manage/slc_location_calling.php",
			method: "POST",
            data: { id : slc_location},
			success: function(data){
					var select_val = $('#slc_work_center');
                    select_val.empty().append(data);
				}
		});
		return false;
	});


$(document).on('change','#sub_master',function(){
//alert("data is synchronization successfully .... ");
	var cons_cat_id = $(this).val();
	$.ajax({
		url: "manage/unit_calling.php",
		method: "POST",
		data: { id : cons_cat_id},
		success: function(data){
				var select_val = $('#unit_master');
				select_val.empty().append(data);
			}
	});
	return false;
});
</script>
