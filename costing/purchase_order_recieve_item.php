<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";
$page_security = 'SA_PUCHORDER';
//include_once($path_to_root . "/purchasing/includes/po_class.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/ui/purchase_order_receive_item_ui.inc");
include_once($path_to_root . "/purchasing/includes/db/purchase_order_receive_item_db.inc");
/*include_once($path_to_root . "/purchasing/includes/db/suppliers_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");*/
set_page_security( @$_SESSION['PO']->trans_type,
	array(	ST_PURCHORDER => 'SA_PURCHASEORDER')
);


$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();



page(_($help_context = "Purchase Order"), false, false, "", $js);


if(isset($_POST['ProcessReceiveItem']))
{
	$trans_no = receive_ordered_item($_POST);
	meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
}

if (isset($_GET['AddedID'])) {
	$order_no = $_GET['AddedID'];
	$trans_type = ST_PURCHORDER;
	display_notification_centered(sprintf( _("Items have been received."),$order_no));
	//display_note(get_trans_view_str($trans_type, $order_no, _("&View this Purchase Order")), 0, 1);
	//submenu_print(_("&Print This Order"), ST_SALESORDER, $order_no, 'prtopt');
	//submenu_print(_("&Email This Order"), ST_SALESORDER, $order_no, null, 1);
	submenu_option(_("Receive more items from another purchase order"),	"/purchasing/purchase_order_maintenance.php");
	//submenu_option(_("Select an outstanding Purchase request"),	"/purchasing/purchase_request_entry.php");
	display_footer_exit();

}
else
{
	start_form();
	
		display_purchase_order_header();
		echo "<br>";

		display_order_items();
		echo "<br>";
		div_start('controls', 'items_table');
		
		submit_center('ProcessReceiveItem', 'Process Receive Item', true, false, 'process_receive_item');
		div_end();
		//---------------------------------------------------------------------------------------------------
		div_end();
	end_form();
}
end_page();
?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../js/jquery/jquery-ui.min.js"></script>

<script type="text/javascript">

$(".received_quantity").blur(function(){
   var id = $(this).attr('id');
   var arr = id.split('_');	
	var received_quantity = $('#'+id).val();
	var ordered_quantity = $('#ordered_quantity_'+arr[2]).val();
	if(parseInt(received_quantity) > parseInt(ordered_quantity)){
		alert("Received Quantity Should Be Less Then Ordered Quantity");
		$('#'+id).val('');
		$('#'+id).focus();
	}
	 
});

$(".ok_quantity").blur(function(){
   var id = $(this).attr('id');
   var arr = id.split('_');	
	var ok_quantity = $('#'+id).val();
	var received_quantity = $('#received_quantity_'+arr[2]).val();
	if(parseInt(ok_quantity) > parseInt(received_quantity)){
		alert("OK quantity Should Be Less Then received Quantity");
		$('#'+id).val('');
		$('#'+id).focus();
	}
	else
	{
		if(parseInt(received_quantity) != '' && parseInt(ok_quantity) != '')
		{
		   var rejected_quantity = parseInt(received_quantity) -  parseInt(ok_quantity);
		   if(!isNaN(rejected_quantity))
		   {
			   $('#rejected_quantity_'+arr[2]).val(rejected_quantity);
		   }
		}
	}
	 
});


</script>