<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PRICE_LIST';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/ui/view_reference_no_price_ui.inc");

?>

<style>
#popup_image { display: none; text-align: center; background: #CCC; width: 60%; z-index: 99999; top: 0px; position: absolute; height: auto%;  }
#popup_image img { width: auto; height: auto; border: 7px #EEE9E5 solid; margin-top: 40px; margin-bottom: 40px; opacity: 1;}

.photo { min-width: 100px; cursor: pointer;}
.sno { min-width: 40px;}

table#product_table input {
    border: 0px;
    width: 100%;
}
#lock_popup
{    top: 150px;
    left: 33%;
    display: none;
    position: absolute;
    font-size: 13px;
    max-width: 80%;
    min-width: 400px;
    min-height: 150px;
    margin: auto;
    background: #B4C6D4;
    border: 16px solid #EAE7E7;
    border-radius: 5px;
}
#lock { display:block; background:#CCC; border:1px #000000 solid; padding:5px 10px; }

#controls { text-align:center; display:block; margin:auto; width:90%; height:70px; padding-top:50px;}
#close
{
	cursor:pointer; font-size:15px; margin:50px 25px;border:#FFF 3px solid;     padding: 2px 8px; color:#FFF;	
}
#close a
{
	text-decoration:none;color: #FFF;
}
#no
{
	cursor:pointer; font-size:15px;  margin:50px 25px; border:#FFF 3px solid;      padding: 2px 8px; color:#FFF;	
}
.fa-side {
    display: none;
	position:absolute;
}
.fa-content {
    width: 100% !important;
	float:none !important;
}
tr.tableheader td {
    white-space: nowrap  !important;
	text-align:left;
}
table td {
	
}
#display_costing { width:auto; }
table td, table td * {
    vertical-align: top !important;
}
#inner_div { overflow-y:scroll; overflow-x:hidden; }


#inner_div2 { overflow-x:scroll; margin-bottom: -16px; }
#inner_table { overflow-y:scroll; max-height:500px;  } 

::-webkit-scrollbar {
    width: 12px;
}
 
::-webkit-scrollbar-track {
    /*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); */
    border-radius: 10px;
}
 
::-webkit-scrollbar-thumb {
    border-radius: 6px;
    -webkit-box-shadow:inset 0 0 18px #006699; 
}
</style>

<?php
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Price List"), @$_REQUEST['popup'], false, "", $js);

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");


if(isset($_GET['reference_id']))
{	
	$_POST['ref_id'] = $_GET['reference_id'];
	$_POST['version'] = $_GET['version'];
}

display_costing_details($_GET['reference_id'],$_GET['version']);
end_page(@$_REQUEST['popup']);


?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="edit_locked_cost.js"></script>

<script>
$(function () {
    $('#inner_table').on('scroll', function (e) {
        $('#inner_div').scrollLeft($('#inner_table').scrollLeft());
    }); 
    $('#inner_div').on('scroll', function (e) {
        $('#inner_table').scrollLeft($('#inner_div').scrollLeft());
    });
});
</script>
<script>
$("tr.finish_product :input").attr("readonly", true);

</script>