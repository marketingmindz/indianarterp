<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SAVED_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/saved_consumable_costing_ui.inc");
include_once($path_to_root . "/includes/ui.inc");
?>
<style>


#freeze_costing_popup
{
	 top: 150px;    left: 33%;
	display:none;position:absolute;max-width:80%;
	max-width:400px;	min-height:150px;  padding:0px 50px 10px; 
	margin:auto;	background:#EAE7E7;
	border:3px solid #000;
	border-radius:5px;
}
#close
{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}
#close a
{	text-decoration:none;color: #FFF;
}

#no
{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}


#cancel_costing_popup
{	 top: 150px;    left: 33%;
	display:none;position:absolute;max-width:80%;
	max-width:400px;	min-height:150px; padding:0px; 50px 10px;
	margin:auto;	background:#F36;
	border:3px solid #000;
	border-radius:5px;
}
#close_cancel
{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}
#close_cancel a
{	text-decoration:none;color: #FFF;
}

#no_cancel
{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}
</style>


<?php
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Saved Consumable Costs"), @$_REQUEST['popup'], false, "", $js);


function check_form_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Category cannot be empty."));
		set_focus('category_id');
		return false;
	}
	if(get_post('design_id') == -1) {
		display_error( _("Select Design Code."));
		set_focus('sub_design');
		return false;
	}
	if(get_post('finish_code_id') == -1) {
		display_error( _("Select Finish Code.."));
		set_focus('packaging_finish_code');
		return false;
	}
    return true;	
}

start_form(true);
	//display_header($_SESSION['DCODE']);
	div_start("costing_reference_list","style='min-height:400px;'");
		global $Ajax;
		$Ajax->activate('costing_reference_list');
	
			display_costing_reference_list();	

	div_end();
end_form();
end_page();

echo "<div id='freeze_costing_popup'>
			<div id='freeze_costing_text'></div><div style='margin:auto; width:90px;'>		
				<span id='close'><a href='' id='redirect_url'>YES</a></span><span id='no'>NO</span>
			</div>
			
		  </div>";

echo "<div id='cancel_costing_popup'>
			<div id='cancel_costing_text'></div><div style='margin:auto; width:90px;'>		
				<span id='close_cancel'><a href='' id='cancel_costing_url'>YES</a></span><span id='no_cancel'>NO</span>
			</div>
			
		  </div>";
?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="saved_consumable_costing.js"></script>
<style>
#costing_details table
{
	font-size:20px !important;
	color:#06F;
	font-weight:bold;
	margin-top:25px;
	margin-bottom:35px;
}
#costing_details td
{
	font-size:14px !important;
}
.button
{
	height:20px;
	min-width:70px;
	margin:20px;
	padding:0px 5px;
	border:1px solid #000;
}
</style>