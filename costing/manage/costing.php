<?php /**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/costing_ui.inc");
include($path_to_root . "/costing/includes/wood_costing_class.inc");
include($path_to_root . "/costing/includes/labour_costing_class.inc");
?>

<style>
	#design_consumables, #finish_consumables, #fabric_consumables { display:none; }
	#proceed {  display:block; cursor:pointer; text-align:center; font-weight:600; font-size:14px; height:20px; width:70px; background-color:#7D9AB0; border:#060F16 1px solid; border-radius:3px; color: #fff;}
	#cancel {  cursor:pointer; text-align:center; font-weight:bold; font-size:14px; height:20px; width:70px; background-color:#999; border:#000 2px solid; border-radius:5px; }
	#GetFinishProduct {     margin: 20px; cursor:pointer; text-align:center; font-weight:bold; font-size:14px; height:20px; width:70px; background-color:#999; border:#000 2px solid; border-radius:5px; }

	#popup_div {     top: 150px;    left: 33%;     display:none;position:absolute
	;max-width:80%;     min-width:400px;    min-height:150px;     margin:auto;
	background:#FFF;     border:3px solid #000;     border-radius:5px; }

	#save_list
	{	display:block; width:150px; cursor:pointer; height:22px; border: #000 2px solid; background-color:#CCC; color: #09F; font-size:18px;
	}
	#save_list a
	{	text-decoration:none;color: #09F;
	}

	#save_list_popup {     top: 950px;left: 33%;display: none;position: absolute;     font-size: 13px;
		max-width: 80%;     min-width: 400px;     min-height: 150px;     margin: auto;     background:#B4C6D4;     border: 16px solid #EAE7E7;     border-radius: 5px; }

	#close
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}
	#close a
	{	text-decoration:none;color: #FFF;
	}

	#no
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}

	#popup { height:auto; min-height:100px; }
	#popup-controls { height:auto; min-height:100px; }

	#finish_products
	{
		max-height:1000px; 
		overflow-y: scroll;
		overflow-x:hidden;
	}
	.finish_link
	{
		text-decoration:underline;
		color:#093;
		cursor:pointer;
	}

	.multiselect {
	    width: 200px;
		    
	} .selectBox {     position: relative; } .selectBox select {     width: 100%;
	font-weight: bold; } .overSelect {     position: absolute;     left: 0; right:
	0; top: 0; bottom: 0; } #checkboxes {     display: none;     width: 200px;
	border: 1px #dadada solid;     position: absolute; z-index: 999999999;
	background-color: #fff; max-height:300px; overflow:scroll; overflow-x:hidden;
	overflow-wrap: break-word;

	}
	#checkboxes label {
	    display: block;
	}
	#checkboxes label:hover {
	    background-color: #1e90ff;
	}





	#freeze_costing_popup
	{
		 top: 0px;    left: 33%;	display:none;position:absolute;max-width:80%;	max-width:400px;	min-height:150px;  padding:0px 50px 10px; 	margin:auto;	background:#EAE7E7;
		border:3px solid #000;	border-radius:5px;}
	#close
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}
	#close a
	{	text-decoration:none;color: #FFF;
	}

	#no
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}


	#cancel_costing_popup
	{	 top: 150px;    left: 33%;	display:none;position:absolute;max-width:80%;	max-width:400px;	min-height:150px; padding:0px; 50px 10px;
		margin:auto;	background:#F36;	border:3px solid #000;	border-radius:5px;}
	#close_cancel
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}
	#close_cancel a
	{	text-decoration:none;color: #FFF;
	}

	#no_cancel
	{	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
	}

	.button {     height:13px;     min-width:70px;     margin:20px; padding:4px 5px;     border:1px solid #000;     font-size: 16px; }

	a.button.freeze_costing {     right: 0;     display: inline-block;     margin:0px;     float: right; }
</style>

<?php
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");


if(isset($_GET['reference_id']))
{	
	$_POST['ref_id'] = $_GET['reference_id'];

	$sql = "SELECT reference_no, remark from ".TB_PREF."costing_reference where ref_id = ".db_escape($_POST['ref_id']);
	$result = db_query($sql, "Could not get reference no detilas.");
	while($row = db_fetch($result))
	{
		$_POST['costing_reference'] = $row['reference_no'];
		$_POST['costing_remark'] = $row['remark'];
	}

	$sql = "SELECT category_id, range_id from ".TB_PREF."costing_reference_details where ref_id = ".db_escape($_GET['reference_id']);
	$result = db_query($sql, "Cannot get costing reference detilas ###range-cat");
	$category_id = array();
	$categories = "";
	while($row = db_fetch($result))
	{
		$category_id[] = $row['category_id'];
		$_POST['range_id'] = $row['range_id'];
		$sql1 = "SELECT cat_name from ".TB_PREF."item_category where category_id = ".db_escape($row['category_id']);
		$result1 = db_query($sql1);
		$cat_name = db_fetch_row($result1);
		$categories = $categories.", ".$cat_name['0'];
	}
	$categories = trim($categories, ',');
	$_POST['pro_categories'] = $categories;
	$_POST['category_id'] = $category_id;
	if($_POST['range_id'] == '-1')
	{
		$_POST['collection_id'] = "NonCollection";
	}
	else
	{
		$_POST['collection_id'] = "Collection";
	}
}

if(isset($_POST['finish_button']))
{
	unset($_SESSION['wood']);
	unset($_POST['basic_price']);
	unset($_SESSION['labour']);
	unset($_SESSION['edit_step_1']);
}

if(isset($_POST['edit_step_1'])) {

	global $Ajax;
	$_SESSION['edit_step_1'] = true;

	$man_finish_code_id = $_POST['man_finish_code_id'];
	$mfrg_id = get_mfrg_id($man_finish_code_id,1);

	// get manufacturing cost data
	get_manufacturing_cost($mfrg_id);

	$Ajax->activate('finish_products');
}

if(isset($_POST['update_step_1'])) 
{
	global $Ajax;

	// update manufacturing cost data
	$manuf_cost = array();
	$manuf_cost['man_finish_code_id'] = trim($_POST['man_finish_code_id']);
	$manuf_cost['mfrg_id'] = get_mfrg_id(trim($_POST['man_finish_code_id']));
	$manuf_cost['ref_id'] = trim($_POST['ref_id']);
	$manuf_cost['total_wood_cost'] = trim($_POST['total_wood_cost']);
	$manuf_cost['total_cons_cost'] = trim($_POST['total_cons_cost']);
	$manuf_cost['total_finish_cons_cost'] = trim($_POST['total_finish_cons_cost']);
	$manuf_cost['total_fabric_cost'] = trim($_POST['total_fabric_cost']);
	$manuf_cost['basic_price'] = trim($_POST['basic_price']);
	$manuf_cost['extra_percent'] = trim($_POST['extra_percent']);
	$manuf_cost['extra_amount'] = trim($_POST['extra_amount']);
	$manuf_cost['total_special_cost'] = trim($_POST['total_special_amount']);
	$manuf_cost['total_labour_cost'] = trim($_POST['total_labour_cost']);
	$manuf_cost['total_cost'] = trim($_POST['total_cost']);
	$manuf_cost['admin_percent'] = trim($_POST['admin_percent']);
	$manuf_cost['admin_cost'] = trim($_POST['admin_cost']);
	$manuf_cost['total_cp'] = trim($_POST['total_cp']);
	$manuf_cost['profit_percent'] = trim($_POST['profit_percent']);
	$manuf_cost['profit'] = trim($_POST['profit']);
	$manuf_cost['total_mfrg'] = trim($_POST['total_mfrg']);

	$wood_session = unserialize (serialize ($_SESSION['wood']));
	$labour_session = unserialize (serialize ($_SESSION['labour']));

	// update manufacturing cost data
	update_manufacturing_cost($manuf_cost, $wood_session, $labour_session);

	//update finishing cost data
	$updated_finishing = update_finishing_cost_on_change_manufacturing_cost($manuf_cost);

	//update final cost data
	update_final_cost_on_change_manufacturing_cost($manuf_cost, $updated_finishing);

	// remove session data
	unset($_SESSION['edit_step_1']);
	unset($_SESSION['wood']);
	unset($_SESSION['labour']);
	unset_costing_form_variables();
	display_notification(_("Manufacturing Cost has been updated."));
	
	$Ajax->activate('finish_products');
	//$Ajax->activate('_page_body'); 
}

if(!isset($_POST['session_var']))
{
	unset($_SESSION['wood']);		 
}

if(isset($_SESSION['wood']) && isset($_SESSION['labour'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['wood']);
	 fixObject($_SESSION['labour']);
}else{
	unset($_SESSION['wood']);
   	$_SESSION['wood'] = new wood;
   	unset($_SESSION['labour']);
   	$_SESSION['labour'] = new labour;
}

if(!isset($_POST['session_var']))
{
	unset($_SESSION['labour']);		 
}

if (isset($_POST['category_id'])) 
{
	$category_id = array();
	$category_id = $_POST['category_id'];
	if($_POST['collection_id'] == 'Collection')
	{
		$range_id = $_POST['range_id'];
	}
	if($_POST['collection_id'] == 'NonCollection')
	{
		$range_id = '-1';
	}
}
function wood_check_data()
{
	if(get_post('wood_id') == -1) {
		display_error( _("wood cannot be empty."));
		set_focus('wood_id');
		return false;
	}
	if(get_post('cft') == 0) {
		display_error( _("cft cannot be empty."));
		set_focus('cft');
		return false;
	}
	if(!get_post('rate')) {
		display_error( _("Rate cannot be empty."));
		set_focus('rate');
		return false;
	}
	if (!check_num('amount', 0))
    {
	   	display_error(_("The amount entered must be numeric and not less than zero."));
		set_focus('amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_cons_variables() {
	unset($_POST['wood_id']);
    unset($_POST['cft']);
    unset($_POST['rate']);
    unset($_POST['amount']);
}

function labour_check_data()
{
	/*if(!get_post('basic_price')) {
		display_error( _("Basic Price cannot be empty."));
		set_focus('basic_price');
		return false;
	}*/

	if(!get_post('labour_type')) {
		display_error( _("labour type cannot be empty."));
		set_focus('labour_type');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('labour_amount', 0))
    {
	   	display_error(_("The labour amount entered must be numeric and not less than zero."));
		set_focus('labour_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_labour_variables() {
    unset($_POST['labour_type']);
    unset($_POST['special_rate']);
    unset($_POST['labour_amount']);
	unset($_POST['special_labour_cost']);
}


function handle_new_labour()
{ 
	$check_data = labour_check_data();
	if($check_data){
		$_SESSION['labour']->add_to_labour_part(count($_SESSION['labour']->line_items),$_POST['labour_type'],$_POST['special_rate'],$_POST['labour_amount'], $_POST['special_labour_cost']);
		unset_form_labour_variables();
	}
	line_start_labour_focus();
}

function handle_labour_delete_item($line_no)
{
	unset($_SESSION['labour']->line_items[$line_no]);
	unset_form_labour_variables();
    line_start_labour_focus();
}

function handle_labour_update()
{
   $_SESSION['labour']->update_labour_item($_POST['line_no'],$_POST['labour_type'],$_POST['special_rate'],$_POST['labour_amount'], $_POST['special_labour_cost']);
	unset_form_labour_variables();
    line_start_labour_focus();
}

function handle_new_wood()
{
	$check_data = wood_check_data();

	if($check_data){
		$_SESSION['wood']->add_to_wood_part(count($_SESSION['wood']->line_items),$_POST['wood_id'],$_POST['cft'],$_POST['rate'],$_POST['amount']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}

function handle_wood_delete_item($line_no)
{
	array_splice($_SESSION['wood']->line_items, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

function handle_wood_update()
{
   $_SESSION['wood']->update_wood_item($_POST['line_no'],$_POST['wood_id'],$_POST['cft'],$_POST['rate'],$_POST['amount']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

//$category_id = get_post('category_id'); 

function line_start_consumable_focus() {
  global 	$Ajax;
  $Ajax->activate('wood_table');
  //set_focus('part_name');
}

function line_start_labour_focus() {
	global 	$Ajax;
	$Ajax->activate('labour_table');
	//set_focus('part_name');
}

$id = find_submit('Delete');
if ($id != -1)
{
	handle_wood_delete_item($id);
}
if (isset($_POST['WoodAddItem']))
{
	handle_new_wood();
}
if (isset($_POST['WoodUpdateItem'])){
 handle_wood_update();
}
if (isset($_POST['WoodCancelItemChanges'])) {
	line_start_consumable_focus();
}


//labour section  
$id = find_submit('Delete'); 
if ($id != -1)
{
	handle_labour_delete_item($id);
}
if (isset($_POST['LabourAddItem']))
{
	handle_new_labour();
}
if (isset($_POST['LabourUpdateItem'])){
    handle_labour_update();
}
if (isset($_POST['LabourCancelItemChanges'])) {
	line_start_labour_focus();
}

//--------------------------------------------------------------------------------------------
function get_finish_product(&$category_id, $range_id)
{
	start_outer_table(TABLESTYLE2, "style=min-width:1000px;min-height:300px;");
	div_start("finish_products");
	start_table(1, " style='width:100%;'");
	
	global $Ajax, $edit_step_1;
	$Ajax->activate('finish_products');
	$Ajax->activate('finish_code');
	$Ajax->activate('finish_code_id');
	start_row();
		hidden('session_var', '1');
		label_cell("Finish Code",'class="tableheader" width=120px');
		label_cell("Finish Product Name",'class="tableheader" style=min-width:200px');
		label_cell("W - H - D",'class="tableheader" width=100px');
		label_cell("CBM",'class="tableheader" width=50px');
	end_row();
	
	foreach($category_id as $cat_id)
	{
		if ($cat_id != -1) 
		{
			//SupplierID exists - either passed when calling the form or from the form itself
			
			$result = get_finish_product_list($cat_id, $range_id);
			while($row = db_fetch($result))
			{
				start_row("width=300px");
				$finish_code = $row['finish_comp_code'];
				$finish_pro_id = $row['finish_pro_id'];
				   echo '<input type="hidden" name="'.$finish_code.'" value="'.$finish_pro_id.'" >';
				   
				   $disabled = check_exist_mfrg_cost($finish_pro_id);
				   
				   $style = "";

				   if(!isset($_POST['man_finish_code_id']))
				   {
				   		$_POST['man_finish_code_id'] = $finish_pro_id;
				   		$_POST['man_finish_code'] = $finish_code;
				   }
				   if($_POST['man_finish_code_id'] == $finish_pro_id)
				   {
					   $style = "style=color:red;";
				   }
				   if(!empty($style) && !empty($disabled))
				   {
						$disabled = "";
						$style = "style=color:red;font-size:14px";
				   }
				   label_cell('<button type="submit" data="'.$finish_code.'" '.$disabled." ".$style.' name="finish_button" class="man_finish_code" >'.$finish_code.'</button> ');//'$row['finish_code'],'class="finish_link ajaxsubmit" id='.$row['finish_code'].' ');
				   
				   label_cell($row['finish_product_name']);
				   label_cell($row['asb_weight']."*".$row['asb_height']."*".$row['asb_density']);
				   label_cell('');
				end_row();
			}	
		}
	}
	div_end();
	table_section(2,"550px;");
	
 /*	if(isset($_POST['finish_code']) && !empty($_POST['finish_code']))
	{*/ 

		$check_exist = is_exist_mfrg_cost($_POST['man_finish_code_id']);

		start_table();
			echo '<input type="hidden" id="man_finish_code_id" name="man_finish_code_id" value="'.$_POST['man_finish_code_id'].'" >';
			text_row(_("Selected Finish Code:"),'man_finish_code', null, 30, 30, null,null,null,"id=man_finish_code");
		end_table();
		
		div_start('costing_entry');

		if($check_exist && $_SESSION['edit_step_1'] == false)
		{
			$finish_code_id = $_POST['man_finish_code_id'];
			$mfrg_id = get_mfrg_id($finish_code_id,1);
			display_design_cons_cost($mfrg_id);
			display_finish_cons_cost($mfrg_id);
			display_finish_fabrics_cost($mfrg_id);
			display_wood_cost($mfrg_id);
			display_labour_cost($mfrg_id);

			div_start('controls');
				submit_center('edit_step_1', _("Edit Manufacturing Cost"), true, '', 'default');
			div_end();
		}
		else if($check_exist && $_SESSION['edit_step_1'] == true)
		{
			display_design_consumables($_POST['man_finish_code_id']);
			display_finish_consumables($_POST['man_finish_code_id']);
			display_finish_fabrics($_POST['man_finish_code_id']);
			display_wood_summary($_SESSION['wood']);
			display_labour_summary($_SESSION['labour']);
			
			div_start('controls');
				submit_center('update_step_1', _("Update Manufacturing Cost"), true, '', 'default');
			div_end();
		
		} else if(!$check_exist && $_SESSION['edit_step_1'] == false) {

			display_design_consumables($_POST['man_finish_code_id']);
			display_finish_consumables($_POST['man_finish_code_id']);
			display_finish_fabrics($_POST['man_finish_code_id']);
			display_wood_summary($_SESSION['wood']);
			display_labour_summary($_SESSION['labour']);	
			
			div_start('controls');
				submit_center('submit', _("Add Manufacturing Cost"), true, '', 'default');
			div_end();
		}
		div_end();
	/*}*/
	end_outer_table(1);

	
}

function unset_costing_form_variables()
{
	//unset($_POST['man_finish_code_id']);
	unset($_POST['basic_price']);
	unset($_POST['total_special_amount']);
	unset($_POST['total_wood_cost']);
	unset($_POST['total_labour_cost']);
	unset($_POST['extra_percent']);
	unset($_POST['extra_amount']);
	unset($_POST['total_cost']);
	unset($_POST['admin_percent']);
	unset($_POST['admin_cost']);
	unset($_POST['total_cp']);
	unset($_POST['profit_percent']);
	unset($_POST['profit']);
	unset($_POST['total_mfrg']);
}

if (isset($_POST['submit'])) 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;


	if(!get_post('man_finish_code_id')) {
		$input_error = 1;
		display_error( _("Select a product to enter cost."));
		return false;
	}
	if(!get_post('total_mfrg')) {
		$input_error = 1;
		display_error( _("Total mfrg cannot be empty."));
		return false;
	}
	/*if(!get_post('basic_price')) {
		$input_error = 1;
		display_error( _("Basic Price cannot be empty."));
		set_focus('basic_price');
		return false;
	}*/

	//check to prevent from double entry in DB
	$check_exist = is_exist_mfrg_cost($_POST['man_finish_code_id']);
	if($check_exist)
	{
		$input_error = 1;
	}
	if ($input_error !=1 )
	{
		begin_transaction();
			
			$manuf_cost = array();
	
			$manuf_cost['man_finish_code_id'] = trim($_POST['man_finish_code_id']);
			$manuf_cost['ref_id'] = trim($_POST['ref_id']);
			$manuf_cost['total_wood_cost'] = trim($_POST['total_wood_cost']);
			$manuf_cost['total_cons_cost'] = trim($_POST['total_cons_cost']);
			$manuf_cost['total_finish_cons_cost'] = trim($_POST['total_finish_cons_cost']);
			$manuf_cost['total_fabric_cost'] = trim($_POST['total_fabric_cost']);
			$manuf_cost['basic_price'] = trim($_POST['basic_price']);
			$manuf_cost['extra_percent'] = trim($_POST['extra_percent']);
			$manuf_cost['extra_amount'] = trim($_POST['extra_amount']);
			$manuf_cost['total_special_cost'] = trim($_POST['total_special_amount']);
			$manuf_cost['total_labour_cost'] = trim($_POST['total_labour_cost']);
			$manuf_cost['total_cost'] = trim($_POST['total_cost']);
			$manuf_cost['admin_percent'] = trim($_POST['admin_percent']);
			$manuf_cost['admin_cost'] = trim($_POST['admin_cost']);
			$manuf_cost['total_cp'] = trim($_POST['total_cp']);
			$manuf_cost['profit_percent'] = trim($_POST['profit_percent']);
			$manuf_cost['profit'] = trim($_POST['profit']);
			$manuf_cost['total_mfrg'] = trim($_POST['total_mfrg']);
	
			$wood_session = &$_SESSION['wood'];
			$labour_session = &$_SESSION['labour'];
		
			insert_manufacturing_cost($manuf_cost, $wood_session, $labour_session);
				 
			unset($_SESSION['wood']);
			unset($_SESSION['labour']);
			unset_costing_form_variables();
			display_notification(_("Manufacturing Cost has been added."));
			//$Ajax->activate('_page_body');

		commit_transaction();
	}
} 
elseif (isset($_POST['delete']) && $_POST['delete'] != "") 
{
	//the link to delete a selected record was clicked instead of the submit button
	$cancel_delete = 0;

	// PREVENT DELETES IF DEPENDENT RECORDS IN 'supp_trans' , purch_orders
	if (key_in_foreign_table($_POST['supplier_id'], 'supp_trans', 'supplier_id'))
	{
		$cancel_delete = 1;
		display_error(_("Cannot delete this supplier because there are transactions that refer to this supplier."));

	} 
	else 
	{
		if (key_in_foreign_table($_POST['supplier_id'], 'purch_orders', 'supplier_id'))
		{
			$cancel_delete = 1;
			display_error(_("Cannot delete the supplier record because purchase orders have been created against this supplier."));
		}

	}
	if ($cancel_delete == 0) 
	{
		delete_supplier($_POST['supplier_id']);
		unset($_SESSION['supplier_id']);
		$supplier_id = '';
		$Ajax->activate('_page_body');
	} //end if Delete supplier
}

start_form();
	display_header();

	if ($category_id)
	{
		if(isset($_POST['session_var']))
		unset($_POST['_tabs_sel']); // force settings tab for new customer
	}

	tabbed_content_start('tabs', array(
		'manufacturing' => array(_('&Manufacturing Cost'), $_POST['category_id']),
		'finishing' => array(_('&Finishing Cost'), $category_id),
		'final' => array(_('&Final Cost'), $category_id),
	));
	
	switch (get_post('_tabs_sel')) {
		default:
		case 'manufacturing':
			$page_security = 'SA_MFRGCOSTING';
			check_page_security($page_security);
			check_page_access($page_security);
			unset($_SESSION['edit_step_2']);
			unset($_SESSION['edit_step_3']);
			get_finish_product($_POST['category_id'], $range_id); 
			break;
		case 'finishing':
			$page_security = 'SA_FINISHCOSTING';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['popup'] = 1;
			unset($_SESSION['edit_step_1']);
			unset($_SESSION['edit_step_3']);
			include_once($path_to_root."/costing/manage/finishing_cost.php");
			break;
		case 'final':
			$page_security = 'SA_FINALCOSTING';
			check_page_security($page_security);
			check_page_access($page_security);
			$_GET['popup'] = 1;
			unset($_SESSION['edit_step_1']);
			unset($_SESSION['edit_step_2']);
			include_once($path_to_root."/costing/manage/final_costing.php");
			break;
		};
		br();
		tabbed_content_end();
		hidden('popup', @$_REQUEST['popup']);
end_form();

if(!empty(get_post('costing_reference')))
echo "<div><center><span id='save_list'><a href='#save_list_popup'>Save Costing List</a></span></center></div>";
//echo "<pre>";print_r($_POST);
end_page(@$_REQUEST['popup']);


?>

<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="costing.js"></script>
<script src="finishing_cost.js"></script>
<script src="final_costing.js"></script>

<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="jquery-ui.js"></script>
<link rel="stylesheet" href="jquery-ui.css">

<script type="text/javascript">
	$(function() 
	{
		$(document).on('keydown','input[name=labour_type]',function(){
			var text = $("input[name=labour_type]").val();

			$.ajax({
				url: "autocomplete.php",
				dataType:"json",
				type: "GET",
            			data:"term="+text+"&functionName=get_labour_types",
				success: function(data){
					$('input[name=labour_type]').autocomplete({
				        source: data,
						autoFocus:true
				    });
				},
				error: function(data){
					console.log(data);
				}
			});
		});
	});
</script>

