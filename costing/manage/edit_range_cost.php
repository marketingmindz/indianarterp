<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_EDIT_RANGE_COST';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/edit_range_cost_db.inc");
include($path_to_root . "/costing/includes/ui/edit_range_cost_ui.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Edit Range Costs"), @$_REQUEST['popup'], false, "", $js);

function check_form_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Select Product Category."));
		set_focus('category_id');
		return false;
	}

	/*if(get_post('collection_id') == 'SelectCollection') {
		display_error( _("Select Collection Type."));
		set_focus('collection_id');
		return false;
	}*/

	if(get_post('collection_id') == 'Collection') {
		if(get_post('range_id') == -1)
		display_error( _("Select range."));
		set_focus('range_id');
		return false;
	}

    return true;	
}

start_form(true);
		display_header($_SESSION['DCODE']);

		div_start("costing_reference_list","style='min-height:400px; margin-top:50px;'");

		global $Ajax;
		$Ajax->activate('costing_reference_list');
		if(isset($_POST['GetReferenceNo']))
		{
			$check = check_form_data();
				if($check){
					if($_POST['collection_id'] == 'NonCollection')
					{
						$range_id = '-1';
					}
					else
					{
						$range_id = $_POST['range_id'];
					}
					display_costing_reference_list($_POST['category_id'], $range_id);
				}
		}
		div_end();
end_form();

end_page(@$_REQUEST['popup']);

?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="edit_range_cost.js"></script>



<style>
#costing_details table
{
	font-size:20px !important;
	color:#06F;
	font-weight:bold;
	margin-top:25px;
	margin-bottom:35px;
}
#costing_details td
{
	font-size:14px !important;
}
.button
{
	height:20px;
	min-width:70px;
	margin:20px;
	padding:0px 5px;
	border:1px solid #000;
}
.sign
{
	font-size:18px;
	font-weight:bold;
	margin-top:-5px;
	    padding: 0px 2px 2px 0px;
}
.tablestyle input[type="radio"]
{
	margin: 3px 0px 5px 8px;
}



.multiselect {
        width: 200px;
		    
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
		width: 200px;
        border: 1px #dadada solid;
		position: absolute;
    z-index: 999999999;
    background-color: #fff;
	max-height:300px;
	overflow:scroll;
	overflow-x:hidden;
	overflow-wrap: break-word;

    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
</style>
