<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_LABEL';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/costing/includes/db/currency_master_db.inc");
simple_page_mode(true);
//----------------------------------------------------------------------------------
page(_($help_context = "Currency Master"));

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['currency_name']) == 0) 
	{
		$input_error = 1;
		display_error(_("The currency name cannot be empty."));
		set_focus('currency_name');
	}
	
	if ($input_error != 1) 
	{
    	if ($selected_id != -1) 
    	{
			update_currency_details($selected_id, $_POST['currency_name'], $_POST['country_name'],$_POST['symbol'],$_POST['rate']);
			display_notification(_('Selected Currency has been updated'));

    	} 
    	else 
    	{
			$check_update_currency = check_update_currency($_POST['currency_name']);
		    if($check_update_currency)
             { 
	             display_notification(_('Currency already exist.'));	 
	         }else{ 
    		     add_new_currency($_POST['currency_name'], $_POST['country_name'],$_POST['symbol'],$_POST['rate']);
			     display_notification(_('New currency has been added'));
			 }
    	}
    	
		$Mode = 'RESET';
	}
} 

if ($Mode == 'Delete')
{
	delete_currency_details($selected_id);
	display_notification(_('Selected currency has been deleted'));
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------

$result = get_all_currencies();

start_form();
start_table(TABLESTYLE, "style='width:50%;min-width:50%;'");

$th = array(_('Currency Name'),_('Country'),_('Symbol'), "Rate", "", "");

table_header($th);
$k = 0;
while ($myrow = db_fetch($result)) 
{
	
	alt_table_row_color($k);	

	label_cell($myrow["currency_name"]);
	label_cell($myrow["country_name"]);
	label_cell($myrow["symbol"]);
	label_cell($myrow["rate"]);

 	edit_button_cell("Edit".$myrow['id'], _("Edit"));
 	delete_button_cell("Delete".$myrow['id'], _("Delete"));
	end_row();
}

end_table(1);

//-----------------------------------------------------------------------------------

start_table(TABLESTYLE2, "style='width:50%;min-width:50%;'");

if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing status code

		$myrow = get_currency_details($selected_id);

		$_POST['currency_name']  = $myrow["currency_name"];
		$_POST['country_name']  = $myrow["country_name"];
		$_POST['symbol']  = $myrow["symbol"];
		$_POST['rate']  = $myrow["rate"];
	}
	hidden('selected_id', $selected_id);
} 

text_row(_("Currency Name:"), 'currency_name', null, 40, 40);

text_row(_("Country:"), 'country_name', null, 40, 40);

text_row(_("Symbol:"), 'symbol', null, 40, 40);

text_row(_("Rate:"), 'rate', null, 40, 40);
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

//------------------------------------------------------------------------------------
end_page();

?>
 <script type="text/javascript">	
   function noNumerics(evt)
	 {
		 var e = event || evt;
		 var charCode = e.which || e.keyCode;
		 if ((charCode >= 48) && (charCode <= 57))
			return false;
		 return true;
	 }
	 function upper(ustr)
	{
		var str = document.getElementById('refernec').value;
		document.getElementById('refernec').value = str.toUpperCase();
	}
   
</script>