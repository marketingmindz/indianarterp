<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SAVED_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/saved_costing_ui.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Cancel Costing"), @$_REQUEST['popup'], false, "", $js);




function delete_consumable_cost($ref_id)
{	
	$sql = "delete from ".TB_PREF."consumable_cost where ref_id = ".db_escape($ref_id);
	db_query($sql, "Could not delete consumable_cost ##cancel_costing");
}


if(isset($_GET['reference_id']))
{
	$ref_id = $_GET['reference_id'];
	$sql = "delete from ".TB_PREF."cons_costing_reference where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not delete consumable cost");
	
	$sql = "delete from ".TB_PREF."cons_costing_reference_details where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not delete consumable cost details.");

	
	delete_consumable_cost($ref_id);

	display_notification('Costing has been canceled. Go to <a style="font-size:14px;" href="saved_consumable_costing.php">Consumable Saved Costing </a>');
} 
else
{
	meta_forward("saved_consumable_costing.php");
}

?>
