<?php
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/ui/re_enter_finishing_cost_ui.inc");
include($path_to_root . "/costing/includes/finish_costing_class.inc");
include_once($path_to_root . "/includes/ui.inc");
//page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);
//simple_page_mode(true);
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
	

if(isset($_POST['finish_cost_button']))
{
	unset($_SESSION['finish_cost']);
	unset($_SESSION['edit_step_2']);
	unset_finishing_cost_variables();
}
	

if(!isset($_POST['session_finish_var']))
{
	unset($_SESSION['finish_cost']);
}
if(isset($_SESSION['finish_cost'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['finish_cost']);

}else{
	unset($_SESSION['finish_cost']);
   $_SESSION['finish_cost'] = new finish;
}


if(isset($_POST['edit_step_2'])) {

	global $Ajax;
	$_SESSION['edit_step_2'] = true;

	$finish_code_id = $_POST['finish_code_id'];
	$mfrg_id = get_locked_mfrg_id($finish_code_id, $_POST['ref_id'], $_POST['ref_version']);

	// get locked finishing cost data
	get_locked_finishing_cost($finish_code_id, $_POST['ref_id'], $_POST['ref_version']);

	$Ajax->activate('_page_body');
}

if(isset($_POST['update_step_2'])) 
{
	global $Ajax;

	$finish_cost = array();

	$finish_cost['finish_code_id'] = $_POST['finish_code_id'];
	$finish_cost['ref_id'] = $_POST['ref_id'];
	$finish_cost['ref_version'] = $_POST['ref_version'];
	$finish_cost['sanding_labour'] = $_POST['sanding_labour'];
	$finish_cost['sanding_percent'] = $_POST['sanding_percent'];
	$finish_cost['sanding_cost'] = $_POST['sanding_cost'];
	$finish_cost['polish_labour'] = $_POST['polish_labour'];
	$finish_cost['polish_percent'] = $_POST['polish_percent'];
	$finish_cost['polish_cost'] = $_POST['polish_cost'];
	$finish_cost['packaging_labour'] = $_POST['packaging_labour'];
	$finish_cost['packaging_percent'] = $_POST['packaging_percent'];
	$finish_cost['packaging_cost'] = $_POST['packaging_cost'];
	$finish_cost['actual_sending_cost'] = $_POST['actual_sending_cost'];
	$finish_cost['actual_polish_cost'] = $_POST['actual_polish_cost'];
	$finish_cost['actual_packaging_cost'] = $_POST['actual_packaging_cost'];		
	$finish_cost['lumsum_cost_percent'] = $_POST['lumsum_cost_percent'];
	$finish_cost['lumsum_cost_value'] = $_POST['lumsum_cost_value'];
	$finish_cost['forwarding_percent'] = $_POST['forwarding_percent'];
	$finish_cost['forwarding_cost'] = $_POST['forwarding_cost'];
	$finish_cost['other_cost'] = $_POST['other_cost'];
	$finish_cost['total_special_cost'] = $_POST['total_special_cost'];
	$finish_cost['total_finishing_cost'] = $_POST['total_finishing_cost'];
	$finish_cost['total_amount'] = $_POST['total_finishing_cost'] + $_POST['total_mfrg_cost'];
	$finish_cost['finish_record_id'] = $_POST['finish_record_id'];


	$finish_cost_session = unserialize (serialize ($_SESSION['finish_cost']));

	// update locked finishing cost data
	update_locked_finishing_cost($finish_cost, $finish_cost_session);

	// update locked final cost data
	update_locked_final_cost_on_change_locked_finishing_cost($finish_cost);

	// remove session data
	unset($_SESSION['edit_step_2']);
	display_notification(_("Locked Finishing Cost has been updated."));
	
	$Ajax->activate('_page_body');
}



function get_product_for_finishing_cost(&$category_id, $range_id, $ref_id, $ref_version)
{
	start_outer_table(TABLESTYLE2, "style=min-width:1000px;min-height:300px;");
	div_start("finish");
	start_table(1, "style='width:100%;'");
	
	global $Ajax;
	$Ajax->activate('finish');

     start_row();
	 hidden('session_finish_var', '1');
	   label_cell("Finish Code",'class="tableheader" width=120px');
	   label_cell("Finish Product Name",'class="tableheader" style=min-width:200px');
	   label_cell("W - H - D",'class="tableheader" width=100px');
	   label_cell("CBM",'class="tableheader" width=50px');
	 end_row();
	 foreach($category_id as $cat_id)
	 {
		if ($cat_id != -1) 
		{
			//SupplierID exists - either passed when calling the form or from the form itself
			$result = get_finish_product_list_from_manufacturing($cat_id, $range_id, $ref_id, $ref_version);
			while($row = db_fetch($result))
			{
				start_row("width=300px");
				$finish_code = $row['finish_comp_code'];
				$finish_pro_id = $row['finish_pro_id'];
				
				echo '<input type="hidden" name="'.$finish_code.'" value="'.$finish_pro_id.'" >';
				
				$disabled = check_exist_finishing_cost($finish_pro_id, $ref_id, $ref_version);
				
				$style = "";
				if(!isset($_POST['finish_code_id']))
				{
						$_POST['finish_code_id'] = $finish_pro_id;
						$_POST['finish_code'] = $finish_code;
				}
				if($_POST['finish_code_id'] == $finish_pro_id)
				{
				   $style = "style=color:red;";
				}
				if(!empty($style) && !empty($disabled))
				{
					$disabled = "";
					$style = "style=color:red;font-size:14px";
				}
				label_cell('<button type="submit" id="'.$finish_code.'" '.$disabled." ".$style.' name="finish_cost_button" class="finish_code" >'.$finish_code.'</button> ');//'$row['finish_code'],'class="finish_link ajaxsubmit" id='.$row['finish_code'].' ');
				
				label_cell($row['finish_product_name']);
				label_cell($row['asb_weight']."*".$row['asb_height']."*".$row['asb_density']);
				label_cell('');
				end_row();
			}	
		}
	 }
	div_end();
	table_section(2,"550px;");
	
/*	if(isset($_POST['finish_code']) && !empty($_POST['finish_code']))
	{*/ 
		start_table();
			echo '<input type="hidden" id="finish_code_id" name="finish_code_id" value="'.$_POST['finish_code_id'].'" >';
			text_row(_("Selected Finish Code:"),'finish_code', null, 30, 30, null,null,null,"id= finish_code");
		end_table();
		
		$check_exist = is_exist_finishing_cost($_POST['finish_code_id'], $ref_id, $ref_version);

		if($check_exist && $_SESSION['edit_step_2'] == false)
		{
	    	display_finishing_cost($_POST['finish_code_id'], $ref_id, $ref_version);

	    	div_start('controls');
	    		submit_center('edit_step_2', _("Edit Finishing Cost"), true, '', 'default');
	    	div_end();
		
		} else if($check_exist && $_SESSION['edit_step_2'] == true)
		{
			display_finishing_costing_form($_SESSION['finish_cost'], $ref_id, $ref_version);
			
			div_start('controls');
				submit_center('update_step_2', _("Update Finishing Cost"), true, '', 'default');
			div_end();
		
		} else if(!$check_exist && $_SESSION['edit_step_2'] == false) 
		{
			display_finishing_costing_form($_SESSION['finish_cost'], $ref_id, $ref_version);

			div_start('controls');
				submit_center('AddFinishingCost', _("Add Finishing Cost"), true, '', 'default');
			div_end();
		}

		div_end();
	/*}*/
	end_outer_table(1);
	div_end();
}


function finish_check_data()
{
	if(!get_post('special_cost_ref')) {
		display_error( _("Special Cost Reference cannot be empty."));
		set_focus('special_cost_ref');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('special_amount', 0))
    {
	   	display_error(_("Special amount entered must be numeric and not less than zero."));
		set_focus('special_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_finish_variables() {
    unset($_POST['special_cost_ref']);
    unset($_POST['special_rate']);
    unset($_POST['special_amount']);
	unset($_POST['total']);
}


function handle_new_finish()
{ 
	$check_data = finish_check_data();
	if($check_data){
		$_SESSION['finish_cost']->add_to_finish_part(count($_SESSION['finish_cost']->line_items),$_POST['special_cost_ref'],$_POST['special_rate'],$_POST['special_amount'], $_POST['total']);
		unset_form_finish_variables();
	}
	line_start_finish_focus();
}

function handle_finish_delete_item($line_no)
{
	unset($_SESSION['finish_cost']->line_items[$line_no]);
	unset_form_finish_variables();
    line_start_finish_focus();
}

function handle_finish_update()
{
   $_SESSION['finish_cost']->update_finish_item($_POST['line_no'],$_POST['special_cost_ref'],$_POST['special_rate'],$_POST['special_amount'], $_POST['total']);
	unset_form_finish_variables();
    line_start_finish_focus();
}

function line_start_finish_focus() {
  global 	$Ajax;
  $Ajax->activate('finish_table');
  //set_focus('part_name');
}

$id = find_submit('Delete'); 
if ($id != -1)
{
	handle_finish_delete_item($id);
}
if (isset($_POST['FinishAddItem']))
{
	handle_new_finish();
}
if (isset($_POST['FinishUpdateItem'])){
    handle_finish_update();
}
if (isset($_POST['FinishCancelItemChanges'])) {
	line_start_finish_focus();
}


if (isset($_POST['AddFinishingCost'])) 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if(!get_post('finish_code_id')) {
		$input_error = 1;
		display_error( _("Select a product to enter cost."));
		return false;
	}
	if(!get_post('total_finishing_cost')) {
		$input_error = 1;
		display_error( _("Total Finishing cost cannot be empty."));
		return false;
	}

	//check to prevent from double entry in DB
	$check_exist = is_exist_finishing_cost($_POST['finish_code_id'], $_POST['ref_id'], $_POST['ref_version']);
	if($check_exist)
	{
		$input_error = 1;
	}

	if ($input_error !=1 )
	{
		begin_transaction();
			$finishing_cost = array();
	
			$finishing_cost['finish_code_id'] = trim($_POST['finish_code_id']);
			$finishing_cost['ref_id'] = trim($_POST['ref_id']);
			$finishing_cost['ref_version'] = trim($_POST['ref_version']);
			$finishing_cost['ref_id'] = trim($_POST['ref_id']);
			$finishing_cost['sanding_labour'] = trim($_POST['sanding_labour']);
			$finishing_cost['sanding_percent'] = trim($_POST['sanding_percent']);
			$finishing_cost['sanding_cost'] = trim($_POST['sanding_cost']);
			$finishing_cost['polish_labour'] = trim($_POST['polish_labour']);
			$finishing_cost['polish_percent'] = trim($_POST['polish_percent']);
			$finishing_cost['polish_cost'] = trim($_POST['polish_cost']);
			$finishing_cost['packaging_labour'] = trim($_POST['packaging_labour']);
			$finishing_cost['packaging_percent'] = trim($_POST['packaging_percent']);
			$finishing_cost['packaging_cost'] = trim($_POST['packaging_cost']);
			$finishing_cost['actual_sending_cost'] = trim($_POST['actual_sending_cost']);
			$finishing_cost['actual_polish_cost'] = trim($_POST['actual_polish_cost']);
			$finishing_cost['actual_packaging_cost'] = trim($_POST['actual_packaging_cost']);
			$finishing_cost['lumsum_cost_percent'] = trim($_POST['lumsum_cost_percent']);
			$finishing_cost['lumsum_cost_value'] = trim($_POST['lumsum_cost_value']);
			$finishing_cost['forwarding_percent'] = trim($_POST['forwarding_percent']);
			$finishing_cost['forwarding_cost'] = trim($_POST['forwarding_cost']);
			$finishing_cost['other_cost'] = trim($_POST['other_cost']);
			$finishing_cost['total_finishing_cost'] = trim($_POST['total_finishing_cost']);
			$finishing_cost['total_special_cost'] = trim($_POST['total_special_cost']);
			$finishing_cost['total_amount'] = trim($_POST['total_mfrg_cost'] + $_POST['total_finishing_cost']);

			$finishing_session = &$_SESSION['finish_cost'];
		
			insert_locked_finishing_cost($finishing_cost, $finishing_session);

			unset($_SESSION['finish_cost']);
			unset_finishing_cost_variables();
			display_notification(_("Finishing Cost has been added."));
			$Ajax->activate('_page_body');

		commit_transaction();
	}
} 
elseif (isset($_POST['delete']) && $_POST['delete'] != "") 
{
	//the link to delete a selected record was clicked instead of the submit button
	$cancel_delete = 0;

	// PREVENT DELETES IF DEPENDENT RECORDS IN 'supp_trans' , purch_orders
	if (key_in_foreign_table($_POST['supplier_id'], 'supp_trans', 'supplier_id'))
	{
		$cancel_delete = 1;
		display_error(_("Cannot delete this supplier because there are transactions that refer to this supplier."));
	} 
	else 
	{
		if (key_in_foreign_table($_POST['supplier_id'], 'purch_orders', 'supplier_id'))
		{
			$cancel_delete = 1;
			display_error(_("Cannot delete the supplier record because purchase orders have been created against this supplier."));
		}
	}
	if ($cancel_delete == 0) 
	{
		delete_supplier($_POST['supplier_id']);
		unset($_SESSION['supplier_id']);
		$supplier_id = '';
		$Ajax->activate('_page_body');
	} //end if Delete supplier
}
$ref_id = get_post("ref_id");
$ref_version = get_post("ref_version");
start_form();
	get_product_for_finishing_cost($_POST['category_id'], $range_id, $ref_id, $ref_version); 
end_form();
//end_page(@$_REQUEST['popup']);
?>