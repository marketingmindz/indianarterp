<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_FREEZE_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/freeze_costing_ui.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Freeze"), @$_REQUEST['popup'], false, "", $js);

function check_form_data()
{
	if(get_post('category_id') == -1) {
		display_error( _("Category cannot be empty."));
		set_focus('category_id');
		return false;
	}
	if(get_post('design_id') == -1) {
		display_error( _("Select Design Code."));
		set_focus('sub_design');
		return false;
	}
	if(get_post('finish_code_id') == -1) {
		display_error( _("Select Finish Code.."));
		set_focus('packaging_finish_code');
		return false;
	}
    return true;	
}

start_form(true);
	div_start("costing_reference_list","style='min-height:400px;'");
		$Ajax->activate('costing_reference_list');
		display_costing_reference_list();	
	div_end();
end_form();
end_page();
?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="freeze_costing.js"></script>
<style>
#costing_details table
{
	font-size:20px !important;
	color:#06F;
	font-weight:bold;
	margin-top:25px;
	margin-bottom:35px;
}
#costing_details td
{
	font-size:14px !important;
}
.button
{
	height:20px;
	min-width:70px;
	margin:10px;
	padding:0px 5px;
	border:1px solid #000;
	white-space: nowrap;
}
</style>
	