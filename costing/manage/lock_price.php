<?php
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


function insert_manufacturing_cost($ref_id)
{
	$sql = "select version from ".TB_PREF."reference_version_seq where ref_id =".db_escape($ref_id);
	$result = db_query($sql, "Costing reference version could not be checked");
	
	$sql = "select * from ".TB_PREF."manufacturing_cost where ref_id =".db_escape($ref_id);
	$result1 = db_query($sql, "manufacturing_cost could not be retrieved");
	while($manuf_cost = db_fetch($result1))
	{
		$mfrg_id = $manuf_cost['id'];
		$date = Today();
		$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_cost(finish_code_id, ref_id, total_cons_cost,total_finish_cons_cost,total_fabric_cost,total_wood_cost,
		total_labour_cost, extra_percent, extra_amount, total_cost,admin_percent,admin_cost,total_cp,profit_percent,profit,total_mfrg, version, date) VALUES(";
		$sql .= db_escape($manuf_cost['finish_code_id']) . "," .
		db_escape($manuf_cost['ref_id']) . "," .
		db_escape($manuf_cost['total_cons_cost']) . "," .
		db_escape($manuf_cost['total_finish_cons_cost']) . "," .
		db_escape($manuf_cost['total_fabric_cost']) . "," .
		db_escape($manuf_cost['total_wood_cost']) . "," .
		db_escape($manuf_cost['total_labour_cost']) . "," .
		db_escape($manuf_cost['extra_percent']) . "," .
		db_escape($manuf_cost['extra_amount']) . "," .
		db_escape($manuf_cost['total_cost']) . "," .
		db_escape($manuf_cost['admin_percent']) . "," .
		db_escape($manuf_cost['admin_cost']) . "," .
		db_escape($manuf_cost['total_cp']) . "," .
		db_escape($manuf_cost['profit_percent']) . "," .
		db_escape($manuf_cost['profit']) . "," .
		db_escape($manuf_cost['total_mfrg']) . ",'A','". date2sql($date)."')";
		db_query($sql, "Manufacturing cost is not inserted.");
		$new_mfrg_id = db_insert_id();
		
		$sql = "select * from ".TB_PREF."manufacturing_wood_cost where mfrg_id =".db_escape($mfrg_id);
		$result = db_query($sql, "manufacturing_wood_cost could not be checked");	
		
		/*for($i = 0;$i < $_POST['design_cons_no'];$i++ )
		{
			$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['des_master_name_'.$i]). "," .
			db_escape($_POST['des_consumable_name_'.$i]). "," .
			db_escape($_POST['des_cons_unit_'.$i]). "," .
			db_escape($_POST['des_cons_qty_'.$i]). "," .
			db_escape($_POST['des_rate_'.$i]). "," .
			db_escape($_POST['des_cost_'.$i]). ")";
			db_query($sql, "Design consumable Costing is not inserted");
		}
		 
		 for($i = 0;$i < $_POST['finish_cons_no'];$i++ )
		 {
			$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['finish_master_name_'.$i]). "," .
			db_escape($_POST['finish_consumable_name_'.$i]). "," .
			db_escape($_POST['finish_cons_unit_'.$i]). "," .
			db_escape($_POST['finish_cons_qty_'.$i]). "," .
			db_escape($_POST['finish_cons_rate_'.$i]). "," .
			db_escape($_POST['finish_cost_'.$i]). ")";
			db_query($sql, "Finish consumable Costing is not inserted");
		 }
		
		for($i = 0;$i < $_POST['fabric_cons_no'];$i++ )
		 {
			$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['fabric_name_'.$i]). "," .
			db_escape($_POST['perecentage_'.$i]). "," .
			db_escape($_POST['fabric_rate_'.$i]). "," .
			db_escape($_POST['fabric_cost_'.$i]). ")";
			db_query($sql, "Fabric consumable Costing is not inserted");
		 }*/
		while($row = db_fetch($result))
		{
			$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
			$sql .=  db_escape($new_mfrg_id). ", " . 
			db_escape($row['wood_id']). "," .
			db_escape($row['cft']). "," .
			db_escape($row['rate']). "," .
			db_escape($row['amount']). ")";
			db_query($sql, "Wood Costing is not inserted");
		 }
		 $sql = "select * from ".TB_PREF."manufacturing_labour_cost where mfrg_id =".db_escape($mfrg_id);
		$result = db_query($sql, "manufacturing_labour_cost could not be retrieved");	
		while($row = db_fetch($result))
		{
		 
			$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_labour_cost(mfrg_id, basic_price, total_special_cost, total_labour_cost) VALUES (";
			$sql .=  db_escape($new_mfrg_id). ", " . 
			db_escape($row['basic_price']). "," .
			db_escape($row['total_special_cost']). "," .
			db_escape($row['total_labour_cost']). ")";
			db_query($sql, "Labour Costing is not inserted");
		}
		 
		$sql = "select * from ".TB_PREF."manufacturing_labour_special_cost where mfrg_id =".db_escape($mfrg_id);
		$result = db_query($sql, "manufacturing_labour_special_cost could not be retrieved");	
		while($row = db_fetch($result))
		{
			$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
			$sql .=  db_escape($new_mfrg_id). ", " . 
			db_escape($row['labour_type']). "," .
			db_escape($row['special_rate']). "," .
			db_escape($row['labour_amount']). "," .
			db_escape($row['special_labour_cost']). ")";
			db_query($sql, "Labour Costing is not inserted");
		 }
	}
	return true; 
}

function insert_finishing_cost($ref_id)
{
	$date = Today();
	$sql = "select * from ".TB_PREF."finishing_cost where ref_id =".db_escape($ref_id);
	$result1 = db_query($sql, "finishing_cost could not be retrieved");
	while($finishing_cost = db_fetch($result1))
	{
		$finishing_cost_id = $finishing_cost['id'];
		$sql = "INSERT INTO ".TB_PREF."locked_finishing_cost(finish_code_id, ref_id,sanding_labour,sanding_percent,
		sanding_cost,polish_labour,polish_percent,polish_cost,packaging_labour,packaging_percent,packaging_cost,actual_sending_cost,actual_polish_cost,actual_packaging_cost,lumsum_cost_percent,lumsum_cost_value,forwarding_percent,forwarding_cost,total_special_cost,other_cost,total_finishing_cost, total_amount, version, date) VALUES(";
		$sql .= db_escape($finishing_cost['finish_code_id']) . "," .
				db_escape($finishing_cost['ref_id']) . "," .
				db_escape($finishing_cost['sanding_labour']) . "," .
				db_escape($finishing_cost['sanding_percent']) . "," .
				db_escape($finishing_cost['sanding_cost']) . "," .
				db_escape($finishing_cost['polish_labour']) . "," .
				db_escape($finishing_cost['polish_percent']) . "," .
				db_escape($finishing_cost['polish_cost']) . "," .
				db_escape($finishing_cost['packaging_labour']) . "," .
				db_escape($finishing_cost['packaging_percent']) . "," .
				db_escape($finishing_cost['packaging_cost']) . "," .
				db_escape($finishing_cost['actual_sending_cost']) . "," .
				db_escape($finishing_cost['actual_polish_cost']) . "," .
				db_escape($finishing_cost['actual_packaging_cost']) . "," .
				db_escape($finishing_cost['lumsum_cost_percent']) . "," .
				db_escape($finishing_cost['lumsum_cost_value']) . "," .
				db_escape($finishing_cost['forwarding_percent']) . "," .
				db_escape($finishing_cost['forwarding_cost']) . "," .
				db_escape($finishing_cost['total_special_cost']) . "," .
				db_escape($finishing_cost['other_cost']) . "," .
				db_escape($finishing_cost['total_finishing_cost']) . "," .
				db_escape($finishing_cost['total_amount']) . ",'A','". date2sql($date)."')";
		db_query($sql, "finishing cost is not inserted.");
		
		$new_finishing_cost_id = db_insert_id();
		
		 
		$sql = "select * from ".TB_PREF."finishing_special_cost where finishing_cost_id =".db_escape($finishing_cost_id);
		$result = db_query($sql, "finishing_special_cost could not be retrieved");	
		while($row = db_fetch($result))
		{
			$sql = "INSERT INTO ".TB_PREF."locked_finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
			$sql .= db_escape($new_finishing_cost_id). ", " . 
					db_escape($row['special_cost_ref']). "," .
					db_escape($row['special_rate']). "," .
					db_escape($row['special_amount']). "," .
					db_escape($row['total']). ")";
			db_query($sql, "locked_finishing_special_cost is not inserted");
		 }
	}
	return true; 
}
function insert_final_cost($ref_id)
{
	$date = Today();
	$sql = "select * from ".TB_PREF."final_costing where ref_id =".db_escape($ref_id);
	$result = db_query($sql, "final_costing could not be retrieved");
	while($final_cost = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."locked_final_costing(finish_code_id, ref_id, total_mfrg_cost,
				total_finishing_cost, total_finishing_percent, finishing_exp_percent, final_finishing_cost,
				total_cost, final_profit_percent, final_profit, final_total_cost,
				 currency_price, version, date, locked, locked_date) VALUES(";
		$sql .= db_escape($final_cost['finish_code_id']) . "," .
				db_escape($final_cost['ref_id']) . "," .
				db_escape($final_cost['total_mfrg_cost']) . "," .
				db_escape($final_cost['total_finishing_cost']) . "," .
				db_escape($final_cost['total_finishing_percent']) . "," .
				db_escape($final_cost['finishing_exp_percent']) . "," .
				db_escape($final_cost['final_finishing_cost']) . "," .
				db_escape($final_cost['total_cost']) . "," .
				db_escape($final_cost['final_profit_percent']) . "," .
				db_escape($final_cost['final_profit']) . "," .
				db_escape($final_cost['final_total_cost']) . "," .
				db_escape($final_cost['currency_price']).",'A','".date2sql($date)."', '1', '".date2sql($date)."')";
		db_query($sql, "final cost is not inserted.");
	}
	return true; 
}

if(isset($_GET['ref_id']))
{
	$ref_id = $_GET['ref_id'];
	$sql = "select version from ".TB_PREF."reference_version_seq where ref_id =".db_escape($ref_id);
	$result = db_query($sql, "Costing reference version could not be checked");
	if(db_num_rows($result) > 0)
	{
		display_error("This Reference No. Is already locked.");
	}
	else
	{
		$ref_id = $_GET['ref_id'];
		insert_manufacturing_cost($ref_id);
		insert_finishing_cost($ref_id);
		insert_final_cost($ref_id);
		$date = Today();
		$sql = "update ".TB_PREF."costing_reference set locked = '1', locked_date = '".date2sql($date)."' where ref_id = ".db_escape($ref_id);
		db_query($sql, "could not update reference no.");
		$sql = "insert into ".TB_PREF."reference_version_seq(ref_id, version, locked) values(".db_escape($ref_id).", 'A', '1')";
		db_query($sql, "could not update reference version.");

		$sql = "update ".TB_PREF."costing_reference set current_version = 'A' where ref_id = ".db_escape($ref_id);                  
		db_query($sql, "could not set current_version");
		meta_forward("locked_costing.php");
	}
}
else
{
	display_error("Please choose a reference no. to lock the price. Go to freeze costing!!!");
}
?>