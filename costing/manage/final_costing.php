<?php
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/ui/final_costing_ui.inc");
//include($path_to_root . "/costing/includes/finish_costing_class.inc");
include_once($path_to_root . "/costing/includes/db/costing_db.inc");
include_once($path_to_root . "/includes/ui.inc");
//page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);
//simple_page_mode(true);
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
	

if(isset($_POST['final_cost_button']))
{
	unset($_SESSION['finish_cost']);
	unset($_SESSION['edit_step_3']);
	unset_final_cost_variables();
}
	
if(isset($_POST['edit_step_3'])) {

	global $Ajax;
	$_SESSION['edit_step_3'] = true;
	$Ajax->activate('costing_entry');
}

if(isset($_POST['update_step_3'])) 
{
	global $Ajax;

	$final_cost = array();
	
	$final_cost['final_finish_code_id'] = trim($_POST['final_finish_code_id']);
	$final_cost['ref_id'] = trim($_POST['ref_id']);
	$final_cost['total_mfrg_cost'] = trim($_POST['total_mfrg_cost']);
	$final_cost['total_percent'] = trim($_POST['total_percent']);
	$final_cost['total_finishing_cost'] = trim($_POST['total_finishing_cost']);
	$final_cost['total_cost'] = trim($_POST['total_cost']);
	$final_cost['final_profit_percent'] = trim($_POST['final_profit_percent']);
	$final_cost['final_profit'] = trim($_POST['final_profit']);
	$final_cost['final_total_cost'] = trim($_POST['final_total_cost']);
	$final_cost['date'] = trim($_POST['date']);

	$currency_price = array();
	$curr = explode(",",$_POST['currencies']);
	
	for($i=0; $i<$_POST['total_currency']; $i++)
	{
		$currency_price[][$curr[$i]] = $_POST['currency'.$curr[$i]];
	}

	$currency = json_encode($currency_price);
	$final_cost['currency'] = trim($currency);

	// update final cost data
	update_final_costing($final_cost);

	unset($_SESSION['edit_step_3']);
	display_notification(_("Final Cost has been updated."));
	
	$Ajax->activate('costing_entry');
}


function get_product_for_final_cost(&$category_id, $range_id)
{
	start_outer_table(TABLESTYLE2, "style=min-width:1000px;min-height:300px;");
	div_start("final");
	start_table(1, "style='width:100%;'");
	
	global $Ajax;
	$Ajax->activate('final');
	start_row();
	hidden('session_finish_var', '1');
		label_cell("Finish Code",'class="tableheader" width=120px');
		label_cell("Finish Product Name",'class="tableheader" style=min-width:200px');
		label_cell("W - H - D",'class="tableheader" width=100px');
		label_cell("CBM",'class="tableheader" width=50px');
	end_row();
	 foreach($category_id as $cat_id)
	 {
		if ($cat_id != -1) 
		{
			//SupplierID exists - either passed when calling the form or from the form itself
			$result = get_finish_product_list_from_finishing($cat_id, $range_id);
			while($row = db_fetch($result))
			{
				start_row("width=300px");
				$finish_code = $row['finish_comp_code'];
				$finish_pro_id = $row['finish_pro_id'];
				
				echo '<input type="hidden" name="'.$finish_code.'" value="'.$finish_pro_id.'" >';
				$disabled = check_exist_final_cost($finish_pro_id);
				
				$style = "";

				if(!isset($_POST['final_finish_code_id']))
				{
						$_POST['final_finish_code_id'] = $finish_pro_id;
						$_POST['final_finish_code'] = $finish_code;
				}
				if($_POST['final_finish_code_id'] == $finish_pro_id)
				{
				   $style = "style=color:red;";
				}
				if(!empty($style) && !empty($disabled))
				{
					$disabled = "";
					$style = "style=color:red;font-size:14px";
				}
				label_cell('<button type="submit" id="'.$finish_code.'" '.$disabled." ".$style.' name="final_cost_button" class="final_finish_code" >'.$finish_code.'</button> ');//'$row['finish_code'],'class="finish_link ajaxsubmit" id='.$row['finish_code'].' ');
				
				label_cell($row['finish_product_name']);
				label_cell($row['asb_weight']."*".$row['asb_height']."*".$row['asb_density']);
				label_cell('');
				end_row();
			}	
		}
	 }
	
	div_end();
	table_section(2,"550px;");
	
 /*	if(isset($_POST['finish_code']) && !empty($_POST['finish_code']))
	{*/ 
		start_table();
		echo '<input type="hidden" id="final_finish_code_id" name="final_finish_code_id" value="'.$_POST['final_finish_code_id'].'" >';
		text_row(_("Selected Finish Code:"),'final_finish_code', null, 30, 30, null,null,null,"id= final_finish_code");
		end_table();
		div_start('costing_entry');
		
		if(isset($_POST['final_finish_code_id']) && !empty($_POST['final_finish_code_id'])) {
			$check_exist = is_exist_final_cost($_POST['final_finish_code_id']);
		} else {
			$check_exist = false;
		}

		if($check_exist && $_SESSION['edit_step_3'] == false) {

			display_final_costing($_POST['final_finish_code_id']);
			submit_center('edit_step_3', _("Edit Final Cost"), true, '', 'default');
		
		} else if($check_exist && $_SESSION['edit_step_3'] == true) {

			display_final_costing_edit_form();		
			submit_center('update_step_3', _("Update Final Cost"), true, '', 'default');

		} else if(!$check_exist && $_SESSION['edit_step_3'] == false) {

		    display_final_costing_form();		
			submit_center('AddFinalCost', _("Add Final Cost"), true, '', 'default');
		}
		
		div_end();
	/*}*/
	end_outer_table(1);
}


function final_check_data()
{
	if(!get_post('special_cost_ref')) {
		display_error( _("Special Cost Reference cannot be empty."));
		set_focus('special_cost_ref');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('special_amount', 0))
    {
	   	display_error(_("Special amount entered must be numeric and not less than zero."));
		set_focus('special_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_final_variables() {
    unset($_POST['special_cost_ref']);
    unset($_POST['special_rate']);
    unset($_POST['special_amount']);
	unset($_POST['total']);
}


if (isset($_POST['AddFinalCost'])) 
{
	$input_error = 0;

	if(!get_post('final_finish_code_id')) {
		$input_error = 1;
		display_error( _("Select a product to enter cost."));
		return false;
	}
	if(!get_post('final_total_cost')) 
	{
		$input_error = 1;
		display_error( _("Total Final cost cannot be empty."));
		return false;
	}

	//check to prevent from double entry in DB
	$check_exist = is_exist_final_cost($_POST['final_finish_code_id']);
	
	if($check_exist)
	{
		$input_error = 1;
	}

	if ($input_error != 1 )
	{
		begin_transaction();
		insert_final_cost();
		unset_final_cost_variables();
		display_notification(_("Final Cost has been saved succcessfully!!!."));
		$Ajax->activate('_page_body');
		commit_transaction();
	}
} 

start_form();
	get_product_for_final_cost($_POST['category_id'], $range_id); 
end_form();
//end_page(@$_REQUEST['popup']);
?>
