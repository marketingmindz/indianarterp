<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CONSUMABLE_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/consumable_costing_db.inc");
include($path_to_root . "/costing/includes/ui/consumable_costing_ui.inc");
?>

<style>
#design_consumables, #finish_consumables, #fabric_consumables { display:none; }
#proceed {  display:block; cursor:pointer; text-align:center; font-weight:600; font-size:14px; height:20px; width:70px; background-color:#7D9AB0; border:#060F16 1px solid; border-radius:3px; color: #fff;}
	#cancel {  cursor:pointer; text-align:center; font-weight:bold; font-size:14px; height:20px; width:70px; background-color:#999; border:#000 2px solid; border-radius:5px; }
#getConsumables {     margin: 20px; cursor:pointer; text-align:center; font-weight:bold; font-size:14px; height:20px; width:70px; background-color:#999; border:#000 2px solid; border-radius:5px; }

#popup_div
{
	    top: 150px;    left: 33%;
	display:none;position:absolute;max-width:80%;
	min-width:400px;	min-height:150px;
	margin:auto;	background:#FFF;
	border:3px solid #000;
	border-radius:5px;
}

#save_list
{
	display:block; width:150px; cursor:pointer; height:22px; border: #000 2px solid; background-color:#CCC; color: #09F; font-size:18px;
}
#save_list a
{
	text-decoration:none;color: #09F;
}

#save_list_popup
{
	    top: 150px;
    left: 33%;
    display: none;
    position: absolute;
    font-size: 13px;
    max-width: 80%;
    min-width: 400px;
    min-height: 150px;
    margin: auto;
    background: #B4C6D4;
    border: 16px solid #EAE7E7;
    border-radius: 5px;
}

#close
{
	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:5px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}
#close a
{
	text-decoration:none;color: #FFF;
}

#no
{
	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:5px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}

#popup { height:auto; min-height:100px; }
#popup-controls { height:auto; min-height:100px; }

#finish_products
{
	max-height:500px; 
	overflow-y: scroll;
	overflow-x:hidden;
}
.finish_link
{
	text-decoration:underline;
	color:#093;
	cursor:pointer;
}

    .multiselect {
        width: 200px;
		    
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
		width: 200px;
        border: 1px #dadada solid;
		position: absolute;
    z-index: 999999999;
    background-color: #fff;
	max-height:300px;
	overflow:scroll;
	overflow-x:hidden;
	overflow-wrap: break-word;

    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }

 .cost_input { width: 100px; }
 .input-disabled{background-color:#EBEBE4;border:0px;}
 .input-hidden{display: none;}
 .supp_price { width: 200px !important; }
</style>

<?php
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Consumable Costing"), @$_REQUEST['popup'], false, "", $js);

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");
include_once($path_to_root . "/includes/ui_list.inc");


if(isset($_GET['reference_id']))
{
	$ref_id = $_POST['ref_id'] = $_GET['reference_id'];
	$categories = array();
	$cat_name = array();
	$sql = "select * from ".TB_PREF."cons_costing_reference c left join ".TB_PREF."cons_costing_reference_details d on c.ref_id = d.ref_id
		LEFT JOIN ".TB_PREF."master_creation m on m.master_id = d.category_id where c.ref_id = ".db_escape($ref_id);         
	$result = db_query($sql, "could not get reference no details.");
	while($row = db_fetch($result))
	{	
		$categories[] = $row['category_id'];
		$_POST['costing_remark'] = $row['remark'];
		$cat_name[] = $row['master_name'];
	}
	$_POST['category_id'] = $categories;
	$_POST['pro_categories'] = implode(", ",$cat_name);
}

start_form();
display_header();
if(isset($_POST['category_id']))
{
	display_consumable($_POST['category_id']);

}


end_form();
//echo "<div><center><span id='save_list'><a href='#save_list_popup'>Save Costing List</a></span></center></div>";

end_page(@$_REQUEST['popup']);


?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="consumable_costing.js"></script>

<?php

if(isset($_POST['ref_id']))
		{ ?>
				<script type="text/javascript">
				$("#checkboxes :input").attr("disabled","disabled");
				$("#proceed").css("display","none");

				</script>
		<?php }


?>