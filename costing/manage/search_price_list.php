<?php

$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");


$keyword = '%'.$_POST['keyword'].'%';
$type = $_POST['type'];
if($type == "reference_no")
{
	$sql = "SELECT ref_id, reference_no FROM ".TB_PREF."costing_reference WHERE locked = '1' && reference_no LIKE 	".db_escape($keyword)." ORDER BY ref_id LIMIT 0, 15";
	$query = db_query($sql, "Could not find reference no.");
	while($row = db_fetch($query)){

		// put in bold the written text
		$reference_no = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $row['reference_no']);
		// add new option
	    echo '<li onclick="set_item(\''.str_replace("'", "\'", $row['reference_no']).'\','.$row["ref_id"].')">'.$reference_no.'</li>';
	}
}

if($type == "product_code")
{
	$sql = "SELECT finish_pro_id,finish_comp_code FROM ".TB_PREF."finish_product WHERE finish_comp_code LIKE 	".db_escape($keyword)." ORDER BY finish_pro_id LIMIT 0, 15";
	$query = db_query($sql, "Could not find finish_comp_code .");
	while($row = db_fetch($query)){

		// put in bold the written text
		$finish_comp_code = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $row['finish_comp_code']);
		// add new option
	    echo '<li onclick="set_item(\''.str_replace("'", "\'", $row['finish_comp_code']).'\','.$row["finish_pro_id"].')">'.$finish_comp_code.'</li>';
	}
}


if($type == "product_name")
{
	$sql = "SELECT finish_pro_id, finish_product_name,finish_comp_code FROM ".TB_PREF."finish_product WHERE finish_product_name LIKE 	".db_escape($keyword)." ORDER BY finish_pro_id LIMIT 0, 15";
	$query = db_query($sql, "Could not find reference no.");
	while($row = db_fetch($query)){

		// put in bold the written text
		$finish_product_name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $row['finish_product_name']);
		// add new option
	    echo '<li onclick="set_item(\''.str_replace("'", "\'", $row['finish_product_name']).'\','.$row["finish_pro_id"].')">'.$finish_product_name.' - '.$row['finish_comp_code'].'</li>';
	}
}


if($type == "category")
{
	$sql = "SELECT category_id,cat_name FROM ".TB_PREF."item_category WHERE cat_name LIKE 	".db_escape($keyword)." ORDER BY category_id LIMIT 0, 15";
	$query = db_query($sql, "Could not find reference no.");
	while($row = db_fetch($query)){

		// put in bold the written text
		$cat_name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $row['cat_name']);
		// add new option
	    echo '<li onclick="set_item(\''.str_replace("'", "\'", $row['cat_name']).'\','.$row["category_id"].')">'.$cat_name.'</li>';
	}
}
if($type == "range")
{
	$sql = "SELECT id,range_name FROM ".TB_PREF."item_range WHERE range_name LIKE 	".db_escape($keyword)." ORDER BY id LIMIT 0, 15";
	$query = db_query($sql, "Could not find reference no.");
	while($row = db_fetch($query)){

		// put in bold the written text
		$range_name = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $row['range_name']);
		// add new option
	    echo '<li onclick="set_item(\''.str_replace("'", "\'", $row['range_name']).'\','.$row["id"].')">'.$range_name.'</li>';
	}
}

?>