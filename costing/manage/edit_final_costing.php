<?php
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/ui/edit_final_costing_ui.inc");
//include($path_to_root . "/costing/includes/finish_costing_class.inc");
include_once($path_to_root . "/costing/includes/db/edit_costing_db.inc");
include_once($path_to_root . "/includes/ui.inc");
//page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);
//simple_page_mode(true);
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
	


function get_product_for_final_cost(&$finish_code_id)
{
	start_outer_table(TABLESTYLE2, "style=min-width:1100px;min-height:300px;");
	
	table_section(1,"550px;");
	$product = get_product_details($_POST['finish_code_id']);
 	start_table();
		echo '<input type="hidden" id="finish_code_id" name="finish_code_id" value="'.$finish_code_id.'" >';
		text_row(_("Selected Finish Code:"),'finish_comp_code', $product['finish_comp_code'], 30, 30, null,null,null,"id=man_finish_code");
		text_row(_("Product Name:"),'finish_product_name', $product['finish_product_name'], 30, 30, null,null,null,"id=man_finish_code");

		end_table();
		div_start('costing_entry');
			read_final_costing($finish_code_id);
		    display_final_costing_form($finish_code_id);
		div_end();

	end_outer_table(1);

	div_start('controls');
		submit_center_first('UpdateFinalCost', _("Update Final Costing"), 
		  _('Update Final Costing data'), @$_REQUEST['popup'] ? true : 'default');
	div_end();
}


function final_check_data()
{
	if(!get_post('special_cost_ref')) {
		display_error( _("Special Cost Reference cannot be empty."));
		set_focus('special_cost_ref');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('special_amount', 0))
    {
	   	display_error(_("Special amount entered must be numeric and not less than zero."));
		set_focus('special_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_final_variables() {
    unset($_POST['special_cost_ref']);
    unset($_POST['special_rate']);
    unset($_POST['special_amount']);
	unset($_POST['total']);
}




if (isset($_POST['UpdateFinalCost'])) 
{
	$input_error = 0;
	if(!get_post('final_total_cost')) 
	{
		$input_error = 1;
		display_error( _("Total Final cost cannot be empty."));
		return false;
	}
	if ($input_error !=1 )
	{
		begin_transaction();
		update_final_cost();
		display_notification(_("Final Cost has been Updated succcessfully!!!."));
		$Ajax->activate('_page_body');
		commit_transaction();
	}
} 




start_form();
	get_product_for_final_cost($finish_code_id); 
end_form();
//end_page(@$_REQUEST['popup']);
?>