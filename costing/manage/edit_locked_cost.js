// JavaScript Document

/* function to tuncate number by two digits after decimal number */
Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf().toFixed(2);
};


$(document).on("click", "#lock", function() {
    $("#lock_popup").show().focus();
    //alert(data);
});

$(document).on("click", "#no", function() {
    $("#lock_popup").hide();
});


$("body").on("mousemove", function(event) {
    if (event.pageX < 50 && event.pageY < 120 && event.pageY > 60) {
        $('.fa-side').show();
    }
    if (event.pageX > 230 && event.pageY > 100) {
        $('.fa-side').hide();
    }
});


/*calculate final costing */
function calculate_final_costing(self) {
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    var total_finishing_cost = $(self + "#total_finishing_cost").val();
    if (total_finishing_cost == '') {
        var total_finishing_cost = 0;
    }
    var sanding_percent = $(self + "#sanding_percent").val();
    if (sanding_percent == '') {
        var sanding_percent = 0;
    }
    var polish_percent = $(self + "#polish_percent").val();
    if (polish_percent == '') {
        var polish_percent = 0;
    }
    var packaging_percent = $(self + "#packaging_percent").val();
    if (packaging_percent == '') {
        var packaging_percent = 0;
    }
    var forwarding_percent = $(self + "#forwarding_percent").val();
    if (forwarding_percent == '') {
        var forwarding_percent = 0;
    }

    var total_percent = parseFloat(sanding_percent) + parseFloat(polish_percent) + parseFloat(packaging_percent) + parseFloat(forwarding_percent);
    $(self + "#total_percent").val(total_percent.toFixedDown(2));
    /*
        var finishing_exp_percent = $(self + "#finishing_exp_percent").val();
        alert(finishing_exp_percent);
        if (finishing_exp_percent == '') {
            var finishing_exp_percent = 0;
        }
        var final_finishing_cost = 0;
        final_finishing_cost = (total_mfrg_cost * finishing_exp_percent) / 100;

        if (final_finishing_cost == '') {
            var final_finishing_cost = 0;
        }

        $(self + "#final_finishing_cost").val(final_finishing_cost.toFixedDown(2));*/

    var total_finishing_cost = $(self + "#total_finishing_cost").val();
    if (total_finishing_cost == '') {
        var total_finishing_cost = 0;
    }

    var total_cost = parseFloat(total_finishing_cost) + parseFloat(total_mfrg_cost);
    $(self + "#total_fcost").val(total_cost.toFixedDown(2));

    var final_profit_percent = $(self + "#final_profit_percent").val();
    if (final_profit_percent == '') {
        var final_profit_percent = 0;
    }
    var final_profit = 0;
    final_profit = (total_cost * final_profit_percent) / 100;
    $(self + "#final_profit").val(final_profit.toFixedDown(2));

    var final_total_cost = parseFloat(total_cost) + parseFloat(final_profit);
    $(self + "#final_total_cost").val(final_total_cost.toFixedDown(2));

    $.ajax({
        url: "convert_currency.php",
        method: "POST",
        dataType: "json",
        async: false,
        data: { total_cost: final_total_cost },
        success: function(data) {
            var total_currency = $(self + "input[name=total_currency]").val();
            var currencies = $(self + "input[name=currencies]").val();
            var curr = currencies.split(",");
            for (var k = 0; k < total_currency; k++) {
                $(self + "#currency" + curr[k]).val(data[k].toFixedDown(2));
            }
        }
    });
    var wood_cost = [];
    var i = 0;
    $(self + ".wood_cost tr").each(function() {
        if ($(this).find(":input.w_in").length) {
            wood_cost.push($(this).find(":input.w_in").serialize());
        }
    });

    var labour_cost = [];
    $(self + ".special_labour_cost tr").each(function() {
        if ($(this).find(":input.l_in").length) {
            labour_cost.push($(this).find(":input.l_in").serialize());
        }
    });


    var fs_cost = [];
    $(self + ".special_finishing_cost tr").each(function() {
        if ($(this).find(":input.f_in").length) {
            fs_cost.push($(this).find(":input.f_in").serialize());
        }
    });

    var version = $("#version").val();
    var ref_id = $("#reference_id").val();
    $.ajax({
        url: "update_locked_costing.php",
        async: false,
        method: "POST",
        data: { ref_id: ref_id, version: version, cost: $(self + ":input").serialize(), wood_cost: wood_cost, labour_cost: labour_cost, fs_cost: fs_cost },
        success: function(data) {
            alert("Costing Updated.");
        }
    });

}

/*calculate finishing cost*/
function calculate_finishing_cost(self) {
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    var sanding_percent = $(self + "#sanding_percent").val();
    if (sanding_percent == '') {
        var sanding_percent = 0;
    }
    var sanding_labour = $(self + "#sanding_labour").val();
    if (sanding_labour == '') {
        var sanding_labour = 0;
    }
    var sanding_cost = 0;
    sanding_cost = (total_mfrg_cost * sanding_percent) / 100;
    var sanding_cost = parseFloat(sanding_cost, 10) + parseFloat(sanding_labour, 10);
    $(self + "#sanding_cost").val(sanding_cost.toFixedDown(2));

    var polish_percent = $(self + "#polish_percent").val();
    if (polish_percent == '') {
        var polish_percent = 0;
    }
    var polish_labour = $(self + "#polish_labour").val();
    if (polish_labour == '') {
        var polish_labour = 0;
    }
    var polish_cost = 0;
    polish_cost = (total_mfrg_cost * polish_percent) / 100;
    var polish_cost = parseFloat(polish_cost, 10) + parseFloat(polish_labour, 10);
    $(self + "#polish_cost").val(polish_cost.toFixedDown(2));

    var packaging_percent = $(self + "#packaging_percent").val();
    if (packaging_percent == '') {
        var packaging_percent = 0;
    }
    var packaging_labour = $(self + "#packaging_labour").val();
    if (packaging_labour == '') {
        var packaging_labour = 0;
    }
    var packaging_cost = 0;
    packaging_cost = (total_mfrg_cost * packaging_percent) / 100;
    var packaging_cost = parseFloat(packaging_cost, 10) + parseFloat(packaging_labour, 10);
    $(self + "#packaging_cost").val(packaging_cost.toFixedDown(2));

    var actual_sending_cost = $(self + "#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $(self + "#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $(self + "#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }

    var lumsum_cost_percent = $(self + "#lumsum_cost_percent").val();
    if (lumsum_cost_percent == '') {
        var lumsum_cost_percent = 0;
    }
    var lumsum_cost_value = 0;
    lumsum_cost_value = (total_mfrg_cost * lumsum_cost_percent) / 100;
    $(self + "#lumsum_cost_value").val(lumsum_cost_value.toFixedDown(2));

    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $(self + "#total_average").html(total_average.toFixedDown(2));


    var forwarding_percent = $(self + "#forwarding_percent").val();
    if (forwarding_percent == '') {
        var forwarding_percent = 0;
    }
    var forwarding_cost = 0;
    forwarding_cost = (total_mfrg_cost * forwarding_percent) / 100;
    $(self + "#forwarding_cost").val(forwarding_cost.toFixedDown(2));

    var total_special_cost = $(self + "#total_finishing_special_cost").val();
    if (total_special_cost == '') {
        var total_special_cost = 0;
    }
    var other_cost = $(self + "#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }

    /*total_finishing_cost = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost) +parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost); 		
    $(self+"#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $(self+"#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));*/

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $(self + "#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $(self + "#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $(self + "#total_finishing_cost").html(total_finishing_cost.toFixedDown(2));

    calculate_final_costing(self);
    /*finish_costing();
    final_cost();*/
}

/* calculate manufacturing cost  */
function calculate_mfrg_cost(self) {
    var total_wood_cost = $(self + "#total_wood_cost").val();
    if (total_wood_cost == '') {
        var total_wood_cost = 0;
    }
    var total_labour_cost = $(self + "#total_labour_cost").val();
    if (total_labour_cost == '') {
        var total_labour_cost = 0;
    }
    var wood_plus_labour = parseFloat(total_wood_cost) + parseFloat(total_labour_cost);

    var percent = $(self + "#extra_percent").val();
    if (percent == '') {
        var percent = 0;
    }
    var extra_amount = 0;
    extra_amount = (wood_plus_labour * percent) / 100;
    $(self + "#extra_amount").val(extra_amount.toFixedDown(2));

    var total_cost = parseFloat(wood_plus_labour) + parseFloat(extra_amount);
    $(self + "#total_cost_label").html(total_cost.toFixedDown(2));
    $(self + "#total_cost").val(total_cost.toFixedDown(2));


    var admin_percent = $(self + "#admin_percent").val();
    if (admin_percent == '') {
        var admin_percent = 0;
    }
    var admin_cost = 0;
    admin_cost = (total_cost * admin_percent) / 100;
    $(self + "#admin_cost").val(admin_cost.toFixedDown(2));

    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $(self + "#total_cp_label").html(total_cp.toFixedDown(2));
    $(self + "#total_cp").val(total_cp.toFixedDown(2));


    var profit_percent = $(self + "#profit_percent").val();
    if (profit_percent == '') {
        var profit_percent = 0;
    }
    var profit = 0;
    profit = (total_cp * profit_percent) / 100;
    $(self + "#profit").val(profit.toFixedDown(2));

    var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
    $(self + "#total_mfrg_label").html(total_mfrg.toFixedDown(2));
    $(self + "#total_mfrg").val(total_mfrg.toFixedDown(2));

    calculate_finishing_cost(self);
    /*finish_costing();
    final_cost();*/
}



function calculate_wood_costing(sel, id) {
    var total_wood_cost = 0;
    $(sel + "input[name^=amount]").each(function(index, element) {
        total_wood_cost += parseFloat($(this).val());
    });
    $(sel + "#total_wood_cost").val(total_wood_cost.toFixedDown(2));
    calculate_mfrg_cost(sel);
}




$(document).on('change', 'input[name^=cft]', function() {
    var cft = $(this).val();
    if (cft == '') {
        var cft = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var rate = $(self + "#rate" + k).val();
    if (rate == '') {
        var rate = 0;
    }
    var amount = 0;
    amount = cft * rate;
    if (!isNaN(amount))
        $(self + "#amount" + k).val(amount);
    calculate_wood_costing(self, id);

});

$(document).on('change', 'input[name^=rate]', function() {
    var rate = $(this).val();
    if (rate == '') {
        var rate = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var cft = $(self + "#cft" + k).val();
    if (cft == '') {
        var cft = 0;
    }
    var amount = 0;
    amount = cft * rate;
    $(self + "#amount" + k).val(amount);
    calculate_wood_costing(self, id);

});

$(document).on('change', 'input[name^=amount]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var rate = $(self + "#rate" + k).val();
    if (rate == '') {
        var rate = 0;
    }
    var cft = $(self + "#cft" + k).val();
    if (cft == '') {
        var cft = 0;
    }
    var amount = 0;
    amount = cft * rate;
    $(self + "#amount" + k).val(amount);
    calculate_wood_costing(self, id);
});
$(document).on('change', 'input[name^=total_wood_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_wood_costing(self, id);
});



/* calculate labour cost */
function calculate_labour_cost(sel) {
    var total_special_labour_cost = 0;
    $(sel + "input[name^=special_labour_cost]").each(function(index, element) {
        total_special_labour_cost += parseFloat($(this).val());
    });
    $(sel + "#total_special_labour_cost").val(total_special_labour_cost.toFixedDown(2));
    var basic_price = $(sel + "#basic_price").val();

    var total_labour_cost = parseFloat(basic_price) + parseFloat(total_special_labour_cost);
    $(sel + "#total_labour_cost").val(total_labour_cost.toFixedDown(2));
    $(sel + "#total_labour_cost_label").html(total_labour_cost.toFixedDown(2));
    calculate_mfrg_cost(sel);
}
$(document).on('change', 'input[name^=labour_amount]', function() {
    var labour_amount = $(this).val();
    if (labour_amount == '') {
        var labour_amount = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[2];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var special_rate = $(self + "#special_rate" + k).val();
    if (special_rate == '') {
        var special_rate = 0;
    }
    var special_labour_cost = 0;

    special_labour_cost = labour_amount * special_rate;
    $(self + "#special_labour_cost" + k).val(special_labour_cost.toFixedDown(2));
    calculate_labour_cost(self);
});
$(document).on('change', 'input[name^=special_rate]', function() {
    var special_rate = $(this).val();
    if (special_rate == '') {
        var special_rate = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[2];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var labour_amount = $(self + "#labour_amount" + k).val();
    if (labour_amount == '') {
        var labour_amount = 0;
    }
    var special_labour_cost = 0;

    special_labour_cost = labour_amount * special_rate;
    $(self + "#special_labour_cost" + k).val(special_labour_cost.toFixedDown(2));
    calculate_labour_cost(self);

});
$(document).on('change', 'input[name^=special_labour_cost]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[3];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var labour_amount = $(self + "#labour_amount" + k).val();
    if (labour_amount == '') {
        var labour_amount = 0;
    }
    var special_rate = $(self + "#special_rate" + k).val();
    if (special_rate == '') {
        var special_rate = 0;
    }
    var special_labour_cost = 0;
    special_labour_cost = labour_amount * special_rate;

    $(self + "#special_labour_cost" + k).val(special_labour_cost.toFixedDown(2));
    calculate_labour_cost(self);

});
$(document).on('change', 'input[name^=total_special_labour_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_labour_cost(self);

});

/*basic price changes*/
$(document).on('change', 'input[name=basic_price]', function() {
    var basic_price = $(this).val();
    if (basic_price == '') {
        var basic_price = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var total_special_labour_cost = $(self + "#total_special_labour_cost").val();
    if (total_special_labour_cost == '') {
        var total_special_labour_cost = 0;
    }
    var total_labour_cost = 0;
    total_labour_cost = parseFloat(basic_price) + parseFloat(total_special_labour_cost);
    $(self + "#total_labour_cost_label").html(total_labour_cost.toFixedDown(2));
    $(self + "#total_labour_cost").val(total_labour_cost.toFixedDown(2));

    calculate_mfrg_cost(self);
});

/*extra_percent change*/
$(document).on('change', 'input[name=extra_percent]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_mfrg_cost(self);
});
$(document).on('change', 'input[name=extra_amount]', function() {
    var extra_amount = $(this).val();
    if (extra_amount == '') {
        var extra_amount = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var extra_percent = 0;
    var total_wood_cost = $(self + "#total_wood_cost").val();
    if (total_wood_cost == '') {
        var total_wood_cost = 0;
    }
    var total_labour_cost = $(self + "#total_labour_cost").val();
    if (total_labour_cost == '') {
        var total_labour_cost = 0;
    }
    var wood_plus_labour = parseFloat(total_wood_cost) + parseFloat(total_labour_cost);
    extra_percent = (extra_amount * 100) / wood_plus_labour;
    $(self + "#extra_percent").val(extra_percent.toFixedDown(2));


    calculate_mfrg_cost(self);
});



/*admin_percent changes*/
$(document).on('change', 'input[name=admin_percent]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_mfrg_cost(self);
});
$(document).on('change', 'input[name=admin_cost]', function() {
    var admin_cost = $(this).val();
    if (admin_cost == '') {
        var admin_cost = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var admin_percent = 0;
    var total_cost = $(self + "#total_cost").val();
    if (total_cost == '') {
        var total_cost = 0;
    }
    admin_percent = (admin_cost * 100) / total_cost;
    $(self + "#admin_percent").val(admin_percent.toFixedDown(2));

    calculate_mfrg_cost(self);
});


/*profit perfcent changes*/
$(document).on('change', 'input[name=profit_percent]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_mfrg_cost(self);
});
$(document).on('change', 'input[name=profit]', function() {
    var profit = $(this).val();
    if (profit == '') {
        var profit = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var profit_percent = 0;
    var total_cp = $(self + "#total_cp").val();
    if (total_cp == '') {
        var total_cp = 0;
    }
    profit_percent = (profit * 100) / total_cp;
    $(self + "#profit_percent").val(profit_percent.toFixedDown(2));

    calculate_mfrg_cost(self);
});

/* calculate finishing cost */

function special_finishing_cost(sel) {
    var total_finishing_special_cost = 0;
    $(sel + "input[name^=special_ftotal]").each(function(index, element) {
        total_finishing_special_cost += parseFloat($(this).val());
    });
    $(sel + "#total_finishing_special_cost").val(total_finishing_special_cost.toFixedDown(2));

    calculate_finishing_cost(sel);
}
$(document).on('change', 'input[name^=special_frate]', function() {
    var special_frate = $(this).val();
    if (special_frate == '') {
        var special_frate = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[2];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var special_famount = $(self + "#special_famount" + k).val();
    if (special_famount == '') {
        var special_famount = 0;
    }
    var special_ftotal = 0;

    special_ftotal = special_famount * special_frate;
    $(self + "#special_ftotal" + k).val(special_ftotal.toFixedDown(2));
    special_finishing_cost(self);
});
$(document).on('change', 'input[name^=special_famount]', function() {
    var special_famount = $(this).val();
    if (special_famount == '') {
        var special_famount = 0;
    }
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[2];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var special_frate = $(self + "#special_frate" + k).val();
    if (special_frate == '') {
        var special_frate = 0;
    }
    var special_ftotal = 0;

    special_ftotal = special_famount * special_frate;
    $(self + "#special_ftotal" + k).val(special_ftotal.toFixedDown(2));
    special_finishing_cost(self);

});
$(document).on('change', 'input[name^=special_ftotal]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[2];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    var special_famount = $(self + "#special_famount" + k).val();
    if (special_famount == '') {
        var special_famount = 0;
    }
    var special_frate = $(self + "#special_frate" + k).val();
    if (special_frate == '') {
        var special_frate = 0;
    }
    var special_ftotal = 0;
    special_ftotal = special_famount * special_frate;

    $(self + "#special_ftotal" + k).val(special_ftotal.toFixedDown(2));
    special_finishing_cost(self);

});
$(document).on('change', 'input[name^=total_finishing_special_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    special_finishing_cost(self);

});




/*sanding_percent changes*/
$(document).on('change', 'input[name=sanding_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=sanding_labour]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=polish_labour]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=packaging_labour]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=actual_sending_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=actual_polish_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=actual_packaging_cost]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=lumsum_cost_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=lumsum_cost_value]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});

$(document).on('change', 'input[name=sanding_cost]', function() {
    var sanding_cost = $(this).val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var sanding_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    sanding_percent = (sanding_cost * 100) / total_mfrg_cost;
    $(self + "#sanding_percent").val(sanding_percent.toFixedDown(2));

    calculate_finishing_cost(self);
});


/*polish_percent changes*/
$(document).on('change', 'input[name=polish_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});
$(document).on('change', 'input[name=polish_cost]', function() {
    var polish_cost = $(this).val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var polish_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    polish_percent = (polish_cost * 100) / total_mfrg_cost;
    $(self + "#polish_percent").val(polish_percent.toFixedDown(2));

    calculate_finishing_cost(self);
});

/*packaging_percent changes*/
$(document).on('change', 'input[name=packaging_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});
$(document).on('change', 'input[name=packaging_cost]', function() {
    var packaging_cost = $(this).val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var packaging_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    packaging_percent = (packaging_cost * 100) / total_mfrg_cost;
    $(self + "#packaging_percent").val(packaging_percent.toFixedDown(2));

    calculate_finishing_cost(self);
});



/*forwarding_percent changes*/
$(document).on('change', 'input[name=forwarding_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});
$(document).on('change', 'input[name=forwarding_cost]', function() {
    var forwarding_cost = $(this).val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var forwarding_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    forwarding_percent = (forwarding_cost * 100) / total_mfrg_cost;
    $(self + "#forwarding_percent").val(forwarding_percent.toFixedDown(2));

    calculate_finishing_cost(self);
});


/*  other cost changes */
$(document).on('change', 'input[name=other_cost]', function() {
    var id = $(this).attr('id');
    var k = "_" + id.split("_")[1];
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_finishing_cost(self);
});


/*  final_profit_percent changes */
$(document).on('change', 'input[name=final_profit_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_final_costing(self);
});

/*  final_profit changes */
$(document).on('change', 'input[name=final_profit]', function() {
    var final_profit = $(this).val();
    if (final_profit == '') {
        var final_profit = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var final_profit_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    var total_finishing_cost = $(self + "#total_finishing_cost").val();
    if (total_finishing_cost == '') {
        var total_finishing_cost = 0;
    }
    var total_amount = parseFloat(total_mfrg_cost) + parseFloat(total_finishing_cost);
    final_profit_percent = (final_profit * 100) / total_amount;
    $(self + "#final_profit_percent").val(final_profit_percent.toFixedDown(2));

    calculate_final_costing(self);
});

/*  finishing_exp_percent changes */
$(document).on('change', 'input[name=finishing_exp_percent]', function() {
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";
    calculate_final_costing(self);
});

/*  final_finishing_cost changes */
$(document).on('change', 'input[name=final_finishing_cost]', function() {
    var final_finishing_cost = $(this).val();
    if (final_finishing_cost == '') {
        var final_finishing_cost = 0;
    }
    var id = $(this).closest("tr.finish_product").attr("id");
    var self = "#" + id + " ";

    var finishing_exp_percent = 0;
    var total_mfrg_cost = $(self + "#total_mfrg_cost").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    finishing_exp_percent = (final_finishing_cost * 100) / total_mfrg_cost;
    $(self + "#finishing_exp_percent").val(finishing_exp_percent.toFixedDown(2));

    calculate_final_costing(self);
});

$(document).on('change', '.des_rate', function() {
    var des_rate = $(this).val();
    var id = $(this).attr('id');
    var k = id.split('_');
    var i = k[2];

    var des_cons_qty = $("input[name=des_cons_qty_" + i + "]").val();
    var des_cost = 0;
    var des_cost = des_cons_qty * des_rate;
    $("#des_cost_" + i).val(des_cost.toFixedDown(2));

    var i = 0;
    var total_cons_cost = 0;
    var no = $("#design_cons_no").val();
    for (; i < no; i++) {
        total_cons_cost += parseFloat($("#des_cost_" + i).val());
    }
    $("#total_cons_cost").val(parseFloat(total_cons_cost).toFixedDown(2));
    $("#total_cons_cost_label").html(parseFloat(total_cons_cost).toFixedDown(2));

    var total_finish_cons_cost = $("#total_finish_cons_cost").val();
    if (total_finish_cons_cost == '') {
        var total_finish_cons_cost = 0;
    }
    var total_fabric_cost = $("#total_fabric_cost").val();
    if (total_fabric_cost == '') {
        var total_fabric_cost = 0;
    }
    var total_wood_cost = $("input[name=total_wood_cost]").val();
    if (total_wood_cost == '') {
        var total_wood_cost = 0;
    }
    var total_labour_cost = $("#total_labour_cost").val();
    if (total_labour_cost == '') {
        var total_labour_cost = 0;
    }

    var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost);
    var extra_percent = $("#extra_percent").val();
    if (extra_percent == '') {
        var extra_percent = 0;
    }
    var extra_amount = 0;
    extra_amount = (total_cost * extra_percent) / 100;
    $("#extra_amount").val(extra_amount.toFixedDown(2));
    total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
    $("#total_cost_label").html(total_cost.toFixedDown(2));
    $("#total_cost").val(total_cost.toFixedDown(2));

    var admin_percent = $("#admin_percent").val();
    if (admin_percent == '') {
        var admin_percent = 0;
    }
    var admin_cost = (total_cost * admin_percent) / 100;
    $("#admin_cost").val(admin_cost.toFixedDown(2));

    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var profit_percent = $("#profit_percent").val();
    if (profit_percent == '') {
        var profit_percent = 0;
    }
    var profit = (total_cp * admin_percent) / 100;
    $("#profit").val(profit.toFixedDown(2));

    var admin_cost = $("#admin_cost").val();
    if (admin_cost == '') {
        var admin_cost = 0;
    }
    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
    $("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg").val(total_mfrg.toFixedDown(2));

    $("#total_mfrg_cost_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg_cost").val(total_mfrg.toFixedDown(2));
    finish_costing();
    total_amount();
    final_cost();
});


$(document).on('change', '.finish_cons_rate', function() {
    var finish_cons_rate = $(this).val();
    var id = $(this).attr('id');
    var k = id.split('_');
    var i = k[3];

    var finish_cons_qty = $("input[name=finish_cons_qty_" + i + "]").val();
    var finish_cost = 0;
    var finish_cost = finish_cons_qty * finish_cons_rate;
    $("#finish_cost_" + i).val(finish_cost.toFixedDown(2));

    var i = 0;
    var total_finish_cons_cost = 0;
    var no = $("#finish_cons_no").val();
    for (; i < no; i++) {
        total_finish_cons_cost += parseFloat($("#finish_cost_" + i).val());
    }
    $("#total_finish_cons_cost").val(parseFloat(total_finish_cons_cost).toFixedDown(2));
    $("#total_finish_cons_cost_label").html(parseFloat(total_finish_cons_cost).toFixedDown(2));

    var total_cons_cost = $("#total_cons_cost").val();
    if (total_cons_cost == '') {
        var total_cons_cost = 0;
    }
    var total_fabric_cost = $("#total_fabric_cost").val();
    if (total_fabric_cost == '') {
        var total_fabric_cost = 0;
    }
    var total_wood_cost = $("input[name=total_wood_cost]").val();
    if (total_wood_cost == '') {
        var total_wood_cost = 0;
    }
    var total_labour_cost = $("#total_labour_cost").val();
    if (total_labour_cost == '') {
        var total_labour_cost = 0;
    }

    var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost);
    var extra_percent = $("#extra_percent").val();
    if (extra_percent == '') {
        var extra_percent = 0;
    }
    var extra_amount = 0;
    extra_amount = (total_cost * extra_percent) / 100;
    $("#extra_amount").val(extra_amount.toFixedDown(2));
    total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
    $("#total_cost_label").html(total_cost.toFixedDown(2));
    $("#total_cost").val(total_cost.toFixedDown(2));

    var admin_percent = $("#admin_percent").val();
    if (admin_percent == '') {
        var admin_percent = 0;
    }
    var admin_cost = (total_cost * admin_percent) / 100;
    $("#admin_cost").val(admin_cost.toFixedDown(2));

    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var profit_percent = $("#profit_percent").val();
    var profit = (total_cp * admin_percent) / 100;
    $("#profit").val(profit.toFixedDown(2));

    var admin_cost = $("#admin_cost").val();
    if (admin_cost == '') {
        var admin_cost = 0;
    }
    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
    $("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg").val(total_mfrg.toFixedDown(2));

    $("#total_mfrg_cost_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg_cost").val(total_mfrg.toFixedDown(2));
    finish_costing();
    total_amount();
    final_cost();
});

$(document).on('change', '.fabric_rate', function() {
    var fabric_rate = $(this).val();
    var id = $(this).attr('id');
    var k = id.split('_');
    var i = k[2];

    var perecentage = $("input[name=perecentage_" + i + "]").val();
    var fabric_cost = 0;
    var fabric_cost = perecentage * fabric_rate;
    $("#fabric_cost_" + i).val(fabric_cost.toFixedDown(2));

    var i = 0;
    var total_fabric_cost = 0;
    var no = $("#fabric_cons_no").val();
    for (; i < no; i++) {
        total_fabric_cost += parseFloat($("#fabric_cost_" + i).val());
    }
    $("#total_fabric_cost").val(parseFloat(total_fabric_cost).toFixedDown(2));
    $("#total_fabric_cost_label").html(parseFloat(total_fabric_cost).toFixedDown(2));

    var total_cons_cost = $("#total_cons_cost").val();
    if (total_cons_cost == '') {
        var total_cons_cost = 0;
    }
    var total_finish_cons_cost = $("#total_finish_cons_cost").val();
    if (total_finish_cons_cost == '') {
        var total_finish_cons_cost = 0;
    }
    var total_wood_cost = $("input[name=total_wood_cost]").val();
    if (total_wood_cost == '') {
        var total_wood_cost = 0;
    }
    var total_labour_cost = $("#total_labour_cost").val();
    if (total_labour_cost == '') {
        var total_labour_cost = 0;
    }

    var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost);
    var extra_percent = $("#extra_percent").val();
    if (extra_percent == '') {
        var extra_percent = 0;
    }
    var extra_amount = 0;
    extra_amount = (total_cost * extra_percent) / 100;
    $("#extra_amount").val(extra_amount.toFixedDown(2));
    total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
    $("#total_cost_label").html(total_cost.toFixedDown(2));
    $("#total_cost").val(total_cost.toFixedDown(2));

    var admin_percent = $("#admin_percent").val();
    if (admin_percent == '') {
        var admin_percent = 0;
    }
    var admin_cost = (total_cost * admin_percent) / 100;
    $("#admin_cost").val(admin_cost.toFixedDown(2));

    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var profit_percent = $("#profit_percent").val();
    if (profit_percent == '') {
        var profit_percent = 0;
    }
    var profit = (total_cp * admin_percent) / 100;
    $("#profit").val(profit.toFixedDown(2));

    var admin_cost = $("#admin_cost").val();
    if (admin_cost == '') {
        var admin_cost = 0;
    }
    var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
    $("#total_cp_label").html(total_cp.toFixedDown(2));
    $("#total_cp").val(total_cp.toFixedDown(2));

    var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
    $("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg").val(total_mfrg.toFixedDown(2));

    $("#total_mfrg_cost_label").html(total_mfrg.toFixedDown(2));
    $("#total_mfrg_cost").val(total_mfrg.toFixedDown(2));
    finish_costing();
    total_amount();
    final_cost();

});
