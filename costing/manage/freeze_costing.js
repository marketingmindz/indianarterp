// JavaScript Document
/* function to tuncate number by two digits after decimal number */
	Number.prototype.toFixedDown= function(digits) {
	    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
	        m = this.toString().match(re);
	    return m ? parseFloat(m[1]) : this.valueOf();
	};



$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
	var slc_master = $(this).val();
	$.ajax({
		url: "sub_master_calling.php",
		method: "POST",
		data: { id : slc_master},
		success: function(data){
				var select_val = $('#sub_master');
				//alert(data);
				select_val.empty().append(data);
			}
	});
	return false;
});
	

	$(document).on('change','#slc_category',function(){
		 //alert( this.value ); // or $(this).val()
		  $('#collection_id').val('SelectCollection');
		var catval = parseInt(this.value);
		if ($.inArray(catval, [28, 41, 60, 64, 77, 79]) > -1) {
			
			  $('#tr_structure_wood').show();
			 $('#tr_no_setting').show();	
		}else{
			
			 $('#tr_structure_wood').hide();
			 $('#tr_no_setting').hide();
			 $('#no_setting').val('');
			 $('#structure_wood_id').val('');
			 	
		}
	});
	
	$(document).on('change','#collection_id',function(){
		var collect_name = $('#collection_id').val();	
		var cat_id = $('#slc_category').val();
		 if(collect_name == 'NonCollection')
		    {
				$("#range_span").hide();
				$('#range_span').val('-1');
			}else{
				$("#range_span").show();		
				
			}
		 if(collect_name == 'NonCollection' && cat_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php",
			      method: "POST",
                  data: { cid : cat_id },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});	
	
	$(document).on('change','#range_id_code',function(){
		var collect_name = $('#collection_id').val();	
		var range_id = $('#range_id_code').val();
		var categ_id = $('#slc_category').val(); 
		 if(collect_name == 'Collection' && range_id > 0)
		    {
				$.ajax({
			      url: "get_design_code_new.php?op=1",
			      method: "POST",
                  data: { rid : range_id, c_id : categ_id  },
			      success: function(data){
					  $( "option" ).remove( ".design_extra" );
					  $('#sub_design').append(data);
				 }
		       });
			}else{
				$( "option" ).remove( ".design_extra" );
			}
			
	});

$(document).on('change','#sub_design',function(){
		var design_id = $("#sub_design").val();
		$.ajax({
			url: "get_finish_code_new.php",
			method: "POST",
            data: { f_id : design_id },
			success: function(data){
				
					$('#packaging_finish_code').empty().append(data);
				}
		});
		
	});
	$(document).on('change','#wood_name',function(){
		var wood_id = $(this).val();
		if(wood_id != -1)
		{
			$.ajax({
				url: "wood_rate_calling.php",
				method: "POST",
				data: { id : wood_id},
				success: function(data){
					if(data == "false")
					{
						alert("Rate is not available for this Wood");
					}
					else
					{
						$('input[name=rate]').val(data);
					}
				}
			});
		}
		else
		{
			$('input[name=rate]').val('0');
		}
		return false;
	});
	
	$(document).on('blur','input[name=cft]',function(){
		var cft = $(this).val();
		var rate = $("input[name=rate]").val();
		var amount =0;
		amount = cft * rate;
		if(!isNaN(amount))
		$("input[name=amount]").val(amount);
		
	});
		
	$(document).on('blur','input[name=rate]',function(){
		var rate = $(this).val();
		var cft = $("input[name=cft]").val();
		var amount =0;
		amount = cft * rate;
		$("input[name=amount]").val(amount);
		
	});
	
	$(document).on('blur','input[name=amount]',function(){
		var rate = $('input[name=rate]').val();
		var cft = $("input[name=cft]").val();
		var amount =0;
		amount = cft * rate;
		$("input[name=amount]").val(amount);
		
	});
	
	$(document).on('blur','input[name=amount]',function(){
		var rate = $('input[name=rate]').val();
		var cft = $("input[name=cft]").val();
		var amount =0;
		amount = cft * rate;
		$("input[name=amount]").val(amount);
		
	});
	
	$(document).on('blur','input[name=labour_amount]',function(){
		var labour_amount = $(this).val();
		var special_rate = $("input[name=special_rate]").val();
		var special_labour_cost = labour_amount * special_rate;
		$("input[name=special_labour_cost]").val(special_labour_cost);
		
	});
	$(document).on('blur','input[name=special_rate]',function(){
		var special_rate = $(this).val();
		var labour_amount = $("input[name=labour_amount]").val();
		var special_labour_cost = labour_amount * special_rate;
		$("input[name=special_labour_cost]").val(special_labour_cost);
		
	});
	$(document).on('blur','input[name=special_labour_cost]',function(){
		var special_rate = $("input[name=special_rate]").val();
		var labour_amount = $("input[name=labour_amount]").val();
		var special_labour_cost = labour_amount * special_rate;
		$("input[name=special_labour_cost]").val(special_labour_cost);
		
	});
	
	
	$(document).on('change','.des_rate',function(){
		var des_rate = $(this).val();
		var id = $(this).attr('id');
		var k = id.split('_');
		var i = k[2];
		
		var des_cons_qty = $("input[name=des_cons_qty_"+i+"]").val();
		var des_cost = 0;
		var des_cost = des_cons_qty*des_rate;
		$("#des_cost_"+i).val(des_cost.toFixedDown(2));
		
		var i = 0;
		var total_cons_cost = 0;
		var no = $("#design_cons_no").val();
		for(; i < no; i++)
		{
			total_cons_cost += parseFloat($("#des_cost_"+i).val());
		}
		$("#total_cons_cost").val(parseFloat(total_cons_cost).toFixedDown(2));
		$("#total_cons_cost_label").html(parseFloat(total_cons_cost).toFixedDown(2));
		
		var total_finish_cons_cost = $("#total_finish_cons_cost").val();
		var total_fabric_cost = $("#total_fabric_cost").val();
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var total_labour_cost = $("#total_labour_cost").val();
		var extra_amount =  $("#extra_amount").val();
		var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
		$("#total_cost_label").html(total_cost.toFixedDown(2));
		$("#total_cost").val(total_cost.toFixedDown(2));
		$("#admin_percent").val('0');
		$("#admin_cost").val('0');
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});
	
	
	$(document).on('change','.finish_cons_rate',function(){
		var finish_cons_rate = $(this).val();
		var id = $(this).attr('id');
		var k = id.split('_');
		var i = k[3];
		
		var finish_cons_qty = $("input[name=finish_cons_qty_"+i+"]").val();
		var finish_cost = 0;
		var finish_cost = finish_cons_qty*finish_cons_rate;
		$("#finish_cost_"+i).val(finish_cost.toFixedDown(2));
		
		var i = 0;
		var total_finish_cons_cost = 0;
		var no = $("#finish_cons_no").val();
		for(; i < no; i++)
		{
			total_finish_cons_cost += parseFloat($("#finish_cost_"+i).val());
		}
		$("#total_finish_cons_cost").val(parseFloat(total_finish_cons_cost).toFixedDown(2));
		$("#total_finish_cons_cost_label").html(parseFloat(total_finish_cons_cost).toFixedDown(2));
		
		var total_cons_cost = $("#total_cons_cost").val();
		var total_fabric_cost = $("#total_fabric_cost").val();
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var total_labour_cost = $("#total_labour_cost").val();
		var extra_amount =  $("#extra_amount").val();
		var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
		$("#total_cost_label").html(total_cost.toFixedDown(2));
		$("#total_cost").val(total_cost.toFixedDown(2));
		$("#admin_percent").val('0');
		$("#admin_cost").val('0');
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
	});
	
	$(document).on('change','.fabric_rate',function(){
		var fabric_rate = $(this).val();
		var id = $(this).attr('id');
		var k = id.split('_');
		var i = k[2];

		var perecentage = $("input[name=perecentage_"+i+"]").val();
		var fabric_cost = 0;
		var fabric_cost = perecentage*fabric_rate;
		$("#fabric_cost_"+i).val(fabric_cost.toFixedDown(2));
		
		var i = 0;
		var total_fabric_cost = 0;
		var no = $("#fabric_cons_no").val();
		for(; i < no; i++)
		{
			total_fabric_cost += parseFloat($("#fabric_cost_"+i).val());
		}
		$("#total_fabric_cost").val(parseFloat(total_fabric_cost).toFixedDown(2));
		$("#total_fabric_cost_label").html(parseFloat(total_fabric_cost).toFixedDown(2));
		
		var total_cons_cost = $("#total_cons_cost").val();
		var total_finish_cons_cost = $("#total_finish_cons_cost").val();
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var total_labour_cost = $("#total_labour_cost").val();
		var extra_amount =  $("#extra_amount").val();
		var total_cost = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost) + parseFloat(total_wood_cost) + parseFloat(total_labour_cost) + parseFloat(extra_amount);
		$("#total_cost_label").html(total_cost.toFixedDown(2));
		$("#total_cost").val(total_cost.toFixedDown(2));
		$("#admin_percent").val('0');
		$("#admin_cost").val('0');
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
	});
		
	$(document).on('change','#extra_percent',function(){
		var percent = $(this).val();
		
		var total_cons_cost = $("#total_cons_cost").val();
		var total_finish_cons_cost = $("#total_finish_cons_cost").val();
		var total_fabric_cost = $("#total_fabric_cost").val();
		var total_labour_cost = $("input[name=total_labour_cost]").val();
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var wood_plus_labour_plus_cons = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost)+ parseFloat(total_wood_cost) + parseFloat(total_labour_cost);
		var extra_amount =0;
		extra_amount = (wood_plus_labour_plus_cons * percent)/100;
		$("#extra_amount").val(extra_amount.toFixedDown(2));

		var total_cost = parseFloat(wood_plus_labour_plus_cons) + parseFloat(extra_amount);
		$("#total_cost_label").html(total_cost.toFixedDown(2));
		$("#total_cost").val(total_cost.toFixedDown(2));
		$("#admin_percent").val('0');
		$("#admin_cost").val('0');
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});
	$(document).on('change','#extra_amount',function(){
		var extra_amount = $(this).val();
		var percent = 0;
		var total_cons_cost = $("#total_cons_cost").val();
		var total_finish_cons_cost = $("#total_finish_cons_cost").val();
		var total_fabric_cost = $("#total_fabric_cost").val();
		var total_labour_cost = $("input[name=total_labour_cost]").val();
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var wood_plus_labour_plus_cons = parseFloat(total_cons_cost) + parseFloat(total_finish_cons_cost) + parseFloat(total_fabric_cost)+ parseFloat(total_wood_cost) + parseFloat(total_labour_cost);

		percent = (extra_amount * 100)/wood_plus_labour_plus_cons;

		$("#extra_percent").val(percent.toFixedDown(2));
		
		var total_wood_cost = $("input[name=total_wood_cost]").val();
		var total_labour_cost = $("#total_labour_cost").val();
		var total_cost = parseFloat(wood_plus_labour_plus_cons) + parseFloat(extra_amount);
		$("#total_cost_label").html(total_cost.toFixedDown(2));
		$("#total_cost").val(total_cost.toFixedDown(2));

		$("#admin_percent").val('0');
		$("#admin_cost").val('0');
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});	
	
	
	$(document).on('change','#admin_percent',function(){
		var admin_percent = $(this).val();
		var total_cost = $("input[name=total_cost]").val();
		var admin_cost =0;
		admin_cost = (total_cost * admin_percent)/100;
		$("#admin_cost").val(admin_cost.toFixedDown(2));
		
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});
	$(document).on('change','#admin_cost',function(){
		var admin_cost = $(this).val();
		var admin_percent = 0;
		var total_cost = $("input[name=total_cost]").val();
		admin_percent = (admin_cost * 100)/total_cost;

		$("#admin_percent").val(admin_percent.toFixedDown(2));
		
		$("#profit_percent").val('0');
		$("#profit").val('0');
		
		var admin_cost = $("#admin_cost").val();
		var total_cp = parseFloat(total_cost) + parseFloat(admin_cost);
		$("#total_cp_label").html(total_cp.toFixedDown(2));
		$("#total_cp").val(total_cp.toFixedDown(2));
		
		var profit = $("#profit").val();
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		

		
	});	
	
	$(document).on('change','#profit_percent',function(){
		var profit_percent = $(this).val();
		var total_cp = $("input[name=total_cp]").val();

		var profit =0;
		profit = (total_cp * profit_percent)/100;
		$("#profit").val(profit.toFixedDown(2));
		
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});
	$(document).on('change','#profit',function(){
		var profit = $(this).val();
		var profit_percent = 0;
		var total_cp = $("input[name=total_cp]").val();
		profit_percent = (profit * 100)/total_cp;

		$("#profit_percent").val(profit_percent);
		
		var total_mfrg = parseFloat(total_cp) + parseFloat(profit);
		$("#total_mfrg_label").html(total_mfrg.toFixedDown(2));
		$("#total_mfrg").val(total_mfrg.toFixedDown(2));
		
	});	
	
	$(document).on('click','.man_finish_code',function(){
		var man_finish_code = $(this).attr('data');
		var man_finish_code_id = $("input[name="+man_finish_code+"]").val();
		$("input[name=man_finish_code_id]").val(man_finish_code_id);
		$("#man_finish_code").val(man_finish_code);
		$(this).css("color","red");
		
		return true;
		
	});	
	
	$(document).on('click','.finish_code',function(){
		var finish_code = $(this).attr('id');
		var finish_code_id = $("input[name="+finish_code+"]").val();
		$("input[name=finish_code_id]").val(finish_code_id);
		$("#finish_code").val(finish_code);
		$(this).css("color","red");
		
		return true;
		
	});	
	
	$(document).on('click','.final_finish_code',function(){
		var final_finish_code = $(this).attr('id');
		var final_finish_code_id = $("input[name="+final_finish_code+"]").val();
		$("input[name=final_finish_code_id]").val(final_finish_code_id);
		$("#final_finish_code").val(final_finish_code);
		$(this).css("color","red");
		return true;
		
	});	
	
	$(document).on('click','#GetFinishProduct',function(){
		$("#finish_code").val('');
		return true;
		
	});	

    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }

/*$('#checkboxes').click(function(e){
    e.stopPropagation();
});*/

$(document).on('focusout','.selectBox',function(){
		var checkboxes = document.getElementById("checkboxes");
		checkboxes.style.display = "none";
            expanded = false;
		return true;
		
	});
	
$(document).ready(function()
{
    $(".multiselect").mouseup(function(e)
    {
        var subject = $(".multiselect"); 

        if(e.target.id != subject.attr('id') && !subject.has(e.target).length)
        {
            subject.fadeOut();
        }
    });
});

$(document).click(function(e) {

	if($(e.target).attr('class') != 'overSelect')
	{
		if($(e.target).attr('class') != $('.selectBox'))
		{
			if ($(e.target).attr('id') != $('#checkboxes') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = document.getElementById("checkboxes");
				checkboxes.style.display = "none";
				$('#checkboxes').fadeOut('slow');
					expanded = false; 
			}
		}
		}
	}
	/*if (e.target != $('.overSelect')) {
		if (!expanded) { 
				checkboxes.style.display = "block";
				expanded = true;
        }
	else 
	{
    checkboxes.style.display = "none";
        expanded = false;
	}
	}
		return true;*/
});