<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/edit_db.inc");
include($path_to_root . "/costing/includes/ui/edit_ui.inc");
include($path_to_root . "/costing/includes/wood_costing_class.inc");
include($path_to_root . "/costing/includes/labour_costing_class.inc");
?>

<style>
/* Css By Pradeep TR Sharma 07 June 2017 */
table.tablestyle td, table.tablestyle th{
    border: 1px solid #b9b9b9!important; 
}
td.pro_details + td, td.wood_cost table, .special_labour_cost table, td.special_labour_cost, td.special_finishing_cost, td.special_finishing_cost table, td.curre_div, td.curre_div table {
    padding: 0 !important;
    border: 0 !important;
}

.mm-fixed-header{
    display: flex;
}
#display_costing thead tr{
    position: relative;
}
#display_costing thead td{
    display:block;
}
#display_costing thead td:first-child{
      display:initial;
}

#display_costing thead + tbody {
    display: block;
    overflow: auto;
    width: 100%;
    min-height: 130px;
    height: 450;
}

thead.mm-table-head th {
    word-wrap: break-word;
    font-size: 11px;
    color: #133963;
    white-space: pre-wrap;
}

th.head1 {
    width: 168px;
}

th.head1 + th.head2 {
    width: 52px!important;
}
th.head3, th.head4, th.head5,
th.head6, th.head7, th.head9,
th.head10, th.head11, th.head12,
th.head13, th.head14, th.head15,
th.head16, th.head17, th.head18,
th.head19, th.head20, th.head21,
th.head22, th.head23, th.head24,
th.head25, th.head26, th.head27,
th.head28, th.head29, th.head30,
th.head31, th.head32, th.head33,
th.head34, th.head35, th.head36,
th.head37, th.head38, th.head39,
th.head41, th.head42, th.head43,
th.head44, th.head45, th.head46,
th.head47, th.head48, th.head49,
th.head50, th.head51 {
    width: 72px;
}
th.head8, th.head40 {
    width: 130px;
}
th.head52 {
    width: 876px;
}
input.wood_cft, input.wood_rate, input.wood_amount, input#total_wood_cost,
input#basic_price, td.special_labour_cost input, input#total_special_labour_cost,
input#total_labour_cost, input#extra_percent, input#extra_amount, input#admin_percent,
input#admin_cost, input#profit_percent, input#profit, input#sanding_labour,
input#sanding_percent, input#sanding_cost, input#polish_labour, input#polish_percent,
input#polish_cost, input#packaging_labour, input#packaging_percent, input#packaging_cost,
input#actual_sending_cost, input#actual_polish_cost, input#actual_packaging_cost,
input#lumsum_cost_percent, input#lumsum_cost_value, td#total_average, input#forwarding_percent,
input#forwarding_cost, input.mm_s_rate, input.mm_s_famount, input.mm_s_ftotal,
input#total_finishing_special_cost, input#other_cost, input#total_cost, input#total_cost,
input#final_profit_percent, input#final_profit, input#final_total_cost  {
    width: 62px;
}
td.special_labour_cost table tr td:first-child input,
td.special_finishing_cost table tr td:first-child input{
    width: 120px;
}

td.special_labour_cost table tr td:first-child, td.special_finishing_cost table tr td:first-child {
    border-left: 0 !important;
}

td.special_labour_cost table tr td, td.special_finishing_cost table tr td {
    border-top: 0 !important;
}

td.special_labour_cost table tr td:nth-child(4), td.special_finishing_cost table tr td:nth-child(4){
    border-right: 0 !important;
}


td.pro_details td.tableheader + td {
    word-wrap: break-word;
    white-space: pre-wrap;
    display: block;
    width: 70px;
}
#display_costing table.tablestyle
{
    width:100%!important;
}
td.wood_cost table tr td:first-child {
    width: 42px !important;
    display: block;
    word-wrap: break-word;
    white-space: pre-wrap;
}
td.pro_details + td h1 {
    width: 100%;
    display: block;
    position: relative;
    top: 50px;
    left: 20px;
}
td.pro_details td {
    border: 0 !important;
}

/* Css 26 June */
.finish_product
{
    display:flex;
}
td.pro_details {
    width: 158px;
    display: block;
    word-wrap: break-word;
    white-space: pre-wrap;
}
td#total_average, td#total_cost_label, td#total_cp_label, td#total_mfrg_label {
    width: 62px;
    min-width: 62px;
    display: block;
    white-space: pre-wrap;
    word-wrap: break-word;
}
td#total_other_than_process_cost, #total_finishing_cost {
    width: 62px !important;
    display: block;
    word-wrap: break-word;
    min-width: 62px !important;
}
td.no-data-td {
    width: 100%;
}
/* Css 26 June */

/* Css By Pradeep TR Sharma 07 June 2017 */
#lock_popup
{   top: 150px;
    left: 33%;
    display: none;
    position: absolute;
    font-size: 13px;
    max-width: 80%;
    min-width: 400px;
    min-height: 150px;
    margin: auto;
    background: #B4C6D4;
    border: 16px solid #EAE7E7;
    border-radius: 5px;
}
#lock { display:block; background:#CCC; border:1px #000000 solid; padding:5px 10px; }

#controls { text-align:center; display:block; margin:auto; width:90%; height:70px; padding-top:50px;}
#close
{
	cursor:pointer; font-size:15px; margin:50px 25px;border:#FFF 3px solid;     padding: 2px 8px; color:#FFF;	
}
#close a
{
	text-decoration:none;color: #FFF;
}
#no
{
	cursor:pointer; font-size:15px;  margin:50px 25px; border:#FFF 3px solid;      padding: 2px 8px; color:#FFF;	
}
.fa-side {
    display: none;
	position:absolute;
}
.fa-content {
    width: 100% !important;
	float:none !important;
}
tr.tableheader td {
    white-space: nowrap  !important;
	text-align:left;
}

table td, table td * {
    vertical-align: top !important;
}
#inner_div { overflow-y:scroll; overflow-x:hidden; }
#inner_div2 { /* overflow-x:scroll; width:5537px; */ margin-bottom: -16px; }
#inner_table { overflow-y:scroll; max-height:450px;  } 
th.pd{
    width: 172px;
}
/*::-webkit-scrollbar {
    width: 12px;
}*/
 
::-webkit-scrollbar-track {
    /*-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3); */
    border-radius: 10px;
}
 
::-webkit-scrollbar-thumb {
    border-radius: 6px;
    -webkit-box-shadow:inset 0 0 18px #006699; 
}
table.tablestyle {
    width:100% !important;
}
</style>
<div id="output"></div>
<?php
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");


if(isset($_GET['reference_id']))
{	
	$_POST['ref_id'] = $_GET['reference_id'];
}

display_costing_details($_GET['reference_id']);
end_page(@$_REQUEST['popup']);


?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="edit.js"></script>

<script>
$(function () {
    $('#inner_table').on('scroll', function (e) {
        $('#inner_div').scrollLeft($('#inner_table').scrollLeft());
    }); 
    $('#inner_div').on('scroll', function (e) {
        $('#inner_table').scrollLeft($('#inner_div').scrollLeft());
    });
});


/*$(document).on('keydown', '#product_table input', function(e) {
    if (e.which == 9) {
        alert("dsfaff");
    }
});*/

</script>
