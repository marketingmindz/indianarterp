<?php
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
function sync()
{
	$category_id = $_REQUEST['category_id'];
	$range_id = $_REQUEST['range_id'];
	$collection_id = $_REQUEST['collection_id'];
	$date = $_REQUEST['date'];
	
	
	$cat_id = explode(",",$category_id);

	foreach($cat_id as $category)
	{
		$sql1 = "select * from ".TB_PREF."cons_costing_reference c LEFT JOIN ".TB_PREF."cons_costing_reference_details
		d ON c.ref_id = d.ref_id where d.category_id = ".db_escape($category);
		$result = db_query($sql1);
		if(db_num_rows($result) > 0)
		{
			$sql = "select master_name from ".TB_PREF."master_creation where master_id = ".db_escape($category);
		    $result = db_query($sql);
			$cat_name = db_fetch_row($result);
			$message =  "reference no. for ".$cat_name['0']." category is already generated, Please unselect this category.";
			return json_encode(array("reference_no"=>"","error"=>"1","message"=>$message),JSON_UNESCAPED_SLASHES);
		}
	}
	
	
	$datestr = str_replace("/", "", $date);
	$reference_no = 'CONS11-'.$datestr;
	
	$sql = "select ref_id from ".TB_PREF."cons_costing_reference where reference_no = ".db_escape($reference_no);
	$result = db_query($sql);
	if(db_num_rows($result) > 0)
	{
		$ref_id = db_fetch_row($result);
		$ref_id = $ref_id['0'];
		$sql = "select seq from ".TB_PREF."consumable_costing_reference_seq where ref_id = ".db_escape($ref_id);
		$result = db_query($sql);
		$seq = db_fetch_row($result);
		$seq = $seq['0']+1;
		$reference_no = 'CONS'.$seq.'-'.$datestr;
		
		$sql = "update ".TB_PREF."consumable_costing_reference_seq set seq = ".db_escape($seq)." where ref_id = ".db_escape($ref_id);
		db_query($sql);
		
		$categories = "";
		foreach($cat_id as $category)
		{
			$sql = "select master_name from ".TB_PREF."master_creation where master_id = ".db_escape($category);
			$result = db_query($sql);
			$cat_name = db_fetch_row($result);
			$categories = $categories.", ".$cat_name['0'];
		}
		$categories = trim($categories, ',');
		
		$remark = "This pricing list is for category ".$categories;
		
		
		
		$date = date2sql($date);
		$sql = "insert into ".TB_PREF."cons_costing_reference(reference_no,date,remark)  values(".db_escape($reference_no).", ".db_escape($date).", ".db_escape($remark).")";   
		db_query($sql, "reference no. is not inserted#001");
		$ref_id = db_insert_id();

		foreach($cat_id as $category)
		{
			$sql = "insert into ".TB_PREF."cons_costing_reference_details(ref_id,category_id)  values(".db_escape($ref_id).", ".db_escape($category).")";   
			db_query($sql, "reference no. details are not inserted#001");
		}
		
	}
	else
	{
		$categories = "";
		foreach($cat_id as $category)
		{
			$sql = "select master_name from ".TB_PREF."master_creation where master_id = ".db_escape($category);
			$result = db_query($sql);
			$cat_name = db_fetch_row($result);
			$categories = $categories.", ".$cat_name['0'];
		}
		$categories = trim($categories, ',');
		
		$remark = "This pricing list is for category ".$categories;
		
		
		$date = date2sql($date);
		$sql = "insert into ".TB_PREF."cons_costing_reference(reference_no,date,remark)  values(".db_escape($reference_no).", ".db_escape($date).", ".db_escape($remark).")";   
		db_query($sql, "reference no. is not inserted");
		$ref_id = db_insert_id();
		
		$sql = "insert into ".TB_PREF."consumable_costing_reference_seq(ref_id, seq) values(".db_escape($ref_id).", '11')";     
		db_query($sql, "reference no. seq is not inserted.");
		
		foreach($cat_id as $category)
		{
			$sql = "insert into ".TB_PREF."cons_costing_reference_details(ref_id,category_id)  values(".db_escape($ref_id).", ".db_escape($category).")";   
			db_query($sql, "reference no. details are not inserted#001");
		}
	}
	
	

	return json_encode(array("ref_id"=>$ref_id,"reference_no"=>$reference_no,"error"=>"0","message"=>"","remark"=>$remark),JSON_UNESCAPED_SLASHES);
}
echo sync();
?>