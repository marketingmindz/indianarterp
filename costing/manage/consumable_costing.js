// JavaScript Document

/* function to tuncate number by two digits after decimal number */
	Number.prototype.toFixedDown= function(digits) {
	    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
	        m = this.toString().match(re);
	    return m ? parseFloat(m[1]) : this.valueOf();
	};


	
	$(document).on('click','#proceed',function(){

		var category_id = $("input[name^=category_id]:checked");
		var date = $("input[name=date]").val();
		var cat_id = [];
		$("input[name^=category_id]:checked").each(function() {
		   cat_id.push($(this).val());
		 });
		if(category_id.length < 1){
			alert("Select category"); $(this).focus(); return false;
		}
		
		
		$.ajax({
			url: "consumable_costing_reference_no.php",
			dataType:"json",
			type: "GET",
            data:"category_id="+cat_id+"&date="+date,
			success: function(data){
					if(data.error == 1)
					{
						alert(data.message);
					}
					else
					{
						$("#popup_div").show();	
						$("#costing_date").val(date);
						$("#ref_id").val(data.ref_id);
						$("#costing_reference").val(data.reference_no);
						$("#costing_remark").val(data.remark);
					}
				}
		});
		
		
		
		
		
		$("#finish_code").val('');
		
		

		//return false;
		
	});	

$(document).on('click','#cancel',function(){
		var ref_id = $("#ref_id").val();
		$.ajax({
			url: "delete_cons_costing_reference_no.php",
			type: "POST",
            data:"ref_id="+ref_id,
			success: function(data){
					$("#popup_div").hide();
					$("#costing_date").val("");
						$("#costing_reference").val("");
						$("#costing_remark").val("");
				}
		});
		
		

		return false;
		
	});	

$(document).on("click","#save_list", function() {
	
	var ref_id = $("#ref_id").val();
	var range_id = $("#range_id_code").val();
	var category_id = $("input[name^=category_id]:checked");
	var cat_id = [];
	$("input[name^=category_id]:checked").each(function() {
	   cat_id.push($(this).val());
	 });
	$.ajax({
		url: "display_entered_costing_details.php",
		type: "POST",
		dataType:"json",
		data:"ref_id="+ref_id+"&category_id="+cat_id+"&range_id="+range_id,
		success: function(data){
				$("#save_list_popup").show().focus();
				//alert(data);
				$("#save_list_content").html("<center><h3>Saved Costing = "+data.saved+"</h3><h3>Pending Costing = "+data.pending+"</h3><h3>Total Costing = "+data.products+"</h3></center>");
			}
	});	

});		  
		  
$(document).on("click","#close", function() {
	$("#save_list_popup").hide();
	var host = window.location.hostname;
	var url = host+"/costing/manage/costing.php";
	window.onbeforeunload = null;
	//$(location).attr('href',url);
});	
$(document).on("click","#no", function() {
	$("#save_list_popup").hide();
});

    var expanded = false;
    function showCheckboxes() {
        var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.style.display = "block";
            expanded = true;
        } else {
            checkboxes.style.display = "none";
            expanded = false;
        }
    }

/*$('#checkboxes').click(function(e){
    e.stopPropagation();
});*/

$(document).on('focusout','.selectBox',function(){
		var checkboxes = document.getElementById("checkboxes");
		checkboxes.style.display = "none";
            expanded = false;
		return true;
		
	});
	
$(document).ready(function()
{
    $(".multiselect").mouseup(function(e)
    {
        var subject = $(".multiselect"); 

        if(e.target.id != subject.attr('id') && !subject.has(e.target).length)
        {
            subject.fadeOut();
        }
    });
});

$(document).click(function(e) {

	if($(e.target).attr('class') != 'overSelect')
	{
		if($(e.target).attr('class') != $('.selectBox'))
		{
			if ($(e.target).attr('id') != $('#checkboxes') && !$('.multiselect').has(e.target).length) {
				if(expanded) {
					var checkboxes = document.getElementById("checkboxes");
				checkboxes.style.display = "none";
				$('#checkboxes').fadeOut('slow');
					expanded = false; 
			}
		}
		}
	}
	
});


$(document).on('click','#select_all',function(){
		if(this.checked){
            $('.check').each(function(){
                this.checked = true;
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
            });
        }else{
             $('.check').each(function(){
                this.checked = false;
				
				
				var arr = [];
				$.each($("input[name^='category_id']:checked"), function(){            
					arr.push($(this).val());
			  });
			  var count = arr.length;
			  //alert(count);
			  var prod = [];
			  var id;
			  for(var j = 0; j<count; j++)
					{
						id = arr[j];
						value = $("#"+id).val();
						prod.push(value);
					}
			  $('#pro_categories').val(prod.join(", "));
			  $('#pro_cat').html(prod.join(", "));
				
				
            });
        }
	});



$('body').on('click','.check', function() {
	
	if($('.check:checked').length == $('.check').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
		
	
	var arr = [];
	$.each($("input[name^='category_id']:checked"), function(){            
    	arr.push($(this).val());
  });
  var count = arr.length;
  //alert(count);
  var prod = [];
  var id;
  for(var j = 0; j<count; j++)
		{
			id = arr[j];
			value = $("#"+id).val();
			prod.push(value);
		}
  $('#pro_categories').val(prod.join(", "));
  $('#pro_cat').html(prod.join(", "));
});


$(document).on("click","#getConsumables", function() {
	  window.onbeforeunload = null;
	  }); 


  window.onbeforeunload = function(e) {
	  if($(e.target).attr('class') != 'finish_code' && $(e.target).attr('class') != 'ajaxsubmit' && $(e.target).attr('class') != 'man_finish_code' && $(e.target).attr('class') != 'ajaxtabs')
	  {
		  return 'You have not saved this costing list.';
	  } 
	 
  };

       
$(document).on('click','.save',function(){
	var id = $(this).closest("tr.cons_row").attr("id");
	var self = "#"+id+" ";
	var category_id = $(self+"input[name='category_id']").val();
	var consumable_id = $(self+"input[name='consumable_id']").val();
	var cost = $(self+"input[name='cost']").val();
	var ref_id = $("#ref_id").val();
	if(cost.length < 1)
	{
		alert("Enter Cost for this Consumable.");
		return false;
	}

	$(this).hide();
	$(self+"input[name='cost']").attr('readonly', true);
	$(self+"input[name='set_price']").attr('disabled', true);
    $(self+"input[name='cost']").addClass('input-disabled');

     $.ajax({
			url: "insert_consumable_costing.php",
			async:false,
			method: "POST",
			data:{ref_id:ref_id, cost:cost,category_id:category_id,consumable_id:consumable_id},
			success: function(data){
				alert("Cost Inserted.");
				}
		});
});	  

function set_max_price(self)
{
	max_price = 0;
	var supp_price = [];
	$(self+"#supp_price option").each(function() { 
    	supp_price.push( $(this).val());
	}); 
	max_price = Math.max.apply(Math,supp_price);
	$(self+"input[name='cost']").val(max_price.toFixedDown(2));
}



/*Update consumable costing*/
$(document).on('click','.update_cost',function(){
	var id = $(this).closest("tr.cons_row").attr("id");
	var self = "#"+id+" ";
	var category_id = $(self+"input[name='category_id']").val();
	var consumable_id = $(self+"input[name='consumable_id']").val();
	var cost = $(self+"input[name='cost']").val();
	var ref_id = $("#ref_id").val();
	if(cost.length < 1)
	{
		alert("Enter Cost for this Consumable.");
		return false;
	}



     $.ajax({
			url: "update_consumable_costing.php",
			async:false,
			method: "POST",
			data:{ref_id:ref_id, cost:cost,category_id:category_id,consumable_id:consumable_id},
			success: function(data){
				alert("Cost Updated.");
				}
		});
});	  
function set_avg_price(self)
{	var i= 0;
	var total_price = 0;
	var avg_price = 0
	var supp_price = [];
	$(self+"#supp_price option").each(function() { 
    	supp_price.push($(this).val());
    	total_price = parseFloat(total_price)+parseFloat($(this).val());
    	i++;
	});
	var avg_price =  parseFloat(total_price/i);
	$(self+"input[name='cost']").val(avg_price.toFixedDown(2));
}

function set_min_price(self)
{
	var min_price = 0;
	var supp_price = [];
	$(self+"#supp_price option").each(function() { 
   		supp_price.push( $(this).val());
	});
	min_price = Math.min.apply(Math,supp_price);
	$(self+"input[name='cost']").val(min_price.toFixedDown(2)); 
}


$(document).on('change','input[name="set_price"]',function(){
		var set_price = $(this).val();
		var id = $(this).closest("tr.cons_row").attr("id");
		var self = "#"+id+" ";
		if(set_price == "max")
		{
			set_max_price(self);
		}
		if(set_price == "avg")
		{
			set_avg_price(self);
		}
		if(set_price == "min")
		{
			set_min_price(self);
		}
		return false;
		
	});	
