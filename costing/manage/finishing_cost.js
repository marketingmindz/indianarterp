// JavaScript Document


/* function to tuncate number by two digits after decimal number */
Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf().toFixed(2);
};


$(document).on('change', '#sanding_percent', function() {
    var sanding_percent = $(this).val();
    if (sanding_percent == '') {
        var sanding_percent = 0;
    }
    var sanding_labour = $("input[name=sanding_labour]").val();
    if (sanding_labour == '') {
        var sanding_labour = 0;
    }
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    var sanding_cost = 0;
    sanding_cost = (total_mfrg_cost * sanding_percent) / 100;
    sanding_cost = parseFloat(sanding_cost, 10) + parseFloat(sanding_labour, 10);
    $("#sanding_cost").val(sanding_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#sanding_labour', function() {
    var sanding_labour = $(this).val();
    var sanding_percent = $("input[name=sanding_percent]").val();
    if (sanding_percent == '') {
        var sanding_percent = 0;
    }
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    if (sanding_labour == '') {
        var sanding_labour = 0;
    }

    var sanding_cost = 0;
    sanding_cost = (total_mfrg_cost * sanding_percent) / 100;
    sanding_cost = parseFloat(sanding_cost, 10) + parseFloat(sanding_labour, 10);
    $("#sanding_cost").val(sanding_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#sanding_cost', function() {
    var sanding_cost = $(this).val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var sanding_percent = 0;
    var sanding_labour = $("input[name=sanding_labour]").val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    if (sanding_labour == '') {
        var sanding_labour = 0;
    }


    sanding_cost = parseFloat(sanding_cost, 10) - parseFloat(sanding_labour, 10);

    sanding_percent = (sanding_cost * 100) / total_mfrg_cost;
    $("#sanding_percent").val(sanding_percent.toFixedDown(2));

    var total_finishing_cost = 0;
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('change', '#polish_percent', function() {
    var polish_percent = $(this).val();
    var polish_labour = $("input[name=polish_labour]").val();
    if (polish_labour == '') {
        var polish_labour = 0;
    }
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    var polish_cost = 0;
    polish_cost = (total_mfrg_cost * polish_percent) / 100;

    polish_cost = parseFloat(polish_cost, 10) + parseFloat(polish_labour, 10);
    $("#polish_cost").val(polish_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#polish_labour', function() {
    var polish_labour = $(this).val();
    var polish_percent = $("input[name=polish_percent]").val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (polish_labour == '') {
        var polish_labour = 0;
    }
    var polish_cost = 0;
    polish_cost = (total_mfrg_cost * polish_percent) / 100;

    polish_cost = parseFloat(polish_cost, 10) + parseFloat(polish_labour, 10);
    $("#polish_cost").val(polish_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#polish_cost', function() {
    var polish_cost = $(this).val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var polish_percent = 0;
    var polish_labour = $("input[name=polish_labour]").val();
    if (polish_labour == '') {
        var polish_labour = 0;
    }
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }
    if (polish_labour == '') {
        var polish_labour = 0;
    }

    polish_cost = parseFloat(polish_cost, 10) - parseFloat(polish_labour, 10);

    polish_percent = (polish_cost * 100) / total_mfrg_cost;
    $("#polish_percent").val(polish_percent.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('change', '#packaging_percent', function() {
    var packaging_percent = $(this).val();
    var packaging_labour = $("input[name=packaging_labour]").val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (packaging_labour == '') {
        var packaging_labour = 0;
    }
    var packaging_cost = 0;
    packaging_cost = (total_mfrg_cost * packaging_percent) / 100;

    packaging_cost = parseFloat(packaging_cost, 10) + parseFloat(packaging_labour, 10);
    $("#packaging_cost").val(packaging_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#packaging_labour', function() {
    var packaging_labour = $(this).val();
    var packaging_percent = $("input[name=packaging_percent]").val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (packaging_labour == '') {
        var packaging_labour = 0;
    }
    var packaging_cost = 0;
    packaging_cost = (total_mfrg_cost * packaging_percent) / 100;

    packaging_cost = parseFloat(packaging_cost, 10) + parseFloat(packaging_labour, 10);
    $("#packaging_cost").val(packaging_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#packaging_cost', function() {
    var packaging_cost = $(this).val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }

    var packaging_percent = 0;
    var packaging_labour = $("input[name=packaging_labour]").val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    if (total_mfrg_cost == '') {
        var total_mfrg_cost = 0;
    }

    if (packaging_labour == '') {
        var packaging_labour = 0;
    }

    packaging_cost = parseFloat(packaging_cost, 10) - parseFloat(packaging_labour, 10);

    packaging_percent = (packaging_cost * 100) / total_mfrg_cost;
    $("#packaging_percent").val(packaging_percent.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('change', '#actual_sending_cost', function() {
    var actual_sending_cost = $(this).val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#actual_polish_cost', function() {
    var actual_polish_cost = $(this).val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#actual_packaging_cost', function() {
    var actual_packaging_cost = $(this).val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('change', '#lumsum_cost_percent', function() {
    var lumsum_cost_percent = $(this).val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    var lumsum_cost_value = 0;
    lumsum_cost_value = (total_mfrg_cost * lumsum_cost_percent) / 100;
    $("#lumsum_cost_value").val(lumsum_cost_value.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#lumsum_cost_value', function() {
    var lumsum_cost_value = $(this).val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var lumsum_cost_percent = 0;
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    lumsum_cost_percent = (lumsum_cost_value * 100) / total_mfrg_cost;
    $("#lumsum_cost_percent").val(lumsum_cost_percent.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }

    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('change', '#forwarding_percent', function() {
    var forwarding_percent = $(this).val();
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    var forwarding_cost = 0;
    forwarding_cost = (total_mfrg_cost * forwarding_percent) / 100;
    $("#forwarding_cost").val(forwarding_cost.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#forwarding_cost', function() {
    var forwarding_cost = $(this).val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var forwarding_percent = 0;
    var total_mfrg_cost = $("input[name=total_mfrg_cost]").val();
    forwarding_percent = (forwarding_cost * 100) / total_mfrg_cost;
    $("#forwarding_percent").val(forwarding_percent.toFixedDown(2));

    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var total_special_cost = $("#total_special_cost").val();
    var other_cost = $("#other_cost").val();
    if (other_cost == '') {
        var other_cost = 0;
    }

    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});

$(document).on('change', '#other_cost', function() {
    var other_cost = $(this).val();
    if (other_cost == '') {
        var other_cost = 0;
    }
    var total_finishing_cost = 0;
    var sanding_cost = $("#sanding_cost").val();
    if (sanding_cost == '') {
        var sanding_cost = 0;
    }
    var polish_cost = $("#polish_cost").val();
    if (polish_cost == '') {
        var polish_cost = 0;
    }
    var packaging_cost = $("#packaging_cost").val();
    if (packaging_cost == '') {
        var packaging_cost = 0;
    }
    var actual_sending_cost = $("#actual_sending_cost").val();
    if (actual_sending_cost == '') {
        var actual_sending_cost = 0;
    }
    var actual_polish_cost = $("#actual_polish_cost").val();
    if (actual_polish_cost == '') {
        var actual_polish_cost = 0;
    }
    var actual_packaging_cost = $("#actual_packaging_cost").val();
    if (actual_packaging_cost == '') {
        var actual_packaging_cost = 0;
    }
    var lumsum_cost_value = $("#lumsum_cost_value").val();
    if (lumsum_cost_value == '') {
        var lumsum_cost_value = 0;
    }
    var forwarding_cost = $("#forwarding_cost").val();
    if (forwarding_cost == '') {
        var forwarding_cost = 0;
    }
    var total_special_cost = $("#total_special_cost").val();

    var total1 = 0;
    total1 = parseFloat(sanding_cost) + parseFloat(polish_cost) + parseFloat(packaging_cost);
    $("#total1").html(total1.toFixedDown(2));

    var total2 = 0;
    total2 = parseFloat(actual_sending_cost) + parseFloat(actual_polish_cost) + parseFloat(actual_packaging_cost);
    $("#total2").html(total2.toFixedDown(2));

    var total_average = 0;
    total_average = parseFloat(total1) + parseFloat(total2) + parseFloat(lumsum_cost_value);
    total_average = total_average / 3;
    $("#total_average").html(total_average.toFixedDown(2));

    var total_other_than_process_cost = 0;
    var total_other_than_process_cost = parseFloat(forwarding_cost) + parseFloat(total_special_cost) + parseFloat(other_cost);
    $("#total_other_than_process_cost").val(total_other_than_process_cost.toFixedDown(2));
    $("#total_other_than_process_cost_label").html(total_other_than_process_cost.toFixedDown(2));

    total_finishing_cost = parseFloat(total_average) + parseFloat(total_other_than_process_cost);
    $("#total_finishing_cost").val(total_finishing_cost.toFixedDown(2));
    $("#total_finishing_cost_label").html(total_finishing_cost.toFixedDown(2));
});


$(document).on('blur', 'input[name=special_amount]', function() {
    var special_amount = $(this).val();
    var special_rate = $("input[name=special_rate]").val();
    var total = special_amount * special_rate;
    $("input[name=total]").val(total);

});
$(document).on('blur', 'input[name=special_rate]', function() {
    var special_rate = $(this).val();
    var special_amount = $("input[name=special_amount]").val();
    var total = special_amount * special_rate;
    $("input[name=total]").val(total);

});
$(document).on('blur', 'input[name=total]', function() {
    var special_rate = $("input[name=special_rate]").val();
    var special_amount = $("input[name=special_amount]").val();
    var total = special_amount * special_rate;
    $("input[name=total]").val(total);

});
