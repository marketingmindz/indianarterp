<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_LOCKED_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/locked_costing_ui.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Locked Costing"), @$_REQUEST['popup'], false, "", $js);

function check_form_data()
{
	if(get_post('ref_id') == -1) {
		display_error( _("Please select reference no."));
		set_focus('ref_id');
		return false;
	}
    return true;	
}

start_form(true);
	/*display_header($_SESSION['DCODE']);
	
	div_start("costing_details");
	
		global $Ajax;
		$Ajax->activate('costing_details');
	if(isset($_POST['ref_id']))
	{
		$check = check_form_data();
		if($check){*/
			display_costing_reference();	
		/*}
	}
	div_end();*/
	
end_form();
end_page();
?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script>
$(document).on("click",".create_version", function() {
	$("#popup").show().focus();
	var url = $(this).attr('url');

	$("#redirect_url").attr("href",url);
});		  
	
$(document).on("click","#no", function() {
	$("#popup").hide();
});


</script>
<style>
#costing_details table
{
	font-size:20px !important;
	color:#06F;
	font-weight:bold;
	margin-top:25px;
	margin-bottom:35px;
}
#costing_details td
{
	font-size:14px !important;
}
.button
{
	height:25px;
	min-width:70px;
	margin:10px;
	padding:3px 5px;
	border:1px solid #000;
}
#product { height:100px; }

#save_list
{
	display:block; width:150px; cursor:pointer; height:22px; border: #000 2px solid; background-color:#CCC; color: #09F; font-size:18px;
}
#save_list a
{
	text-decoration:none;color: #09F;
}

#popup
{
	    top: 150px;
    left: 33%;
    display: none;
    position: absolute;
    font-size: 13px;
    max-width: 80%;
    min-width: 400px;
    min-height: 150px;
    margin: auto;
    background: #B4C6D4;
    border: 16px solid #EAE7E7;
    border-radius: 5px;
}

#close
{
	display:block; cursor:pointer; height:20px; float:left; font-size:15px;margin-right:10px;width:35px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}
#close a
{
	text-decoration:none;color: #FFF;
}

#no
{
	display:block; cursor:pointer; height:20px; float:left; font-size:15px;  margin-left:30px;  width:23px; border:#FFF 1px solid; background-color:#999; color:#FFF;	
}

/* Css By Pradeep TR Sharma */
table.tablestyle {
	width: 100%;
}
.mm-link-btn a.button.editLinkButton, a.button.create_version {
    width: 165px !important;
    display: inline-block;
    height: 30px;
    margin: 4px 4px;
    padding: 0;
    line-height: 30px;
    font-size: 1.2em;
}

.mm-link-btn a.button.viewLinkButton {
    min-width: initial;
    width: 25px;
    margin: 5px;
    padding: 0 4px;
    text-align: center;
    float: left;
    line-height: 24px;
}
/* Css By Pradeep TR Sharma */

</style>