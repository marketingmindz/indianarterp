<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/edit_costing_db.inc");
include($path_to_root . "/costing/includes/ui/edit_costing_ui.inc");
include($path_to_root . "/costing/includes/edit_wood_costing_class.inc");
include($path_to_root . "/costing/includes/edit_labour_costing_class.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/contacts_view.inc");



if(isset($_SESSION['edit_wood']) && isset($_SESSION['edit_labour'])){
	function fixObject (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject($_SESSION['edit_wood']);
	 fixObject($_SESSION['edit_labour']);
}else{
	unset($_SESSION['edit_wood']);
	$_SESSION['edit_wood'] = new wood;
	unset($_SESSION['edit_labour']);
	$_SESSION['edit_labour'] = new labour;
}


if (isset($_POST['category_id'])) 
{
	$category_id = array();
	$category_id = $_POST['category_id'];
	if($_POST['collection_id'] == 'Collection')
	{
		$range_id = $_POST['range_id'];
	}
	else
	{
		$range_id = '-1';
	}
}
if(!isset($_POST['session_var']))
{
	if(isset($_GET['finish_code_id']))
	{
		//display_error(asdfsaf);
		$finish_code_id = $_POST['finish_code_id'] = $_GET['finish_code_id'];
		$_SESSION['edit_labour'] = new labour;
		$_SESSION['edit_wood'] = new wood;
		$mfrg_id = get_mfrg_id($finish_code_id);
		read_wood_costing($mfrg_id, $_SESSION['edit_wood']);
		read_labour_special_costing($mfrg_id, $_SESSION['edit_labour']);
		
	}
}
if(isset($_POST['finish_code_id']) && !isset($_POST['submit']))
{
	$finish_code_id = $_POST['finish_code_id'];
	$mfrg_id = get_mfrg_id($finish_code_id);
	read_labour_costing($mfrg_id);
	
	read_manufacturing_cost($mfrg_id);
}
function wood_check_data()
{
	if(get_post('wood_id') == -1) {
		display_error( _("wood cannot be empty."));
		set_focus('wood_id');
		return false;
	}
	if(get_post('cft') == 0) {
		display_error( _("cft cannot be empty."));
		set_focus('cft');
		return false;
	}
	if(!get_post('rate')) {
		display_error( _("Rate cannot be empty."));
		set_focus('rate');
		return false;
	}
	if (!check_num('amount', 0))
    {
	   	display_error(_("The amount entered must be numeric and not less than zero."));
		set_focus('amount');
	   	return false;	   
    }
    return true;	
}

function unset_mfrg_form_variables() {
    unset($_POST['extra_percent']);
    unset($_POST['extra_amount']);
    unset($_POST['admin_percent']);
	unset($_POST['admin_cost']);
	unset($_POST['profit_percent']);
	unset($_POST['profit']);
}
function unset_form_cons_variables() {
	unset($_POST['wood_id']);
    unset($_POST['cft']);
    unset($_POST['rate']);
    unset($_POST['amount']);
	unset_mfrg_form_variables();
}

function labour_check_data()
{
	if(!get_post('basic_price')) {
		display_error( _("Basic Price cannot be empty."));
		set_focus('basic_price');
		return false;
	}

	if(!get_post('labour_type')) {
		display_error( _("labour type cannot be empty."));
		set_focus('labour_type');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('labour_amount', 0))
    {
	   	display_error(_("The labour amount entered must be numeric and not less than zero."));
		set_focus('labour_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_labour_variables() {
    unset($_POST['labour_type']);
    unset($_POST['special_rate']);
    unset($_POST['labour_amount']);
	unset($_POST['special_labour_cost']);
	unset_mfrg_form_variables();
}


function handle_new_labour()
{ 
	$check_data = labour_check_data();
	if($check_data){
		$_SESSION['edit_labour']->add_to_labour_part(count($_SESSION['edit_labour']->line_items),$_POST['labour_type'],$_POST['special_rate'],$_POST['labour_amount'], $_POST['special_labour_cost']);
		unset_form_labour_variables();
	}
	line_start_labour_focus();
}

function handle_labour_delete_item($line_no)
{
	unset($_SESSION['edit_labour']->line_items[$line_no]);
	unset_form_labour_variables();
    line_start_labour_focus();
}

function handle_labour_update()
{
   $_SESSION['edit_labour']->update_labour_item($_POST['line_no'],$_POST['labour_type'],$_POST['special_rate'],$_POST['labour_amount'], $_POST['special_labour_cost']);
	unset_form_labour_variables();
    line_start_labour_focus();
}

function handle_new_wood()
{
	$check_data = wood_check_data();
	if($check_data){
		$_SESSION['edit_wood']->add_to_wood_part(count($_SESSION['edit_wood']->line_items),$_POST['wood_id'],$_POST['cft'],$_POST['rate'],$_POST['amount']);
		unset_form_cons_variables();
	}
	line_start_consumable_focus();
}

function handle_wood_delete_item($line_no)
{
	array_splice($_SESSION['edit_wood']->line_items, $line_no, 1);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

function handle_wood_update()
{
   $_SESSION['edit_wood']->update_wood_item($_POST['line_no'],$_POST['wood_id'],$_POST['cft'],$_POST['rate'],$_POST['amount']);
	unset_form_cons_variables();
    line_start_consumable_focus();
}

function line_start_consumable_focus() {
  global 	$Ajax;
  $Ajax->activate('wood_table');
  //set_focus('part_name');
}

function line_start_labour_focus() {
  global 	$Ajax;
  $Ajax->activate('labour_table');
  //set_focus('part_name');
}

$id = find_submit('Delete');
if ($id != -1)
{
	handle_wood_delete_item($id);
}
if (isset($_POST['WoodAddItem']))
{
	handle_new_wood();
}
if (isset($_POST['WoodUpdateItem'])){
 	handle_wood_update();
}
if (isset($_POST['WoodCancelItemChanges'])) {
	line_start_consumable_focus();
}


//labour section  
$id = find_submit('Delete'); 
if ($id != -1)
{
	handle_labour_delete_item($id);
}
if (isset($_POST['LabourAddItem']))
{
	handle_new_labour();
}
if (isset($_POST['LabourUpdateItem'])){
    handle_labour_update();
}
if (isset($_POST['LabourCancelItemChanges'])) {
	line_start_labour_focus();
}


//--------------------------------------------------------------------------------------------
function get_finish_product($finish_code_id)
{
	global $Ajax;
	$Ajax->activate('finish_products');
	$Ajax->activate('finish_code');
	$Ajax->activate('finish_code_id');
	start_outer_table(TABLESTYLE2, "style=min-width:1100px;min-height:300px;");
	
	$product = get_product_details($_POST['finish_code_id']);
	table_section(1,"550px;");
	start_table();
		echo '<input type="hidden" id="finish_code_id" name="finish_code_id" value="'.$finish_code_id.'" >';
		text_row(_("Selected Finish Code:"),'finish_comp_code', $product['finish_comp_code'], 30, 30, null,null,null,"id=man_finish_code");
		text_row(_("Product Name:"),'finish_product_name', $product['finish_product_name'], 30, 30, null,null,null,"id=man_finish_code");
	end_table();
		//$check_exist = is_exist_mfrg_cost($finish_code_id);
		hidden('session_var', 1);
		div_start('costing_entry');
		$finish_code_id = $_POST['finish_code_id'];
		$mfrg_id = get_mfrg_id($_POST['finish_code_id']);
		display_design_consumables($finish_code_id, $mfrg_id);
		display_finish_consumables($finish_code_id, $mfrg_id);
		display_finish_fabrics($finish_code_id, $mfrg_id);
		display_wood_summary($_SESSION['edit_wood']);
		display_labour_summary($_SESSION['edit_labour']);	
		div_start('controls');
			submit_center_first('submit', _("Update Manufacturing Cost"), 
			  _('Update Manufacturing Cost data'), @$_REQUEST['popup'] ? true : 'default');
		div_end();		
	end_outer_table(1);
}


function unset_costing_form_variables()
{
	unset($_POST['basic_price']);
	unset($_POST['total_special_amount']);
	unset($_POST['total_wood_cost']);
	unset($_POST['total_labour_cost']);
	unset($_POST['extra_percent']);
	unset($_POST['extra_amount']);
	unset($_POST['total_cost']);
	unset($_POST['admin_percent']);
	unset($_POST['admin_cost']);
	unset($_POST['total_cp']);
	unset($_POST['profit_percent']);
	unset($_POST['profit']);
	unset($_POST['total_mfrg']);
	
}

if (isset($_POST['submit'])) 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if(!get_post('total_mfrg')) {
		$input_error = 1;
		display_error( _("Total mfrg cannot be empty."));
		return false;
	}
	if(!get_post('basic_price')) {
		$input_error = 1;
		display_error( _("Basic Price cannot be empty."));
		set_focus('basic_price');
		return false;
	}
	if ($input_error !=1 )
	{
		begin_transaction();
			
			$manuf_cost = array();
	
			$manuf_cost['finish_code_id'] = trim($_POST['finish_code_id']);
			$manuf_cost['total_cons_cost'] = trim($_POST['total_cons_cost']);
			$manuf_cost['total_finish_cons_cost'] = trim($_POST['total_finish_cons_cost']);
			$manuf_cost['total_fabric_cost'] = trim($_POST['total_fabric_cost']);
			$manuf_cost['total_wood_cost'] = trim($_POST['total_wood_cost']);
			$manuf_cost['basic_price'] = trim($_POST['basic_price']);
			$manuf_cost['extra_percent'] = trim($_POST['extra_percent']);
			$manuf_cost['extra_amount'] = trim($_POST['extra_amount']);
			$manuf_cost['total_special_cost'] = trim($_POST['total_special_amount']);
			$manuf_cost['total_labour_cost'] = trim($_POST['total_labour_cost']);
			$manuf_cost['total_cost'] = trim($_POST['total_cost']);
			$manuf_cost['admin_percent'] = trim($_POST['admin_percent']);
			$manuf_cost['admin_cost'] = trim($_POST['admin_cost']);
			$manuf_cost['total_cp'] = trim($_POST['total_cp']);
			$manuf_cost['profit_percent'] = trim($_POST['profit_percent']);
			$manuf_cost['profit'] = trim($_POST['profit']);
			$manuf_cost['total_mfrg'] = trim($_POST['total_mfrg']);
	
			$wood_session = &$_SESSION['edit_wood'];
			$labour_session = &$_SESSION['edit_labour'];
		
			update_manufacturing_cost($manuf_cost, $wood_session, $labour_session);
				 
			//unset($_SESSION['edit_wood']);
			//unset($_SESSION['edit_labour']);
			//unset_costing_form_variables();
			display_notification(_("Manufacturing Cost has been updated."));
			$Ajax->activate('_page_body');

		commit_transaction();
	}
} 
elseif (isset($_POST['delete']) && $_POST['delete'] != "") 
{
	//the link to delete a selected record was clicked instead of the submit button
	$cancel_delete = 0;

	// PREVENT DELETES IF DEPENDENT RECORDS IN 'supp_trans' , purch_orders
	if (key_in_foreign_table($_POST['supplier_id'], 'supp_trans', 'supplier_id'))
	{
		$cancel_delete = 1;
		display_error(_("Cannot delete this supplier because there are transactions that refer to this supplier."));

	} 
	else 
	{
		if (key_in_foreign_table($_POST['supplier_id'], 'purch_orders', 'supplier_id'))
		{
			$cancel_delete = 1;
			display_error(_("Cannot delete the supplier record because purchase orders have been created against this supplier."));
		}

	}
	if ($cancel_delete == 0) 
	{
		delete_supplier($_POST['supplier_id']);
		unset($_SESSION['supplier_id']);
		$supplier_id = '';
		$Ajax->activate('_page_body');
	} //end if Delete supplier
}

start_form();
	//display_header();

if($finish_code_id)
{
	//die("sdfaa");
	//if(isset($_POST['finish_code_id']))
	//unset($_POST['_tabs_sel']); // force settings tab for new customer
}
tabbed_content_start('tabs', array(
		'manufacturing' => array(_('&Manufacturing Cost'), $finish_code_id),
		'finishing' => array(_('&Finishing Cost'), $finish_code_id),
		'final' => array(_('&Final Cost'), $finish_code_id),

	));
	
	switch (get_post('_tabs_sel')) {
		default:
		case 'manufacturing':
			get_finish_product($finish_code_id); 
			break;
		case 'finishing':
			$_GET['popup'] = 1;
			include_once($path_to_root."/costing/manage/edit_finishing_cost.php");
			break;
		case 'final':
			$_GET['popup'] = 1;
			include_once($path_to_root."/costing/manage/edit_final_costing.php");
			break;
		};
br();
tabbed_content_end();
hidden('popup', @$_REQUEST['popup']);
end_form();

end_page(@$_REQUEST['popup']);

?>
<script src="../../js/jquery/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery/jquery-ui.min.js"></script>
<script src="costing.js"></script>
<script src="finishing_cost.js"></script>
<script src="final_costing.js"></script>

