// JavaScript Document
/* function to tuncate number by two digits after decimal number */
Number.prototype.toFixedDown = function(digits) {
    var re = new RegExp("(\\d+\\.\\d{" + digits + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf().toFixed(2);
};


$(document).on('change', '#final_profit_percent', function() {
    var final_profit_percent = $(this).val();
    var total_cost = $("#total_cost").val();
    var final_profit = (total_cost * final_profit_percent) / 100;
    $("#final_profit").val(final_profit.toFixedDown(2));

    var final_total_cost = parseFloat(final_profit) + parseFloat(total_cost);
    $("#final_total_cost").val(final_total_cost.toFixedDown(2));

    $.ajax({
        url: "convert_currency.php",
        method: "POST",
        dataType: "json",
        data: { total_cost: final_total_cost },
        success: function(data) {
            var total_currency = $("input[name=total_currency]").val();
            var currencies = $("input[name=currencies]").val();
            var curr = currencies.split(",");
            for (var k = 0; k < total_currency; k++) {
                $("#currency" + curr[k]).val(data[k].toFixedDown(2));
            }
        }
    });
});
$(document).on('change', '#final_profit', function() {
    var final_profit = $(this).val();
    if (final_profit == '') {
        var final_profit = 0;
    }
    var final_profit_percent = 0;
    var total_cost = $("#total_cost").val();
    final_profit_percent = (final_profit * 100) / total_cost;
    $("#final_profit_percent").val(final_profit_percent.toFixedDown(2));

    var final_total_cost = parseFloat(final_profit) + parseFloat(total_cost);
    $("#final_total_cost").val(final_total_cost.toFixedDown(2));

    $.ajax({
        url: "convert_currency.php",
        method: "POST",
        dataType: "json",
        data: { total_cost: final_total_cost },
        success: function(data) {
            var total_currency = $("input[name=total_currency]").val();
            var currencies = $("input[name=currencies]").val();
            var curr = currencies.split(",");
            for (var k = 0; k < total_currency; k++) {
                $("#currency" + curr[k]).val(data[k].toFixedDown(2));
            }
        }
    });
});


$(document).on('change', '#finishing_exp_percent', function() {
    var finishing_exp_percent = $(this).val();

    var total_mfrg_cost = $("#total_mfrg_cost").val();
    var final_finishing_cost = 0;
    final_finishing_cost = (total_mfrg_cost * finishing_exp_percent) / 100;
    $("#final_finishing_cost").val(final_finishing_cost.toFixedDown(2));

    var total_finishing_cost = $("#total_finishing_cost").val();
    $("#total_final_finishing_cost").html(parseFloat(Math.max(final_finishing_cost, total_finishing_cost)).toFixedDown(2));

    var total_finishing_cost = $("#total_finishing_cost").val();
    var total_cost = parseFloat(Math.max(final_finishing_cost, total_finishing_cost)) + parseFloat(total_mfrg_cost);
    $("#total_cost").val(total_cost.toFixedDown(2));

    var final_profit_percent = $("#final_profit_percent").val();
    var total_cost = $("#total_cost").val();
    var final_profit = (total_cost * final_profit_percent) / 100;
    $("#final_profit").val(final_profit.toFixedDown(2));

    var final_total_cost = parseFloat(total_cost) + parseFloat(final_profit);
    $("#final_total_cost").val(final_total_cost.toFixedDown(2));
    $.ajax({
        url: "convert_currency.php",
        method: "POST",
        dataType: "json",
        data: { total_cost: final_total_cost },
        success: function(data) {
            var total_currency = $("input[name=total_currency]").val();
            var currencies = $("input[name=currencies]").val();
            var curr = currencies.split(",");
            for (var k = 0; k < total_currency; k++) {
                $("#currency" + curr[k]).val(data[k].toFixedDown(2));
            }
        }
    });

});
$(document).on('change', '#final_finishing_cost', function() {
    var final_finishing_cost = $(this).val();
    var finishing_exp_percent = 0;
    var total_mfrg_cost = $("#total_mfrg_cost").val();
    finishing_exp_percent = (final_finishing_cost * 100) / total_mfrg_cost;
    $("#finishing_exp_percent").val(finishing_exp_percent.toFixedDown(2));

    var total_finishing_cost = $("#total_finishing_cost").val();
    $("#total_final_finishing_cost").html(parseFloat(Math.max(final_finishing_cost, total_finishing_cost)).toFixedDown(2));

    var total_finishing_cost = $("#total_finishing_cost").val();
    var total_cost = parseFloat(Math.max(final_finishing_cost, total_finishing_cost)) + parseFloat(total_mfrg_cost);
    $("#total_cost").val(total_cost.toFixedDown(2));

    var final_profit_percent = $("#final_profit_percent").val();
    var total_cost = $("#total_cost").val();
    var final_profit = (total_cost * final_profit_percent) / 100;
    $("#final_profit").val(final_profit.toFixedDown(2));

    var final_total_cost = parseFloat(total_cost) + parseFloat(final_profit);
    $("#final_total_cost").val(final_total_cost.toFixedDown(2));

    $.ajax({
        url: "convert_currency.php",
        method: "POST",
        dataType: "json",
        data: { total_cost: final_total_cost },
        success: function(data) {
            var total_currency = $("input[name=total_currency]").val();
            var currencies = $("input[name=currencies]").val();
            var curr = currencies.split(",");
            for (var k = 0; k < total_currency; k++) {
                $("#currency" + curr[k]).val(data[k].toFixedDown(2));
            }
        }
    });
});



$(document).on('click', '.finish_code', function() {
    var finish_code = $(this).attr('id');
    var finish_code_id = $("input[name=" + finish_code + "]").val();
    $("input[name=finish_code_id]").val(finish_code_id);
    $("#finish_code").val(finish_code);
    $(this).css("color", "red");
    $.ajax({
        url: "destroy_session.php",
        method: "POST",
        data: { finish_code: finish_code },
        success: function(data) {
            //alert(data);
        }
    });
    return true;

});

$(document).on('click', '#GetFinishProduct', function() {
    $("#finish_code").val('');
    return true;

});
