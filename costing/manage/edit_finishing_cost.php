<?php
$page_security = 'SA_SUPPLIER';
$path_to_root = "../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/ui/edit_finishing_cost_ui.inc");
include($path_to_root . "/costing/includes/edit_finish_costing_class.inc");
//include_once($path_to_root . "/costing/includes/db/edit_costing_db.inc");
include_once($path_to_root . "/includes/ui.inc");
//page(_($help_context = "Costing"), @$_REQUEST['popup'], false, "", $js);
simple_page_mode(true);
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
		

if(!isset($_POST['session_finish_var']))
{
	unset($_SESSION['finish_cost']);
	$_SESSION['finish_cost'] = new finish;
	$finishing_cost_id = get_finishing_cost_id($finish_code_id);
	read_finish_special_costing($finishing_cost_id, $_SESSION['finish_cost']);	 
}

if(isset($_SESSION['finish_cost'])){
	function fixObject1 (&$object)
     {
        if (!is_object ($object) && gettype ($object) == 'object')
           return ($object = unserialize (serialize ($object)));
         return $object;
     }
	 fixObject1($_SESSION['finish_cost']);

}else{
	unset($_SESSION['finish_cost']);
   $_SESSION['finish_cost'] = new finish;
}


function get_product_for_finishing_cost($finish_code_id)
{
	start_outer_table(TABLESTYLE2, "style=min-width:1100px;min-height:300px;");
	$product = get_product_details($_POST['finish_code_id']);
	table_section(1,"550px;");
	global 	$Ajax;
  	$Ajax->activate('finish_table');
	start_table();
		echo '<input type="hidden" id="finish_code_id" name="finish_code_id" value="'.$finish_code_id.'" >';
		text_row(_("Selected Finish Code:"),'finish_comp_code', $product['finish_comp_code'], 30, 30, null,null,null,"id=man_finish_code");
		text_row(_("Product Name:"),'finish_product_name', $product['finish_product_name'], 30, 30, null,null,null,"id=man_finish_code");

	end_table();
	hidden('session_finish_var', 1);
	div_start('costing_entry');
		display_finishing_costing_form($_SESSION['finish_cost']);
		div_start('controls');
			submit_center_first('updateFinishing', _("Update Finishing Cost"), 
			  _('Update Finishing Cost data'), @$_REQUEST['popup'] ? true : 'default'); 
		div_end();
	div_end();
	end_outer_table(1);

}


function finish_check_data()
{
	if(!get_post('special_cost_ref')) {
		display_error( _("Special Cost Reference cannot be empty."));
		set_focus('special_cost_ref');
		return false;
	}
	if(!get_post('special_rate')) {
		display_error( _("Special Rate cannot be empty."));
		set_focus('special_rate');
		return false;
	}
	if (!check_num('special_amount', 0))
    {
	   	display_error(_("Special amount entered must be numeric and not less than zero."));
		set_focus('special_amount');
	   	return false;	   
    }
    return true;	
}
function unset_form_finish_variables() {
    unset($_POST['special_cost_ref']);
    unset($_POST['special_rate']);
    unset($_POST['special_amount']);
	unset($_POST['total']);
}


function handle_new_finish()
{ 
	$check_data = finish_check_data();
	if($check_data){
		$_SESSION['finish_cost']->add_to_finish(count($_SESSION['finish_cost']->line_items),$_POST['special_cost_ref'],$_POST['special_rate'],$_POST['special_amount'], $_POST['total']);
		unset_form_finish_variables();
	} 
	line_start_finish_focus();
}

function handle_finish_delete_item($line_no)
{
	unset($_SESSION['finish_cost']->line_items[$line_no]);
	unset_form_finish_variables();
    line_start_finish_focus();
}

function handle_finish_update()
{
   $_SESSION['finish_cost']->update_finish_item($_POST['line_no'],$_POST['special_cost_ref'],$_POST['special_rate'],$_POST['special_amount'], $_POST['total']);
	unset_form_finish_variables();
    line_start_finish_focus();
}

function line_start_finish_focus() {
  global 	$Ajax;
  $Ajax->activate('finish_table');
  //set_focus('part_name');
}

$id = find_submit('Delete'); 
if ($id != -1)
{
	handle_finish_delete_item($id);
}
if (isset($_POST['FinishAddItem']))
{
	handle_new_finish();
}
if (isset($_POST['FinishUpdateItem'])){
    handle_finish_update();
}
if (isset($_POST['FinishCancelItemChanges'])) {
	line_start_finish_focus();
}


if (isset($_POST['updateFinishing'])) 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;
	if(!get_post('total_finishing_cost')) {
		$input_error = 1;
		display_error( _("Total Finishing cost cannot be empty."));
		return false;
	}
	if ($input_error !=1 )
	{
		begin_transaction();
			$finishing_cost = array();
	
			$finishing_cost['finish_code_id'] = trim($_POST['finish_code_id']);
			$finishing_cost['sanding_percent'] = trim($_POST['sanding_percent']);
			$finishing_cost['sanding_cost'] = trim($_POST['sanding_cost']);
			$finishing_cost['polish_percent'] = trim($_POST['polish_percent']);
			$finishing_cost['polish_cost'] = trim($_POST['polish_cost']);
			$finishing_cost['packaging_percent'] = trim($_POST['packaging_percent']);
			$finishing_cost['packaging_cost'] = trim($_POST['packaging_cost']);
			$finishing_cost['forwarding_percent'] = trim($_POST['forwarding_percent']);
			$finishing_cost['forwarding_cost'] = trim($_POST['forwarding_cost']);
			$finishing_cost['other_cost'] = trim($_POST['other_cost']);
			$finishing_cost['total_finishing_cost'] = trim($_POST['total_finishing_cost']);
			$finishing_cost['total_special_cost'] = trim($_POST['total_special_cost']);
			$finishing_cost['total_amount'] = trim($_POST['total_mfrg_cost'] + $_POST['total_finishing_cost']);

			$finishing_session = &$_SESSION['finish_cost'];
		
			update_finishing_cost($finishing_cost, $finishing_session);
			//unset($_SESSION['edit_finish_cost']);
			//unset_finishing_cost_variables();
			display_notification(_("Finishing Cost has been Updated."));
			$Ajax->activate('_page_body');

		commit_transaction();
	}
} 
elseif (isset($_POST['delete']) && $_POST['delete'] != "") 
{
	//the link to delete a selected record was clicked instead of the submit button
	$cancel_delete = 0;

	// PREVENT DELETES IF DEPENDENT RECORDS IN 'supp_trans' , purch_orders
	if (key_in_foreign_table($_POST['supplier_id'], 'supp_trans', 'supplier_id'))
	{
		$cancel_delete = 1;
		display_error(_("Cannot delete this supplier because there are transactions that refer to this supplier."));
	} 
	else 
	{
		if (key_in_foreign_table($_POST['supplier_id'], 'purch_orders', 'supplier_id'))
		{
			$cancel_delete = 1;
			display_error(_("Cannot delete the supplier record because purchase orders have been created against this supplier."));
		}
	}
	if ($cancel_delete == 0) 
	{
		delete_supplier($_POST['supplier_id']);
		unset($_SESSION['supplier_id']);
		$supplier_id = '';
		$Ajax->activate('_page_body');
	} //end if Delete supplier
}





start_form();
if($_POST['finish_code_id'])
{
	read_finish_cost($_POST['finish_code_id']);
	//if()
}
	get_product_for_finishing_cost($finish_code_id); 
end_form();
//end_page(@$_REQUEST['popup']);
?>