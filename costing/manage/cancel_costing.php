<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SAVED_COSTING';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/costing/includes/db/costing_db.inc");
include($path_to_root . "/costing/includes/ui/saved_costing_ui.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Cancel Costing"), @$_REQUEST['popup'], false, "", $js);


function delete_mfrg_cost($ref_id)
{
	$sql = "select id from ".TB_PREF."manufacturing_cost where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not get mfrg cost id");
	while($row = db_fetch($result))
	{
		$mfrg_id = $row['id'];
		$sql = "delete from ".TB_PREF."manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete manufacturing_labour_cost ##cancel_costing");
		
		
		$sql = "delete from ".TB_PREF."manufacturing_labour_special_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete manufacturing_labour_special_cost ##cancel_costing");
		
		$sql = "delete from ".TB_PREF."manufacturing_wood_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete manufacturing_wood_cost ##cancel_costing");
		
		$sql = "delete from ".TB_PREF."man_design_cons_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete man_design_cons_cost ##cancel_costing");
		
		$sql = "delete from ".TB_PREF."man_fabric_cons_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete man_fabric_cons_cost ##cancel_costing");
		
		$sql = "delete from ".TB_PREF."man_finish_cons_cost where mfrg_id = ".db_escape($mfrg_id);
		db_query($sql, "Could not delete man_finish_cons_cost ##cancel_costing");
			
	}
	
	$sql = "delete from ".TB_PREF."manufacturing_cost where ref_id = ".db_escape($ref_id);
	db_query($sql, "Could not delete manufacturing_cost ##cancel_costing");
}

function delete_finishing_cost($ref_id)
{
	$sql = "select id from ".TB_PREF."finishing_cost where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not get finishing cost id");
	while($row = db_fetch($result))
	{
		$finishing_cost_id = $row['id'];
		$sql = "delete from ".TB_PREF."finishing_special_cost where finishing_cost_id = ".db_escape($finishing_cost_id);
		db_query($sql, "Could not delete finishing_special_cost ##cancel_costing");
					
	}
	
	$sql = "delete from ".TB_PREF."finishing_cost where ref_id = ".db_escape($ref_id);
	db_query($sql, "Could not delete finishing_cost ##cancel_costing");
}



function delete_final_cost($ref_id)
{	
	$sql = "delete from ".TB_PREF."final_costing where ref_id = ".db_escape($ref_id);
	db_query($sql, "Could not delete final_costing ##cancel_costing");
}










if(isset($_GET['reference_id']))
{
	$ref_id = $_GET['reference_id'];
	$sql = "delete from ".TB_PREF."costing_reference where ref_id = ".db_escape($ref_id);
	$result = db_query($sql);
	
	$sql = "delete from ".TB_PREF."costing_reference_details where ref_id = ".db_escape($ref_id);
	$result = db_query($sql);
	
	delete_mfrg_cost($ref_id);
	delete_finishing_cost($ref_id);
	delete_final_cost($ref_id);
	
	
	
	db_query($sql, "Could not freeze final cost");

	display_notification('Costing has been canceled. Go to <a style="font-size:14px;" href="saved_costing.php"> Saved Costing </a>');
} 
else
{
	meta_forward("saved_costing.php");
}

?>
