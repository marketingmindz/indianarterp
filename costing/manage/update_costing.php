<?php
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

function get_mfrg_id($finish_pro_id)
{
	$sql = "SELECT id FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}
function get_finishing_cost_id($finish_code_id)
{
	$sql = "SELECT id FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code_id);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function update_manufacturing_cost($ref_id, $params)
{
	$sql = "UPDATE ".TB_PREF."manufacturing_cost SET 
			total_cons_cost = ". db_escape($params['total_cons_cost']).",
			total_finish_cons_cost = ". db_escape($params['total_finish_cons_cost']).",
			total_fabric_cost = ". db_escape($params['total_fabric_cost']).",
			total_wood_cost = ". db_escape($params['total_wood_cost']).",
			total_labour_cost = ". db_escape($params['total_labour_cost']).",
			extra_percent = ". db_escape($params['extra_percent']).",
			extra_amount = ". db_escape($params['extra_amount']).",
			total_cost = ". db_escape($params['total_cost']).",
			admin_percent = ". db_escape($params['admin_percent']).",
			admin_cost = ". db_escape($params['admin_cost']).",
			total_cp = ". db_escape($params['total_cp']).",
			profit_percent = ". db_escape($params['profit_percent']).",
			profit = ". db_escape($params['profit']).",
			total_mfrg = ". db_escape($params['total_mfrg']);
 
  	$sql .= " WHERE finish_code_id = ".$params['finish_code_id'];
	db_query($sql, "Manufacturing cost is not updated.");
	
	$mfrg_id = get_mfrg_id($params['finish_code_id']);
	
	/*$sql = "DELETE FROM ".TB_PREF."man_design_cons_cost WHERE mfrg_id=".db_escape($mfrg_id);
	db_query($sql, "could not delete old design consumables details");
	for($i = 0;$i < $_POST['design_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['des_master_name_'.$i]). "," .
		db_escape($_POST['des_consumable_name_'.$i]). "," .
		db_escape($_POST['des_cons_unit_'.$i]). "," .
		db_escape($_POST['des_cons_qty_'.$i]). "," .
		db_escape($_POST['des_rate_'.$i]). "," .
		db_escape($_POST['des_cost_'.$i]). ")";
		
	
		db_query($sql, "Design consumable Costing is not inserted");
     }
	
	$sql = "DELETE FROM ".TB_PREF."man_finish_cons_cost WHERE mfrg_id=".db_escape($mfrg_id);
	db_query($sql, "could not delete old finish consumables details");
	
	for($i = 0;$i < $_POST['finish_cons_no'];$i++ )
	{
		$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['finish_master_name_'.$i]). "," .
		db_escape($_POST['finish_consumable_name_'.$i]). "," .
		db_escape($_POST['finish_cons_unit_'.$i]). "," .
		db_escape($_POST['finish_cons_qty_'.$i]). "," .
		db_escape($_POST['finish_cons_rate_'.$i]). "," .
		db_escape($_POST['finish_cost_'.$i]). ")";
		db_query($sql, "Finish consumable Costing is not inserted");
		
	}
	
	
	$sql = "DELETE FROM ".TB_PREF."man_fabric_cons_cost WHERE mfrg_id=".db_escape($mfrg_id);
	db_query($sql, "could not delete old fabric consumables details");
	
	for($i = 0;$i < $_POST['fabric_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['fabric_name_'.$i]). "," .
		db_escape($_POST['perecentage_'.$i]). "," .
		db_escape($_POST['fabric_rate_'.$i]). "," .
		db_escape($_POST['fabric_cost_'.$i]). ")";
		db_query($sql, "Fabric consumable Costing is not inserted");
		
     }*/
	

	
	$sql = "DELETE FROM ".TB_PREF."manufacturing_wood_cost WHERE mfrg_id=".db_escape($mfrg_id);
	db_query($sql, "could not delete old manufacturing wood cost");
	
	
	$i = 0; print_r($wood);
	foreach ($_POST['wood_cost'] as $wood_cost)
     {

		$w = array();
		$wood = html_entity_decode($wood_cost);
		parse_str($wood, $w);
		$sql = "INSERT INTO ".TB_PREF."manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($w['wood_id'.$i]). "," .
		db_escape($w['cft'.$i]). "," .
		db_escape($w['rate'.$i]). "," .
		db_escape($w['amount'.$i]). ")";
		db_query($sql, "Wood Costing is not inserted");
		$i++;
     }
	 
	 
	 $sql = "UPDATE ".TB_PREF."manufacturing_labour_cost SET 
			basic_price = ". db_escape($params['basic_price']).",
			total_special_cost = ". db_escape($params['total_special_cost']).",
			total_labour_cost = ". db_escape($params['total_labour_cost'])." where mfrg_id = ".db_escape($mfrg_id);
	db_query($sql, "Labour Costing is not updated");


	$sql = "DELETE FROM ".TB_PREF."manufacturing_labour_special_cost WHERE mfrg_id=".db_escape($mfrg_id);
	db_query($sql, "could not delete old manufacturing wood cost");
	$i = 0;
	 foreach ($_POST['labour_cost'] as $labour_cost)
     {
		$l = array();
		$labour = html_entity_decode($labour_cost);
		parse_str($labour, $l);
		
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($l['labour_type'.$i]). "," .
		db_escape($l['special_rate_'.$i]). "," .
		db_escape($l['labour_amount_'.$i]). "," .
		db_escape($l['special_labour_cost_'.$i]). ")";
		db_query($sql, "Labour Costing is not inserted");
		$i++;
     }
	return true; 
}


function update_finishing_cost($ref_id, $params)
{
	$total_amount = $params['total_mfrg'] + $params['total_finishing_cost'];
	$sql = "UPDATE ".TB_PREF."finishing_cost SET 
			sanding_labour = ". db_escape($params['sanding_labour']).",
			polish_labour = ". db_escape($params['polish_labour']).",
			packaging_labour = ". db_escape($params['packaging_labour']).",
			actual_sending_cost = ". db_escape($params['actual_sending_cost']).",
			actual_polish_cost = ". db_escape($params['actual_polish_cost']).",
			actual_packaging_cost = ". db_escape($params['actual_packaging_cost']).",
			lumsum_cost_percent = ". db_escape($params['lumsum_cost_percent']).",
			lumsum_cost_value = ". db_escape($params['lumsum_cost_value']).",
			sanding_percent = ". db_escape($params['sanding_percent']).",
			sanding_cost = ". db_escape($params['sanding_cost']).",
			polish_percent = ". db_escape($params['polish_percent']).",
			polish_cost = ". db_escape($params['polish_cost']).",
			packaging_percent = ". db_escape($params['packaging_percent']).",
			packaging_cost = ". db_escape($params['packaging_cost']).",
			forwarding_percent = ". db_escape($params['forwarding_percent']).",
			forwarding_cost = ". db_escape($params['forwarding_cost']).",
			total_special_cost = ". db_escape($params['total_special_cost']).",
			other_cost = ". db_escape($params['other_cost']).",
			total_finishing_cost = ". db_escape($params['total_finishing_cost']).",
			total_amount = ". db_escape($total_amount);
  
  	$sql .= " WHERE finish_code_id = ".$params['finish_code_id'];
	db_query($sql, "Finishing cost is not updated.");

	$finishing_cost_id = get_finishing_cost_id($params['finish_code_id']);
	$sql = "DELETE FROM ".TB_PREF."finishing_special_cost WHERE finishing_cost_id=".db_escape($finishing_cost_id);
	db_query($sql, "could not delete old finishing special details");
	$i = 0;
	foreach ($_POST['fs_cost'] as $fs_cost)
     {
		$fs = array();
		$fsc = html_entity_decode($fs_cost);
		parse_str($fsc, $fs);
		
		$sql = "INSERT INTO ".TB_PREF."finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
		$sql .= db_escape($finishing_cost_id). ", " . 
				db_escape($fs['special_cost_ref_'.$i]). "," .
				db_escape($fs['special_frate_'.$i]). "," .
				db_escape($fs['special_famount_'.$i]). "," .
				db_escape($fs['special_ftotal_'.$i]). ")";
		db_query($sql, "finishing special cost Costing is not updated");
		$i++;	
		
     }
	return true; 
}


function update_final_cost($ref_id,$params)
{
	$currency_price = array();
	$curr = explode(",",$params['currencies']);
	for($i=0; $i<$params['total_currency']; $i++)
	{
		$currency_price[][$curr[$i]] = $params['currency'.$curr[$i]];
	}

	$currency = json_encode($currency_price);
	$total_amount = $params['total_mfrg'] + $params['total_finishing_cost'];
	$sql = "UPDATE ".TB_PREF."final_costing SET 
			total_mfrg_cost = ". db_escape($params['total_mfrg']).",
			total_finishing_cost = ". db_escape($params['total_finishing_cost']).",
			total_finishing_percent = ". db_escape($params['total_percent']).",
			finishing_exp_percent = ". db_escape($params['finishing_exp_percent']).",
			final_finishing_cost = ". db_escape($params['final_finishing_cost']).",
			total_cost = ". db_escape($params['total_cost']).",
			final_profit_percent = ". db_escape($params['final_profit_percent']).",
			final_profit = ". db_escape($params['final_profit']).",
			final_total_cost = ". db_escape($params['final_total_cost']).",
			currency_price = ". db_escape($currency);
  
  	$sql .= " WHERE finish_code_id = ".db_escape($params['finish_code_id']);
	db_query($sql, "Final cost is not updated.");	
	return true; 
	
}


$params = array();
$str = html_entity_decode($_POST['cost']);
parse_str($str, $params);
$ref_id = $_POST['ref_id'];

update_manufacturing_cost($ref_id,$params);
update_finishing_cost($ref_id,$params);
update_final_cost($ref_id,$params);



?>