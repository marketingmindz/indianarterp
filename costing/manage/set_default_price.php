<?php
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");



$category_id = $_POST['category_id'];
$consumable_id = $_POST['consumable_id'];
$supplier_id = $_POST['supplier_id'];

$sql = "select default_supplier from ".TB_PREF."default_consumable_supplier where consumable_cat = ".db_escape($category_id)." AND consumable_name = ".db_escape($consumable_id);
$result = db_query($sql, "Could not check default supplier for consumable.");
if(db_num_rows($result) > 0)
{
	$sql = "update ".TB_PREF."default_consumable_supplier set default_supplier = ".db_escape($supplier_id)." where consumable_cat = ".db_escape($category_id)." AND consumable_name = ".db_escape($consumable_id);
	db_query($sql,"Could not update default supplier.");
}
else
{
	$sql = "insert into ".TB_PREF."default_consumable_supplier(consumable_cat,consumable_name,default_supplier) values(".db_escape($category_id).",".db_escape($consumable_id).",".db_escape($supplier_id).")";
	db_query($sql,"Could not set default supplier.");
}


?>