<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_order_details($po_id)
{
   $sql = "SELECT po_no,s.supp_name,l.location_id, w.work_center_id,  lo.location_name, wo.work_center_name, d.date, pr.supplier_id FROM ".TB_PREF."purchase_order_receive pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.po_id where ";

	
	$sql .= " pr.po_supp_rel = ".db_escape($po_id);

   	$result = db_query($sql, "The Purchase_order_details can not be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	$_POST['po_no'] = $myrow["po_supp_rel"];
		$_POST['po_date'] = $myrow["date"];
		$_POST['supplier_id'] = $myrow["supplier_id"];
		$_POST['supp_name'] = $myrow["supp_name"];
		$_POST['location_id'] = $myrow["location_id"];
		$_POST['location_id_0'] = $myrow["location_name"];
		$_POST['work_center_id'] = $myrow["work_center_id"];
		$_POST['work_center_id_0'] = $myrow["work_center_name"];
      	//$_POST['pr_date'] = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no."FATAL : Purchase_request_details can not be retrieved", "", true);
	return false;
}




function new_purchase_request(&$header_adjustment,&$consumable_type_obj)
{
	$pr_no = $header_adjustment['pr_no'];
	$date = date2sql($header_adjustment['date']);
	$sql = "INSERT INTO ".TB_PREF."purchase_request(pr_no,location_id,work_center_id,date) VALUES(";
	$sql .= db_escape($pr_no) . "," .
	db_escape($header_adjustment['location_id']) . "," .
	db_escape($header_adjustment['work_center_id']) . "," .
	db_escape($date).")";
	db_query($sql, "Purchase request is not created");
	
	foreach ($consumable_type_obj->line_con as $line_no => $lines_on_con)
	 {		
	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
		$sql .= $pr_no . ", " . 
		db_escape($lines_on_con->cons_type).",".
		db_escape($lines_on_con->cons_select) . "," . 		
		db_escape($lines_on_con->cons_unit) . ", " .
		db_escape($lines_on_con->cons_quntity). ")";
		db_query($sql, " purchase requesr items are not inserted in database can not be made");
	 }
	 return $pr_no;
}

function update_purchase_request($pr_id,&$header_adjustment,&$consumable_type_obj)
{
  
  $dateisset = strstr($header_adjustment['date'], '-');
  if($dateisset)
    $date = $header_adjustment['date'];
  else
    $date = date2sql($header_adjustment['date']);
    $sql = "UPDATE ".TB_PREF."purchase_request SET 
    pr_no = ". db_escape($header_adjustment['pr_no']).",
    location_id = ". db_escape($header_adjustment['location_id']).",
    work_center_id = ". db_escape($header_adjustment['work_center_id']).",
    date = ". db_escape($date);
  
  $sql .= " WHERE pr_no = ".$pr_id;
  db_query($sql, "Purchase request is not updated");
  
	$sql = "DELETE FROM ".TB_PREF."purchase_request_item WHERE pr_id="
		.db_escape($pr_id);
    db_query($sql, "could not delete old item details");
 	 foreach ($consumable_type_obj->line_con as $line_no => $lines_on_con)
	 {		
	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
		$sql .= $pr_id . ", " . 
		db_escape($lines_on_con->cons_type).",".
		db_escape($lines_on_con->cons_select) . "," . 		
		db_escape($lines_on_con->cons_unit) . ", " .
		db_escape($lines_on_con->cons_quntity). ")";
		db_query($sql, " purchase requesr items are not inserted in database can not be made");
	 }
	 return $pr_no;
}

function get_cons_type_name($cons_type){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_select_name($cons_select){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function read_pr_header($order_no, &$order)
{
   	$sql = "SELECT *, l.location_name, w.work_center_name FROM ".TB_PREF."purchase_request p LEFT JOIN ".TB_PREF."location_master l on p.location_id = l.location_id LEFT JOIN ".TB_PREF."work_center w on p.work_center_id = w.work_center_id WHERE pr_no = ".db_escape($order_no);

   	$result = db_query($sql, "The order cannot be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	//$order->trans_type = ST_PURCHORDER;
      	$order->order_no = $order_no;
		$order->pr_no = $order_no;
		$order->location_id = $myrow["location_name"];
		$order->work_center_id = $myrow["work_center_name"];
      	$order->pr_date = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no."FATAL : duplicate purchase order found", "", true);
	return false;
}

//----------------------------------------------------------------------------------------

function read_pr_items($order_no, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT m.master_name,c.consumable_name,unit,quantity FROM ".TB_PREF."purchase_request_item pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       WHERE pr_id =".db_escape($order_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_order($order->lines_on_order, $myrow["master_name"],
            	$myrow["consumable_name"],$myrow["unit"],$myrow["quantity"])) {
            		$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

//----------------------------------------------------------------------------------------

function read_pr($order_no, &$order, $open_items_only=false)
{
	$result = read_pr_header($order_no, $order);

	if ($result)
		read_pr_items($order_no, $order, $open_items_only);
}

//----------- serach purchase inquiry no ------------

function get_purchase_request_search($all_items){
	global $order_number;
	
	$sql ='Select pr.pr_no, pr.date, loc.location_name,wc.work_center_name from '.TB_PREF.'purchase_request pr 
	Left Join '.TB_PREF.'location_master loc on pr.location_id = loc.location_id Left Join '.TB_PREF.'work_center wc on wc.work_center_id = pr.work_center_id ';
	
	if (isset($order_number) && $order_number != ""){
		$sql .= " where pr.pr_no = ".db_escape($order_number);
	}
	return $sql;
}


function read_purchase_request_header($pr_id){
	
	$result = get_purchase_request_header($pr_id, $order);
	
}

function get_purchase_request_header($pr_id, $order)
{
   $sql = "SELECT * FROM ".TB_PREF."purchase_request WHERE pr_no=".db_escape($pr_id);
	$result = db_query($sql, "The purchase request cannot be retrieved");
	if (db_num_rows($result) == 1)
	{

      	$myrow = db_fetch($result);
				$order->pr_no = $pr_no;
      			$_POST['pr_no'] = $myrow["pr_no"];
				$_POST['date']  = $myrow["date"];
				$_POST['location_id'] = $myrow["location_id"];
				$_POST['work_center_id'] = $myrow["work_center_id"];
		return true;
	}

	
	return false;
}

function read_cons_purchase_item($pr_id, &$cons)
{
	#get consumable_part id from pr_id
   $sql = "SELECT * FROM ".TB_PREF."purchase_request_item WHERE pr_id=".db_escape($pr_id); 
	$result = db_query($sql, "consumable part cannot be retrive");
	
	if (db_num_rows($result) > 0)
			{
				while ($myrow1 = db_fetch($result))
				{
					
					if ($cons->add_to_consuable_part($cons->lines_on_con, $myrow1["consumable_id"], $myrow1["consumable_category"], $myrow1["unit"], $myrow1["quantity"])) {
							//echo "fsdfsdfsdfsd";
							$newline = &$cons->line_con[$cons->lines_on_con-1];
							$newline->con_detail_rec = $myrow1["cons_line_details"];	
					}
				} 
			}  
}


function purchase_request_deleted($id)
{
	$sql = "DELETE FROM ".TB_PREF."purchase_request WHERE pr_no=".db_escape($id);
	db_query($sql, "Purchase request could not be deleted");

	$sql = "DELETE FROM ".TB_PREF."purchase_request_item WHERE pr_id =".db_escape($id);
	db_query($sql, "Purchase request item could not be deleted.");
}

function get_sql_for_pr_search_completed($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT pr_no, l.location_name,w.work_center_name,date, st.selection_name, t.request_type FROM ".TB_PREF."purchase_request pr LEFT JOIN ".TB_PREF."location_master l on pr.location_id = l.location_id LEFT JOIN ".TB_PREF."work_center w on pr.work_center_id = w.work_center_id LEFT JOIN ".TB_PREF."selection st on pr.status = st.selection_id
		LEFT JOIN ".TB_PREF."purchase_request_type t on pr.type = t.request_type_id
		WHERE ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.pr_id LIKE ".db_escape('%'. $order_number . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " pr.date >= '$data_after'";
		$sql .= " AND pr.date <= '$date_before'";

		/*if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
		{
			$sql .= " AND pr.location_id = ".db_escape($_POST['StockLocation']);
		}
		if (isset($selected_stock_item))
		{
			$sql .= " AND line.item_code=".db_escape($selected_stock_item);
		}*/
		 //end not order number selected

	$sql .= "AND status != '1' GROUP BY pr_no";
	}
	return $sql;
}	

?>
