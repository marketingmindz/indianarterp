<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//  code by bajrang -  request details - - -- - 
function get_order_details($po_id)
{
   $sql = "SELECT po_supp_rel,s.supp_name,l.location_id, w.work_center_id,  lo.location_name, wo.work_center_name, d.date, pr.supplier_id FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.po_id where ";

	
	$sql .= " pr.po_supp_rel = ".db_escape($po_id);

   	$result = db_query($sql, "The Purchase_order_details can not be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	$_POST['po_no'] = $myrow["po_supp_rel"];
		$_POST['po_date'] = $myrow["date"];
		$_POST['supplier_id'] = $myrow["supplier_id"];
		$_POST['supp_name'] = $myrow["supp_name"];
		$_POST['location_id'] = $myrow["location_id"];
		$_POST['location_id_0'] = $myrow["location_name"];
		$_POST['work_center_id'] = $myrow["work_center_id"];
		$_POST['work_center_id_0'] = $myrow["work_center_name"];
      	//$_POST['pr_date'] = sql2date($myrow["date"]);
      	return true;
	}
	display_db_error($order_no."FATAL : Purchase_request_details can not be retrieved", "", true);
	return false;
}

function get_purchase_order_item_details($po_no)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT po_id,supplier_id FROM ".TB_PREF."purchase_supp_rel where po_supp_rel =".db_escape($po_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$po_id = $myrow['po_id'];
			$supplier_id = $myrow['supplier_id'];
		}
	}
	$sql = "SELECT pr.po_id,pr.consumable_id, pr.consumable_category, m.master_name,c.consumable_name,unit,approved_quantity FROM ".TB_PREF."purchase_order_details pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       WHERE po_id =".db_escape($po_id)." && supplier_id = ".db_escape($supplier_id);
		   
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
	
	
	return $result;
}





function receive_ordered_item(&$POST)
{
	$po_no = $POST['po_no'];
	$date = date2sql($POST['date']);
	$sql = "INSERT INTO ".TB_PREF."purchase_order_receive(po_no,supplier_id,location_id,work_center_id,po_date,receive_date) VALUES(";
	$sql .= db_escape($po_no) . "," .
	db_escape($POST['supplier_id']) . "," .
	db_escape($POST['location_id']) . "," .
	db_escape($POST['work_center_id']) . "," .
	db_escape($POST['po_date']) . "," .
	db_escape($POST['receive_date']).")";
	db_query($sql, "Receiving details are not inserted");
	
	$receiving_id = db_insert_id();
	
	$sql = "SELECT po_id FROM ".TB_PREF."purchase_supp_rel where po_supp_rel =".db_escape($po_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$po_id = $myrow['po_id'];
		}
	}
	
	$count = $POST['count'];
	for($i=0; $i<=$count; $i++)
	 {		
		$sql = "INSERT INTO ".TB_PREF."purchase_order_received_items(receiving_id, consumable_id, consumable_category,unit, ordered_quantity, received_quantity, ok_quantity, rejected_quantity, chalan_no, invoice_no) VALUES (";
			$sql .= $receiving_id . ", " . 
			db_escape($POST['consumable_id_'.$i]).",".
			db_escape($POST['consumable_category_'.$i]) . "," . 		
			db_escape($POST['unit_'.$i]) . ", " .
			db_escape($POST['ordered_quantity_'.$i]) . ", " .
			db_escape($POST['received_quantity_'.$i]) . ", " .
			db_escape($POST['ok_quantity_'.$i]) . ", " .
			db_escape($POST['rejected_quantity_'.$i]) . ", " .
			db_escape($POST['chalan_no_'.$i]) . ", " .
			db_escape($POST['invoice_no_'.$i]).")";
			db_query($sql, " Received item details are not inserted in database");
		if($POST['received_quantity_'.$i] != '0')
		{	
			$remaining_qty = $POST['ordered_quantity_'.$i] - $POST['received_quantity_'.$i] + $POST['rejected_quantity_'.$i];
		}
		else
		{
			$remaining_qty = $POST['ordered_quantity_'.$i];
		}
		 echo $sql = "Update ".TB_PREF."purchase_order_details set approved_quantity =".$remaining_qty." where po_id = ".db_escape($po_id)." && consumable_id = ".db_escape($POST['consumable_id_'.$i])." && consumable_category = ".db_escape($POST['consumable_category_'.$i]);
		
		db_query($sql, " Purchase order detials are not updated");
		
		$check_consumable = "select stock_qty from ".TB_PREF."opening_stock_master where  
				consumable_id = ".db_escape($POST['consumable_id_'.$i]).
				" && consumable_category = ".db_escape($POST['consumable_category_'.$i]).
				" && location_id = ".db_escape($POST['location_id']).
				"&& work_center_id = ".db_escape($POST['work_center_id']);
		$result = db_query($check_consumable,'check_consumable_stock stock check error');
		if(db_num_rows($result) < 1 )
		{
			$sql = "INSERT INTO ".TB_PREF."opening_stock_master(stock_type_id, location_id, work_center_id,consumable_id,consumable_category,pro_cat_id,pro_range_id,pro_design_id,pro_finish_id,stock_qty,date)
			VALUES ("."101".","
			.db_escape($POST['location_id']).","
			.db_escape($POST['work_center_id']).","
			.db_escape($POST['consumable_id_'.$i]).","
			.db_escape($POST['consumable_category_'.$i]).",'-1','-1','-1','-1',"
			.db_escape($POST['ok_quantity_'.$i]).","
			.db_escape($POST['receive_date']).")";
			
		   db_query($sql,"could not add new stock");
		}
		else
		{
			while ($myrow = db_fetch($result))
			{
				$stock_quantity = $myrow['stock_qty'];
			}
			$stock_quantity = $stock_quantity+$POST['ok_quantity_'.$i];
			$sql = "UPDATE ".TB_PREF."opening_stock_master SET stock_qty=".db_escape($stock_quantity)
				  ." where location_id=".db_escape($POST['location_id'])
				  ." && work_center_id=".db_escape($_POST['work_center_id'])
				  ." && consumable_id = ".db_escape($POST['consumable_id_'.$i])
				  ." && consumable_category = ".db_escape($POST['consumable_category_'.$i]);
			db_query($sql, "could not update opening stock master");
		 }
	
	 }
	 
	 $sql = "select approved_quantity from ".TB_PREF."purchase_order_details where po_id = ".db_escape($po_id). "
	                && supplier_id = ".db_escape($POST['supplier_id']);
	 $sel_result = db_query($sql, " error while updating status of purchase Order");
	 while($row = db_fetch($sel_result))
	 {
		 if($row['approved_quantity'] != 0)
		 {
			 $status = 2;
			 break;
		 }
	 }
	 if($status == 2)
	 {
		 $sql = "update ".TB_PREF."purchase_supp_rel set status = '2' where po_supp_rel='$po_supp_rel' && po_id = ".db_escape($po_id);
		db_query($sql, " purchase Order status is not updated error #1");
	 }
	 else
	 {
		 $sql = "update ".TB_PREF."purchase_supp_rel set status = '1' where po_supp_rel='$po_supp_rel' && po_id = ".db_escape($po_id);
		db_query($sql, " purchase Order status is not updated error #2");
	 }
	 
	 return $receiving_id;
}



function get_purchase_order_search($supplier_id=ALL_TEXT)
{
	global $order_number, $selected_stock_item;;

	$sql = "SELECT po_supp_rel,s.supp_name, lo.location_name,wo.work_center_name,d.date FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.po_id where ";

	if (isset($order_number) && $order_number != "")
	{
		$sql .= " pr.po_supp_rel LIKE ".db_escape('%'. $order_number . '%');
	}
	else
	{

		$data_after = date2sql($_POST['OrdersAfterDate']);
		$date_before = date2sql($_POST['OrdersToDate']);

		$sql .= " d.date >= '$data_after'";
		$sql .= " AND d.date <= '$date_before'";

		if (isset($_POST['supplier_id']) && $_POST['supplier_id'] != ALL_TEXT)
		{
			$sql .= " AND pr.supplier_id = ".db_escape($_POST['supplier_id']);
		}
		/*if (isset($selected_stock_item))
		{
			$sql .= " AND line.item_code=".db_escape($selected_stock_item);
		}*/
		 //end not order number selected

	$sql .= " GROUP BY po_supp_rel";
	

	}

	$result = db_query($sql, "fsfsfs");
	//while($row  =db_fetch($result))
	//{
		$row  =db_fetch($result);
	//}
	return $sql;
}





function read_purchase_order_header($order_no, &$order)
{
	$sql = "SELECT po_supp_rel,s.supp_name, lo.location_name,wo.work_center_name,d.date FROM ".TB_PREF."purchase_supp_rel pr
	 LEFT JOIN ".TB_PREF."suppliers s on pr.supplier_id = s.supplier_id 
	 LEFT JOIN ".TB_PREF."purchase_order l on pr.po_id = l.po_id
	 LEFT JOIN ".TB_PREF."purchase_order w on pr.po_id = w.po_id 
	 LEFT JOIN ".TB_PREF."location_master lo on l.location_id = lo.location_id 
	 LEFT JOIN ".TB_PREF."work_center wo on w.work_center_id = wo.work_center_id
	 LEFT JOIN ".TB_PREF."purchase_order d on pr.po_id = d.date WHERE  po_supp_rel= ".db_escape($order_no);

   	$result = db_query($sql, "The order cannot be retrieved");

	if (db_num_rows($result) == 1)
	{
     	$myrow = db_fetch($result);
      	//$order->trans_type = ST_PURCHORDER;
      	$order->order_no = $order_no;
		$order->po_no = $order_no;
		$order->location_id = $myrow["location_name"];
		$order->work_center_id = $myrow["work_center_name"];
		$order->supplier_id = $myrow["supp_name"];
      	$order->po_date = sql2date($myrow["date"]);
      	return true;

	}
	display_db_error($order_no."FATAL : duplicate purchase order found", "", true);
	return false;
}

//----------------------------------------------------------------------------------------

function read_purchase_order_items($order_no, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT po_id,supplier_id FROM ".TB_PREF."purchase_supp_rel where po_supp_rel =".db_escape($order_no);
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
			$po_id = $myrow['po_id'];
			$supplier_id = $myrow['supplier_id'];
		}
	}
	$sql = "SELECT pr.po_id,m.master_name,c.consumable_name,unit,approved_quantity FROM ".TB_PREF."purchase_order_details pr
	       Left Join ".TB_PREF."master_creation m on pr.consumable_id = m.master_id
	       Left Join ".TB_PREF."consumable_master c on pr.consumable_category = c.consumable_id
	       WHERE po_id =".db_escape($po_id)." && supplier_id = ".db_escape($supplier_id);
		   
	$result = db_query($sql, "The lines on the purchase order cannot be retrieved");
    if (db_num_rows($result) > 0)
    {
		while ($myrow = db_fetch($result))
        {
            if ($order->add_to_order($order->lines_on_order, $myrow["master_name"],
            	$myrow["consumable_name"],$myrow["unit"],$myrow["approved_quantity"])) {
            		$newline = &$order->line_items[$order->lines_on_order-1];
					 			}
        } /* line po from purchase order details */
    } //end of checks on returned data set
}

//----------------------------------------------------------------------------------------

function read_purchase_order($order_no, &$order, $open_items_only=false)
{
	$result = read_purchase_order_header($order_no, $order);

	if ($result)
		read_purchase_order_items($order_no, $order, $open_items_only);
}


?>