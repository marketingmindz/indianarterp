<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_new_currency($currency_name, $country_name, $symbol, $rate)
{
	$sql = "INSERT INTO ".TB_PREF."currency_master(currency_name, country_name, symbol, rate)
			VALUES (" . db_escape($currency_name) . ", " .
			db_escape($country_name). ", " . 
			db_escape($symbol). ", " . 
			db_escape($rate) . ")";
	db_query($sql,"could not add new master");
}

function update_currency_details($id, $currency_name, $country_name, $symbol, $rate)
{
	$sql = "UPDATE ".TB_PREF."currency_master SET currency_name=".db_escape($currency_name)
			.",country_name=".db_escape($country_name)
			.",symbol=".db_escape($symbol)
			.",rate=".db_escape($rate)
			."WHERE id=".db_escape($id);
	db_query($sql, "could not update label master");
}
//--------------------------------------------------------------------------------------------------

function delete_currency_details($id)
{
	$sql = "DELETE FROM ".TB_PREF."currency_master WHERE id=".db_escape($id);
	db_query($sql, "could not delete currency");
}

function get_all_currencies() {
    $sql = "SELECT * FROM ".TB_PREF."currency_master";
    return  db_query($sql, "could not get Currencies list");
}

function get_currency_details($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."currency_master WHERE id=".db_escape($selected_id);
	$result = db_query($sql,"Currency could not be retrieved");
	return db_fetch($result);
}

function check_update_currency($currency_name)
{
	 $sql = 'SELECT * FROM '.TB_PREF.'currency_master where currency_name = '.db_escape($currency_name).' ';
	 $result = db_query($sql);
	 return db_fetch($result);
}




?>