<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function generate_purchase_request($location_id, $work_center_id, $consumable_id, $consumable_category, $request_quantity, $unit, $date)
{
	$pr_no = '';
	$sql = "SELECT pr_no FROM ".TB_PREF."purchase_request ORDER BY pr_no DESC LIMIT 1";
	$query1 = db_query($sql,"cannot get purchase request no.");
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $pr_no= $row['pr_no'];
	   $pr_no++;
	}
	if($pr_no == '' && $pr_no == NULL)
	{
		$pr_no = 1;
	}

	$sql = "INSERT INTO ".TB_PREF."purchase_request(pr_no,location_id,work_center_id,type,date) VALUES(";
	$sql .= db_escape($pr_no) . "," .
	db_escape($location_id) . "," .
	db_escape($work_center_id) . "," ."'1',".
	db_escape($date).")";
	db_query($sql, "Purchase request is not created");

	$sql = "INSERT INTO ".TB_PREF."purchase_request_item(pr_id, consumable_id, consumable_category,unit, quantity) VALUES (";
		$sql .= $pr_no . ", " . 
		db_escape($consumable_id).",".
		db_escape($consumable_category) . "," . 		
		db_escape($unit) . ", " .
		db_escape($request_quantity). ")";
	db_query($sql, " purchase request items are not inserted in database can not be made");

}

function new_purchase_request(&$header_adjustment)
{
	$date = date2sql($header_adjustment['date']);
	$sql = "SELECT * from  ".TB_PREF."opening_stock_master where location_id = ".db_escape($header_adjustment['location_id'])." AND work_center_id = ".db_escape($header_adjustment['work_center_id']). " and stock_qty < level";
	
	$result = db_query($sql,"stock cannot be retrived - #ioioio");

	while($row = db_fetch($result))
	{
		$request_quantity = $row['level'] - $row['stock_qty'];
		generate_purchase_request($header_adjustment['location_id'], $header_adjustment['work_center_id'], $row['consumable_id'], $row['consumable_category'], $request_quantity,$row['unit'], $date);
	}
	
}



?>
