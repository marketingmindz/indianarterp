<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function truncate_number( $number, $precision = 2) {
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}


function insert_des_cons_cost($mfrg_id, $mfrg_id_new)
{
	$sql = "select * from ".TB_PREF."man_design_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "could not get man_design_cons_cost");
	while($row = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id_new). ", " . 
		db_escape($row['consumable_name']). "," .
		db_escape($row['consumable_category']). "," .
		db_escape($row['unit']). "," .
		db_escape($row['quantity']). "," .
		db_escape($row['rate']). "," .
		db_escape($row['cost']). ")";
		db_query($sql, "Design consumable Costing is not inserted");
	}
}
function insert_finish_cons_cost($mfrg_id, $mfrg_id_new)
{
	$sql = "select * from ".TB_PREF."man_finish_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "could not get man_finish_cons_cost");
	while($row = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id_new). ", " . 
		db_escape($row['consumable_id']). "," .
		db_escape($row['consumable_category']). "," .
		db_escape($row['unit']). "," .
		db_escape($row['quantity']). "," .
		db_escape($row['rate']). "," .
		db_escape($row['cost']). ")";
		db_query($sql, "Finish consumable Costing is not inserted");
	}
}
function insert_fabric_cons_cost($mfrg_id, $mfrg_id_new)
{
	$sql = "select * from ".TB_PREF."man_fabric_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "could not get man_fabric_cons_cost");
	while($row = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id_new). ", " . 
		db_escape($row['fabric_id']). "," .
		db_escape($row['percentage']). "," .
		db_escape($row['rate']). "," .
		db_escape($row['cost']). ")";
		db_query($sql, "Fabric consumable Costing is not inserted");
	}
}

function insert_wood_cost($mfrg_id, $mfrg_id_new)
{
	$sql = "select * from ".TB_PREF."locked_manufacturing_wood_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "could not get manufacturing_wood_cost");
	while($row = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
		$sql .=  db_escape($mfrg_id_new). ", " . 
		db_escape($row['wood_id']). "," .
		db_escape($row['cft']). "," .
		db_escape($row['rate']). "," .
		db_escape($row['amount']). ")";
		db_query($sql, "Wood Costing is not inserted");
	}
}
function insert_finishing_special_cost($finishing_cost_id, $finishing_cost_id_new)
{
	$sql = "select * from ".TB_PREF."locked_finishing_special_cost where finishing_cost_id = ".db_escape($finishing_cost_id);
	$result = db_query($sql, "could not get finishing_special_cost");
	while($row = db_fetch($result))
	{
		$sql = "INSERT INTO ".TB_PREF."locked_finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
		$sql .= db_escape($finishing_cost_id_new). ", " . 
				db_escape($row['special_cost_ref']). "," .
				db_escape($row['special_rate']). "," .
				db_escape($row['special_amount']). "," .
				db_escape($row['total']). ")";
		db_query($sql, "finishing special cost Costing is not inserted");
	}
}

function edit_range_cost()
{
	$date = Today();
	$ref_id = $_POST['ref_id'];
	$sql = "select version from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not select reference version seq");
	while($row = db_fetch($result))
	{	
		$version = $version1 = $row['version'];
	}
	$new_version = ++$version1;

	$sql = "select current_version from ".TB_PREF."costing_reference where ref_id = ".db_escape($ref_id);
	$result = db_query($sql, "Could not select current reference version");
	while($row = db_fetch($result))
	{	
		$version = $row['current_version'];
	}



	$sql = "update ".TB_PREF."reference_version_seq set locked='0', version = ".db_escape($new_version)." where ref_id = ".db_escape($ref_id);
	db_query($sql, "Costing version is not updated");

$pro_count =0;
	
	$sql = "select finish_code_id from ".TB_PREF."locked_manufacturing_cost where ref_id = ".db_escape($ref_id)." && version = ".db_escape($version);
	$result = db_query($sql, "Could not select reference version seq");
	$product = array();
	while($row = db_fetch($result))
	{	
		$product[] = $row;
	}


foreach($product as $pro)
	{   /*   Updating Manufacturing Cost Starts Here */
		$sql = "select * from ".TB_PREF."locked_manufacturing_cost where finish_code_id = ".db_escape($pro['finish_code_id'])."
		        && version = ".db_escape($version);
	    $result = db_query($sql, "could not get mfrg_id");
		

		while($row = db_fetch($result))
		{
	
		
			$mfrg_id = $row['id'];
			$cons_plus_wood = $row['total_cons_cost']+$row['total_finish_cons_cost']+$row['total_fabric_cost']+$row['total_wood_cost'];
			
			$extra_percent = $row['extra_percent'];
			$admin_percent = $row['admin_percent'];
			
			if(get_post('admin_percent')) {
		
				if($_POST['admin_sign'] == "plus") { 
					$admin_percent = $admin_percent + $_POST['admin_percent'];
				 } 
				 if($_POST['admin_sign'] == "minus") { 
					$admin_percent = $admin_percent - $_POST['admin_percent'];
				 } 
			}
			
			$profit_percent = $row['profit_percent'];
			if(get_post('profit_percent')) {
				if($_POST['profit_sign'] == "plus") { 
					$profit_percent = $profit_percent + $_POST['profit_percent'];
				 } 
				 if($_POST['profit_sign'] == "minus") { 
					$profit_percent = $profit_percent - $_POST['profit_percent'];
				 } 
			}
			
			$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_cost(finish_code_id,ref_id,total_cons_cost,total_finish_cons_cost,total_fabric_cost,total_wood_cost,
					total_labour_cost, extra_percent, extra_amount, total_cost,admin_percent,admin_cost,total_cp,profit_percent,profit,total_mfrg, version, date) VALUES(";
			$sql .= db_escape($pro['finish_code_id']) . "," .
					db_escape($ref_id) . "," .
					db_escape($row['total_cons_cost']) . "," .
					db_escape($row['total_finish_cons_cost']) . "," .
					db_escape($row['total_fabric_cost']) . "," .
					db_escape($row['total_wood_cost']) . "," .
					db_escape($row['total_labour_cost']) . "," .
					db_escape($row['extra_percent']) . "," .
					db_escape($row['extra_amount']) . "," .
					db_escape($row['total_cost']) . "," .
					db_escape($row['admin_percent']) . "," .
					db_escape($row['admin_cost']) . "," .
					db_escape($row['total_cp']) . "," .
					db_escape($row['profit_percent']) . "," .
					db_escape($row['profit']) . "," .
					db_escape($row['total_mfrg']) . ",'$new_version','". date2sql($date)."')";
			db_query($sql, "Manufacturing cost is not inserted.");
			$mfrg_id_new = db_insert_id();
			
		}
		
				
		
		/*insert_des_cons_cost($mfrg_id, $mfrg_id_new);
		insert_finish_cons_cost($mfrg_id, $mfrg_id_new);
		insert_fabric_cons_cost($mfrg_id, $mfrg_id_new);*/
		insert_wood_cost($mfrg_id, $mfrg_id_new);
		
		$sql1 = "select * from ".TB_PREF."locked_manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
		$result1 = db_query($sql1, "Could not retrieve labour costing");
		if(db_num_rows($result1) > 0)
		{
			while($row1 = db_fetch($result1))
			{
				$basic_price = $row1['basic_price'];
				$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_labour_cost(mfrg_id, basic_price, total_special_cost, total_labour_cost) VALUES (";
				$sql .=  db_escape($mfrg_id_new). ", " . 
						db_escape($row1['basic_price']). "," .
						db_escape($row1['total_special_cost']). "," .
						db_escape($row1['total_labour_cost']). ")";
				db_query($sql, "Labour Costing is not inserted");

			}
			
			
			$total_special_cost = 0;
			$sql2 = "select * from ".TB_PREF."locked_manufacturing_labour_special_cost where mfrg_id = ".db_escape($mfrg_id);
			$result2 = db_query($sql2, "Could not retrieve special labour costing");
			if(db_num_rows($result2) > 0)
			{
				while($row2 = db_fetch($result2))
				{
					$special_rate = $row2['special_rate'];
					if(get_post('labour_amount')) {
						if($_POST['labour_sign'] == "plus") { 
							$labour_amount = $row2['labour_amount'] + $_POST['labour_amount'];
						 } 
						 if($_POST['labour_sign'] == "minus") { 
							$labour_amount = $row2['labour_amount'] - $_POST['labour_amount'];
						 } 
					 }
					 
					$special_labour_cost = $special_rate*$labour_amount;
					
					$sql = "INSERT INTO ".TB_PREF."locked_manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
					$sql .= db_escape($mfrg_id_new). ", " . 
							db_escape($row2['labour_type']). "," .
							db_escape($row2['special_rate']). "," .
							db_escape($labour_amount). "," .
							db_escape($special_labour_cost). ")";

					db_query($sql, "Labour Costing is not inserted");
					$total_special_cost += $special_labour_cost;
				}
			}
			
			$total_labour_cost = $basic_price + $total_special_cost;
			$sql = "update ".TB_PREF."locked_manufacturing_labour_cost set total_special_cost = ".db_escape($total_special_cost).
					", total_labour_cost = ".db_escape($total_labour_cost)." where mfrg_id = ".db_escape($mfrg_id_new);
			db_query($sql,"could not update labour cost");
			
		}
				
		$cons_wood_labour = $cons_plus_wood + $total_labour_cost;
		$extra_amount = ($cons_wood_labour*$extra_percent)/100;
		$extra_amount = truncate_number($extra_amount);
		
		$total_cost = $cons_wood_labour + $extra_amount;
		$admin_cost = ($total_cost*$admin_percent)/100;
		$admin_cost = truncate_number($admin_cost);
		
		$total_cp = $total_cost + $admin_cost;
		$profit = ($total_cp*$profit_percent)/100;
		$profit = truncate_number($profit);
		
		$total_mfrg = $total_cp + $profit;
		
		$sql = "update ".TB_PREF."locked_manufacturing_cost set total_labour_cost = ".db_escape($total_labour_cost).
				", extra_percent = ".db_escape($extra_percent).
				", extra_amount = ".db_escape($extra_amount).
				", total_cost = ".db_escape($total_cost).
				", admin_percent = ".db_escape($admin_percent).
				", admin_cost = ".db_escape($admin_cost).
				", total_cp = ".db_escape($total_cp).
				", profit_percent = ".db_escape($profit_percent).
				", profit = ".db_escape($profit).
				", total_mfrg = ".db_escape($total_mfrg).
				", date = '".date2sql($date).
				"' where id = ".db_escape($mfrg_id_new);
		db_query($sql,"could not update manufacturing cost");
		/*   Updating Manufacturing Cost Ends Here  */




		
		/*   Updating Finishing Cost Starts Here  */
		$sql = "select * from ".TB_PREF."locked_finishing_cost where finish_code_id = ".db_escape($pro['finish_code_id'])."
		        && version = ".db_escape($version);
	    $result = db_query($sql, "could not get finishing_cost");
		$row = db_fetch($result);
		
		$finishing_cost_id = $row['id'];
		$sanding_percent = $row['sanding_percent'];
		$polish_percent = $row['polish_percent'];
		$packaging_percent = $row['packaging_percent'];
		$forwarding_percent = $row['forwarding_percent'];
		
		$total_special_cost = $row['total_special_cost'];
		$other_cost = $row['other_cost'];

		if(get_post('sanding_percent')) {
			if($_POST['sanding_sign'] == "plus") { 
				$sanding_percent = $sanding_percent + $_POST['sanding_percent'];
			 } 
			 if($_POST['sanding_sign'] == "minus") { 
				$sanding_percent = $sanding_percent - $_POST['sanding_percent'];
			 } 
		}
		if(get_post('polish_percent')) {
			if($_POST['polish_sign'] == "plus") { 
				$polish_percent = $polish_percent + $_POST['polish_percent'];
			 } 
			 if($_POST['polish_sign'] == "minus") { 
				$polish_percent = $polish_percent - $_POST['polish_percent'];
			 }
		}
		if(get_post('packaging_percent')) {
			if($_POST['packaging_sign'] == "plus") { 
				$packaging_percent = $packaging_percent + $_POST['packaging_percent'];
			 } 
			 if($_POST['packaging_sign'] == "minus") { 
				$packaging_percent = $packaging_percent - $_POST['packaging_percent'];
			 }
		}
		if(get_post('forwarding_percent')) {
			if($_POST['forwarding_sign'] == "plus") { 
				$forwarding_percent = $forwarding_percent + $_POST['forwarding_percent'];
			 } 
			 if($_POST['forwarding_sign'] == "minus") { 
				$forwarding_percent = $forwarding_percent - $_POST['forwarding_percent'];
			 } 
		}
		

		$sanding_cost = ($total_mfrg*$sanding_percent)/100;
		$sanding_cost = truncate_number($sanding_cost);
		$polish_cost = ($total_mfrg*$polish_percent)/100;
		$polish_cost = truncate_number($polish_cost);
		$packaging_cost = ($total_mfrg*$packaging_percent)/100;
		$packaging_cost = truncate_number($packaging_cost);
		$forwarding_cost = ($total_mfrg*$forwarding_percent)/100;
		$forwarding_cost = truncate_number($forwarding_cost);
		
		
		$total_finishing_cost = $sanding_cost+$polish_cost+$packaging_cost+$forwarding_cost+$total_special_cost+$other_cost;
		$total_amount = $total_mfrg + $total_finishing_cost;
		
		$sql = "INSERT INTO ".TB_PREF."locked_finishing_cost(finish_code_id, ref_id, sanding_percent,
				sanding_cost,polish_percent,polish_cost,packaging_percent,packaging_cost,forwarding_percent,forwarding_cost,total_special_cost,other_cost,total_finishing_cost, total_amount, version, date) VALUES(";
		$sql .= db_escape($row['finish_code_id']) . "," .
				db_escape($ref_id) . "," .
				db_escape($sanding_percent) . "," .
				db_escape($sanding_cost) . "," .
				db_escape($polish_percent) . "," .
				db_escape($polish_cost) . "," .
				db_escape($packaging_percent) . "," .
				db_escape($packaging_cost) . "," .
				db_escape($forwarding_percent) . "," .
				db_escape($forwarding_cost) . "," .
				db_escape($total_special_cost) . "," .
				db_escape($other_cost) . "," .
				db_escape($total_finishing_cost) . "," .
				db_escape($total_amount) . ",'$new_version','". date2sql($date)."')";
		db_query($sql, "finishing cost is not inserted.");
		
		$finishing_cost_id_new = db_insert_id();
		insert_finishing_special_cost($finishing_cost_id, $finishing_cost_id_new);
		
		/*   Updating Finishing Cost Ends Here  */
		
		/*   Updating Final Cost Starts Here  */
		$sql = "select * from ".TB_PREF."locked_final_costing where finish_code_id = ".db_escape($pro['finish_code_id'])."
		        ANd version = ".db_escape($version);
	    $result = db_query($sql, "could not get final_costing");
		$row = db_fetch($result);
		
		$final_costing_id = $row['final_costing_id'];
		$final_profit_percent = $row['final_profit_percent'];
		$finishing_exp_percent = $row['finishing_exp_percent'];

		if(get_post('final_profit_percent')) {
			if($_POST['final_profit_sign'] == "plus") { 
				$final_profit_percent = $final_profit_percent + $_POST['final_profit_percent'];
			 } 
			 if($_POST['final_profit_sign'] == "minus") { 
				$final_profit_percent = $final_profit_percent - $_POST['final_profit_percent'];
			 } 
		}
		
		if(get_post('finishing_exp_percent')) {
			$finishing_exp_percent = $_POST['finishing_exp_percent'];
		}
		

		$total_percent = $sanding_percent + $polish_percent + $packaging_percent + $forwarding_percent;

		$final_finishing_cost = ($total_mfrg*$finishing_exp_percent)/100;
		$final_finishing_cost = truncate_number($final_finishing_cost);

		$total_fcost = max($final_finishing_cost , $total_finishing_cost) + $total_mfrg;
		$total_fcost = truncate_number($total_fcost);

		$final_profit = ($total_fcost*$final_profit_percent)/100;
		$final_profit = truncate_number($final_profit);

		$final_total_cost = $total_fcost + $final_profit;
		$final_total_cost = truncate_number($final_total_cost);
		
		$sql = "select * from ".TB_PREF."currency_master";
		$result = db_query($sql, "Could not get currencies.");
		$currencies = array();
		$count = 0;
		while($row = db_fetch($result))
		{
			$id = $row['id'];
			$_POST['currency'.$id] = $row['rate']*$final_total_cost;
			$_POST['currency'.$id] = truncate_number($_POST['currency'.$id]);
			$currencies[] = $id;
			$count++;
		}
		$currencies2 = implode(",",$currencies);
		$currency_price = array();
		$curr = explode(",",$currencies2);
		for($i=0; $i<$count; $i++)
		{
			$currency_price[][$curr[$i]] = $_POST['currency'.$curr[$i]];
		}
		$currency = json_encode($currency_price);
		
		
		$sql = "INSERT INTO ".TB_PREF."locked_final_costing(finish_code_id, ref_id, total_mfrg_cost,
			total_finishing_cost, total_finishing_percent, finishing_exp_percent, final_finishing_cost, total_cost, final_profit_percent, final_profit, final_total_cost, currency_price, version, date) VALUES(";
		$sql .= db_escape($pro['finish_code_id']) . "," .
			db_escape($ref_id) . "," .
			db_escape($total_mfrg) . "," .
			db_escape($total_finishing_cost) . "," .
			db_escape($total_finishing_percent) . "," .
			db_escape($finishing_exp_percent) . "," .
			db_escape($final_finishing_cost) . "," .
			db_escape($total_fcost) . "," .
			db_escape($final_profit_percent) . "," .
			db_escape($final_profit) . "," .
			db_escape($final_total_cost) . "," .
			db_escape($currency).",'$new_version','". date2sql($date)."')";
		db_query($sql, "final cost is not inserted.");
		/*   Updating Final Cost Ends Here  */
		$pro_count++;
	}
	display_notification('<span style="font-size:14px;">Cost of '.$pro_count.' Products have been updated.</span>');
}





function get_finish_product_list($category_id, $range_id)
{	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get finish product list");
	return $result;
}
function get_product_details($finish_code_id)
{	$sql = "SELECT finish_product_name, finish_comp_code FROM ".TB_PREF."finish_product WHERE finish_pro_id=".db_escape($finish_code_id);
	$result = db_query($sql, "could not get finish product list");
	$row = db_fetch($result);
	return $row;
}

function get_wood_name($wood_id){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($wood_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_rate($consumable_id)
{
	$sql = "SELECT max(purchase_price) FROM ".TB_PREF."supp_product where sub_category = ".db_escape($consumable_id);
	$query1 = db_query($sql,"get the ref code.");
	
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $purchase_price = $row['0'];     
	}
	if($purchase_price != "" && $purchase_price != NULL)
	{
		return $purchase_price;
	}
	else
	{
		$purchase_price = 0;
		return $purchase_price;
	}
}

function get_cons_select_name($cons_select){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}
function get_cons_type_name($cons_type){
	$sql = "SELECT master_name FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}


function check_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_mfrg_id($finish_pro_id, $version)
{
	$sql = "SELECT id FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id)." && version=".db_escape($version);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function get_finishing_cost_id($finish_code_id, $version)
{
	$sql = "SELECT id FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code_id)." && version=".db_escape($version);
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function insert_manufacturing_cost($manuf_cost, &$wood_session, &$labour_session)
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($manuf_cost['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version']+1;
	
	$sql = "INSERT INTO ".TB_PREF."manufacturing_cost(finish_code_id,total_cons_cost,total_finish_cons_cost,total_fabric_cost,total_wood_cost,
	total_labour_cost, extra_percent, extra_amount, total_cost,admin_percent,admin_cost,total_cp,profit_percent,profit,total_mfrg, version) VALUES(";
	$sql .= db_escape($manuf_cost['finish_code_id']) . "," .
	db_escape($manuf_cost['total_cons_cost']) . "," .
	db_escape($manuf_cost['total_finish_cons_cost']) . "," .
	db_escape($manuf_cost['total_fabric_cost']) . "," .
	db_escape($manuf_cost['total_wood_cost']) . "," .
	db_escape($manuf_cost['total_labour_cost']) . "," .
	db_escape($manuf_cost['extra_percent']) . "," .
	db_escape($manuf_cost['extra_amount']) . "," .
	db_escape($manuf_cost['total_cost']) . "," .
	db_escape($manuf_cost['admin_percent']) . "," .
	db_escape($manuf_cost['admin_cost']) . "," .
	db_escape($manuf_cost['total_cp']) . "," .
	db_escape($manuf_cost['profit_percent']) . "," .
	db_escape($manuf_cost['profit']) . "," .
	db_escape($manuf_cost['total_mfrg']) . ",'$version')";
	
	db_query($sql, "Manufacturing cost is not inserted.");
	$mfrg_id = db_insert_id();
	
	$sql = "update ".TB_PREF."costing_version set version = ".db_escape($version)." where finish_code_id = ".db_escape($manuf_cost['finish_code_id']);
	db_query($sql, "Costing version is not updated");
	
	
	
	for($i = 0;$i < $_POST['design_cons_no'];$i++ )
	{
		$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['des_master_name_'.$i]). "," .
		db_escape($_POST['des_consumable_name_'.$i]). "," .
		db_escape($_POST['des_cons_unit_'.$i]). "," .
		db_escape($_POST['des_cons_qty_'.$i]). "," .
		db_escape($_POST['des_rate_'.$i]). "," .
		db_escape($_POST['des_cost_'.$i]). ")";
		db_query($sql, "Design consumable Costing is not inserted");
	}
	 
	 for($i = 0;$i < $_POST['finish_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['finish_master_name_'.$i]). "," .
		db_escape($_POST['finish_consumable_name_'.$i]). "," .
		db_escape($_POST['finish_cons_unit_'.$i]). "," .
		db_escape($_POST['finish_cons_qty_'.$i]). "," .
		db_escape($_POST['finish_cons_rate_'.$i]). "," .
		db_escape($_POST['finish_cost_'.$i]). ")";
		db_query($sql, "Finish consumable Costing is not inserted");
     }
	
	for($i = 0;$i < $_POST['fabric_cons_no'];$i++ )
     {
		$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($_POST['fabric_name_'.$i]). "," .
		db_escape($_POST['perecentage_'.$i]). "," .
		db_escape($_POST['fabric_rate_'.$i]). "," .
		db_escape($_POST['fabric_cost_'.$i]). ")";
		db_query($sql, "Fabric consumable Costing is not inserted");
     }
	
	
	foreach ($wood_session->line_items as $line_no => $wood_line)
     {
		$sql = "INSERT INTO ".TB_PREF."manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($wood_line->wood_id). "," .
		db_escape($wood_line->cft). "," .
		db_escape($wood_line->rate). "," .
		db_escape($wood_line->amount). ")";
		db_query($sql, "Wood Costing is not inserted");
     }
	 
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_cost(mfrg_id, basic_price, total_special_cost, total_labour_cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($manuf_cost['basic_price']). "," .
		db_escape($manuf_cost['total_special_cost']). "," .
		db_escape($manuf_cost['total_labour_cost']). ")";
		db_query($sql, "Labour Costing is not inserted");

	 
	 foreach ($labour_session->line_items as $line_no => $labour_line)
     {
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
		$sql .=  db_escape($mfrg_id). ", " . 
		db_escape($labour_line->labour_type). "," .
		db_escape($labour_line->special_rate). "," .
		db_escape($labour_line->labour_amount). "," .
		db_escape($labour_line->special_labour_cost). ")";
		db_query($sql, "Labour Costing is not inserted");
     } 
	return true; 
}

function get_finish_product_list_from_manufacturing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."manufacturing_cost m on m.finish_code_id = f.finish_pro_id WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get product list");
	return $result;
}

function get_total_mfrg_cost($finish_code)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total MFRG");
	$total_mfrg = db_fetch($result);
	return $total_mfrg['total_mfrg'];
}

function get_total_finishing_cost($finish_code)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total finishing");
	$total_finishing_cost = db_fetch($result);
	return $total_finishing_cost['total_finishing_cost'];
}

function get_total_percent($finish_code)
{
	$sql = "SELECT sanding_percent, polish_percent, packaging_percent, forwarding_percent FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total percent");
	$total_percent = db_fetch($result);
	return $total_percent;
}

function get_total_export_cost($finish_code)
{
	$sql = "SELECT sanding_cost, polish_cost, packaging_cost, forwarding_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total export cost");
	$total_export_cost = db_fetch($result);
	return $total_export_cost;
}


function check_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function insert_finishing_cost($finishing_cost, &$finishing_session)
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($finishing_cost['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version'];
	
	$sql = "INSERT INTO ".TB_PREF."finishing_cost(finish_code_id,sanding_percent, sanding_cost,polish_percent,polish_cost,
	packaging_percent,packaging_cost,forwarding_percent,forwarding_cost,total_special_cost,other_cost,total_finishing_cost,
	 total_amount, version) VALUES(";
	$sql .= db_escape($finishing_cost['finish_code_id']) . "," .
			db_escape($finishing_cost['sanding_percent']) . "," .
			db_escape($finishing_cost['sanding_cost']) . "," .
			db_escape($finishing_cost['polish_percent']) . "," .
			db_escape($finishing_cost['polish_cost']) . "," .
			db_escape($finishing_cost['packaging_percent']) . "," .
			db_escape($finishing_cost['packaging_cost']) . "," .
			db_escape($finishing_cost['forwarding_percent']) . "," .
			db_escape($finishing_cost['forwarding_cost']) . "," .
			db_escape($finishing_cost['total_special_cost']) . "," .
			db_escape($finishing_cost['other_cost']) . "," .
			db_escape($finishing_cost['total_finishing_cost']) . "," .
			db_escape($finishing_cost['total_amount']) . ",'$version')";
	db_query($sql, "finishing cost is not inserted.");
	
	$finishing_cost_id = db_insert_id();
	
	foreach ($finishing_session->line_items as $line_no => $finish_line)
     {
		$sql = "INSERT INTO ".TB_PREF."finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
		$sql .= db_escape($finishing_cost_id). ", " . 
				db_escape($finish_line->special_cost_ref). "," .
				db_escape($finish_line->special_rate). "," .
				db_escape($finish_line->special_amount). "," .
				db_escape($finish_line->total). ")";
		db_query($sql, "finishing special cost Costing is not inserted");
     }
	return true; 
}




function insert_final_cost()
{
	$sql = "select version from ".TB_PREF."costing_version where finish_code_id = ".db_escape($_POST['finish_code_id']);
	$result = db_query($sql, "Costing version could not be retrieved");
	$row = db_fetch($result);
	$version = $row['version'];

	$currency_price = array();
	$curr = explode(",",$_POST['currencies']);
	for($i=0; $i<$_POST['total_currency']; $i++)
	{
		$currency_price[][$curr[$i]] = $_POST['currency'.$curr[$i]];
	}

	$currency = json_encode($currency_price);

	$sql = "INSERT INTO ".TB_PREF."final_costing(finish_code_id,total_mfrg_cost,
			total_finishing_cost, total_amount, final_profit_percent, final_profit, total_percent, total_export_cost,
			total_fob_cost, finishing_exp_percent, final_finishing_cost, sp_by_process, sp_by_lumsum, final_total_cost, currency_price, version) VALUES(";
	$sql .= db_escape($_POST['finish_code_id']) . "," .
			db_escape($_POST['total_mfrg_cost']) . "," .
			db_escape($_POST['total_finishing_cost']) . "," .
			db_escape($_POST['total_amount']) . "," .
			db_escape($_POST['final_profit_percent']) . "," .
			db_escape($_POST['final_profit']) . "," .
			db_escape($_POST['total_percent']) . "," .
			db_escape($_POST['total_export_cost']) . "," .
			db_escape($_POST['total_fob_cost']) . "," .
			db_escape($_POST['finishing_exp_percent']) . "," .
			db_escape($_POST['final_finishing_cost']) . "," .
			db_escape($_POST['sp_by_process']) . "," .
			db_escape($_POST['sp_by_lumsum']) . "," .
			db_escape($_POST['final_total_cost']) . "," .
			db_escape($currency).",'$version')";
	db_query($sql, "final cost is not inserted.");
	return true; 
}

function get_finish_product_list_from_finishing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."finishing_cost m on m.finish_code_id = f.finish_pro_id WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get product list");
	return $result;
}
function check_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT total_final_cost FROM ".TB_PREF."final_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check Final Cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = "disabled";
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}
function is_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT final_total_cost FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_currency_name($id)
{
	$sql = "select currency_name from ".TB_PREF."currency_master where id=".$id;
	$result = db_query($sql);
	$row = db_fetch($result);
	return $row['currency_name'];
}

?>