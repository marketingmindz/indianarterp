<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function add_supplier($supp_no,$supp_name, $supp_ref, $address, $supp_address,$tin_no,$cin_no,$pan_no,$can_no,$st_no,
	 $credit_limit,$curr_code, $payment_terms, $tax_included)  
{
	$sql = "INSERT INTO ".TB_PREF."suppliers (supp_no,supp_name, supp_ref, address, supp_address, tin_no, cin_no,
		pan_no, can_no,st_no,credit_limit, curr_code,payment_terms,tax_included)
		VALUES (".db_escape($supp_no). ", "
		.db_escape($supp_name). ", "
		.db_escape($supp_ref). ", "
		.db_escape($address) . ", "
		.db_escape($supp_address) . ", "
		.db_escape($tin_no). ", "
		.db_escape($cin_no). ", "
		.db_escape($pan_no). ", "
		.db_escape($can_no). ", "
		.db_escape($st_no). ", "
		.db_escape($credit_limit). ", "
		.db_escape($curr_code). ", "
		.db_escape($payment_terms). ", "
		.db_escape($tax_included). ")";

	db_query($sql,"The supplier could not be added");
}

function update_supplier($supplier_id,$supp_no,$supp_name, $supp_ref, $address, $supp_address, $tin_no, 
	$cin_no, $pan_no, $can_no,$st_no, $credit_limit,$curr_code, $payment_terms,$tax_included)
{
	$sql = "UPDATE ".TB_PREF."suppliers SET supp_no=".db_escape($supp_no) . ",
		supp_name=".db_escape($supp_name) . ",
		supp_ref=".db_escape($supp_ref) . ",
		address=".db_escape($address) . ",
		supp_address=".db_escape($supp_address) . ",
		tin_no=".db_escape($tin_no) . ",
		cin_no=".db_escape($cin_no) . ",
		pan_no=".db_escape($pan_no) . ",
		can_no=".db_escape($can_no) . ",
		st_no=".db_escape($st_no) . ",
		credit_limit=".$credit_limit . ",
		curr_code=".db_escape($curr_code).",
		payment_terms=".db_escape($payment_terms) . ",
		tax_included=".db_escape($tax_included)
		." WHERE supplier_id = ".db_escape($supplier_id);

	db_query($sql,"The supplier could not be updated");
}

function delete_supplier($supplier_id)
{
	$sql="DELETE FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);
	db_query($sql,"check failed");
}

function get_supplier_details($supplier_id, $to=null, $all=true)
{

	if ($to == null)
		$todate = date("Y-m-d");
	else
		$todate = date2sql($to);
	$past1 = get_company_pref('past_due_days');
	$past2 = 2 * $past1;
	// removed - supp_trans.alloc from all summations

	if ($all)
    	$value = "(trans.ov_amount + trans.ov_gst + trans.ov_discount)";
    else	
    	$value = "IF (trans.type=".ST_SUPPINVOICE." OR trans.type=".ST_BANKDEPOSIT.",
    		(trans.ov_amount + trans.ov_gst + trans.ov_discount - trans.alloc),
    		(trans.ov_amount + trans.ov_gst + trans.ov_discount + trans.alloc))";
	$due = "IF (trans.type=".ST_SUPPINVOICE." OR trans.type=".ST_SUPPCREDIT.",trans.due_date,trans.tran_date)";
    $sql = "SELECT supp.supp_name, supp.curr_code, ".TB_PREF."payment_terms.terms,

		Sum(IFNULL($value,0)) AS Balance,

		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= 0,$value,0)) AS Due,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past1,$value,0)) AS Overdue1,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past2,$value,0)) AS Overdue2,
		supp.credit_limit - Sum(IFNULL(IF(trans.type=".ST_SUPPCREDIT.", -1, 1) 
			* (ov_amount + ov_gst + ov_discount),0)) as cur_credit

		FROM ".TB_PREF."suppliers supp
			 LEFT JOIN ".TB_PREF."supp_trans trans ON supp.supplier_id = trans.supplier_id AND trans.tran_date <= '$todate',
			 ".TB_PREF."payment_terms

		WHERE
			 supp.payment_terms = ".TB_PREF."payment_terms.terms_indicator
			 AND supp.supplier_id = $supplier_id ";
	if (!$all)
		$sql .= "AND ABS(trans.ov_amount + trans.ov_gst + trans.ov_discount) - trans.alloc > ".FLOAT_COMP_DELTA." ";  
	$sql .= "GROUP BY
			  supp.supp_name,
			  ".TB_PREF."payment_terms.terms,
			  ".TB_PREF."payment_terms.days_before_due,
			  ".TB_PREF."payment_terms.day_in_following_month";

    $result = db_query($sql,"The customer details could not be retrieved");
    $supp = db_fetch($result);

    return $supp;
}

function get_finish_product_list($category_id, $range_id)
{
	echo $sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);

	//$result = db_query($sql, "could not get supplier");
display_error("sfs");
	return db_fetch($result);
}

function get_supplier_name($supplier_id)
{
	$sql = "SELECT supp_name AS name FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_supplier_accounts($supplier_id)
{
	$sql = "SELECT payable_account,purchase_account,payment_discount_account FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	return db_fetch($result);
}

function get_supplier_contacts($supplier_id, $action=null)
{
	$results = array();
	$res = get_crm_persons('supplier', $action, $supplier_id);
	while($contact = db_fetch($res))
		$results[] = $contact;

	return $results;
}

function get_current_supp_credit($supplier_id)
{
	$suppdet = get_supplier_details($supplier_id);
	return $suppdet['cur_credit'];

}

function is_new_supplier($id)
{
	$tables = array('supp_trans', 'grn_batch', 'purch_orders', 'purch_data');

	return !key_in_foreign_table($id, $tables, 'supplier_id');
}



function add_product($supplier_id,$catagory, $sub_category, $purchase_price, $lead_time)
{
	
	$sql = "INSERT INTO ".TB_PREF."supp_product(supplier_id,catagory, sub_category, purchase_price,lead_time)
		VALUES ("  . db_escape($supplier_id) . ", " . db_escape($catagory) . ", " .db_escape($sub_category) . ", " . db_escape($purchase_price) . ", " . db_escape($lead_time). ")";		
        
   	   db_query($sql,"could not add new product");
	  
}

//--------------------------------------------------------------------------------------------------

function update_product($id,  $catagory, $sub_category, $purchase_price, $lead_time)
{
      $sql = "UPDATE ".TB_PREF."supp_product SET catagory=".db_escape($catagory)
	  .",sub_category=".db_escape($sub_category)
	  .",purchase_price=".db_escape($purchase_price)
      .",lead_time=".db_escape($lead_time)
      ."WHERE product_id=".db_escape($id);
    	db_query($sql, "could not update product");
}
//--------------------------------------------------------------------------------------------------

function delete_product($id)
{
	 $sql = "DELETE FROM ".TB_PREF."supp_product WHERE product_id=".db_escape($id);
	 db_query($sql, "could not delete product");
}
//--------------------------------------------------------------------------------------------------
function get_all_product_list($supp_id) {
  $sql = "SELECT * FROM ".TB_PREF."supp_product where supplier_id = ".db_escape($supp_id);
	//if (!$all) $sql .= " WHERE !inactive";
    return  db_query($sql, "could not get product");
}

function get_product($selected_id)
{
	$sql="SELECT * FROM ".TB_PREF."supp_product WHERE product_id=".db_escape($selected_id);
	$result = db_query($sql,"product could not be retrieved");
	return db_fetch($result);
}
//------------ get supplier id (code by chetan)--------------
function get_product_supplier_id($supp_id){
	
	$sql="SELECT * FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supp_id);
	$result = db_query($sql,"supplier could not be retrieved");
	return db_fetch($result);
}

//------------ get consuable category ( code by chetan )----------

function get_product_cons_category($cons_id){
	$sql = "SELECT * FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_product_cons_subcategory($sub_cons_id){
	$sql = "SELECT * FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($sub_cons_id);
	$result = db_query($sql);
	return db_fetch($result);
}


// ----------------------- get supplier no. ( code by bajrang ) -------------------
function get_supplier_id()
{
	$supp_no = '';
	$sql = "SELECT supp_no FROM ".TB_PREF."suppliers ORDER BY supp_no DESC LIMIT 1";
	
	$query1 = db_query($sql,"cannot get purchase request no.");
	
	while($row = db_fetch($query1,MYSQL_NUM))
	   {
		   $supp_no= $row['supp_no'];
		   $supp_no++;
		   
		}
	if($supp_no == '' && $supp_no == NULL)
	{
		$supp_no = 1;
	}
	return $supp_no;	
}
?>