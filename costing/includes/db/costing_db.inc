<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_finish_product_list($category_id, $range_id)
{	$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);
	$result = db_query($sql, "could not get finish product list");
	return $result;
}

function get_wood_name($wood_id){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($wood_id);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_rate($consumable_id)
{
	$sql = "SELECT max(purchase_price) FROM ".TB_PREF."supp_product where sub_category = ".db_escape($consumable_id);
	$query1 = db_query($sql,"get the ref code.");
	
	while($row = db_fetch($query1,MYSQL_NUM))
	{
	   $purchase_price = $row['0'];     
	}
	if($purchase_price != "" && $purchase_price != NULL)
	{
		return $purchase_price;
	}
	else
	{
		$purchase_price = 0;
		return $purchase_price;
	}
}

function get_cons_select_name($cons_select){
	$sql = "SELECT consumable_name FROM ".TB_PREF."consumable_master WHERE consumable_id=".db_escape($cons_select);
	$result = db_query($sql);
	return db_fetch($result);
}

function get_cons_type_name($cons_type){
	$sql = "SELECT master_name FROM ".TB_PREF."master_creation WHERE master_id=".db_escape($cons_type);
	$result = db_query($sql);
	return db_fetch($result);
}

//----------------------------------- Manufacturing Step Operation --------------------------------

function check_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		$disabled = 'style="color:green; font-size:14px;"';
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}

function is_exist_mfrg_cost($finish_pro_id)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check mfrg");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_mfrg_id($finish_pro_id,$version=0)
{
	$sql = "SELECT id FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	if($version != '0')
	{
		$sql .= " && version = ".db_escape($version);
	}
	$result = db_query($sql, "could not check mfrg");
	$row = db_fetch($result);
	return $row['id'];
}

function insert_manufacturing_cost($manuf_cost, &$wood_session, &$labour_session)
{
	$sql = "SELECT version from ".TB_PREF."costing_version where finish_code_id =".db_escape($manuf_cost['man_finish_code_id']);
	db_query($sql, "Costing version could not be checked");
	if(db_num_rows < 1)
	{
		$sql = "INSERT INTO ".TB_PREF."manufacturing_cost(finish_code_id, ref_id, total_cons_cost,total_finish_cons_cost,total_fabric_cost,total_wood_cost,
		total_labour_cost, extra_percent, extra_amount, total_cost,admin_percent,admin_cost,total_cp,profit_percent,profit,total_mfrg, version, date) VALUES(";
		$sql .= db_escape($manuf_cost['man_finish_code_id']) . "," .
		db_escape($manuf_cost['ref_id']) . "," .
		db_escape($manuf_cost['total_cons_cost']) . "," .
		db_escape($manuf_cost['total_finish_cons_cost']) . "," .
		db_escape($manuf_cost['total_fabric_cost']) . "," .
		db_escape($manuf_cost['total_wood_cost']) . "," .
		db_escape($manuf_cost['total_labour_cost']) . "," .
		db_escape($manuf_cost['extra_percent']) . "," .
		db_escape($manuf_cost['extra_amount']) . "," .
		db_escape($manuf_cost['total_cost']) . "," .
		db_escape($manuf_cost['admin_percent']) . "," .
		db_escape($manuf_cost['admin_cost']) . "," .
		db_escape($manuf_cost['total_cp']) . "," .
		db_escape($manuf_cost['profit_percent']) . "," .
		db_escape($manuf_cost['profit']) . "," .
		db_escape($manuf_cost['total_mfrg']) . ",'1','". date2sql($_POST['date'])."')";
		db_query($sql, "Manufacturing cost is not inserted.");
		$mfrg_id = db_insert_id();
		
		$sql = "INSERT INTO ".TB_PREF."costing_version(finish_code_id, version) VALUES (";
		$sql .=  db_escape($manuf_cost['man_finish_code_id']). ", '1')";
		db_query($sql, "Costing version is not inserted");
		
		
		
		for($i = 0;$i < $_POST['design_cons_no'];$i++ )
		{
			$sql = "INSERT INTO ".TB_PREF."man_design_cons_cost(mfrg_id, consumable_name, consumable_category, unit, quantity, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['des_master_name_'.$i]). "," .
			db_escape($_POST['des_consumable_name_'.$i]). "," .
			db_escape($_POST['des_cons_unit_'.$i]). "," .
			db_escape($_POST['des_cons_qty_'.$i]). "," .
			db_escape($_POST['des_rate_'.$i]). "," .
			db_escape($_POST['des_cost_'.$i]). ")";
			db_query($sql, "Design consumable Costing is not inserted");
		}
		 
		 for($i = 0;$i < $_POST['finish_cons_no'];$i++ )
		 {
			$sql = "INSERT INTO ".TB_PREF."man_finish_cons_cost(mfrg_id, consumable_id, consumable_category, unit, quantity, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['finish_master_name_'.$i]). "," .
			db_escape($_POST['finish_consumable_name_'.$i]). "," .
			db_escape($_POST['finish_cons_unit_'.$i]). "," .
			db_escape($_POST['finish_cons_qty_'.$i]). "," .
			db_escape($_POST['finish_cons_rate_'.$i]). "," .
			db_escape($_POST['finish_cost_'.$i]). ")";
			db_query($sql, "Finish consumable Costing is not inserted");
		 }
		
		for($i = 0;$i < $_POST['fabric_cons_no'];$i++ )
		 {
			$sql = "INSERT INTO ".TB_PREF."man_fabric_cons_cost(mfrg_id, fabric_id, percentage, rate, cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($_POST['fabric_name_'.$i]). "," .
			db_escape($_POST['perecentage_'.$i]). "," .
			db_escape($_POST['fabric_rate_'.$i]). "," .
			db_escape($_POST['fabric_cost_'.$i]). ")";
			db_query($sql, "Fabric consumable Costing is not inserted");
		 }
		
		
		foreach ($wood_session->line_items as $line_no => $wood_line)
		 {
			$sql = "INSERT INTO ".TB_PREF."manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($wood_line->wood_id). "," .
			db_escape($wood_line->cft). "," .
			db_escape($wood_line->rate). "," .
			db_escape($wood_line->amount). ")";
			db_query($sql, "Wood Costing is not inserted");
		 }
		 
			$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_cost(mfrg_id, basic_price, total_special_cost, total_labour_cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($manuf_cost['basic_price']). "," .
			db_escape($manuf_cost['total_special_cost']). "," .
			db_escape($manuf_cost['total_labour_cost']). ")";
			db_query($sql, "Labour Costing is not inserted");
	
		 
		 foreach ($labour_session->line_items as $line_no => $labour_line)
		 {
			$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
			$sql .=  db_escape($mfrg_id). ", " . 
			db_escape($labour_line->labour_type). "," .
			db_escape($labour_line->special_rate). "," .
			db_escape($labour_line->labour_amount). "," .
			db_escape($labour_line->special_labour_cost). ")";
			db_query($sql, "Labour Costing is not inserted");
		 }
	}
	return true;
}

function get_manufacturing_cost($mfrg_id)
{
	//-------------------------- Wood data ---------------------------------

	$sql = "SELECT c.consumable_name, f.* 
		from ".TB_PREF."manufacturing_wood_cost f 
		LEFT JOIN ".TB_PREF."consumable_master c on c.consumable_id = f.wood_id 
		WHERE f.mfrg_id=".$mfrg_id;

	$result = db_query($sql,"Could not get wood cost");

	$_SESSION['wood'] = new wood;

	while($row = db_fetch($result))
	{
		$_SESSION['wood']->add_to_wood_part(count($_SESSION['wood']->line_items),$row['wood_id'],$row['cft'],$row['rate'],$row['amount']);
	}

	//-------------------------- Basic Price ---------------------------------

	$sql1 = "SELECT basic_price from ".TB_PREF."manufacturing_labour_cost where mfrg_id=".$mfrg_id;
	$result1 = db_query($sql1,"Could not get labour cost");

	while($row1 = db_fetch($result1))
	{
		$_POST['basic_price'] = $row1['basic_price'];
	}

	//-------------------------- Labour Price ---------------------------------

	$_SESSION['labour'] = new labour;

	$sql2 = "SELECT labour_type,special_rate,labour_amount,special_labour_cost from ".TB_PREF."manufacturing_labour_special_cost where mfrg_id=".$mfrg_id;//.$finish_code_id;
	$result2 = db_query($sql2,"Could not get labour special cost");

	while($row2 = db_fetch($result2))
	{
		$_SESSION['labour']->add_to_labour_part(count($_SESSION['labour']->line_items),$row2['labour_type'],$row2['special_rate'],$row2['labour_amount'], $row2['special_labour_cost']);
	}

	//-------------------------- Extra data ---------------------------------

	$sql3 = "SELECT extra_percent, extra_amount, total_cost, admin_percent, admin_cost, total_cp, profit_percent, profit, total_mfrg  from ".TB_PREF."manufacturing_cost where id=".$mfrg_id;
	$result3 = db_query($sql3,"Could not get manufacturing cost");

	while($row3 = db_fetch($result3))
	{
		$_POST['extra_percent'] = $row3['extra_percent'];
		$_POST['extra_amount'] = $row3['extra_amount'];
		$_POST['total_cost'] = $row3['total_cost'];
		$_POST['admin_percent'] = $row3['admin_percent'];
		$_POST['admin_cost'] = $row3['admin_cost'];
		$_POST['total_cp'] = $row3['total_cp'];
		$_POST['profit_percent'] = $row3['profit_percent'];
		$_POST['profit'] = $row3['profit'];
		$_POST['total_mfrg'] = $row3['total_mfrg'];
	}
}

function update_manufacturing_cost($manuf_cost, $wood_session, $labour_session)
{
	$sql = "UPDATE ".TB_PREF."manufacturing_cost SET
			total_wood_cost=".db_escape($manuf_cost['total_wood_cost']).",
			total_labour_cost=".db_escape($manuf_cost['total_labour_cost']).",
			extra_percent=".db_escape($manuf_cost['extra_percent']).",
			extra_amount=".db_escape($manuf_cost['extra_amount']).",
			total_cost=".db_escape($manuf_cost['total_cost']).",
			admin_percent=".db_escape($manuf_cost['admin_percent']).",
			admin_cost=".db_escape($manuf_cost['admin_cost']).",
			total_cp=".db_escape($manuf_cost['total_cp']).",
			profit_percent=".db_escape($manuf_cost['profit_percent']).",
			profit=".db_escape($manuf_cost['profit']).",
			total_mfrg=".db_escape($manuf_cost['total_mfrg']);

	$sql .= " WHERE id=".db_escape($manuf_cost['mfrg_id']);
	db_query($sql, "Attachment could not be updated");

	//------------------------------ update wood data --------------------------------------------

	$sql = "DELETE FROM ".TB_PREF."manufacturing_wood_cost WHERE mfrg_id = ".db_escape($manuf_cost['mfrg_id']);
	db_query($sql, "Could not delete attachment");

	foreach ($wood_session->line_items as $line_no => $wood_line)
	{
		$sql = "INSERT INTO ".TB_PREF."manufacturing_wood_cost(mfrg_id, wood_id, cft, rate, amount) VALUES (";
		$sql .=  db_escape($manuf_cost['mfrg_id']). ", " . 
		db_escape($wood_line->wood_id). "," .
		db_escape($wood_line->cft). "," .
		db_escape($wood_line->rate). "," .
		db_escape($wood_line->amount). ")";
		db_query($sql, "Wood Costing is not inserted");
	}
	
	//------------------------------ update labour data --------------------------------------------

	$sql = "UPDATE ".TB_PREF."manufacturing_labour_cost SET
			basic_price=".db_escape($manuf_cost['basic_price']).",
			total_special_cost=".db_escape($manuf_cost['total_special_cost']).",
			total_labour_cost=".db_escape($manuf_cost['total_labour_cost']);

	$sql .= " WHERE mfrg_id=".db_escape($manuf_cost['mfrg_id']);
	db_query($sql, "Attachment could not be updated");


	$sql = "DELETE FROM ".TB_PREF."manufacturing_labour_special_cost WHERE mfrg_id = ".db_escape($manuf_cost['mfrg_id']);
	db_query($sql, "Could not delete attachment");

	foreach ($labour_session->line_items as $line_no => $labour_line)
	{
		$sql = "INSERT INTO ".TB_PREF."manufacturing_labour_special_cost(mfrg_id, labour_type, special_rate, labour_amount, special_labour_cost) VALUES (";
		$sql .=  db_escape($manuf_cost['mfrg_id']). ", " . 
		db_escape($labour_line->labour_type). "," .
		db_escape($labour_line->special_rate). "," .
		db_escape($labour_line->labour_amount). "," .
		db_escape($labour_line->special_labour_cost). ")";
		db_query($sql, "Labour Costing is not inserted");
	}
}

function update_finishing_cost_on_change_manufacturing_cost($manuf_cost)
{
	$sql = "SELECT * FROM ".TB_PREF."finishing_cost WHERE finish_code_id =".$manuf_cost['man_finish_code_id']." && version = '1'";
	$result = db_query($sql, "Could not get finishing cost");
	while($row = db_fetch($result))
	{
		$updated_finishing = array();

		$sanding_cost = ($manuf_cost['total_mfrg'] * $row['sanding_percent'])/100;
		$updated_finishing['updated_sanding_cost'] = $sanding_cost + $row['sanding_labour'];
		$updated_finishing['updated_sanding_cost'] = number_format((float)$updated_finishing['updated_sanding_cost'], 2, '.', '');

		$polish_cost = ($manuf_cost['total_mfrg'] * $row['polish_percent'])/100;
		$updated_finishing['updated_polish_cost'] = $polish_cost + $row['polish_labour'];
		$updated_finishing['updated_polish_cost'] = number_format((float)$updated_finishing['updated_polish_cost'], 2, '.', '');

		$packaging_cost = ($manuf_cost['total_mfrg'] * $row['packaging_percent'])/100;
		$updated_finishing['updated_packaging_cost'] = $packaging_cost + $row['packaging_labour'];
		$updated_finishing['updated_packaging_cost'] = number_format((float)$updated_finishing['updated_packaging_cost'], 2, '.', '');

		$total1 = $updated_finishing['updated_sanding_cost'] + $updated_finishing['updated_polish_cost'] + $updated_finishing['updated_packaging_cost'];
		$total2 = $row['actual_sending_cost'] + $row['actual_polish_cost'] + $row['actual_packaging_cost'];

		$updated_finishing['updated_lumsum_cost_value'] = ($manuf_cost['total_mfrg'] * $row['lumsum_cost_percent'])/100;
		$updated_finishing['updated_lumsum_cost_value'] = number_format((float)$updated_finishing['updated_lumsum_cost_value'], 2, '.', '');

		$total_average = ($total1 + $total2 + $updated_finishing['updated_lumsum_cost_value'])/3;

		$updated_finishing['updated_forwarding_cost'] = ($manuf_cost['total_mfrg'] * $row['forwarding_percent'])/100;
		$updated_finishing['updated_forwarding_cost'] = number_format((float)$updated_finishing['updated_forwarding_cost'], 2, '.', '');

		$updated_finishing['total_other_than_process_cost'] = $updated_finishing['updated_forwarding_cost'] + $row['total_special_cost'] + $row['other_cost'];

		$updated_finishing['total_finishing_cost'] = $total_average + $updated_finishing['total_other_than_process_cost'];
		$updated_finishing['total_finishing_cost'] = number_format((float)$updated_finishing['total_finishing_cost'], 2, '.', '');

		$updated_finishing['total_amount'] = $manuf_cost['total_mfrg'] + $updated_finishing['total_finishing_cost'];
		$updated_finishing['total_amount'] = number_format((float)$updated_finishing['total_amount'], 2, '.', '');

		$sql = "UPDATE ".TB_PREF."finishing_cost SET
				sanding_cost=".db_escape($updated_finishing['updated_sanding_cost']).",
				polish_cost=".db_escape($updated_finishing['updated_polish_cost']).",
				packaging_cost=".db_escape($updated_finishing['updated_packaging_cost']).",
				lumsum_cost_value=".db_escape($updated_finishing['updated_lumsum_cost_value']).",
				forwarding_cost=".db_escape($updated_finishing['updated_forwarding_cost']).",
				total_finishing_cost=".db_escape($updated_finishing['total_finishing_cost']).",
				total_amount=".db_escape($updated_finishing['total_amount']);

		$sql .= " WHERE id=".db_escape($row['id']);
		db_query($sql, "Attachment could not be updated");
	}
	return $updated_finishing;
}

function update_final_cost_on_change_manufacturing_cost($manuf_cost, $updated_finishing)
{
	$sql1 = "SELECT * FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($manuf_cost['man_finish_code_id']);
	$result1 = db_query($sql1, "Could not get final costing");
	$final_cost_data = db_fetch($result1);

	$updated_final = array();
	$updated_final['total_mfrg'] = $manuf_cost['total_mfrg'];
	$updated_final['total_finishing_cost'] = $updated_finishing['total_finishing_cost'];
	$updated_final['total_cost'] = $updated_final['total_mfrg'] + $updated_final['total_finishing_cost'];

	$updated_final['final_profit'] = ($updated_final['total_cost'] * $final_cost_data['final_profit_percent'])/100;
	$updated_final['final_profit'] = number_format((float)$updated_final['final_profit'], 2, '.', '');

	$updated_final['final_total_cost'] = $updated_final['final_profit'] + $updated_final['total_cost'];
	$updated_finishing['final_total_cost'] = number_format((float)$updated_finishing['final_total_cost'], 2, '.', '');

	$sql = "UPDATE ".TB_PREF."final_costing SET
			total_mfrg_cost=".db_escape($updated_final['total_mfrg']).",
			total_finishing_cost=".db_escape($updated_final['total_finishing_cost']).",
			total_cost=".db_escape($updated_final['total_cost']).",
			final_profit=".db_escape($updated_final['final_profit']).",
			final_total_cost=".db_escape($updated_final['final_total_cost']);

	$sql .= " WHERE finish_code_id=".db_escape($manuf_cost['man_finish_code_id']);
	db_query($sql, "Data could not be updated");

	return $updated_final;
}

//----------------------------------- Finishing Step Operation --------------------------------

function get_finish_product_list_from_manufacturing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."manufacturing_cost m on m.finish_code_id = f.finish_pro_id WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id)." && m.version = '1'";
	$result = db_query($sql, "could not get product list");
	return $result;
}

function get_total_mfrg_cost($finish_code)
{
	$sql = "SELECT total_mfrg FROM ".TB_PREF."manufacturing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total MFRG");
	$total_mfrg = db_fetch($result);
	return $total_mfrg['total_mfrg'];
}

function get_total_finishing_cost($finish_code)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total finishing");
	$total_finishing_cost = db_fetch($result);
	return $total_finishing_cost['total_finishing_cost'];
}

function get_total_percent($finish_code)
{
	$sql = "SELECT sanding_percent, polish_percent, packaging_percent, forwarding_percent FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total percent");
	$total_percent = db_fetch($result);
	return $total_percent;
}

function get_total_export_cost($finish_code)
{
	$sql = "SELECT sanding_cost, polish_cost, packaging_cost, forwarding_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code);
	$result = db_query($sql, "could not get total export cost");
	$total_export_cost = db_fetch($result);
	return $total_export_cost;
}

function check_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = 'style="color:green; font-size:14px;"';
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}

function is_exist_finishing_cost($finish_pro_id)
{
	$sql = "SELECT total_finishing_cost FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function insert_finishing_cost($finishing_cost, &$finishing_session)
{
	$sql = "INSERT INTO ".TB_PREF."finishing_cost(
					finish_code_id, 
					ref_id, 
					sanding_labour, 
					sanding_percent,
					sanding_cost, 
					polish_labour, 
					polish_percent, 
					polish_cost, 
					packaging_labour, 
					packaging_percent, 
					packaging_cost, 
					actual_sending_cost,
					actual_polish_cost,
					actual_packaging_cost, 
					lumsum_cost_percent, 
					lumsum_cost_value, 
					forwarding_percent, 
					forwarding_cost, 
					total_special_cost, 
					other_cost, 
					total_finishing_cost, 
					total_amount, 
					version, 
					date ) VALUES(";
		$sql .= db_escape($finishing_cost['finish_code_id']) . "," .
				db_escape($finishing_cost['ref_id']) . "," .
				db_escape($finishing_cost['sanding_labour']) . "," .
				db_escape($finishing_cost['sanding_percent']) . "," .
				db_escape($finishing_cost['sanding_cost']) . "," .
				db_escape($finishing_cost['polish_labour']) . "," .
				db_escape($finishing_cost['polish_percent']) . "," .
				db_escape($finishing_cost['polish_cost']) . "," .
				db_escape($finishing_cost['packaging_labour']) . "," .
				db_escape($finishing_cost['packaging_percent']) . "," .
				db_escape($finishing_cost['packaging_cost']) . "," .
				db_escape($finishing_cost['actual_sending_cost']) . "," .
				db_escape($finishing_cost['actual_polish_cost']) . "," .
				db_escape($finishing_cost['actual_packaging_cost']) . "," .
				db_escape($finishing_cost['lumsum_cost_percent']) . "," .
				db_escape($finishing_cost['lumsum_cost_value']) . "," .
				db_escape($finishing_cost['forwarding_percent']) . "," .
				db_escape($finishing_cost['forwarding_cost']) . "," .
				db_escape($finishing_cost['total_special_cost']) . "," .
				db_escape($finishing_cost['other_cost']) . "," .
				db_escape($finishing_cost['total_finishing_cost']) . "," .
				db_escape($finishing_cost['total_amount']) . ",'1','". date2sql($_POST['date'])."')";
		db_query($sql, "finishing cost is not inserted.");
		
		$finishing_cost_id = db_insert_id();
		
		foreach ($finishing_session->line_items as $line_no => $finish_line)
		 {
			$sql = "INSERT INTO ".TB_PREF."finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
			$sql .= db_escape($finishing_cost_id). ", " . 
					db_escape($finish_line->special_cost_ref). "," .
					db_escape($finish_line->special_rate). "," .
					db_escape($finish_line->special_amount). "," .
					db_escape($finish_line->total). ")";
			db_query($sql, "finishing special cost Costing is not inserted");
		 }
	return true; 
}

function get_finishing_cost($finish_code_id) {
	
	$sql = "SELECT * FROM ".TB_PREF."finishing_cost WHERE finish_code_id =".$finish_code_id." && version = '1'";
	$result = db_query($sql, "Could not get finishing cost");
	while($row = db_fetch($result))
	{ 
		$_POST['sanding_labour'] = $row['sanding_labour'];
		$_POST['sanding_percent'] = $row['sanding_percent'];
		$_POST['sanding_cost'] = $row['sanding_cost'];
		$_POST['polish_labour'] = $row['polish_labour'];
		$_POST['polish_percent'] = $row['polish_percent'];
		$_POST['polish_cost'] = $row['polish_cost'];
		$_POST['packaging_labour'] = $row['packaging_labour'];
		$_POST['packaging_percent'] = $row['packaging_percent'];
		$_POST['packaging_cost'] = $row['packaging_cost'];
		$_POST['actual_sending_cost'] = $row['actual_sending_cost'];
		$_POST['actual_polish_cost'] = $row['actual_polish_cost'];
		$_POST['actual_packaging_cost'] = $row['actual_packaging_cost'];		
		$_POST['lumsum_cost_percent'] = $row['lumsum_cost_percent'];
		$_POST['lumsum_cost_value'] = $row['lumsum_cost_value'];
		$_POST['forwarding_percent'] = $row['forwarding_percent'];
		$_POST['forwarding_cost'] = $row['forwarding_cost'];
		$_POST['other_cost'] = $row['other_cost'];
		$_POST['total_special_cost'] = $row['total_special_cost'];
		$_POST['total_finishing_cost'] = $row['total_finishing_cost'];
		$_POST['finish_record_id'] = $row['id'];
		$finishing_cost_id = $row['id'];
	}

	//------------------------------- Special costing data ----------------------

	$_SESSION['finish_cost'] = new finish;

	$sql1 = "SELECT * from ".TB_PREF."finishing_special_cost where finishing_cost_id =".$finishing_cost_id;
	$result1 = db_query($sql1, "Could not get finishing cost");
	
	while($row1 = db_fetch($result1))
	{
		$_SESSION['finish_cost']->add_to_finish_part(count($_SESSION['finish_cost']->line_items),$row1['special_cost_ref'],$row1['special_rate'],$row1['special_amount'], $row1['total']);
	}
}

function update_finishing_cost($finish_cost, $finish_cost_session)
{
	$sql = "UPDATE ".TB_PREF."finishing_cost SET
			sanding_labour=".db_escape($finish_cost['sanding_labour']).",
			sanding_percent=".db_escape($finish_cost['sanding_percent']).",
			sanding_cost=".db_escape($finish_cost['sanding_cost']).",
			polish_labour=".db_escape($finish_cost['polish_labour']).",
			polish_percent=".db_escape($finish_cost['polish_percent']).",
			polish_cost=".db_escape($finish_cost['polish_cost']).",
			packaging_labour=".db_escape($finish_cost['packaging_labour']).",
			packaging_percent=".db_escape($finish_cost['packaging_percent']).",
			packaging_cost=".db_escape($finish_cost['packaging_cost']).",
			actual_sending_cost=".db_escape($finish_cost['actual_sending_cost']).",
			actual_polish_cost=".db_escape($finish_cost['actual_polish_cost']).",
			actual_packaging_cost=".db_escape($finish_cost['actual_packaging_cost']).",
			lumsum_cost_percent=".db_escape($finish_cost['lumsum_cost_percent']).",
			lumsum_cost_value=".db_escape($finish_cost['lumsum_cost_value']).",
			forwarding_cost=".db_escape($finish_cost['forwarding_cost']).",
			other_cost=".db_escape($finish_cost['other_cost']).",
			total_special_cost=".db_escape($finish_cost['total_special_cost']).",
			total_special_cost=".db_escape($finish_cost['total_special_cost']).",
			total_finishing_cost=".db_escape($finish_cost['total_finishing_cost']).",
			total_amount=".db_escape($finish_cost['total_amount']);

	$sql .= " WHERE id=".db_escape($finish_cost['finish_record_id']);
	db_query($sql, "Attachment could not be updated");

	//------------------------------ update wood data --------------------------------------------

	$sql = "DELETE FROM ".TB_PREF."finishing_special_cost WHERE finishing_cost_id = ".db_escape($finish_cost['finish_record_id']);
	db_query($sql, "Could not delete attachment");

	foreach ($finish_cost_session->line_items as $line_no => $finish_line)
	{
		$sql = "INSERT INTO ".TB_PREF."finishing_special_cost(finishing_cost_id, special_cost_ref, special_rate, special_amount, total) VALUES (";
			$sql .= db_escape($finish_cost['finish_record_id']). ", " . 
					db_escape($finish_line->special_cost_ref). "," .
					db_escape($finish_line->special_rate). "," .
					db_escape($finish_line->special_amount). "," .
					db_escape($finish_line->total). ")";
			db_query($sql, "finishing special cost Costing is not inserted");
	}
}

function update_final_cost_on_change_finishing_cost($finish_cost)
{
	$sql1 = "SELECT * FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_cost['finish_code_id']);
	$result1 = db_query($sql1, "Could not get final costing");
	$final_cost_data = db_fetch($result1);

	$updated_final = array();
	$updated_final['total_mfrg'] = $_POST['total_mfrg_cost'];
	$updated_final['total_finishing_cost'] = $finish_cost['total_finishing_cost'];
	$updated_final['total_cost'] = $_POST['total_mfrg_cost'] + $finish_cost['total_finishing_cost'] ;

	$updated_final['final_profit'] = ($updated_final['total_cost'] * $final_cost_data['final_profit_percent'])/100;
	$updated_final['final_profit'] = number_format((float)$updated_final['final_profit'], 2, '.', '');

	$updated_final['final_total_cost'] = $updated_final['final_profit'] + $updated_final['total_cost'];

	$sql = "UPDATE ".TB_PREF."final_costing SET
			total_mfrg_cost=".db_escape($updated_final['total_mfrg']).",
			total_finishing_cost=".db_escape($updated_final['total_finishing_cost']).",
			total_cost=".db_escape($updated_final['total_cost']).",
			final_profit=".db_escape($updated_final['final_profit']).",
			final_total_cost=".db_escape($updated_final['final_total_cost']);

	$sql .= " WHERE finish_code_id=".db_escape($finish_cost['finish_code_id']);
	db_query($sql, "Data could not be updated");
}


//----------------------------------- Final Step Operation --------------------------------

function insert_final_cost()
{
	$currency_price = array();
	$curr = explode(",",$_POST['currencies']);
	
	for($i=0; $i<$_POST['total_currency']; $i++)
	{
		$currency_price[][$curr[$i]] = $_POST['currency'.$curr[$i]];
	}

	$currency = json_encode($currency_price);

	$sql = "INSERT INTO ".TB_PREF."final_costing(finish_code_id, ref_id, total_mfrg_cost, total_finishing_percent, total_finishing_cost, total_cost, final_profit_percent, final_profit, final_total_cost, currency_price, version, date) VALUES(";
	$sql .= db_escape($_POST['final_finish_code_id']) . "," .
			db_escape($_POST['ref_id']) . "," .
			db_escape($_POST['total_mfrg_cost']) . "," .
			db_escape($_POST['total_percent']) . "," .
			db_escape($_POST['total_finishing_cost']) . "," .
			db_escape($_POST['total_cost']) . "," .
			db_escape($_POST['final_profit_percent']) . "," .
			db_escape($_POST['final_profit']) . "," .
			db_escape($_POST['final_total_cost']) . "," .
			db_escape($currency).",'1','". date2sql($_POST['date'])."')";
	db_query($sql, "final cost is not inserted.");
	
	return true; 
}

function update_final_costing($final_cost)
{
	$sql = "UPDATE ".TB_PREF."final_costing SET
			ref_id=".db_escape($final_cost['ref_id']).",
			finish_code_id=".db_escape($final_cost['final_finish_code_id']).",
			total_mfrg_cost=".db_escape($final_cost['total_mfrg_cost']).",
			total_finishing_cost=".db_escape($final_cost['total_finishing_cost']).",
			total_cost=".db_escape($final_cost['total_cost']).",
			final_profit_percent=".db_escape($final_cost['final_profit_percent']).",
			final_profit=".db_escape($final_cost['final_profit']).",
			total_finishing_percent=".db_escape($final_cost['total_percent']).",
			final_total_cost=".db_escape($final_cost['final_total_cost']).",
			currency_price=".db_escape($final_cost['currency']).",
			date=".date2sql($final_cost['date']);

	$sql .= " WHERE finish_code_id=".db_escape($final_cost['final_finish_code_id']);
	db_query($sql, "Data could not be updated");
}

//-----------------------------------------------------------------------------------------

function get_finish_product_list_from_finishing($category_id, $range_id)
{
	$sql = "SELECT f.finish_pro_id, f.finish_comp_code, f.finish_product_name, f.asb_weight, f.asb_height, 
			f.asb_density FROM ".TB_PREF."finish_product f INNER JOIN ".TB_PREF."finishing_cost m on 
			m.finish_code_id = f.finish_pro_id 
			WHERE f.category_id=".db_escape($category_id)." AND f.range_id = ".db_escape($range_id)." && m.version = '1'";
	$result = db_query($sql, "could not get product list");
	return $result;
}

function check_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT final_total_cost FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check Final Cost");
	if (db_num_rows($result) > 0)
	{
		$disabled = 'style="color:green; font-size:14px;"';
	}
	else
	{
		$disabled = "";
	}
	return $disabled;
}

function is_exist_final_cost($finish_pro_id)
{
	$sql = "SELECT final_total_cost FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_pro_id);
	$result = db_query($sql, "could not check finishing cost");
	if (db_num_rows($result) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function get_currency_name($id)
{
	$sql = "SELECT currency_name from ".TB_PREF."currency_master where id=".$id;
	$result = db_query($sql);
	$row = db_fetch($result);
	return $row['currency_name'];
}

?>