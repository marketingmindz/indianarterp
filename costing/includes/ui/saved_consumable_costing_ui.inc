<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



function display_costing_reference_list()
{
	$sql = "select * from ".TB_PREF."cons_costing_reference order by date DESC";
	$result = db_query($sql, "could not get costing_reference details.");
	if(db_num_rows($result))
	{
		start_table(TABLESTYLE, "width=70%");
		$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" "),_(" "));
		table_header($th);
		$i = 0;
		while($row = db_fetch($result))
		{
			$ref_id = $row['ref_id'];
			$reference_no = $row['reference_no'];
			$remark = $row['remark'];
			$date = sql2date($row['date']);
			$complete = $row['complete'];
			start_row();
				label_cell($date,null,"date");
				label_cell($reference_no,"width='120' height='40'","reference_no");
				label_cell($remark,null,"remark");	
			
				echo '<td colspan="2" style="min-width:240px"><center>';

				echo '<a class="button edit_costing" reference_no="'.$reference_no.'" remark="'.$remark.'" url="edit_consumable_costing.php?reference_id='.$ref_id.'" href="#freeze_costing_popup" >Edit</a> ';      
				echo '<a class="button openCostingButton" href="consumable_costing.php?reference_id='.$ref_id.'" >Open</a> <a class="button cancel_costing" reference_no="'.$reference_no.'" remark="'.$remark.'" url="cancel_consumable_costing.php?reference_id='.$ref_id.'" href="#cancel_costing_popup" >Cancel</a></center></td>';      


				$i++;
			end_row();
		}end_table();
		
			
	}
	else
	{
		display_notification("Costing is not entered.");
	}
}



?>
