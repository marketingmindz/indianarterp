<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


//---------------------------------------------------------------------------------------------------

function display_purchase_order_header(&$order)
{
	global 	$Ajax;

  $Ajax->activate('cons_table');
div_start('cons_table');
	start_outer_table(TABLESTYLE2, 'width=80%');

	table_section(1);
    if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_request_details($purchase_request_id);
	}
	text_row(_("PR No:"), 'pr_no', null, 30, 30, '','','','id="pr_no" readonly="true"');
	//date_row(_("Date:"), 'po_date', null, 30, 30, '','','','id="po_date" readonly="true"');
	date_row(_("Date") . ":", 'po_date');


	table_section(2);
	hidden("location_id", $_POST['location_id']);
	text_row(_("Location Name:"), 'location_id_0', null, 30, 30, '','','','id="location_id" readonly="true"');
	
	hidden("work_center_id", $_POST['work_center_id']);
	text_row(_("Work Center Name:"), 'work_center_id_0', null, 30, 30, '','','','id="work_center_id" readonly="true"');


	end_outer_table(); // outer table
}

//---------------------------------------------------------------------------------------------------
function approved_qty($i){
	echo "<td><input type='text' value='' name='app_qty_".$i."' id='appqty_".$i."' class='appqtyclass' ></td>";
}
function select_supplier_list($consumable_id, $consumable_category, $i){
	$result = get_suppliers($consumable_id, $consumable_category);
	$supplier = "";
	echo "<td><select class='supplier' style = 'font-size:15px; font-weight: bolder;' name='supp_id_".$i."'><option value='-1'>Select Supplier</option>";
	while($myrow = db_fetch($result)){
		$supplier_id = $myrow['supplier_id'];
		echo $supplier_id;
		$supplier = "". $myrow['supp_name']." - Price:-".$myrow['purchase_price']." - LeadTime:-".$myrow['lead_time'];
	    echo "<option value='".$supplier_id."'><h1 style='margin-right:20px;'>".$supplier."</h1></option>";
	}
	echo "</select></td>";

}


function display_supp_details_header()
{
	if(isset($_GET['RequestID']))
	{
		$purchase_request_id = $_GET['RequestID'];
		$result = get_item_details($purchase_request_id);
	}

	start_table(TABLESTYLE2, 'width=60%');
	$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Requested Qty"),  _("Supplier - Price -  Time"), _("Approved Qty"));
	table_header($th);
	$k = 0;
	$i=0;
	while($myrow = db_fetch($result)){
		$consumable_id = $myrow["consumable_id"];
		$consumable_category = $myrow["consumable_category"];
		$_POST['consumable_id_'.$i] = $_POST['cons_id'] = $myrow["master_name"];
		$_POST['consumable_category_'.$i] = $_POST['consumable_cat'] = $myrow["consumable_name"];
		$_POST['unit_'.$i] = $_POST['unit'] = $myrow["unit"];
		$_POST['requested_quantity_'.$i] = $_POST['requested_qty'] = $myrow["quantity"];
		
		alt_table_row_color($k);
		
		    hidden("consumable_id_".$i, $consumable_id);
			text_cells_ex(null, 'cons_id', 30, 30, null,'','','', '','id="consumable_id", readonly="true"');
			
			hidden("consumable_category_".$i, $consumable_category);
			text_cells_ex(null, 'consumable_cat', 30, 30,null,'','','', '','id="consumable_category", readonly="true"');
			
			hidden("unit_".$i, $_POST['unit']);
			text_cells_ex(null, 'unit_'.$i, 10, 10,null,'','','', '','id="cons_unit" readonly="true"');
			
			hidden("requested_quantity_".$i, $_POST['requested_quantity_'.$i]);
			text_cells_ex(null, 'requested_qty', 10, 10,null,'','','', 'id=reqqty_'.$i, 'readonly="true"');
			select_supplier_list($consumable_id, $consumable_category, $i);
			approved_qty($i);
		end_row();
		
		hidden("count", $i);
		$i++;
	}
	
	end_table(); // outer table
}


function display_apr_summary(&$pr, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "width=60%");
    start_row();
    label_cells(_("PR No"), $pr->pr_no, "class='tableheader2'");
	label_cells(_("Location"), $pr->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $pr->work_center_id, "class='tableheader2'");
	label_cells(_("Date"), $pr->pr_date, "class='tableheader2'");
	end_row();

    end_table(1);
}

?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>