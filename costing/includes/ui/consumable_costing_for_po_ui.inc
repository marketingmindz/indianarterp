<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// costing module - multiple category list
function multiple_categories_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	$sql = "SELECT master_id, master_name FROM ".TB_PREF."master_creation order by master_name ASC";
	$result = db_query($sql);
	echo '<div class="multiselect">
           <div class="selectBox" onclick="showCheckboxes()">';
	echo '<select autocomplete="off" class="combo" title="Select Category" id="consumable_category" >
	  <option value="-1" >- select category - </option>';
	while($row = db_fetch($result))
	{
		$master_id = $row['master_id'];
		$selected = "";
		
		echo '<option value="'.$master_id.'">'.$row['master_name'].'</option>';	
	}
	echo "</select>";
	if(isset($_POST['select_all']))
	{
		$checked = 'checked="checked"';
	}
	echo '<div class="overSelect"></div>
        </div>
        <div id="checkboxes"> <label><input type="checkbox" id="select_all" name="select_all" '.$checked.' /> Select all</label>';
	$result = db_query($sql);
	while($row = db_fetch($result))
	{
		$master_id = $row['master_id'];
		$checked = "";	
		if(in_array($master_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		
		echo '<label><input type="hidden" id="'.$master_id.'" value="'.$row['master_name'].'" >
		<input type="checkbox" class="check" value="'.$master_id.'" '.$checked.' name="categories['.$master_id.']" />'.$row['master_name'].'</label>';
	}	

    echo ' </div>
    </div>';
}
function multiple_category_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_categories_list($name, $selected_id, $spec_opt, $submit_on_change);
	echo "</td>\n";
}

function multiple_category_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_category_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change);
	echo "</tr>\n";
}



function display_header(){
	div_start('product', "style=min-height:100px;");
	echo '<p id="pro_cat" style="max-width:700px; margin:auto;">'.$_POST['pro_categories'].'</p>
			<input type="hidden" name="pro_categories" id="pro_categories" value="'.$_POST['pro_categories'].'" >';
	start_table(TABLESTYLE_NOBORDER, "width=500px");
	global $Ajax;
	$Ajax->activate('proceed');
	$Ajax->activate('getConsumables');
		
		multiple_category_list_row(_("Consumable Category:"), 'category_id', null, _('----Select Category---'));
		
		echo "<td colspan='2'>";
		submit_center('proceed', _("Submit"), true, '', 'default');
		
		//echo "<tr><td></td><td><span id='proceed'>Proceed</span></td></tr>";
		hidden("date",Today());
	end_table();
	
	
	div_end();
		
}



function display_consumable_for_po($category)
{
	echo "<div id=''table-head>";
	start_table(TABLESTYLE_NOBORDER);
?>
		<tr><td class='tableheader' width="130">Consumable Category</td><td  class='tableheader' width="130">Consumable Name</td><td  class='tableheader' width="100">Make</td><td  class='tableheader' width="200">Supllier Prices</td><td  class='tableheader' width="70">Set Default</td><td  class='tableheader' width="175">Min. Price</td><td  class='tableheader' width="175">Max. Price</td><td width='30'></td></tr>
<?php
	end_table();
	echo "</div>";


	echo "<div id='consumables'>";
		start_table(TABLESTYLE);
		foreach ($category as $category_id) {
			$cons = get_cons_type_name($category_id);

			$sql = "SELECT consumable_id,consumable_name, company_name FROM ".TB_PREF."consumable_master c left join ".TB_PREF."item_company i on i.company_id = c.company_id  WHERE master_id=".db_escape($category_id);
			$result = db_query($sql, "Could not get Consumables details.");
			while($row = db_fetch($result))
			{
				$consumable_id = $row['consumable_id'];
				$cost = 0;

				
				$checked = "";
				$default_supplier = "";
				$sql_check = "select default_supplier from ".TB_PREF."default_consumable_supplier where consumable_cat = ".db_escape($category_id)." && consumable_name = ".db_escape($consumable_id);
				$check_result = db_query($sql_check, "Could not check Default supplier.");
				if(db_num_rows($check_result) > 0)
				{
					$row2 = db_fetch($check_result);
					$default_supplier = $row2['default_supplier'];
					if($default_supplier != '0' && $default_supplier != '-1')
					{
						$checked = "checked='checked'";
					}					
				}
			

				echo "<tr class='cons_row' id='".$consumable_id."'><td  width='130'><input type='hidden' name='category_id' value='".$category_id."' >".$cons['master_name']."</td>";

				echo "<td width='130'><input type='hidden' name='consumable_id' value='".$consumable_id."' >".$row['consumable_name']."</td>";

				$brand = $row['company_name'] == "" ? "No Brand" : $row['company_name'];
				echo "<td width='100'>".$brand."</td>";

				echo "<td class='supp_price' width='200'>";
				$sql1 = "select s.supplier_id,purchase_price, supp_name, lead_time from ".TB_PREF."supp_product sp left join ".TB_PREF."suppliers s on s.supplier_id = sp.supplier_id where catagory = ".db_escape($category_id)." && sub_category = ".db_escape($consumable_id)." ORDER BY convert(`purchase_price`, decimal) DESC";
				$result1 = db_query($sql1, "Could not get purchase price");

				$disable = 0;
				$available = "yes";
				$purchase_price = array();
				if(db_num_rows($result1) > 0)
				{
					echo "<select name='supp_price' id='supp_price'><option value='-1'>- select supplier -</option>";
					while($row1 = db_fetch($result1))
					{
						$selected = "";
						if($default_supplier == $row1['supplier_id'])
						{
							$selected = "selected='selected'";
						}
						echo "<option ".$selected." value='".$row1['supplier_id']."'>".$row1['supp_name']." - Rs. ".$row1['purchase_price']." - ".$row1['lead_time']."</option>";
						$purchase_price[] = $row1;
					}
					echo "</select>";


					$max_purchase_price = 0;
					foreach($purchase_price as $k=>$val)
					{
						if($val['purchase_price'] > $max_purchase_price)
						{
							$max_purchase_price = $val['purchase_price'];
							$key = $k;
						}
					}
					$max_purchase_price = $purchase_price[$key]['supp_name']." - Rs. ".$purchase_price[$key]['purchase_price']." - ".$purchase_price[$key]['lead_time'];


					$min_purchase_price = $purchase_price['0']['supp_name'];
					foreach($purchase_price as $k=>$val)
					{
						if($val['purchase_price'] < $min_purchase_price)
						{
							$min_purchase_price = $val['purchase_price'];
							$key = $k;
						}
					}
					$min_purchase_price = $purchase_price[$key]['supp_name']." - Rs. ".$purchase_price[$key]['purchase_price']." - ".$purchase_price[$key]['lead_time'];
				}
				else
				{
					echo "Not Available";
					$disable = 1;
					$max_purchase_price = $min_purchase_price = "Not Available";
					$available = "no";
				}
				echo "</td>";

				echo "<td width='70'>";
				if($available != "no")
				echo "<input type='checkbox' ".$checked." class='setDefault' name='setDefault' value='1' />Default";
				echo "</td>";
				echo "<td width='175'>".$min_purchase_price."</td>";
				echo "<td width='175'>".$max_purchase_price."</td>";
?>
				<td width="35"><input type="button" class="save" <?php echo $hidden; ?> value="save" name="save"></td>

<?php

			echo "</tr>";
			}	
		}
		end_table();



	echo "</div>";
}

?>
