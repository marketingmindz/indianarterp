<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


function display_finishing_costing_form(&$order, $editable=true)
{
	if(!isset($_POST['sanding_labour']))
		$_POST['sanding_labour'] = 0;
	if(!isset($_POST['sanding_percent']))
		$_POST['sanding_percent'] = 0;
	if(!isset($_POST['sanding_cost']))
		$_POST['sanding_cost'] = 0;

	if(!isset($_POST['polish_labour']))
		$_POST['polish_labour'] = 0;
	if(!isset($_POST['polish_percent']))
		$_POST['polish_percent'] = 0;
	if(!isset($_POST['polish_cost']))
		$_POST['polish_cost'] = 0;

	if(!isset($_POST['packaging_labour']))
		$_POST['packaging_labour'] = 0;
	if(!isset($_POST['packaging_percent']))
		$_POST['packaging_percent'] = 0;
	if(!isset($_POST['packaging_cost']))
		$_POST['packaging_cost'] = 0;

	if(!isset($_POST['actual_sending_cost']))
		$_POST['actual_sending_cost'] = 0;
	if(!isset($_POST['actual_polish_cost']))
		$_POST['actual_polish_cost'] = 0;
	if(!isset($_POST['actual_packaging_cost']))
		$_POST['actual_packaging_cost'] = 0;

	if(!isset($_POST['lumsum_cost_percent']))
		$_POST['lumsum_cost_percent'] = 0;
	if(!isset($_POST['lumsum_cost_value']))
		$_POST['lumsum_cost_value'] = 0;

	if(!isset($_POST['forwarding_percent']))
		$_POST['forwarding_percent'] = 0;
	if(!isset($_POST['forwarding_cost']))
		$_POST['forwarding_cost'] = 0;

	if(!isset($_POST['other_cost']))
		$_POST['other_cost'] = 0;
	if(!isset($_POST['total_special_cost']))
		$_POST['total_special_cost'] = 0;
	if(!isset($_POST['total_finishing_cost']))
		$total_special_cost = 0;
	
	div_start('finish_table');
	
	//---------------------------------------- Finish Costing ---------------------------

	display_heading("Finish Costing");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");
		$th = array(_(" "),_(" "),_(" "),_(" "),_(" "),_(" "));
		//table_header($th);
		$_POST['total_mfrg_cost'] = get_total_mfrg_cost($_POST['finish_code_id']);
		echo "<input type='hidden' name='finish_record_id' id='finish_record_id' value='".$_POST['finish_record_id']."' />";
	
	start_row();
	    label_cell("Total MFRG Cost : ","colspan=3 align=right");
     	label_cell($_POST['total_mfrg_cost'],"colspan=3",null,"total_mfrg_label");	
		echo '<input type="hidden" name="total_mfrg_cost" id="total_mfrg_cost" value="'.$_POST['total_mfrg_cost'].'" />';	
	end_row();

	start_row();
	    label_cell("Sanding Labour");
     	text_cells(null, 'sanding_labour', $_POST['sanding_labour'], 10, 10, null, null, null,"id=sanding_labour");

     	label_cell("Sanding %");
     	text_cells(null, 'sanding_percent', $_POST['sanding_percent'], 10, 10, null, null, null,"id=sanding_percent");

     	label_cell("Sanding Cost");
     	text_cells(null, 'sanding_cost', $_POST['sanding_cost'], 10, 10, null, null, null,"id=sanding_cost");	
	end_row();

	start_row();
	    label_cell("Polish Labour");
     	text_cells(null, 'polish_labour', $_POST['polish_labour'], 10, 10, null, null, null,"id=polish_labour");

     	label_cell("Polish %");
     	text_cells(null, 'polish_percent', $_POST['polish_percent'], 10, 10, null, null, null,"id=polish_percent");

     	label_cell("Polish Cost");
     	text_cells(null, 'polish_cost', $_POST['polish_cost'], 10, 10, null, null, null,"id=polish_cost");	
	end_row();

	start_row();
	    label_cell("Packaging Labour");
     	text_cells(null, 'packaging_labour', $_POST['packaging_labour'], 10, 10, null, null, null,"id=packaging_labour");

     	label_cell("Packaging %");
     	text_cells(null, 'packaging_percent', $_POST['packaging_percent'], 10, 10, null, null, null,"id=packaging_percent");

     	label_cell("Packaging Cost");
     	text_cells(null, 'packaging_cost', $_POST['packaging_cost'], 10, 10, null, null, null,"id=packaging_cost");	
	end_row();

	$total1 = $_POST['sanding_cost'] + $_POST['polish_cost'] + $_POST['packaging_cost'];

	start_row();
		label_cell("Total : ","colspan=5 align=right");
		label_cell($total1, null,"total1");
	end_row();

	end_table();

	//---------------------------------------- Actual Costing ---------------------------

	display_heading("Actual Costing");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

	start_row();
	    label_cell("Sending : ","colspan=3 align=right");
     	text_cells(null, 'actual_sending_cost', $_POST['actual_sending_cost'], 10, 10, null, null, null,"id=actual_sending_cost");
	end_row();

	start_row();
	    label_cell("Polish : ","colspan=3 align=right");
     	text_cells(null, 'actual_polish_cost', $_POST['actual_polish_cost'], 10, 10, null, null, null,"id=actual_polish_cost");
	end_row();

	start_row();
	    label_cell("Packaging : ","colspan=3 align=right");
     	text_cells(null, 'actual_packaging_cost', $_POST['actual_packaging_cost'], 10, 10, null, null, null,"id=actual_packaging_cost");
	end_row();

	$total2 = $_POST['actual_sending_cost'] + $_POST['actual_polish_cost'] + $_POST['actual_packaging_cost'];

	start_row();
	    label_cell("Total : ","colspan=3 align=right");
     	label_cell($total2, null,"total2");
	end_row();

	end_table();
    
    //---------------------------------------- Lumsum Costing ---------------------------
	
	display_heading("Cost By Lumsum");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

	start_row();
	    label_cell("% Cost : ","colspan=3 align=right");
     	text_cells(null, 'lumsum_cost_percent', $_POST['lumsum_cost_percent'], 10, 10, null, null, null,"id=lumsum_cost_percent");

     	label_cell("Lumsum Cost : ","colspan=3 align=right");
     	text_cells(null, 'lumsum_cost_value', $_POST['lumsum_cost_value'], 10, 10, null, null, null,"id=lumsum_cost_value");
	end_row();

	end_table();

	//---------------------------------------- Total Average Process Cost ---------------------------

		display_heading("Total Average Process Cost");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		$total_average = ($total1 + $total2 + $_POST['lumsum_cost_value'])/3;
		$total_average = number_format((float)$total_average, 2, '.', '');

		start_row();
			label_cell("Total : ","colspan=5 align=right");
			label_cell($total_average, null,"total_average");
		end_row();

		end_table();

	//---------------------------------------- Other Than Process Cost ---------------------------

	display_heading("Other Than Process Cost");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

	start_row();
	    label_cell("Forwarding % : ","colspan=3 align=right");
     	text_cells(null, 'forwarding_percent', $_POST['forwarding_percent'], 10, 10, null, null, null,"id=forwarding_percent");

     	label_cell("Forwarding Cost : ","colspan=3 align=right");
     	text_cells(null, 'forwarding_cost', $_POST['forwarding_cost'], 10, 10, null, null, null,"id=forwarding_cost");
	end_row();

	end_table();

	//---------------------------------------- Special Costing ---------------------------
	
	display_heading("Special Costing");

	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array( _("Special Cost Reference"), _("Special Cost Rate"), _("Special cost Amount"),_("Total"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);

	global $total_special_cost;
	$total_special_cost = 0;
	$id = find_submit('Edit');
	$k = 0; 

	foreach ($order->line_items as $line_con => $con_line)
   	{
			
		$total_special_cost += $con_line->total;
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
    		label_cell($con_line->special_cost_ref);
    		label_cell($con_line->special_rate);
			label_cell($con_line->special_amount);
			label_cell($con_line->total);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			finishing_controls($order, $k, $line_con);
		}
		$total += $line_total;
    }

	if ($id==-1 && $editable)
		finishing_controls($order, $k);
	start_row();
		label_cell("Total Special Cost : ","colspan=3 align=right");
		label_cell($total_special_cost,null,"total_special_cost_label");	
		echo '<input type="hidden" name="total_special_cost" id="total_special_cost" value="'.$total_special_cost.'" />';		
	end_row();

	start_row();
	    label_cell("Other Costs : ","colspan=3 align=right");
     	text_cells(null, 'other_cost', $_POST['other_cost'], 10, 10, null, null, null,"id=other_cost");	
	end_row();

	$total_other_than_process_cost = $_POST['forwarding_cost'] + $_POST['other_cost'] + $total_special_cost;

	start_row();
	    label_cell("Total Other Than Process Cost : ","colspan=3 align=right");
     	label_cell($total_other_than_process_cost, null, "total_other_than_process_cost_label");	
		echo '<input type="hidden" name="total_other_than_process_cost" id="total_other_than_process_cost" value="'.$total_other_than_process_cost.'" />';		
	end_row();
	
	$total_finishing_cost = $total_average + $total_other_than_process_cost;
	
	start_row();
	    label_cell("Total Finishing Cost : ","colspan=3 align=right");
     	label_cell($total_finishing_cost,null,"total_finishing_cost_label");	
		echo '<input type="hidden" name="total_finishing_cost" id="total_finishing_cost" value="'.$total_finishing_cost.'" />';		
	end_row();
	
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
	
}

function finishing_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	$Ajax->activate('finish_table');
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		
		$_POST['special_cost_ref'] = $order->line_items[$id]->special_cost_ref;
		$_POST['special_rate'] = $order->line_items[$id]->special_rate;
		$_POST['special_amount'] = $order->line_items[$id]->special_amount;
		$_POST['total'] = $order->line_items[$id]->total;
		

	    $Ajax->activate('finish_table');
	}
	 
	
	text_cells_ex(null, 'special_cost_ref', null, null);
	
	text_cells_ex(null, 'special_rate', 10, 10);
	text_cells_ex(null, 'special_amount', 10, 10);
	text_cells_ex(null, 'total', 10, 10);
	if ($id != -1)
	{
		button_cell('FinishUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('FinishCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		//set_focus('amount');
	} 
	else 
		submit_cells('FinishAddItem', _("Save"), "colspan=2",
			_('Add new line to Specail cost Section'), true);
	end_row();
}


function unset_finishing_cost_variables()
{
	unset($_POST['sanding_labour']);
	unset($_POST['sanding_percent']);
	unset($_POST['sanding_cost']);
	unset($_POST['polish_labour']);
	unset($_POST['polish_percent']);
	unset($_POST['polish_cost']);
	unset($_POST['packaging_labour']);
	unset($_POST['packaging_percent']);
	unset($_POST['packaging_cost']);
	unset($_POST['actual_sending_cost']);
	unset($_POST['actual_polish_cost']);
	unset($_POST['actual_packaging_cost']);
	unset($_POST['lumsum_cost_percent']);
	unset($_POST['lumsum_cost_value']);
	unset($_POST['forwarding_percent']);
	unset($_POST['forwarding_cost']);
	unset($_POST['other_cost']);
	unset($_POST['total_finishing_cost']);
	unset($_POST['total_special_cost']);
}


function display_finishing_cost($finish_code_id)
{
	$total_mfrg_cost = get_total_mfrg_cost($finish_code_id);

	div_start('finish_table');

	//---------------------------------------- Finish Costing ---------------------------

		display_heading("Finish Costing");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		/*$th = array(_(" "),_(" "),_(" "),_(" "),_(" "),_(" "));
		table_header($th);*/
			
		start_row();
		    label_cell("Total MFRG Cost : ","colspan=3 align=right");
	     	label_cell($total_mfrg_cost, "colspan=3", null, "total_mfrg_label");		
		end_row();
	
	$sql = "SELECT * FROM ".TB_PREF."finishing_cost WHERE finish_code_id =".$finish_code_id." && version = '1'";
	$result = db_query($sql, "Could not get finishing cost");
	while($row = db_fetch($result))
	{
		start_row();
			label_cell("Sanding Labour");
			label_cell($row['sanding_labour'],null,"sanding_labour");
			label_cell("Sanding %");
			label_cell($row['sanding_percent'],null,"sanding_percent");
			label_cell("Sanding Cost");
			label_cell($row['sanding_cost'],null,"sanding_cost");
		end_row();

		start_row();
			label_cell("Polish Labour");
			label_cell($row['polish_labour'],null,"polish_labour");
			label_cell("Polish %");
			label_cell($row['polish_percent'],null,"polish_percent");
			label_cell("Polish Cost");
			label_cell($row['polish_cost'],null,"polish_cost");
		end_row();

		start_row();
			label_cell("Packaging Labour");
			label_cell($row['packaging_labour'],null,"packaging_labour");
			label_cell("Packaging %");
			label_cell($row['packaging_percent'],null,"packaging_percent");
			label_cell("Packaging Cost");
			label_cell($row['packaging_cost'],null,"packaging_cost");
		end_row();

		$total1 = $row['sanding_cost'] + $row['polish_cost'] + $row['packaging_cost'];

		start_row();
			label_cell("Total : ","colspan=5 align=right style='font-weight:bold;' ");
			label_cell($total1, "style='font-weight:bold;'", null,"total1");
		end_row();

		end_table();

		//---------------------------------------- Actual Costing ---------------------------

		display_heading("Actual Costing");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		$th = array(_(" "),_(" "),_(" "),_(" "),_(" "),_(" "));
		table_header($th);

		start_row();
		    label_cell("Sending : ", "colspan=1 align=right");
	     	label_cell($row['actual_sending_cost'], "colspan=5", null,"actual_sending_cost");
		end_row();

		start_row();
		    label_cell("Polish : ","colspan=1 align=right");
	     	label_cell($row['actual_polish_cost'], "colspan=5", null,"actual_polish_cost");
		end_row();

		start_row();
		    label_cell("Packaging : ","colspan=1 align=right");
	     	label_cell($row['actual_packaging_cost'], "colspan=5", null,"actual_packaging_cost");
		end_row();

		$total2 = $row['actual_sending_cost'] + $row['actual_polish_cost'] + $row['actual_packaging_cost'];

		start_row();
			label_cell("Total : ","colspan=5 align=right style='font-weight:bold;'");
			label_cell($total2, "style='font-weight:bold;'", null,"total2");
		end_row();

		end_table();

		//---------------------------------------- Lumsum Costing ---------------------------

		display_heading("Cost By Lumsum");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		/*$th = array(_(" "),_(" "),_(" "),_(" "),_(" "),_(" "));
		table_header($th);*/

		start_row();
		    label_cell("% Cost : ","colspan=1 align=right");
	     	label_cell($row['lumsum_cost_percent'], "colspan=3", null,"lumsum_cost_percent");

	     	label_cell("Lumsum Cost : ","colspan=1 align=right");
	     	label_cell($row['lumsum_cost_value'], "colspan=2", null,"lumsum_cost_value");

		end_row();

		end_table();

		//---------------------------------------- Total Average Process Cost ---------------------------

		display_heading("Total Average Process Cost");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		$total_average = ($total1 + $total2 + $row['lumsum_cost_value'])/3;
		$total_average = number_format((float)$total_average, 2, '.', '');

		start_row();
			label_cell("Total : ","colspan=5 align=right");
			label_cell($total_average, null,"total_average");
		end_row();

		end_table();

		//---------------------------------------- Other Than Process Cost ---------------------------

		display_heading("Other Than Process Cost");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		start_row();
		    label_cell("Forwarding % : ","colspan=3 align=right");
	     	label_cell($row['forwarding_percent'], "colspan=3", null,"forwarding_percent");

	     	label_cell("Forwarding Cost : ","colspan=3 align=right");
	     	label_cell($row['forwarding_cost'], "colspan=3", null,"forwarding_cost");
		end_row();

		end_table();

		//---------------------------------- Total Cost ----------------------------------------------

		$other_cost = $row['other_cost'];
		$total_special_cost = $row['total_special_cost'];
		//$total_finishing_cost = $row['total_finishing_cost'];
		$finishing_cost_id = $row['id'];

		$total_other_than_process_cost = $total_special_cost + $other_cost + $row['forwarding_cost'];
		$total_finishing_cost = $total_average + $total_other_than_process_cost;
	}
	
		//---------------------------------- Special Costing -----------------------------------------

		display_heading("Special Costing");
		start_table(TABLESTYLE, "colspan=7 style='width:100%;'");

		$th = array( _("Special Cost Reference"), _("Special Cost Rate"), _("Special cost Amount"),_("Total"));
		if (count($order->line_items)) $th[] = '';
		table_header($th);
		$total_special_amount = 0;
		
		$sql = "SELECT * from ".TB_PREF."finishing_special_cost where finishing_cost_id =".$finishing_cost_id;
		$result = db_query($sql, "Could not get finishing cost");
		while($row = db_fetch($result))
		{
			start_row();
				label_cell($row['special_cost_ref']);
				label_cell($row['special_rate']);
				label_cell($row['special_amount']);
				label_cell($row['total']);
			end_row();
		}

		start_row();
			label_cell("Total Special Cost : ","colspan=3 align=right");
			label_cell($total_special_cost,null,"total_special_cost_label");	
		end_row();

		start_row();
		    label_cell("Other Costs : ","colspan=3 align=right");
	     	label_cell($other_cost, null,"total_special_cost_label");
		end_row();

		start_row();
		    label_cell("Total Other Than Process Cost : ", "colspan=3 align=right");
	     	label_cell($total_other_than_process_cost, null, "total_other_than_process_cost_label");
		end_row();
		
		start_row();
		    label_cell("Total Finishing Cost : ","colspan=3 align=right style='font-weight: bold;' ");
	     	label_cell($total_finishing_cost, "style='font-weight: bold;'" ,null,"total_finishing_cost_label");	
		end_row();
		$colspan = count($th)-2;
		if (count($order->line_items))
			$colspan--;
		end_table();

    div_end();
}
?>
