<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function text_cells_cost($label, $name, $value=null, $size="", $max="", $title=false, 
	$labparams="", $post_label="", $inparams="", $width="")
{
  	global $Ajax;

	default_focus($name);
	if ($label != null)
		label_cell($label, $labparams);
	echo "<td width=".$width.">";

	if ($value === null)
		$value = get_post($name);
	echo "<input $inparams type=\"text\" name=\"$name\" size=\"$size\" maxlength=\"$max\" value=\"$value\""
	    .($title ? " title='$title'" : '')
	    .">";

	if ($post_label != "")
		echo " " . $post_label;

	echo "</td>\n";
	$Ajax->addUpdate($name, $name, $value);
}


function get_product_details($finish_code_id)
{
	$sql = "select *, cat.cat_name, r.range_name, des.design_code from ".TB_PREF."finish_product pro
			LEFT JOIN ".TB_PREF."item_category cat ON cat.category_id = pro.category_id
			LEFT JOIN ".TB_PREF."item_range r ON r.id = pro.range_id
			LEFT JOIN ".TB_PREF."design_code des on des.design_id = pro.design_id
			where finish_pro_id =".db_escape($finish_code_id);
	$result = db_query($sql, "Could not get product details");
	return db_fetch($result);
}


function display_costing_details($ref_id,$version)
{	
	$path_to_root = "../..";
	div_start("display_costing");
	echo "<div id='inner_div'>";
	start_table(TABLESTYLE, "width=1262");

	echo "<tr class='tableheader'><td width='40' >S. No.</td><td width='100' >Photo</td><td width='80' >Design Code</td><td width='110' >Finish Code</td><td  width='100'>Category</td><td width='100' >Range</td><td  width='120' >Product Name</td><td width='80'>Size</td><td width='80'>PKG Size</td><td width='80'>Box CBM</td><td width='60'>Qty 20".'"'."</td><td width='60'>Qty 40".'"'."</td><td width='60'>Qty 40".'"'."HQ</td><td width='100'>Price(USD)</td></tr>";      

	end_table();
	echo "</div>";
	echo "<div id='inner_table'>";
	echo "<div id='inner_div2'>";

	echo '<input type="hidden" id="reference_id" value="'.$ref_id.'" >';
	echo '<input type="hidden" id="version" value="'.$version.'" >';
	start_table(TABLESTYLE, "id='product_table' width='1262'");
	$th = array(_("Product Details"),_("Manufacturing Cost"), _("Finishing Cost"), _("Final Cost"));

	
	$sql = "select * from ".TB_PREF."costing_reference_details where ref_id = ".db_escape($ref_id);
	$res = db_query($sql, "Cannot get costing reference detilas ###range-cat");
	while($cat = db_fetch($res))
	{
		$category_id = $cat['category_id'];
		$range_id = $cat['range_id'];
		$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);
		$result1 = db_query($sql, "could not get finish product list");
		$i= 1;
		while($row1 = db_fetch($result1))
		{
			$finish_code_id = $row1['finish_pro_id'];	
			$sql = "select finish_code_id from ".TB_PREF."locked_final_costing where finish_code_id = ".db_escape($finish_code_id)." && ref_id = ".db_escape($ref_id)."&& version = ".db_escape($version);
			$res1 = db_query($sql, "could not match with final costing");
			if(db_num_rows($res1) > 0)
			{
				$product = get_product_details($finish_code_id);
				//echo "<pre>"; print_r($product);die();
				echo "<tr class='finish_product' id='".$finish_code_id."'>";
				echo '<td class="sno" width="40" >'.$i.'</td>';

				echo '<td width="90" class="photo">';
				$img = explode(",", $product['product_image']);

				foreach ($img as $product_image) {
					echo '<a ><img onclick=display_image($(this).attr("src")); src="'.$path_to_root.'/company/0/finishProductImage/'.$product_image.'" width="40" /> </a>';
				}

				echo '</td>';

				text_cells_cost(null, 'design_code', $product['design_code'], null, null, null, null, null,"class=design_code","80");
				text_cells_cost(null, 'finish_comp_code', $product['finish_comp_code'], null, null, null, null, null,"class=finish_comp_code","110");
				
				echo "<td class='cat_name' width='100'>".$product['cat_name']."</td>";
				if($product['range_id'] == '-1'){
					$range_name = "Non-Collection";
				}
				else{
					$range_name = $product['range_name'];
				}

				text_cells_cost(null, 'range_name', $range_name, null, null, null, null, null,"class=range_name","100");


				$w = 0; $h = 0; $d = 0;
				if($product['width'] != "")
					$w = $product['width'];
				if($product['height'] != "")
					$h = $product['height'];
				if($product['density'] != "")
					$d = $product['density'];				
				echo "<td width=120>".$product['finish_product_name']."</td>";
				text_cells_cost(null, 'size', $w.'*'.$h.'*'.$d, null, null, null, null, null,"class=size","80");


				$pkg_w = 0; $pkg_h = 0; $pkg_d = 0;
				if($product['pkg_w'] != "")
					$pkg_w = $product['pkg_w'];
				if($product['pkg_h'] != "")
					$pkg_h = $product['pkg_h'];
				if($product['pkg_d'] != "")
					$pkg_d = $product['pkg_d'];
				text_cells_cost(null, 'pkg_size', $pkg_w.'*'.$pkg_h.'*'.$pkg_d, null, null, null, null, null,"class=pkg_size","80");

				$pak_cbm = 0;
				if($product['pak_cbm'] != "")
					$pak_cbm = $product['pak_cbm'];
				text_cells_cost(null, 'box_cbm', $pak_cbm, null, null, null, null, null,"class=box_cbm","80");

				$qty_20 = $pak_cbm/26;
				text_cells_cost(null, 'qty_20', $qty_20, null, null, null, null, null,"class=qty_20","60");
				$qty_40 = $pak_cbm/52;
				text_cells_cost(null, 'qty_40', $qty_40, null, null, null, null, null,"class=qty_40","60");
				$qty_40_hq = $pak_cbm/65;
				text_cells_cost(null, 'qty_40_hq', $qty_40_hq, null, null, null, null, null,"class=qty_40_hq","60");


				$sql12 = "select final_total_cost from ".TB_PREF."locked_final_costing where finish_code_id = ".db_escape($finish_code_id)." && version = ".db_escape($version);
				$pr = db_query($sql12, "Could not retrieve finishing_cost ###edit_cost");
				$price = db_fetch($pr);

				$sql13 = "select rate from ".TB_PREF."currency_master where id = '3'";
				$result13 = db_query($sql13, "Could not get currencies.");
				$curr = db_fetch($result13);

				$usd_price = $price['final_total_cost']*$curr['rate'];
				text_cells_cost(null, 'price_usd', "$".$usd_price, null, null, null, null, null,"class=price_usd","100");

							  
				echo "</tr>";
			}
			else
			{
				$product = get_product_details($finish_code_id);
				echo "<tr class='finish_product' id='".$finish_code_id."'>";
				echo '<td class="sno" width="40" >'.$i.'</td>';
				
				echo "<td colspan=4><h1>Costing is not entered for this product</h1></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
			}

			$i++;
		}
	}
	end_table();
    echo "</div></div>";
	div_end();
echo "<div id='popup_image' onclick='$(this).hide();' ><h1>click to Close</h1><img id='full_image' src='' ></div>";
}


?>
<script type="text/javascript">
	
function display_image(src)
{
	$("#popup_image").show();
	$("#full_image").attr('src',src);

}


</script>