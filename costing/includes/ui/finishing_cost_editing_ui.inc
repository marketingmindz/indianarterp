<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


function display_finishing_costing_form(&$order, $editable=true)
{
	if(!isset($_POST['sanding_percent']))
		$_POST['sanding_percent'] = 0;
	if(!isset($_POST['sanding_cost']))
		$_POST['sanding_cost'] = 0;
	if(!isset($_POST['polish_percent']))
		$_POST['polish_percent'] = 0;
	if(!isset($_POST['polish_cost']))
		$_POST['polish_cost'] = 0;
	if(!isset($_POST['packaging_percent']))
		$_POST['packaging_percent'] = 0;
	if(!isset($_POST['packaging_cost']))
		$_POST['packaging_cost'] = 0;
	if(!isset($_POST['forwarding_percent']))
		$_POST['forwarding_percent'] = 0;
	if(!isset($_POST['forwarding_cost']))
		$_POST['forwarding_cost'] = 0;
	if(!isset($_POST['other_cost']))
		$_POST['other_cost'] = 0;
	if(!isset($_POST['total_special_cost']))
		$_POST['total_special_cost'] = 0;
	if(!isset($_POST['total_finishing_cost']))
		$total_special_cost = 0;	
	
	div_start('finish_table');
	display_heading("Finish Costing");
	start_table(TABLESTYLE, "colspan=7  width=100%");
		$th = array(_(" "),_(" "),_(" "),_(" "));
		table_header($th);
	$_POST['total_mfrg_cost'] = $_POST['total_mfrg'];
	calculate_finishing_cost();
	start_row();
	    label_cell("Total MFRG Cost : ","colspan=3 width=175px align=right");
     	label_cell($_POST['total_mfrg_cost'],null,"total_mfrg_cost_label");	
		echo '<input type="hidden" name="total_mfrg_cost" id="total_mfrg_cost" value="'.$_POST['total_mfrg_cost'].'" />';	
	end_row();
	start_row();
	    label_cell("Sanding % : ","colspan=3 align=right");
     	text_cells(null, 'sanding_percent', $_POST['sanding_percent'], 10, 10, null, null, null,"id=sanding_percent");	
	end_row();
	start_row();
	    label_cell("Sanding Cost : ","colspan=3 align=right");
		text_cells(null, 'sanding_cost', $_POST['sanding_cost'], 10, 10, null, null, null,"id=sanding_cost");
	end_row();
	start_row();
	    label_cell("Polish % : ","colspan=3 align=right");
     	text_cells(null, 'polish_percent', $_POST['polish_percent'], 10, 10, null, null, null,"id=polish_percent");	
	end_row();
	start_row();
	    label_cell("Polish Cost : ","colspan=3 align=right");
     	text_cells(null, 'polish_cost', $_POST['polish_cost'], 10, 10, null, null, null,"id=polish_cost");	
	end_row();
	start_row();
	    label_cell("Packaging  % : ","colspan=3 align=right");
     	text_cells(null, 'packaging_percent', $_POST['packaging_percent'], 10, 10, null, null, null,"id=packaging_percent");		
	end_row();
	start_row();
	    label_cell("Packaging Cost : ","colspan=3 align=right");
     	text_cells(null, 'packaging_cost', $_POST['packaging_cost'], 10, 10, null, null, null,"id=packaging_cost");	
	end_row();
	start_row();
	    label_cell("Forwarding  % : ","colspan=3 align=right");
     	text_cells(null, 'forwarding_percent', $_POST['forwarding_percent'], 10, 10, null, null, null,"id=forwarding_percent");		
	end_row();
	start_row();
	    label_cell("Forwarding Cost : ","colspan=3 align=right");
     	text_cells(null, 'forwarding_cost', $_POST['forwarding_cost'], 10, 10, null, null, null,"id=forwarding_cost");	
	end_row();
	end_table();
    
	

	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array( _("Special Cost Reference"), _("Special Cost Rate"), _("Special cost Amount"),_("Total"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);

	
	$_POST['total_special_cost'] = 0;
	$id = find_submit('Edit');
	$k = 0; 

	foreach ($order->line_items as $line_con => $con_line)
   	{
			
		$_POST['total_special_cost'] += $con_line->total;
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
    		label_cell($con_line->special_cost_ref);
    		label_cell($con_line->special_finish_rate);
			label_cell($con_line->special_amount);
			label_cell($con_line->total);
    	
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			finishing_controls($order, $k, $line_con);
		}
		$total += $line_total;
    }

	if ($id==-1 && $editable)
		finishing_controls($order, $k);
	start_row();
	label_cell("Total Special Cost : ","colspan=3 align=right");
	label_cell($_POST['total_special_cost'],null,"total_special_cost_label");	
	echo '<input type="hidden" name="total_special_cost" id="total_special_cost" value="'.$_POST['total_special_cost'].'" />';		
	end_row();	
	start_row();
	    label_cell("Other Costs : ","colspan=3 align=right");
     	text_cells(null, 'other_cost', $_POST['other_cost'], 10, 10, null, null, null,"id=other_cost");	
	end_row();
	
	$_POST['total_finishing_cost'] = $_POST['sanding_cost']+$_POST['polish_cost']+$_POST['packaging_cost']+$_POST['forwarding_cost']+$_POST['other_cost']+$_POST['total_special_cost'];     
	start_row();
	    label_cell("Total Finishing Cost : ","colspan=3 align=right");
     	label_cell($_POST['total_finishing_cost'],null,"total_finishing_cost_label");	
		echo '<input type="hidden" name="total_finishing_cost" id="total_finishing_cost" value="'.$_POST['total_finishing_cost'].'" />';		
	end_row();
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	
	end_table();
}

function finishing_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		
		$_POST['special_cost_ref'] = $order->line_items[$id]->special_cost_ref;
		$_POST['special_finish_rate'] = $order->line_items[$id]->special_finish_rate;
		$_POST['special_amount'] = $order->line_items[$id]->special_amount;
		$_POST['total'] = $order->line_items[$id]->total;
		

	    $Ajax->activate('finish_table');
	}
	 
	
	text_cells_ex(null, 'special_cost_ref', null, null);
	
	text_cells_ex(null, 'special_finish_rate', 10, 10);
	text_cells_ex(null, 'special_amount', 10, 10);
	text_cells_ex(null, 'total', 10, 10);
	if ($id != -1)
	{
		button_cell('FinishUpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('FinishCancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		//set_focus('amount');
	} 
	else 
		submit_cells('FinishAddItem', _("Add"), "colspan=2",
			_('Add new line to Specail cost Section'), true);
	end_row();
	
}


function unset_finishing_cost_variables()
{
	unset($_POST['sanding_percent']);
	unset($_POST['sanding_cost']);
	unset($_POST['polish_percent']);
	unset($_POST['polish_cost']);
	unset($_POST['packaging_percent']);
	unset($_POST['packaging_cost']);
	unset($_POST['forwarding_percent']);
	unset($_POST['forwarding_cost']);
	unset($_POST['other_cost']);
	unset($_POST['total_finishing_cost']);
	unset($_POST['total_special_cost']);
}


function display_finishing_cost($finish_code_id)
{
	div_start('finish_table');
	display_heading("Finish Costing");
	start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_(" "),_(" "),_(" "),_(" "));
		table_header($th);
		$total_mfrg_cost = get_total_mfrg_cost($finish_code_id);
	start_row();
	    label_cell("Total MFRG Cost : ","colspan=3 align=right");
     	label_cell($total_mfrg_cost,null,"total_mfrg_label");		
	end_row();
	
	$sql = "select * from ".TB_PREF."finishing_cost where finish_code_id =".$finish_code_id;
	$result = db_query($sql, "Could not get finishing cost");
	while($row = db_fetch($result))
	{
		start_row();
			label_cell("Sanding % : ","colspan=3 align=right");
			label_cell($row['sanding_percent'],null,"sanding_percent");
		end_row();
		start_row();
			label_cell("Sanding Cost : ","colspan=3 align=right");
			label_cell($row['sanding_cost'],null,"sanding_cost");
		end_row();
		start_row();
			label_cell("Polish % : ","colspan=3 align=right");
			label_cell($row['polish_percent'],null,"polish_percent");
		end_row();
		start_row();
			label_cell("Polish Cost : ","colspan=3 align=right");
			label_cell($row['polish_cost'],null,"polish_cost");
		end_row();
		start_row();
			label_cell("Packaging  % : ","colspan=3 align=right");
			label_cell($row['packaging_percent'],null,"packaging_percent");
		end_row();
		start_row();
			label_cell("Packaging Cost : ","colspan=3 align=right");
			label_cell($row['packaging_cost'],null,"packaging_cost");
		end_row();
		start_row();
			label_cell("Forwarding  % : ","colspan=3 align=right");
			label_cell($row['forwarding_percent'],null,"forwarding_percent");
		end_row();
		start_row();
			label_cell("Forwarding Cost : ","colspan=3 align=right");
			label_cell($row['forwarding_cost'],null,"forwarding_cost");
		end_row();
		$other_cost = $row['other_cost'];
		$total_special_cost = $row['total_special_cost'];
		$total_finishing_cost = $row['total_finishing_cost'];
		$finishing_cost_id = $row['id']; 
		
	}
	end_table();
    
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array( _("Special Cost Reference"), _("Special Cost Rate"), _("Special cost Amount"),_("Total"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	$total_special_amount = 0;
	$sql = "select * from ".TB_PREF."finishing_special_cost where finishing_cost_id =".$finishing_cost_id;
	$result = db_query($sql, "Could not get finishing cost");
	while($row = db_fetch($result))
	{
		start_row();
			label_cell($row['special_cost_ref']);
			label_cell($row['special_rate']);
			label_cell($row['special_amount']);
			label_cell($row['total']);
		end_row();
	}

	start_row();
		label_cell("Total Special Cost : ","colspan=3 align=right");
		label_cell($total_special_cost,null,"total_special_cost_label");	
	end_row();	
	start_row();
	    label_cell("Other Costs : ","colspan=3 align=right");
     	label_cell($other_cost,null,"total_special_cost_label");	
	end_row();
	
	start_row();
	    label_cell("Total Finishing Cost : ","colspan=3 align=right");
     	label_cell($total_finishing_cost,null,"total_finishing_cost_label");	
	end_row();
	$colspan = count($th)-2;
	if (count($order->line_items))
		$colspan--;
	end_table(1);

    div_end();
	
}

function read_finish_cost($finish_code_id, $version)
{
	$sql = "select * from ".TB_PREF."finishing_cost where finish_code_id=".$finish_code_id." && version=".db_escape($version);
	$result = db_query($sql, "Could not read finishing cost.");
	if(db_num_rows($result) > 0)
	{
		while($row = db_fetch($result))
		{
			$_POST['sanding_percent'] = $row['sanding_percent'];
			$_POST['sanding_cost'] = $row['sanding_cost'];
			$_POST['polish_percent'] = $row['polish_percent'];
			$_POST['polish_cost'] = $row['polish_cost'];
			$_POST['packaging_percent'] = $row['packaging_percent'];
			$_POST['packaging_cost'] = $row['packaging_cost'];
			$_POST['forwarding_percent'] = $row['forwarding_percent'];
			$_POST['forwarding_cost'] = $row['forwarding_cost'];
			$_POST['total_special_cost'] = $row['total_special_cost'];
			$_POST['other_cost'] = $row['other_cost'];
		}
	}
	else
	{
		display_error("Finising Cost is not entered for this product.");
	}
}

function read_finish_special_costing($finishing_cost_id, &$order, $open_items_only=false)
{
	/*now populate the line po array with the purchase order details records */
	$sql = "SELECT special_cost_ref,special_rate,special_amount, total FROM ".TB_PREF."finishing_special_cost
	       WHERE finishing_cost_id =".db_escape($finishing_cost_id);
	$result = db_query($sql, "The lines on the special finish costing cannot be retrieved");
    if (db_num_rows($result) > 0)
    { //echo "<pre>"; print_r($order);
		while ($myrow = db_fetch($result))
        {
			
            if ($order->add_to_finish($order->lines_on_order, $myrow["special_cost_ref"],
            	$myrow["special_rate"],$myrow["special_amount"],$myrow["total"])) {
            		//$newline = &$order->line_items[$order->lines_on_order-1];
					// print_r($order);			
					}
								
        } 
    }
}

?>













