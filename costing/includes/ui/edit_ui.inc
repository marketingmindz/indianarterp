<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function text_cells_cost($label, $name, $value=null, $size="", $max="", $title=false, 
	$labparams="", $post_label="", $inparams="", $width="")
{
  	global $Ajax;

	default_focus($name);
	if ($label != null)
		label_cell($label, $labparams);
	echo "<td width=".$width.">";

	if ($value === null)
		$value = get_post($name);
	echo "<input $inparams type=\"text\" name=\"$name\" size=\"$size\" maxlength=\"$max\" value=\"$value\""
	    .($title ? " title='$title'" : '')
	    .">";

	if ($post_label != "")
		echo " " . $post_label;

	echo "</td>\n";
	$Ajax->addUpdate($name, $name, $value);
}


function display_design_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_design_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_design_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_design_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['consumable_name']);
			label_cell($row['consumable_category']);
			label_cell($row['unit']);
			text_cells(null, 'quantity', $row['quantity'], 10, 10, null, null, null,"id=quantity");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_design_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="5">Total Design Consumables Cost</td>';
		 text_cells(null, 'total_design_cons_cost', $total_design_cons_cost, 10, 10, null, null, null,"id=total_design_cons_cost");    
	end_row();
	end_table();
	
	
}
function display_finish_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_finish_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_finish_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Consumable Name"),_("Consumable Category"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_finish_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['consumable_id']);
			label_cell($row['consumable_category']);
			label_cell($row['unit']);
			text_cells(null, 'quantity', $row['quantity'], 10, 10, null, null, null,"id=quantity");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_finish_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="5">Total Finish Consumables Cost</td>';
		 text_cells(null, 'total_finish_cons_cost', $total_finish_cons_cost, 10, 10, null, null, null,"id=total_finish_cons_cost");    
	end_row();
	end_table();
}
function display_fabric_consumables($mfrg_id)
{
	$sql = "select * from ".TB_PREF."man_fabric_cons_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve man_fabric_cons_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Fabric Name"), _("unit"), _("quantity"), _("rate"), _("Cost"));         
	table_header($th);
	$total_fabric_cons_cost = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['cost'];
			label_cell($row['fabric_id']);
			text_cells(null, 'percentage', $row['percentage'], 10, 10, null, null, null,"id=percentage");	
			text_cells(null, 'rate', $row['rate'], 10, 10, null, null, null,"id=rate");	
			text_cells(null, 'cost', $cost, 10, 10, null, null, null,"id=cost");	
		end_row();	
			$total_fabric_cons_cost += $cost;
	}
	start_row();
	 echo '<td colspan="4">Total Fabric Consumables Cost</td>';
		 text_cells(null, 'total_fabric_cons_cost', $total_fabric_cons_cost, 10, 10, null, null, null,"id=total_fabric_cons_cost");    
	end_row();
	end_table();
}
function display_wood_costing($mfrg_id)
{
	$sql = "select * from ".TB_PREF."manufacturing_wood_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_wood_cost ###edit_cost");
	
	start_table(TABLESTYLE, "colspan=7 width=120%");
	$th = array(_("Wood"), _("CFT"), _("Rate"), _("Amount"));         
	//table_header($th);
	$total_wood_cost = 0;
	$i = 0;
	$data_count = mysql_num_rows($result);
	while($row = db_fetch($result))
	{
		start_row();
			$cost = $row['amount'];
			$wood = get_wood_name($row['wood_id']);
			label_cell($wood['consumable_name'], null,"wood_name");
			echo '<input type="hidden" name="wood_id'.$i.'" class="w_in" value="'.$row['wood_id'].'" id="wood_id"'.$i.' >';
			text_cells(null, 'cft'.$i, $row['cft'], 10, 10, null, null, null,"class='w_in wood_cft' id=cft_".$i);	
			text_cells(null, 'rate'.$i, $row['rate'], 10, 10, null, null, null,"class='w_in wood_rate' id=rate_".$i);	
			text_cells(null, 'amount'.$i, $cost, 10, 10, null, null, null,"class='w_in wood_amount' id=amount_".$i);	
		end_row();	
			$total_wood_cost += $cost;
			$i++;
	}
	if($data_count ==0)
	{
		start_row();
			echo '<td id="wood_name"></td>';
			echo '<td><input class="w_in wood_cft" id="cft_0" type="text" name="cft0" size="10" maxlength="10" value="0" readonly="readonly"></td>';
			echo '<td><input class="w_in wood_rate" id="rate_0" type="text" name="rate0" size="10" maxlength="10" value="0" readonly="readonly"></td>';
			echo '<td><input class="w_in wood_amount" id="amount_0" type="text" name="amount0" size="10" maxlength="10" value="0" readonly="readonly"></td>';
		end_row();
	}
	/*start_row();
	 echo '<td colspan="3">Total Wood Cost</td>';
		 text_cells(null, 'total_wood_cost', $total_wood_cost, 10, 10, null, null, null,"readonly id=total_wood_cost");    
	end_row();*/
	end_table();	
}

function display_special_labour_costing($mfrg_id, $basic_price)
{
	$sql = "select * from ".TB_PREF."manufacturing_labour_special_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_special_cost ###edit_cost");
	$data_count = mysql_num_rows($result);
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Labout Type"), _("special Rate"), _("Amount"), _("Cost"));         
	//table_header($th);
	$total_special_labour_cost = 0;
	$i = 0;
	while($row = db_fetch($result))
	{
		start_row();
			$special_labour_cost = $row['special_labour_cost'];
			text_cells(null, 'labour_type'.$i, $row['labour_type'], 20, null, null, null, null,"class='l_in' id=labour_type_".$i);	
			text_cells(null, 'special_rate_'.$i, $row['special_rate'], 10, 10, null, null, null,"class='l_in spacial-rate_1' id=special_rate_".$i);	
			text_cells(null, 'labour_amount_'.$i, $row['labour_amount'], 10, 10, null, null, null,"class='l_in labour_amount_1' id=labour_amount_".$i);	
			text_cells(null, 'special_labour_cost_'.$i, $special_labour_cost, 10, 10, null, null, null,"class='l_in' id=special_labour_cost_".$i);	
		end_row();	
			$total_special_labour_cost += $special_labour_cost;
			$i++;
	}
	if($data_count==0)
	{
		start_row();
		echo '<td><input class="l_in" id="labour_type_0" type="text" name="labour_type0" size="20" maxlength="" value="" readonly="readonly"></td>
			<td><input class="l_in spacial-rate_1" id="special_rate_0" type="text" name="special_rate_0" size="10" maxlength="10" value="0" readonly="readonly"></td>
			<td><input class="l_in labour_amount_1" id="labour_amount_0" type="text" name="labour_amount_0" size="10" maxlength="10" value="0" readonly="readonly"></td>
			<td><input class="l_in" id="special_labour_cost_0" type="text" name="special_labour_cost_0" size="10" maxlength="10" value="0" readonly="readonly"></td>';
		end_row();
	}
	end_table();
	 //echo '<td colspan="3">Total Special Cost</td>';
	echo "<td>";
	echo '<input readonly="readonly" id="total_special_labour_cost" type="text" name="total_special_labour_cost" size="19" maxlength="10" value="'.$total_special_labour_cost.'">';
	echo "</td>";    
	//end_row();
	
	
	/*$total_labour_cost = $basic_price + $total_special_labour_cost;	
	//label_cell($total_labour_cost);
		//echo '<input type="hidden" name="total_labour_cost" id="total_labour_cost" value="'.$total_labour_cost.'" />';		
		label_cell($total_labour_cost,null,"total_labour_cost_label");
		echo '<input type="hidden" readonly name="total_labour_cost" id="total_labour_cost" value="'.$total_labour_cost.'" />';	
*/

}

function basic_labour_price($mfrg_id)
{
	$sql = "select basic_price from ".TB_PREF."manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_cost ###edit_cost");
	while($row = db_fetch($result))
	{
		$basic_price = $row['basic_price'];		
	}
	return $basic_price;
}
function display_labour_costing($mfrg_id)
{	
	start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Basic Price"),_("Special Labour Cost"),_("Total Labour Cost"));         
	//table_header($th);
	start_row();
	
	
	$sql = "select * from ".TB_PREF."manufacturing_labour_cost where mfrg_id = ".db_escape($mfrg_id);
	$result = db_query($sql, "Could not retrieve manufacturing_labour_cost ###edit_cost");
	
	//start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Basic Price"));         
	//table_header($th);
	//start_row();
	while($row = db_fetch($result))
	{
		$basic_price = $row['basic_price'];
		text_cells(null, 'basic_price', $basic_price, 10, 10, null, null, null,"id=basic_price");		
	}
	//end_row();
	//end_table();
	echo "<td  class='special_labour_cost'>";
	display_special_labour_costing($mfrg_id, $basic_price);
	echo "</td>";
	end_row();
	end_table();
}


function display_mfrg_cost($finish_code_id)
{
	$sql = "select * from ".TB_PREF."manufacturing_cost where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve manufacturing_cost ###edit_cost");
	
	//start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(/*_("Design Consumables"),_("Finish Consumables"), _("Fabric Consumables"),*/ _("Wood"), _("Labour"),  _("Extra %"), _("Extra Amount"), _("Total Cost"), _("Admin %"), _("Admin Cost"), _("Total CP"), _("Profit %"), _("Profit"), _("Total MFRG Cost"));         
	//table_header($th);
	
	while($row = db_fetch($result))
	{
		//start_row();
		$mfrg_id = $row['id'];
		/*echo "<td>";
		display_design_consumables($mfrg_id);
		echo "</td><td>";
		display_finish_consumables($mfrg_id);
		echo "</td><td>";
		display_fabric_consumables($mfrg_id);
		
		echo "</td>";*/
		//echo "<td class='wood_cost'>";
		echo '<input type="hidden" name="total_cons_cost" id="total_cons_cost" value="0" />';
		echo '<input type="hidden" name="total_finish_cons_cost" id="total_finish_cons_cost" value="0" />';
		echo '<input type="hidden" name="total_fabric_cost" id="total_fabric_cost" value="0" />';
		//display_wood_costing($mfrg_id);
		//echo "</td><td>";
		//display_labour_costing($mfrg_id);
		//echo "</td>";
		text_cells_cost(null, 'extra_percent', $row['extra_percent'], 10, 10, null, null, null,"id=extra_percent");
		text_cells_cost(null, 'extra_amount', $row['extra_amount'], 10, 10, null, null, null,"readonly id=extra_amount");

		//echo "<td>".$row['total_cost']."</td>";
		label_cell($row['total_cost'],"id='total_cost_label'");	
		echo '<input type="hidden" name="total_cost" id="total_cost" value="'.$row['total_cost'].'" />';
		
		text_cells_cost(null, 'admin_percent', $row['admin_percent'], 10, 10, null, null, null,"id=admin_percent");
		text_cells_cost(null, 'admin_cost', $row['admin_cost'], 10, 10, null, null, null,"readonly id=admin_cost");
				
		//echo "<td>".$row['total_cp'];
		label_cell($row['total_cp'],"id='total_cp_label'");	
		echo '<input type="hidden" name="total_cp" id="total_cp" value="'.$row['total_cp'].'" />';
		
		text_cells_cost(null, 'profit_percent', $row['profit_percent'], 10, 10, null, null, null,"id=profit_percent");
		text_cells_cost(null, 'profit', $row['profit'], 10, 10, null, null, null,"readonly id=profit");

		//echo "<td>".$row['total_mfrg'];
		//echo "</td>";
		label_cell($row['total_mfrg'],"id='total_mfrg_label'");	
		echo '<input type="hidden" readonly name="total_mfrg" id="total_mfrg" value="'.$row['total_mfrg'].'" />';
		//end_row();	
		
	}
	//end_table();
}



function display_finishing_cost($finish_code_id)
{	
	/*start_table(TABLESTYLE, "colspan=7 width=100%");
	$th = array(_("Sanding %"),_("Sending Cost"),_("Polish %"),_("Polish Cost"),_("Packaging %"),_("Packaging Cost"),_("Forwarding %"),_("Forwarding Cost"), _("Special Finishing Cost"), _("Other Cost"), _("Total Finishing Cost"));            
	table_header($th);
	start_row();*/
	
	
	$sql = "select * from ".TB_PREF."finishing_cost where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve finishing_cost ###edit_cost");

	while($row = db_fetch($result))
	{
		$finishing_cost_id = $row['id'];
		$sanding_percent = $row['sanding_percent'];
		$sanding_labour = $row['sanding_labour'];
		$actual_sending_cost = $row['actual_sending_cost'];
		$sanding_cost = $row['sanding_cost'];
		$polish_labour = $row['polish_labour'];
		$actual_polish_cost = $row['actual_polish_cost'];
		$polish_percent = $row['polish_percent'];
		$polish_cost = $row['polish_cost'];
		$packaging_labour = $row['packaging_labour'];
		$actual_packaging_cost = $row['actual_packaging_cost'];
		$packaging_percent = $row['packaging_percent'];
		$packaging_cost = $row['packaging_cost'];
		$forwarding_percent = $row['forwarding_percent'];
		$forwarding_cost = $row['forwarding_cost'];
		$other_cost = $row['other_cost'];
		$lumsum_cost_percent = $row['lumsum_cost_percent'];
		$lumsum_cost_value = $row['lumsum_cost_value'];
		
		text_cells_cost(null, 'sanding_labour', $sanding_labour, 10, 10, null, null, null,"id=sanding_labour");
		text_cells_cost(null, 'sanding_percent', $sanding_percent, 10, 10, null, null, null,"id=sanding_percent");		
		text_cells_cost(null, 'sanding_cost', $sanding_cost, 10, 10, null, null, null,"id=sanding_cost");
		text_cells_cost(null, 'polish_labour', $polish_labour, 10, 10, null, null, null,"id=polish_labour");		
		text_cells_cost(null, 'polish_percent', $polish_percent, 10, 10, null, null, null,"id=polish_percent");		
		text_cells_cost(null, 'polish_cost', $polish_cost, 10, 10, null, null, null,"id=polish_cost");
		text_cells_cost(null, 'packaging_labour', $packaging_labour, 10, 10, null, null, null,"id=packaging_labour");		
		text_cells_cost(null, 'packaging_percent', $packaging_percent, 10, 10, null, null, null,"id=packaging_percent");		
		text_cells_cost(null, 'packaging_cost', $packaging_cost, 10, 10, null, null, null,"id=packaging_cost");

		text_cells_cost(null, 'actual_sending_cost', $actual_sending_cost, 10, 10, null, null, null,"id=actual_sending_cost");
		text_cells_cost(null, 'actual_polish_cost', $actual_polish_cost, 10, 10, null, null, null,"id=actual_polish_cost");
	text_cells_cost(null, 'actual_packaging_cost', $actual_packaging_cost, 10, 10, null, null, null,"id=actual_packaging_cost");
		text_cells_cost(null, 'lumsum_cost_percent', $lumsum_cost_percent, 10, 10, null, null, null,"id=lumsum_cost_percent");
		text_cells_cost(null, 'lumsum_cost_value', $lumsum_cost_value, 10, 10, null, null, null,"readonly id=lumsum_cost_value");
		$total1 = $sanding_cost + $polish_cost + $packaging_cost;
		$total2 = $actual_sending_cost + $actual_polish_cost + $actual_packaging_cost;
		$total_average = ($total1 + $total2 + $lumsum_cost_value)/3;
		$total_average = number_format((float)$total_average, 2, '.', '');
		label_cell($total_average, null,"total_average");

		text_cells_cost(null, 'forwarding_percent', $forwarding_percent, 10, 10, null, null, null,"id=forwarding_percent");		
		text_cells_cost(null, 'forwarding_cost', $forwarding_cost, 10, 10, null, null, null,"readonly id=forwarding_cost");
		$total_finishing = $sanding_cost + $polish_cost + $packaging_cost + $forwarding_cost;		
	}

	echo "<td  class='special_finishing_cost' width='418' colspan='4'>";
		$sql = "select * from ".TB_PREF."finishing_special_cost where finishing_cost_id = ".db_escape($finishing_cost_id);
		$result = db_query($sql, "Could not retrieve special finishing cost##edit cost");
		start_table(TABLESTYLE, "colspan=7 width=100%");
		$th = array(_("Special Cost Reference"),_("Special Cost Rate"),_("Special Cost Amount"),_("Total"));         
		//table_header($th);
		$total_finishing_special_cost = 0;
		$i = 0;
		$data_count = mysql_num_rows($result);
		while($row =  db_fetch($result))
		{
			start_row();
				$special_amount = $row['special_amount'];
				$total = $row['total'];
				text_cells_cost(null, 'special_cost_ref_'.$i, $row['special_cost_ref'], 18, null, null, null, null,"class='f_in mm_s_ref' id=special_cost_ref_".$i);
				text_cells_cost(null, 'special_frate_'.$i, $row['special_rate'], 10, 10, null, null, null,"class='f_in mm_s_rate' id=special_frate_".$i);
				text_cells_cost(null, 'special_famount_'.$i, $special_amount, 10, 10, null, null, null,"class='f_in mm_s_famount' id=special_famount_".$i);
				text_cells_cost(null, 'special_ftotal_'.$i, $total, 10, 10, null, null, null,"class='f_in mm_s_ftotal' id=special_ftotal_".$i);
			end_row();
			$total_finishing_special_cost += $total;
			$i++;
		}
		
		if($data_count==0)
		{
			start_row();
				echo  '<td width=""><input class="f_in mm_s_ref" id="special_cost_ref_0" type="text" name="special_cost_ref_0" size="18" maxlength="" value="" readonly="readonly"></td>
				<td width=""><input class="f_in mm_s_rate" id="special_frate_0" type="text" name="special_frate_0" size="10" maxlength="10" value="0" readonly="readonly"></td>
				<td width=""><input class="f_in mm_s_famount" id="special_famount_0" type="text" name="special_famount_0" size="10" maxlength="10" value="0" readonly="readonly"></td>
				<td width=""><input class="f_in mm_s_ftotal" id="special_ftotal_0" type="text" name="special_ftotal_0" size="10" maxlength="10" value="0" readonly="readonly"></td>';
			end_row();
		}		
			
		end_table();
		text_cells_cost(null, 'total_finishing_special_cost', $total_finishing_special_cost, 11, 10, null, null, null,"readonly id=total_finishing_special_cost");

	echo "</td>";
		text_cells_cost(null, 'other_cost', $other_cost, 10, 10, null, null, null,"id=other_cost");
		$total_finishing_cost = $other_cost + $total_finishing_special_cost + $total_finishing;		
		//text_cells(null, 'total_finishing_cost', $total_finishing_cost, 10, 10, null, null, null,"id=total_finishing_cost");		
		/*label_cell($total_finishing_cost,"width=111","total_finishing_cost_label");	
		echo '<input type="hidden" readonly name="total_finishing_cost" id="total_finishing_cost" value="'.$total_finishing_cost.'" />';*/		


	/*end_row();
	end_table();*/
}


function display_final_costing($finish_code_id)
{	
	/*$sql = "select * from ".TB_PREF."final_costing where finish_code_id = ".db_escape($finish_code_id);
	$result = db_query($sql, "Could not retrieve finishing_cost ###edit_cost");

	while($row = db_fetch($result))
	{
		$total_amount = $row['total_amount'];
		$final_profit_percent = $row['final_profit_percent'];
		$final_profit = $row['final_profit'];
		$total_percent = $row['total_finishing_percent'];
		$total_finishing_cost = $row['total_finishing_cost'];
		$total_fcost = $row['total_cost'];
		$finishing_exp_percent = $row['finishing_exp_percent'];
		$final_finishing_cost = $row['final_finishing_cost'];
		$final_total_cost = $row['final_total_cost'];

		text_cells_cost(null, 'total_percent', $total_percent, 10, 10, null, null, null,"readonly id=total_percent","90");

		text_cells_cost(null, 'finishing_exp_percent', $finishing_exp_percent, 10, 10, null, null, null,"id=finishing_exp_percent","90");		
		text_cells_cost(null, 'final_finishing_cost', $final_finishing_cost, 13, 10, null, null, null,"id=final_finishing_cost","107");

		text_cells_cost(null, 'total_fcost', $total_fcost, 10, 10, null, null, null,"readonly id=total_fcost","90");	

		text_cells_cost(null, 'final_profit_percent', $final_profit_percent, 10, 10, null, null, null,"id=final_profit_percent","90");		
		text_cells_cost(null, 'final_profit', $final_profit, 10, 10, null, null, null,"id=final_profit","90");					
		text_cells_cost(null, 'final_total_cost', $final_total_cost, 10, 10, null, null, null,"readonly id=final_total_cost","90");
		$currency = $row['currency_price'];		
	}*/


	$sql = "SELECT * FROM ".TB_PREF."finishing_cost WHERE finish_code_id=".db_escape($finish_code_id);
		$result = db_query($sql, "could not get total percent");
		while($finishing_data = db_fetch($result))
		{
			$_POST['total_finishing_percent'] = $finishing_data['sanding_percent'] + $finishing_data['polish_percent'] + $finishing_data['packaging_percent'] + $finishing_data['lumsum_cost_percent'] + $finishing_data['forwarding_percent'];

			$total_process_cost = $finishing_data['sanding_cost'] + $finishing_data['polish_cost'] + $finishing_data['packaging_cost'];
			$total_actual_cost = $finishing_data['actual_sending_cost'] + $finishing_data['actual_polish_cost'] + $finishing_data['actual_packaging_cost'];
			$total_lumsum_cost = $finishing_data['lumsum_cost_value'];

			$average_process_cost = ($total_process_cost + $total_actual_cost + $total_lumsum_cost )/3;
			$average_process_cost = number_format((float)$average_process_cost, 2, '.', '');

			$total_other_than_process_cost = $finishing_data['forwarding_cost'] + $finishing_data['other_cost'] + $finishing_data['total_special_cost'];
			$total_other_than_process_cost = number_format((float)$total_other_than_process_cost, 2, '.', '');

			// get total finishing cost
			$total_finishing_cost = $average_process_cost + $total_other_than_process_cost;

			// get total cost
			$total_cost = $total_mfrg_cost + $total_finishing_cost;

			// get final cost step data

			$total_amount = $finishing_data['total_amount'];

			//label_cell($total_mfrg_cost,null,"total_mfrg_cost");
			label_cell($total_other_than_process_cost, null, "total_other_than_process_cost");
			label_cell($total_finishing_cost, null, "total_finishing_cost");
			echo '<input type="hidden" name="total_finishing_cost" id="total_finishing_cost" value="'.$total_finishing_cost.'" />';
		}

		$sql1 = "SELECT * FROM ".TB_PREF."final_costing WHERE finish_code_id=".db_escape($finish_code_id);
		$result1 = db_query($sql1, "could not get total percent");
		$final_cost_data = db_fetch($result1);

		$total_cost = $final_cost_data['total_cost'];
		$final_profit_percent = $final_cost_data['final_profit_percent'];
		$final_profit = $final_cost_data['final_profit'];
		$final_total_cost = $final_cost_data['final_total_cost'];
		

		text_cells_cost(null, 'total_cost', $total_cost, 10, 10, null, null, null,"readonly id=total_cost");
		text_cells_cost(null, 'final_profit_percent', $final_profit_percent, 10, 10, null, null, null," id=final_profit_percent");
		text_cells_cost(null, 'final_profit', $final_profit, 10, 10, null, null, null,"readonly id=final_profit");
		text_cells_cost(null, 'final_total_cost', $final_total_cost, 10, 10, null, null, null,"readonly id=final_total_cost");
	
		// get total finishing percent
		




	echo "<td class='curre_div'>";
	start_table();
	start_row();
		$sql = "SELECT * from ".TB_PREF."currency_master";
		$result = db_query($sql, "Could not get currencies.");
		$currencies = array();
		$total_currency = 0;

		while($row = db_fetch($result))
		{
			$id = $row['id'];
			$currencies[] = $id;
	
		    label_cell("Price in: ".$row['currency_name'],"colspan=	 align=right");
	     	text_cells(null, "currency".$id, $final_cost_data['final_total_cost']*$row['rate'], 10, 10, null, null, null,"readonly id=currency".$id);	
		
			$total_currency++;
		}
		$currency = implode(",",$currencies);
		echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
		echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	end_row();





	$currency = implode(",",$currencies);
	echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
	echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	end_table();
	echo "</td>";
		
	/*end_row();
	
	end_table();*/
}


function display_costing_details($ref_id)
{	
	div_start("display_costing");
	echo "<div id='inner_div'>";
	echo "</div>";
	echo "<div id='inner_table'>";
	echo "<div id='inner_div2'>";
	echo '<input type="hidden" id="reference_id" value="'.$ref_id.'" >';
	start_table(TABLESTYLE, "id='product_table'");
	echo "<thead class='mm-table-head'><tr class='tableheader mm-fixed-header'>
		    <th class='head1'>Product Details</th>
		    <th class='head2'>Wood</th>
		    <th class='head3'>CFT</th>
		    <th class='head4'>Rate</th>
		    <th class='head5'>Amount</th>
		    <th class='head6'>Total Wood Cost</th>
		    <th class='head7'>Basic Labour Price</th>
		    <th class='head8'>Labour Type</th>
		    <th class='head9'>Labour Rate</th>
		    <th class='head10'>Labour Amount</th>
		    <th class='head11'>Labour Cost</th>
		    <th class='head12'>Total Special Labour Cost</th>
		    <th class='head13'>Total Labour Cost</th>
		    <th class='head14'>Extra %</th>
		    <th class='head15'>Extra Amount</th>
		    <th class='head16'>Total Cost</th>
		    <th class='head17'>Admin %</th>
		    <th class='head18'>Admin Cost</th>
		    <th class='head19'>Total CP</th>
		    <th class='head20'>Profit %</th>
		    <th class='head21'>Profit</th>
		    <th class='head22'>Total MFRG Cost</th>
		    <th class='head23'>Sanding Labour</th>
		    <th class='head24'>Sanding %</th>
		    <th class='head25'>Sanding Cost</th>
		    <th class='head26'>Polish Labour</th>
		    <th class='head27'>Polish %</th>
		    <th class='head28'>Polish Cost</th>
		    <th class='head29'>Packaging Labour</th>
		    <th class='head30'>Packaging %</th>
		    <th class='head31'>Packaging Cost</th>
		    <th class='head32'>Actual Sanding Cost</th>
		    <th class='head33'>Actual Polish Cost</th>
		    <th class='head34'>Actual Packaging Cost</th>
		    <th class='head35'>Lumsum Cost Percentage</th>
		    <th class='head36'>Lumsum Cost</th>
		    <th class='head37'>Total Average Process Cost</th>
		    <th class='head38'>Forwarding %</th>
		    <th class='head39'>Forwarding Cost</th>
		    <th class='head40'>Special Cost Reference</th>
		    <th class='head41'>Special Rate</th>
		    <th class='head42'>Amount</th>
		    <th class='head43'>Special Cost</th>
		    <th class='head44'>Total Special Cost</th>
		    <th class='head45'>Other Cost</th>
		    <th class='head46'>Total Other Than Process Cost</th>
		    <th class='head47'>Total Finishing Cost</th>
		    <th class='head48'>Total Costs</th>
		    <th class='head49'>Proft %</th>
		    <th class='head50'>Final Profit</th>
		    <th class='head51'>Final Total Cost</th>
		    <th class='head52'>Currency Rate</th>
		</tr></thead>"; 
	//$th = array(_("Product Details"),_("Manufacturing Cost"), _("Finishing Cost"), _("Final Cost"));

	$sql = "SELECT * from ".TB_PREF."costing_reference_details where ref_id = ".db_escape($ref_id);
	$res = db_query($sql, "Cannot get costing reference detilas ###range-cat");
	while($cat = db_fetch($res))
	{
		$category_id = $cat['category_id'];
		$range_id = $cat['range_id'];
		$sql = "SELECT * FROM ".TB_PREF."finish_product WHERE category_id=".db_escape($category_id)." AND range_id = ".db_escape($range_id);
		$result1 = db_query($sql, "could not get finish product list");

		while($row1 = db_fetch($result1))
		{	
			$finish_code_id = $row1['finish_pro_id'];
			$sql = "SELECT finish_code_id FROM ".TB_PREF."final_costing where finish_code_id = ".db_escape($finish_code_id);
			$res1 = db_query($sql, "could not match with final costing");
			if(db_num_rows($res1) > 0)
			{
				$product = get_product_details($finish_code_id);
				echo "<tr class='finish_product' id='".$finish_code_id."'>";
				echo '<td class="pro_details"><table><tr><td class="tableheader">Product Name</td><td>';
				echo '<input type="hidden" name="finish_code_id" value="'.$finish_code_id.'" >';
				echo $product['finish_product_name'].'</td></tr>';
				echo '<tr><td class="tableheader">Finish Code</td><td>'.$product['finish_comp_code'].'</td></tr></table></td>';
				echo '<td colspan="4" class="wood_cost">';
				$sql = "select * from ".TB_PREF."manufacturing_cost where finish_code_id = ".db_escape($finish_code_id);
				$result = db_query($sql, "Could not retrieve manufacturing_cost ###edit_cost");
				while($row = db_fetch($result))
				{
					$mfrg_id = $row['id'];
					display_wood_costing($mfrg_id);
					$total_wood_cost = $row['total_wood_cost'];
					$total_labour_cost = $row['total_labour_cost'];
				}
					//display_mfrg_cost($finish_code_id);
				  echo ' </td>';
				  echo "<td>";
				  echo '<input readonly="readonly" id="total_wood_cost" type="text" name="total_wood_cost" size="10" maxlength="10" value="'.$total_wood_cost.'">';
				  
				  echo "</td>";

				  echo "<td>";
				  $basic_price = basic_labour_price($mfrg_id);
				  echo '<input width="104" id="basic_price" type="text" name="basic_price" size="11" maxlength="10" value="'.$basic_price.'" >';
				  echo "</td>";


				  echo '<td colspan="4" class="special_labour_cost">';
				  display_special_labour_costing($mfrg_id, $basic_price);
				  echo '</td>';

				  echo "<td>";
						echo '<input readonly="readonly" id="total_labour_cost" type="text" name="total_labour_cost" size="11" maxlength="10" value="'.$total_labour_cost.'">';
				  echo '</td>';    
		
				  display_mfrg_cost($finish_code_id);
				  
					display_finishing_cost($finish_code_id);
				
					display_final_costing($finish_code_id);
				  
				echo "</tr>";
			}
			else
			{
				$product = get_product_details($finish_code_id);
				echo "<tr class='finish_product' id='".$finish_code_id."'>";
				echo '<td class="pro_details"><table><tr><td class="tableheader">Product Name</td><td>';
				echo '<input type="hidden" name="finish_code_id" value="'.$finish_code_id.'" >';
				echo $product['finish_product_name'].'</td></tr>';
				echo '<tr><td class="tableheader">Finish Code</td><td>'.$product['finish_comp_code'].'</td></tr></table></td>';
				
				echo "<td colspan='100' class='no-data-td'><h1>Costing is not entered for this product</h1></td></tr>";
			}
		}
	}
	end_table();
    echo "</div></div>";
	div_end();
	
	div_start();
	echo "<div id='lock_popup'>
			<div id='save_list_content'></div><div style='margin:auto; text-align:center;'>
			<h2>Are you sure to lock this price?</h2>		
				<div id='controls'><span id='close'><a href='lock_price.php?ref_id=".$ref_id."' >YES</a></span><span id='no'>NO</span> </div>
			</div>
			
		  </div>";
	echo '<center><a id="lock" style="font-size:15px; width:130px;" href="#lock_popup" >Lock Price List</a></center>';
	
	div_end();
	
}


?>
