<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_reference_no_list($reference_id){

	start_table();
	$sql = "select reference_no from ".TB_PREF."costing_reference  where ref_id = ".db_escape($reference_id);
	$result = db_query($sql, "could not get reference_no");
	$reference_no = db_fetch($result);
	echo '<tr><td>Reference No.</td><td><input type="hidden" name="ref_id" value="'.$reference_id.'" ><input type="text" name="reference_no" value="'.$reference_no['reference_no'].'" >';	
	echo "</td></tr>";
	end_table();
	
}



// costing module - multiple category list
function multiple_categories_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	$sql = "SELECT category_id, cat_name, cat_reference, inactive FROM 0_item_category order by cat_name ASC";
	$result = db_query($sql);
	echo '<div class="multiselect">
           <div class="selectBox" onclick="showCheckboxes()">';
	echo '<select autocomplete="off" class="combo" title="Select Category" id="slc_category" >
	  <option value="-1" >- select category - </option>';
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$selected = "";
		
		echo '<option value="'.$category_id.'">'.$row['cat_name'].'</option>';	
	}
	echo "</select>";
	if(isset($_POST['select_all']))
	{
		$checked = 'checked="checked"';
	}
	echo '<div class="overSelect"></div>
        </div>
        <div id="checkboxes"> <label><input type="checkbox" id="select_all" name="select_all" '.$checked.' /> Select all</label>';
	$result = db_query($sql);
	while($row = db_fetch($result))
	{
		$category_id = $row['category_id'];
		$checked = "";	
		if(in_array($category_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		
		echo '<label><input type="hidden" id="'.$category_id.'" value="'.$row['cat_name'].'" >
		<input type="checkbox" class="check" value="'.$category_id.'" '.$checked.' name="category_id['.$category_id.']" />'.$row['cat_name'].'</label>';
	}	

    echo ' </div>
    </div>';
}
function multiple_category_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_categories_list($name, $selected_id, $spec_opt, $submit_on_change);
	echo "</td>\n";
}

function multiple_category_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_category_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change);
	echo "</tr>\n";
}

function display_header(){
	div_start('product');
	start_table(TABLESTYLE_NOBORDER, "style='width:50%;min-width:50%;'");
	global $Ajax;
	$Ajax->activate('product');
	$Ajax->activate('GetReferenceNo');
		design_category_list_row(_("Select Category:"), 'category_id', null, _('----Select---'));
		
		
		sub_collection_code_list_row(_("Collection:"), 'collection_id', null);
		if($_POST['collection_id'] == "NonCollection"){
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'), null, 'style=display:none');
		}else{
			design_range_list_row(_("Range:"), 'range_id', null, _('----Select---'));
		}

	end_table();
	
	div_end();
	submit_center_first('GetReferenceNo', _("Submit"), '', 'default');
}

function display_costing_section()
{
    global $Ajax;
	start_table(TABLESTYLE, "width=40%");
	$th = array(_("Manufacturing Section"), "");
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
		start_row();
		echo '<td>Labour Amount: </td><td><input type="radio" checked="checked" name="labour_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="labour_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="labour_amount" />
		     </td>';
		end_row();
		start_row();
		echo '<td>Admin %: </td><td><input type="radio" checked="checked" name="admin_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="admin_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="admin_percent" />
		     </td>';
		end_row();
		start_row();
		echo '<td>Profit %: </td><td><input type="radio" checked="checked" name="profit_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="profit_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="profit_percent" />
		     </td>';
		end_row();
	end_table();
	
	start_table(TABLESTYLE, "width=40%");
	$th = array(_("&nbsp;&nbsp;&nbsp;&nbsp; Finishing Section &nbsp;&nbsp;&nbsp;&nbsp;"),_(" "));
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
		start_row();
		echo '<td>Sanding %: </td><td><input type="radio" checked="checked" name="sanding_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="sanding_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="sanding_percent" />
		     </td>';
		end_row();
		start_row();
		echo '<td>Polish %: </td><td><input type="radio" checked="checked" name="polish_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="polish_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="polish_percent" />
		     </td>';
		end_row();
		start_row();
		echo '<td>Packaging % </td><td><input type="radio" checked="checked" name="packaging_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="packaging_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="packaging_percent" />
		     </td>';
		end_row();
		start_row();
		echo '<td>Forwarding % : </td><td><input type="radio" checked="checked" name="forwarding_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="forwarding_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="forwarding_percent" />
		     </td>';
		end_row();
	end_table();
	
	start_table(TABLESTYLE, "width=40%");
	$th = array(_("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Final Section &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"),_(" "));
	if (count($order->line_items)) $th[] = '';
	table_header($th);
	
		start_row();
		echo '<td>Profit %:  </td><td><input type="radio" checked="checked" name="final_profit_sign" value="plus" /><span class="sign"> + </span>
					<input type="radio" name="final_profit_sign" value="minus" /><span class="sign"> - </span>	
					<input type="text" name="final_profit_percent" />
		     </td>';
		end_row();
		start_row();
		echo "<td colspan='2'><center>";
	    echo '<input type="submit" name="submit" id="submit" value="submit" >';
		echo "</center></td>";
		
		end_row();
	end_table();
	
}




function display_costing_reference_list($category_id, $range_id)
{

	$sql = "select * from ".TB_PREF."costing_reference_details d LEFT JOIN ".TB_PREF."costing_reference c on 
			c.ref_id = d.ref_id where c.locked = '1' && d.range_id = ".db_escape($range_id)." && d.category_id = ".db_escape($category_id)." group by d.ref_id";

	$result = db_query($sql, "could not get costing_reference details.");
	if(db_num_rows($result) > 0)
	{
		start_table(TABLESTYLE, "width=70%");
		$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version "));
		table_header($th);
		$i = 0;
		while($row = db_fetch($result))
		{
			//print_r($row); break;
			$ref_id = $row['ref_id'];
			$reference_no = $row['reference_no'];
			$remark = $row['remark'];
			$date = sql2date($row['date']);
			$complete = $row['complete'];
			start_row();
				label_cell($date,null,"date");
				label_cell($reference_no,"width='120' height='40'","reference_no");
				label_cell($remark,null,"remark");	

				echo '<td><a class="button" href="edit_range.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >Edit </a></td>';
				/*$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
				$result1 = db_query($sql, "could not get costing_reference details.");
				$k = db_fetch($result1);

				echo "<td>";
				for($i = "A"; $i <= $k['version']; $i++)
				{
					echo '<a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$i.'" >'.$i."</a>";
					$version = $i;
				}
				echo "</td>";*/
			
				

				$i++;
			end_row();
		}end_table();
		
			
	}
	else
	{
		display_notification("Costing is not entered.");
	}


}




?>
