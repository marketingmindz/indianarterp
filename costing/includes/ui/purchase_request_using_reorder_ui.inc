<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function display_hearder(&$order){
	global $selected_id;

	
		/** detail **/
		start_outer_table(TABLESTYLE2);
		
		table_section(1);
		design_location_list_row(_("Location:"), 'location_id', null, _('--Select Location--'));
		if(isset($_GET['PurchaseRequestId'])){
			stock_work_center_list_row(_("Work Center:"),'work_center_id', null, _('-----Select Work Center----'));
		}else{
			design_work_center_list_row(_("Work Center:"),'work_center_id', null, _('-----Select Work Center----'));
		}
		date_row(_("Date") . ":", 'date');
		end_outer_table(1);
	
}


//  consumable part //

function display_consumable_summary(&$order, $editable=true)
{
	

    div_start('cons_table');
	display_heading("Consumable Part");
	start_table(TABLESTYLE, "colspan=7 width=80%");
	$th = array(_("Type"),_("Select"), _("Unit"), _("Quantity"), "");
	if (count($order->line_con)) $th[] = '';
	table_header($th);


	$id = find_submit('Edit');
	$k = 0; 
	foreach ($order->line_con as $line_con => $con_line)
   	{
		
		//$line_total =	round($asb_line->quantity * $asb_line->price,  user_price_dec());
    	if (!$editable || ($id != $line_con))
		{
    		alt_table_row_color($k);
			$cons_type = $con_line->cons_type;
			$get_cons_type = get_cons_type_name($cons_type);
			$const_type = $get_cons_type['master_name'];
			$cons_select = $con_line->cons_select;
			$get_cons_select = get_cons_select_name($cons_select);
			$const_select = $get_cons_select['consumable_name'];
        	label_cell($const_type);
    		label_cell($const_select);
    		label_cell($con_line->cons_unit);
			label_cell($con_line->cons_quntity);
    		
    	hidden('session_var', '1');
			if ($editable)
			{
					edit_button_cell("Edit$line_con", _("Edit"),
					  _('Edit document line'),"custom_edit");
					delete_button_cell("Delete$line_con", _("Delete"),
						_('Remove line from document'),"custom_edit");
			}
		end_row();
		}
		else
		{
			design_consumable_item_controls($order, $k, $line_con);
		}
		//$total += $line_total;
    }
	
	if ($id==-1 && $editable)
		design_consumable_item_controls($order, $k);
		
	$colspan = count($th)-2;
	if (count($order->line_con))
		$colspan--;
	end_table(1);

    div_end();
	
}




function design_consumable_item_controls(&$order, &$rowcounter, $line_con=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	
	if (($id != -1) && $line_con == $id)
	{
		hidden('line_no', $id);
		$_POST['cons_type'] = $order->line_con[$id]->cons_type;
		$_POST['cons_select'] = $order->line_con[$id]->cons_select;
		$_POST['cons_unit'] = $order->line_con[$id]->cons_unit;
		$_POST['cons_quntity'] = $order->line_con[$id]->cons_quntity;
		

	    $Ajax->activate('cons_table');
	}
	 
	stock_master_list_cells(null, 'cons_type', null, _('----Select---'));
	if($_POST['cons_type'] == '-1'){
		sub_consumable_master_list_cells(null, 'cons_select',null, _('----Select---'));
	}else{
		customer_consumable_list_cells(null, $_POST['cons_type'],'cons_select', null, false, true, true, true);
	}
	
	if($_POST['cons_select'] == '-1')
	{
		unit_list_cells(null, 'cons_unit',null, _('----Select---'));
	}
	else
	{
	 	update_unit_list_cells(null, $_POST['cons_select'], 'cons_unit', null, false, true, true, true);
	}
	text_cells_ex(null, 'cons_quntity', 5, 5);
	//check_cells_ex_design(null, 'cons_iscbm',null, true);
		if ($id != -1)
		{
			button_cell('ConsumeUpdateItem', _("Update"),
					_('Confirm changes'), ICON_UPDATE);
			button_cell('ConsumeCancelItemChanges', _("Cancel"),
					_('Cancel changes'), ICON_CANCEL);
			//set_focus('amount');
		} 
		else 
			submit_cells('ConsumeAddItem', _("Add Consume"), "colspan=2",
				_('Add new line to consumable'), true);
	end_row();
	
}

function display_pr_summary(&$pr, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "width=60%");
    start_row();
    label_cells(_("PR No"), $pr->pr_no, "class='tableheader2'");
	label_cells(_("Location"), $pr->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $pr->work_center_id, "class='tableheader2'");
	label_cells(_("Date"), $pr->pr_date, "class='tableheader2'");
	end_row();

    end_table(1);
}





?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>
