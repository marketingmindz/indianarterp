<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



// costing module - multiple category list
function multiple_categories_list($name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	$sql = "SELECT master_id, master_name FROM ".TB_PREF."master_creation order by master_name ASC";
	$result = db_query($sql);
	echo '<div class="multiselect">
           <div class="selectBox" onclick="showCheckboxes()">';
	echo '<select autocomplete="off" class="combo" title="Select Category" id="consumable_category" >
	  <option value="-1" >- select category - </option>';
	while($row = db_fetch($result))
	{
		$master_id = $row['master_id'];
		$selected = "";
		
		echo '<option value="'.$master_id.'">'.$row['master_name'].'</option>';	
	}
	echo "</select>";
	if(isset($_POST['select_all']))
	{
		$checked = 'checked="checked"';
	}
	echo '<div class="overSelect"></div>
        </div>
        <div id="checkboxes"> <label><input type="checkbox" id="select_all" name="select_all" '.$checked.' /> Select all</label>';
	$result = db_query($sql);
	while($row = db_fetch($result))
	{
		$master_id = $row['master_id'];
		$checked = "";	
		if(in_array($master_id, $_POST['category_id']))
		{
			$checked = "checked";
		}
		
		echo '<label><input type="hidden" id="'.$master_id.'" value="'.$row['master_name'].'" >
		<input type="checkbox" class="check" value="'.$master_id.'" '.$checked.' name="category_id['.$master_id.']" />'.$row['master_name'].'</label>';
	}	

    echo ' </div>
    </div>';
}
function multiple_category_list_cells($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo multiple_categories_list($name, $selected_id, $spec_opt, $submit_on_change);
	echo "</td>\n";
}

function multiple_category_list_row($label, $name, $selected_id=null, $spec_opt=false, $submit_on_change=false, $param)
{
	echo "<tr ".$param."><td class='label'>$label</td>";
	multiple_category_list_cells(null, $name, $selected_id, $spec_opt, $submit_on_change);
	echo "</tr>\n";
}



function display_header(){
	div_start('product', "style=min-height:70px;");
	echo '<h3 id="pro_cat" style="max-width:700px; margin:auto;">Consumable Category :  '.$_POST['pro_categories'].'<br>
			Reference No. : '.$_POST['reference_no'].' <br> Remark : '.$_POST['costing_remark'].'


	</h3>
			<input type="hidden" name="pro_categories" id="pro_categories" value="'.$_POST['pro_categories'].'" >';
	start_table(TABLESTYLE_NOBORDER, "width=500px");
	global $Ajax;
	$Ajax->activate('popup_controls');
	$Ajax->activate('getConsumables');
		
		
		hidden("date",Today());
	end_table();
	
	
		  
	echo "<div id='save_list_popup'>
			<div id='save_list_content'></div><div style='margin:auto; width:90px;'>		
				<span id='close'><a href='saved_costing.php'>YES</a></span><span id='no'>NO</span>
			</div>
			
		  </div>";
		 
	div_end();
		
}

function display_consumable($category)
{
	echo "<div id=''table-head>";
	start_table(TABLESTYLE_NOBORDER);
?>
		<tr><td class='tableheader' width="200">Consumable Category</td><td  class='tableheader' width="200">Consumable Name</td><td  class='tableheader' width="100">Make</td><td  class='tableheader' width="200">Supllier Prices</td><td  class='tableheader' width="130">Set Price</td><td  class='tableheader' width="100">Cost</td><td  class='tableheader' width="60"></td></tr>
<?php
	end_table();
	echo "</div>";


	echo "<div id='consumables'>";
		start_table(TABLESTYLE);
		foreach ($category as $category_id) {
			$cons = get_cons_type_name($category_id);

			$sql = "SELECT consumable_id,consumable_name, company_name FROM ".TB_PREF."consumable_master c left join ".TB_PREF."item_company i on i.company_id = c.company_id  WHERE master_id=".db_escape($category_id);
			$result = db_query($sql, "Could not get Consumables details.");
			while($row = db_fetch($result))
			{
				$consumable_id = $row['consumable_id'];
				$cost = 0;

				$not_available = 0;
				$sql_check = "select * from ".TB_PREF."consumable_cost where category_id = ".db_escape($category_id)." && consumable_id = ".db_escape($consumable_id);
				$check_result = db_query($sql_check, "Could not check Cost.");
				if(db_num_rows($check_result) > 0)
				{
					$disabled = "disabled='disabled'";
					$readonly = "readonly='readonly' style='background-color:#EBEBE4;border:0px;'";
					$hidden = "style='display:none;'";
					$check_cost = db_fetch($check_result);
					$cost = $check_cost['cost'];
				}
				else
				{
					$not_available = 1;
					$disabled = "";	
					$readonly = "";
					$hidden = "";
				}

			if($not_available == 0)
			{
			echo "<tr class='cons_row' id='".$consumable_id."'><td  width='200'><input type='hidden' name='category_id' value='".$category_id."' >".$cons['master_name']."</td>";

			echo "<td width='200'><input type='hidden' name='consumable_id' value='".$consumable_id."' >".$row['consumable_name']."</td>";

			echo "<td width='100'>".$row['company_name']."</td>";

			echo "<td class='supp_price' width='200'>";
			$sql1 = "select purchase_price, supp_name from ".TB_PREF."supp_product sp left join ".TB_PREF."suppliers s on s.supplier_id = sp.supplier_id where catagory = ".db_escape($category_id)." && sub_category = ".db_escape($consumable_id)." ORDER BY convert(`purchase_price`, decimal)DESC";
			$result1 = db_query($sql1, "Could not get purchase price");

			$disable = 0;
			if(db_num_rows($result1) > 0)
			{
				echo "<select name='supp_price' id='supp_price'>";
				while($row1 = db_fetch($result1))
				{
					echo "<option value='".$row1['purchase_price']."'> Rs. ".$row1['purchase_price']." - ".$row1['supp_name']."</option>";
				}
				echo "</select>";
			}
			else
			{
				echo "Not Available";
				$disable = 1;
			}
			echo "</td>";
?>

			<td width="130">
<?php if($disable != 1)
{ ?> 	<form>
			<input type="radio" <?php /* if($disabled == "") { echo 'checked="checked"'; }*/ ?> name="set_price" <?php /*echo $disabled;*/ ?> value="max" />Max 
			<input type="radio" name="set_price" <?php /*echo $disabled;*/ ?> value="avg" />Avg 
			<input type="radio" name="set_price" <?php /*echo $disabled;*/ ?> value="min" />Min 
			<!-- <input type="radio" name="set_price" value="manual">Manual  -->
		</form>
<?php } ?>
			</td>
			<td width="100">
			<?php 
			if($cost == 0)
			{
				$sql1 = "select max(convert(`purchase_price`, decimal)) as max_purchase_price from ".TB_PREF."supp_product where catagory = ".db_escape($category_id)." && sub_category = ".db_escape($consumable_id);
				$result1 = db_query($sql1, "Could not get purchase price");
				if(db_num_rows($result1) > 0)
				{	$row1 = db_fetch($result1);
					$cost = $row1['max_purchase_price'];
				}
			}
			?>
			<input type="text" class="cost_input" name="cost" <?php /*echo $readonly; */?>  value="<?php echo $cost;  ?>"></td>
			<td width="50"><input type="button" class="update_cost" <?php /*echo $hidden;*/ ?> value="Update" name="update_cost"></td>

<?php

			echo "</tr>";
			}
			else
			{
				echo "<tr class='cons_row' id='".$consumable_id."'><td  width='200'><input type='hidden' name='category_id' value='".$category_id."' >".$cons['master_name']."</td>";

				echo "<td width='200'><input type='hidden' name='consumable_id' value='".$consumable_id."' >".$row['consumable_name']."</td>";

				echo "<td width='100'>".$row['company_name']."</td>";
				echo "<td colspan='10'>Cost in not enterd.</td></tr>";
			}	
		}
		}
		end_table();



	echo "</div>";
}




?>
