<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/




function display_header(){
	div_start('product');
	start_table(TABLESTYLE_NOBORDER, "width=400px ");
	global $Ajax;
	//$Ajax->activate('product');
	//$Ajax->activate('GetFinishProduct');
		
		echo "<tr><td height=30>Select search Type</td><td><select style='min-width:150;' name='search_type' id='search_type'><option value='-1'> -- select  -- </option>";
		
			echo "<option value='product_code'>Product Code</option>";
			echo "<option value='product_name'>Product Name</option>";
			echo "<option value='reference_no'>Reference NO.</option>";
			echo "<option value='category'>category</option>";
			echo "<option value='range'>Range</option>";		

				
		echo "</select></td></tr>";
		echo "<tr><td height='30'>Type Search Keyword</td><td><input type='text' onkeyup='autocomplet()' name='keyword' id='keyword'>
					<input type='hidden' id='keyword_id' name='keyword_id' /></td></tr>";
		echo "<tr><td></td><td><ul id='search_result'></ul></td></tr>";


		$Ajax->activate('GetFinishProduct');
		echo "<td colspan='2'>";
		submit_center('GetFinishProduct', _("Submit"), true, '', 'default');
		echo "</td>";
		
	end_table();

	div_end();
	
}

function display_costing_reference_list($keyword, $search_type, $keyword_id)
{

	if($search_type == 'reference_no')
	{
		$sql = "select * from ".TB_PREF."costing_reference where  locked = '1' && ref_id = ".db_escape($keyword_id);
		$result = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result))
		{
			start_table(TABLESTYLE, "style=width:100%;");
			$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version  "),_(" Current Version  "));
			table_header($th);
			$i = 0;
			while($row = db_fetch($result))
			{
				$ref_id = $row['ref_id'];
				$reference_no = $row['reference_no'];
				$remark = $row['remark'];
				$date = sql2date($row['date']);
				$complete = $row['complete'];
				start_row();
					label_cell($date,null,"date");
					label_cell($reference_no,"width='120' height='40'","reference_no");
					label_cell($remark,null,"remark");	
				
					$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
					$result1 = db_query($sql, "could not get costing_reference details.");
					$k = db_fetch($result1);

					echo "<td>";
					for($i = "A"; $i <= $k['version']; $i++)
					{
						echo '<a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$i.'" >'.$i."</a>";
						$version = $i;
					}
					echo '</td>';

					echo '<td><a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >'.$row['current_version']."</a>";

					echo "</td>";
$curr_sql = "select current_version from ".TB_PREF."costing_reference where ref_id = ".db_escape($ref_id);
					$i++;
				end_row();
			}end_table();
			
				
		}
		else
		{
			display_notification("Costing is not entered.");
		}

	}





	if($search_type == 'product_code')
	{
		$sql = "select *, f.finish_pro_id from ".TB_PREF."locked_final_costing l LEFT JOIN ".TB_PREF."finish_product f on 
				f.finish_pro_id = l.finish_code_id LEFT JOIN ".TB_PREF."costing_reference c on c.ref_id = l.ref_id  
				where c.locked = '1' && f.finish_pro_id = ".db_escape($keyword_id)." group by c.ref_id";

		$result = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result))
		{
			start_table(TABLESTYLE, "style=width:100%;");
			$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version "),_(" Reference Version  "));
			table_header($th);
			$i = 0;
			while($row = db_fetch($result))
			{
				//print_r($row); break;
				$ref_id = $row['ref_id'];
				$reference_no = $row['reference_no'];
				$remark = $row['remark'];
				$date = sql2date($row['date']);
				$complete = $row['complete'];
				start_row();
					label_cell($date,null,"date");
					label_cell($reference_no,"width='120' height='40'","reference_no");
					label_cell($remark,null,"remark");	


					$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
					$result1 = db_query($sql, "could not get costing_reference details.");
					$k = db_fetch($result1);

					echo "<td>";
					for($i = "A"; $i <= $k['version']; $i++)
					{
						echo '<a class="button" href="view_product_price.php?finish_code_id='.$row['finish_pro_id'].'&version='.$i.'" >'.$i."</a>";
						$version = $i;
					}
					echo "</td>";
				
					echo '<td><a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >'.$row['current_version']."</a>";

					echo "</td>";

					$i++;
				end_row();
			}end_table();
			
				
		}
		else
		{
			display_notification("Costing is not entered.");
		}

	}

	if($search_type == 'product_name')
	{
		$sql = "select *,f.finish_pro_id from ".TB_PREF."locked_final_costing l LEFT JOIN ".TB_PREF."finish_product f on 
				f.finish_pro_id = l.finish_code_id LEFT JOIN ".TB_PREF."costing_reference c on c.ref_id = l.ref_id  
				where c.locked = '1' && f.finish_pro_id = ".db_escape($keyword_id)." group by c.ref_id";

		$result = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result))
		{
			start_table(TABLESTYLE, "style=width:100%;");
			$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version "),_(" Reference Version  "));
			table_header($th);
			$i = 0;
			while($row = db_fetch($result))
			{
				//print_r($row); break;
				$ref_id = $row['ref_id'];
				$reference_no = $row['reference_no'];
				$remark = $row['remark'];
				$date = sql2date($row['date']);
				$complete = $row['complete'];
				start_row();
					label_cell($date,null,"date");
					label_cell($reference_no,"width='120' height='40'","reference_no");
					label_cell($remark,null,"remark");	


					$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
					$result1 = db_query($sql, "could not get costing_reference details.");
					$k = db_fetch($result1);

					echo "<td>";
					for($i = "A"; $i <= $k['version']; $i++)
					{
						echo '<a class="button" href="view_product_price.php?finish_code_id='.$row['finish_pro_id'].'&version='.$i.'" >'.$i."</a>";
						$version = $i;
					}
					echo "</td>";
				
					echo '<td><a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >'.$row['current_version']."</a>";

					echo "</td>";

					$i++;
				end_row();
			}end_table();
			
				
		}
		else
		{
			display_notification("Costing is not entered.");
		}

	}


	if($search_type == 'range')
	{
		$sql = "select * from ".TB_PREF."costing_reference_details d LEFT JOIN ".TB_PREF."costing_reference c on 
				c.ref_id = d.ref_id where c.locked = '1' && d.range_id = ".db_escape($keyword_id)." group by d.ref_id";

		$result = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result))
		{
			start_table(TABLESTYLE, "style=width:100%;");
			$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version "),_(" Reference Version  "));
			table_header($th);
			$i = 0;
			while($row = db_fetch($result))
			{
				//print_r($row); break;
				$ref_id = $row['ref_id'];
				$reference_no = $row['reference_no'];
				$remark = $row['remark'];
				$date = sql2date($row['date']);
				$complete = $row['complete'];
				start_row();
					label_cell($date,null,"date");
					label_cell($reference_no,"width='120' height='40'","reference_no");
					label_cell($remark,null,"remark");	


					$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
					$result1 = db_query($sql, "could not get costing_reference details.");
					$k = db_fetch($result1);

					echo "<td>";
					for($i = "A"; $i <= $k['version']; $i++)
					{
						echo '<a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$i.'" >'.$i."</a>";
						$version = $i;
					}
					echo "</td>";
				
					echo '<td><a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >'.$row['current_version']."</a>";

					echo "</td>";

					$i++;
				end_row();
			}end_table();
			
				
		}
		else
		{
			display_notification("Costing is not entered.");
		}
	}


	if($search_type == 'category')
	{
		$sql = "select * from ".TB_PREF."costing_reference_details d LEFT JOIN ".TB_PREF."costing_reference c on 
				c.ref_id = d.ref_id where c.locked = '1' && d.category_id = ".db_escape($keyword_id)." group by d.ref_id";

		$result = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result))
		{
			start_table(TABLESTYLE, "style=width:100%;");
			$th = array(_("Date "),_("Costing Reference "),_("Remark "),_(" Reference Version "),_(" Reference Version  "));
			table_header($th);
			$i = 0;
			while($row = db_fetch($result))
			{
				//print_r($row); break;
				$ref_id = $row['ref_id'];
				$reference_no = $row['reference_no'];
				$remark = $row['remark'];
				$date = sql2date($row['date']);
				$complete = $row['complete'];
				start_row();
					label_cell($date,null,"date");
					label_cell($reference_no,"width='120' height='40'","reference_no");
					label_cell($remark,null,"remark");	


					$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
					$result1 = db_query($sql, "could not get costing_reference details.");
					$k = db_fetch($result1);

					echo "<td>";
					for($i = "A"; $i <= $k['version']; $i++)
					{
						echo '<a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$i.'" >'.$i."</a>";
						$version = $i;
					}
					echo "</td>";
				
				echo '<td><a class="button" href="view_reference_no_price.php?reference_id='.$ref_id.'&version='.$row['current_version'].'" >'.$row['current_version']."</a>";

					echo "</td>";	

					$i++;
				end_row();
			}end_table();
			
				
		}
		else
		{
			display_notification("Costing is not entered.");
		}
	}



}





?>
