<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


function display_locked_final_costing_form($ref_id, $ref_version, &$order, $editable=true)
{
	if(!isset($_POST['final_profit_percent']))
		$_POST['final_profit_percent'] = 0;
	if(!isset($_POST['final_profit']))
		$_POST['final_profit'] = 0;
	if(!isset($_POST['total_percent']))
		$_POST['total_percent'] = 0;
	if(!isset($_POST['total_export_cost']))
		$_POST['total_export_cost'] = 0;
	if(!isset($_POST['total_fob_cost']))
		$_POST['total_fob_cost'] = 0;
	/*if(!isset($_POST['finishing_exp_percent']))
		$_POST['finishing_exp_percent'] = 0;
	if(!isset($_POST['final_finishing_cost']))
		$_POST['final_finishing_cost'] = 0;*/
	if(!isset($_POST['total_cost']))
		$_POST['total_cost'] = 0;
	if(!isset($_POST['inr_price']))
		$_POST['inr_price'] = 0;	
	if(!isset($_POST['gbp_price']))
		$_POST['gbp_price'] = 0;	
	if(!isset($_POST['usd_price']))
		$_POST['usd_price'] = 0;
	if(!isset($_POST['euro_price']))
		$_POST['euro_price'] = 0;
	
	div_start('final_table');

	//------------------------------------ Final Costing --------------------------------

	display_heading("Final Costing");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");
		
		$th = array(_(" "),_(" "),_(" "));
		table_header($th);
		
		// get total manufacturing cost
		$_POST['total_mfrg_cost'] = get_total_locked_mfrg_cost($_POST['final_finish_code_id'], $ref_id, $ref_version);

		// get all finishing step data
		$sql = "SELECT * from ".TB_PREF."locked_finishing_cost where finish_code_id =".$_POST['final_finish_code_id']." && ref_id = '".$ref_id."' && version = '".$ref_version."' ";
		$result = db_query($sql, "could not get total percent");
		$finishing_data = db_fetch($result);

		// get total finishing percent
		$_POST['finishing_total_percent'] = $finishing_data['sanding_percent'] + $finishing_data['polish_percent'] + $finishing_data['packaging_percent'] + $finishing_data['lumsum_cost_percent'] + $finishing_data['forwarding_percent'];

		$total_process_cost = $finishing_data['sanding_cost'] + $finishing_data['polish_cost'] + $finishing_data['packaging_cost'];
		$total_actual_cost = $finishing_data['actual_sending_cost'] + $finishing_data['actual_polish_cost'] + $finishing_data['actual_packaging_cost'];
		$total_lumsum_cost = $finishing_data['lumsum_cost_value'];

		$average_process_cost = ($total_process_cost + $total_actual_cost + $total_lumsum_cost)/3;
		$_POST['average_process_cost'] = number_format((float)$average_process_cost, 2, '.', '');

		$total_other_than_process_cost = $finishing_data['forwarding_cost'] + $finishing_data['total_special_cost'] + $finishing_data['other_cost'];
		$_POST['total_other_than_process_cost'] = number_format((float)$total_other_than_process_cost, 2, '.', '');

		// get total finishing cost
		$total_finishing_cost = $average_process_cost + $total_other_than_process_cost;
		$_POST['total_finishing_cost'] = number_format((float)$total_finishing_cost, 2, '.', '');

		// get total cost
		$_POST['total_cost'] = $_POST['total_mfrg_cost'] + $_POST['total_finishing_cost'];

		// get total final cost
		$_POST['final_total_cost'] = $_POST['total_cost'] + $_POST['final_profit'];


	start_row();
		echo "<td><table width=100%>";
		echo "<tr><td  class='tableheader' colspan=2>SP By Process</td></tr>";
		
		start_row();
		    label_cell($total_process_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>Actual Costing</td></tr>";
		start_row();
		    label_cell($total_actual_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>SP By Lumsum</td></tr>";
		start_row();
		    label_cell($total_lumsum_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";
 	end_row();
 	
 	end_table();

 	//---------------------------------- Total Costing -----------------------------------

 	start_table(TABLESTYLE, "colspan=7 style='width:100%; border:10px solid #7895AC;' ");

	start_row();
		label_cell("Total MFRG Cost : ","width=274");
     	label_cell($_POST['total_mfrg_cost'],null,"total_mfrg_cost");
     	echo '<input type="hidden" name="total_mfrg_cost" value="'.$_POST['total_mfrg_cost'].'" >';
	end_row();

	start_row();
		label_cell("Total Other Than Process Cost : ","width=274");
     	label_cell($_POST['total_other_than_process_cost'],null,"total_other_than_process_cost");
     	echo '<input type="hidden" name="total_other_than_process_cost" value="'.$_POST['total_other_than_process_cost'].'" >';
	end_row();

	start_row();
	    label_cell("Total Finishing Cost : ","");
     	label_cell($_POST['total_finishing_cost'], null, "total_final_finishing_cost");
     	echo '<input type="hidden" name="total_finishing_cost" value="'.$_POST['total_finishing_cost'].'" >';
     	echo '<input type="hidden" name="total_percent" value="'.$_POST['finishing_total_percent'].'" >';
	end_row();

	start_row();
	    label_cell("Total Costs : ","");
     	text_cells(null, 'total_cost', $_POST['total_cost'], 10, 10, null, null, null,"id=total_cost");
	end_row();
	
	start_row();
	label_cell("Profit % : ","");
     	text_cells(null, 'final_profit_percent', $_POST['final_profit_percent'], 10, 10, null, null, null,"id=final_profit_percent");
	end_row();

	start_row();
	    label_cell("Profit : ","");
		text_cells(null, 'final_profit', $_POST['final_profit'], 10, 10, null, null, null,"id=final_profit");
	end_row();

	start_row();
	    label_cell("Total Final Costs : ","");
     	text_cells(null, 'final_total_cost', $_POST['final_total_cost'], 10, 10, null, null, null,"id=final_total_cost");	
	end_row();

	$sql = "SELECT * from ".TB_PREF."currency_master";
	$result = db_query($sql, "Could not get currencies.");
	$currencies = array();
	$total_currency = 0;

	while($row = db_fetch($result))
	{
		$id = $row['id'];
		$currencies[] = $id;
		start_row();
	    label_cell("Price in: ".$row['currency_name'],"colspan=	 align=right");
     	text_cells(null, "currency".$id, $_POST['final_total_cost']*$row['rate'], 10, 10, null, null, null,"id=currency".$id);	
		end_row();
		$total_currency++;
	}
	$currency = implode(",",$currencies);
	echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
	echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	
	end_table();   

    div_end();
}


function display_locked_final_costing_edit_form($ref_id, $ref_version, &$order, $editable=true)
{
	if(!isset($_POST['final_profit_percent']))
		$_POST['final_profit_percent'] = 0;
	if(!isset($_POST['final_profit']))
		$_POST['final_profit'] = 0;
	if(!isset($_POST['total_percent']))
		$_POST['total_percent'] = 0;
	if(!isset($_POST['total_export_cost']))
		$_POST['total_export_cost'] = 0;
	if(!isset($_POST['total_fob_cost']))
		$_POST['total_fob_cost'] = 0;
	if(!isset($_POST['total_cost']))
		$_POST['total_cost'] = 0;
	if(!isset($_POST['inr_price']))
		$_POST['inr_price'] = 0;	
	if(!isset($_POST['gbp_price']))
		$_POST['gbp_price'] = 0;	
	if(!isset($_POST['usd_price']))
		$_POST['usd_price'] = 0;
	if(!isset($_POST['euro_price']))
		$_POST['euro_price'] = 0;	
	
	div_start('final_table');

	//------------------------------------ Final Costing --------------------------------

	display_heading("Final Costing");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");
		
		$th = array(_(" "),_(" "),_(" "));
		table_header($th);
		
		// get total manufacturing cost
		$_POST['total_mfrg_cost'] = get_total_locked_mfrg_cost($_POST['final_finish_code_id'], $ref_id, $ref_version);

		// get all finishing step data
		$sql = "SELECT * from ".TB_PREF."locked_finishing_cost where finish_code_id =".$_POST['final_finish_code_id']." && ref_id = '".$ref_id."' && version = '".$ref_version."' ";
		$result = db_query($sql, "could not get total percent");
		$finishing_data = db_fetch($result);

		// get total finishing percent
		$_POST['finishing_total_percent'] = $finishing_data['sanding_percent'] + $finishing_data['polish_percent'] + $finishing_data['packaging_percent'] + $finishing_data['lumsum_cost_percent'] + $finishing_data['forwarding_percent'];

		$total_process_cost = $finishing_data['sanding_cost'] + $finishing_data['polish_cost'] + $finishing_data['packaging_cost'];
		$total_actual_cost = $finishing_data['actual_sending_cost'] + $finishing_data['actual_polish_cost'] + $finishing_data['actual_packaging_cost'];
		$total_lumsum_cost = $finishing_data['lumsum_cost_value'];

		$average_process_cost = ($total_process_cost + $total_actual_cost + $total_lumsum_cost)/3;
		$_POST['average_process_cost'] = number_format((float)$average_process_cost, 2, '.', '');

		$total_other_than_process_cost = $finishing_data['forwarding_cost'] + $finishing_data['total_special_cost'] + $finishing_data['other_cost'];
		$_POST['total_other_than_process_cost'] = number_format((float)$total_other_than_process_cost, 2, '.', '');

		// get total finishing cost
		$total_finishing_cost = $average_process_cost + $total_other_than_process_cost;
		$_POST['total_finishing_cost'] = number_format((float)$total_finishing_cost, 2, '.', '');

		// get total cost
		$_POST['total_cost'] = $_POST['total_mfrg_cost'] + $_POST['total_finishing_cost'];

		// get total final cost
		$_POST['final_total_cost'] = $_POST['total_cost'] + $_POST['final_profit'];

		// get final cost step data
		$sql1 = "SELECT * FROM ".TB_PREF."locked_final_costing WHERE finish_code_id=".db_escape($_POST['final_finish_code_id'])." && ref_id = '".$ref_id."' && version = '".$ref_version."' ";
		$result1 = db_query($sql1, "could not get total percent");
		$final_cost_data = db_fetch($result1);

		$_POST['final_profit_percent'] = $final_cost_data['final_profit_percent'];
		$_POST['final_profit'] = $final_cost_data['final_profit'];
		$_POST['final_total_cost'] = $final_cost_data['final_total_cost'];


	start_row();
		echo "<td><table width=100%>";
		echo "<tr><td  class='tableheader' colspan=2>SP By Process</td></tr>";
		
		start_row();
		    label_cell($total_process_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>Actual Costing</td></tr>";
		start_row();
		    label_cell($total_actual_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>SP By Lumsum</td></tr>";
		start_row();
		    label_cell($total_lumsum_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";
 	end_row();
 	
 	end_table();

 	//---------------------------------- Total Costing -----------------------------------

 	start_table(TABLESTYLE, "colspan=7 style='width:100%; border:10px solid #7895AC;' ");

	start_row();
		label_cell("Total MFRG Cost : ","width=274");
     	label_cell($_POST['total_mfrg_cost'],null,"total_mfrg_cost");
     	echo '<input type="hidden" name="total_mfrg_cost" value="'.$_POST['total_mfrg_cost'].'" >';
	end_row();

	start_row();
		label_cell("Total Other Than Process Cost : ","width=274");
     	label_cell($_POST['total_other_than_process_cost'],null,"total_other_than_process_cost");
     	echo '<input type="hidden" name="total_other_than_process_cost" value="'.$_POST['total_other_than_process_cost'].'" >';
	end_row();

	start_row();
	    label_cell("Total Finishing Cost : ","");
     	label_cell($_POST['total_finishing_cost'], null, "total_final_finishing_cost");
     	echo '<input type="hidden" name="total_finishing_cost" value="'.$_POST['total_finishing_cost'].'" >';
     	echo '<input type="hidden" name="total_percent" value="'.$_POST['finishing_total_percent'].'" >';
	end_row();

	start_row();
	    label_cell("Total Costs : ","");
     	text_cells(null, 'total_cost', $_POST['total_cost'], 10, 10, null, null, null,"id=total_cost");
	end_row();
	
	start_row();
	label_cell("Profit % : ","");
     	text_cells(null, 'final_profit_percent', $_POST['final_profit_percent'], 10, 10, null, null, null,"id=final_profit_percent");
	end_row();

	start_row();
	    label_cell("Profit : ","");
		text_cells(null, 'final_profit', $_POST['final_profit'], 10, 10, null, null, null,"id=final_profit");
	end_row();

	start_row();
	    label_cell("Total Final Costs : ","");
     	text_cells(null, 'final_total_cost', $_POST['final_total_cost'], 10, 10, null, null, null,"id=final_total_cost");	
	end_row();

	$sql = "SELECT * from ".TB_PREF."currency_master";
	$result = db_query($sql, "Could not get currencies.");
	$currencies = array();
	$total_currency = 0;

	while($row = db_fetch($result))
	{
		$id = $row['id'];
		$currencies[] = $id;
		start_row();
	    label_cell("Price in: ".$row['currency_name'],"colspan=	 align=right");
     	text_cells(null, "currency".$id, $_POST['final_total_cost']*$row['rate'], 10, 10, null, null, null,"id=currency".$id);	
		end_row();
		$total_currency++;
	}
	$currency = implode(",",$currencies);
	echo '<input type="hidden" name="currencies" value="'.$currency.'" >';
	echo '<input type="hidden" name="total_currency" value="'.$total_currency.'" >';
	
	end_table();   

    div_end();
}


function unset_final_cost_variables()
{
	unset($_POST['final_profit_percent']);
	unset($_POST['final_profit']);
	unset($_POST['total_percent']);
	unset($_POST['total_export_cost']);
	unset($_POST['total_fob_cost']);
	unset($_POST['finishing_exp_percent']);
	unset($_POST['final_finishing_cost']);
	unset($_POST['sp_by_process']);
	unset($_POST['sp_by_lumsum']);
	unset($_POST['final_total_cost']);
	unset($_POST['price_in_inr']);
}


function display_locked_final_costing($finish_code_id, $ref_id, $ref_version)
{
	div_start('final_table');

	//------------------------------------ Final Costing --------------------------------

	display_heading("Final Costing");
	start_table(TABLESTYLE, "colspan=7 style='width:100%;'");
		
		$th = array(_(" "),_(" "),_(" "));
		table_header($th);
		
		// get total locked manufacturing cost
		$total_mfrg_cost = get_total_locked_mfrg_cost($finish_code_id, $ref_id, $ref_version);

		// get all finishing step data
		$sql = "SELECT * from ".TB_PREF."locked_finishing_cost where finish_code_id =".$finish_code_id." && ref_id = '".$ref_id."' && version = '".$ref_version."' ";
		$result = db_query($sql, "could not get total percent");
		$finishing_data = db_fetch($result);

		// get total finishing percent
		$_POST['total_finishing_percent'] = $finishing_data['sanding_percent'] + $finishing_data['polish_percent'] + $finishing_data['packaging_percent'] + $finishing_data['lumsum_cost_percent'] + $finishing_data['forwarding_percent'];

		$total_process_cost = $finishing_data['sanding_cost'] + $finishing_data['polish_cost'] + $finishing_data['packaging_cost'];
		$total_actual_cost = $finishing_data['actual_sending_cost'] + $finishing_data['actual_polish_cost'] + $finishing_data['actual_packaging_cost'];
		$total_lumsum_cost = $finishing_data['lumsum_cost_value'];

		$average_process_cost = ($total_process_cost + $total_actual_cost + $total_lumsum_cost )/3;
		$average_process_cost = number_format((float)$average_process_cost, 2, '.', '');

		$total_other_than_process_cost = $finishing_data['forwarding_cost'] + $finishing_data['other_cost'] + $finishing_data['total_special_cost'];
		$total_other_than_process_cost = number_format((float)$total_other_than_process_cost, 2, '.', '');

		// get total finishing cost
		$total_finishing_cost = $average_process_cost + $total_other_than_process_cost;

		// get total cost
		$total_cost = $total_mfrg_cost + $total_finishing_cost;

		// get final cost step data
		$sql1 = "SELECT * FROM ".TB_PREF."locked_final_costing WHERE finish_code_id=".db_escape($finish_code_id)." && ref_id = '".$ref_id."' && version = '".$ref_version."' ";
		$result1 = db_query($sql1, "could not get total percent");
		$final_cost_data = db_fetch($result1);

	start_row();
		echo "<td><table width=100%>";
		echo "<tr><td  class='tableheader' colspan=2>SP By Process</td></tr>";
		
		start_row();
		    label_cell($total_process_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>Actual Costing</td></tr>";
		start_row();
		    label_cell($total_actual_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";

		echo "<td><table width=100%>";
		echo "<tr><td   class='tableheader' colspan=2>SP By Lumsum</td></tr>";
		start_row();
		    label_cell($total_lumsum_cost, "align=center", null, "total_amount_label");
		end_row();

		echo "</table></td>";
 	end_row();
 	
 	end_table();

 	//---------------------------------- Total Costing -----------------------------------

 	start_table(TABLESTYLE, "colspan=7 style='width:100%; border:10px solid #7895AC;' ");

	start_row();
		label_cell("Total MFRG Cost : ","width=274");
     	label_cell($total_mfrg_cost,null,"total_mfrg_cost");
	end_row();

	start_row();
	    label_cell("Total Other Than Process Cost : ","");
     	label_cell($total_other_than_process_cost, null, "total_finishing_cost");		
	end_row();

	start_row();
	    label_cell("Total Finishing Cost : ","");
     	label_cell($total_finishing_cost, null, "total_finishing_cost");		
	end_row();

	start_row();
	    label_cell("Total Costs : ","");
     	label_cell($total_cost, null,"total_cost");
	end_row();
	
	start_row();
		label_cell("Profit % : ","");
     	label_cell($final_cost_data['final_profit_percent'], null,"final_profit_percent");	
	end_row();

	start_row();
	    label_cell("Profit : ","");
		label_cell($final_cost_data['final_profit'], null,"final_profit");
	end_row();

	start_row();
	    label_cell("Total Final Costs : ","style='font-weight:bold;' ");
     	label_cell($final_cost_data['final_total_cost'], "style='font-weight:bold;' ", null,"final_total_cost");	
	end_row();

	$sql2 = "SELECT * from ".TB_PREF."currency_master";
	$result2 = db_query($sql2, "Could not get currencies.");
	$currencies = array();
	$total_currency = 0;

	while($row2 = db_fetch($result2))
	{
		start_row();
	    label_cell("Price in: ".$row2['currency_name'],"colspan=	 align=right");
     	label_cell($final_cost_data['final_total_cost']*$row2['rate'], null,"total_mfrg_cost");	
		end_row();
		$total_currency++;
	}
	
	end_table();   

    div_end();
}

?>
