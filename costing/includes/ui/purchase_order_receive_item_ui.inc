<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/


//---------------------------------------------------------------------------------------------------

function display_purchase_order_header(&$order)
{
	global 	$Ajax;

  $Ajax->activate('cons_table');
div_start('cons_table');
	start_outer_table(TABLESTYLE2, 'width=80%');

	table_section(1);
    if(isset($_GET['PONumber']))
	{
		$purchase_order_id = $_GET['PONumber'];
		$result = get_order_details($purchase_order_id);
	}
	text_row(_("PO No:"), 'po_no', null, 30, 30, '','','','id="po_no" readonly="true"');
	hidden("supplier_id", $_POST['supplier_id']);
	text_row(_("Supplier:"), 'supp_name', null, 30, 30, '','','','id="supp_name" readonly="true"');
	date_row(_("Order Date:"), 'po_date', null, 30, 30, '','','','id="po_date" readonly="true"');
	//date_row(_("Date") . ":", 'po_date');


	table_section(2);
	hidden("location_id", $_POST['location_id']);
	text_row(_("Location Name:"), 'location_id_0', null, 30, 30, '','','','id="location_id" readonly="true"');
	
	hidden("work_center_id", $_POST['work_center_id']);
	text_row(_("Work Center Name:"), 'work_center_id_0', null, 30, 30, '','','','id="work_center_id" readonly="true"');
	
	date_row(_("Recieving Date") . ":", 'receive_date');


	end_outer_table(); // outer table
}

//---------------------------------------------------------------------------------------------------
function received_qty($i){
	echo "<td><input type='text' size='10' value='' name='received_quantity_".$i."' id='received_quantity_".$i."' class='received_quantity' ></td>";
}
function select_supplier_list($consumable_id, $consumable_category, $i){
	$result = get_suppliers($consumable_id, $consumable_category);
	$supplier = "";
	echo "<td><select name='supp_id_".$i."'><option value='-1'>Select Supplier</option>";
	while($myrow = db_fetch($result)){
		$supplier_id = $myrow['supplier_id'];
		echo $supplier_id;
		$supplier = $myrow['supp_name']." - ".$myrow['purchase_price']." - ".$myrow['lead_time'];
	    echo "<option value='".$supplier_id."'>".$supplier."</option>";
	}
	echo "</select></td>";

}

function display_order_items()
{
	if(isset($_GET['PONumber']))
	{
		$purchase_order_id = $_GET['PONumber'];
		$result = get_purchase_order_item_details($purchase_order_id);
	}

	start_table(TABLESTYLE2, 'width=60%');
	$th = array(_("Consumable Name"), _("Consumable Category"), _("Unit"), _("Ordered Qty"), _("Received Qty"), _("OK Quantity"), _("Rejected Qty"), _("Chalan No"), _("Invoice No"));
	table_header($th);
	$k = 0;
	$i=0;
	while($myrow = db_fetch($result)){
		$consumable_id = $myrow["consumable_id"];
		$consumable_category = $myrow["consumable_category"];
		$_POST['consumable_id_'.$i] = $_POST['cons_id'] = $myrow["master_name"];
		$_POST['consumable_category_'.$i] = $_POST['consumable_cat'] = $myrow["consumable_name"];
		$_POST['unit_'.$i] = $_POST['unit'] = $myrow["unit"];
		$_POST['ordered_quantity_'.$i] = $_POST['ordered_quantity_'] = $myrow["approved_quantity"];
		
		alt_table_row_color($k);
		
		    hidden("consumable_id_".$i, $consumable_id);
			text_cells_ex(null, 'cons_id', 30, 30, null,'','','', '','id="consumable_id" readonly="true"');
			
			hidden("consumable_category_".$i, $consumable_category);
			text_cells_ex(null, 'consumable_cat', 30, 30,null,'','','', '','id="consumable_category" readonly="true"');
			
			hidden("unit_".$i, $_POST['unit']);
			text_cells_ex(null, 'unit_'.$i, 10, 10,null,'','','', '','id="cons_unit" readonly="true"');
			
			hidden("ordered_quantity_".$i, $_POST['ordered_quantity_'.$i]);
			text_cells_ex(null, 'ordered_quantity_', 10, 10,null,'','','', '','id="ordered_quantity_'.$i.'"  readonly="true"');
			
			//select_supplier_list($consumable_id, $consumable_category, $i);
			received_qty($i);
			text_cells_ex(null, 'ok_quantity_'.$i, 10, 10,null,'','','', '','id="ok_quantity_'.$i.'" class="ok_quantity"');
			text_cells_ex(null, 'rejected_quantity_'.$i, 10, 10,null,'','','', '','id="rejected_quantity_'.$i.'"');
			text_cells_ex(null, 'chalan_no_'.$i, 10, 10,null,'','','', '','id="chalan_no_'.$i.'"');
			text_cells_ex(null, 'invoice_no_'.$i, 10, 10,null,'','','', '','id="invoice_no_'.$i.'"');

		end_row();
		
		hidden("count", $i);
		$i++;
	}
	
	end_table(); // outer table
}

//---------------------------------------------------------------------------------------------------

function display_purchase_order_summary(&$po, $is_self=false, $editable=false)
{
    start_table(TABLESTYLE, "width=50%");
    start_row();
    label_cells(_("Purchase Request No"), $po->po_no, "class='tableheader2'");
	label_cells(_("Date"), $po->po_date, "class='tableheader2'");
	end_row();
	start_row();
	label_cells(_("Location"), $po->location_id, "class='tableheader2'");
	label_cells(_("Work Center"), $po->work_center_id, "class='tableheader2'");
	end_row();
	start_row();
	label_cells(_("Supplier"), $po->supplier_id, "class='tableheader2'");
	end_row();

    end_table(1);
}



?>
<script src="../js/jquery/jquery-1.11.3.min.js"></script>
  <script src="../js/jquery/jquery-ui.min.js"></script>


<script type="text/javascript">
	$(document).on('change','#slc_master',function(){
	//alert("data is synchronization successfully .... ");
		var slc_master = $(this).val();
		$.ajax({
			url: "manage/sub_master_calling.php",
			method: "POST",
            data: { id : slc_master},
			success: function(data){
					var select_val = $('#sub_master');
                    select_val.empty().append(data);
				}
		});
		return false;
	});
</script>
