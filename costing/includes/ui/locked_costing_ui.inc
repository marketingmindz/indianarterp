<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/



function display_header(){
	div_start('product');
	start_table(TABLESTYLE_NOBORDER, "width=400px");
	global $Ajax;
	$Ajax->activate('product');
	//$Ajax->activate('GetFinishProduct');
	$sql = "select * from ".TB_PREF."costing_reference where locked = '1'";
	$result = db_query($sql, "could not get costing_reference details.");
	if(db_num_rows($result))
	{
		
		$i = 0;
		echo "<tr><td class='tableheader'>Reference No. </td>";
		echo "<td><select name='ref_id'><option value='-1'>-Select Reference NO.-</option>";
		while($row = db_fetch($result))
		{
			$ref_id = $row['ref_id'];
			//$freeze_date = sql2date($row['freeze_date']);
			echo "<option value='".$ref_id."' ";
			if($ref_id == $_POST['ref_id'])
			{
				echo "selected ";
			}
			
			echo ">".$row['reference_no']."</option>";
			
				//label_cell($freeze_date,null,"freeze_date");
				//label_cell($row['reference_no'],"width='120' height='40'","reference_no");
				//label_cell($row['remark'],null,"remark");	
			
				//echo '<td colspan="2" width="200"><center>';
				
				//echo '<a class="button" href="view.php?reference_id='.$ref_id.'" >View</a> <a class="button" href="edit.php?reference_id='.$ref_id.'" >Edit</a></center></td>';      
			
				$i++;
			
		}
		echo "</select></td></tr>";

		end_table();
	}
	submit_center('GetFinishProduct', _("Submit"), true, '', 'default');
	div_end();
	
}

function display_costing_reference()
{
	echo "<div id='popup'>
			<div id='save_list_content'>
			<br>
			<br>
			<center><h2>Are sure to create new veersion?</h1></center>
			
			</div>
			<div style='margin:50px auto; width:150px;'>		
				<span id='close'><a id='redirect_url' href=''>YES</a></span><span id='no'>NO</span>
			</div>
			
		  </div>";
	echo "<div style='min-height:400px;' class='mm-link-btn'> ";
	$sql = "select * from ".TB_PREF."costing_reference where locked = '1' order by locked_date DESC ";
	$result = db_query($sql, "could not get costing_reference details.");
	
	start_table(TABLESTYLE);
	$th = array(_("Locked Date"), _("Reference NO."), _("Remark"),_("Version"), _());
	table_header($th);
	while($row = db_fetch($result))
	{
		$ref_id = $row['ref_id'];
		$sql = "select * from ".TB_PREF."reference_version_seq where ref_id = ".db_escape($ref_id);
		$result1 = db_query($sql, "could not get costing_reference details.");
		if(db_num_rows($result1) > 0)
		{
			$k = db_fetch($result1);
			
			
			start_row();
				label_cell(sql2date($row['locked_date']), 'height="40"');
				
				label_cell($row['reference_no']);
				label_cell($row['remark'], "width=300");
				echo "<td>";
				for($i = "A"; $i <= $k['version']; $i++)
				{
					echo '<a class="button viewLinkButton" href="view_locked_cost.php?reference_id='.$ref_id.'&version='.$i.'" >'.$i."</a>";
					$version = $i;
				}
				echo "</td>";
				echo '<td colspan="2" width="250"><center>';
				
				$sql = "select locked from ".TB_PREF."locked_final_costing where ref_id = ".db_escape($ref_id)."&& version = ".db_escape($version);
				$result2 = db_query($sql,"Could not check locked status!!!");
				$locked = 0;
				while($row = db_fetch($result2))
				{
					$locked = $row['locked'];
				}
				if($locked == 1)
				{
					echo '<a class="button create_version" href="#popup" url="create_new_version.php?reference_id='.$ref_id.'&version='.$version.'" >Create New Version</a><a class="button editLinkButton" href="re_enter_costing.php?reference_id='.$ref_id.'&version='.$version.'" >Re-Enter</a></center></td>';      

				}
				else
				{
					echo '<a class="button editLinkButton" href="edit_locked_cost.php?reference_id='.$ref_id.'&version='.$version.'" >Edit '.$version.' version </a><a class="button editLinkButton" href="re_enter_costing.php?reference_id='.$ref_id.'&version='.$version.'" >Re-Enter</a></center></td>'; 
				}
							
			end_row();
			
			
		}
	}
	end_table();
	echo "</div>";
}


?>
