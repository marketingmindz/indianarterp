<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/* Definition of the purch_order class to hold all the information for a purchase order and delivery
*/

class labour 
{

	var $trans_type; // order/grn/invoice (direct)
	var $line_items;
	var $labour_type;
	var $special_rate;
	var $labour_amount;
	var $special_labour_cost;
	
	var $unit;
	var $quantity;
	var $Location;
	var $pr_no;
	var $pr_date;
	var $location_id;
	var $supplier_id;
	var $order_no; /*Only used for modification of existing orders otherwise only established when order committed */
	var $lines_on_order = 100;
	
	function labour()
	{
		/*Constructor function initialises a new purchase order object */
		$this->line_items = array();
		$this->lines_on_order = $this->order_no = 100;
	}
	
	function add_to_labour_part($line_no,  $labour_type,  $special_rate,$labour_amount, $special_labour_cost)
	{
		$line_no += 100; 
		$this->line_items[$line_no] = new labour_line_details($line_no,$labour_type,$special_rate, $labour_amount, $special_labour_cost);
		$this->lines_on_order++;
		return 1;
	}
	function update_labour_item($line_no,   $labour_type,  $special_rate,$labour_amount, $special_labour_cost)
	{

		$this->line_items[$line_no]->labour_type = $labour_type;
		$this->line_items[$line_no]->special_rate = $special_rate;
		$this->line_items[$line_no]->labour_amount = $labour_amount;
		$this->line_items[$line_no]->special_labour_cost = $special_labour_cost;
	}

	function remove_from_labour($line_no)
	{
		array_splice($this->line_items, $line_no, 1);
	}
	
	function labour_has_items() 
	{
		return count($this->line_items) != 0;
	}
	
	function clear_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		$this->lines_on_order = 0;  
		$this->order_no = 0;
	}


} /* end of class defintion */

class labour_line_details 
{
	var $line_no;
	var $po_detail_rec;
	var $grn_item_id;
	var $labour_type;
	var $special_rate;
	var $labour_amount;
	var $consumable_category;
	var $unit;
	var $quantity;
	var $special_labour_cost;
	
	function labour_line_details($line_no, $labour_type, $special_rate,$labour_amount, $special_labour_cost)
	{
		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->labour_type = $labour_type;
		$this->special_rate = $special_rate;
		$this->labour_amount = $labour_amount;
		$this->special_labour_cost = $special_labour_cost;
		

	}
	
}

?>
