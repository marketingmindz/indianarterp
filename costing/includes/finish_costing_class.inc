<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/* Definition of the purch_order class to hold all the information for a purchase order and delivery
*/

class finish 
{
	var $trans_type; // order/grn/invoice (direct)
	var $line_items;
	var $special_cost_ref;
	var $special_rate;
	var $special_amount;
	var $total;
	
	var $order_no; /*Only used for modification of existing orders otherwise only established when order committed */
	var $lines_on_order = 0;
	
	function finish()
	{
		/*Constructor function initialises a new purchase order object */
		$this->line_items = array();
		$this->lines_on_order = $this->order_no = 0;
	}
	
	function add_to_finish_part($line_no,  $special_cost_ref,  $special_rate,$special_amount, $total)
	{
		$line_no ++; 
		$this->line_items[$line_no] = new finish_line_details($line_no,$special_cost_ref,$special_rate, $special_amount, $total);
		$this->lines_on_order++;
		return 1;
	}
	function update_finish_item($line_no,   $special_cost_ref,  $special_rate,$special_amount, $total)
	{

		$this->line_items[$line_no]->special_cost_ref = $special_cost_ref;
		$this->line_items[$line_no]->special_rate = $special_rate;
		$this->line_items[$line_no]->special_amount = $special_amount;
		$this->line_items[$line_no]->total = $total;
	}

	function remove_from_finish($line_no)
	{
		array_splice($this->line_items, $line_no, 1);
	}
	
	function finish_has_items() 
	{
		return count($this->line_items) != 0;
	}
	
	function clear_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		$this->lines_on_order = 0;  
		$this->order_no = 0;
	}


} /* end of class defintion */

class finish_line_details 
{
	var $line_no;
	var $po_detail_rec;
	var $special_cost_ref;
	var $special_rate;
	var $special_amount;
	var $total;

	
	function finish_line_details($line_no, $special_cost_ref, $special_rate,$special_amount, $total)
	{
		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->special_cost_ref = $special_cost_ref;
		$this->special_rate = $special_rate;
		$this->special_amount = $special_amount;
		$this->total = $total;
		

	}
	
}

?>
