<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/* Definition of the purch_order class to hold all the information for a purchase order and delivery
*/

class wood 
{

	var $trans_type; // order/grn/invoice (direct)
	var $line_items;
	var $wood_id;
	var $cft;
	var $rate;
	var $amount;
	
	var $unit;
	var $quantity;
	var $Location;
	var $pr_no;
	var $pr_date;
	var $location_id;
	var $supplier_id;
	var $order_no; /*Only used for modification of existing orders otherwise only established when order committed */
	var $lines_on_order = 0;
	
	function wood()
	{
		/*Constructor function initialises a new purchase order object */
		$this->line_items = array();
		$this->lines_on_order = $this->order_no = 0;
	}
	
	function add_to_wood_part($line_no, $wood_id,  $cft,  $rate,$amount)
	{
			$this->line_items[$line_no] = new pr_line_details($line_no, $wood_id,$cft, $rate, $amount);
			$this->lines_on_order++;
			return 1;
	}

	function update_wood_item($line_no, $wood_id, $cft, $rate,$amount)
	{
		$this->line_items[$line_no]->wood_id = $wood_id;
		$this->line_items[$line_no]->cft = $cft;
		$this->line_items[$line_no]->rate = $rate;
		$this->line_items[$line_no]->amount = $amount;
	}
	function remove_from_order($line_no)
	{
		array_splice($this->line_items, $line_no, 1);
	}
	
	function order_has_items() 
	{
		return count($this->line_items) != 0;
	}
	
	function clear_items() 
	{
    	unset($this->line_items);
		$this->line_items = array();
		$this->lines_on_order = 0;  
		$this->order_no = 0;
	}


} /* end of class defintion */

class pr_line_details 
{
	var $line_no;
	var $po_detail_rec;
	var $wood_id;
	var $cft;
	var $rate;
	var $amount;

	
	function pr_line_details($line_no, $wood_id, $cft, $rate,$amount)
	{
		/* Constructor function to add a new LineDetail object with passed params */
		$this->line_no = $line_no;
		$this->wood_id = $wood_id;
		$this->cft = $cft;
		$this->rate = $rate;
		$this->amount = $amount;

	}
	
}

?>
