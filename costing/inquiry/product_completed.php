<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPTRANSVIEW';
$path_to_root="../..";
include_once($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/purchasing/includes/db/suppliers_db.inc");

simple_page_mode(true);
//----------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{
	//initialise no input errors assumed initially before we test
	$input_error = 0;
	
	if ($input_error != 1) 
	{	
		if ($selected_id != -1) 
		{
			update_product($selected_id, $_POST['catagory'],$_POST['sub_category'],$_POST['purchase_price'],$_POST['lead_time']);
			display_notification(_('Selected product has been updated'));		
		} 
		else 
		{	
			add_product($supplier_id,$_POST['catagory'],$_POST['sub_category'],$_POST['purchase_price'],$_POST['lead_time']);
			display_notification(_('New product has been added'));
		}	
		$Mode = 'RESET';
	}
} 


if ($Mode == 'Delete')
{
	/*if (can_delete($selected_id))
	{*/
		delete_product($selected_id);
		display_notification(_('Selected product has been deleted'));
	/*}*/
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//-----------------------------------------------------------------------------------
$result = get_all_product_list($supplier_id);

start_form();
	start_table(TABLESTYLE, "colspan=7 width=80% style='margin-top:10px;'");
	$th = array(_("product_id"),_("supplier_id"),_("Category"),_("Sub Category "), _("Purchase Price"), _("Lead Time"), "", "");
	table_header($th);
	$k = 0;
	$i=1;
	while ($myrow = db_fetch($result)) 
	{
		
		alt_table_row_color($k);	
		//----------- get supplier id ----------
		$supp_id = get_product_supplier_id($myrow["supplier_id"]);
		$supp_name = $supp_id['supp_name'];
		
		$cons_id = get_product_cons_category($myrow["catagory"]);
		$cons_cat_name = $cons_id['master_name'];
		
		$sub_cons_id = get_product_cons_subcategory($myrow["sub_category"]);
		$sub_cons_name = $sub_cons_id['consumable_name'];

		label_cell($i);
		label_cell($supp_name);
		label_cell($cons_cat_name);
		label_cell($sub_cons_name);
		label_cell($myrow["purchase_price"]);
		label_cell($myrow["lead_time"]);
		edit_button_cell("Edit".$myrow['product_id'], _("Edit"));
		delete_button_cell("Delete".$myrow['product_id'], _("Delete"));
		end_row();
		$i++;
	}
	inactive_control_row($th);
	end_table(1);

	start_table(TABLESTYLE2);

	if ($selected_id != -1) 
	{
		if ($Mode == 'Edit') {
	
			$myrow = get_product($selected_id);
			$_POST['catagory']  = $myrow["catagory"];
			$_POST['sub_category']  = $myrow["sub_category"];
			$_POST['purchase_price']  = $myrow["purchase_price"];
			$_POST['lead_time']  = $myrow["lead_time"];
					
		}
		hidden('selected_id', $selected_id);
	} 
	$th = array(_("Category"),_("Sub Category "), _("Purchase Price"), _("Lead Time"));
	table_header($th);
	stock_master_list_cells(null, 'catagory', null, _('----Select---'));
		if($_POST['catagory'] == '-1'){
			sub_consumable_master_list_cells(null, 'sub_category',null, _('----Select---'));
		}else{
			customer_consumable_list_cells(null, $_POST['catagory'],'sub_category', null, false, true, true, true);
		}
		text_cells_ex(null, 'purchase_price', 15, 15);
		text_cells_ex(null, 'lead_time', 15, 15);
	
	end_table(1);

	submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();


?>








