<?php

/* List of installed additional extensions. If extensions are added to the list manually
	make sure they have unique and so far never used extension_ids as a keys,
	and $next_extension_id is also updated. More about format of this file yo will find in 
	FA extension system documentation.
*/

$next_extension_id = 26; // unique id for next installed extension

$installed_extensions = array (
  0 => 
  array (
    'name' => 'Arabic Egypt 8 digits COA - GAAP',
    'package' => 'chart_ar_EG-GAAP',
    'version' => '2.3.11-2',
    'type' => 'chart',
    'active' => false,
    'path' => 'sql',
    'sql' => 'ar_EG-8digits.sql',
  ),
  1 => 
  array (
    'name' => 'Arabic Egypt 8 digits COA - GAAP (English version)',
    'package' => 'chart_ar_EG-english',
    'version' => '2.3.11-2',
    'type' => 'chart',
    'active' => false,
    'path' => 'sql',
    'sql' => 'ar_EG-8digists-eng.sql',
  ),
  2 => 
  array (
    'name' => 'Arabic Egypt COA',
    'package' => 'chart_ar_EG-general',
    'version' => '2.3.5-1',
    'type' => 'chart',
    'active' => false,
    'path' => 'sql',
    'sql' => 'ar_EG-general.sql',
  ),
  4 => 
  array (
    'name' => 'Asset register',
    'package' => 'asset_register',
    'version' => '2.3.3-10',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/asset_register',
  ),
  5 => 
  array (
    'name' => 'Company Dashboard',
    'package' => 'dashboard',
    'version' => '2.3.15-5',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/dashboard',
  ),
  6 => 
  array (
    'package' => 'api',
    'name' => 'api',
    'version' => '-',
    'available' => '',
    'type' => 'extension',
    'path' => 'modules/api',
    'active' => false,
  ),
  7 => 
  array (
    'name' => 'Auth_LDAP',
    'package' => 'auth_ldap',
    'version' => '2.3.5-2',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/auth_ldap',
  ),
  8 => 
  array (
    'name' => 'Inventory Items CSV Import',
    'package' => 'import_items',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/import_items',
  ),
  9 => 
  array (
    'name' => 'Import Multiple Journal Entries',
    'package' => 'import_multijournalentries',
    'version' => '2.3.0-7',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/import_multijournalentries',
  ),
  10 => 
  array (
    'name' => 'Import Paypal transactions',
    'package' => 'import_paypal',
    'version' => '2.3.10-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/import_paypal',
  ),
  11 => 
  array (
    'name' => 'Import Transactions',
    'package' => 'import_transactions',
    'version' => '2.3.22-4',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/import_transactions',
  ),
  12 => 
  array (
    'name' => 'osCommerce Order and Customer Import Module',
    'package' => 'osc_orders',
    'version' => '2.3.0-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/osc_orders',
  ),
  13 => 
  array (
    'name' => 'Annual balance breakdown report',
    'package' => 'rep_annual_balance_breakdown',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_annual_balance_breakdown',
  ),
  14 => 
  array (
    'name' => 'Annual expense breakdown report',
    'package' => 'rep_annual_expense_breakdown',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_annual_expense_breakdown',
  ),
  15 => 
  array (
    'name' => 'Cash Flow Statement Report',
    'package' => 'rep_cash_flow_statement',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_cash_flow_statement',
  ),
  16 => 
  array (
    'name' => 'Check Printing based on Tom Hallman, USA',
    'package' => 'rep_check_print',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_check_print',
  ),
  17 => 
  array (
    'name' => 'Check Printing based on Tu Nguyen, Canada',
    'package' => 'rep_cheque_print',
    'version' => '2.3.0-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_cheque_print',
  ),
  18 => 
  array (
    'name' => 'Dated Stock Sheet',
    'package' => 'rep_dated_stock',
    'version' => '2.3.3-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_dated_stock',
  ),
  19 => 
  array (
    'name' => 'Inventory History',
    'package' => 'rep_inventory_history',
    'version' => '2.3.2-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_inventory_history',
  ),
  20 => 
  array (
    'name' => 'Sales Summary Report',
    'package' => 'rep_sales_summary',
    'version' => '2.3.3-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_sales_summary',
  ),
  21 => 
  array (
    'name' => 'Bank Statement w/ Reconcile',
    'package' => 'rep_statement_reconcile',
    'version' => '2.3.3-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_statement_reconcile',
  ),
  22 => 
  array (
    'name' => 'Tax inquiry and detailed report on cash basis',
    'package' => 'rep_tax_cash_basis',
    'version' => '2.3.7-4',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/rep_tax_cash_basis',
  ),
  23 => 
  array (
    'name' => 'Report Generator',
    'package' => 'repgen',
    'version' => '2.3.9-4',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/repgen',
  ),
  24 => 
  array (
    'name' => 'Requisitions',
    'package' => 'requisitions',
    'version' => '2.3.13-3',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/requisitions',
  ),
  25 => 
  array (
    'name' => 'zen_import',
    'package' => 'zen_import',
    'version' => '2.3.15-1',
    'type' => 'extension',
    'active' => false,
    'path' => 'modules/zen_import',
  ),
);
?>