<?php

// to perform multiple HTTP requests and process their results
require_once($path_to_root.'/syncing/HTTP_Client/Client.php');

// Zend Framework Integration
require_once($path_to_root.'/syncing/ZendFramework/library/Zend/Json.php');

include_once($path_to_root . "/admin/db/crmsetting_db.inc");


//------------------------ Get Vtiger data information for login ------------------------

    // Get vtiger data
    $data = get_crmsetting();

    //url path to vtiger/webservice.php like http://vtiger_url/webservice.php
    $endpointUrl = $data['crm_endpointurl'];

    //username of the user who is to logged in.
    $userName = $data['crm_username'];

    //access key of the user admin, found on my preferences page.
    $userAccessKey = $data['crm_access_key'];

    //Vtiger crm database hostname.
    $crm_db_hostname = $data['crm_db_hostname'];

    //Vtiger crm database username
    $crm_db_username = $data['crm_db_username'];

    //Vtiger crm database password.
    $crm_db_password = $data['crm_db_password'];

    //Vtiger crm database name.
    $crm_db_name = $data['crm_db_name'];

    //$db_connect = mysql_connect($crm_db_hostname , $crm_db_username , $crm_db_password);
    //mysql_select_db($crm_db_name , $db_connect);
    
    
    // check database connectivity
    /*if($crm_db_name == ''){
        echo "Database Name Not Found. Set database name in CRM Setting";
        die();    
    }else if(!$db_connect){
        echo "Invalid CRM Credentials.";
        die();    
    }*/

    //mysql_close($db_connect);

//----------------------------- Get challenge token -------------------------------------

    global $httpc;
    $httpc = new HTTP_CLIENT();

    //getchallenge request must be a GET request.
    $httpc->get("$endpointUrl?operation=getchallenge&username=$userName");
    $response = $httpc->currentResponse();

    //decode the json encode response from the server.
    $jsonResponse = Zend_JSON::decode($response['body']);

    //check for whether the requested operation was successful or not.
    if($jsonResponse['success']==false) 
        //handle the failure case.
        die('getchallenge failed:'.$jsonResponse['error']['errorMsg']);

    //operation was successful get the token from the response.
    $challengeToken = $jsonResponse['result']['token'];



//------------------------------ Get login access ----------------------------------------

    //create md5 string concatenating user accesskey from my preference page 
    //and the challenge token obtained from get challenge result. 
    $generatedKey = md5($challengeToken.$userAccessKey);

    //login request must be POST request.
    $httpc->post("$endpointUrl", 
        array('operation'=>'login', 'username'=>$userName, 
            'accessKey'=>$generatedKey), true);
    $response = $httpc->currentResponse();
	
    //decode the json encode response from the server.
    $jsonResponse = Zend_JSON::decode($response['body']);

    //operation was successful get the token from the reponse.
    if($jsonResponse['success']==false)
        //handle the failure case.
        die('login failed:'.$jsonResponse['error']['errorMsg']);

    //login successful extract sessionId and userId from LoginResult to it can used for further calls.
    $sessionId = $jsonResponse['result']['sessionName']; 
    $userId = $jsonResponse['result']['userId'];

?>