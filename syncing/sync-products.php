<?php


//-------------------------- Get data from vTiger Products Module --------------------
function sync_products()
{
    global $path_to_root;
    include_once($path_to_root . "/inventory/includes/db/finish_product_db.inc");
    global $httpc, $endpointUrl, $sessionId;
    //query to select data from the server.
    $query = "select count(*) from Products;";
    
    //urlencode to as its sent over http.
    $queryParam = urlencode($query);

    //sessionId is obtained from login result.
    $params = "sessionName=$sessionId&operation=query&query=$queryParam";

    //query must be GET Request.
    $httpc->get("$endpointUrl?$params");
    $response = $httpc->currentResponse();

    //decode the json encode response from the server.
    $jsonResponse = Zend_JSON::decode($response['body']);

    //operation was successful get the token from the reponse.
    if($jsonResponse['success']==false)
        //handle the failure case.
        die('query failed:'.$jsonResponse['errorMsg']);

    //Array of vtigerObjects
    $retrievedObjects = $jsonResponse['result'];
echo "<pre>";
    $total_products = $retrievedObjects[0]['count'];
    $products = Array();

    for($i = 0; $i <= $total_products;)
    {
        $query = "select * from Products limit ".$i.", ".($i+100).";";

        $queryParam = urlencode($query);
        $params = "sessionName=$sessionId&operation=query&query=$queryParam";

        $httpc->get("$endpointUrl?$params");
        $response = $httpc->currentResponse();
        $jsonResponse = Zend_JSON::decode($response['body']);

        if($jsonResponse['success']==false)
            die('query failed:'.$jsonResponse['errorMsg']);

        $retrievedObjects = $jsonResponse['result'];

        foreach ($retrievedObjects as $pro) {
            $products[] = $pro;
        }

        $i += 100;
    } 
   //count total products
    $count_products = count($products);
    
    $product_crm_id = get_product_crm_id();
    $product_finish_codes = get_finish_codes();

    foreach ($products as $pro) {
        if(in_array($pro['id'], $product_crm_id) OR in_array($pro['serial_no'], $product_finish_codes))
        {
            if(!in_array($pro['serial_no'], $product_finish_codes)){
                update_finish_product_crm_id($pro['serial_no'], $pro['id']);
            }
        }
        else
        {
            $header_design = Array();
            /*add_product_from_crm_to_erp($pro['firstname'], $pro['firstname'], $pro['otherstreet'], $pro['tax_id'], $pro['curr_code'], $pro['dimension_id'], $pro['dimension2_id'], $pro['credit_status'], $pro['payment_terms'], $pro['discount'], $pro['pymt_discount'], 
    $pro['credit_limit'], $pro['sales_type'], $pro['cf_759'], $pro['id']$header_design);*/
        }
    }
    display_notification("Products have been synced successfully.");
}


//-------------------------- Get data from vTiger Products Module --------------------
function sync_customers()
{
    global $httpc, $endpointUrl, $sessionId;
    $query = "select * from Contacts;";

    $queryParam = urlencode($query);

    $params = "sessionName=$sessionId&operation=query&query=$queryParam";

    //query must be GET Request.
    $httpc->get("$endpointUrl?$params");
    $response = $httpc->currentResponse();

    $jsonResponse = Zend_JSON::decode($response['body']);

    if($jsonResponse['success']==false)
        die('query failed:'.$jsonResponse['errorMsg']);

    $retrievedObjects = $jsonResponse['result'];
echo "<pre>";//print_r($retrievedObjects);
    
    $crm_id = get_customers_crm_id();

    foreach ($retrievedObjects as $cust) {
        if(in_array($cust['id'], $crm_id))
        {
            $customer_id = $cust['id'];
        }
        else
        {
            add_customer($cust['firstname'], $cust['firstname'], $cust['otherstreet'], $cust['tax_id'], $cust['curr_code'], $cust['dimension_id'], $cust['dimension2_id'], $cust['credit_status'], $cust['payment_terms'], $cust['discount'], $cust['pymt_discount'], 
    $cust['credit_limit'], $cust['sales_type'], $cust['cf_759'], $cust['id']);
        }
    }

   /* add_customer($CustName, $cust_ref, $address, $tax_id, $curr_code,
    $dimension_id, $dimension2_id, $credit_status, $payment_terms, $discount, $pymt_discount, 
    $credit_limit, $sales_type, $notes, $crm_id);
*/
    $count_products = count($retrievedObjects);
    display_notification("Contacts have been synced successfully.");
}


//-------------------------- Get data from vTiger Products Module --------------------
function sync_sales_order()
{
    global $path_to_root;
    include_once($path_to_root . "/sales/sales_order_entry.php");
    global $httpc, $endpointUrl, $sessionId;
    //query to select data from the server.
    $query = "select count(*) from SalesOrder;";
    
    //urlencode to as its sent over http.
    $queryParam = urlencode($query);

    //sessionId is obtained from login result.
    $params = "sessionName=$sessionId&operation=query&query=$queryParam";

    //query must be GET Request.
    $httpc->get("$endpointUrl?$params");
    $response = $httpc->currentResponse();

    //decode the json encode response from the server.
    $jsonResponse = Zend_JSON::decode($response['body']);

    //operation was successful get the token from the reponse.
    if($jsonResponse['success']==false)
        //handle the failure case.
        die('query failed:'.$jsonResponse['errorMsg']);

    //Array of vtigerObjects
    $retrievedObjects = $jsonResponse['result'];
echo "<pre>";
    $total_orders = $retrievedObjects[0]['count'];
    $sales_orders = Array();

    for($i = 0; $i <= $total_orders;)
    {
        $query = "select * from SalesOrder limit ".$i.", ".($i+100).";";

        $queryParam = urlencode($query);
        $params = "sessionName=$sessionId&operation=query&query=$queryParam";

        $httpc->get("$endpointUrl?$params");
        $response = $httpc->currentResponse();
        $jsonResponse = Zend_JSON::decode($response['body']);

        if($jsonResponse['success']==false)
            die('query failed:'.$jsonResponse['errorMsg']);

        $retrievedObjects = $jsonResponse['result'];

        foreach ($retrievedObjects as $order) {
            $sales_orders[] = $order;
        }

        $i += 100;
    } 
   //count total sales_orders
    $count_sales_orders = count($sales_orders);

    $order_crm_id = get_order_crm_id();

    

    /*foreach ($sales_orders as $ord) {
        if(in_array($ord['id'], $order_crm_id))
        {
            $crm_id = $ord['id'];
            //update_finish_product_crm_id($pro['serial_no'], $pro['id']);
        }
        else
        {
            create_cart(ST_SALESORDER, 0);
            add_product_from_crm_to_erp($pro['firstname'], $pro['firstname'], $pro['otherstreet'], $pro['tax_id'], $pro['curr_code'], $pro['dimension_id'], $pro['dimension2_id'], $pro['credit_status'], $pro['payment_terms'], $pro['discount'], $pro['pymt_discount'], 
    $pro['credit_limit'], $pro['sales_type'], $pro['cf_759'], $pro['id']$header_design);



        }
    }
    display_notification("Sales Orders have been synced successfully.");*/
}




?>