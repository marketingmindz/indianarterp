<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CRM_SYNCING';
$path_to_root="..";
include($path_to_root . "/includes/session.inc");
page(_($help_context = "CRM Syncing"));
include($path_to_root . "/includes/ui.inc");
include($path_to_root . "/admin/db/crmsetting_db.inc");

include_once($path_to_root."/syncing/crm-login-access.php");
include_once($path_to_root."/syncing/sync-products.php");


		//sync_customers();
		//sync_products();
		//sync_sales_order();
		//sync_customized_products();
?>

