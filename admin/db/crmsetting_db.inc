<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

//--------------------------------------------------------------------------------------

function add_crmsetting($crm_endpointurl, $crm_username, $crm_access_key, $crm_db_hostname , $crm_db_username, $crm_db_password, $crm_db_name, $crm_root_path, $syncingType)
{
	$sql = "INSERT INTO ".TB_PREF."crm_setting(crm_endpointurl, crm_username, crm_access_key, crm_db_hostname , crm_db_username, crm_db_password, crm_db_name, crm_root_path, syncingType)
		VALUES (" . db_escape($crm_endpointurl) . ", " .
		db_escape($crm_username). ", " .
		db_escape($crm_access_key). ", " .
		db_escape($crm_db_hostname). ", " .
		db_escape($crm_db_username). ", " .
		db_escape($crm_db_password). ", " .
		db_escape($crm_db_name). ", " .
		db_escape($crm_root_path). ", " .
		db_escape($syncingType). ")";

	db_query($sql,"The crm Setting not be added");
}

//--------------------------------------------------------------------------------------

function update_crmsetting($id, $crm_endpointurl, $crm_username, $crm_access_key, $crm_db_hostname , $crm_db_username, $crm_db_password, $crm_db_name, $crm_root_path, $syncingType)
{
	$sql = "UPDATE ".TB_PREF."crm_setting SET crm_endpointurl=" . db_escape($crm_endpointurl). " ,
		crm_username =" . db_escape($crm_username). " ,
		crm_access_key =" . db_escape($crm_access_key). " ,
		crm_db_hostname =" . db_escape($crm_db_hostname). " ,
		crm_db_username =" . db_escape($crm_db_username). " ,
		crm_db_password =" . db_escape($crm_db_password). " ,
		crm_db_name =" . db_escape($crm_db_name). " ,
		crm_root_path =" . db_escape($crm_root_path). " ,
		syncingType =" . db_escape($syncingType). "
		WHERE id = ".db_escape($id);

	db_query($sql,"The crm Setting could not be updated");
}

//--------------------------------------------------------------------------------------

function delete_crmsetting($selected_id)
{
	$sql="DELETE FROM ".TB_PREF."crm_setting WHERE id=".db_escape($selected_id);
	
	db_query($sql,"could not delete shipper");
}

//--------------------------------------------------------------------------------------

function get_crmset($show_inactive)
{
	$sql = "SELECT * FROM ".TB_PREF."crm_setting";
	if (!$show_inactive) $sql .= " WHERE !inactive";
	$sql .= " ORDER BY id";
	
	return db_query($sql,"could not get crm setting");
}

//--------------------------------------------------------------------------------------

function get_crmsetting()
{
	$sql = "SELECT * FROM ".TB_PREF."crm_setting";

	$result = db_query($sql, "could not get crm setting");
	return db_fetch($result);
}

?>