<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CRM_SYNCING';
$path_to_root="..";
include($path_to_root . "/includes/session.inc"); 
page(_($help_context = "CRM Syncing"));
include($path_to_root . "/includes/ui.inc");
include($path_to_root . "/admin/db/crmsetting_db.inc");

include_once($path_to_root."/syncing/crm-login-access.php");
include_once($path_to_root."/syncing/sync-products.php");


if (!@$_GET['popup'])
{
	$js = "";
	if ($use_popup_windows)
		$js .= get_js_open_window(900, 500);
	if ($use_date_picker)
		$js .= get_js_date_picker();
}

start_form();
div_start("Customer_Syncing");
	
	div_start("sync_result");
	$Ajax->activate('sync_result');

	if(isset($_POST['SyncCustomers'])){
		//sync_customers();
	}

	if(isset($_POST['SyncProducts'])){
		//sync_products();
	}

	/*if(isset($_POST['SyncSalesOrder'])){
		//sync_sales_order();
	}*/
	
	div_end();
	

	start_table(TABLESTYLE2, "style=width:50%;", 20, 5);
	start_row();
		label_cell("Customer Syncing");		
		button_cell("SyncCustomers", 'Sync Customers', 'Sync Customers from CRM to ERP', false, 'default');
	end_row();

	start_row();
		label_cell("Products Syncing");		
		button_cell("SyncProducts", 'Sync Products', 'Sync Products from CRM to ERP', false, 'default');
	end_row();

	start_row();
		label_cell("Sales Order Syncing");		
		button_cell("SyncSalesOrder", 'Sync Sales Order', 'Sync Sales Orders from CRM to ERP', false, 'default');
	end_row();

	end_table();
div_end();

end_form();
end_page();

?>

