<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CRM';
$path_to_root="..";
include($path_to_root . "/includes/session.inc");
page(_($help_context = "CRM Database Setting"));
include($path_to_root . "/includes/ui.inc");
include($path_to_root . "/admin/db/crmsetting_db.inc");
simple_page_mode(true);

//----------------------------------------------------------------------------------------------

function can_process() 
{
	if (strlen($_POST['crm_endpointurl']) == 0) 
	{
		display_error(_("The crm webservice.php url cannot be empty."));
		set_focus('crm_endpointurl');
		return false;
	}
	if (strlen($_POST['crm_username']) == 0) 
	{
		display_error(_("The crm username cannot be empty."));
		set_focus('crm_username');
		return false;
	}
	if (strlen($_POST['crm_access_key']) == 0) 
	{
		display_error(_("The crm access key cannot be empty."));
		set_focus('crm_access_key');
		return false;
	}
	if (strlen($_POST['crm_db_hostname']) == 0) 
	{
		display_error(_("The database hostname cannot be empty."));
		set_focus('crm_db_hostname');
		return false;
	}
	if (strlen($_POST['crm_db_username']) == 0) 
	{
		display_error(_("The database username cannot be empty."));
		set_focus('crm_db_username');
		return false;
	}
	if (strlen($_POST['crm_db_password']) == 0) 
	{
		display_error(_("The database password cannot be empty."));
		set_focus('crm_db_password');
		return false;
	}
	if (strlen($_POST['crm_db_name']) == 0) 
	{
		display_error(_("The database name cannot be empty."));
		set_focus('crm_db_name');
		return false;
	}
	if (strlen($_POST['crm_root_path']) == 0) 
	{
		display_error(_("The crm root path cannot be empty."));
		set_focus('crm_root_path');
		return false;
	}
	return true;
}

//----------------------------------------------------------------------------------------------

if ($Mode=='UPDATE_ITEM' && can_process()) 
{
	update_crmsetting($selected_id, $_POST['crm_endpointurl'], $_POST['crm_username'], $_POST['crm_access_key'], $_POST['crm_db_hostname'],$_POST['crm_db_username'],$_POST['crm_db_password'],$_POST['crm_db_name'],$_POST['crm_root_path']);
	display_notification(_('CRM Setting has been updated'));
	$Mode = 'RESET';
}

//----------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
// PREVENT DELETES IF DEPENDENT RECORDS IN 'sales_orders'
			delete_crmsetting($selected_id);
			display_notification(_('Selected CRM Setting has been deleted'));
		
	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}

//----------------------------------------------------------------------------------------------

$id = "";
$result = get_crmset(check_value('show_inactive'));
while ($myrow = db_fetch($result)) 
{
	$id =  $myrow["id"];	
}

if ($Mode=='ADD_ITEM' && can_process()) 
{	
	if($id){
		update_crmsetting($id,$_POST['crm_endpointurl'], $_POST['crm_username'], $_POST['crm_access_key'], $_POST['crm_db_hostname'],$_POST['crm_db_username'],$_POST['crm_db_password'],$_POST['crm_db_name'],$_POST['crm_root_path'], $_POST['syncingType']);
		display_notification(_('CRM Setting has been updated'));
		$Mode = 'RESET';
	}else{
		add_crmsetting($_POST['crm_endpointurl'], $_POST['crm_username'], $_POST['crm_access_key'], $_POST['crm_db_hostname'],$_POST['crm_db_username'],$_POST['crm_db_password'],$_POST['crm_db_name'],$_POST['crm_root_path'], $_POST['syncingType']);
		display_notification(_('New CRM Setting has been added'));
		$Mode = 'RESET';
	}
}	
	
//----------------------------------------------------------------------------------------------

start_form();
start_table(TABLESTYLE,  "style='width:80%!important'");

$k = 0;

		$myrow = get_crmsetting();
		$_POST['id'] 				= $myrow["id"];
		$_POST['crm_endpointurl']	= $myrow["crm_endpointurl"];
		$_POST['crm_username']		= $myrow["crm_username"];
		$_POST['crm_access_key']	= $myrow["crm_access_key"];
		$_POST['crm_db_hostname']	= $myrow["crm_db_hostname"];
		$_POST['crm_db_username']	= $myrow["crm_db_username"];
		$_POST['crm_db_password']	= $myrow["crm_db_password"];
		$_POST['crm_db_name']		= $myrow["crm_db_name"];
		$_POST['crm_root_path']		= $myrow["crm_root_path"];
		$_POST['syncingType']		= $myrow["syncingType"];
		
		text_row_ex(_("Endpoint Url :"), 'crm_endpointurl', 50, "-1");

		text_row_ex(_("CRM Username :"), 'crm_username', 50, "-1");

		text_row_ex(_("User Access Key :"), 'crm_access_key', 50, "-1");
		
		text_row_ex(_("CRM Database Hostname :"), 'crm_db_hostname', 50, "-1");

		text_row_ex(_("CRM Database Name :"), 'crm_db_name', 50, "-1");

		text_row_ex(_("Database Username :"), 'crm_db_username', 50, "-1");

		text_row_ex(_("Database Password :"), 'crm_db_password', 50, "-1");
		
		text_row_ex(_("CRM Root Path :"), 'crm_root_path', 50, "-1");
		start_row();
		label_cell("Syncing Type");
		echo "<td>";
		$type = array("auto"=>"Auto Syncing", "manual"=>"Manual Syncing");
		echo array_selector("destination", $_POST['syncingType'], $type);
		echo "</td>";
		end_row();
		
end_table(1);
		
submit_add_or_update_center($selected_id == -1, 'Save Setting', 'both');
submit_center("Delete".$myrow["id"], _("Delete"));

end_form();
end_page();

?>

